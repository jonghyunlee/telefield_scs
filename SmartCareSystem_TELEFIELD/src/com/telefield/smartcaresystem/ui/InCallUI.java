package com.telefield.smartcaresystem.ui;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.RemoteException;
import android.provider.Telephony;
import android.speech.tts.TextToSpeech;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.internal.telephony.ITelephony;
import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.database.ConnectDB;
import com.telefield.smartcaresystem.database.DBInfo;
import com.telefield.smartcaresystem.database.Users;
import com.telefield.smartcaresystem.lcd.SmartCareSystemLCDManager;
import com.telefield.smartcaresystem.power.SmartCareSystemPowerManager;
import com.telefield.smartcaresystem.telephony.TelephonyConfig;
import com.telefield.smartcaresystem.util.LogMgr;
import com.telefield.smartcaresystem.util.SmartCarePreference;
import com.telefield.smartcaresystem.util.SpeechUtil;

public class InCallUI extends Activity implements OnClickListener {

	private TextView callTimeView;
	private TextView callNumberView;
	private Button endCallButton, volumeUp, volumeDown;
	private ITelephony telephonyService;
	private String TimeSet;
	private long m_start_time, m_current_time = 0;
	private Timer m_timer;
	private AudioManager mAudioManager;
	private ImageView tempRssi, isCharge, tempBattery;
	private ProgressBar volumeProgress;
	private String callState;
	private Timer mTimer;
	private Context mContext;
	private int seconds, minutes, allCallTime, callLimit;
	private SmartCarePreference pref;
	private SmartCareSystemLCDManager lcdManager;
	/**
	 * true : 발신, false : 수신
	 */
	private boolean callingflag;
	
	/**
	 * true : 응급전화, false : 일반전화
	 */
	private static boolean isEmergency;
	private String calltype;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.incall_ui);
		mContext = this;
		callState = getIntent().getStringExtra(TelephonyConfig.CALL_STATE);

		callNumberView = (TextView) findViewById(R.id.callNumbertextView);
		String number = getIntent().getStringExtra(TelephonyConfig.CALL_NUMBER);
		if (number != null && number.length() >= 3){
			callNumberView.setText(number);
			calltype = (String) number.subSequence(0, 3);
		}
		else
		{
			callNumberView.setText(" ");
			calltype = "010";
		}
		callingflag = true;
		
		
		callTimeView = (TextView) findViewById(R.id.callTimetextView);
		tempBattery = (ImageView) findViewById(R.id.tempBattery);
		tempRssi = (ImageView) findViewById(R.id.tempRssi);
		isCharge = (ImageView) findViewById(R.id.isCharge);

		endCallButton = (Button) findViewById(R.id.endCallButton);
		volumeDown = (Button) findViewById(R.id.volume_down);
		volumeUp = (Button) findViewById(R.id.volume_up);
		endCallButton.setOnClickListener(this);
		volumeDown.setOnClickListener(this);
		volumeUp.setOnClickListener(this);

		volumeProgress = (ProgressBar) findViewById(R.id.volume_progress);

		TelephonyManager telephonyManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
		telephonyService = initTelephonyService(this, telephonyManager);
		telephonyManager.listen(mPhoneStateListener, PhoneStateListener.LISTEN_CALL_STATE | PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);

		SmartCareSystemPowerManager mgr = SmartCareSystemApplication.getInstance().getPwrMgr();
		lcdManager = SmartCareSystemApplication.getInstance().getLCDMgr();
		lcdManager.setLCDAlwaysOn();
		
		int batLevel = mgr.getBatt_level();
		if (batLevel < 20)
			tempBattery.setImageResource(R.drawable.icon_battery_01);
		else if (batLevel >= 20 && batLevel < 40)
			tempBattery.setImageResource(R.drawable.icon_battery_02);
		else if (batLevel >= 40 && batLevel < 60)
			tempBattery.setImageResource(R.drawable.icon_battery_03);
		else if (batLevel >= 60 && batLevel < 80)
			tempBattery.setImageResource(R.drawable.icon_battery_04);
		else if (batLevel >= 80)
			tempBattery.setImageResource(R.drawable.icon_battery_05);

		if (mgr.getBatt_status() == BatteryManager.BATTERY_STATUS_CHARGING) {
			isCharge.setVisibility(View.VISIBLE);
		} else {
			isCharge.setVisibility(View.GONE);
		}
		
		mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

		if (!callState.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
			volumeProgress.setProgress(mAudioManager.getStreamVolume(AudioManager.STREAM_RING));
			volumeProgress.setMax(mAudioManager.getStreamMaxVolume(AudioManager.STREAM_RING));
		}

		/*
		List<Users> queryResult = ConnectDB.getInstance(mContext).selectUser(new Users(null, null, number, null, null));
		if(queryResult != null && queryResult.size() > 0){
			SmartCarePreference prefs = new SmartCarePreference(mContext, SmartCarePreference.CALL_SETTING_INFO_FILENAME);
			//119와 지역센터 전화만 받는다.
			Users user = null;
			for(Users users : queryResult){
				if(users.getType().equals(DBInfo.HELPER_TYPE_119) || users.getType().equals(DBInfo.HELPER_TYPE_CENTER)){
					user = users;
				}
			}
		}*/
		//오는 모든 전화를 받는다 (55초 이후에)
		mTimer = new Timer();
		mTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				try {
					callState = getIntent().getStringExtra(TelephonyConfig.CALL_STATE);
					if(callState.equals(TelephonyManager.EXTRA_STATE_RINGING)){
						LogMgr.d(LogMgr.TAG, "Auto receive call....");
						telephonyService.answerRingingCall();	
					}
				} catch (Exception e) {
					answerPhoneHeadsethook(mContext);
				}
			}
			//1분이 되면 자동 착신 불가능....통신사에서 막아버림 "전화를 받지않아 소리셈으로 ... "
		}, 55 * 1000 /*prefs.getIntValue(SmartCarePreference.AUTO_RECEIVING_TIME, UIConstant.DEFAULT_AUTO_RECEIVE_TIME_VALUE) * 60 * 1000*/);
		
		pref = new SmartCarePreference(this, SmartCarePreference.CALL_SETTING_INFO_FILENAME);
		allCallTime = pref.getIntValue(SmartCarePreference.ALL_CALL_TIME, 0);
		callLimit = pref.getIntValue(SmartCarePreference.CALL_LIMIT_TIME, 10);
		
		/*
		 * 2015 03 10 통화가 시작되면 응급알람 플래그를 false로 바꾼다.
		 * 통화가 시작됫다는건 응급알람이 완전히 끝낫기 때문에.....
		 */
		SmartCareSystemApplication.getInstance().getMySmartCareState().setAlert119(false);
	}
	PhoneStateListener mPhoneStateListener = new PhoneStateListener() {

		@Override
		public void onCallStateChanged(int state, String incomingNumber) {
			if (state == TelephonyManager.CALL_STATE_IDLE)
				EndCall();
			else if(state == TelephonyManager.CALL_STATE_OFFHOOK){
				callState = TelephonyManager.EXTRA_STATE_OFFHOOK;
				if(m_timer == null){
					m_timer = new Timer();
					m_start_time = System.currentTimeMillis() - (m_current_time - m_start_time);
					m_timer.schedule(new UpdateTimeTask(), 1000, 1000);
					
					volumeProgress.setProgress(mAudioManager.getStreamVolume(AudioManager.STREAM_VOICE_CALL));
					volumeProgress.setMax(mAudioManager.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL));
				}				
			}else if(state == TelephonyManager.CALL_STATE_RINGING){
				//Ringing이 오면 수신이라는 뜻이다.
				callingflag = false;
			}
			super.onCallStateChanged(state, incomingNumber);
		}

		@Override
		public void onSignalStrengthsChanged(SignalStrength signalStrength) {
			setRssiValue(signalStrength.getGsmSignalStrength());

			super.onSignalStrengthsChanged(signalStrength);
		}

	};

	class UpdateTimeTask extends TimerTask {
		public void run() {
			m_current_time = System.currentTimeMillis();

			long millis = m_current_time - m_start_time;
			seconds = (int) (millis / 1000);
			minutes = seconds / 60;
			seconds = seconds % 60;
			
			TimeSet = String.format("%02d : %02d", minutes, seconds);

			callTimeView.post(run);
		}
	};
	
	Thread run = new Thread(new Runnable() {
		@Override
		public void run() {
			//080번은 무료통화이므로 종료시키지 않는다. //발신전화이고 일반전화일떄만 전화를 종료시킨다..
			if(calltype.equals("080") == false && callingflag && isEmergency == false)
			{				
					if(allCallTime + (int)((minutes * 60) + seconds) >= callLimit * 60 && callState.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)){
						SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.END_CALL, TextToSpeech.QUEUE_FLUSH, null);
						EndCall();
					}
			}
			callTimeView.setText(TimeSet);
		}
	});

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.endCallButton:
//			SmartCareSystemApplication.getInstance().getPhoneManager().answerCall();
			EndCall();
			break;
		case R.id.volume_down:
			changeVolume(-1);
			break;
		case R.id.volume_up:
			changeVolume(1);
			break;
		}
	}
	
	private void changeVolume(int value){
		if(callState.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)){
			volumeProgress.setProgress(volumeProgress.getProgress() + value);
			mAudioManager.setStreamVolume(AudioManager.STREAM_VOICE_CALL, volumeProgress.getProgress(), 0);
		}
		else{
			volumeProgress.setProgress(volumeProgress.getProgress() + value);
			mAudioManager.setStreamVolume(AudioManager.STREAM_RING, volumeProgress.getProgress(), 0);
		}
	}

	private void EndCall() {
		try {
			telephonyService.endCall();
		} catch (RemoteException e) {
		}
		finish();
	}

	private ITelephony initTelephonyService(Context context, TelephonyManager tm) {
		ITelephony telephonyService = null;
		try {
			Class c = Class.forName(tm.getClass().getName());
			Method m = c.getDeclaredMethod("getITelephony");
			m.setAccessible(true);
			telephonyService = (ITelephony) m.invoke(tm);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return telephonyService;
	}

	private void setRssiValue(int signalPower) {
		if (signalPower >= 0 && signalPower <= 31) {
			int dBm = -113 + (2 * signalPower);
			if (dBm <= -113)
				tempRssi.setImageResource(R.drawable.icon_rssi_05);
			else if (dBm > -113 && dBm <= -98)
				tempRssi.setImageResource(R.drawable.icon_rssi_04);
			else if (dBm > -98 && dBm <= -83)
				tempRssi.setImageResource(R.drawable.icon_rssi_03);
			else if (dBm > -83 && dBm <= -68)
				tempRssi.setImageResource(R.drawable.icon_rssi_02);
			else if (dBm > -68 && dBm <= -53)
				tempRssi.setImageResource(R.drawable.icon_rssi_01);
		} else {
			tempRssi.setImageResource(R.drawable.icon_rssi_05);
		}
	}
	
	private void answerPhoneHeadsethook(Context context) {
		// Simulate a press of the headset button to pick up the call
		Intent buttonDown = new Intent(Intent.ACTION_MEDIA_BUTTON);		
		buttonDown.putExtra(Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_HEADSETHOOK));
		context.sendOrderedBroadcast(buttonDown, "android.permission.CALL_PRIVILEGED");

		// froyo and beyond trigger on buttonUp instead of buttonDown
		Intent buttonUp = new Intent(Intent.ACTION_MEDIA_BUTTON);		
		buttonUp.putExtra(Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_HEADSETHOOK));
		context.sendOrderedBroadcast(buttonUp, "android.permission.CALL_PRIVILEGED");
	}
	
	@Override
	protected void onStop() {
		if(callingflag){
			//발신전화만 총 통화량에 포함 시킨다.
			if(isEmergency || calltype.equals("080")){
				//응급전화, 080번호는 총통화량에 포함시키지 않는다....
				//응급전화를 종료한 경우에 상황이 종료된 것으로 판단 한다.
			}else{
				allCallTime = allCallTime + (int)((minutes * 60) + seconds);
				pref = new SmartCarePreference(mContext, SmartCarePreference.CALL_SETTING_INFO_FILENAME);
				pref.setIntValue(SmartCarePreference.ALL_CALL_TIME, allCallTime);
			}	
		}
		LogMgr.d(LogMgr.TAG, "callingflag : " + callingflag + "..isEmergency : " + isEmergency +"..allCallTime : " 
		+ allCallTime + "..callLimitTime : " + callLimit + "..Call Minute:"+ minutes + "..Call Seconds :" + seconds);
		
		isEmergency = false;
		
		lcdManager.setLCDOff();
		if(m_timer != null)
			m_timer.cancel();
		
		if(mTimer != null)
			mTimer.cancel();
		super.onStop();
	}
	
	public void setIsEmergency(boolean isEmergency){
		InCallUI.isEmergency = isEmergency;
	}
	
}
