package com.telefield.smartcaresystem.ui;

import java.util.Calendar;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.hardware.usb.UsbManager;
import android.media.AudioManager;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gcm.GCMRegistrar;
import com.telefield.smartcaresystem.Constant;
import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.firmware.UpgradeController;
import com.telefield.smartcaresystem.m2m.GatewayDevice;
import com.telefield.smartcaresystem.ui.testcase.EventTestCaseUI;
import com.telefield.smartcaresystem.util.AppMonitorService;
import com.telefield.smartcaresystem.util.EmergencyAction;
import com.telefield.smartcaresystem.util.LogMgr;
import com.telefield.smartcaresystem.util.SmartCarePreference;

public class LauncherUI extends Activity implements OnClickListener {
	int[] buttonIDs = {R.id.helperButton,/** R.id.oneOneNineButton, R.id.centerButton, R.id.cancelButton, R.id.careButton1,*/ R.id.careButton2, R.id.menuButton};
	final private String REQUEST_STATE = "REQUEST_STATE";

	SmartCareSystemApplication mainApp;
	EmergencyAction emergencyCallUtil;
	AudioManager mAudioManager;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		PackageInfo info = null;
		String version = "0.0.0";
		try {
			info = this.getPackageManager().getPackageInfo(this.getPackageName(), 0);
			version = info.versionName;
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		LogMgr.d(LogMgr.TAG, "LauncherUI onCreate() ...System version : " + version);
		super.onCreate(savedInstanceState);
		
		SmartCareSystemApplication.getInstance().getLCDMgr().setWindow(getWindow());
		
		
//		GCMRegistrar.checkDevice(this);
//		GCMRegistrar.checkManifest(this);				
//		GCMRegistrar.register(this, Constant.SENDER_ID);
//		LogMgr.d(LogMgr.TAG,"GCMRegistrar : "+GCMRegistrar.isRegistered(this));
		//150106 매인앱에서 예외가 발생하면 주기보고가 정상적으로 가지 않던 문제 수정 <--
		if(mainApp == null){
			LogMgr.d(LogMgr.TAG,"MainApp is Null ... LuncherUI onCreate");
			mainApp = SmartCareSystemApplication.getInstance();
			mainApp.stopPeriodReportTimer();
			mainApp.startPeriodReportTimer();
			
			mainApp.stopNonActivityReportTimer();
			mainApp.startNonActivityReportTimer();
		}
		//-->
		
		//mainApp.setChangedContext(this);	//Hook On/Off 시 exception 발생하여 추가함
		emergencyCallUtil = mainApp.getEmergencyCallUtil();
		
		setContentView(R.layout.launcher_ui);
		
		for(int i=0;i<buttonIDs.length;i++) {
			Button button = (Button) findViewById(buttonIDs[i]);
			if(button != null)
				button.setOnClickListener(this);
		}
		Intent intent = getIntent();
		
		if( intent != null && (intent.getAction() == UsbManager.ACTION_USB_ACCESSORY_ATTACHED ||
				 intent.getAction() == UsbManager.ACTION_USB_DEVICE_ATTACHED))
		 {
			if( intent.getAction() == UsbManager.ACTION_USB_ACCESSORY_ATTACHED )
				mainApp.getMessageManager().createFtdiController(true);
			else if (intent.getAction() == UsbManager.ACTION_USB_DEVICE_ATTACHED )
				mainApp.getMessageManager().createFtdiController(false);
			
			mainApp.getMessageManager().ftdi_connect();		
			
		 }

		//LockScreen
		startService(new Intent(this, LockScreenService.class));
		//startService(new Intent(this, AppMonitorService.class));

		String number = SmartCareSystemApplication.getInstance().getMyPhoneNumber();
		if (number != null) {
			byte[] bytenum = number.getBytes();
			System.arraycopy(bytenum, 0, GatewayDevice.getInstance().number, 0, bytenum.length);
		}

		registerAlarm();

		mainApp.getSmartCareHealthApp().bloodPressureStartService();
		mainApp.acquireWakeLock();

		//기본적으로 앱모니터링은 true이므로, APK 설치 후 실행할 때부터 적용되어야 한다.
		SmartCarePreference pref = new SmartCarePreference(this, SmartCarePreference.APPLICATION_SETTING_INFO_FILENAME);
		boolean isAppMonitoring = pref.getBooleanValue(SmartCarePreference.START_APP_MONITORING, true);
		if(isAppMonitoring) {
			pref.setBooleanValue(SmartCarePreference.START_APP_MONITORING, isAppMonitoring);
			startService(new Intent(this, AppMonitorService.class));
			AppMonitorService.setAppMonitorService(isAppMonitoring);
		}

		//상태바 비활성화도 마찬가지이다.
		boolean disableSatusbar = pref.getBooleanValue(SmartCarePreference.CHARGE_STATUSBAR_STATE, true);
		if(disableSatusbar) {
			pref.setBooleanValue(SmartCarePreference.CHARGE_STATUSBAR_STATE, disableSatusbar);
			mainApp.getEnableDisableStatusBar().enableOrDisableStatusBar(disableSatusbar);
		}
		
		mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		int volume = pref.getIntValue(SmartCarePreference.VOLUME_SETTING, mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
		mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, volume, AudioManager.FLAG_PLAY_SOUND);
		
//		mAudioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_SAME, AudioManager.FLAG_PLAY_SOUND);
		LogMgr.d(LogMgr.TAG, "volume level = " + volume);
	
	}

	public void registerAlarm()
	{
		AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);

		Intent checkIntent = new Intent(Constant.DELETE_HISTORY_ACTION);
	    PendingIntent checkSender = PendingIntent.getBroadcast(this, 0, checkIntent, PendingIntent.FLAG_NO_CREATE);

	    LogMgr.d(LogMgr.TAG, "LauncherUI.java:registerAlarm:// checkSender : " + checkSender);

	    if(checkSender != null){
	    	Intent deleteIntent = new Intent(Constant.DELETE_HISTORY_ACTION);
		    PendingIntent deleteSender = PendingIntent.getBroadcast(this, 0, deleteIntent, PendingIntent.FLAG_UPDATE_CURRENT);

		    if(deleteSender != null){
		    	am.cancel(deleteSender);
		    	deleteSender.cancel();
		    }
	    }

	    Intent intent = new Intent(Constant.DELETE_HISTORY_ACTION);
	    PendingIntent sender = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

	    Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis());
		calendar.add(Calendar.DATE, 1);
		calendar.set(Calendar.HOUR_OF_DAY, 10);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND,0);

		am.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, sender);
	}


	@Override
	protected void onNewIntent(Intent intent) {

		LogMgr.d(LogMgr.TAG,"onNewIntent :: " + intent);

		// TODO Auto-generated method stub
		super.onNewIntent(intent);

		if(Constant.DEBUG_OPTION)
			Toast.makeText(this, "onNewIntent :: " + intent, Toast.LENGTH_SHORT).show();

		 if(intent != null &&
				 (intent.getAction() == UsbManager.ACTION_USB_ACCESSORY_ATTACHED ||
				 intent.getAction() == UsbManager.ACTION_USB_DEVICE_ATTACHED
				 ))
		 {
			 if( mainApp != null)
			 {
				 LogMgr.d(LogMgr.TAG, "mainApp null.... ftdi_dsisconnect()....");
				 mainApp.getMessageManager().ftdi_disconnect();
				 //mainApp.applicationExit();
				 mainApp.setChangedContext(this);
			 }
			 //mainApp = SmartCareSystemApplication.createInstance(this/*getApplicationContext()*/);
			 //mainApp.init();
			 
			 //AVAD_yhan 20141120 펌웨어 업데이트중 accessory attached 등을 받은경우 초기화 시키도록.			 
			 SmartCareSystemApplication.getInstance().setCodiFirmwareUpgrade(null);
			 UpgradeController.getInstance().setState(UpgradeController.CONTROLLER_IDLE_STATE);
			 
			 if( intent.getAction() == UsbManager.ACTION_USB_ACCESSORY_ATTACHED )
					mainApp.getMessageManager().createFtdiController(true);
				else if (intent.getAction() == UsbManager.ACTION_USB_DEVICE_ATTACHED )
				{
					mainApp.getMessageManager().createFtdiController(false);

					SmartCareSystemApplication.getInstance().getPwrMgr().setCanModeChange(true);
				}

			 if(mainApp != null)
				 mainApp.getMessageManager().ftdi_connect();
		 }
	}

	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {
	        if (event.getAction() == KeyEvent.ACTION_DOWN) {
	            switch (event.getKeyCode()) {
	                case KeyEvent.KEYCODE_BACK:
	                    return true;
	                case KeyEvent.KEYCODE_HOME:
	                    return true;
	            }
	        }
	        return super.dispatchKeyEvent(event);
	}
	@Override
	protected void onDestroy() {
		LogMgr.d(LogMgr.TAG, "LauncherUI onDestroy().");

		super.onDestroy();

		mainApp.acquireWakeLock();

		AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);

		Intent checkIntent = new Intent(Constant.DELETE_HISTORY_ACTION);
	    PendingIntent checkSender = PendingIntent.getBroadcast(this, 0, checkIntent, PendingIntent.FLAG_NO_CREATE);

	    LogMgr.d(LogMgr.TAG, "LauncherUI.java:onDestroy:// checkSender : " + checkSender);

	    //Destroy에서 앱모니터링을 종료할 때는 Preference의 값과 상관없이 앱모니터링을 하고 있으면 종료시킨다.
	    if(AppMonitorService.isAppMonitorService()) {
	    	stopService(new Intent(this, AppMonitorService.class));
	    	AppMonitorService.setAppMonitorService(false);
	    }

	    //Destroy에서는 Preference의 값과 상관없이 상태바를 활성화 시킨다.
	    mainApp.getEnableDisableStatusBar().enableOrDisableStatusBar(false);

	    if(checkSender != null){
	    	Intent deleteIntent = new Intent(Constant.DELETE_HISTORY_ACTION);
		    PendingIntent deleteSender = PendingIntent.getBroadcast(this, 0, deleteIntent, PendingIntent.FLAG_UPDATE_CURRENT);

		    LogMgr.d(LogMgr.TAG, "LauncherUI.java:onDestroy:// deleteSender : " + deleteSender);
		    if(deleteSender != null){
		    	am.cancel(deleteSender);
		    	deleteSender.cancel();
		    }
	    }

	    mainApp.getSmartCareHealthApp().bloodPressureStopService();

		mainApp.getMessageManager().ftdi_disconnect();
		mainApp.applicationExit();
		mainApp = null;

		//android.os.Process.killProcess(android.os.Process.myPid());

		//LockScreen
		//stopService(new Intent(this, LockScreenService.class));

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		//USIM이 없는경우 전화를 하지 않는다. 메뉴버튼은 동작해야함..
		TelephonyManager tm = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
		if(tm.getSimState() == TelephonyManager.SIM_STATE_ABSENT && v.getId() != R.id.menuButton)
			return;
	
		switch(v.getId()) {
				case R.id.helperButton:
//					Intent intent = new Intent(getApplicationContext(), EventTestCaseUI.class);
//					startActivity(intent);
				emergencyCallUtil.CallHelper();
				break;
			case R.id.careButton2:
				emergencyCallUtil.CallCare2();
				break;
			case R.id.menuButton:
				startSettingUI();
				break;
				

//				case R.id.oneOneNineButton:
//				emergencyCallUtil.Call119(null, Constant.EMERGENCY);
//				break;				
//				case R.id.careButton1:
//				emergencyCallUtil.CallCare1();
//				break;
//				case R.id.centerButton:
//				emergencyCallUtil.CallCenter();
//				break;
//			case R.id.cancelButton:
//				//startActivity(new Intent(this, bttest.class));
//				emergencyCallUtil.actionStop();
//				break;

		}
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		//LuncherUI가 실행될때마다 GCM을 등록한다..
		
		GCMRegistrar.checkDevice(this);
		GCMRegistrar.checkManifest(this);	
		
		final String regId = GCMRegistrar.getRegistrationId(this);
        GCMRegistrar.register(this, Constant.SENDER_ID);
		LogMgr.d(LogMgr.TAG,"GCMRegistrar.isRegistered : "+GCMRegistrar.isRegistered(this) + "...regId : " + regId);
		
		super.onResume();
	}
	private void startSettingUI() {
		// TODO Auto-generated method stub
		Intent intent = /*new Intent(this, SettingPasswordUI.class);*/new Intent(this, PasswordSelectUI.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}
}
