package com.telefield.smartcaresystem.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.ui.hiddenmenu.HiddenMenuSettingUI;
import com.telefield.smartcaresystem.ui.log.LogHistoryListViewActivity;
import com.telefield.smartcaresystem.ui.testcase.TestCaseUI;

public class PasswordSelectUI extends Activity implements OnClickListener{
	Context mContext;
	int[] buttonID = {R.id.oneButton, R.id.twoButton, R.id.threeButton, R.id.fourButton, R.id.fiveButton, R.id.sixButton, R.id.homeButton};
	StringBuffer strb;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.password_select_ui);
        
		//0x11 플래그로 이 Activity에 이동될 때는 LCD가 ON 되어야 한다.
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
		
		mContext = this;
		strb = new StringBuffer();
		for(int i = 0; i < buttonID.length; i++){
			Button button = (Button) findViewById(buttonID[i]);
			if(button != null)
				button.setOnClickListener(this);
		}
	}
	
	protected void onPause() {
		strb.delete(0, strb.capacity());
		super.onPause();
	};
	
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.oneButton:
			strb.append("1");
//			SmartCareSystemApplication.getInstance().getSpeechUtil().speech("일", TextToSpeech.QUEUE_FLUSH, null);
			break;
		case R.id.twoButton:
			strb.append("2");
//			SmartCareSystemApplication.getInstance().getSpeechUtil().speech("이", TextToSpeech.QUEUE_FLUSH, null);
			break;
		case R.id.threeButton:
			strb.append("3");
//			SmartCareSystemApplication.getInstance().getSpeechUtil().speech("삼", TextToSpeech.QUEUE_FLUSH, null);
			break;
		case R.id.fourButton:
			strb.append("4");
//			SmartCareSystemApplication.getInstance().getSpeechUtil().speech("사", TextToSpeech.QUEUE_FLUSH, null);
			break;
		case R.id.fiveButton:
			strb.append("5");
//			SmartCareSystemApplication.getInstance().getSpeechUtil().speech("오", TextToSpeech.QUEUE_FLUSH, null);
			break;
		case R.id.sixButton:
			strb.append("6");
//			SmartCareSystemApplication.getInstance().getSpeechUtil().speech("육", TextToSpeech.QUEUE_FLUSH, null);
			break;
		case R.id.homeButton:
			strb.delete(0, strb.capacity());
			Intent intent = new Intent(mContext, LauncherUI.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
			break;
		}
		String password = new String(strb);
		if(password.matches(".*"+UIConstant.INIT_PASSWORD_NUM+".*")){
			Intent intent = new Intent(mContext, SettingMenuUI.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
			finish();
		}
		else if(password.matches(".*"+UIConstant.DEBUG_PASSWORD_NUM+".*")){
			Intent intent = new Intent(mContext, DebugUI.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
			finish();
		}
		else if(password.matches(".*"+UIConstant.MESSAGE_VIEW_NUM+".*")){
			Intent intent = new Intent(mContext, LogHistoryListViewActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
			finish();
		}
		else if(password.matches(".*"+UIConstant.HIDDEN_PASSWORD_NUM+".*")){
			Intent intent = new Intent(mContext, HiddenMenuSettingUI.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
			finish();
		}
		else if(password.matches(".*"+UIConstant.TEST_CASE_NUM+".*")){
			Intent intent = new Intent(mContext, TestCaseUI.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
			finish();
		}
		else if(password.matches(".*"+UIConstant.MAIN_NUM+".*")){
			Intent intent = new Intent(mContext, LauncherUI.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
			finish();
		}
		
	}
//	TextWatcher textWatcherInput = new TextWatcher() {
//		@Override
//		public void onTextChanged(CharSequence s, int start, int before, int count) {
//			// TODO Auto-generated method stub
//			if(pw.getText().toString().equals(UIConstant.INIT_PASSWORD_NUM)) {
//				Intent intent = new Intent(mContext, SettingMenuUI.class);
//				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//				startActivity(intent);
//				finish();
//			}
//			else if(pw.getText().toString().equals(UIConstant.DEBUG_PASSWORD_NUM)){
//				Intent intent = new Intent(mContext, DebugUI.class);
//				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//				startActivity(intent);
//				finish();
//			}
//			else if(pw.getText().toString().equals(UIConstant.MESSAGE_VIEW_NUM)){
//				Intent intent = new Intent(mContext, LogHistoryListViewActivity.class);
//				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//				startActivity(intent);
//				finish();
//			}
//			else if(pw.getText().toString().equals(UIConstant.HIDDEN_PASSWORD_NUM)){
//				Intent intent = new Intent(mContext, HiddenMenuSettingUI.class);
//				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//				startActivity(intent);
//				finish();
//			}
//			else if(pw.getText().toString().equals(UIConstant.TEST_CASE_NUM)){
//				Intent intent = new Intent(mContext, TestCaseUI.class);
//				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//				startActivity(intent);
//				finish();
//			}
//			else if(pw.getText().toString().equals(UIConstant.MAIN_NUM)){
//				Intent intent = new Intent(mContext, LauncherUI.class);
//				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//				startActivity(intent);
//				finish();
//			}
//		}
//		
//		@Override
//		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//			// TODO Auto-generated method stub
//			
//		}
//
//		@Override
//		public void afterTextChanged(Editable s) {
//			// TODO Auto-generated method stub
//			
//		}
//	};
}
