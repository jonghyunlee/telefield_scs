package com.telefield.smartcaresystem.ui;

import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.util.Log;

public class LockScreenService extends Service {
	private LockScreenReceiver mReceiver = null;
	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	 
	public void onCreate() {
		super.onCreate();
		
		mReceiver = new LockScreenReceiver();		
		
		IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_OFF);
		
		registerReceiver(mReceiver, filter);
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId){
		super.onStartCommand(intent, flags, startId);
		Log.e("tests","start");
		if(intent != null) {
			if(intent.getAction()==null) {
				if(mReceiver==null) {
					mReceiver = new LockScreenReceiver();					
					
					IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_OFF);
					
					registerReceiver(mReceiver, filter);
				}
			}
		}
		
		return START_REDELIVER_INTENT;
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		
		if(mReceiver != null) {
			unregisterReceiver(mReceiver);
		}
	}
	
}
