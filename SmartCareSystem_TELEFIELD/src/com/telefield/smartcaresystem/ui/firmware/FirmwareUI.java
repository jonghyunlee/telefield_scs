package com.telefield.smartcaresystem.ui.firmware;

import android.app.Activity;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.firmware.UpgradeController;
import com.telefield.smartcaresystem.firmware.codi.CodiFirmwareUpgrade;
import com.telefield.smartcaresystem.util.LogMgr;

public class FirmwareUI extends Activity implements IFirmwareUpdateCallback{
	//public static final int DO_DOWNLOAD = 1;
	
	TextView firmwareDescription;
	ProgressBar progressBar;
	
	CodiFirmwareUpgrade cfu;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.firnware_ui);
		
		//업데이트 UI는 화면이 계속 켜진 상태로 유지한다.
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
		
		firmwareDescription = (TextView) findViewById(R.id.firmware_description);
		progressBar = (ProgressBar) findViewById(R.id.firmware_progressBar);
		
		UpgradeController.getInstance().setState(UpgradeController.CONTROLLER_RUNNING_STATE);
		
		cfu = new CodiFirmwareUpgrade(this);
		cfu.setListener(this);
		SmartCareSystemApplication.getInstance().setCodiFirmwareUpgrade(cfu);
		
		LogMgr.d(LogMgr.TAG,"FirmwareUI onCreate");
		cfu.firmwareUpgrade();
		

	}

	@Override
	public void notify(int result, int percentage, String text) {
		if( result == RESULT_COMPL)
		{
			LogMgr.d(LogMgr.TAG,"firmware download complete!!!!");
			progressBar.setProgress(100);
			firmwareDescription.setText(IFirmwareUpdateCallback.RESULT_UPGRAGE_OK_STR);
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			finish();
					
		}
		else if( result == IFirmwareUpdateCallback.RESULT_OK)
		{
			LogMgr.d(LogMgr.TAG,"firmware downloading");
			progressBar.setProgress(percentage);
			if( text == null ) firmwareDescription.setText(IFirmwareUpdateCallback.RESULT_UPGRAGE_ING_STR + " " + percentage +"%");
		}else
		{
			//fail
			firmwareDescription.setText(IFirmwareUpdateCallback.RESULT_UPGRAGE_FAIL_STR);
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			finish();
		}
		
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		
		LogMgr.d(LogMgr.TAG,"FirmwareUI onStop");
		
		SmartCareSystemApplication.getInstance().setCodiFirmwareUpgrade(null);
		UpgradeController.getInstance().setState(UpgradeController.CONTROLLER_IDLE_STATE);
	}
	
	
}
