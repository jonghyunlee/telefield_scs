package com.telefield.smartcaresystem.ui.firmware;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.telefield.smartcaresystem.Constant;
import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.firmware.ApplicationManager;
import com.telefield.smartcaresystem.firmware.UpgradeController;
import com.telefield.smartcaresystem.firmware.codi.CodiFirmwareUpgrade;
import com.telefield.smartcaresystem.http.AsyncCallback;
import com.telefield.smartcaresystem.http.AsyncFileDownloader;
import com.telefield.smartcaresystem.message.MessageID;
import com.telefield.smartcaresystem.util.LogMgr;
import com.telefield.smartcaresystem.util.SpeechUtil;

public class AppdownloadUI extends Activity implements AppdownloadCallback{
	//public static final int DO_DOWNLOAD = 1;
	
	TextView downloadDescription;
	ProgressBar progressBar;
	
	CodiFirmwareUpgrade cfu;
	Activity activity;
	boolean remoteUpdate;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.appupdate_ui);
		
		//업데이트 UI는 화면이 계속 켜진 상태로 유지한다.
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
		
		String tel_number = SmartCareSystemApplication.getInstance().getMyPhoneNumber();
		activity =  this;
		if(activity.getIntent() == null || activity.getIntent().getAction() == null){
			
		}
		
		if(activity.getIntent().getAction() == "localUpdate"){
			remoteUpdate = false;
		}else{
			remoteUpdate = activity.getIntent().getAction().equals("remoteUpdate");
		}

		downloadDescription = (TextView) findViewById(R.id.firmware_description);
		downloadDescription.setText("스마트폰 펌웨어 다운로드 진행중...");
//		progressBar = (ProgressBar) findViewById(R.id.firmware_progressBar);  
		File file = new File(Constant.FIRMWARE_PATH);
		if(!file.exists())
			file.mkdirs();
		
		new AsyncFileDownloader(activity.getApplicationContext()).download(Constant.GATEWAY_FW_DOWNLOAD_PATH + "/" + tel_number, Constant.FW_APP_PATH, fileDownCallback);
		
	}
	
	private AsyncCallback<File> fileDownCallback = new AsyncCallback.Base<File>() {
		@Override
		public void onResult(File result) {
			LogMgr.d("APP_UPDATE","APP_UPDATE_DOWNLOAD_END");

			LogMgr.d(LogMgr.TAG, "AppdownloadUI GATEWAY_FIRMWARE_UPDATE result.getAbsolutePath() : " + result.getAbsolutePath());
			LogMgr.d(LogMgr.TAG, "AppdownloadUI GATEWAY_FIRMWARE_UPDATE result.length() : " + result.length());
			
			//원격 업데이트에서는 음성이 나오면 안된다.
			if(remoteUpdate == true){
//				SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.START_APPLICATION_UPDATE, TextToSpeech.QUEUE_FLUSH, null);
			}else
				SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.START_APPLICATION_UPDATE, TextToSpeech.QUEUE_FLUSH, null);

			if (SmartCareSystemApplication.getInstance().getMessageManager().ftdi_isConnect()) {
				SmartCareSystemApplication.getInstance().getGlobalMsgFunc().sendGateWayMsg((byte) MessageID.CODI_RESET);
			}
			remoteUpdate = false;
			updateAPK();
		}
		
		public void exceptionOccured(Exception e) {
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			String stackTrace = sw.toString();
			LogMgr.e(LogMgr.TAG,"exception ::" + stackTrace);
			SmartCareSystemApplication.getInstance().getSpeechUtil().speech("어플리케이션 업데이트 실패하였습니다.", TextToSpeech.QUEUE_FLUSH, null);
			onBackPressed();
			
		};
	};
	
	private void updateAPK() {
		LogMgr.d("APP_UPDATE", "updateAPK.............");
		ApplicationManager am;

		try {
			am = new ApplicationManager(activity);
			am.installPackage(Constant.FW_APP_PATH);
			LogMgr.d(LogMgr.TAG, "Constant.FW_APP_PATH : " + Constant.FW_APP_PATH);
		} catch (Exception e) {
			e.printStackTrace();
		}
		ActivityManager mgr = (ActivityManager) activity.getSystemService(Activity.ACTIVITY_SERVICE);
		mgr.killBackgroundProcesses(activity.getPackageName());
		LogMgr.d(LogMgr.TAG, "activity.getPackageName() : " + activity.getPackageName());
		Log.d("APP_UPDATE",activity.getPackageName());
		//android.os.Process.killProcess(android.os.Process.myPid());
	}

	@Override
	public void notify(int result, int percentage, String text) {
		// TODO Auto-generated method stub
		
	}	
}

