package com.telefield.smartcaresystem.ui.firmware;

public interface AppdownloadCallback {
	
	public static final int RESULT_COMPL = 1;
	public static final int RESULT_OK = 0;
	public static final int RESULT_FAIL = -1;
	public static final int RESULT_STOP = -2;
	
	public static final String RESULT_UPGRAGE_FAIL_STR = "다운로드 진행을 실패하였습니다.";
	public static final String RESULT_UPGRAGE_OK_STR   = "다운로드에 성공하였습니다.";
	public static final String RESULT_UPGRAGE_ING_STR   = "다운로드 진행중...";
	
	public void notify(int result,int percentage,String text );

}
