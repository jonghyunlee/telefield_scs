package com.telefield.smartcaresystem.ui.firmware;

public interface IFirmwareUpdateCallback {
	
	public static final int RESULT_COMPL = 1;
	public static final int RESULT_OK = 0;
	public static final int RESULT_FAIL = -1;
	public static final int RESULT_STOP = -2;
	
	public static final String RESULT_UPGRAGE_FAIL_STR = "본체 업그레이드 진행을 실패하였습니다.";
	public static final String RESULT_UPGRAGE_OK_STR   = "버전 업그레이드에 성공하였습니다.";
	public static final String RESULT_UPGRAGE_ING_STR   = "업그레이드 진행중...";
	
	public void notify(int result,int percentage,String text );

}
