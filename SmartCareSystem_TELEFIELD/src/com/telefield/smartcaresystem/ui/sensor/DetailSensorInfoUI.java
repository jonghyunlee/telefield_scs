package com.telefield.smartcaresystem.ui.sensor;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.database.Sensor;
import com.telefield.smartcaresystem.message.SensorID;
import com.telefield.smartcaresystem.ui.LauncherUI;
import com.telefield.smartcaresystem.ui.UIConstant;
import com.telefield.smartcaresystem.util.ByteArrayToHex;

public class DetailSensorInfoUI extends Activity {
	Button backKeyButton, homeKeyButton;
	Context mContext;
	TextView macInfo, batteryInfo, zigbeeBatteryInfo, rssiInfo, communicationStatusInfo, registerTimeInfo, lastCommunicationTimeInfo, keepAliveTimeInfo, sensorVersionInfo;
	
	public static final int UPDATE_UI = 1;	//Handler message
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.detail_sensor_info);
		
		mContext = this;
		
		Intent intent = getIntent();
		
		long getSensorId = 0;
		
		TextView titleTextView = (TextView) findViewById(R.id.titleTextView);
		TextView middleTitleTextView = (TextView) findViewById(R.id.middleTitleTextView);
		macInfo = (TextView) findViewById(R.id.macInfo);
		batteryInfo = (TextView) findViewById(R.id.batteryInfo);
		zigbeeBatteryInfo = (TextView) findViewById(R.id.zigbeeBatteryInfo);
		rssiInfo = (TextView) findViewById(R.id.rssiInfo);
		communicationStatusInfo = (TextView) findViewById(R.id.communicationStatusInfo);
		registerTimeInfo = (TextView) findViewById(R.id.registerTimeInfo);
		lastCommunicationTimeInfo = (TextView) findViewById(R.id.lastCommunicationTimeInfo);
		keepAliveTimeInfo = (TextView) findViewById(R.id.keepAliveTimeInfo);
		sensorVersionInfo = (TextView) findViewById(R.id.sensorVersionInfo);
		
		titleTextView.setText(R.string.detail_sensor_info);
		
		if(intent != null) {
			middleTitleTextView.setText(intent.getStringExtra(UIConstant.TITLE));
			macInfo.setText(UIConstant.SPACE_CHARACTER + intent.getStringExtra(UIConstant.SENSOR_MAC));
			if(intent.getExtras().get(UIConstant.SENSOR_BATTERY).equals("정보없음")) {	//'정보없음'은 String이고 Battery는 Integer이므로 Object를 가져옴
				batteryInfo.setText(UIConstant.SPACE_CHARACTER + intent.getExtras().get(UIConstant.SENSOR_BATTERY));
			}else {
				batteryInfo.setText(UIConstant.SPACE_CHARACTER + intent.getExtras().get(UIConstant.SENSOR_BATTERY) + "%");
			}
			zigbeeBatteryInfo.setText(UIConstant.SPACE_CHARACTER + intent.getExtras().get(UIConstant.SENSOR_ZIGBEE_BATTERY));
			rssiInfo.setText(UIConstant.SPACE_CHARACTER + intent.getExtras().get(UIConstant.SENSOR_RSSI));
			communicationStatusInfo.setText(UIConstant.SPACE_CHARACTER + intent.getStringExtra(UIConstant.SENSOR_COMMUNICATION_STATUS));
			registerTimeInfo.setText(UIConstant.SPACE_CHARACTER + intent.getStringExtra(UIConstant.SENSOR_REGISTER_TIME));
			lastCommunicationTimeInfo.setText(UIConstant.SPACE_CHARACTER + intent.getStringExtra(UIConstant.SENSOR_LAST_COMMUNICATION_TIME));
			keepAliveTimeInfo.setText(UIConstant.SPACE_CHARACTER + intent.getStringExtra(UIConstant.SENSOR_KEEP_ALIVE_TIME));
			getSensorId = (long) (intent.getExtras().get(UIConstant.SENSOR_ID_KEY));
			
			if(intent.getStringExtra(UIConstant.SENSOR_VERSION).equals("0") ||intent.getStringExtra(UIConstant.SENSOR_VERSION) == null){
				sensorVersionInfo.setText("정보 없음");
			}else{
				sensorVersionInfo.setText(UIConstant.SPACE_CHARACTER + intent.getStringExtra(UIConstant.SENSOR_VERSION));
			}
			// Jong 통신상태가 불량이면 zigbee rssi battery 정보를 표시하지 않는다. <--
			if((UIConstant.SPACE_CHARACTER + intent.getStringExtra(UIConstant.SENSOR_COMMUNICATION_STATUS)).equals(UIConstant.SPACE_CHARACTER + "불량")){
				batteryInfo.setText("정보없음");
				zigbeeBatteryInfo.setText("정보없음");
				rssiInfo.setText("정보없음");
			}//-->
		
		}
		
		SmartCareSystemApplication.getInstance().getGlobalMsgFunc().setHandler(handler);
		SmartCareSystemApplication.getInstance().getGlobalMsgFunc().setGetSensorID(getSensorId);
		
		backKeyButton = (Button) findViewById(R.id.backButton);
		backKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		homeKeyButton = (Button) findViewById(R.id.homeButton);
		homeKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mContext, LauncherUI.class);
				//intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});
	}
	
	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			int message = msg.what;
			
			switch (message) {
				case UPDATE_UI:
					Sensor getSensor = (Sensor) msg.obj;
					
					if (getSensor != null) {
						if(getSensor.getMAC() == null) {
							macInfo.setText(UIConstant.SPACE_CHARACTER + "정보없음");
						}
						else {
							macInfo.setText(UIConstant.SPACE_CHARACTER + getSensor.getMAC());
						}
						
						if (getSensor.getSensor_batt() == null) {
							batteryInfo.setText(UIConstant.SPACE_CHARACTER + "정보없음");
						} else {
							batteryInfo.setText(UIConstant.SPACE_CHARACTER + getSensor.getSensor_batt() + "%");
						}
						
						if(getSensor.getZigbee_batt() == null) {
							zigbeeBatteryInfo.setText(UIConstant.SPACE_CHARACTER + "정보없음");
						}
						else if(getSensor.getZigbee_batt() == 0) {
							zigbeeBatteryInfo.setText(UIConstant.SPACE_CHARACTER + "양호");
						}
						else {
							zigbeeBatteryInfo.setText(UIConstant.SPACE_CHARACTER + "부족");
						}
						
						if (getSensor.getRssi() == null) {
							rssiInfo.setText(UIConstant.SPACE_CHARACTER + "정보없음");
						} else {
							rssiInfo.setText(UIConstant.SPACE_CHARACTER + getSensor.getRssi());
						}
						// Jong 통신상태가 불량이면 zigbee rssi battery 정보를 표시하지 않는다. <--
						if (getSensor.getComm_status() == null || getSensor.getComm_status() == 0) {
							communicationStatusInfo.setText(UIConstant.SPACE_CHARACTER + "불량");
							rssiInfo.setText(UIConstant.SPACE_CHARACTER + "정보없음");
							zigbeeBatteryInfo.setText(UIConstant.SPACE_CHARACTER + "정보없음");
							batteryInfo.setText(UIConstant.SPACE_CHARACTER + "정보없음");
						} else {
							communicationStatusInfo.setText(UIConstant.SPACE_CHARACTER + "정상");
						}// -->
						if (getSensor.getSensor_batt() == null) {
							batteryInfo.setText(UIConstant.SPACE_CHARACTER + "정보없음");
						} 
						else {
							batteryInfo.setText(UIConstant.SPACE_CHARACTER + getSensor.getSensor_batt() + "%");
						}
						if(getSensor.getRegister_time() == null) {
							registerTimeInfo.setText(UIConstant.SPACE_CHARACTER + "정보없음");
						} 
						else {
							registerTimeInfo.setText(UIConstant.SPACE_CHARACTER + getStringValueDate(getSensor.getRegister_time()));
						}
						
						if(getSensor.getLast_communication_time() == null) {
							lastCommunicationTimeInfo.setText(UIConstant.SPACE_CHARACTER + "정보없음");
						}
						else {
							lastCommunicationTimeInfo.setText(UIConstant.SPACE_CHARACTER + getStringValueDate(getSensor.getLast_communication_time()));
						}
						
						if (getSensor.getKeepAliveTime() == null) {
							keepAliveTimeInfo.setText(UIConstant.SPACE_CHARACTER + "정보없음");
						} else {
							keepAliveTimeInfo.setText(UIConstant.SPACE_CHARACTER + getSensor.getKeepAliveTime() + "분");
						}
						if (getSensor.getVersion() == null || getSensor.getVersion() == 0) {
							sensorVersionInfo.setText("정보없음");
						} else {
							sensorVersionInfo.setText(UIConstant.SPACE_CHARACTER + 
									Sensor.getStringVersion(String.valueOf(Integer.toHexString(getSensor.getVersion().intValue()))));
						}
					}
					break;
			}
		}
	};
	
	private String getStringValueDate(Date data) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		
		return format.format(data);
	}
}
