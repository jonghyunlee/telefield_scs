package com.telefield.smartcaresystem.ui.sensor;

import java.util.ArrayList;

import android.app.FragmentManager;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.bluetooth.DeviceScan;
import com.telefield.smartcaresystem.ui.common.ProgressDialogUI;

public class PulseSensorAdapterUI extends BaseAdapter {
	private ArrayList<BluetoothDevice> availableDevice;
	private ArrayList<BluetoothDevice> pairedDevice;
	FragmentManager fragmentManager;
	Context mContext;
	int mode;
	
	public final static int MODE_PAIRED_BLOOD_PRESSURE_MACHINE = 1;
	public final static int MODE_AVAILABLE_BLOOD_PRESSURE_MACHINE = 2;
	
	public PulseSensorAdapterUI(Context mContext, int mode, FragmentManager fragmentManager) {
		this.mContext = mContext;
		this.mode = mode;
		this.fragmentManager = fragmentManager;
		
		if(mode == MODE_PAIRED_BLOOD_PRESSURE_MACHINE)
			pairedDevice = new ArrayList<BluetoothDevice>();
		else
			availableDevice = new ArrayList<BluetoothDevice>();
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if(mode == MODE_PAIRED_BLOOD_PRESSURE_MACHINE)
			return pairedDevice.size();
		else
			return availableDevice.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		if(mode == MODE_PAIRED_BLOOD_PRESSURE_MACHINE)
			return pairedDevice.get(position);
		else
			return availableDevice.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;
		
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		if(view == null)
			view = inflater.inflate(R.layout.child_system_sub_menu, parent, false);
		
		TextView menuTitle = (TextView) view.findViewById(R.id.menuTitle);
		
		TextView info = (TextView) view.findViewById(R.id.info);
		info.setVisibility(View.GONE);
		
		ImageView image = (ImageView) view.findViewById(R.id.menuItemImage);
		image.setVisibility(View.GONE);
		ImageView expandImage = (ImageView) view.findViewById(R.id.openSubMenuImageView);
		
		if(mode == MODE_PAIRED_BLOOD_PRESSURE_MACHINE) {
			expandImage.setVisibility(View.VISIBLE);
			expandImage.setBackgroundResource(R.drawable.icon_delete_selector);
			expandImage.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					PulseSensorUI.setRemovePosition(position);
					
					ProgressDialogUI progressDialog = ProgressDialogUI.createInstance();
					progressDialog.setMode(ProgressDialogUI.MODE_UNPAIRING_BLOOD_PRESSURE);
					progressDialog.setTitle(pairedDevice.get(position).getName());
					progressDialog.show(fragmentManager, "Dialog");
					
					DeviceScan dev = new DeviceScan();
					dev.unPairDevice(pairedDevice.get(position));
				}
			});
			menuTitle.setText(pairedDevice.get(position).getName());
		}
		else {
			expandImage.setVisibility(View.GONE);
			menuTitle.setText(availableDevice.get(position).getName());
		}
		
		return view;
	}
	
	public void add(BluetoothDevice device) {
		if(mode == MODE_PAIRED_BLOOD_PRESSURE_MACHINE)
			this.pairedDevice.add(device);
		else
			this.availableDevice.add(device);
	}
	
	public void remove(int index) {
		if(mode == MODE_PAIRED_BLOOD_PRESSURE_MACHINE)
			this.pairedDevice.remove(index);
		else
			this.availableDevice.remove(index);
	}
	
	public void clear() {
		if(mode == MODE_PAIRED_BLOOD_PRESSURE_MACHINE)
			this.pairedDevice.clear();
		else
			this.availableDevice.clear();
	}
}
