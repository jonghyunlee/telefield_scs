package com.telefield.smartcaresystem.ui.sensor;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.bluetooth.DeviceScan;
import com.telefield.smartcaresystem.ui.LauncherUI;
import com.telefield.smartcaresystem.ui.UIConstant;
import com.telefield.smartcaresystem.ui.common.ConfirmDialogUI;
import com.telefield.smartcaresystem.util.LogMgr;

public class SelectSensorUI extends Activity {
	Button seeRegisterConditionButton, backKeyButton, homeKeyButton;
	ListView sensorListView;
	TextView titelTextView;
	SensorAdapterUI mAdapter;
	Context mContext;
	FragmentManager currentFragmentManager;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.select_sensor_ui);
		
		mContext = this;
		currentFragmentManager = getFragmentManager();
		
		titelTextView = (TextView) findViewById(R.id.titleTextView);
		titelTextView.setText(R.string.register_sensor);
		
		final String[] sensorNameArray = getResources().getStringArray(R.array.sensor_name);
		
		sensorListView = (ListView) findViewById(R.id.sensorListView);
		mAdapter = new SensorAdapterUI(this, sensorNameArray, UIConstant.MODE_REGISTER_SENSOR, currentFragmentManager);
		sensorListView.setAdapter(mAdapter);
		
		sensorListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				if(position == 6) {
					DeviceScan dev = new DeviceScan();
					
					if(dev.isTurnOn()) {
						Intent intent = new Intent(mContext, PulseSensorUI.class);
						intent.putExtra(UIConstant.SENSOR_TYPE, sensorNameArray[position]);	
						intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(intent);
					}
					else {
						ConfirmDialogUI dialog = ConfirmDialogUI.newInstance(ConfirmDialogUI.MODE_BLUETOOTH_ON);
						dialog.setTitle(sensorNameArray[position]);
						dialog.show(currentFragmentManager, "Dialog");
					}
				}
				else { 
					Intent intent = new Intent(mContext, SeeSensorGroupUI.class);
					intent.putExtra(UIConstant.SENSOR_TYPE, sensorNameArray[position]);
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(intent);
				}
			}
		});
		
		seeRegisterConditionButton = (Button) findViewById(R.id.seeRegisterConditionButton);
		seeRegisterConditionButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mContext, SeeRegisterConditionUI.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});
		
		backKeyButton = (Button) findViewById(R.id.backButton);
		backKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		homeKeyButton = (Button) findViewById(R.id.homeButton);
		homeKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mContext, LauncherUI.class);
				//intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});
	}
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		
		/*if(mAdapter != null )				
			SmartCareSystemApplication.getInstance().getSensorRegisterFunc().removeEventListener(mAdapter.getHandler());*/
		
		LogMgr.d(LogMgr.TAG , " SelectSensorUI onStop()");
	}
}
