package com.telefield.smartcaresystem.ui.sensor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParserException;

import android.app.FragmentManager;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.XmlResourceParser;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.bluetooth.DeviceScan;
import com.telefield.smartcaresystem.database.Sensor;
import com.telefield.smartcaresystem.message.SensorID;
import com.telefield.smartcaresystem.ui.UIConstant;
import com.telefield.smartcaresystem.ui.common.ConfirmDialogUI;
import com.telefield.smartcaresystem.ui.common.IDialogChangeListener;
import com.telefield.smartcaresystem.util.LogMgr;

public class SensorAdapterUI extends BaseAdapter implements IDialogChangeListener {
	private boolean[] isCheckedList;
	
	private Context mContext;
	private ArrayList<String> mListData = new ArrayList<String>();
	private ArrayList<Sensor> mSensor;
	private int mode;
	private FragmentManager getFragmentManager;
	private int completeTestIndex;
	private String[] testResultArray;
	
	public SensorAdapterUI(Context mContext, String[] data, int mode, FragmentManager getFragmentManager) {
		this.mContext = mContext;
		this.mode = mode;
		
		if(mode == UIConstant.MODE_REGISTER_SENSOR) {
			this.getFragmentManager = getFragmentManager;
		}
		
		for(int i=0;i<data.length;i++) {
			mListData.add(data[i]);
		}
		
		if(mode == UIConstant.MODE_TEST_SENSOR || mode == UIConstant.MODE_ALL_CHECK_TEST_LIST) {
			isCheckedList = new boolean[mListData.size()];
		}
	}
	
	public SensorAdapterUI(Context mContext, Object obj, int mode, FragmentManager getFragmentManager) {
		this.mContext = mContext;
		this.mode = mode;
		
		if(mode == UIConstant.MODE_DELETE_SENSOR || mode == UIConstant.MODE_SEE_REGISTER_CONDITION || mode == UIConstant.MODE_SEE_SENSOR_FOR_SUB_TEST) {
			mSensor = (ArrayList<Sensor>) obj;
			if(mode == UIConstant.MODE_DELETE_SENSOR)
				this.getFragmentManager = getFragmentManager;
		}
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if(mode == UIConstant.MODE_SEE_REGISTER_CONDITION ){
			return mSensor.size();
		}
		if(mode == UIConstant.MODE_DELETE_SENSOR || /*mode == UIConstant.MODE_SEE_REGISTER_CONDITION || */mode == UIConstant.MODE_SEE_SENSOR_FOR_SUB_TEST)
			return mSensor.size();
		else
			return mListData.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		if(mode == UIConstant.MODE_DELETE_SENSOR || mode == UIConstant.MODE_SEE_REGISTER_CONDITION || mode == UIConstant.MODE_SEE_SENSOR_FOR_SUB_TEST)
			return mSensor.get(position);
		else
			return mListData.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;
		
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		if(mode ==  UIConstant.MODE_REGISTER_SENSOR) {
			if(view == null)
				view = inflater.inflate(R.layout.child_list_item_register_sensor, parent, false);
			
			TextView sensorName = (TextView) view.findViewById(R.id.sensorName);
			sensorName.setText(mListData.get(position));
			
			ImageView image = (ImageView) view.findViewById(R.id.typeImageView);
			if(position == 0) {
				image.setBackgroundResource(R.drawable.icon_sensor01_nor);
			}
			else if(position == 1) {
				image.setBackgroundResource(R.drawable.icon_sensor02_nor);
			}
			else if(position == 2) {
				image.setBackgroundResource(R.drawable.icon_sensor03_nor);
			}
			else if(position == 3) {
				image.setBackgroundResource(R.drawable.icon_sensor04_nor);
			}
			else if(position == 4) {
				image.setBackgroundResource(R.drawable.icon_sensor05_nor);
			}
			else if(position == 5) {
				image.setBackgroundResource(R.drawable.icon_sensor07_nor);
			}
			else if(position == 6) {
				image.setBackgroundResource(R.drawable.icon_sensor06_nor);
			}
			
			Button register = (Button) view.findViewById(R.id.register_and_delete);
			register.setBackgroundResource(R.drawable.icon_register_selector);
			register.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(position == 0) {	//화재 센서
						ConfirmDialogUI dialog = ConfirmDialogUI.newInstance(ConfirmDialogUI.MODE_REGISTER_FIRE_SENSOR);
						dialog.setTitle(mListData.get(position));
						dialog.show(getFragmentManager, "Dialog");
					}
					else if (position == 1) {
						ConfirmDialogUI dialog = ConfirmDialogUI.newInstance(ConfirmDialogUI.MODE_REGISTER_GAS_SENSOR);
						dialog.setTitle(mListData.get(position));
						dialog.show(getFragmentManager, "Dialog");
					}
					else if (position == 2) {
						ConfirmDialogUI dialog = ConfirmDialogUI.newInstance(ConfirmDialogUI.MODE_REGISTER_ACTIVITY_SENSOR);
						dialog.setTitle(mListData.get(position));
						dialog.show(getFragmentManager, "Dialog");
					}
					else if (position == 3) {
						ConfirmDialogUI dialog = ConfirmDialogUI.newInstance(ConfirmDialogUI.MODE_REGISTER_DOOR_SENSOR);
						dialog.setTitle(mListData.get(position));
						dialog.show(getFragmentManager, "Dialog");
					}
					else if (position == 4) {
						ConfirmDialogUI dialog = ConfirmDialogUI.newInstance(ConfirmDialogUI.MODE_REGISTER_EMERGENCY);
						dialog.setTitle(mListData.get(position));
						dialog.show(getFragmentManager, "Dialog");
					}
					else if (position == 5) {
						ConfirmDialogUI dialog = ConfirmDialogUI.newInstance(ConfirmDialogUI.MODE_REGISTER_GAS_BREAKER);
						dialog.setTitle(mListData.get(position));
						dialog.show(getFragmentManager, "Dialog");
					}
				}
			});
			
			if(position == 6)
				register.setVisibility(View.GONE);
			else
				register.setVisibility(View.VISIBLE);	//스크롤을 맨 아래로 이동 후 맨위로 이동 시 가스센서의 등록 버튼이 사라지는 문제 수정
		}
		else if(mode == UIConstant.MODE_DELETE_SENSOR) {
			if(view == null)
				view = inflater.inflate(R.layout.child_list_item_delete_sensor, parent, false);
			
			ImageView image = (ImageView) view.findViewById(R.id.typeImageView);
			image.setVisibility(View.GONE);
			
			TextView sensorName = (TextView) view.findViewById(R.id.sensorName);
			sensorName.setText(mSensor.get(position).getTypeStr() + mSensor.get(position).getSensorIndex());
			
			TextView macInfo = (TextView) view.findViewById(R.id.macInfo);
			macInfo.setText(UIConstant.SPACE_CHARACTER + mSensor.get(position).getMAC());
			
			TextView batteryInfo = (TextView) view.findViewById(R.id.batteryInfo);
			
			Integer sensor_batt = mSensor.get(position).getSensor_batt();
			batteryInfo.setText(UIConstant.SPACE_CHARACTER + ( (sensor_batt == null || sensor_batt== 0)  ? "정보없음" : sensor_batt + "%"));
			
			ImageView sensorBatteryImage = (ImageView) view.findViewById(R.id.sensorBatteryImage);
			setBatteryImage(sensorBatteryImage, sensor_batt);
			
			TextView zigbeeBatteryInfo = (TextView) view.findViewById(R.id.zigbeeBatteryInfo);
			
			Integer zigbee_batt = mSensor.get(position).getZigbee_batt();
			if(zigbee_batt == null) {
				zigbeeBatteryInfo.setText("정보없음");
			}
			else if(zigbee_batt== 0) {
				zigbeeBatteryInfo.setText("양호");
			}
			else {
				zigbeeBatteryInfo.setText("부족");
			}
			
			LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
			param.gravity = Gravity.CENTER_VERTICAL;
			param.rightMargin = 0;
			
			sensorBatteryImage.setLayoutParams(param);
			
			Integer sensor_rssi = mSensor.get(position).getRssi();
			TextView rssiInfo = (TextView) view.findViewById(R.id.rssiInfo);
			rssiInfo.setText(UIConstant.SPACE_CHARACTER + ((sensor_rssi == null || sensor_rssi == 0) ? "정보없음" : sensor_rssi));
			
			ImageView sensorStatusImage = (ImageView) view.findViewById(R.id.sensorOnOffImage);
			setStatusImage(sensorStatusImage, mSensor.get(position).getComm_status());
			
			Button register = (Button) view.findViewById(R.id.register_and_delete);
			register.setBackgroundResource(R.drawable.icon_delete_selector);
			register.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					ConfirmDialogUI dialog = ConfirmDialogUI.newInstance(ConfirmDialogUI.MODE_DELETE_SENSOR);
					dialog.setTitle(mSensor.get(position).getTypeStr());
					dialog.setSensorNum(mSensor.get(position).getSensorIndex());
					dialog.setDeleteSensorID(mSensor.get(position).getSensor_id().intValue());
					dialog.show(getFragmentManager, "Dialog");
				}
			});
			
			//Jong 통신상태가 불량이면 mac을 제외한 정보는 표시하지 않는다. -->
			if(mSensor.get(position).getComm_status() != 1){
				zigbeeBatteryInfo.setText("정보없음");
				setBatteryImage(sensorBatteryImage, 0);
				batteryInfo.setText(UIConstant.SPACE_CHARACTER + "정보없음");
				rssiInfo.setText(UIConstant.SPACE_CHARACTER + "정보없음");
				setStatusImage(sensorStatusImage, 0);
			}else{
				rssiInfo.setText(UIConstant.SPACE_CHARACTER + ((sensor_rssi == null || sensor_rssi == 0) ? "정보없음" : sensor_rssi));
				setStatusImage(sensorStatusImage, mSensor.get(position).getComm_status());
				if(zigbee_batt == null) {
					zigbeeBatteryInfo.setText("정보없음");
				}
				else if(zigbee_batt== 0) {
					zigbeeBatteryInfo.setText("양호");
				}
				else {
					zigbeeBatteryInfo.setText("부족");
				}
				setBatteryImage(sensorBatteryImage, sensor_batt);
				batteryInfo.setText(UIConstant.SPACE_CHARACTER + ((sensor_batt == null || sensor_batt== 0)  ? "정보없음" : sensor_batt + "%"));
			}// <--
			
			SmartCareSystemApplication.getInstance().getGlobalMsgFunc().setDialogChangeListener(this);
		}
		else if(mode == UIConstant.MODE_SEE_REGISTER_CONDITION) {
			
			if(SensorID.CODI_SENSOR_TYPE.getSensorType() == mSensor.get(position).getType().intValue()){
			}
			else{
				if(view == null)
					view = inflater.inflate(R.layout.child_list_item_delete_sensor, parent, false);

				ImageView image = (ImageView) view.findViewById(R.id.typeImageView);
				TextView sensorName = (TextView) view.findViewById(R.id.sensorName);
				TextView macInfo = (TextView) view.findViewById(R.id.macInfo);
				TextView batteryInfo = (TextView) view.findViewById(R.id.batteryInfo);
				TextView zigbeeBatteryInfo = (TextView) view.findViewById(R.id.zigbeeBatteryInfo);
				TextView rssiInfo = (TextView) view.findViewById(R.id.rssiInfo);
				TextView mac = (TextView) view.findViewById(R.id.mac);
				TextView battery = (TextView) view.findViewById(R.id.battery);
				TextView rssi = (TextView) view.findViewById(R.id.rssi);
				TextView zigbee = (TextView) view.findViewById(R.id.zigbeeBattery);
				
				Button btn = (Button) view.findViewById(R.id.register_and_delete);
				btn.setVisibility(View.GONE);
				
				if(mSensor.size() > 0) {
					if(mSensor.get(position).getComm_status() == 1000 && mSensor.get(position).getSensor_batt() == 1000) {
						sensorName.setText(mContext.getString(R.string.blood_pressure_machine));
						image.setBackgroundResource(R.drawable.icon_sensor06_nor);
						macInfo.setText(UIConstant.SPACE_CHARACTER + mSensor.get(position).getMAC());
						
						batteryInfo.setText(UIConstant.SPACE_CHARACTER + mContext.getString(R.string.good));
						
						ImageView sensorBatteryImage = (ImageView) view.findViewById(R.id.sensorBatteryImage);
						setBatteryImage(sensorBatteryImage, 100);
						
						rssi.setVisibility(View.GONE);
						rssiInfo.setVisibility(View.GONE);
						
						zigbee.setVisibility(View.GONE);
						zigbeeBatteryInfo.setVisibility(View.GONE);
						
						ImageView sensorStatusImage = (ImageView) view.findViewById(R.id.sensorOnOffImage);
						setStatusImage(sensorStatusImage, 1);
						
					}
					else {
						String sensor_name = mSensor.get(position).getTypeStr() + mSensor.get(position).getSensorIndex();
						
						if(sensor_name.startsWith(UIConstant.FIRE_SENSOR)) {
							image.setBackgroundResource(R.drawable.icon_sensor01_nor);
						}
						else if(sensor_name.startsWith(UIConstant.GAS_SENSOR)) {
							image.setBackgroundResource(R.drawable.icon_sensor02_nor);
						}
						else if(sensor_name.startsWith(UIConstant.ACTIVITY_SENSOR)) {
							image.setBackgroundResource(R.drawable.icon_sensor03_nor);
						}
						else if(sensor_name.startsWith(UIConstant.DOOR_SENSOR)) {
							image.setBackgroundResource(R.drawable.icon_sensor04_nor);
						}
						else if(sensor_name.startsWith(UIConstant.EMERGENCY_SENSOR)) {
							image.setBackgroundResource(R.drawable.icon_sensor05_nor);
						}
						else if(sensor_name.startsWith(UIConstant.GAS_BREAKER_SENSOR)) {
							image.setBackgroundResource(R.drawable.icon_sensor07_nor);
						}
						
						sensorName.setText(sensor_name);
						macInfo.setText(UIConstant.SPACE_CHARACTER + mSensor.get(position).getMAC());
						
						Integer sensor_batt = mSensor.get(position).getSensor_batt();
						ImageView sensorBatteryImage = (ImageView) view.findViewById(R.id.sensorBatteryImage);		
						Integer zigbee_batt = mSensor.get(position).getZigbee_batt();	
						Integer sensor_rssi = mSensor.get(position).getRssi();
						ImageView sensorStatusImage = (ImageView) view.findViewById(R.id.sensorOnOffImage);
						
						rssi.setVisibility(View.VISIBLE);
						rssiInfo.setVisibility(View.VISIBLE);
						
						zigbee.setVisibility(View.VISIBLE);
						zigbeeBatteryInfo.setVisibility(View.VISIBLE);
						
						
						
						//Jong 통신상태가 불량이면 mac을 제외한 정보는 표시하지 않는다. -->
						if(mSensor.get(position).getComm_status() != 1){
							zigbeeBatteryInfo.setText("정보없음");
							setBatteryImage(sensorBatteryImage, 0);
							batteryInfo.setText(UIConstant.SPACE_CHARACTER + "정보없음");
							rssiInfo.setText(UIConstant.SPACE_CHARACTER + "정보없음");
							setStatusImage(sensorStatusImage, 0);
						}else{
							rssiInfo.setText(UIConstant.SPACE_CHARACTER + ((sensor_rssi == null || sensor_rssi == 0) ? "정보없음" : sensor_rssi));
							setStatusImage(sensorStatusImage, mSensor.get(position).getComm_status());
							if(zigbee_batt == null) {
								zigbeeBatteryInfo.setText("정보없음");
							}
							else if(zigbee_batt== 0) {
								zigbeeBatteryInfo.setText("양호");
							}
							else {
								zigbeeBatteryInfo.setText("부족");
							}
							setBatteryImage(sensorBatteryImage, sensor_batt);
							batteryInfo.setText(UIConstant.SPACE_CHARACTER + ((sensor_batt == null || sensor_batt== 0)  ? "정보없음" : sensor_batt + "%"));
						}// <--
						SmartCareSystemApplication.getInstance().getGlobalMsgFunc().setDialogChangeListener(this);
					}
				}
				else {
					mac.setText("");
					battery.setText("");
					rssi.setText("");
				}
			}
		}
		else if(mode == UIConstant.MODE_SEE_SENSOR_FOR_TEST) {
			if(view == null)
				view = inflater.inflate(R.layout.child_list_item_register_sensor, parent, false);
			
			TextView sensorName = (TextView) view.findViewById(R.id.sensorName);
			sensorName.setText(mListData.get(position));
			
			ImageView image = (ImageView) view.findViewById(R.id.typeImageView);
			if(position == 0) {
				image.setBackgroundResource(R.drawable.icon_sensor01_nor);
			}
			else if(position == 1) {
				image.setBackgroundResource(R.drawable.icon_sensor02_nor);
			}
			else if(position == 2) {
				image.setBackgroundResource(R.drawable.icon_sensor03_nor);
			}
			else if(position == 3) {
				image.setBackgroundResource(R.drawable.icon_sensor04_nor);
			}
			else if(position == 4) {
				image.setBackgroundResource(R.drawable.icon_sensor05_nor);
			}
			else if(position == 5) {
				image.setBackgroundResource(R.drawable.icon_sensor07_nor);
			}
			
			Button btn = (Button) view.findViewById(R.id.register_and_delete);
			btn.setVisibility(View.GONE);
		}
		else if(mode == UIConstant.MODE_SEE_SENSOR_FOR_SUB_TEST) {
			if(view == null)
				view = inflater.inflate(R.layout.child_list_item_register_sensor, parent, false);
			
			TextView sensorName = (TextView) view.findViewById(R.id.sensorName);
			sensorName.setText(mSensor.get(position).getTypeStr() + mSensor.get(position).getSensorIndex());
			LogMgr.d(LogMgr.TAG,"mSensor.get(position).getTypeStr() = "+mSensor.get(position).getTypeStr());
			Button btn = (Button) view.findViewById(R.id.register_and_delete);
			btn.setVisibility(View.GONE);
			
			ImageView image = (ImageView) view.findViewById(R.id.typeImageView);
			image.setVisibility(View.GONE);
		}
		else if(mode == UIConstant.MODE_TEST_SENSOR) {
			if(view == null)
				view = inflater.inflate(R.layout.child_list_item_test_sensor, parent, false);
			
			TextView itemText = (TextView) view.findViewById(R.id.itemText);
			itemText.setText(mListData.get(position));
			
			TextView subItemText = (TextView) view.findViewById(R.id.subItemText);
			subItemText.setVisibility(View.GONE);
			
			CheckBox checkBox = (CheckBox) view.findViewById(R.id.testCheckBox);
			if(isCheckedList[position]) {
				checkBox.setChecked(true);
			}
			else {
				checkBox.setChecked(false);
			}
			checkBox.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(isCheckedList[position]) {
						isCheckedList[position] = false;
					}
					else {
						isCheckedList[position] = true;
					}
				}
			});
		}
		else if(mode == UIConstant.MODE_REAL_TEST_SENSOR) {
			if(view == null)
				view = inflater.inflate(R.layout.child_list_item_test_sensor_no_checkbox, parent, false);
			
			TextView itemText = (TextView) view.findViewById(R.id.itemText);
			itemText.setText((position + 1) + ". " + mListData.get(position));
			
			Button ignoreButton = (Button) view.findViewById(R.id.ignoreButton);
			
			if(position == 0) {
				XmlResourceParser runParser = mContext.getResources().getXml(R.color.diagnosis_run_color);
				ColorStateList runColor = null;
				try {
					runColor = ColorStateList.createFromXml(mContext.getResources(), runParser);
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if(runColor != null)
					itemText.setTextColor(runColor);
				ignoreButton.setVisibility(View.VISIBLE);
				
			}
			else {
				XmlResourceParser waitParser = mContext.getResources().getXml(R.color.diagnosis_wait_color);
			    ColorStateList waitColor = null;
			    try {
			    	waitColor = ColorStateList.createFromXml(mContext.getResources(), waitParser);
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if(waitColor != null)
					itemText.setTextColor(waitColor);
				ignoreButton.setVisibility(View.INVISIBLE);
			}
			
			TextView subItemText = (TextView) view.findViewById(R.id.subItemText);
			subItemText.setVisibility(View.GONE);
			
			TextView testResult = (TextView) view.findViewById(R.id.testResult);
			testResult.setText(R.string.testing);
			
			ignoreButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					
					LogMgr.d(LogMgr.TAG,"pos = "+position);
					if(SmartCareSystemApplication.getInstance().getDiagnosticManager().isDiagnosticTesting())
						SmartCareSystemApplication.getInstance().getDiagnosticManager().stopCurrentTest();
				}
			});
		}
		else if(mode == UIConstant.MODE_ALL_CHECK_TEST_LIST) {
			if(view == null)
				view = inflater.inflate(R.layout.child_list_item_test_sensor, parent, false);
			
			TextView itemText = (TextView) view.findViewById(R.id.itemText);
			itemText.setText(mListData.get(position));
			
			TextView subItemText = (TextView) view.findViewById(R.id.subItemText);
			subItemText.setVisibility(View.GONE);
			
			CheckBox checkBox = (CheckBox) view.findViewById(R.id.testCheckBox);
			checkBox.setChecked(isCheckedList[position]);
			
			checkBox.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(isCheckedList[position]) {
						isCheckedList[position] = false;
					}
					else {
						isCheckedList[position] = true;
					}
				}
			});
		}
		else if(mode == UIConstant.MODE_ONGOING_TEST) {
			if(view == null)
				view = inflater.inflate(R.layout.child_list_item_test_sensor_no_checkbox, parent, false);
			
			TextView itemText = (TextView) view.findViewById(R.id.itemText);
			itemText.setText((position + 1) + ". " + mListData.get(position));
			
			TextView subItemText = (TextView) view.findViewById(R.id.subItemText);
			subItemText.setVisibility(View.GONE);
			
			TextView testResult = (TextView) view.findViewById(R.id.testResult);
			
			Button ignoreButton = (Button) view.findViewById(R.id.ignoreButton);
			
			XmlResourceParser endParser = mContext.getResources().getXml(R.color.diagnosis_end_color);
			XmlResourceParser runParser = mContext.getResources().getXml(R.color.diagnosis_run_color);
			XmlResourceParser waitParser = mContext.getResources().getXml(R.color.diagnosis_wait_color);
		    ColorStateList endColor = null;
		    ColorStateList runColor = null;
		    ColorStateList waitColor = null;
		    try {
		    	endColor = ColorStateList.createFromXml(mContext.getResources(), endParser);
		    	runColor = ColorStateList.createFromXml(mContext.getResources(), runParser);
		    	waitColor = ColorStateList.createFromXml(mContext.getResources(), waitParser);
			} catch (XmlPullParserException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    
			if(!mContext.getString(R.string.testing).equals(testResultArray[position])) {
				ignoreButton.setVisibility(View.INVISIBLE);
				testResult.setText(testResultArray[position]);
				if(endColor != null) {
					itemText.setTextColor(endColor);
					testResult.setTextColor(endColor);
				}
			}
			else {
				if(position == completeTestIndex+1) {
					ignoreButton.setVisibility(View.VISIBLE);
					testResult.setText(testResultArray[position]);
					if(runColor != null)
						itemText.setTextColor(runColor);
					if(waitColor != null)
						testResult.setTextColor(waitColor);
				}
				else {
					ignoreButton.setVisibility(View.INVISIBLE);
					testResult.setText(testResultArray[position]);
					if(waitColor != null) {
						itemText.setTextColor(waitColor);
						testResult.setTextColor(waitColor);
					}
				}
			}
			
			ignoreButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					
					LogMgr.d(LogMgr.TAG,"pos = "+position);
					if(SmartCareSystemApplication.getInstance().getDiagnosticManager().isDiagnosticTesting())
						SmartCareSystemApplication.getInstance().getDiagnosticManager().stopCurrentTest();
				}
			});
		}
		else if(mode == UIConstant.MODE_STOP_TEST) {
			if(view == null)
				view = inflater.inflate(R.layout.child_list_item_test_sensor_no_checkbox, parent, false);
			
			TextView itemText = (TextView) view.findViewById(R.id.itemText);
			itemText.setText((position + 1) + ". " + mListData.get(position));
			
			TextView subItemText = (TextView) view.findViewById(R.id.subItemText);
			subItemText.setVisibility(View.GONE);
			
			TextView testResult = (TextView) view.findViewById(R.id.testResult);
			
			Button ignoreButton = (Button) view.findViewById(R.id.ignoreButton);
			ignoreButton.setVisibility(View.INVISIBLE);
			
			XmlResourceParser parser = mContext.getResources().getXml(R.color.diagnosis_end_color);
		    ColorStateList colors = null;
		    try {
				colors = ColorStateList.createFromXml(mContext.getResources(), parser);
			} catch (XmlPullParserException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    
		    
			if(!mContext.getString(R.string.testing).equals(testResultArray[position])) {
				testResult.setText(testResultArray[position]);
				
				if(colors != null) {
					itemText.setTextColor(colors);
					testResult.setTextColor(colors);
				}
			}
			else {
				testResult.setText(R.string.fail);
				
				if(colors != null) {
					itemText.setTextColor(colors);
					testResult.setTextColor(colors);
				}
			}
		}
		
		return view;
	}
	
	/*public Handler getHandler()
	{
		return mHandler;
	}*/
	
	public boolean[] getCheckedList() {
		return isCheckedList;
	}
	
	public void setAllCheck() {
		for(int i=0;i<isCheckedList.length;i++) {
			isCheckedList[i] = true;
		}
	}
	
	public void setCompleteTestIndex(int completeTestIndex) {
		this.completeTestIndex = completeTestIndex;
	}
	
	public void setTestResultArray(String[] testResultArray) {
		this.testResultArray = testResultArray;
	}
	
	public void updateList(List<Sensor> newList) {
		// TODO Auto-generated method stub
		if(mSensor != null) {
			mSensor.clear();
			mSensor.addAll(newList);
			this.notifyDataSetChanged();
		}
	}
	
	private void setBatteryImage(ImageView imageView, Integer batteryValue) {
		if(batteryValue == null || (batteryValue >= 0 && batteryValue <= 30)) {
			imageView.setImageResource(R.drawable.icon_sensor_battery_01);
		}
		else if(batteryValue > 30 && batteryValue <= 60) {
			imageView.setImageResource(R.drawable.icon_sensor_battery_02);
		}
		else if(batteryValue > 60 && batteryValue <= 90) {
			imageView.setImageResource(R.drawable.icon_sensor_battery_03);
		}
		else if(batteryValue > 90 && batteryValue <= 95) {
			imageView.setImageResource(R.drawable.icon_sensor_battery_04);
		}
		else if(batteryValue > 95 && batteryValue <= 100) {
			imageView.setImageResource(R.drawable.icon_sensor_battery_05);
		}
	}
	
	private void setStatusImage(ImageView imageView, Integer statusValue) {
		if(statusValue == 1) {
			imageView.setImageResource(R.drawable.icon_sensor_on);
		}
		else {
			imageView.setImageResource(R.drawable.icon_sensor_off);
		}
	}
	
	public void addBloodPressureMachine(Sensor addBloodPressureData) {
		/*
		 * 기존에 존재하는 Sensor ArrayList에 임의로 혈압계 정보를 추가하는 것이다.
		 * 따라서, 센서 등록 현황 외에는 쓰지도 않는다.
		 */
		if(mode == UIConstant.MODE_SEE_REGISTER_CONDITION) {
			if(mSensor != null) {
				mSensor.add(addBloodPressureData);
			}
		}
	}

	@Override
	public void notifyChangeListener() {
		// TODO Auto-generated method stub
		notifyDataSetChanged();
	}
}
