package com.telefield.smartcaresystem.ui.sensor;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import android.app.Activity;
import android.app.FragmentManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.bluetooth.DeviceScan;
import com.telefield.smartcaresystem.database.Sensor;
import com.telefield.smartcaresystem.m2m.GatewayIndex;
import com.telefield.smartcaresystem.m2m.ProtocolFrame;
import com.telefield.smartcaresystem.m2m.ProtocolManger;
import com.telefield.smartcaresystem.message.SensorID;
import com.telefield.smartcaresystem.ui.LauncherUI;
import com.telefield.smartcaresystem.ui.UIConstant;
import com.telefield.smartcaresystem.ui.common.ProgressDialogUI;
import com.telefield.smartcaresystem.util.LogMgr;

public class PulseSensorUI extends Activity {
	Button backKeyButton, homeKeyButton, searchButton;
	TextView emptyTextView, searchingTextView;
	LinearLayout showPairedLayout;
	Context mContext;
	ListView availablePulseMachineListView, pairedListView;
	ProgressBar searchingProgressBar;
	PulseSensorAdapterUI availableAdapter, pairedAdapter;
	ArrayList<BluetoothDevice> bluetoothDevice;
	ArrayList<BluetoothDevice> pairedDevice;
	DeviceScan dev;
	ProgressDialogUI progressDialog;
	FragmentManager fragmentManager;
	
	BluetoothDevice getBluetoothDevice;	//페어링 하고자 할 장비를 저장함
	int pairPosition;	//페어링을 위해 페어링 할 장비의 위치값 저장함
	static int removePosition;	//페어링 해제를 위해 페어링 해제 할 장비의 위치값 저장함 
	
	private BroadcastReceiver mReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			String action = intent.getAction();
			LogMgr.d(LogMgr.TAG, "PulseSensorUI.java:enclosing_method:// action : " + action);

			if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
				
			} else if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
				bluetoothDevice = new ArrayList<BluetoothDevice>();
				
				availableAdapter.clear();	//재검색 시 리스트를 클리어한다.
				
				searchingTextView.setVisibility(View.VISIBLE);
				searchingProgressBar.setVisibility(View.VISIBLE);
			} else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
				searchingTextView.setVisibility(View.INVISIBLE);
				searchingProgressBar.setVisibility(View.INVISIBLE);
				
				if(bluetoothDevice.size() <= 0) {	//Bluetooth 장치가 검색되지 않은 경우
					availablePulseMachineListView.setVisibility(View.GONE);
					emptyTextView.setVisibility(View.VISIBLE);
					emptyTextView.setText(R.string.no_searched_bluetooth_device);
				}
			} else if (BluetoothDevice.ACTION_FOUND.equals(action)) {
				BluetoothDevice device = (BluetoothDevice) intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
				
				if(device.getBluetoothClass().getDeviceClass() == BluetoothClass.Device.HEALTH_BLOOD_PRESSURE) {
					//이미 페어링 된 Device를 추가하면 안 된다.
					Set<BluetoothDevice> acquirePairedDevice = dev.getPairedDevice();
					if(acquirePairedDevice.size() > 0) {
						for(BluetoothDevice getDevice : acquirePairedDevice) {
							if(!getDevice.getAddress().equals(device.getAddress())) {
								if(emptyTextView.getVisibility() == View.VISIBLE) {
									emptyTextView.setVisibility(View.GONE);	//첫번째 검색 때는 검색된 장치가 없었지만, 재검색 시 검색된 장치가 있으면 이 TextView는 없애야 한다.
									availablePulseMachineListView.setVisibility(View.VISIBLE);	//emptyTextView를 없애면 ListView는 다시 보여줘야 한다.
								}
								
								bluetoothDevice.add(device);
								
								availableAdapter.add(device);
								availableAdapter.notifyDataSetChanged();
							}
						}
					}
					else {
						if(emptyTextView.getVisibility() == View.VISIBLE) {
							emptyTextView.setVisibility(View.GONE);	//첫번째 검색 때는 검색된 장치가 없었지만, 재검색 시 검색된 장치가 있으면 이 TextView는 없애야 한다.
							availablePulseMachineListView.setVisibility(View.VISIBLE);	//emptyTextView를 없애면 ListView는 다시 보여줘야 한다.
						}
						
						bluetoothDevice.add(device);
						
						availableAdapter.add(device);
						availableAdapter.notifyDataSetChanged();
					}
				}
			} else if (BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action)) {
				if(getBluetoothDevice == null) {
					BluetoothDevice device = (BluetoothDevice) intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
					
					if(device.getBondState() == BluetoothDevice.BOND_BONDING) {
						getBluetoothDevice = device;
					}
				}
				
				final int state = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, BluetoothDevice.ERROR);
				final int prevState = intent.getIntExtra(BluetoothDevice.EXTRA_PREVIOUS_BOND_STATE, BluetoothDevice.ERROR);
				
				if (state == BluetoothDevice.BOND_BONDED && prevState == BluetoothDevice.BOND_BONDING) {
					LogMgr.d(LogMgr.TAG, "PulseSensorUI.java:enclosing_method:// Paired");
					
					if(progressDialog != null) {
						progressDialog.dismiss();
						progressDialog = null;
					}
					if( bluetoothDevice != null && bluetoothDevice.size() > 0) {
						bluetoothDevice.remove(pairPosition);	//페어링 하고자 할 장비의 리스트 삭제는 페어링 완료 후에 하는 것이 맞다.
					}
					
					if( availableAdapter != null && availableAdapter.getCount() > 0)
					{
						availableAdapter.remove(pairPosition);
						availableAdapter.notifyDataSetChanged();
					}
					
					if(showPairedLayout.getVisibility() == View.GONE) {	//'연결된 장비' 영역이 없으면, 생성해야 한다.
						showPairedLayout.setVisibility(View.VISIBLE);
					}
					
					if(pairedAdapter != null)
					{
						pairedAdapter.add(getBluetoothDevice);
						pairedAdapter.notifyDataSetChanged();
					}
					
					if( pairedDevice != null)
						pairedDevice.add(getBluetoothDevice);
					
					Toast.makeText(mContext, "연결 완료", Toast.LENGTH_SHORT).show();
					
					getBluetoothDevice = null;
					
					SmartCareSystemApplication.getInstance().getSmartCareHealthApp().bloodPressureStopService();
					SmartCareSystemApplication.getInstance().getSmartCareHealthApp().bloodPressureStartService();
					
					//M2M으로 혈압계 등록 패킷 전달
					BluetoothDevice device = (BluetoothDevice) intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
					String mac = device.getAddress().replaceAll(":", " ");
					Sensor sensor = new Sensor(SensorID.getSensorId(SensorID.BLOOD_PRESURE_TYPE.getSensorType(), 0x01), mac);
					sensor.setComm_status(1);
					sensor.setSensor_batt(100);
					sensor.setReg_status(1);
					
					ProtocolManger pManger = SmartCareSystemApplication.getInstance().getProtocolManger();
					pManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REGISTER_SENSOR, sensor, null));
					pManger.messageSend(null);
					
				} else if (state == BluetoothDevice.BOND_NONE && prevState == BluetoothDevice.BOND_BONDED) {
					LogMgr.d(LogMgr.TAG, "PulseSensorUI.java:enclosing_method:// Unpaired");
					
					//PulseSensorAdapterUI에서 생성한 ProgressDialog를 가져온다.
					ProgressDialogUI getProgressDialog = ProgressDialogUI.getInstance();
					if(getProgressDialog != null) {
						getProgressDialog.dismiss();
						getProgressDialog.setNullInstance();
					}
					
					pairedDevice.remove(removePosition);
					
					pairedAdapter.remove(removePosition);
					pairedAdapter.notifyDataSetChanged();
					
					if(pairedDevice.size() <= 0) {	//연결된 장비가 없다면, '연결된 장비' 영역은 없애야 한다.
						showPairedLayout.setVisibility(View.GONE);
					}
					
					Toast.makeText(mContext, "연결 해제 완료", Toast.LENGTH_SHORT).show();
					
					//SmartCareSystemApplication.getInstance().getSmartCareHealthApp().bloodPressureStopService();
				}
			}
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pulse_sensor_ui);
		
		mContext = this;
		
		fragmentManager  = getFragmentManager();
		
		searchingProgressBar = (ProgressBar) findViewById(R.id.searchingProgressBar);
		
		emptyTextView = (TextView) findViewById(R.id.emptyTextView);
		emptyTextView.setVisibility(View.GONE);
		
		searchingTextView = (TextView) findViewById(R.id.searchingTextView);
		
		TextView titleTextView = (TextView) findViewById(R.id.titleTextView);
		
		String titleText;
		
		Intent intent = getIntent();
		
		Bundle bundle = intent.getExtras();
		
		if(bundle != null) {
			titleText = bundle.getString(UIConstant.SENSOR_TYPE);
		}
		else {	//TestCase의 경우 bundle이 없다. 여기서 직접 text를 설정한다.
			titleText = "혈압계";
		}
		
		titleTextView.setText(titleText);
		
		availableAdapter = new PulseSensorAdapterUI(mContext, PulseSensorAdapterUI.MODE_AVAILABLE_BLOOD_PRESSURE_MACHINE, fragmentManager);
		pairedAdapter = new PulseSensorAdapterUI(mContext, PulseSensorAdapterUI.MODE_PAIRED_BLOOD_PRESSURE_MACHINE, fragmentManager);
		
		bluetoothDevice = new ArrayList<BluetoothDevice>();
		pairedDevice = new ArrayList<BluetoothDevice>();
		
		dev = new DeviceScan();
		
		showPairedLayout = (LinearLayout) findViewById(R.id.pairedDeviceLayout);
		
		Set<BluetoothDevice> pairedDevices = dev.getPairedDevice();
		
		int bloodPressureMachineCnt = 0;
		
		for (BluetoothDevice device : pairedDevices) {
			//페어링 된 장치가 혈압계인지 확인한다.
			if(device.getBluetoothClass().getDeviceClass() == BluetoothClass.Device.HEALTH_BLOOD_PRESSURE)
				bloodPressureMachineCnt++;
		}
		
		if(bloodPressureMachineCnt == 0) {
			showPairedLayout.setVisibility(View.GONE);
		}
		else {
			showPairedLayout.setVisibility(View.VISIBLE);
			
			for (BluetoothDevice device : pairedDevices) {
				if(device.getBluetoothClass().getDeviceClass() == BluetoothClass.Device.HEALTH_BLOOD_PRESSURE) {
					pairedDevice.add(device);
					pairedAdapter.add(device);
				}
            }
		}
		
		dev.turnOnBluetooth();	//현재 Bluetooth가 꺼져 있으면, On 한다.
//		dev.scan();
			
		IntentFilter filter = new IntentFilter();
		filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
		filter.addAction(BluetoothDevice.ACTION_FOUND);
		filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
		filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
		filter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
		
		registerReceiver(mReceiver, filter);
		
		backKeyButton = (Button) findViewById(R.id.backButton);
		backKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		homeKeyButton = (Button) findViewById(R.id.homeButton);
		homeKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if(dev.isDiscovering())
					dev.cancelScan();
				
				Intent intent = new Intent(mContext, LauncherUI.class);
				//intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});
		
		searchButton = (Button) findViewById(R.id.searchButton);
		searchButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dev.scan();
			}
		});
		
		availablePulseMachineListView = (ListView) findViewById(R.id.registerConditionListView);
		availablePulseMachineListView.setAdapter(availableAdapter);
		availablePulseMachineListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				pairPosition = position;
				
				getBluetoothDevice = bluetoothDevice.get(position);
				
				progressDialog = ProgressDialogUI.createInstance();
				progressDialog.setMode(ProgressDialogUI.MODE_PAIRING_BLOOD_PRESSURE);
				progressDialog.setTitle(getBluetoothDevice.getName());
				progressDialog.show(fragmentManager, "Dialog");
				
				dev.cancelScan();	//페어링 할 때, 스캔을 멈춘다.
				dev.pairDevice(getBluetoothDevice);
			}
		});
		
		pairedListView = (ListView) findViewById(R.id.pairedListView);
		pairedListView.setAdapter(pairedAdapter);
		pairedListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				//혈압계 상세보기
				Intent intent = new Intent(mContext, DetailBloodPressureMachineUI.class);
				intent.putExtra(UIConstant.BLOOD_PRESSURE_MAC, pairedDevice.get(position).getAddress());
				intent.putExtra(UIConstant.BLOOD_PRESSURE_NAME, pairedDevice.get(position).getName());
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
		unregisterReceiver(mReceiver);
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		
		if(dev.isDiscovering())
			dev.cancelScan();
	}

	public static void setRemovePosition(int position) {
		removePosition = position;
	}
	
}
