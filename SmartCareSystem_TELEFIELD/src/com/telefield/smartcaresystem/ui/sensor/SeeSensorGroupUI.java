package com.telefield.smartcaresystem.ui.sensor;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.database.ConnectDB;
import com.telefield.smartcaresystem.database.Sensor;
import com.telefield.smartcaresystem.message.SensorID;
import com.telefield.smartcaresystem.ui.LauncherUI;
import com.telefield.smartcaresystem.ui.UIConstant;
import com.telefield.smartcaresystem.util.LogMgr;

public class SeeSensorGroupUI extends Activity {
	Button backKeyButton, homeKeyButton;
	TextView titleTextView, emptyTextView;
	ListView sensorGroupListView;
	SensorAdapterUI mAdapter;
	FragmentManager currentFragmentManager;
	String titleText;
	Context mContext;
	
	private static boolean isDeleteSensor = false; 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.see_register_condition);
		
		mContext = this;
		
		currentFragmentManager = getFragmentManager();
		
		titleTextView = (TextView) findViewById(R.id.titleTextView);
		emptyTextView = (TextView) findViewById(R.id.emptyTextView);
		sensorGroupListView = (ListView) findViewById(R.id.registerConditionListView);
		
		Intent intent = getIntent();
		
		Bundle bundle = intent.getExtras();
		
		if(bundle != null) {
			titleText = bundle.getString(UIConstant.SENSOR_TYPE);
		}
		else {	//TestCase의 경우 bundle이 없다. 여기서 직접 text를 설정한다.
			titleText = UIConstant.FIRE_SENSOR;
		}
		
		titleTextView.setText(titleText);
		
		backKeyButton = (Button) findViewById(R.id.backButton);
		backKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		homeKeyButton = (Button) findViewById(R.id.homeButton);
		homeKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mContext, LauncherUI.class);
				//intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		String getSensorName = titleText;
		if(getSensorName.equals(UIConstant.DISPLAY_FIRE_SENSOR)) {
			getSensorName = UIConstant.FIRE_SENSOR;
		}
		else if(getSensorName.equals(UIConstant.DISPLAY_GAS_SENSOR)) {
			getSensorName = UIConstant.GAS_SENSOR;
		}
		
		ConnectDB con = ConnectDB.getInstance(this);
		Sensor sensor = new Sensor();
		sensor.setType(SensorID.getSensorType(getSensorName));
		
		final List<Sensor> result = con.selectSensor(sensor);
		
		if(result != null)
		{
			LogMgr.d(LogMgr.TAG, "MSG " + SensorID.getSensorType(titleText) + " result size : " + result.size());
			mAdapter = new SensorAdapterUI(this, result, UIConstant.MODE_DELETE_SENSOR, currentFragmentManager);
			sensorGroupListView.setAdapter(mAdapter);
			
			if(sensorGroupListView.getCount() > 0) {
				emptyTextView.setVisibility(View.GONE);
				
				sensorGroupListView.setOnItemClickListener(new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
						// TODO Auto-generated method stub
						Sensor getSensor = result.get(position);	//센서 정보
						
						Integer sensor_batt = getSensor.getSensor_batt();	//센서 배터리 상태
						Integer sensor_status = getSensor.getComm_status();	//센서 상태
						Integer sensor_zigbee = getSensor.getZigbee_batt();	//Zigbee 배터리 상태
						Integer sensor_rssi = getSensor.getRssi();	//센서 RSSI
						String registerTime = getStringValueDate(getSensor.getRegister_time());						//등록 시간
						String lastCommunicationTime = getStringValueDate(getSensor.getLast_communication_time());	//마지막 통신 시간
						Integer keepAliveTime = getSensor.getKeepAliveTime();	//Keep alive time
						Integer sensor_version = getSensor.getVersion();
						TextView middleTitle = (TextView) view.findViewById(R.id.sensorName);	//센서 이름이 있는 View
						
						Intent intent = new Intent(mContext, DetailSensorInfoUI.class);
						intent.putExtra(UIConstant.SENSOR_MAC, getSensor.getMAC());
						
						intent.putExtra(UIConstant.SENSOR_BATTERY, (sensor_batt == null || sensor_batt== 0)  ? "정보없음" : sensor_batt);
						if(sensor_zigbee == null) {
							intent.putExtra(UIConstant.SENSOR_ZIGBEE_BATTERY,"정보없음");
						}
						else if(sensor_zigbee== 0) {
							intent.putExtra(UIConstant.SENSOR_ZIGBEE_BATTERY,"양호");
						}
						else {
							intent.putExtra(UIConstant.SENSOR_ZIGBEE_BATTERY,"부족");
						}
						intent.putExtra(UIConstant.SENSOR_RSSI, (sensor_rssi == null || sensor_rssi == 0) ? "정보없음" : sensor_rssi);
						intent.putExtra(UIConstant.SENSOR_COMMUNICATION_STATUS, (sensor_status == null || sensor_status== 0)  ? "불량" : "정상");
						intent.putExtra(UIConstant.SENSOR_REGISTER_TIME, registerTime);
						intent.putExtra(UIConstant.SENSOR_LAST_COMMUNICATION_TIME, lastCommunicationTime);
						intent.putExtra(UIConstant.SENSOR_KEEP_ALIVE_TIME, keepAliveTime == null ? "정보 없음" : keepAliveTime+"분");
						intent.putExtra(UIConstant.SENSOR_VERSION, Sensor.getStringVersion(Integer.toHexString(sensor_version.intValue())));
						intent.putExtra(UIConstant.TITLE, middleTitle.getText());
						intent.putExtra(UIConstant.SENSOR_ID_KEY, getSensor.getSensor_id());	//Sensor_ID
						intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(intent);
					}
				});
				
			}
			else {
				emptyTextView.setVisibility(View.VISIBLE);
				emptyTextView.setText(R.string.no_sensor);
			}
		}else
		{
			emptyTextView.setVisibility(View.VISIBLE);
			emptyTextView.setText(R.string.no_sensor);
		}
	}
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();	
		
		/*if( mAdapter != null)		
			SmartCareSystemApplication.getInstance().getSensorRegisterFunc().removeEventListener(mAdapter.getHandler());*/
		
		LogMgr.d(LogMgr.TAG , " SeeSensorGroupUI onStop()");
	}
	
	private String getStringValueDate(Date data) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		
		return format.format(data);
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		// TODO Auto-generated method stub
		super.onWindowFocusChanged(hasFocus);
		
		if(hasFocus && isDeleteSensor) {	//삭제 완료 팝업의 버튼을 클릭한 경우
			ConnectDB con = ConnectDB.getInstance(this);
			Sensor sensor = new Sensor();
			sensor.setType(SensorID.getSensorType(titleText));
			
			List<Sensor> result = con.selectSensor(sensor);
			
			if(!(result != null && result.size() > 0)) {
				sensorGroupListView.setVisibility(View.GONE);
				emptyTextView.setVisibility(View.VISIBLE);
				emptyTextView.setText(R.string.no_sensor);
			}
			mAdapter.updateList(result);
			
			isDeleteSensor = false;
		}
		
	}
	
	public static void deleteSensor() {
		isDeleteSensor = true;
	}
	
}
