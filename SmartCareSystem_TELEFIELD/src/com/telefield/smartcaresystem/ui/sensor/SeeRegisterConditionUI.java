package com.telefield.smartcaresystem.ui.sensor;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.bluetooth.DeviceScan;
import com.telefield.smartcaresystem.database.ConnectDB;
import com.telefield.smartcaresystem.database.Sensor;
import com.telefield.smartcaresystem.message.SensorID;
import com.telefield.smartcaresystem.ui.LauncherUI;
import com.telefield.smartcaresystem.ui.UIConstant;

public class SeeRegisterConditionUI extends Activity {
	Button backKeyButton, homeKeyButton;
	ListView wholeSensorInfo;
	TextView titleTextView, emptyTextView;
	SensorAdapterUI mAdapter;
	Context mContext;
	DeviceScan dev;
	Set<BluetoothDevice> getPairedDevice;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.see_register_condition);
		
		mContext = this;
		
		titleTextView = (TextView) findViewById(R.id.titleTextView);
		titleTextView.setText(R.string.register_condition);
		emptyTextView = (TextView) findViewById(R.id.emptyTextView);
		
		wholeSensorInfo = (ListView) findViewById(R.id.registerConditionListView);
		
		backKeyButton = (Button) findViewById(R.id.backButton);
		backKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		homeKeyButton = (Button) findViewById(R.id.homeButton);
		homeKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mContext, LauncherUI.class);
				//intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		ConnectDB con = ConnectDB.getInstance(this);
		final List<Sensor> result = con.selectAllSensors();
		
		mAdapter = new SensorAdapterUI(this, result, UIConstant.MODE_SEE_REGISTER_CONDITION, null);
		wholeSensorInfo.setAdapter(mAdapter);
		
		//페어링 된 혈압계가 있으면, 혈압계도 추가한다.
		dev = new DeviceScan();
		
		getPairedDevice = dev.getPairedDevice();
		if(getPairedDevice.size() > 0) {
			for(BluetoothDevice device : getPairedDevice) {
				Sensor tempAdd = new Sensor();
				tempAdd.setMAC(device.getAddress());
				tempAdd.setComm_status(1000);	//통신 상태는 '정상'이 나오도록 설정(차후에 혈압계와 다른 센서의 구분으로도 사용)
				tempAdd.setSensor_batt(1000);	//베터리 상태도 '정상'이 나오도록 설정(차후에 혈압계와 다른 센서의 구분으로도 사용)
				
				mAdapter.addBloodPressureMachine(tempAdd);
			}
			
			mAdapter.notifyDataSetChanged();
		}
		
		if(wholeSensorInfo.getCount() > 0) {
			emptyTextView.setVisibility(View.GONE);
			
			wholeSensorInfo.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					// TODO Auto-generated method stub
					Sensor getSensor = result.get(position);	//센서 정보
					
					if(getSensor.getComm_status() == 1000 && getSensor.getSensor_batt() == 1000) {
						String bloodPressureMac = result.get(position).getMAC();
						
						for(BluetoothDevice device : getPairedDevice) {
							if(device.getAddress().equals(bloodPressureMac)) {
								Intent intent = new Intent(mContext, DetailBloodPressureMachineUI.class);
								intent.putExtra(UIConstant.BLOOD_PRESSURE_MAC, device.getAddress());
								intent.putExtra(UIConstant.BLOOD_PRESSURE_NAME, device.getName());
								intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
								startActivity(intent);
								
								break;
							}
						}
					}
					else {
						
						Integer sensor_batt = getSensor.getSensor_batt();	//센서 배터리 상태
						Integer sensor_status = getSensor.getComm_status();	//센서 상태
						Integer sensor_zigbee = getSensor.getZigbee_batt();	//Zigbee 배터리 상태
						Integer sensor_rssi = getSensor.getRssi();	//센서 RSSI
						Integer sensor_version = getSensor.getVersion();
						String registerTime = getStringValueDate(getSensor.getRegister_time());						//등록 시간
						String lastCommunicationTime = getStringValueDate(getSensor.getLast_communication_time());	//마지막 통신 시간
						Integer keepAliveTime = getSensor.getKeepAliveTime();	//Keep alive time
						
						TextView middleTitle = (TextView) view.findViewById(R.id.sensorName);	//센서 이름이 있는 View
						
						Intent intent = new Intent(mContext, DetailSensorInfoUI.class);
						intent.putExtra(UIConstant.SENSOR_MAC, getSensor.getMAC());
						intent.putExtra(UIConstant.SENSOR_BATTERY, (sensor_batt == null || sensor_batt== 0)  ? "정보없음" : sensor_batt);
						if(sensor_zigbee == null) {
							intent.putExtra(UIConstant.SENSOR_ZIGBEE_BATTERY,"정보없음");
						}
						else if(sensor_zigbee== 0) {
							intent.putExtra(UIConstant.SENSOR_ZIGBEE_BATTERY,"양호");
						}
						else {
							intent.putExtra(UIConstant.SENSOR_ZIGBEE_BATTERY,"부족");
						}
						intent.putExtra(UIConstant.SENSOR_RSSI, (sensor_rssi == null || sensor_rssi == 0) ? "정보없음" : sensor_rssi);
						intent.putExtra(UIConstant.SENSOR_COMMUNICATION_STATUS, (sensor_status == null || sensor_status== 0)  ? "불량" : "정상");
						intent.putExtra(UIConstant.SENSOR_REGISTER_TIME, registerTime);
						intent.putExtra(UIConstant.SENSOR_LAST_COMMUNICATION_TIME, lastCommunicationTime);
						intent.putExtra(UIConstant.SENSOR_KEEP_ALIVE_TIME, keepAliveTime == null ? "정보 없음" : keepAliveTime+"분");
						intent.putExtra(UIConstant.SENSOR_VERSION, Sensor.getStringVersion(Integer.toHexString(sensor_version.intValue())));
						intent.putExtra(UIConstant.TITLE, middleTitle.getText());
						intent.putExtra(UIConstant.SENSOR_ID_KEY, getSensor.getSensor_id());	//Sensor_ID
						intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(intent);
					}
				}
			});
		}
		else {
			emptyTextView.setVisibility(View.VISIBLE);
			emptyTextView.setText(R.string.no_whole_sensor);
		}
		
		backKeyButton = (Button) findViewById(R.id.backButton);
		backKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		homeKeyButton = (Button) findViewById(R.id.homeButton);
		homeKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mContext, LauncherUI.class);
				//intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});
	}

	private String getStringValueDate(Date data) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		
		return format.format(data);
	}
}
