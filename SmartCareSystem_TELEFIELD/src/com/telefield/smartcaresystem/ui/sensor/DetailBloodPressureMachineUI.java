package com.telefield.smartcaresystem.ui.sensor;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.ui.LauncherUI;
import com.telefield.smartcaresystem.ui.UIConstant;

public class DetailBloodPressureMachineUI extends Activity {
	Button backKeyButton, homeKeyButton;
	Context mContext;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.detail_sensor_info);
		
		mContext = this;
		
		TextView rssi;
		rssi = (TextView) findViewById(R.id.rssi);
		rssi.setText("이름 :");
		
		LinearLayout registerTimeLayout, lastCommunicationLayout, keepAliveTimeLayout;
		
		registerTimeLayout = (LinearLayout) findViewById(R.id.registerTimeLayout);
		lastCommunicationLayout = (LinearLayout) findViewById(R.id.lastCommunicationLayout);
		keepAliveTimeLayout = (LinearLayout) findViewById(R.id.keepAliveTimeLayout);
		
		registerTimeLayout.setVisibility(View.GONE);
		lastCommunicationLayout.setVisibility(View.GONE);
		keepAliveTimeLayout.setVisibility(View.GONE);
		
		TextView titleTextView = (TextView) findViewById(R.id.titleTextView);
		TextView middleTitleTextView = (TextView) findViewById(R.id.middleTitleTextView);
		TextView macInfo = (TextView) findViewById(R.id.macInfo);
		TextView batteryInfo = (TextView) findViewById(R.id.batteryInfo);
		TextView rssiInfo = (TextView) findViewById(R.id.rssiInfo);
		TextView communicationStatusInfo = (TextView) findViewById(R.id.communicationStatusInfo);
		
		// 이 UI는 혈압계 상세보기만 해당되고, 배터리 상태는 '양호', 통신 상태는 '정상'으로 처리함
		titleTextView.setText(R.string.blood_pressure_machine);
		batteryInfo.setText(" " + getString(R.string.good));
		communicationStatusInfo.setText(" " + getString(R.string.normality));
		
		Intent intent = getIntent();
		
		middleTitleTextView.setText(intent.getExtras().getString(UIConstant.BLOOD_PRESSURE_NAME));
		rssiInfo.setText(" " + intent.getExtras().getString(UIConstant.BLOOD_PRESSURE_NAME));
		macInfo.setText(" " + intent.getExtras().getString(UIConstant.BLOOD_PRESSURE_MAC));
		
		backKeyButton = (Button) findViewById(R.id.backButton);
		backKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		homeKeyButton = (Button) findViewById(R.id.homeButton);
		homeKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mContext, LauncherUI.class);
				//intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});
	}
}
