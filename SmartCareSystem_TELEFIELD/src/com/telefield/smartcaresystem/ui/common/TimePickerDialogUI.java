package com.telefield.smartcaresystem.ui.common;

import java.util.StringTokenizer;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.TimePicker.OnTimeChangedListener;
import android.widget.Toast;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.ui.UIConstant;
import com.telefield.smartcaresystem.util.SmartCarePreference;

public class TimePickerDialogUI extends DialogFragment implements OnClickListener {
	SmartCarePreference pref;
	TimePicker fromPicker, toPicker;
	Context mContext;
	IDialogChangeListener listener;

	String title;
	int mode;
	int fromHour, fromMinute, toHour, toMinute;
	private Button btnOK, btnCancel;
	private TextView titleView;

	public static TimePickerDialogUI newInstance() {
		TimePickerDialogUI f = new TimePickerDialogUI();
		return f;
	}

	public static TimePickerDialogUI newInstance(IDialogChangeListener listener) {
		TimePickerDialogUI f = new TimePickerDialogUI();
		f.setDialogChangeListener(listener);
		return f;
	}

	public static TimePickerDialogUI newInstance(IDialogChangeListener listener, int mode) {
		TimePickerDialogUI f = new TimePickerDialogUI();
		f.setDialogChangeListener(listener);
		f.setMode(mode);
		return f;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (getDialog() != null) {
			getDialog().setCanceledOnTouchOutside(false);
		}
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		mContext = getActivity().getApplicationContext();
		pref = new SmartCarePreference(mContext, SmartCarePreference.APPLICATION_SETTING_INFO_FILENAME);

		String fromTimeData = pref.getStringValue(SmartCarePreference.SELF_CALL_FROM_TIME, UIConstant.DEFAULT_START_SELF_CALL_TIME_VALUE);
		String toTimeData = pref.getStringValue(SmartCarePreference.SELF_CALL_TO_TIME, UIConstant.DEFAULT_END_SELF_CALL_TIME_VALUE);

		AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
		LayoutInflater mLayoutInflater = getActivity().getLayoutInflater();

		View view = mLayoutInflater.inflate(R.layout.time_picker_dialog, null);

		btnOK = (Button) view.findViewById(R.id.btn_ok);
		btnCancel = (Button) view.findViewById(R.id.btn_cancel);
		titleView = (TextView) view.findViewById(R.id.title_view);

		titleView.setText(title);
		
		btnOK.setOnClickListener(this);
		btnCancel.setOnClickListener(this);

		TabHost tabHost = (TabHost) view.findViewById(android.R.id.tabhost);
		tabHost.setup();

		TabSpec spec1 = tabHost.newTabSpec("Tab1").setContent(R.id.tab1).setIndicator(getString(R.string.start));
		tabHost.addTab(spec1);
		TabSpec spec2 = tabHost.newTabSpec("Tab2").setContent(R.id.tab2).setIndicator(getString(R.string.end));
		tabHost.addTab(spec2);

		tabHost.getTabWidget().getChildAt(0).getLayoutParams().height = 50;
		tabHost.getTabWidget().getChildAt(1).getLayoutParams().height = 50;

		// 셀프콜 금지 시작/종료 시간 가져옴
		StringTokenizer st = new StringTokenizer(fromTimeData, ":");
		fromHour = Integer.parseInt(st.nextToken());
		fromMinute = Integer.parseInt(st.nextToken());

		st = new StringTokenizer(toTimeData, ":");
		toHour = Integer.parseInt(st.nextToken());
		toMinute = Integer.parseInt(st.nextToken());

		fromPicker = (TimePicker) view.findViewById(R.id.fromTimePicker);
		fromPicker.setCurrentHour(fromHour);
		fromPicker.setCurrentMinute(fromMinute);
		fromPicker.setOnTimeChangedListener(new OnTimeChangedListener() {
			@Override
			public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
				// TODO Auto-generated method stub
				fromHour = hourOfDay;
				fromMinute = minute;
			}
		});

		toPicker = (TimePicker) view.findViewById(R.id.toTimePicker);
		toPicker.setCurrentHour(toHour);
		toPicker.setCurrentMinute(toMinute);
		toPicker.setOnTimeChangedListener(new OnTimeChangedListener() {
			@Override
			public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
				// TODO Auto-generated method stub
				toHour = hourOfDay;
				toMinute = minute;
			}
		});

		mBuilder.setView(view);

		return mBuilder.create();
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setMode(int mode) {
		this.mode = mode;
	}

	public void setDialogChangeListener(IDialogChangeListener listener) {
		this.listener = listener;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_ok:

			if ((fromHour == toHour) && (fromMinute == toMinute)) {
				Toast.makeText(mContext, R.string.no_time_different_at_selfcall, Toast.LENGTH_SHORT).show();
			} else {
				String editHourvalue = Integer.toString(fromHour);
				if (editHourvalue.length() == 1)
					editHourvalue = "0" + editHourvalue;

				String editMinutevalue = Integer.toString(fromMinute);
				if (editMinutevalue.length() == 1)
					editMinutevalue = "0" + editMinutevalue;

				pref.setStringValue(SmartCarePreference.SELF_CALL_FROM_TIME, editHourvalue + ":" + editMinutevalue);

				editHourvalue = Integer.toString(toHour);
				if (editHourvalue.length() == 1)
					editHourvalue = "0" + editHourvalue;

				editMinutevalue = Integer.toString(toMinute);
				if (editMinutevalue.length() == 1)
					editMinutevalue = "0" + editMinutevalue;

				pref.setStringValue(SmartCarePreference.SELF_CALL_TO_TIME, editHourvalue + ":" + editMinutevalue);

				if (listener != null)
					listener.notifyChangeListener();

				dismiss();
			}

			break;
		case R.id.btn_cancel:
			dismiss();
			break;
		}
	}
}
