package com.telefield.smartcaresystem.ui.common;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.http.IProgressNotify;
import com.telefield.smartcaresystem.ui.UIConstant;

public class ProgressDialogUI extends DialogFragment implements IProgressNotify{
	int mode;
	String title;	
	Handler mHandler;
	Activity mActivity;
	private TextView titleView;
	
	private static ProgressDialogUI progressDialog;	//맥박센서 검색 시 사용(맥박 센서 검색 중 전화 수신 시 ProgressDialog가 사라져야 함)
	int currentProgress;
	ProgressBar pb;
	
	public static final int MODE_REGISTER = 1;	//센서 등록
	public static final int MODE_DELETE = 2;	//센서 삭제
	public static final int MODE_OUT_VISIT_REGISTER = 3;	//돌보미 퇴실
	public static final int MODE_INITIALIZE_SENSOR_FINAL_STEP = 4;	//시스템 초기화 마지막 단계에 사용
	public static final int MODE_PAIRING_BLOOD_PRESSURE = 5;	//혈압계 페어링
	public static final int MODE_UNPAIRING_BLOOD_PRESSURE = 6;	//혈압계 언페어링
//	public static final int MODE_APP_UPDATE_PROGRESS = 7; // 어플리케이션 업데이트 진행바
//	public static final int MODE_CODI_UPDATE_PROGRESS = 8;
	
	public static ProgressDialogUI createInstance() {
		if(progressDialog == null) {
			progressDialog = new ProgressDialogUI();
		}
		return progressDialog;
	}
	
	public static ProgressDialogUI getInstance() {
		return progressDialog;
	}
	
	public void init()
	{
		//data 초기화
		currentProgress = 0;
		pb = null;
		
	}
	
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		
		mActivity = activity;
		mActivity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON | 
				WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
	}
	
	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		
		mActivity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON | 
				WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	    if (getDialog() != null) {
	        getDialog().setCanceledOnTouchOutside(false);
	    }
	    return super.onCreateView(inflater, container, savedInstanceState);
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
		LayoutInflater mLayoutInflater = getActivity().getLayoutInflater();
		
		View view = mLayoutInflater.inflate(R.layout.progress_dialog, null);
		
		titleView = (TextView) view.findViewById(R.id.title_view);
			
		if(mode == MODE_REGISTER)
			titleView.setText(title+" "+getResources().getText(R.string.during_registration).toString());
		else if(mode == MODE_OUT_VISIT_REGISTER)
			titleView.setText(title+" "+getResources().getText(R.string.during_registration).toString());
		else if(mode == MODE_INITIALIZE_SENSOR_FINAL_STEP)
			titleView.setText(title+" "+getResources().getText(R.string.jung).toString());
		else if(mode == MODE_DELETE)
			titleView.setText(title+" "+getResources().getText(R.string.during_delete).toString());
		else if(mode == MODE_PAIRING_BLOOD_PRESSURE)
			titleView.setText(title+" "+getResources().getText(R.string.connecting).toString());
		else if(mode == MODE_UNPAIRING_BLOOD_PRESSURE)
			titleView.setText(title+" "+getResources().getText(R.string.disconnecting).toString());
		/*else if(mode == MODE_APP_UPDATE_PROGRESS)
		{
			titleView.setText(title);
			ProgressBar pb = (ProgressBar)view.findViewById(R.id.progressBar1);
			pb.setVisibility(View.GONE);
			
			ProgressBar pb1 = (ProgressBar)view.findViewById(R.id.progressBar2);
			pb1.setVisibility(View.VISIBLE);
			pb1.setProgress(1);
			this.pb = pb1;
			
			new AsyncFileDownloader(mActivity.getApplicationContext()).download(Constant.GATEWAY_FW_DOWNLOAD_PATH, Constant.FW_APP_PATH, fileDownCallback,this);
		}else if(mode == MODE_CODI_UPDATE_PROGRESS)
		{			
			titleView.setText(title);
			ProgressBar pb = (ProgressBar)view.findViewById(R.id.progressBar1);
			pb.setVisibility(View.GONE);
			
			ProgressBar pb1 = (ProgressBar)view.findViewById(R.id.progressBar2);
			pb1.setVisibility(View.VISIBLE);
			pb1.setProgress(1);
			this.pb = pb1;
			
			new AsyncFileDownloader(mActivity.getApplicationContext()).download(Constant.ZIGBEE_FW_DOWNLOAD_PATH, Constant.FW_GATEWAY_PATH, fileDownCallback,this);
			
			
			
		}*/
		
		
		mBuilder.setView(view);
		
		return mBuilder.create();
	}
	
	@Override
	public void onDismiss(DialogInterface dialog) {
		if(mHandler != null)
			mHandler.sendEmptyMessage(UIConstant.DIALOG_CANCEL);
		
		super.onDismiss(dialog);
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public void setMode(int mode) {
		this.mode = mode;
	}
	
	public void setHandler(Handler handler){
		mHandler = handler;
	}
	
	public void setNullInstance() {
		progressDialog = null;
	}
	
	@Override
	public void setCurrentProgress(int progress) {
	/*	int total_size = 3341267;
		
		this.currentProgress += progress;	
		
		
		if(mode == MODE_APP_UPDATE_PROGRESS && pb!=null && pb.getVisibility() == View.VISIBLE)
		{
			total_size = 3341267;
			LogMgr.d(LogMgr.TAG,"APP Update progress : " + progress);
			pb.setProgress(currentProgress  * 100 / total_size  );
		}else if( mode == MODE_CODI_UPDATE_PROGRESS && pb!=null && pb.getVisibility() == View.VISIBLE)
		{
			total_size = 154400;
			LogMgr.d(LogMgr.TAG,"CODI Update currentProgress : " + currentProgress + " progress : " + progress);
			pb.setProgress(currentProgress  * 100 / total_size  );
		}
		*/
	}

	@Override
	public int getCurrentProgreess() {
		// TODO Auto-generated method stub
		return this.currentProgress;
	}
	
/*	AsyncCallback<File> fileDownCallback = new AsyncCallback.Base<File>() {
		@Override
		public void onResult(File result) {
			LogMgr.d(LogMgr.TAG, "ProgressDialogUI result.getAbsolutePath() : " + result.getAbsolutePath());
			LogMgr.d(LogMgr.TAG, "ProgressDialogUI enclosing_method:// result.length() : " + result.length());
			
			progressDialog.dismiss();
			
			if( result!=null && Constant.FW_GATEWAY_PATH.equals(result.getAbsolutePath()))
			{				
				SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.START_CODI_FIRMWARE_UPDATE, TextToSpeech.QUEUE_FLUSH, null);
				
				LogMgr.d(LogMgr.TAG,"UpgradeController upgrade start");
				//펌웨어 업데이트 진행.
				//UpgradeController.getInstance().upgrade(UpgradeController.CODI_FIRMWARE_UPDATE_TYPE);
				
			}
			else //application update
			{
				// 애플리케이션 업그레이드 '확인' 버튼을 누른 경우
				SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.START_APPLICATION_UPDATE, TextToSpeech.QUEUE_FLUSH, null);
				
				if (SmartCareSystemApplication.getInstance().getMessageManager().ftdi_isConnect()) {
					SmartCareSystemApplication.getInstance().getGlobalMsgFunc().sendGateWayMsg((byte) MessageID.CODI_RESET);
				}
				//updateAPK();
							
//				Timer timer = new Timer();
//				timer.schedule(new TimerTask() {			
//					@Override
//					public void run() {
//						// TODO Auto-generated method stub
//						// 시스템 초기화시 본체 RESET 을 해야 FTDI 가 재연결됨.
//						if (SmartCareSystemApplication.getInstance().getMessageManager().ftdi_isConnect()) {
//							SmartCareSystemApplication.getInstance().getGlobalMsgFunc().sendGateWayMsg((byte) MessageID.CODI_RESET);
//						}
//						updateAPK();
//					}
//				}, 5* 1000);
			
			}

			
		}

		@Override
		public void exceptionOccured(Exception e) {
			e.printStackTrace();
		}
	};	*/
}
