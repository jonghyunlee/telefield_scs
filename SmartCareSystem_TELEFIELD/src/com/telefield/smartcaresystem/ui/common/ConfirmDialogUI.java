package com.telefield.smartcaresystem.ui.common;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.speech.tts.TextToSpeech;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.telefield.smartcaresystem.Constant;
import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.bluetooth.DeviceScan;
import com.telefield.smartcaresystem.database.Care;
import com.telefield.smartcaresystem.database.ConnectDB;
import com.telefield.smartcaresystem.database.Sensor;
import com.telefield.smartcaresystem.firmware.ApplicationManager;
import com.telefield.smartcaresystem.firmware.UpgradeController;
import com.telefield.smartcaresystem.function.SensorRegisterFunction;
import com.telefield.smartcaresystem.http.AsyncCallback;
import com.telefield.smartcaresystem.http.AsyncFileDownloader;
import com.telefield.smartcaresystem.http.HttpRequestHelper;
import com.telefield.smartcaresystem.m2m.CallbackEvent;
import com.telefield.smartcaresystem.m2m.EventRegistration;
import com.telefield.smartcaresystem.message.FormattedFrame;
import com.telefield.smartcaresystem.message.MessageID;
import com.telefield.smartcaresystem.message.SensorID;
import com.telefield.smartcaresystem.sensor.SensorDeviceManager;
import com.telefield.smartcaresystem.ui.UIConstant;
import com.telefield.smartcaresystem.ui.firmware.AppdownloadUI;
import com.telefield.smartcaresystem.ui.sensor.PulseSensorUI;
import com.telefield.smartcaresystem.ui.system.SystemSettingUI;
import com.telefield.smartcaresystem.ui.system.SystemSettingUI.DialogStep;
import com.telefield.smartcaresystem.util.ByteArrayToHex;
import com.telefield.smartcaresystem.util.LogMgr;
import com.telefield.smartcaresystem.util.SmartCarePreference;
import com.telefield.smartcaresystem.util.SpeechUtil;

public class ConfirmDialogUI extends DialogFragment implements OnClickListener {
	int mode;
	int deleteSensorID;
	int sensorIndex;
	private String title;
	DialogStep stepThread;
	CustomCountDownTimer timer;
	ProgressDialogUI customDialog;
	private Activity activity;
	private Button btnOK, btnCancel;
	private TextView titleView, descriptionView;
	private CallbackEvent mcallbackEvent;
	
	public static final int MODE_RELEASE_LOCKER = 1; // 시스템 락커 해제
	public static final int MODE_SYSTEM_RESET = 2; // 시스템 초기화
	public static final int MODE_REGISTER_FIRE_SENSOR = 3; // 화재 센서 등록
	public static final int MODE_REGISTER_GAS_SENSOR = 4; // 가스 센서 등록
	public static final int MODE_REGISTER_ACTIVITY_SENSOR = 5; // 활동 센서 등록
	public static final int MODE_REGISTER_DOOR_SENSOR = 6; // 도어 센서 등록
	public static final int MODE_REGISTER_EMERGENCY = 7; // 응급호출기 등록
	public static final int MODE_DELETE_SENSOR = 8; // 센서 삭제
	public static final int MODE_COME_CARE = 9; // 돌보미 방문 등록
	public static final int MODE_OUT_CARE = 10; // 돌보미 퇴실 등록
	public static final int MODE_APPLICATION_UPDATE = 11; // 애플리케이션 업데이트(스마트폰)
	public static final int MODE_OPENNING = 12; // 개통 안내
	public static final int MODE_BLUETOOTH_ON = 13; // 블루트스 ON
	public static final int MODE_REGISTER_GAS_BREAKER = 14; // 응급호출기 등록
	public static final int MODE_ZIGBEE_UPDATE = 15; // 본체 업데이트
	
	final Handler mHandler = new Handler(Looper.getMainLooper()) {
		public void handleMessage(Message msg) {
			int message = msg.what;

			switch (message) {
			case UIConstant.DIALOG_CANCEL:
				if (timer != null)
					timer.cancel();
				break;
			case UIConstant.NOTIFY_FTDI_DISCONNECTED:
				customDialog.dismiss();
				
				AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
				alertDialog.setTitle("주의");
				alertDialog.setMessage("본체가 연결이 되지 않았습니다. 연결을 확인해주세요");
				alertDialog.setCanceledOnTouchOutside(true);
				//확인버튼 추가 <--
				Message listener = null ;
				alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "확인", listener );
				// -->
				alertDialog.show();
				break;
			case UIConstant.NOTIFY_MESSAGE:
				customDialog.dismiss();
				
				if (timer != null)
					timer.cancel();

				FormattedFrame ff = (FormattedFrame) msg.obj;
				byte sub_data[] = ff.getSub_data();
				byte flag = sub_data[sub_data.length - 1];

				LogMgr.d(LogMgr.TAG, "SUB DATA" + ByteArrayToHex.byteArrayToHex(sub_data, sub_data.length));

				byte macAddr[] = Arrays.copyOf(sub_data, sub_data.length - 1);
				
				int version = Integer.parseInt(Integer.toHexString(ff.getVersion()));
				
				LogMgr.d(LogMgr.TAG, "Settings.NOTIFY_MESSAGE ::" + ff.getMsg_id() + "flag : " + flag);
				
				int sensorId = ff.getSensor_id();
				LogMgr.d(LogMgr.TAG, "Sensor ID :: " + ff.getSensor_id());
				
				if(sensorId >= 0x0101 && sensorId <= 0x07ff) {	//활동센서, 화재센서, 가스센서, 도어센서, 응급호출기, 가스차단기
					// 등록삭제 메시지
					
					if (ff.getMsg_id() == MessageID.REG_INFO_MSG) {
						boolean is_reg = (flag & 0x01) == 0x01 ? true : false;
						boolean is_success = (flag & 0x80) == 0x80 ? false : true;

						LogMgr.d(LogMgr.TAG, "reg info msg is_reg : " + is_reg + " is_success : " + is_success);
						String sensorTypeStr = SensorID.getSensorName(ff.getSensor_id());
						
						if (is_reg) // 등록
						{
							if (timer != null)
								timer.cancel();

							if (is_success) {							
								ResultDialogUI dialog = ResultDialogUI.newInstance(ResultDialogUI.MODE_REGISTER_SUCCESS);
								dialog.setTitle(sensorTypeStr + UIConstant.SPACE_CHARACTER + activity.getString(R.string.register_complete));
								dialog.setSensorNum(sensorIndex);
								dialog.setSensorID(ff.getSensor_id());
								dialog.setMacAddress(macAddr);
								dialog.setVersion(version);
								dialog.show(activity.getFragmentManager(), "Dialog");
							} else {
								ResultDialogUI dialog = ResultDialogUI.newInstance(ResultDialogUI.MODE_REGISTER_FAIL);
								dialog.setTitle(sensorTypeStr + activity.getString(R.string.register_fail));
								dialog.setSensorNum(sensorIndex);
								dialog.setSensorID(ff.getSensor_id());
								dialog.setMacAddress(macAddr);
								dialog.setVersion(version);
								dialog.show(activity.getFragmentManager(), "Dialog");

							}
						} else// 삭제
						{
							if (timer != null)
								timer.cancel();

							if (is_success) {
								sensorTypeStr = SensorID.getSensorName(ff.getSensor_id());
								
								FragmentManager fm = activity.getFragmentManager();
								
								ResultDialogUI dialog = ResultDialogUI.newInstance(ResultDialogUI.MODE_DELETE_SUCCESS);
								dialog.setTitle(sensorTypeStr + UIConstant.SPACE_CHARACTER + activity.getString(R.string.delete_complete));
								dialog.setSensorNum(sensorIndex);
								dialog.setSensorID(ff.getSensor_id());
								dialog.setMacAddress(macAddr);
								dialog.setVersion(version);
								dialog.show(fm, "Dialog");
							} else {

							}
						}
					}
				}
				else {	//돌보미 방문/퇴실
					if( ff.getMsg_id()== MessageID.REG_INFO_MSG )
					{					
						boolean is_reg = (flag & 0x01) == 0x01 ? true: false;
						boolean is_success = (flag & 0x80) == 0x80 ? false : true;
						
						LogMgr.d(LogMgr.TAG,"reg info msg is_reg : " + is_reg );
						String sensorTypeStr = SensorID.getSensorName(ff.getSensor_id());
						FragmentManager fm = activity.getFragmentManager();
						
						if( is_reg ) //방문
						{
							if(timer != null)
								timer.cancel();
							
							if (is_success) {							
								ResultDialogUI dialog = ResultDialogUI.newInstance(ResultDialogUI.MODE_COME_CARE_SUCCESS);
								dialog.setTitle(sensorTypeStr + " " + activity.getString(R.string.visit) + " " + activity.getString(R.string.register));
								dialog.setSensorID(ff.getSensor_id());
								dialog.setMacAddress(macAddr);
								dialog.setVersion(version);
								dialog.show(fm, "Dialog");
							} else {
								ResultDialogUI dialog = ResultDialogUI.newInstance(ResultDialogUI.MODE_REGISTER_FAIL);
								dialog.setTitle(sensorTypeStr + activity.getString(R.string.register_fail));
								dialog.setSensorID(ff.getSensor_id());
								dialog.setMacAddress(macAddr);
								dialog.setVersion(version);
								dialog.show(fm, "Dialog");
							}
						}
						else//퇴실
						{
							if(timer != null)
								timer.cancel();
							
							ResultDialogUI dialog = ResultDialogUI.newInstance(ResultDialogUI.MODE_OUT_CARE_SUCCESS);
							dialog.setTitle(sensorTypeStr + " " + activity.getString(R.string.out) + " " + activity.getString(R.string.register));
							dialog.setSensorID(ff.getSensor_id());
							dialog.setMacAddress(macAddr);
							dialog.setVersion(version);
							dialog.show(fm, "Dialog");
						}
					}
				}
				
				break;
			}
			SmartCareSystemApplication.getInstance().getSensorRegisterFunc().removeEventListener(this);
		}
	};
	
	public ConfirmDialogUI(){
	}
	
	public static ConfirmDialogUI newInstance(int mode) {
		ConfirmDialogUI f = new ConfirmDialogUI();
		f.setMode(mode);
		return f;
	}
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		this.activity = activity;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		getDialog().getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_VISIBLE);
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (getDialog() != null) {
			getDialog().setCanceledOnTouchOutside(false);
		}
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder mBuilder = new AlertDialog.Builder(activity);
		LayoutInflater mLayoutInflater = activity.getLayoutInflater();

		View view = mLayoutInflater.inflate(R.layout.confirm_dialog, null);

		btnOK = (Button) view.findViewById(R.id.btn_ok);
		btnCancel = (Button) view.findViewById(R.id.btn_cancel);
		titleView = (TextView) view.findViewById(R.id.title_view);
		descriptionView = (TextView) view.findViewById(R.id.description_view);
		btnOK.setOnClickListener(this);
		btnCancel.setOnClickListener(this);
		displayData(mode);

		if(mode == MODE_SYSTEM_RESET || mode == MODE_APPLICATION_UPDATE || mode == MODE_OPENNING || mode == MODE_BLUETOOTH_ON || mode == MODE_ZIGBEE_UPDATE) {
			titleView.setText(title);
		}
		else if(mode == MODE_REGISTER_FIRE_SENSOR || mode == MODE_REGISTER_GAS_SENSOR || mode == MODE_REGISTER_ACTIVITY_SENSOR
				|| mode == MODE_REGISTER_DOOR_SENSOR || mode == MODE_REGISTER_EMERGENCY || mode == MODE_COME_CARE || mode == MODE_OUT_CARE
				|| mode == MODE_REGISTER_GAS_BREAKER) {
			titleView.setText(title + UIConstant.SPACE_CHARACTER + activity.getResources().getText(R.string.register).toString());
		}
		else if(mode == MODE_DELETE_SENSOR) {
			titleView.setText(title + UIConstant.SPACE_CHARACTER + activity.getResources().getText(R.string.delete).toString());
		}
		else {
			titleView.setText(title + UIConstant.SPACE_CHARACTER + getResources().getString(R.string.insert_register_or_modify));
		}
		mBuilder.setView(view);

		return mBuilder.create();
	}

	private void displayData(int mode) {
		switch (mode) {
		case MODE_RELEASE_LOCKER:
			descriptionView.setText(R.string.confirm_system_unlock);
			break;
		case MODE_SYSTEM_RESET:
			descriptionView.setText(R.string.confirm_initialize_system);
			break;
		case MODE_REGISTER_FIRE_SENSOR:
		case MODE_REGISTER_GAS_SENSOR:
		case MODE_REGISTER_ACTIVITY_SENSOR:
		case MODE_REGISTER_DOOR_SENSOR:
		case MODE_REGISTER_EMERGENCY:
		case MODE_REGISTER_GAS_BREAKER:
			descriptionView.setText(title + activity.getResources().getText(R.string.confirm_register).toString());
			break;
		case MODE_DELETE_SENSOR:
			descriptionView.setText(title + activity.getResources().getText(R.string.confirm_delete).toString());
			break;
		case MODE_COME_CARE:
		case MODE_OUT_CARE:
			descriptionView.setText(title + UIConstant.SPACE_CHARACTER + activity.getResources().getText(R.string.confirm_register2).toString());
			break;
		case MODE_APPLICATION_UPDATE:
			descriptionView.setText(R.string.confirm_update_application);
			break;
		case MODE_ZIGBEE_UPDATE:
			descriptionView.setText(R.string.confirm_update_zigbee);
			break;
		case MODE_OPENNING:
			descriptionView.setText(R.string.openning_ment);
			break;
		case MODE_BLUETOOTH_ON:
			descriptionView.setText(R.string.comfirm_connect_bluetooth);
			break;
		}
	}

	private void registerSensor(int sensorType){
		Sensor sensor = null;
		
		customDialog = new ProgressDialogUI();
		customDialog.setTitle(title);
		customDialog.setMode(ProgressDialogUI.MODE_REGISTER);
		customDialog.setHandler(mHandler);
		customDialog.show(activity.getFragmentManager(), "Dialog");
		
		SensorRegisterFunction register = SmartCareSystemApplication.getInstance().getSensorRegisterFunc();
		register.addEventListener(mHandler);
		
		ConnectDB con = ConnectDB.getInstance(activity);
		SensorDeviceManager sensorDeviceMgr = SmartCareSystemApplication.getInstance().getSensorDeviceManager();
		
		// DB 검색

		int empty_idx = sensorDeviceMgr.getEmptySensorIndex(sensorType);
		sensorIndex = empty_idx;
		if (empty_idx > 0) {
			sensor = new Sensor();
			// sensor.setType((int)SensorID.FIRE_SENSOR_TYPE);
			sensor.setSensor_id(new Long(SensorID.getSensorId(sensorType, empty_idx)));
			List<Sensor> result = con.selectSensor(sensor);

			if (result.size() == 0) {
				register.registerSensor(sensor.getSensor_id().intValue(), SmartCareSystemApplication.getInstance().getMyPhoneNumber());
			} else {
				Toast.makeText(activity, UIConstant.ALREADY_REGISTERED, Toast.LENGTH_SHORT).show();
				customDialog.dismiss();
				return;
			}
		} else {
			Toast.makeText(activity, UIConstant.FIRE_SENSOR + UIConstant.NO_LONGER_SENSIR_REGISTER, Toast.LENGTH_SHORT).show();
			customDialog.dismiss();
			return;
		}
		
		timer = CustomCountDownTimer.getInstance(UIConstant.DEFAULT_TIMER_TIME * 1000, 1000, customDialog, activity, true);
		timer.setSendMsg(title);
		timer.setMode(CustomCountDownTimer.SENSOR_REGISTER_MODE);
		timer.setSensorId(sensor.getSensor_id().intValue());
		timer.setSensorNum(sensorIndex);
		timer.start();
		
	}
	private void updateData(int mode) {
		if(mode == MODE_RELEASE_LOCKER) {
			Intent intent = new Intent();
			intent.setClassName("com.android.settings", "com.android.settings.ChooseLockGeneric");
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
		}
		else if(mode == MODE_SYSTEM_RESET) {
			SystemSettingUI.doNextDialog = true;

			if (stepThread != null) {
				stepThread.interrupt();
			}
		}
		else if(mode == MODE_REGISTER_FIRE_SENSOR) {
			registerSensor(SensorID.FIRE_SENSOR_TYPE.getSensorType());
		}
		else if(mode == MODE_REGISTER_GAS_SENSOR) {
			registerSensor(SensorID.GAS_SENSOR_TYPE.getSensorType());
		}
		else if(mode == MODE_REGISTER_ACTIVITY_SENSOR) {
			registerSensor(SensorID.ACTIVITY_SENSOR_TYPE.getSensorType());
		}
		else if(mode == MODE_REGISTER_DOOR_SENSOR) {
			registerSensor(SensorID.DOOR_SENSOR_TYPE.getSensorType());
		}
		else if(mode == MODE_REGISTER_EMERGENCY) {
			registerSensor(SensorID.CALL_EMERGENCY_SENSOR_TYPE.getSensorType());
		}
		else if(mode == MODE_REGISTER_GAS_BREAKER) {
			registerSensor(SensorID.GAS_BREAKER_TYPE.getSensorType());
		}
		else if(mode == MODE_DELETE_SENSOR) {
			customDialog = new ProgressDialogUI();
			customDialog.setTitle(title);
			customDialog.setMode(ProgressDialogUI.MODE_DELETE);
			customDialog.setHandler(mHandler);
			customDialog.show(activity.getFragmentManager(), "Dialog");
			
			SensorRegisterFunction register = SmartCareSystemApplication.getInstance().getSensorRegisterFunc();
			register.addEventListener(mHandler);
			
			register.deregisterSensor(deleteSensorID);
			
			timer = CustomCountDownTimer.getInstance(UIConstant.DEFAULT_TIMER_TIME * 1000, 1000, customDialog, activity, true);
			timer.setSendMsg(title);
			timer.setMode(CustomCountDownTimer.SENSOR_DELETE_MODE);
			timer.setSensorId(deleteSensorID);
			timer.setSensorNum(sensorIndex);
			timer.start();
		}
		else if(mode == MODE_COME_CARE) {
			customDialog = new ProgressDialogUI();
			customDialog.setTitle(activity.getResources().getString(R.string.come_care));
			customDialog.setMode(ProgressDialogUI.MODE_REGISTER);
			customDialog.setHandler(mHandler);
			customDialog.show(getFragmentManager(), "Dialog");
			
			SensorRegisterFunction register = SmartCareSystemApplication.getInstance().getSensorRegisterFunc();
			register.addEventListener(mHandler);
			
			ConnectDB con = ConnectDB.getInstance(activity);
			SensorDeviceManager sensorDeviceMgr = SmartCareSystemApplication.getInstance().getSensorDeviceManager();
			
			Care care = new Care();
			
			int empty_idx = sensorDeviceMgr.getEmptySensorIndex(SensorID.CARE_SENSOR_TYPE.getSensorType());
			//if( empty_idx > 0 )
			{
				care.setSensor_ID(new Long( SensorID.getSensorId(SensorID.CARE_SENSOR_TYPE.getSensorType(), empty_idx)));
				register.registerSensor(care.getSensor_ID().intValue(), null);
				
			}
//			else {
//				Toast.makeText(mContext, UIConstant.CARE_SENSOR + UIConstant.NO_LONGER_SENSIR_REGISTER, Toast.LENGTH_SHORT).show();
//				customDialog.dismiss();
//				return;
//			}
			
			timer = CustomCountDownTimer.getInstance(UIConstant.DEFAULT_TIMER_TIME * 1000, 1000, customDialog, activity, true);
			timer.setSendMsg(activity.getResources().getString(R.string.come_care));
			timer.setMode(CustomCountDownTimer.SENSOR_REGISTER_MODE);
			timer.setSensorId(care.getSensor_ID().intValue());
			timer.start();
		}
		else if(mode == MODE_OUT_CARE) {
			customDialog = new ProgressDialogUI();
			customDialog.setTitle(activity.getResources().getString(R.string.out_care));
			customDialog.setMode(ProgressDialogUI.MODE_OUT_VISIT_REGISTER);
			customDialog.setHandler(mHandler);
			customDialog.show(getFragmentManager(), "Dialog");
			
			SensorRegisterFunction register = SmartCareSystemApplication.getInstance().getSensorRegisterFunc();
			register.addEventListener(mHandler);
			
			int sensor_id = new Long( SensorID.getSensorId(SensorID.CARE_SENSOR_TYPE.getSensorType(), 1)).intValue();
			register.deregisterSensor(sensor_id);
			
			timer = CustomCountDownTimer.getInstance(UIConstant.DEFAULT_TIMER_TIME * 1000, 1000, customDialog, activity, true);
			timer.setSendMsg(activity.getResources().getString(R.string.out_care));
			timer.setMode(CustomCountDownTimer.CARE_SENSOR_OUT_COME_MODE);
			timer.setSensorId(sensor_id);
			timer.start();
		}
		else if(mode == MODE_APPLICATION_UPDATE) {
			Context mContext = SmartCareSystemApplication.getInstance().getApplicationContext();
			PackageInfo i = null;
			try {
				i = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0);
			} catch (NameNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			String version = i.versionName;
			
			//http://222.122.128.45:8080/firmware/validate/gateway/version/update/1.0.2			
			String url = "http://222.122.128.45:8080/firmware/validate/gateway/version/update";
			String apk_version = version;			
			String req_url = url + "/" + apk_version;			
			
			String content = HttpRequestHelper.getInstance().urlrequest(req_url);
			
			if( content == null || content.isEmpty())
			{
				
				ResultDialogUI dialog = ResultDialogUI.newInstance(ResultDialogUI.MODE_CHECK_ERROR);
				dialog.setTitle(title);
				dialog.show(activity.getFragmentManager(), "Dialog");
				
				dismiss();
				return;
			}	
						
			else if( !content.equals("true"))
			{
				LogMgr.d(LogMgr.TAG,"Android firmware's current veresion is latest");
				
				ResultDialogUI dialog = ResultDialogUI.newInstance(ResultDialogUI.MODE_LATEST_ZIGBEE_VERSION);
				dialog.setTitle(title);
				dialog.show(activity.getFragmentManager(), "Dialog");				
				
				dismiss();
				return;
			}
			
			
			if(Constant.RUN_FOR_BMT)
			{
				/*ProgressDialogUI progressDialog = ProgressDialogUI.createInstance();
				progressDialog.init();
				progressDialog.setMode(ProgressDialogUI.MODE_APP_UPDATE_PROGRESS);
				progressDialog.setTitle("애플리케이션 업데이트");
				progressDialog.show(activity.getFragmentManager(), "Dialog");
				
				dismiss();
				return;*/
				LogMgr.d(LogMgr.TAG,"Firmware UI start activity");
				Context context = SmartCareSystemApplication.getInstance().getApplicationContext();
				Intent intent = new Intent(context, AppdownloadUI.class);
				intent.setAction("localUpdate");
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(intent);
				//new AsyncFileDownloader(activity.getApplicationContext()).download(Constant.FW_DOWNLOAD_PATH, Constant.FW_APP_PATH, fileDownCallback);
			}
			else
			{
				//애플리케이션 업그레이드 '확인' 버튼을 누른 경우
				SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.START_APPLICATION_UPDATE, TextToSpeech.QUEUE_FLUSH, null);
				LogMgr.d(LogMgr.TAG,"APP_UPDATE_START");
				//시스템 초기화시 본체 RESET 을 해야 FTDI 가 재연결됨.
				if(SmartCareSystemApplication.getInstance().getMessageManager().ftdi_isConnect())
				{
					SmartCareSystemApplication.getInstance().getGlobalMsgFunc().sendGateWayMsg((byte)MessageID.CODI_RESET);
				}
				updateAPK();
			}

		}
		else if(mode == MODE_ZIGBEE_UPDATE) {
			Context mContext = SmartCareSystemApplication.getInstance().getApplicationContext();
			SmartCarePreference pref = new SmartCarePreference(mContext, SmartCarePreference.USER_SETTING_INFO_FILENAME);
			String version = pref.getStringValue(SmartCarePreference.ZIGBEE_VER, UIConstant.ZIGBEE_DEFAULT_VER);
			//http://222.122.128.45:8080/firmware/validate/zigbee/version/update/2.0.0/JN_5168
			String url = "http://222.122.128.45:8080/firmware/validate/zigbee/version/update";
			String codi_version = version;
			String chip_id ="JN_5168";
			String req_url = url + "/" + codi_version + "/" + chip_id;			
			
			String tel_number = SmartCareSystemApplication.getInstance().getMyPhoneNumber();
			String content = HttpRequestHelper.getInstance().urlrequest(req_url);
			
			if( content == null || content.isEmpty())
			{				
				ResultDialogUI dialog = ResultDialogUI.newInstance(ResultDialogUI.MODE_CHECK_ERROR);
				dialog.setTitle(title);
				dialog.show(activity.getFragmentManager(), "Dialog");
				
				dismiss();
				return;
			}			
			else if( !content.equals("true"))
			{
				LogMgr.d(LogMgr.TAG,"Codi firmware's current veresion is latest");
				
				ResultDialogUI dialog = ResultDialogUI.newInstance(ResultDialogUI.MODE_LATEST_ZIGBEE_VERSION);
				dialog.setTitle(title);
				dialog.show(activity.getFragmentManager(), "Dialog");				
				
				dismiss();
				
				return;
			}
			
			if(Constant.RUN_FOR_BMT )
			{
				File file = new File(Constant.FIRMWARE_PATH);
				if(!file.exists())
					file.mkdirs();
				new AsyncFileDownloader(activity.getApplicationContext()).download(Constant.ZIGBEE_FW_DOWNLOAD_PATH + "/" + tel_number, Constant.FW_GATEWAY_PATH, fileDownCallback);
				/*ProgressDialogUI progressDialog = ProgressDialogUI.createInstance();
				progressDialog.init();
				progressDialog.setMode(ProgressDialogUI.MODE_CODI_UPDATE_PROGRESS);
				progressDialog.setTitle("본체 업데이트");				
				progressDialog.show(activity.getFragmentManager(), "Dialog");
				
				dismiss();*/
			}
			else
			{

			}
		}
		else if(mode == MODE_OPENNING){
			TelephonyManager tm = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE); 
			ConnectivityManager connMgr = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
			// 유심이 없는 경우..
			if (tm.getSimState() == TelephonyManager.SIM_STATE_ABSENT) {
				Toast.makeText(getActivity(), "USIM이 인식되지 않았습니다. \nUSIM의 상태를 확인해 주세요.", Toast.LENGTH_LONG).show();
				dismiss();
				getActivity().finish();
			// 3G 상태가 불량일 경우..
			} else if(!mobile.isAvailable()) {
				Toast.makeText(getActivity(), "데이터 통신상태가 좋지 않습니다. \n데이터 통신상태를 확인해 주세요.", Toast.LENGTH_LONG).show();
				dismiss();
				getActivity().finish();
			}else{			
				SmartCareSystemApplication.getInstance().getGlobalMsgFunc().startOpenning(new EventRegistration(mcallbackEvent));					
			}
		}
		else if(mode == MODE_BLUETOOTH_ON) {
			Intent intent = new Intent(activity, PulseSensorUI.class);
			intent.putExtra(UIConstant.SENSOR_TYPE, title);	
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
		}
		dismiss();
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setMode(int mode) {
		this.mode = mode;
	}

	public void setDialogStepThread(DialogStep stepThread) {
		this.stepThread = stepThread;
	}
	
	public void setDeleteSensorID(int deleteSensorID) {
		this.deleteSensorID = deleteSensorID;
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_ok:
			updateData(mode);
			break;
		case R.id.btn_cancel:
			dismiss();
			if(mode == MODE_OPENNING)
				getActivity().finish();
			break;
		}
	}

	public void setSensorNum(int sensorIndex) {
		// TODO Auto-generated method stub
		this.sensorIndex = sensorIndex;
	}
	
	public void setEventCallBack(CallbackEvent callback){
		mcallbackEvent = callback;
	}
	
	private AsyncCallback<File> fileDownCallback = new AsyncCallback.Base<File>() {
		@Override
		public void onResult(File result) {
			LogMgr.d("APP_UPDATE","APP_UPDATE_DOWNLOAD_END");

			LogMgr.d(LogMgr.TAG, "ConfirmDialogUI.java:enclosing_method:// result.getAbsolutePath() : " + result.getAbsolutePath());
			LogMgr.d(LogMgr.TAG, "ConfirmDialogUI.java:enclosing_method:// result.length() : " + result.length());
			
			if( result!=null && Constant.FW_GATEWAY_PATH.equals(result.getAbsolutePath()))
			{				
				SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.START_CODI_FIRMWARE_UPDATE, TextToSpeech.QUEUE_FLUSH, null);
				
				LogMgr.d(LogMgr.TAG,"UpgradeController upgrade start");
				//펌웨어 업데이트 진행.
				UpgradeController.getInstance().upgrade(UpgradeController.CODI_FIRMWARE_UPDATE_TYPE);
				
			}
			else //application update
			{
				// 애플리케이션 업그레이드 '확인' 버튼을 누른 경우
				SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.START_APPLICATION_UPDATE, TextToSpeech.QUEUE_FLUSH, null);
				
				if (SmartCareSystemApplication.getInstance().getMessageManager().ftdi_isConnect() ||
						SmartCareSystemApplication.getInstance().getMessageManager().ftdiController_isConnect()) {
					SmartCareSystemApplication.getInstance().getGlobalMsgFunc().sendGateWayMsg((byte) MessageID.CODI_RESET);
				}
				updateAPK();
										
			}

			
		}

		@Override
		public void exceptionOccured(Exception e) {
			e.printStackTrace();
		}
	};
	
	private void updateAPK() {
		LogMgr.d("APP_UPDATE", "updateAPK.............");
		ApplicationManager am;
		try {
			am = new ApplicationManager(activity);
			am.installPackage(Constant.FW_APP_PATH);
		} catch (Exception e) {
			e.printStackTrace();
		}
		ActivityManager mgr = (ActivityManager) activity.getSystemService(Activity.ACTIVITY_SERVICE);
		mgr.killBackgroundProcesses(activity.getPackageName());
		Log.d("APP_UPDATE",activity.getPackageName());
		//android.os.Process.killProcess(android.os.Process.myPid());
		
		
	}
}
