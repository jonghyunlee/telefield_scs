package com.telefield.smartcaresystem.ui.common;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.NumberPicker.OnValueChangeListener;
import android.widget.TextView;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.ui.UIConstant;
import com.telefield.smartcaresystem.util.SmartCarePreference;

public class ThreeNumberPickerDialogUI extends DialogFragment implements OnValueChangeListener, OnClickListener {
	SmartCarePreference pref;
	Context mContext;
	NumberPicker theFirstpicker, theSecondpicker, theThirdpicker;	//theFirstpicker : 1의 자리, theSecondpicker : 10의 자리, theThirdpicker : 100의 자리
	IDialogChangeListener listener ;
	
	String title;
	int mode;
	
	public static final int MODE_NON_ACTIVITY_TIME = 1;		//미감지 시간 설정(활동센서 설정)
	public static final int MODE_REPORT_TIME_PERIOD = 2;	//주기 보고 시간(서버 주기보고 시간 설정)
	
	public static ThreeNumberPickerDialogUI newInstance() {
		ThreeNumberPickerDialogUI f = new ThreeNumberPickerDialogUI();
		return f;		
	}
	
	public static ThreeNumberPickerDialogUI newInstance(IDialogChangeListener listener) {
		ThreeNumberPickerDialogUI f = new ThreeNumberPickerDialogUI();
		f.setDialogChangeListener(listener);
		return f;		
	}
	
	public static ThreeNumberPickerDialogUI newInstance(IDialogChangeListener listener, int mode) {
		ThreeNumberPickerDialogUI f = new ThreeNumberPickerDialogUI();
		f.setDialogChangeListener(listener);
		f.setMode(mode);
		return f;		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	    if (getDialog() != null) {
	        getDialog().setCanceledOnTouchOutside(false);
	    }
	    return super.onCreateView(inflater, container, savedInstanceState);
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		mContext = getActivity().getApplicationContext();
		
		String value = null;
		
		if(mode == MODE_NON_ACTIVITY_TIME) {
			pref = new SmartCarePreference(mContext, SmartCarePreference.SENSOR_SETTING_INFO_FILENAME);
			
			value = Integer.toString(pref.getIntValue(SmartCarePreference.NON_ACTIVITY_TIME, UIConstant.DEFAULT_NON_ACTIVITY_TIME_VALUE));
		}
		else if(mode == MODE_REPORT_TIME_PERIOD) {
			pref = new SmartCarePreference(mContext, SmartCarePreference.SERVER_SETTING_INFO_FILENAME);
			
			value = Integer.toString(pref.getIntValue(SmartCarePreference.SERVER_PERIODIC_REPORT_TIME, UIConstant.DEFAULT_PERIODIC_SEND_TIME_VALUE));
		}
		
		AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
		LayoutInflater mLayoutInflater = getActivity().getLayoutInflater();
		
		View view = mLayoutInflater.inflate(R.layout.three_number_picker_dialog, null);
		
		Button btnOK = (Button) view.findViewById(R.id.btn_ok);
		Button btnCancel = (Button) view.findViewById(R.id.btn_cancel);
		TextView titleView = (TextView) view.findViewById(R.id.title_view);
		titleView.setText(title + UIConstant.SPACE_CHARACTER + getResources().getString(R.string.insert_register_or_modify));
		
		btnOK.setOnClickListener(this);
		btnCancel.setOnClickListener(this);
		
		TextView explanation = (TextView) view.findViewById(R.id.detailTextView);
		explanation.setText(R.string.minute);
		
		theFirstpicker = (NumberPicker) view.findViewById(R.id.numberPicker_the_first);
		theSecondpicker = (NumberPicker) view.findViewById(R.id.NumberPicker_the_second);
		theThirdpicker = (NumberPicker) view.findViewById(R.id.numberPicker_the_third);
		
		theFirstpicker.setOnValueChangedListener(this);
		theSecondpicker.setOnValueChangedListener(this);
		theThirdpicker.setOnValueChangedListener(this);
		
		//최대/최소값 설정 및 저장된 값 출력
		theThirdpicker.setMaxValue(2);
		theThirdpicker.setMinValue(0);
		theSecondpicker.setMaxValue(9);
		theSecondpicker.setMinValue(0);
		theFirstpicker.setMaxValue(9);
		theFirstpicker.setMinValue(0);
		
		if(value != null) {
			if(value.length() == 3) {
				int hundredValue = Integer.parseInt(value.substring(0, 1));
				int tenValue = Integer.parseInt(value.substring(1, 2));
				theThirdpicker.setValue(hundredValue);
				theSecondpicker.setValue(tenValue);
				theFirstpicker.setValue(Integer.parseInt(value.substring(2)));
				
				if(hundredValue == 2) {
					theSecondpicker.setMaxValue(4);
				}
				if(hundredValue == 2 && tenValue == 4) {
					theFirstpicker.setMaxValue(0);
				}
			}
			else if(value.length() == 2) {
				theThirdpicker.setValue(0);
				theSecondpicker.setValue(Integer.parseInt(value.substring(0, 1)));
				theFirstpicker.setValue(Integer.parseInt(value.substring(1)));
			}
			else if(value.length() == 1) {
				theThirdpicker.setValue(0);
				theSecondpicker.setValue(0);
				theFirstpicker.setValue(Integer.parseInt(value));
			}
		}
		mBuilder.setView(view);
		
		return mBuilder.create();
	}
	
	@Override
	public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
		// TODO Auto-generated method stub
		switch(picker.getId()) {
			case R.id.numberPicker_the_third:
				if(newVal == 2) {
					theSecondpicker.setMaxValue(4);
				}
				else {
					theSecondpicker.setMaxValue(9);
				}
				
				if(newVal == 2 && theSecondpicker.getValue() == 4) {
					theFirstpicker.setMaxValue(0);
				}
				else {
					theFirstpicker.setMaxValue(9);
				}
				
				break;
			case R.id.NumberPicker_the_second:
				if(newVal == 4) {
					if(theThirdpicker.getValue() == 2) {
						theFirstpicker.setMaxValue(0);
					}
					else {
						theFirstpicker.setMaxValue(9);
					}
				}
				else {
					theFirstpicker.setMaxValue(9);
				}
		}
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
			case R.id.btn_ok:
				int value = (100 * theThirdpicker.getValue()) + (10 * theSecondpicker.getValue()) + (1 * theFirstpicker.getValue()); 
				
				if(mode == MODE_NON_ACTIVITY_TIME) {
					pref.setIntValue(SmartCarePreference.NON_ACTIVITY_TIME, value);
					SmartCareSystemApplication.getInstance().stopNonActivityReportTimer();
					SmartCareSystemApplication.getInstance().startNonActivityReportTimer();
				}
				else if(mode == MODE_REPORT_TIME_PERIOD) {
					pref.setIntValue(SmartCarePreference.SERVER_PERIODIC_REPORT_TIME, value);
					SmartCareSystemApplication.getInstance().stopPeriodReportTimer();
					SmartCareSystemApplication.getInstance().startPeriodReportTimer();
				}
				
				if(listener != null)
					listener.notifyChangeListener();
				
				dismiss();
				break;
			case R.id.btn_cancel:
				dismiss();
				break;
		}
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public void setMode(int mode) {
		this.mode = mode;
	}
	
	public void setDialogChangeListener(IDialogChangeListener listener) {
		this.listener = listener;
	}	
}
