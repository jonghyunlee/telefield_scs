package com.telefield.smartcaresystem.ui.common;

import java.io.IOException;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.ui.UIConstant;

public class ControlDialogUI extends DialogFragment implements OnClickListener {
	SeekBar volumeSeekBar;
	MediaPlayer mediaPlayer;
	AudioManager ring; 
	
	String title;
	int progress;
	int mode;
	boolean pressPositiveButton = false;
	private TextView titleView;
	private Button btnOK, btnCancel;
	
	public static final int MODE_RING = 1;			//벨 음량 설정(통화 설정)
	public static final int MODE_VOICE_CALL = 2;	//일반 통화음량 설정(통화 설정)
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	    if (getDialog() != null) {
	        getDialog().setCanceledOnTouchOutside(false);
	    }
	    return super.onCreateView(inflater, container, savedInstanceState);
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
		LayoutInflater mLayoutInflater = getActivity().getLayoutInflater();
		
		View view = mLayoutInflater.inflate(R.layout.custom_dialog_seekbar, null);
		volumeSeekBar = (SeekBar) view.findViewById(R.id.volumeSeekBar);
		
		btnOK = (Button) view.findViewById(R.id.btn_ok);
		btnCancel = (Button) view.findViewById(R.id.btn_cancel);
		titleView = (TextView) view.findViewById(R.id.title_view);

		btnOK.setOnClickListener(this);
		btnCancel.setOnClickListener(this);
		
		ring = (AudioManager) getActivity().getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
		
		int nMax = 0;
		int nCurrentVolumn = 0;
		
		if(mode == MODE_RING) {
			nMax = ring.getStreamMaxVolume(AudioManager.STREAM_RING);
			nCurrentVolumn = ring.getStreamVolume(AudioManager.STREAM_RING);
		}
		else if(mode == MODE_VOICE_CALL) {
			nMax = ring.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL);
			nCurrentVolumn = ring.getStreamVolume(AudioManager.STREAM_VOICE_CALL);
		}
		
		setRestoreProgress(nCurrentVolumn);
		
		volumeSeekBar.setMax(nMax);
		volumeSeekBar.setProgress(nCurrentVolumn);
		volumeSeekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				// TODO Auto-generated method stub
				if(mode == MODE_RING) {
					ring.setStreamVolume(AudioManager.STREAM_RING, progress, 0);
				}
				else if(mode == MODE_VOICE_CALL) {
					ring.setStreamVolume(AudioManager.STREAM_VOICE_CALL, progress, 0);
				}
				
				mediaPlayer.start();
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				if(mediaPlayer != null && !mediaPlayer.isPlaying())
					mediaPlayer.start();
			}
		});
		
		try {
			mediaPlayer = new MediaPlayer();
			//디바이스에 설정된 벨소리
			Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
			
			mediaPlayer.setDataSource(uri.toString());
			if(mode == MODE_RING) {
				mediaPlayer.setAudioStreamType(AudioManager.STREAM_RING);
			}
			else if(mode == MODE_VOICE_CALL) {
				mediaPlayer.setAudioStreamType(AudioManager.STREAM_VOICE_CALL);
			}
			
			mediaPlayer.setLooping(true);
			mediaPlayer.prepare();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		titleView.setText(getTitle() + UIConstant.SPACE_CHARACTER + getResources().getString(R.string.control));
		mBuilder.setView(view);	
		
		return mBuilder.create();
	}
	
	@Override
	public void onDismiss(DialogInterface dialog) {
		if(!pressPositiveButton) {
			if(mode == MODE_RING) {
				ring.setStreamVolume(AudioManager.STREAM_RING, progress, 0);	//볼륨 원래대로
			}
			else if(mode == MODE_VOICE_CALL) {
				ring.setStreamVolume(AudioManager.STREAM_VOICE_CALL, progress, 0);	//볼륨 원래대로
			}
		}
		pressPositiveButton = false;
		
		if(mediaPlayer != null && mediaPlayer.isPlaying()) {
			mediaPlayer.stop();
			mediaPlayer.release();
			mediaPlayer = null;
		}
		super.onDismiss(dialog);
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setRestoreProgress(int progress) {	//볼륨 복원 함수
		this.progress = progress;
	}
	
	public void setMode(int mode) {
		this.mode = mode;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_ok:
			pressPositiveButton = true;
			dismiss();
			break;
		case R.id.btn_cancel:
			if(mode == MODE_RING) {
				ring.setStreamVolume(AudioManager.STREAM_RING, progress, 0);	//볼륨 원래대로
			}
			else if(mode == MODE_VOICE_CALL) {
				ring.setStreamVolume(AudioManager.STREAM_VOICE_CALL, progress, 0);	//볼륨 원래대로
			}
			dismiss();
			break;
		}
	}
	
}
