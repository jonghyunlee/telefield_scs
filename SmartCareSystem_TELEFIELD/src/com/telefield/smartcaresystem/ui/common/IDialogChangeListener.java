package com.telefield.smartcaresystem.ui.common;

public interface IDialogChangeListener {
	
	public void notifyChangeListener();

}
