package com.telefield.smartcaresystem.ui.common;

import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.telefield.smartcaresystem.Constant;
import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.database.ConnectDB;
import com.telefield.smartcaresystem.database.DBInfo;
import com.telefield.smartcaresystem.database.Users;
import com.telefield.smartcaresystem.ui.UIConstant;
import com.telefield.smartcaresystem.ui.system.SystemSettingUI;
import com.telefield.smartcaresystem.ui.system.SystemSettingUI.DialogStep;
import com.telefield.smartcaresystem.util.SmartCarePreference;

public class InsertNormalDialogUI extends DialogFragment implements OnClickListener {
	SmartCarePreference pref;
	IDialogChangeListener listener;

	String title;
	int mode;
	DialogStep stepThread;

	private ConnectDB con;
	private Button btnOK, btnCancel;
	private TextView titleView, descriptionView;
	private EditText mEditText;

	public static final int MODE_USER_NAME = 1; // 사용자명(사용자 설정)
	public static final int MODE_119 = 2; // 119 소방센터
	public static final int MODE_CENTER = 3; // 지역센터
	public static final int MODE_HELPER = 4; // 보호자
	public static final int MODE_CARE1 = 5; // 돌보미1
	public static final int MODE_CARE2 = 6; // 돌보미2
	public static final int MODE_IPADDRESS = 7; // M2M Server IP(서버 설정)
	public static final int MODE_PORT = 8; // M2M Server Port(서버 설정)
	public static final int MODE_RESET_PASSWORD = 9; // 시스템 초기화(애플리케이션 설정)
	public static final int MODE_AUTO_RECEIVE_TIME = 10;	//자동착신 응답대기 시간 설정(통화 설정)
	public static final int MODE_START_ADDRESS = 11;	//시작 주소(시스템 펌웨어 설정(Hidden menu))
	public static final int MODE_ADDRESS_SIZE = 12;	//크기(시스템 펌웨어 설정(Hidden menu))
	public static final int MODE_PASSWORD_CHANGE = 13; //비밀번호 변경
	public static final int MODE_CALL_TIME_LIMIT = 14; //통화량 사용 제한
	public static InsertNormalDialogUI newInstance() {
		InsertNormalDialogUI f = new InsertNormalDialogUI();
		return f;
	}

	public static InsertNormalDialogUI newInstance(IDialogChangeListener listener) {
		InsertNormalDialogUI f = new InsertNormalDialogUI();
		f.setDialogChangeListener(listener);
		return f;
	}
	
	public static InsertNormalDialogUI newInstance(IDialogChangeListener listener, int mode) {
		InsertNormalDialogUI f = new InsertNormalDialogUI();
		f.setDialogChangeListener(listener);
		f.setMode(mode);
		return f;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		getDialog().getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_VISIBLE);
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (getDialog() != null) {
			getDialog().setCanceledOnTouchOutside(false);
		}
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		con = ConnectDB.getInstance(SmartCareSystemApplication.getInstance().getApplicationContext());

		AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
		LayoutInflater mLayoutInflater = getActivity().getLayoutInflater();

		View view = mLayoutInflater.inflate(R.layout.insert_dialog, null);

		mEditText = (EditText) view.findViewById(R.id.insertEditText);
		btnOK = (Button) view.findViewById(R.id.btn_ok);
		btnCancel = (Button) view.findViewById(R.id.btn_cancel);
		titleView = (TextView) view.findViewById(R.id.title_view);
		descriptionView = (TextView) view.findViewById(R.id.description_view);

		btnOK.setOnClickListener(this);
		btnCancel.setOnClickListener(this);
		displayData(mode);
		mEditText.setSelection(0, mEditText.getText().length());
		
		if(mode == MODE_RESET_PASSWORD) {
			titleView.setText(title);
		}
		else {
			titleView.setText(title + UIConstant.SPACE_CHARACTER + getResources().getString(R.string.insert_register_or_modify));
		}
		mBuilder.setView(view);

		return mBuilder.create();
	}

	private void displayData(int mode) {
		String data = "";
		List<Users> userList = null;
		switch (mode) {
		case MODE_USER_NAME:
			pref = new SmartCarePreference(getActivity().getApplicationContext(), SmartCarePreference.USER_SETTING_INFO_FILENAME);
			data = pref.getStringValue(SmartCarePreference.USER_NAME, SmartCarePreference.DEFAULT_COMMON_STRING);
			data = setBlankStringValueByCondition(data);
			descriptionView.setText(R.string.user_name_desc);
			break;
		case MODE_119:
			userList = con.selectUser(new Users(null, null, null, null, DBInfo.HELPER_TYPE_119));
			if (userList.size() > 0)
				data = userList.get(0).getNumber();
			data = setBlankStringValueByCondition(data);
			descriptionView.setText(R.string.emergency_number_desc);
			mEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
			break;
		case MODE_CENTER:
			userList = con.selectUser(new Users(null, null, null, null, DBInfo.HELPER_TYPE_CENTER));
			if (userList.size() > 0)
				data = userList.get(0).getNumber();
			data = setBlankStringValueByCondition(data);
			descriptionView.setText(R.string.center_number_desc);
			mEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
			break;
		case MODE_HELPER:
			userList = con.selectUser(new Users(null, null, null, null, DBInfo.HELPER_TYPE_HELPER));
			if (userList.size() > 0)
				data = userList.get(0).getNumber();
			data = setBlankStringValueByCondition(data);
			descriptionView.setText(R.string.helper_number_desc);
			mEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
			break;
		case MODE_CARE1:
			userList = con.selectUser(new Users(null, null, null, null, DBInfo.HELPER_TYPE_CARE1));
			if (userList.size() > 0)
				data = userList.get(0).getNumber();
			data = setBlankStringValueByCondition(data);
			descriptionView.setText(R.string.care1_number_desc);
			mEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
			break;
		case MODE_CARE2:
			userList = con.selectUser(new Users(null, null, null, null, DBInfo.HELPER_TYPE_CARE2));
			if (userList.size() > 0)
				data = userList.get(0).getNumber();
			data = setBlankStringValueByCondition(data);
			descriptionView.setText(R.string.care2_number_desc);
			mEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
			break;
		case MODE_IPADDRESS:
			pref = new SmartCarePreference(getActivity().getApplicationContext(), SmartCarePreference.ADDRESS_INFO_FILENAME);
			data = pref.getStringValue(SmartCarePreference.M2M_IP, UIConstant.DEFAULT_SERVER_IP);
			data = setBlankStringValueByCondition(data);
			descriptionView.setText(R.string.m2m_ip_desc);
			break;
		case MODE_PORT:
			pref = new SmartCarePreference(getActivity().getApplicationContext(), SmartCarePreference.ADDRESS_INFO_FILENAME);
			data = Integer.toString(pref.getIntValue(SmartCarePreference.M2M_PORT, UIConstant.DEFAULT_SERVER_PORT));
			descriptionView.setText(R.string.m2m_port_desc);
			mEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
			mEditText.setFilters(new InputFilter[] { new InputFilter.LengthFilter(6) });
			break;
		case MODE_RESET_PASSWORD:
			descriptionView.setText(R.string.initialize_system_message);
			mEditText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
			break;
		case MODE_AUTO_RECEIVE_TIME:
			pref = new SmartCarePreference(getActivity(), SmartCarePreference.CALL_SETTING_INFO_FILENAME);
			data = Integer.toString(pref.getIntValue(SmartCarePreference.AUTO_RECEIVING_TIME, UIConstant.DEFAULT_AUTO_RECEIVE_TIME_VALUE));
			mEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
			mEditText.setFilters(new InputFilter[] { new InputFilter.LengthFilter(2) });
			break;
		case MODE_START_ADDRESS:
			pref = new SmartCarePreference(getActivity().getApplicationContext(), SmartCarePreference.HIDDEN_MENU_SETTING_INFO_FILENAME);
			data = Integer.toString(pref.getIntValue(SmartCarePreference.START_ADDRESS, UIConstant.DEFAULT_START_ADDRESS));
			descriptionView.setText(R.string.start_address_desc);
			mEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
			break;
		case MODE_ADDRESS_SIZE:
			pref = new SmartCarePreference(getActivity().getApplicationContext(), SmartCarePreference.HIDDEN_MENU_SETTING_INFO_FILENAME);
			data = Integer.toString(pref.getIntValue(SmartCarePreference.ADDRESS_SIZE, UIConstant.DEFAULT_ADDRESS_SIZE));
			descriptionView.setText(R.string.address_size_desc);
			mEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
			break;
		case MODE_PASSWORD_CHANGE:
			btnOK.setEnabled(false);
			btnOK.setAlpha(70);
			mEditText.setHint("4글자 이상 숫자를 입력하세요.");
			mEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
			mEditText.addTextChangedListener(watcher);
			data = UIConstant.INIT_PASSWORD_NUM;
			descriptionView.setText(R.string.password_change);
			break;
		
		case MODE_CALL_TIME_LIMIT:
			pref = new SmartCarePreference(getActivity().getApplicationContext(), SmartCarePreference.CALL_SETTING_INFO_FILENAME);
			data = Integer.toString(pref.getIntValue(SmartCarePreference.CALL_LIMIT_TIME, 10));
			descriptionView.setText(R.string.call_limit);
			mEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
			break;
		}
		
		mEditText.setText(data);
	}

	private void updateData(int mode, String data) {
		switch (mode) {
		case MODE_USER_NAME:
			data = setDefaultStringValueByCondition(data);
			pref.setStringValue(SmartCarePreference.USER_NAME, data);
			break;
		case MODE_119:
			data = setDefaultStringValueByCondition(data);
			insertData(DBInfo.HELPER_TYPE_119, DBInfo.HELPER_NAME_119, data);
			break;
		case MODE_CENTER:
			data = setDefaultStringValueByCondition(data);
			insertData(DBInfo.HELPER_TYPE_CENTER, DBInfo.HELPER_NAME_CENTER, data);
			break;
		case MODE_HELPER:
			data = setDefaultStringValueByCondition(data);
			insertData(DBInfo.HELPER_TYPE_HELPER, DBInfo.HELPER_NAME_HELPER, data);
			break;
		case MODE_CARE1:
			data = setDefaultStringValueByCondition(data);
			insertData(DBInfo.HELPER_TYPE_CARE1, DBInfo.HELPER_NAME_CARE1, data);
			break;
		case MODE_CARE2:
			data = setDefaultStringValueByCondition(data);
			insertData(DBInfo.HELPER_TYPE_CARE2, DBInfo.HELPER_NAME_CARE2, data);
			break;
		case MODE_IPADDRESS:
			data = setDefaultStringValueByCondition(data);
			pref.setStringValue(SmartCarePreference.M2M_IP, data);
			if(!Constant.IS_RELEASE_VERSION)
				SmartCareSystemApplication.getInstance().updateProtocolManager();
			break;
		case MODE_PORT:
			if(data.equals(""))	//data가 공백문자인 상태에서 int type Preference에 저장하려고 하면 Exception이 발생하므로, 기본값을 넣는다.
				data = Integer.toString(UIConstant.DEFAULT_SERVER_PORT);
			pref.setIntValue(SmartCarePreference.M2M_PORT, Integer.parseInt(data));
			if(!Constant.IS_RELEASE_VERSION)
				SmartCareSystemApplication.getInstance().updateProtocolManager();
			break;
		case MODE_RESET_PASSWORD:
			if (UIConstant.INIT_SETTING_PASSWORD.equals(data)) {
				SystemSettingUI.doNextDialog = true;
			} else {
				Toast.makeText(SmartCareSystemApplication.getInstance().getApplicationContext(), R.string.invalid_password, Toast.LENGTH_SHORT).show();
			}

			if (stepThread != null) {
				stepThread.interrupt();
			}
			break;
		case MODE_AUTO_RECEIVE_TIME:
			pref.setIntValue(SmartCarePreference.AUTO_RECEIVING_TIME, Integer.parseInt(mEditText.getText().toString()));
			break;
		case MODE_START_ADDRESS:
			if(data.equals(""))	//data가 공백문자인 상태에서 int type Preference에 저장하려고 하면 Exception이 발생하므로, 기본값을 넣는다.
				data = Integer.toString(UIConstant.DEFAULT_START_ADDRESS);
			pref.setIntValue(SmartCarePreference.START_ADDRESS, Integer.parseInt(data));
			break;
		case MODE_ADDRESS_SIZE:
			if(data.equals(""))	//data가 공백문자인 상태에서 int type Preference에 저장하려고 하면 Exception이 발생하므로, 기본값을 넣는다.
				data = Integer.toString(UIConstant.DEFAULT_ADDRESS_SIZE);
			pref.setIntValue(SmartCarePreference.ADDRESS_SIZE, Integer.parseInt(data));
			break;
		case MODE_PASSWORD_CHANGE:
			UIConstant.INIT_PASSWORD_NUM = data;
			UIConstant.INIT_SETTING_PASSWORD = data;
			break;
		case MODE_CALL_TIME_LIMIT:
			data = mEditText.getText().toString();
			pref.setIntValue(SmartCarePreference.CALL_LIMIT_TIME, Integer.parseInt(data));
			break;
		}
	
		if (listener != null)
			listener.notifyChangeListener();

		dismiss();
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setMode(int mode) {
		this.mode = mode;
	}

	public void setDialogChangeListener(IDialogChangeListener listener) {
		this.listener = listener;
	}

	protected void insertData(String userType, String userName, String data) {
		List<Users> tempData = con.selectUser(new Users(null, null, null, null, userType));

		Users user = new Users();
		long id;
		if (tempData.size() == 1) {
			id = tempData.get(0).getId();
			user.setId(id);
		} else {
			// userType을 기준으로 DB에 1개 이상이 저장되어 있는 해당 userType을 가지고 있는 데이터를 모두 삭제
			con.deleteUser(new Users(null, userName, null, null, userType));
		}
		user.setType(userType);
		user.setNumber(data);
		user.setName(userName);
		con.insertUser(user);
	}

	public void setDialogStepThread(DialogStep stepThread) {
		this.stepThread = stepThread;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_ok:
			updateData(mode, mEditText.getText().toString());
			break;
		case R.id.btn_cancel:
			dismiss();
			break;
		}
	}
	
	//Preference의 기본 String은 공백문자로 보여주도록 한다.
	private String setBlankStringValueByCondition(String getData) {
		String returnData = getData;
		
		if(returnData.equals(SmartCarePreference.DEFAULT_COMMON_STRING))
			returnData = "";
		
		return returnData;
	}
	
	//공백 문자인 상태에서 확인 버튼을 누를 때 Preference의 기본 String으로 저장한다.
	private String setDefaultStringValueByCondition(String getData) {
		String returnData = getData;
		
		if(returnData.equals(""))
			returnData = SmartCarePreference.DEFAULT_COMMON_STRING;
		
		return returnData;
	}
	
	TextWatcher watcher = new TextWatcher() {
		
		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			// TODO Auto-generated method stub
			String string = new String(s.toString());
			string.contains("1");
			
			if(string.contains("7") || string.contains("8") || string.contains("9") ||string.contains("0") || string.contains(".") || string.contains("-") || string.contains(",")){	
				mEditText.getEditableText().delete(mEditText.getText().toString().length()-1, mEditText.getText().toString().length());
				Toast.makeText(SmartCareSystemApplication.getInstance().getApplicationContext(), "1~6사이의 숫자를 입력 하세요.", Toast.LENGTH_SHORT).show();	
			}
			
			if(mEditText.getText().toString().length() >= 4){
				btnOK.setEnabled(true);
				btnOK.setAlpha(100);
			}else{
				btnOK.setEnabled(false);
				btnOK.setAlpha(70);
			}
			
		}
		
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			
		}
	};
}
