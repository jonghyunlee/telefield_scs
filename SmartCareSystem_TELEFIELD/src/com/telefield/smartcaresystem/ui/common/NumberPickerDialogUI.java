package com.telefield.smartcaresystem.ui.common;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.NumberPicker.OnValueChangeListener;
import android.widget.TextView;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.ui.UIConstant;
import com.telefield.smartcaresystem.util.SmartCarePreference;

public class NumberPickerDialogUI extends DialogFragment implements OnClickListener {
	SmartCarePreference pref;
	Context mContext;
	NumberPicker picker;
	String[] displayValue;
	IDialogChangeListener listener;

	String title;
	int mode;
	int maxValue;
	int minValue;
	int currentValue;
	boolean valueChanged = false;
	private TextView titleView;
	private Button btnOK, btnCancel;

	public static final int MODE_PERIODIC_REPORT_TIME = 1; // 통신 주기 시간(센서 환경설정)
	public static final int MODE_NON_ACTIVITY_NUM = 2; // 미감지 횟수 설정(활동센서 설정)
	public static final int MODE_PERIOD_DAY = 3; //간격(일)(충전/방전 모드 시간 설정)
	public static final int MODE_PERFORM_TIME = 4; //수행 시간(충전/방전 모드 시간 설정)
	public static final int MODE_DETECT_ACTIVITY_TIME = 5; //활동량 감지 시간(활동센서 설정)

	public static NumberPickerDialogUI newInstance() {
		NumberPickerDialogUI f = new NumberPickerDialogUI();
		return f;
	}

	public static NumberPickerDialogUI newInstance(IDialogChangeListener listener) {
		NumberPickerDialogUI f = new NumberPickerDialogUI();
		f.setDialogChangeListener(listener);
		return f;
	}

	public static NumberPickerDialogUI newInstance(IDialogChangeListener listener, int mode) {
		NumberPickerDialogUI f = new NumberPickerDialogUI();
		f.setDialogChangeListener(listener);
		f.setMode(mode);
		return f;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (getDialog() != null) {
			getDialog().setCanceledOnTouchOutside(false);
		}
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		mContext = getActivity().getApplicationContext();
		
		if(mode == MODE_PERIODIC_REPORT_TIME || mode == MODE_NON_ACTIVITY_NUM || mode == MODE_DETECT_ACTIVITY_TIME) {
			pref = new SmartCarePreference(mContext, SmartCarePreference.SENSOR_SETTING_INFO_FILENAME);
		}
		else {	//여기서는 MODE_PERIOD_DAY, MODE_PERFORM_TIME
			pref = new SmartCarePreference(mContext, SmartCarePreference.APPLICATION_SETTING_INFO_FILENAME);
		}
		
		AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
		LayoutInflater mLayoutInflater = getActivity().getLayoutInflater();

		View view = mLayoutInflater.inflate(R.layout.number_picker_dialog, null);

		TextView explanation = (TextView) view.findViewById(R.id.detailTextView);

		btnOK = (Button) view.findViewById(R.id.btn_ok);
		btnCancel = (Button) view.findViewById(R.id.btn_cancel);
		titleView = (TextView) view.findViewById(R.id.title_view);

		btnOK.setOnClickListener(this);
		btnCancel.setOnClickListener(this);

		titleView.setText(title + UIConstant.SPACE_CHARACTER + getResources().getString(R.string.insert_register_or_modify));

		picker = (NumberPicker) view.findViewById(R.id.numberPicker);
		picker.setMaxValue(maxValue);
		picker.setMinValue(minValue);
		picker.setOnValueChangedListener(new OnValueChangeListener() {
			@Override
			public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
				// TODO Auto-generated method stub
				valueChanged = true;

				currentValue = newVal;
			}
		});

		if (mode == MODE_PERIODIC_REPORT_TIME) {
			picker.setValue(pref.getIntValue(SmartCarePreference.PERIODIC_REPORT_TIME, UIConstant.DEFAULT_PERIODIC_REPORT_TIME_VALUE));
			explanation.setText(R.string.minute);
		} else if (mode == MODE_NON_ACTIVITY_NUM) {
			picker.setValue(pref.getIntValue(SmartCarePreference.NON_ACTIVITY_NUM, UIConstant.DEFAULT_NON_ACTIVITY_NUM_VALUE));
			explanation.setText(R.string.num);
		} else if (mode == MODE_PERIOD_DAY) {
			picker.setValue(pref.getIntValue(SmartCarePreference.CHARGE_DISCHARGE_PERIOD_DAY, UIConstant.DEFAULT_CHARGE_DISCHARGE_PERIOD_DAY));
			explanation.setText(R.string.day);
		} else if (mode == MODE_PERFORM_TIME) {
			picker.setValue(pref.getIntValue(SmartCarePreference.CHARGE_DISCHARGE_PERFORM_TIME, UIConstant.DEFAULT_CHARGE_DISCHARGE_PERFORM_TIME));
			explanation.setText(R.string.hour);
		} else if (mode == MODE_DETECT_ACTIVITY_TIME) {
			picker.setValue(pref.getIntValue(SmartCarePreference.DETECT_ACTIVITY_TIME, UIConstant.DEFAULT_DETECT_ACTIVITY_TIME));
			explanation.setText(R.string.minute);
		}

		mBuilder.setView(view);

		return mBuilder.create();
	}

	public void setMaxValue(int maxValue) {
		this.maxValue = maxValue;
	}

	public void setMinValue(int minValue) {
		this.minValue = minValue;
	}

	public void setMode(int mode) {
		this.mode = mode;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setDialogChangeListener(IDialogChangeListener listener) {
		this.listener = listener;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_ok:
			if (valueChanged) {
				if (mode == MODE_PERIODIC_REPORT_TIME) {
					pref.setIntValue(SmartCarePreference.PERIODIC_REPORT_TIME, currentValue);
					SmartCareSystemApplication.getInstance().getGlobalMsgFunc().sendKeepAliveTimeMsg(currentValue);
				} else if (mode == MODE_NON_ACTIVITY_NUM) {
					pref.setIntValue(SmartCarePreference.NON_ACTIVITY_NUM, currentValue);
				} else if (mode == MODE_PERIOD_DAY) {
					pref.setIntValue(SmartCarePreference.CHARGE_DISCHARGE_PERIOD_DAY, currentValue);
				} else if (mode == MODE_PERFORM_TIME) {
					pref.setIntValue(SmartCarePreference.CHARGE_DISCHARGE_PERFORM_TIME, currentValue);
				} else if (mode == MODE_DETECT_ACTIVITY_TIME) {
					pref.setIntValue(SmartCarePreference.DETECT_ACTIVITY_TIME, currentValue);					
					SmartCareSystemApplication.getInstance().getMySmartCareState().setOutModeExpireTimerSet();
				}

				if (listener != null)
					listener.notifyChangeListener();
			}
			dismiss();
			break;
		case R.id.btn_cancel:
			dismiss();
			break;
		}
	}

}
