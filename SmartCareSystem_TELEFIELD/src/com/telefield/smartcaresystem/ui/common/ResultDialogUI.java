package com.telefield.smartcaresystem.ui.common;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.TextView;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.message.SensorID;
import com.telefield.smartcaresystem.ui.UIConstant;
import com.telefield.smartcaresystem.ui.sensor.SeeSensorGroupUI;
import com.telefield.smartcaresystem.ui.system.SystemSettingUI;
import com.telefield.smartcaresystem.util.ByteArrayToHex;
import com.telefield.smartcaresystem.util.LogMgr;
import com.telefield.smartcaresystem.util.SpeechUtil;

public class ResultDialogUI extends DialogFragment implements OnClickListener {
	IDialogChangeListener listener;
	int mode;
	int sensorIndex;
	private String title;
	private Activity activity;
	private Button btnOK;
	private TextView titleView, descriptionView;
	
	//센서 등록/삭제 결과에 필요한 요소들
	int sensorID;
	String sendMsg;
	byte[] macAddress;
	int version;
	public static final int MODE_REGISTER_SUCCESS = 1; // 등록 성공
	public static final int MODE_REGISTER_FAIL = 2; // 등록 실패
	public static final int MODE_DELETE_SUCCESS = 3; // 삭제 성공
	public static final int MODE_DELETE_FAIL = 4; // 삭제 실패
	public static final int MODE_COME_CARE_SUCCESS = 5; // 돌보미 방문 등록 성공
	public static final int MODE_OUT_CARE_SUCCESS = 6; // 돌보미 퇴실 등록 성공
	public static final int MODE_OUT_CARE_FAIL = 7; // 돌보미 퇴실 등록 실패
	public static final int MODE_INITIALIZE_SYSTEM_COMPLETE = 8; // 시스템 초기화 완료
	public static final int MODE_OPENNING_ERROR = 9; // 개통 전 M2M IP 체크
	public static final int MODE_LATEST_ZIGBEE_VERSION = 10; // 본체 버전이 최신인 경우
	public static final int MODE_CHECK_ERROR = 11; //update 체크 에러
	
	public ResultDialogUI(){
	}
	
	public static ResultDialogUI newInstance(int mode) {
		ResultDialogUI f = new ResultDialogUI();
		f.setMode(mode);
		return f;
	}
	
	public static ResultDialogUI newInstance(IDialogChangeListener listener, int mode) {
		ResultDialogUI f = new ResultDialogUI();
		f.setDialogChangeListener(listener);
		f.setMode(mode);
		return f;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		getDialog().getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_VISIBLE);
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (getDialog() != null) {
			getDialog().setCanceledOnTouchOutside(false);
		}
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		activity = getActivity();
		AlertDialog.Builder mBuilder = new AlertDialog.Builder(activity);
		LayoutInflater mLayoutInflater = activity.getLayoutInflater();

		View view = mLayoutInflater.inflate(R.layout.result_dialog, null);

		btnOK = (Button) view.findViewById(R.id.btn_ok);
		titleView = (TextView) view.findViewById(R.id.title_view);
		descriptionView = (TextView) view.findViewById(R.id.description_view);
		btnOK.setOnClickListener(this);
		displayData(mode);

		titleView.setText(title);
		
		mBuilder.setView(view);

		return mBuilder.create();
	}

	private void displayData(int mode) {
		String sensorTypeStr , displayMsg, macAddr;
		LogMgr.d(LogMgr.TAG, "ResultDialogUI.java:displayData:// mode : " + mode);

		switch(mode){
		case MODE_REGISTER_SUCCESS:
			sensorTypeStr = SensorID.getSensorName(sensorID);
			macAddr = ByteArrayToHex.byteArrayToHex(macAddress, macAddress.length);
			displayMsg = sensorTypeStr + sensorIndex + " " + getResources().getString(R.string.register_complete) + getResources().getString(R.string.registration_operator) + macAddr;

			descriptionView.setText(displayMsg);
			//Jong 14.10.30 코디는 등록할때 버전을 가지고오고, 센서는 가져오지 않는다.
			if(SensorID.CODI_SENSOR_TYPE.getSensorType() == SensorID.getSensorType(sensorID)){
				SmartCareSystemApplication.getInstance().getSensorDeviceManager().addSensor(sensorID, macAddr, version);
			}else{
				SmartCareSystemApplication.getInstance().getSensorDeviceManager().addSensor(sensorID, macAddr, 0);
			}
			speaking(sensorID, UIConstant.REGISTER_SUCCESS);
			break;
		case MODE_REGISTER_FAIL:
			sensorTypeStr = SensorID.getSensorName(sensorID);
			if(sensorID >= 0x0101 && sensorID <= 0x07ff)	//화재.가스.활동.도어센서, 응급호출기, 가스차단기
				displayMsg = sensorTypeStr + sensorIndex + " " + getResources().getString(R.string.register_fail);
			else	//돌보미
				displayMsg = sensorTypeStr + " " +getResources().getString(R.string.visit) + " " + getResources().getString(R.string.register_fail);
			descriptionView.setText(displayMsg);
			
			SmartCareSystemApplication.getInstance().getGlobalMsgFunc().cancelAddSensor(sensorID);
			
			if(sensorID >= 0x0101 && sensorID <= 0x07ff)
				speaking(sensorID, UIConstant.REGISTER_FAIL);
			else
				speaking(sensorID, UIConstant.VISIT_CARE_REGISTER_FAIL);
			break;
		case MODE_DELETE_SUCCESS:
			sensorTypeStr = SensorID.getSensorName(sensorID);
			macAddr = ByteArrayToHex.byteArrayToHex(macAddress, macAddress.length);
			displayMsg = sensorTypeStr + sensorIndex + " " + getResources().getString(R.string.delete_complete);

			descriptionView.setText(displayMsg);

			SmartCareSystemApplication.getInstance().getSensorDeviceManager().removeSensor(sensorID, macAddr);

			speaking(sensorID, UIConstant.DELETE_SUCCESS);
			break;
		case MODE_DELETE_FAIL:
			sensorTypeStr = sendMsg;
			displayMsg = sensorTypeStr + sensorIndex + " " + getResources().getString(R.string.delete_fail);
			descriptionView.setText(displayMsg);
			
			speaking(sensorID, UIConstant.DELETE_FAIL);
			break;
		case MODE_COME_CARE_SUCCESS:
			sensorTypeStr = SensorID.getSensorName(sensorID);
			macAddr = ByteArrayToHex.byteArrayToHex(macAddress, macAddress.length);
			displayMsg = sensorTypeStr + " " + getResources().getString(R.string.visit) + " " + getResources().getString(R.string.register_complete) + "\n" + macAddr;
			
			descriptionView.setText(displayMsg);
			
			SmartCareSystemApplication.getInstance().getSensorDeviceManager().visitCareSensor(sensorID, macAddr);
			
			speaking(sensorID, UIConstant.VISIT_CARE_REGISTER_SUCCESS);
			break;
		case MODE_OUT_CARE_SUCCESS:
			sensorTypeStr = SensorID.getSensorName(sensorID);
			macAddr = ByteArrayToHex.byteArrayToHex(macAddress, macAddress.length);
			displayMsg = sensorTypeStr + " " + getResources().getString(R.string.out) + " " + getResources().getString(R.string.register_complete) + "\n" + macAddr;
			
			descriptionView.setText(displayMsg);
			
			SmartCareSystemApplication.getInstance().getSensorDeviceManager().outCareSensor(sensorID, macAddr);
			
			speaking(sensorID, UIConstant.OUT_CARE_REGISTER_SUCCESS);
			break;
		case MODE_OUT_CARE_FAIL:
			sensorTypeStr = sendMsg;
			displayMsg = sensorTypeStr + " " + getResources().getString(R.string.register_fail);
			descriptionView.setText(displayMsg);
			
			if( sensorID != -1) {
				SmartCareSystemApplication.getInstance().getGlobalMsgFunc().cancelAddSensor(sensorID);
			}
			else
				LogMgr.e(LogMgr.TAG, "Error!!. sensor id wrong value ");
			
			speaking(sensorID, UIConstant.OUT_CARE_REGISTER_FAIL);
			break;
		case MODE_INITIALIZE_SYSTEM_COMPLETE:
			descriptionView.setText(R.string.initialize_complete);
			//사용자가 확인을눌러서 다른작업를 하지못하게 방지함.
			btnOK.setEnabled(false);
			break;
		case MODE_OPENNING_ERROR:
			descriptionView.setText(R.string.m2m_address_error);
			break;
		case MODE_LATEST_ZIGBEE_VERSION:
			descriptionView.setText(R.string.latest_version);
			break;
		case MODE_CHECK_ERROR:	
			descriptionView.setText(R.string.update_check_error);
			break;
			
		}
	}

	private void updateData(int mode) {
		switch(mode){
		case MODE_REGISTER_SUCCESS:
			break;
		case MODE_REGISTER_FAIL:
			break;
		case MODE_DELETE_SUCCESS:
			SeeSensorGroupUI.deleteSensor();
			break;
		case MODE_DELETE_FAIL:
			break;
		case MODE_COME_CARE_SUCCESS:
			break;
		case MODE_OUT_CARE_SUCCESS:
			break;
		case MODE_OUT_CARE_FAIL:
			break;
		case MODE_INITIALIZE_SYSTEM_COMPLETE:
			if(listener != null)
				listener.notifyChangeListener();
			
			break;
		case MODE_OPENNING_ERROR:
			startActivity(new Intent(SmartCareSystemApplication.getInstance().getApplicationContext(), SystemSettingUI.class));
			break;
		case MODE_LATEST_ZIGBEE_VERSION:
			break;
		case MODE_CHECK_ERROR:
			break;
		}
		
		
		dismiss();
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setMode(int mode) {
		this.mode = mode;
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_ok:
			updateData(mode);
			break;
		}
	}
	
	public void setSensorID(int sensorID) {
		this.sensorID = sensorID;
	}
	
	public void setSendMsg(String sendMsg) {
		this.sendMsg = sendMsg;
	}
	
	public void setMacAddress(byte[] macAddress) {
		this.macAddress = macAddress;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	
	public void setDialogChangeListener(IDialogChangeListener listener) {
		this.listener = listener;
	}
	
	public void speaking(int sensor_id, int result) {
		if(result == UIConstant.REGISTER_SUCCESS) {			
			SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.SENSOR_REGISTER_MODE,sensor_id, TextToSpeech.QUEUE_FLUSH, null);		
		}
		else if(result == UIConstant.DELETE_SUCCESS) {			
			SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.SENSOR_DELETE_MODE,sensor_id ,TextToSpeech.QUEUE_FLUSH, null);
		}
		else if(result == UIConstant.REGISTER_FAIL) {
			SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.SENSOR_REGISTER_FAIL_MODE,sensor_id ,TextToSpeech.QUEUE_FLUSH, null);
		}
		else if(result == UIConstant.DELETE_FAIL) {			
			SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.SENSOR_DELETE_FAIL_MODE,sensor_id ,TextToSpeech.QUEUE_FLUSH, null);
		}
		else if(result == UIConstant.VISIT_CARE_REGISTER_SUCCESS) {
			SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.CARE_COME_SUCCESS_MODE,sensor_id ,TextToSpeech.QUEUE_FLUSH, null);
		}
		else if(result == UIConstant.VISIT_CARE_REGISTER_FAIL) {			
			SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.CARE_COME_FAIL_MODE,sensor_id ,TextToSpeech.QUEUE_FLUSH, null);
		}
		else if(result == UIConstant.OUT_CARE_REGISTER_SUCCESS) {
			SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.CARE_OUT_SUCCESS_MODE,sensor_id ,TextToSpeech.QUEUE_FLUSH, null);
		}
		else if(result == UIConstant.OUT_CARE_REGISTER_FAIL) {
			SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.CARE_OUT_FAIL_MODE,sensor_id ,TextToSpeech.QUEUE_FLUSH, null);
		}
	}

	public void setSensorNum(int sensorIndex) {
		// TODO Auto-generated method stub
		this.sensorIndex = sensorIndex;
	}
	
}
