package com.telefield.smartcaresystem.ui.common;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.ui.UIConstant;
import com.telefield.smartcaresystem.util.SmartCarePreference;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class SimpleSelectDialogUI extends DialogFragment implements OnClickListener {
	IDialogChangeListener listener;
	SmartCarePreference pref;
	
	String title;
	int mode;
	
	public static final int MODE_PERIODIC_REPORT_TIME = 1; // 통신 주기 시간(센서 환경설정)
	public static final int MODE_NON_ACTIVITY_NUM = 2; // 미감지 횟수 설정(활동센서 설정)
	public static final int MODE_NON_ACTIVITY_TIME = 3;		//미감지 시간 설정(활동센서 설정)
	public static final int MODE_REPORT_TIME_PERIOD = 4;	//주기 보고 시간(서버 주기보고 시간 설정)
	public static final int MODE_PERIOD_DAY = 5;	//간격(일)(충전/방전 모드 시간 설정)
	public static final int MODE_PERFORM_TIME = 6;	//수행 시간(충전/방전 모드 시간 설정)
	
	public static SimpleSelectDialogUI newInstance() {
		SimpleSelectDialogUI f = new SimpleSelectDialogUI();
		return f;
	}

	public static SimpleSelectDialogUI newInstance(IDialogChangeListener listener) {
		SimpleSelectDialogUI f = new SimpleSelectDialogUI();
		f.setDialogChangeListener(listener);
		return f;
	}

	public static SimpleSelectDialogUI newInstance(IDialogChangeListener listener, int mode) {
		SimpleSelectDialogUI f = new SimpleSelectDialogUI();
		f.setDialogChangeListener(listener);
		f.setMode(mode);
		return f;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (getDialog() != null) {
			getDialog().setCanceledOnTouchOutside(false);
		}
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
		LayoutInflater mLayoutInflater = getActivity().getLayoutInflater();

		View view = mLayoutInflater.inflate(R.layout.simple_select_dialog, null);
		
		TextView titleView = (TextView) view.findViewById(R.id.title_view);
		titleView.setText(title + UIConstant.SPACE_CHARACTER + getResources().getString(R.string.insert_register_or_modify));
		
		int[] buttonIDs = {R.id.simpleButton1, R.id.simpleButton2, R.id.simpleButton3, R.id.simpleButton4, R.id.simpleButton5,
				R.id.simpleButton6, R.id.simpleButton7, R.id.simpleButton8, R.id.simpleButton9, R.id.btn_cancel};
		
		for(int i=0;i<buttonIDs.length;i++) {
			Button button = (Button) view.findViewById(buttonIDs[i]);
			button.setOnClickListener(this);
			
			setButtonText(button);
		}
		
		mBuilder.setView(view);
		
		return mBuilder.create();
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()) {
			case R.id.simpleButton1:
			case R.id.simpleButton2:
			case R.id.simpleButton3:
			case R.id.simpleButton4:
			case R.id.simpleButton5:
			case R.id.simpleButton6:
			case R.id.simpleButton7:
			case R.id.simpleButton8:
				setValue(v.getId());
				
				if(listener != null)
					listener.notifyChangeListener();
				
				dismiss();
				break;
			case R.id.simpleButton9:
				dismiss();
				
				if(mode == MODE_PERIODIC_REPORT_TIME) {
					NumberPickerDialogUI dialog = NumberPickerDialogUI.newInstance(listener, NumberPickerDialogUI.MODE_PERIODIC_REPORT_TIME);
					dialog.setTitle(title);
					dialog.setMaxValue(60);
					dialog.setMinValue(1);
					dialog.show(getFragmentManager(), "Dialog");
				}
				else if(mode == MODE_NON_ACTIVITY_NUM) {
					NumberPickerDialogUI dialog = NumberPickerDialogUI.newInstance(listener, NumberPickerDialogUI.MODE_NON_ACTIVITY_NUM);
					dialog.setTitle(title);
					dialog.setMaxValue(10);
					dialog.setMinValue(1);
					dialog.show(getFragmentManager(), "Dialog");
				}
				else if(mode == MODE_NON_ACTIVITY_TIME) {
					ThreeNumberPickerDialogUI dialog = ThreeNumberPickerDialogUI.newInstance(listener, ThreeNumberPickerDialogUI.MODE_NON_ACTIVITY_TIME);
					dialog.setTitle(title);
					dialog.show(getFragmentManager(), "Dialog");
				}
				else if(mode == MODE_REPORT_TIME_PERIOD) {
					ThreeNumberPickerDialogUI dialog = ThreeNumberPickerDialogUI.newInstance(listener, ThreeNumberPickerDialogUI.MODE_REPORT_TIME_PERIOD);
					dialog.setTitle(title);
					dialog.show(getFragmentManager(), "Dialog");
				}
				else if(mode == MODE_PERIOD_DAY) {
					NumberPickerDialogUI dialog = NumberPickerDialogUI.newInstance(listener, NumberPickerDialogUI.MODE_PERIOD_DAY);
					dialog.setTitle(title);
					dialog.setMaxValue(10);
					dialog.setMinValue(1);
					dialog.show(getFragmentManager(), "Dialog");
				}
				else if(mode == MODE_PERFORM_TIME) {
					NumberPickerDialogUI dialog = NumberPickerDialogUI.newInstance(listener, NumberPickerDialogUI.MODE_PERFORM_TIME);
					dialog.setTitle(title);
					dialog.setMaxValue(23);
					dialog.setMinValue(0);
					dialog.show(getFragmentManager(), "Dialog");
				}
				
				break;
			case R.id.btn_cancel:
				dismiss();
				break;
		}
	}
	
	public void setMode(int mode) {
		this.mode = mode;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setDialogChangeListener(IDialogChangeListener listener) {
		this.listener = listener;
	}
	
	public void setButtonText(Button button) {
		switch(mode) {
			case MODE_PERIODIC_REPORT_TIME:
				if(button.getId() == R.id.simpleButton1) {
					button.setText("10분");
				}
				else if(button.getId() == R.id.simpleButton2) {
					button.setText("15분");
				}
				else if(button.getId() == R.id.simpleButton3) {
					button.setText("20분");
				}
				else if(button.getId() == R.id.simpleButton4) {
					button.setText("25분");
				}
				else if(button.getId() == R.id.simpleButton5) {
					button.setText("30분");
				}
				else if(button.getId() == R.id.simpleButton6) {
					button.setText("40분");
				}
				else if(button.getId() == R.id.simpleButton7) {
					button.setText("50분");
				}
				else if(button.getId() == R.id.simpleButton8) {
					button.setText("60분");
				}
				
				break;
			case MODE_NON_ACTIVITY_NUM:
				if(button.getId() == R.id.simpleButton1) {
					button.setText("1회");
				}
				else if(button.getId() == R.id.simpleButton2) {
					button.setText("2회");
				}
				else if(button.getId() == R.id.simpleButton3) {
					button.setText("3회");
				}
				else if(button.getId() == R.id.simpleButton4) {
					button.setText("5회");
				}
				else if(button.getId() == R.id.simpleButton5) {
					button.setText("7회");
				}
				else if(button.getId() == R.id.simpleButton6) {
					button.setText("8회");
				}
				else if(button.getId() == R.id.simpleButton7) {
					button.setText("9회");
				}
				else if(button.getId() == R.id.simpleButton8) {
					button.setText("10회");
				}
				break;
			case MODE_NON_ACTIVITY_TIME:
				if(button.getId() == R.id.simpleButton1) {
					button.setText("30분");
				}
				else if(button.getId() == R.id.simpleButton2) {
					button.setText("60분");
				}
				else if(button.getId() == R.id.simpleButton3) {
					button.setText("90분");
				}
				else if(button.getId() == R.id.simpleButton4) {
					button.setText("120분");
				}
				else if(button.getId() == R.id.simpleButton5) {
					button.setText("150분");
				}
				else if(button.getId() == R.id.simpleButton6) {
					button.setText("180분");
				}
				else if(button.getId() == R.id.simpleButton7) {
					button.setText("210분");
				}
				else if(button.getId() == R.id.simpleButton8) {
					button.setText("240분");
				}
				break;
			case MODE_REPORT_TIME_PERIOD:
				if(button.getId() == R.id.simpleButton1) {
					button.setText("30분");
				}
				else if(button.getId() == R.id.simpleButton2) {
					button.setText("60분");
				}
				else if(button.getId() == R.id.simpleButton3) {
					button.setText("90분");
				}
				else if(button.getId() == R.id.simpleButton4) {
					button.setText("120분");
				}
				else if(button.getId() == R.id.simpleButton5) {
					button.setText("150분");
				}
				else if(button.getId() == R.id.simpleButton6) {
					button.setText("180분");
				}
				else if(button.getId() == R.id.simpleButton7) {
					button.setText("210분");
				}
				else if(button.getId() == R.id.simpleButton8) {
					button.setText("240분");
				}
				break;
			case MODE_PERIOD_DAY:
				if(button.getId() == R.id.simpleButton1) {
					button.setText("1일");
				}
				else if(button.getId() == R.id.simpleButton2) {
					button.setText("2일");
				}
				else if(button.getId() == R.id.simpleButton3) {
					button.setText("3일");
				}
				else if(button.getId() == R.id.simpleButton4) {
					button.setText("5일");
				}
				else if(button.getId() == R.id.simpleButton5) {
					button.setText("7일");
				}
				else if(button.getId() == R.id.simpleButton6) {
					button.setText("8일");
				}
				else if(button.getId() == R.id.simpleButton7) {
					button.setText("9일");
				}
				else if(button.getId() == R.id.simpleButton8) {
					button.setText("10일");
				}
				break;
			case MODE_PERFORM_TIME:
				if(button.getId() == R.id.simpleButton1) {
					button.setText("0시");
				}
				else if(button.getId() == R.id.simpleButton2) {
					button.setText("3시");
				}
				else if(button.getId() == R.id.simpleButton3) {
					button.setText("6시");
				}
				else if(button.getId() == R.id.simpleButton4) {
					button.setText("9시");
				}
				else if(button.getId() == R.id.simpleButton5) {
					button.setText("12시");
				}
				else if(button.getId() == R.id.simpleButton6) {
					button.setText("15시");
				}
				else if(button.getId() == R.id.simpleButton7) {
					button.setText("18시");
				}
				else if(button.getId() == R.id.simpleButton8) {
					button.setText("21시");
				}
				break;
		}
	}
	
	public void setValue(int buttonId) {
		int value = 0;
		
		switch(mode) {
			case MODE_PERIODIC_REPORT_TIME:
				pref = new SmartCarePreference(getActivity(), SmartCarePreference.SENSOR_SETTING_INFO_FILENAME);
				
				if(buttonId == R.id.simpleButton1) {
					value = 10;
				}
				else if(buttonId == R.id.simpleButton2) {
					value = 15;
				}
				else if(buttonId == R.id.simpleButton3) {
					value = 20;
				}
				else if(buttonId == R.id.simpleButton4) {
					value = 25;
				}
				else if(buttonId == R.id.simpleButton5) {
					value = 30;
				}
				else if(buttonId == R.id.simpleButton6) {
					value = 40;
				}
				else if(buttonId == R.id.simpleButton7) {
					value = 50;
				}
				else if(buttonId == R.id.simpleButton8) {
					value = 60;
				}
				
				pref.setIntValue(SmartCarePreference.PERIODIC_REPORT_TIME, value);
				SmartCareSystemApplication.getInstance().getGlobalMsgFunc().sendKeepAliveTimeMsg(value);
				break;
			case MODE_NON_ACTIVITY_NUM:
				pref = new SmartCarePreference(getActivity(), SmartCarePreference.SENSOR_SETTING_INFO_FILENAME);
				if(buttonId == R.id.simpleButton1) {
					value = 1;
				}
				else if(buttonId == R.id.simpleButton2) {
					value = 2;
				}
				else if(buttonId == R.id.simpleButton3) {
					value = 3;
				}
				else if(buttonId == R.id.simpleButton4) {
					value = 5;
				}
				else if(buttonId == R.id.simpleButton5) {
					value = 7;
				}
				else if(buttonId == R.id.simpleButton6) {
					value = 8;
				}
				else if(buttonId == R.id.simpleButton7) {
					value = 9;
				}
				else if(buttonId == R.id.simpleButton8) {
					value = 10;
				}
				
				pref.setIntValue(SmartCarePreference.NON_ACTIVITY_NUM, value);
				break;
			case MODE_NON_ACTIVITY_TIME:
				pref = new SmartCarePreference(getActivity(), SmartCarePreference.SENSOR_SETTING_INFO_FILENAME);
				if(buttonId == R.id.simpleButton1) {
					value = 30;
				}
				else if(buttonId == R.id.simpleButton2) {
					value = 60;
				}
				else if(buttonId == R.id.simpleButton3) {
					value = 90;
				}
				else if(buttonId == R.id.simpleButton4) {
					value = 120;
				}
				else if(buttonId == R.id.simpleButton5) {
					value = 150;
				}
				else if(buttonId == R.id.simpleButton6) {
					value = 180;
				}
				else if(buttonId == R.id.simpleButton7) {
					value = 210;
				}
				else if(buttonId == R.id.simpleButton8) {
					value = 240;
				}
				
				pref.setIntValue(SmartCarePreference.NON_ACTIVITY_TIME, value);
				SmartCareSystemApplication.getInstance().stopNonActivityReportTimer();
				SmartCareSystemApplication.getInstance().startNonActivityReportTimer();
				break;
			case MODE_REPORT_TIME_PERIOD:
				pref = new SmartCarePreference(getActivity(), SmartCarePreference.SERVER_SETTING_INFO_FILENAME);
				if(buttonId == R.id.simpleButton1) {
					value = 30;
				}
				else if(buttonId == R.id.simpleButton2) {
					value = 60;
				}
				else if(buttonId == R.id.simpleButton3) {
					value = 90;
				}
				else if(buttonId == R.id.simpleButton4) {
					value = 120;
				}
				else if(buttonId == R.id.simpleButton5) {
					value = 150;
				}
				else if(buttonId == R.id.simpleButton6) {
					value = 180;
				}
				else if(buttonId == R.id.simpleButton7) {
					value = 210;
				}
				else if(buttonId == R.id.simpleButton8) {
					value = 240;
				}
				
				pref.setIntValue(SmartCarePreference.SERVER_PERIODIC_REPORT_TIME, value);
				SmartCareSystemApplication.getInstance().stopPeriodReportTimer();
				SmartCareSystemApplication.getInstance().startPeriodReportTimer();
				break;
			case MODE_PERIOD_DAY:
				pref = new SmartCarePreference(getActivity(), SmartCarePreference.APPLICATION_SETTING_INFO_FILENAME);
				if(buttonId == R.id.simpleButton1) {
					value = 1;
				}
				else if(buttonId == R.id.simpleButton2) {
					value = 2;
				}
				else if(buttonId == R.id.simpleButton3) {
					value = 3;
				}
				else if(buttonId == R.id.simpleButton4) {
					value = 5;
				}
				else if(buttonId == R.id.simpleButton5) {
					value = 7;
				}
				else if(buttonId == R.id.simpleButton6) {
					value = 8;
				}
				else if(buttonId == R.id.simpleButton7) {
					value = 9;
				}
				else if(buttonId == R.id.simpleButton8) {
					value = 10;
				}
				
				pref.setIntValue(SmartCarePreference.CHARGE_DISCHARGE_PERIOD_DAY, value);
				break;
			case MODE_PERFORM_TIME:
				pref = new SmartCarePreference(getActivity(), SmartCarePreference.APPLICATION_SETTING_INFO_FILENAME);
				if(buttonId == R.id.simpleButton1) {
					value = 0;
				}
				else if(buttonId == R.id.simpleButton2) {
					value = 3;
				}
				else if(buttonId == R.id.simpleButton3) {
					value = 6;
				}
				else if(buttonId == R.id.simpleButton4) {
					value = 9;
				}
				else if(buttonId == R.id.simpleButton5) {
					value = 12;
				}
				else if(buttonId == R.id.simpleButton6) {
					value = 15;
				}
				else if(buttonId == R.id.simpleButton7) {
					value = 18;
				}
				else if(buttonId == R.id.simpleButton8) {
					value = 21;
				}
				
				pref.setIntValue(SmartCarePreference.CHARGE_DISCHARGE_PERFORM_TIME, value);
				break;
		}
	}
}
