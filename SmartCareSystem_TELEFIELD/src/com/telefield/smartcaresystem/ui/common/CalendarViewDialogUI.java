package com.telefield.smartcaresystem.ui.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.CalendarView.OnDateChangeListener;
import android.widget.EditText;
import android.widget.Toast;

import com.telefield.smartcaresystem.R;

public class CalendarViewDialogUI extends DialogFragment {
	CalendarView calendarView;
	EditText startTime, endTime;
	SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
	int editTextId;
	long maximumDate;
	long minimunDate;
	long defaultStartDate;
	String leftDate;
	String rightDate;
	String date;
	boolean ischanged = false;
	
	public CalendarViewDialogUI(EditText startTime, EditText endTime, int editTextId) {
		this.startTime = startTime;
		this.endTime = endTime;
		this.editTextId = editTextId;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	    if (getDialog() != null) {
	        getDialog().setCanceledOnTouchOutside(false);
	    }
	    return super.onCreateView(inflater, container, savedInstanceState);
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
		LayoutInflater mLayoutInflater = getActivity().getLayoutInflater();
		
		View view = mLayoutInflater.inflate(R.layout.calendarview_dialog, null);
		
		calendarView = (CalendarView) view.findViewById(R.id.calendarView);
		
		Date present = new Date();
		
		minimunDate = getMonthAgoDate();
		maximumDate = present.getTime();
		defaultStartDate = getDefaultStartDate();
		
		//CalendarView의 최소 날짜와 최대 날짜의 범위가 좁으면 Exception이 발생함...(현재 범위는 1달)
		calendarView.setMinDate(minimunDate);
		calendarView.setMaxDate(maximumDate);
		
		if(editTextId == R.id.startTime) {
			if(startTime != null && !startTime.getText().toString().equals("")) {
				try {
					String compareFromMinValue = format.format(minimunDate);
					String compareFromGetValue = format.format(format.parse(startTime.getText().toString()).getTime());
					
					if(compareFromMinValue.equals(compareFromGetValue)) {	//시작 날짜가 MinDate와 같으면 MinDate를 넣어도 무방함
						calendarView.setDate(minimunDate);
					}
					else {
						calendarView.setDate(format.parse(startTime.getText().toString()).getTime());
					}
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else {
				calendarView.setDate(defaultStartDate);
			}
		}
		else if(editTextId == R.id.endTime) {
			if(endTime != null && !endTime.getText().toString().equals("")) {
				try {
					calendarView.setDate(format.parse(endTime.getText().toString()).getTime());
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else {
				calendarView.setDate(maximumDate);
			}
		}
		
		calendarView.setOnDateChangeListener(new OnDateChangeListener() {
			@Override
			public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
				// TODO Auto-generated method stub
				ischanged = true;
				
				date = Integer.toString(year);
				
				month = month + 1;	//1월인 경우 0이 나오므로 +1 함.
				if(month > 0 && month < 10) {
					date = date + "0" +Integer.toString(month);
				}
				else {
					date = date + Integer.toString(month);
				}
				
				if(dayOfMonth > 0 && dayOfMonth < 10) {
					date = date + "0" +Integer.toString(dayOfMonth);
				}
				else {
					date = date + Integer.toString(dayOfMonth);
				}
			}
		});
		
		Button negativeButton = (Button) view.findViewById(R.id.dialogCancelButton);
		negativeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dismiss();
			}
		});
		
		Button positiveButton = (Button) view.findViewById(R.id.dialogConfirmButton);
		positiveButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				boolean isComplete = true;
				
				if(ischanged) {
					if(editTextId == R.id.startTime) {
						leftDate = date;
						rightDate = endTime.getText().toString();
					}
					else if(editTextId == R.id.endTime) {
						rightDate = date;
						leftDate = startTime.getText().toString();
					}
					
					if((leftDate == null || leftDate.equals("")) || (rightDate == null || rightDate.equals(""))) {
						if(editTextId == R.id.startTime) {
							startTime.setText(date);
						}
						else if(editTextId == R.id.endTime) {
							endTime.setText(date);
						}
					}
					else {
						if(!leftDate.equals(rightDate)) {
							try {
								long leftCompare = format.parse(leftDate).getTime();
								long rightCompare = format.parse(rightDate).getTime();
								
								if(leftCompare > rightCompare) {
									isComplete = false;
								}
								else {
									if(editTextId == R.id.startTime) {
										startTime.setText(date);
									}
									else if(editTextId == R.id.endTime) {
										endTime.setText(date);
									}
								}
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						else {
							if(editTextId == R.id.startTime) {
								startTime.setText(date);
							}
							else if(editTextId == R.id.endTime) {
								endTime.setText(date);
							}
						}
					}
				}
				else {
					if(editTextId == R.id.startTime) {
						if(startTime.getText().toString().equals("")) {
							startTime.setText(format.format(defaultStartDate));
						}
					}
					else if(editTextId == R.id.endTime) {
						if(endTime.getText().toString().equals("")) {
							endTime.setText(format.format(maximumDate));
						}
					}
				}
				
				if(isComplete) {
					dismiss();
				}
				else {
					Toast.makeText(getActivity().getApplicationContext(), R.string.wrong_date_range, Toast.LENGTH_SHORT).show();
				}
			}
		});
		
		mBuilder.setView(view);
		
		return mBuilder.create();
	}
	
	public long getMonthAgoDate() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);
		Date monthAgo = cal.getTime();
		
		return monthAgo.getTime();
	}
	
	public long getDefaultStartDate() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, -10);
		Date minus10Day = cal.getTime();
		
		return minus10Day.getTime();
	}
}
