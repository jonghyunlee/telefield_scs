package com.telefield.smartcaresystem.ui.common;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.telefield.smartcaresystem.R;

public class SelectDialogUI extends DialogFragment implements OnClickListener {
	IDialogChangeListener listener;
	int mode;
	int selectPosition;
	private String title;
	
	private Button btnOK, btnCancel;
	private TextView titleView;
	ListView screenOffListView;
	
	public static final int MODE_CHANGE_LCD_OFF_TIME = 1; // LCD OFF Time 변경
	
	public SelectDialogUI(){
		
	}
	
	public static SelectDialogUI newInstance(IDialogChangeListener listener, int mode) {
		SelectDialogUI f = new SelectDialogUI();
		f.setDialogChangeListener(listener);
		f.setMode(mode);
		return f;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (getDialog() != null) {
			getDialog().setCanceledOnTouchOutside(false);
		}
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
		LayoutInflater mLayoutInflater = getActivity().getLayoutInflater();

		View view = mLayoutInflater.inflate(R.layout.select_dialog, null);

		btnOK = (Button) view.findViewById(R.id.btn_ok);
		btnCancel = (Button) view.findViewById(R.id.btn_cancel);
		titleView = (TextView) view.findViewById(R.id.title_view);
		screenOffListView = (ListView) view.findViewById(R.id.screenOffListView);
		
		btnOK.setOnClickListener(this);
		btnCancel.setOnClickListener(this);
		
		if(mode == MODE_CHANGE_LCD_OFF_TIME) {
			titleView.setText(title);
			
			int getPosition = getIndex();
			
			String[] getArray = getActivity().getResources().getStringArray(R.array.screen_off_time);
			
			ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_single_choice, getArray);

			selectPosition = getPosition;
			
			screenOffListView.setAdapter(adapter);
			screenOffListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
			screenOffListView.setItemChecked(getPosition, true);
			screenOffListView.setSelection(getPosition);
			screenOffListView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					// TODO Auto-generated method stub
					selectPosition = position;
				}
				
			});
		}
		
		mBuilder.setView(view);

		return mBuilder.create();
	}
	
	private void updateData(int mode) {
		if(mode == MODE_CHANGE_LCD_OFF_TIME) {
			Settings.System.putInt(getActivity().getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT, getTimeByIndex(selectPosition));
		}
		
		if (listener != null)
			listener.notifyChangeListener();
		
		dismiss();
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setMode(int mode) {
		this.mode = mode;
	}
	
	public void setDialogChangeListener(IDialogChangeListener listener) {
		this.listener = listener;
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_ok:
			updateData(mode);
			break;
		case R.id.btn_cancel:
			dismiss();
			break;
		}
	}
	
	private int getIndex() {
		int index = 0;
		
		int getTime = Settings.System.getInt(getActivity().getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT, 15 * 1000);
		
		switch(getTime) {
			case 15000:
				index = 0;
				break;
			case 30000:
				index = 1;
				break;
			case 60000:
				index = 2;
				break;
			case 120000:
				index = 3;
				break;
			case 300000:
				index = 4;
				break;
			case 600000:
				index = 5;
				break;
			case 1800000:
				index = 6;
				break;
		}
		
		return index;
	}
	
	private int getTimeByIndex(int index) {
		int getTime = 0;
		
		switch(index) {
			case 0:
				getTime = 15000;
				break;
			case 1:
				getTime = 30000;
				break;
			case 2:
				getTime = 60000;
				break;
			case 3:
				getTime = 120000;
				break;
			case 4:
				getTime = 300000;
				break;
			case 5:
				getTime = 600000;
				break;
			case 6:
				getTime = 1800000;
				break;
		}
		
		return getTime;
	}
}
