package com.telefield.smartcaresystem.ui.common;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.database.Sensor;
import com.telefield.smartcaresystem.m2m.PeriodicalReportService;
import com.telefield.smartcaresystem.ui.UIConstant;

public class CustomCountDownTimer extends CountDownTimer {
	private ProgressDialogUI customDialog;
	private Context mContext;
	private String sendMsg;
	private Sensor sensorInfo;
	private int sensor_id;
	private int mode;
	private int sensorIndex;
	
	public static final int SENSOR_REGISTER_MODE = 1; //센서 등록 expire timer
	public static final int SENSOR_DELETE_MODE = 2;   //센서 삭제 expire timer
	public static final int CARE_SENSOR_OUT_COME_MODE = 3;	
	public static final int NON_ACTIVITY_REPORT_MODE = 4; //활동량 미감지 보고를 위한 count timer
	
	public static CustomCountDownTimer mCustomCountDownTimer;
	
	public static CustomCountDownTimer getInstance(long millisInFuture, long countDownInterval, ProgressDialogUI customDialog, Context mContext) {
		if(mCustomCountDownTimer == null) {
			mCustomCountDownTimer = new CustomCountDownTimer(millisInFuture, countDownInterval, customDialog, mContext);
		}
		
		return mCustomCountDownTimer;
	}
	
	public static CustomCountDownTimer getInstance(long millisInFuture, long countDownInterval, ProgressDialogUI customDialog, Context mContext, boolean renew) {
		if(renew) {
			mCustomCountDownTimer = null;
			mCustomCountDownTimer = new CustomCountDownTimer(millisInFuture, countDownInterval, customDialog, mContext);
		}
		else {
			getInstance(millisInFuture, countDownInterval, customDialog, mContext);
		}
		return mCustomCountDownTimer;
	}
	
	public static CustomCountDownTimer getInstance() {
		return mCustomCountDownTimer;
	}
	
	public CustomCountDownTimer(long millisInFuture, long countDownInterval) {
		super(millisInFuture, countDownInterval);
		
		sensor_id = -1;
		sensorInfo = null;
		sendMsg = null;
		
	}
	
	public CustomCountDownTimer(long millisInFuture, long countDownInterval, ProgressDialogUI customDialog, Context mContext) {
		this(millisInFuture, countDownInterval);
		// TODO Auto-generated constructor stub
		
		this.customDialog = customDialog;
		this.mContext = mContext;
	}
	
	@Override
	public void onFinish() {
		// TODO Auto-generated method stub
			
		if( mode == SENSOR_REGISTER_MODE || mode == SENSOR_DELETE_MODE || mode == CARE_SENSOR_OUT_COME_MODE)
		{	
			if( customDialog != null ) {
				customDialog.dismiss();
			}
			
			Activity activity = (Activity) mContext;
			
			if(mode == SENSOR_REGISTER_MODE) {
				ResultDialogUI dialog = ResultDialogUI.newInstance(ResultDialogUI.MODE_REGISTER_FAIL);
				dialog.setTitle(getSendMsg() + UIConstant.SPACE_CHARACTER + mContext.getString(R.string.register_fail));
				dialog.setSensorID(getSensorId());
				dialog.setSensorNum(sensorIndex);
				dialog.setSendMsg(getSendMsg());
				dialog.show(activity.getFragmentManager(), "Dialog");
			}
			else if(mode == SENSOR_DELETE_MODE) {
				ResultDialogUI dialog = ResultDialogUI.newInstance(ResultDialogUI.MODE_DELETE_FAIL);
				dialog.setTitle(getSendMsg() + UIConstant.SPACE_CHARACTER + mContext.getString(R.string.delete_fail));
				dialog.setSensorID(getSensorId());
				dialog.setSensorNum(sensorIndex);
				dialog.setSendMsg(getSendMsg());
				dialog.show(activity.getFragmentManager(), "Dialog");
			}
			else if(mode == CARE_SENSOR_OUT_COME_MODE) {
				ResultDialogUI dialog = ResultDialogUI.newInstance(ResultDialogUI.MODE_OUT_CARE_FAIL);
				dialog.setTitle(getSendMsg() + UIConstant.SPACE_CHARACTER + mContext.getString(R.string.register_fail));
				dialog.setSensorID(getSensorId());
				dialog.setSensorNum(sensorIndex);
				dialog.setSendMsg(getSendMsg());
				dialog.show(activity.getFragmentManager(), "Dialog");
			}
			
			mCustomCountDownTimer = null;
		}
		else if(mode == NON_ACTIVITY_REPORT_MODE)
		{
			Intent intent = new Intent(mContext, PeriodicalReportService.class);
			intent.putExtra(PeriodicalReportService.REPORT_PARAM, PeriodicalReportService.NON_ACTIVITY_REPORT);
			mContext.startService(intent);
		}
	}
	
	public void setSendMsg(String sendMsg) {
		this.sendMsg = sendMsg;
	}
	
	public String getSendMsg() {
		return sendMsg;
	}
	
	public void setSensorInfo(Sensor sensorInfo) {
		this.sensorInfo = sensorInfo;
	}
	
	public Sensor getSensorInfo() {
		return sensorInfo;
	}
	
	public void setMode(int mode) {
		this.mode = mode;
	}
	
	public int getMode() {
		return mode;
	}
	
	public void setSensorId(int sensor_id)
	{
		this.sensor_id = sensor_id;
	}
	
	public int getSensorId(){  return sensor_id; }

	@Override
	public void onTick(long millisUntilFinished) {
		// TODO Auto-generated method stub
		
	}

	public void setSensorNum(int sensorIndex) {
		// TODO Auto-generated method stub
		this.sensorIndex = sensorIndex;
	}
	
	public void stopCustomProgressDialog() {
		if(customDialog != null) {
			customDialog.dismiss();
		}
	}
	
	//CustomCountDownTimer를 null 처리를 하지 않으면, 콜 2번 이상 수신시 ProgressDialog가 사라지지 않는 현상이 발생
	public void setNullInstance() {
		mCustomCountDownTimer = null;
	}
}
