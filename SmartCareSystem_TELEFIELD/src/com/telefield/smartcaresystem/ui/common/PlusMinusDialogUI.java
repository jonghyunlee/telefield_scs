package com.telefield.smartcaresystem.ui.common;

import java.io.IOException;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.util.LogMgr;
import com.telefield.smartcaresystem.util.SmartCarePreference;

public class PlusMinusDialogUI extends DialogFragment implements OnClickListener {
	int mode;
	String title;
	private TextView titleView;
	Button okButton, minusButton, plusButton; 
	
	int nMax = 0;
	int nCurrentVolumn = 0;
	
	AudioManager mAudioManager;
	MediaPlayer mediaPlayer;
	SmartCarePreference pref;
	public static final int MODE_MUSIC = 1;	//일반 음량 설정(애플리케이션 설정)
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	    if (getDialog() != null) {
	        getDialog().setCanceledOnTouchOutside(false);
	    }
	    return super.onCreateView(inflater, container, savedInstanceState);
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
		LayoutInflater mLayoutInflater = getActivity().getLayoutInflater();
		
		View view = mLayoutInflater.inflate(R.layout.plus_minus_dialog, null);
		
		titleView = (TextView) view.findViewById(R.id.title_view);
		
		okButton = (Button) view.findViewById(R.id.btn_ok);
		minusButton = (Button) view.findViewById(R.id.minusButton);
		plusButton = (Button) view.findViewById(R.id.plusButton);
		okButton.setOnClickListener(this);
		minusButton.setOnClickListener(this);
		plusButton.setOnClickListener(this);
		
		mAudioManager = (AudioManager) getActivity().getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
		
		mediaPlayer = new MediaPlayer();
		pref = new SmartCarePreference(getActivity(), SmartCarePreference.APPLICATION_SETTING_INFO_FILENAME);
		//디바이스에 설정된 벨소리
		Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
		
		try {
			mediaPlayer.setDataSource(uri.toString());
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(mode == MODE_MUSIC) {
			titleView.setText(title);
			
			nMax = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
			nCurrentVolumn = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
	
			mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
			mediaPlayer.setLooping(true);
			
			try {
				mediaPlayer.prepare();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		mBuilder.setView(view);
		
		return mBuilder.create();
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public void setMode(int mode) {
		this.mode = mode;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
			case R.id.btn_ok:
				dismiss();
				break;
			case R.id.minusButton:
				if(mode == MODE_MUSIC) {
					if(nCurrentVolumn > 0)
						nCurrentVolumn--;
					mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, nCurrentVolumn, AudioManager.FLAG_SHOW_UI);
					pref.setIntValue(SmartCarePreference.VOLUME_SETTING, nCurrentVolumn);
					mediaPlayer.start();
					LogMgr.d(LogMgr.TAG, "volume level = " + nCurrentVolumn);
				}
				break;
			case R.id.plusButton:
				if(mode == MODE_MUSIC) {
					if(nCurrentVolumn < nMax)
						nCurrentVolumn++;
					mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, nCurrentVolumn, AudioManager.FLAG_SHOW_UI);
					pref.setIntValue(SmartCarePreference.VOLUME_SETTING, nCurrentVolumn);
					mediaPlayer.start();
					LogMgr.d(LogMgr.TAG, "volume level = " + nCurrentVolumn);
				}
				break;
		}
	}

	
	
	
	@Override
	public void onCancel(DialogInterface dialog) {
		// TODO Auto-generated method stub
		super.onCancel(dialog);
		
		if(mediaPlayer != null && mediaPlayer.isPlaying()) {
			mediaPlayer.stop();
			mediaPlayer.release();
			mediaPlayer = null;
		}
	}
	
	@Override
	public void dismiss() {
		// TODO Auto-generated method stub
		super.dismiss();
		
		if(mediaPlayer != null && mediaPlayer.isPlaying()) {
			mediaPlayer.stop();
			mediaPlayer.release();
			mediaPlayer = null;
		}
	}
}
