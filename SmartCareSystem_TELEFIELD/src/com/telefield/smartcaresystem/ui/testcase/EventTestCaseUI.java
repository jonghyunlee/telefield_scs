package com.telefield.smartcaresystem.ui.testcase;

import java.nio.ByteBuffer;
import java.util.List;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.database.ConnectDB;
import com.telefield.smartcaresystem.database.Sensor;
import com.telefield.smartcaresystem.m2m.GateWayMessage;
import com.telefield.smartcaresystem.m2m.GatewayDevice;
import com.telefield.smartcaresystem.m2m.GatewayIndex;
import com.telefield.smartcaresystem.m2m.ProtocolFrame;
import com.telefield.smartcaresystem.m2m.ProtocolManger;
import com.telefield.smartcaresystem.message.SensorID;
import com.telefield.smartcaresystem.ui.LauncherUI;
import com.telefield.smartcaresystem.ui.UIConstant;
import com.telefield.smartcaresystem.util.ByteArrayToHex;
import com.telefield.smartcaresystem.util.ConvertTime;
import com.telefield.smartcaresystem.util.LogMgr;

public class EventTestCaseUI extends ListActivity{
	ListView menuListView;
	String[] testCaseList;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		testCaseList = getResources().getStringArray(R.array.testcase);
		ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, testCaseList); 
		setListAdapter(arrayAdapter);
		
	}
	
	@Override
	protected void onListItemClick(ListView list, View v, int position, long id) {
		// TODO Auto-generated method stub
		ProtocolFrame pFrame = null;
		super.onListItemClick(list, v, position, id);
		switch (position) {
			case 0://화제 센서 알람(경보) 발생
				pFrame = eventReportData(SensorID.getSensorType(UIConstant.FIRE_SENSOR), GateWayMessage.DATA_WARN_ALERT);
				break;
			case 1://화제 센서 알람(경보) 취소
				pFrame = eventReportData(SensorID.getSensorType(UIConstant.FIRE_SENSOR), GateWayMessage.DATA_CANCEL_ALERT);
				break;
			case 2://가스 센서 알람(경보) 발생
				pFrame = eventReportData(SensorID.getSensorType(UIConstant.GAS_SENSOR), GateWayMessage.DATA_WARN_ALERT);
				break;
			case 3://가스 센서 알람(경보) 취소
				pFrame = eventReportData(SensorID.getSensorType(UIConstant.GAS_SENSOR), GateWayMessage.DATA_CANCEL_ALERT);
				break;
			case 4://돌보미 방문
				pFrame = eventReportData(SensorID.getSensorType(UIConstant.CARE_SENSOR), GateWayMessage.DATA_COME_CARE);
				break;
			case 5://돌보미 퇴실
				pFrame = eventReportData(SensorID.getSensorType(UIConstant.CARE_SENSOR), GateWayMessage.DATA_OUT_CARE);
				break;
			case 6://재실 감지
				pFrame = eventReportData(SensorID.getSensorType(UIConstant.DOOR_SENSOR), GateWayMessage.DATA_IN_ROOT);
				break;
			case 7://외출 감지
				pFrame = eventReportData(SensorID.getSensorType(UIConstant.DOOR_SENSOR), GateWayMessage.DATA_OUT_ROOT);
				break;
			case 8://활동 감지
				pFrame = eventReportData(SensorID.getSensorType(UIConstant.ACTIVITY_SENSOR), GateWayMessage.DATA_DETECT_ACTIVITY);
				break;
			case 9://미활동 감지
				pFrame = eventReportData(SensorID.getSensorType(UIConstant.ACTIVITY_SENSOR), GateWayMessage.DATA_DETECT_MISSED_ACTIVITY);
				break;
			case 10://119통화버튼 동작상태 알림
				pFrame = gatewayEventReportData(GateWayMessage.ID_GW_EVENT_EMG, (byte) 0x01);
				break;
			case 11://센터 통화 버튼 알림
				pFrame = gatewayEventReportData(GateWayMessage.ID_GW_EVENT_CENTOR, (byte) 0x01);
				break;
			case 12://게이트웨이 경보 취소 알림
				pFrame = gatewayEventReportData(GateWayMessage.ID_GW_EVENT_CANCEL, (byte) 0x01);
				break;
			case 13://게이트웨이 전원 차단 보고
				pFrame = gatewayEventReportData(GateWayMessage.ID_GW_EVENT_POWER, (byte) 0x01);
				break;
			case 14://게이트웨이 전원 복구 보고
				pFrame = gatewayEventReportData(GateWayMessage.ID_GW_EVENT_POWER, (byte) 0x00);
				break;
			case 15://테스트케이스 종료
				Toast.makeText(getApplicationContext(), "테스트를 종료합니다.", Toast.LENGTH_SHORT).show();
				Intent intent = new Intent(getApplicationContext(), LauncherUI.class);
				startActivity(intent);
				break;
			default:
				break;	
		}
		if(pFrame == null && position != 15){
			Toast.makeText(getApplicationContext(),"등록된 센서가 없습니다. 센서를 먼저 등록하여 주십시오.", Toast.LENGTH_SHORT).show();
			return;
		}
		if(position != 15){
			ProtocolManger pManager = SmartCareSystemApplication.getInstance().getProtocolManger();
			pManager.insertFrame(pFrame);
			pManager.messageSend(null);
			Toast.makeText(getApplicationContext(), testCaseList[position] + "메시지를 전송하였습니다.", Toast.LENGTH_SHORT).show();
		}
	}
	
	private ProtocolFrame eventReportData(int sensor_type, byte gateWayMessage){
		byte[] payload = null;
		byte[] byte_time = ConvertTime.getCurrentTime();
		GatewayDevice gw = GatewayDevice.getInstance();
		ProtocolFrame frame = ProtocolFrame.getInstance();
		ConnectDB con = ConnectDB.getInstance(getApplicationContext());
		Sensor sensor = new Sensor();
		List<Sensor> result = null;
		sensor.setType(sensor_type);
		result = con.selectSensor(sensor);
		LogMgr.d(LogMgr.TAG, "result.size...." + result.size());
		if(result == null || result.size() <= 0 ) 
			return null;
		
		sensor = result.get(0);

		payload = new byte[GatewayIndex.SIZE_REPORT_SENSOR_EVENT];
		payload[GatewayIndex.EVENT_SENSOR_BATTERY] = sensor.getSensor_batt().byteValue();
		payload[GatewayIndex.EVENT_SENSOR_STATUS] = sensor.getComm_status().byteValue();
		payload[GatewayIndex.EVENT_SENSOR_DATA] = gateWayMessage;
		payload[GatewayIndex.PACKET_ID] = GatewayIndex.INFO_REPORT_SENSOR_EVENT;
		System.arraycopy(gw.number, 0, payload, GatewayIndex.GW_NUM, gw.number.length);
		System.arraycopy(gw.MAC, 0, payload, GatewayIndex.GW_MAC, gw.MAC.length);
		payload[GatewayIndex.GW_POWER] = gw.power_state;
		payload[GatewayIndex.GW_BATTERY] = gw.battery_state;
		System.arraycopy(byte_time, 0, payload, GatewayIndex.TIME_TIC, byte_time.length);
		byte[] sensorID = ByteBuffer.allocate(Short.SIZE / 8).putShort(sensor.getSensor_id().shortValue()).array();
		System.arraycopy(sensorID, 0, payload, GatewayIndex.SENSOR_ADDR, sensorID.length);
		byte[] sensorMAC = ByteArrayToHex.getMacAddress(sensor.getMAC());
		System.arraycopy(sensorMAC, 0, payload, GatewayIndex.SENSOR_MAC, sensorMAC.length);

		frame.setPacket_start(GatewayIndex.START_DATA);
		frame.setPacket_payload(payload);
		frame.setPacket_end(GatewayIndex.END_DATA);
		
		LogMgr.d(LogMgr.TAG, "GateWayMessage : " + gateWayMessage + ".. Bytes : " + ByteArrayToHex.byteArrayToHex(frame.getBytes(), frame.getBytes().length));
		return frame;
	}
	
	private ProtocolFrame gatewayEventReportData(byte msg_id, byte data){
		ProtocolFrame frame = ProtocolFrame.getInstance();
		byte[] payload = null;
		byte[] byte_time = ConvertTime.getCurrentTime();
		GatewayDevice gw = GatewayDevice.getInstance();
		
		payload = new byte[GatewayIndex.SIZE_REPORT_GW_EVENT];
		payload[GatewayIndex.EVENT_GW_MSG_ID] = msg_id;
		payload[GatewayIndex.EVENT_GW_MSG_DATA] = data;	
		payload[GatewayIndex.PACKET_ID] = GatewayIndex.INFO_REPORT_GW_EVENT;
		System.arraycopy(gw.number, 0, payload, GatewayIndex.GW_NUM, gw.number.length);
		System.arraycopy(gw.MAC, 0, payload, GatewayIndex.GW_MAC, gw.MAC.length);
		payload[GatewayIndex.GW_POWER] = gw.power_state;
		payload[GatewayIndex.GW_BATTERY] = gw.battery_state;
		System.arraycopy(byte_time, 0, payload, GatewayIndex.TIME_TIC, byte_time.length);
	
		frame.setPacket_start(GatewayIndex.START_DATA);
		frame.setPacket_payload(payload);
		frame.setPacket_end(GatewayIndex.END_DATA);
		LogMgr.d(LogMgr.TAG, "GateWayMessage : " + msg_id + data + ".. Bytes : " + ByteArrayToHex.byteArrayToHex(frame.getBytes(), frame.getBytes().length));
		return frame;
	}
}
