package com.telefield.smartcaresystem.ui.testcase;

import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.telefield.smartcaresystem.Constant;
import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.database.ConnectDB;
import com.telefield.smartcaresystem.database.NotReported;
import com.telefield.smartcaresystem.database.Sensor;
import com.telefield.smartcaresystem.database.Users;
import com.telefield.smartcaresystem.http.AsyncCallback;
import com.telefield.smartcaresystem.http.AsyncFileDownloader;
import com.telefield.smartcaresystem.m2m.GateWayMessage;
import com.telefield.smartcaresystem.m2m.GatewayIndex;
import com.telefield.smartcaresystem.m2m.ProtocolFrame;
import com.telefield.smartcaresystem.m2m.ProtocolManger;
import com.telefield.smartcaresystem.message.FormattedFrame;
import com.telefield.smartcaresystem.message.SensorID;
import com.telefield.smartcaresystem.sensor.SensorDeviceManager;
import com.telefield.smartcaresystem.ui.LockScreenActivity;
import com.telefield.smartcaresystem.util.ByteArrayToHex;
import com.telefield.smartcaresystem.util.LogMgr;
import com.telefield.smartcaresystem.util.SmartCarePreference;

public class TestCaseUI extends Activity implements OnClickListener {

	Button periodical;
	Button openning;
	Button event;
	Button zigbeeUpdate;
	Button appUpdate;
	Button backbtn;
	Context mContext;
	
	SensorDeviceManager sensorDeviceMgr;
	ProtocolManger mProtocolManger;
	AlertDialog alertDialog;
	Message listener;
	private int gatewayRssi;
	private static Handler handler;
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.test_case);
		mContext = this;
		
		periodical = (Button) findViewById(R.id.testperiodical);
		openning = (Button) findViewById(R.id.testopenning);
		event = (Button) findViewById(R.id.testeventt);
		zigbeeUpdate = (Button)findViewById(R.id.zigbeeupdate);
		appUpdate = (Button)findViewById(R.id.appupdate);
		backbtn = (Button) findViewById(R.id.testbackbtn);
		
		periodical.setOnClickListener(this);
		openning.setOnClickListener(this);
		event.setOnClickListener(this);
		backbtn.setOnClickListener(this);
		zigbeeUpdate.setOnClickListener(this);
		appUpdate.setOnClickListener(this);
		
		alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle("주의");
		alertDialog.setCanceledOnTouchOutside(true);
		//확인버튼 추가 <--
		listener = null ;
		
		sensorDeviceMgr = SmartCareSystemApplication.getInstance()
				.getSensorDeviceManager();
		
		mProtocolManger = SmartCareSystemApplication.getInstance()
				.getProtocolManger();
		
		TelephonyManager telManager  = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
		telManager.listen(plistener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
	}

	
	@Override
	public void onClick(View v) {
		LogMgr.d(LogMgr.TAG,""+v.getId());
		switch (v.getId()) {
		
		//주기보고 부분
		case R.id.testperiodical:
			
			LogMgr.d(LogMgr.TAG,
					"PeriodicalReportService onStartCommand -- PERIODIC_REPORT ");
			// sensor 들을 update 한다.
			sensorDeviceMgr.updateSensors();

			// to do 주기보고
			List<Sensor> sensor_list = sensorDeviceMgr.getSensorList();

			Iterator<Sensor> it = sensor_list.iterator();
			while (it.hasNext()) {
				Sensor sensor = it.next();
				if (SensorID.getSensorType(sensor.getSensor_id().intValue()) >= SensorID.CARE_SENSOR_TYPE.getSensorType())
					continue; // 돌보미 센서는 주기보고에서 제외한다. 돌보미 센서 타입은 0x80xx ~ 0xFFFF
				
				LogMgr.d(LogMgr.TAG,"PERIODIC_REPORT insert frame : " + sensor.getSensor_id());
				// 센서정보를 전달.
				mProtocolManger.insertFrame(ProtocolFrame.getInstance(
						GatewayIndex.INFO_REPORT_CYCLE, sensor, null));

			}
			Set<BluetoothDevice> pairedDevices = BluetoothAdapter.getDefaultAdapter().getBondedDevices();
    		BluetoothDevice bloodPressure = null;
    		for (BluetoothDevice device : pairedDevices) {
    			if(device.getBluetoothClass().getDeviceClass() == BluetoothClass.Device.HEALTH_BLOOD_PRESSURE)
    				bloodPressure = device;
    		}
			if (bloodPressure != null) {
				String mac = bloodPressure.getAddress().replaceAll(":", " ");
				Sensor sensor = new Sensor(SensorID.getSensorId(SensorID.BLOOD_PRESURE_TYPE.getSensorType(), 0x01), mac);
				sensor.setComm_status(1);
				sensor.setSensor_batt(100);

				mProtocolManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_CYCLE, sensor, null));
			}
			
			if(sensor_list.size() > 0 || bloodPressure != null){
				mProtocolManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_GW_DATA, FormattedFrame.getInstance(GateWayMessage.ID_REPORT_GATEWAY_INFO, this.getSub_data())));
				mProtocolManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_GW_DATA, FormattedFrame.getInstance(GateWayMessage.ID_REPORT_FINISH_SEND)));
				mProtocolManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REQUEST_DATA));
				mProtocolManger.messageSend(null);
			}
			alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "확인", listener );
			alertDialog.setMessage("주기보고를 전송하였습니다.");
			alertDialog.show();
			break;
			
		//개통 부분
		case R.id.testopenning:
			ConnectDB con = ConnectDB.getInstance(SmartCareSystemApplication
					.getInstance().getApplicationContext());
			con.insertNotReported();
			List<NotReported> list = con.selectAllNotReported();
			int cnt = 0;
			LogMgr.d(LogMgr.TAG,"testNotReportedData click openning list size:"+list.size());
			for(NotReported data : list){
				cnt ++;
				String getData = ByteArrayToHex.byteArrayToHex(data.getContent(), data.getContent().length);
				LogMgr.d(LogMgr.TAG,"content:"+ getData+".....id:"+data.getId()+".....cnt:"+cnt);
			}
			con.deleteAllNotReported();
			List<NotReported> list1 = con.selectAllNotReported();
			LogMgr.d(LogMgr.TAG,"testNotReportedData click openning deletelist size:"+list1.size());
			alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "확인", listener );
			alertDialog.setMessage("개통을 전송하였습니다.");
			alertDialog.show();
			break;
			
		//이벤트 보고 부분
		case R.id.testeventt:
			Intent intent = new Intent(mContext, EventTestCaseUI.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
			finish();
			break;
			
		//지그비 업데이트	
		case R.id.zigbeeupdate:
			ConnectDB db = ConnectDB.getInstance(SmartCareSystemApplication.getInstance().getApplicationContext());
			ProtocolManger pManger = SmartCareSystemApplication.getInstance().getProtocolManger();
			List<NotReported> list2 = db.selectAllNotReported();
			db.deleteAllNotReported();
			LogMgr.d(LogMgr.TAG,"GCMIntentHandler handleNotReported start...list.size() : " + list2.size());
			for(NotReported nrData : list2){
				//payload 값만 저장하기때문에 payload값만 가져옴
				byte[] notReportedData = nrData.getContent();
				pManger.insertFrame(ProtocolFrame.getInstance(notReportedData));	
			}
			pManger.messageSend(null);
			LogMgr.d(LogMgr.TAG, "GCMIntentHandler handleNotReported end...");
			break;
		//앱 업데이트
		case R.id.appupdate:
			String url = "http://222.122.128.45:8080/image/download/android/background/seongnam";
			String filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/SmartCareSystem/image";
			File file = new File(filePath);		
			if(!file.exists()){
				file.mkdirs();
			}
			LogMgr.d(LogMgr.TAG, "GCMIntentHandler handleLockscreenImg start...filePath : " + filePath + "...file.exists : " + file.exists() + "...url : " + url);
			new AsyncFileDownloader(getApplicationContext()).download(url,Constant.LOCKSCREEN_IMAGE_PATH, new AsyncCallback.Base<File>() {
				@Override
				public void onResult(File result) {
					LogMgr.d(LogMgr.TAG,"LOCKSCREEN IMAGE DOWNLOAD COMPLETE");
					
					if(handler != null && result != null){
						handler.sendMessage(Message.obtain(handler, LockScreenActivity.UPDATE_CENTERVIEW));
						LogMgr.d(LogMgr.TAG,"LOCKSCREEN IMAGE APPLY COMPLETE");
					}else{
						LogMgr.d(LogMgr.TAG,"LOCKSCREEN IMAGE APPLY FAIL REASON : handler or imagefile is null....");
					}
				}
				
				@Override
				public void exceptionOccured(Exception e) {
					LogMgr.d(LogMgr.TAG,"LOCKSCREEN IMAGE APPLY FAIL REASON : " + e);
					super.exceptionOccured(e);
				}
			});
			LogMgr.d(LogMgr.TAG, "GCMIntentHandler handleLockscreenImg end...");
			break;
	
		//뒤로가기
		case R.id.testbackbtn:
//			Toast.makeText(mContext, "뒤로가기 버튼누름", Toast.LENGTH_SHORT).show();
			onBackPressed();
			break;
		default:
			break;
		}

	}
	public static void setHandler(Handler handler){
		TestCaseUI.handler = handler;
	}
	private String getStringPreference(String fileName, String KEY) {
		SmartCarePreference pref = new SmartCarePreference(SmartCareSystemApplication.getInstance().getApplicationContext(), fileName);
		return pref.getStringValue(KEY, "");
	}
	
	private int getIntPreference(String fileName, String KEY) {
		SmartCarePreference pref = new SmartCarePreference(SmartCareSystemApplication.getInstance().getApplicationContext(), fileName);
		return pref.getIntValue(KEY, 0);
	}
	
	private Users selectData(List<Users> userList, String userType) {
		
		if(userList != null && userList.size() > 0){
			for (Users user : userList){
				if(user != null && user.getType().equals(userType))
					return user;				
			}
			return null;
		}
		else
			return null;
	}
	
	PhoneStateListener plistener = new PhoneStateListener() {
		@Override
		public void onSignalStrengthsChanged(SignalStrength signalStrength) {
			LogMgr.d(LogMgr.TAG, "rssi-"+signalStrength.getGsmSignalStrength());
			int gatewayRssi = -113 + (2 * signalStrength.getGsmSignalStrength());
			LogMgr.d(LogMgr.TAG, "gatewayRssi-"+gatewayRssi);
			setGatewayRssi(gatewayRssi);
			super.onSignalStrengthsChanged(signalStrength);
		}
	};
	
	public byte[] getSub_data(){
		byte [] sub_data = new byte[9]; 
		
		sub_data[0] = (byte) gatewayRssi;
		SmartCarePreference preference = new SmartCarePreference(this, SmartCarePreference.CALL_SETTING_INFO_FILENAME);
		int allCallTime = preference.getIntValue(SmartCarePreference.ALL_CALL_TIME, 0);
		byte[] getAllCallTime = FormattedFrame.intToFourByteArray(allCallTime);
		int i = 1;
		LogMgr.d(LogMgr.TAG, "before for : "+ByteArrayToHex.byteArrayToHex(sub_data, sub_data.length));
		for(byte tobyte : getAllCallTime){
			sub_data[i] = tobyte;
			i++;
		}
		LogMgr.d(LogMgr.TAG, "after for : " + ByteArrayToHex.byteArrayToHex(sub_data, sub_data.length));
		return sub_data;
	}
	
	private void setGatewayRssi(int gatewayRssi){
		this.gatewayRssi = gatewayRssi;
	}
}
