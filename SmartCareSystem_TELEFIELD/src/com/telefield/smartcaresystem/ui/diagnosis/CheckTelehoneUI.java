package com.telefield.smartcaresystem.ui.diagnosis;

import java.util.ArrayList;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.diagnostic.IDiagUnitResultCallback;
import com.telefield.smartcaresystem.message.SensorID;
import com.telefield.smartcaresystem.ui.LauncherUI;
import com.telefield.smartcaresystem.ui.UIConstant;
import com.telefield.smartcaresystem.ui.sensor.SensorAdapterUI;
import com.telefield.smartcaresystem.util.LogMgr;
import com.telefield.smartcaresystem.util.SpeechUtil;

public class CheckTelehoneUI extends Activity implements IDiagUnitResultCallback {
	ListView testTelephoneListView;
	Button selectAllButton, selectedTestButton, stopTestButton, backKeyButton, homeKeyButton;
	Context mContext;
	SensorAdapterUI mAdapter;
	ArrayList checkedValue; 
	ArrayList<String> testListText;
	String[] testListData;
	String[] testResultArray;
	
	final int NOTIFY_EVENT = 1;
	
	Handler mHandler = new Handler(){
		@Override
		public void handleMessage(Message msg) {
			
			switch(msg.what)
			{
			case NOTIFY_EVENT:
				listUpdate(msg.arg1,msg.arg2);
				break;
			}
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sensor_test_ui);
		
		mContext = this;
		
		checkedValue = new ArrayList();
		testListText = new ArrayList<String>();
		
		TextView emptyTextView = (TextView) findViewById(R.id.emptyTextView);
		TextView titleTextView = (TextView) findViewById(R.id.titleTextView);

		String titleText;
		int sensorId = SensorID.getSensorId(SensorID.CODI_SENSOR_TYPE.getSensorType(), 0x1);
		
		titleText = getString(R.string.sensor_test_at_home);
		
		Map<Integer,String> testListMap = SmartCareSystemApplication.getInstance().getDiagnosticManager().getAllDiagList(sensorId);			
		final String[] checkLists = testListMap.values().toArray(new String[0]);
				
		titleTextView.setText(titleText);
		
		testTelephoneListView = (ListView) findViewById(R.id.testSensorListView);
		
		selectAllButton = (Button) findViewById(R.id.testButton_All);
		selectedTestButton = (Button) findViewById(R.id.testButton_selected);
		stopTestButton = (Button) findViewById(R.id.stopTestButton);
		stopTestButton.setVisibility(View.GONE);
		
		if(checkLists != null && checkLists.length > 0) {
			emptyTextView.setVisibility(View.GONE);
			testTelephoneListView.setVisibility(View.VISIBLE);
			selectAllButton.setVisibility(View.VISIBLE);
			selectedTestButton.setVisibility(View.VISIBLE);
			
			mAdapter = new SensorAdapterUI(mContext, checkLists, UIConstant.MODE_TEST_SENSOR, getFragmentManager());
			testTelephoneListView.setAdapter(mAdapter);
			
			selectAllButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					mAdapter = new SensorAdapterUI(mContext, checkLists, UIConstant.MODE_ALL_CHECK_TEST_LIST, getFragmentManager());
					mAdapter.setAllCheck();
					testTelephoneListView.setAdapter(mAdapter);
				}
			});
			
			selectedTestButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					//테스트 중에는 화면이 계속 켜져 있어야 한다.
					getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
				
					boolean[] checkedList = mAdapter.getCheckedList();
					for(int i=0;i<checkedList.length;i++) {
						if(checkedList[i]) {	//체크되어 있음
							checkedValue.add(new Integer(i));
							testListText.add(mAdapter.getItem(i).toString());
						}
					}
					
					if(checkedValue.size() > 0) {
						String[] sendData = new String[checkedValue.size()];
						sendData = testListText.toArray(sendData);
						
						testListData = sendData;
						testResultArray = new String[testListData.length];
						for(int i=0;i<testResultArray.length;i++) {	//Result Array는 일단 Testing으로 초기화
							testResultArray[i] = getString(R.string.testing);
						}
						
						mAdapter = new SensorAdapterUI(mContext, sendData, UIConstant.MODE_REAL_TEST_SENSOR, getFragmentManager());
						testTelephoneListView.setAdapter(mAdapter);
						
						selectAllButton.setVisibility(View.GONE);
						selectedTestButton.setVisibility(View.GONE);
						stopTestButton.setVisibility(View.VISIBLE);
						backKeyButton.setClickable(false);
						homeKeyButton.setClickable(false);
						stopTestButton.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								LogMgr.d(LogMgr.TAG,"테스트 중단!!!! ");
								if(SmartCareSystemApplication.getInstance().getDiagnosticManager().isDiagnosticTesting())
									SmartCareSystemApplication.getInstance().getDiagnosticManager().stopTest();
								
								mAdapter = new SensorAdapterUI(mContext, testListData, UIConstant.MODE_STOP_TEST, getFragmentManager());
								mAdapter.setTestResultArray(testResultArray);
								testTelephoneListView.setAdapter(mAdapter);
								
								stopTestButton.setVisibility(View.INVISIBLE);
								backKeyButton.setClickable(true);
								homeKeyButton.setClickable(true);
							}
						});
						
						SmartCareSystemApplication.getInstance().getDiagnosticManager().runTest(checkedValue, (IDiagUnitResultCallback)mContext);
						
					}
					else {	//체크된 리스트가 없으면 테스트를 해서는 안된다.
						Toast.makeText(mContext, R.string.no_checked_list, Toast.LENGTH_SHORT).show();
					}
				}
			});
		}
		else {
			emptyTextView.setVisibility(View.VISIBLE);
			emptyTextView.setText(titleText + UIConstant.SPACE_CHARACTER + getString(R.string.no_test_item));
			testTelephoneListView.setVisibility(View.GONE);
			selectAllButton.setVisibility(View.GONE);
			selectedTestButton.setVisibility(View.GONE);
		}
		
		backKeyButton = (Button) findViewById(R.id.backButton);
		backKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		homeKeyButton = (Button) findViewById(R.id.homeButton);
		homeKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mContext, LauncherUI.class);
				//intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});
	}
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		
		LogMgr.d(LogMgr.TAG,"SensorTestUI onStop");
		
		if(SmartCareSystemApplication.getInstance().getDiagnosticManager().isDiagnosticTesting())
			SmartCareSystemApplication.getInstance().getDiagnosticManager().stopTest();
		
	}
	
	private void listUpdate(int index, int result)
	{
		if( index < 0 ) return;
		
		if(result == COMPLETED) {
			backKeyButton.setClickable(true);
			homeKeyButton.setClickable(true);
			
			SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.SPEECH_TEST_COMPLETE, TextToSpeech.QUEUE_FLUSH, null);
			
			stopTestButton.setVisibility(View.VISIBLE);
			stopTestButton.setText(R.string.test_complete);
			stopTestButton.setOnClickListener(new View.OnClickListener() {	//onCreate()에 중단 기능을 구현했으므로 여기서는 이전 화면으로 가도록 구현해야 함
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					finish();
				}
			});
			
			return;
		}
		
		if(result == SUCCESS) {
			testResultArray[index] = getString(R.string.success);
		}
		else if(result == IGNORE) {
			testResultArray[index] = getString(R.string.ignore);
		}
		else {
			testResultArray[index] = getString(R.string.fail);
		}
		
		mAdapter = new SensorAdapterUI(mContext, testListData, UIConstant.MODE_ONGOING_TEST, getFragmentManager());
		mAdapter.setCompleteTestIndex(index);
		mAdapter.setTestResultArray(testResultArray);
		testTelephoneListView.setAdapter(mAdapter);
		
		if(index >= testTelephoneListView.getChildCount()) {	//리스트가 한 화면에 다 나오지 않는 경우, '무시' 버튼을 누르기 편하게 항목을 이동한다.
			testTelephoneListView.setSelection(index);
		}
	}
	
	@Override
	public void notify(int index, int testId, int result) {
		// TODO Auto-generated method stub
		LogMgr.d(LogMgr.TAG,"notify :: " + index + ", result :: " + result);
		
		if( index < 0 ) return;
		
		Message msg = mHandler.obtainMessage();
		msg.what = NOTIFY_EVENT;
		msg.arg1 = index;
		msg.arg2 = result;
		mHandler.sendMessage(msg);
	}
}
