package com.telefield.smartcaresystem.ui.diagnosis;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.ui.LauncherUI;
import com.telefield.smartcaresystem.ui.UIConstant;
import com.telefield.smartcaresystem.ui.sensor.SensorAdapterUI;

public class CheckSensorUI extends Activity {
	ListView sensorListView;
	SensorAdapterUI mAdapter;
	Context mContext;
	Button btn, backKeyButton, homeKeyButton;
	String[] testSensorNameArray;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.select_sensor_ui);
		
		mContext = this;
		
		btn = (Button) findViewById(R.id.seeRegisterConditionButton);
		btn.setVisibility(View.GONE);
		
		TextView titleTextView = (TextView) findViewById(R.id.titleTextView);
		titleTextView.setText(R.string.sensor_test_at_sensor);
		
		final String[] sensorNameArray = getResources().getStringArray(R.array.sensor_name);
		
		//일단 혈압계는 센서가 아니니 테스트 항목에서 삭제시킨다.
		testSensorNameArray = new String[sensorNameArray.length - 1];
		for(int i=0;i<testSensorNameArray.length;i++) {
			if(sensorNameArray[i].equals("무선혈압계")) {
				continue;
			}
			else {
				testSensorNameArray[i] = sensorNameArray[i];
			}
		}
		
		sensorListView = (ListView) findViewById(R.id.sensorListView);
		mAdapter = new SensorAdapterUI(this, testSensorNameArray, UIConstant.MODE_SEE_SENSOR_FOR_TEST, getFragmentManager());
		sensorListView.setAdapter(mAdapter);
		
		sensorListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mContext, CheckSensorSuBUI.class);
				intent.putExtra(UIConstant.SENSOR_TYPE, testSensorNameArray[position]);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});
		
		backKeyButton = (Button) findViewById(R.id.backButton);
		backKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		homeKeyButton = (Button) findViewById(R.id.homeButton);
		homeKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mContext, LauncherUI.class);
				//intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});
	}
}
