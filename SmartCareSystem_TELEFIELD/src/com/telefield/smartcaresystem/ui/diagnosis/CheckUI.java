package com.telefield.smartcaresystem.ui.diagnosis;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.ui.LauncherUI;

public class CheckUI extends Activity {
	Button backKeyButton, homeKeyButton;
	ListView diagnosisListView;
	DiagnosisAdapterUI adapter;
	Context mContext;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.check_ui);
		
		mContext = this;
		
		TextView titleTextView = (TextView) findViewById(R.id.titleTextView);
		titleTextView.setText(R.string.check);
		
		String[] diagnosisArray = getResources().getStringArray(R.array.diagnosis);
		
		adapter = new DiagnosisAdapterUI(mContext, diagnosisArray);
		
		diagnosisListView = (ListView) findViewById(R.id.diagnosisListView);
		diagnosisListView.setAdapter(adapter);
		
		diagnosisListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				if(position == 0){	//무선 네트워크
					startActivity(new Intent(mContext, CheckWirelessNetworkUI.class));
				}
				else if(position == 1){	//M2M 서버
					startActivity(new Intent(mContext, CheckM2MServerUI.class));
				}
				else if(position == 2){	//댁내 본체
					startActivity(new Intent(mContext, CheckTelehoneUI.class));			
				}
				else if(position == 3){	//댁내 센서
					startActivity(new Intent(mContext, CheckSensorUI.class));
				}
				else if(position == 4){	//본체 연결 진단
					startActivity(new Intent(mContext, CheckTelehoneConnectUI.class));
				}
			}
		});
		
		backKeyButton = (Button) findViewById(R.id.backButton);
		backKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		homeKeyButton = (Button) findViewById(R.id.homeButton);
		homeKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mContext, LauncherUI.class);
				//intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});
	}
	
}
