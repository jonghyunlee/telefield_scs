package com.telefield.smartcaresystem.ui.diagnosis;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.database.ConnectDB;
import com.telefield.smartcaresystem.database.Sensor;
import com.telefield.smartcaresystem.message.SensorID;
import com.telefield.smartcaresystem.ui.LauncherUI;
import com.telefield.smartcaresystem.ui.UIConstant;
import com.telefield.smartcaresystem.ui.sensor.SensorAdapterUI;
import com.telefield.smartcaresystem.util.LogMgr;

public class CheckSensorSuBUI extends Activity {
	ListView sensorListView;
	SensorAdapterUI mAdapter;
	Context mContext;
	Button btn, backKeyButton, homeKeyButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.select_sensor_ui);
		
		mContext = this;
		
		btn = (Button) findViewById(R.id.seeRegisterConditionButton);
		btn.setVisibility(View.GONE);
		
		Intent intent = getIntent();
		
		Bundle bundle = intent.getExtras();
		
		String titleText;
		if(bundle != null) {
			titleText = bundle.getString(UIConstant.SENSOR_TYPE);
		}
		else {	//TestCase의 경우 bundle이 없다. 여기서 직접 text를 설정한다.
			titleText = UIConstant.FIRE_SENSOR;
		}
		
		TextView titleTextView = (TextView) findViewById(R.id.titleTextView);
		titleTextView.setText(titleText);
		
		String sensorName = titleText;
		if(sensorName.equals(UIConstant.DISPLAY_FIRE_SENSOR)) {
			sensorName = UIConstant.FIRE_SENSOR;
		}
		else if(sensorName.equals(UIConstant.DISPLAY_GAS_SENSOR)) {
			sensorName = UIConstant.GAS_SENSOR;
		}
		
		ConnectDB con = ConnectDB.getInstance(this);
		Sensor sensor = new Sensor();
		sensor.setType(SensorID.getSensorType(sensorName));
		
		final List<Sensor> result = con.selectSensor(sensor);
		
		sensorListView = (ListView) findViewById(R.id.sensorListView);
		
		if(result != null && result.size() > 0) {
			LogMgr.d(LogMgr.TAG, "MSG sub" + SensorID.getSensorType(titleText) + " result size : " + result.size());
			sensorListView.setVisibility(View.VISIBLE);
			
			mAdapter = new SensorAdapterUI(this, result, UIConstant.MODE_SEE_SENSOR_FOR_SUB_TEST, getFragmentManager());
			sensorListView.setAdapter(mAdapter);
			
			sensorListView.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(mContext, SensorTestUI.class);
					intent.putExtra(UIConstant.SENSOR_ID_KEY, result.get(position).getSensor_id().intValue());
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(intent);
				}
			});
		}
		else {
			sensorListView.setVisibility(View.GONE);
			TextView emptyTextView = (TextView) findViewById(R.id.emptyTextView);
			emptyTextView.setVisibility(View.VISIBLE);
		}
		
		backKeyButton = (Button) findViewById(R.id.backButton);
		backKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		homeKeyButton = (Button) findViewById(R.id.homeButton);
		homeKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mContext, LauncherUI.class);
				//intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});
	}
	
}
