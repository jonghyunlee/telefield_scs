package com.telefield.smartcaresystem.ui.diagnosis;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.ui.LauncherUI;

public class CheckWirelessNetworkUI extends Activity {
	Button testButton, backKeyButton, homeKeyButton;
	Context mContext;
	String mobile3G;
	String mobileSignalStrength;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.check_wireless_network_ui);
		
		mContext = this;
		
		TelephonyManager manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		manager.listen(mPhoneStateListener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
		
		
		TextView titleTextView = (TextView) findViewById(R.id.titleTextView);
		titleTextView.setText(R.string.wirelss_network_test);
		
		testButton = (Button) findViewById(R.id.testStartButton);
		testButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//테스트 중에는 화면이 계속 켜져 있어야 한다.
				getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
				
				LinearLayout board = (LinearLayout) findViewById(R.id.boardLayout);
				board.removeAllViews();	//TestStart를 2번 이상 누를 시 이전 결과 밑에 테스트 결과가 기록되는 현상이 없도록 한다.
				
				ConnectivityManager connMgr = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
				NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

				mobile3G = mobile.isAvailable() == true ? "OK" : "Fail";
				
				String[] checkLists = mContext.getResources().getStringArray(R.array.check_wireless_network);	//check list
				
				int textViewId = 1;	//텍스트 뷰에 사용할 임시 ID
				
				for(int i=0;i<checkLists.length;i++) {
					TextView testlists = new TextView(mContext);	//테스트 항목과 테스트 결과를 붙일 TextView
					testlists.setId(textViewId);
					testlists.setTextSize(25);
					
					String item = checkLists[i];
					if(item.equals("3G"))
						item = item + " : " + mobile3G;
					else
						item = item + " : " + mobileSignalStrength;
					
					testlists.setText(item);
					board.addView(testlists);
					textViewId++;
				}
				
			}
		});
		
		backKeyButton = (Button) findViewById(R.id.backButton);
		backKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		homeKeyButton = (Button) findViewById(R.id.homeButton);
		homeKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mContext, LauncherUI.class);
				//intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});
	}

	private void setRssiValue(int signalPower) {
//		if (signalPower >= 0 && signalPower <= 31 || signalPower == 99) {
			int dBm = -113 + (2 * signalPower);
//			if (dBm < -110)
//				mobileSignalStrength = "Fail";
//			else
//				mobileSignalStrength = "OK";
			mobileSignalStrength = String.valueOf(dBm);
//		}
	}
	
	PhoneStateListener mPhoneStateListener = new PhoneStateListener(){
		@Override
		public void onSignalStrengthsChanged(SignalStrength signalStrength) {
			super.onSignalStrengthsChanged(signalStrength);
			setRssiValue(signalStrength.getGsmSignalStrength());
		}
	};

	@Override
	protected void onStop() {
		TelephonyManager manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		manager.listen(mPhoneStateListener, PhoneStateListener.LISTEN_NONE);
		super.onStop();
	}
	
	
}
