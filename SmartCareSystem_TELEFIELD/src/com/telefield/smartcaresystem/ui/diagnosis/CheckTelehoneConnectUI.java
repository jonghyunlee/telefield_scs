package com.telefield.smartcaresystem.ui.diagnosis;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.message.MessageID;
import com.telefield.smartcaresystem.ui.LauncherUI;

public class CheckTelehoneConnectUI extends Activity {
	Button testChargeButton, testDischargeButton, backKeyButton, homeKeyButton;
	Context mContext;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.check_telephone_connect);
		
		mContext = this;
		
		TextView titleTextView = (TextView) findViewById(R.id.titleTextView);
		titleTextView.setText(R.string.telephone_connect_test);
		
		
		LinearLayout board = (LinearLayout) findViewById(R.id.boardLayout);
		board.removeAllViews();	//TestStart를 2번 이상 누를 시 이전 결과 밑에 테스트 결과가 기록되는 현상이 없도록 한다.
		
		//여기서부터 테스트 결과를 붙인다.
		int textViewId = 1;	//텍스트 뷰에 사용할 임시 ID
		
		TextView testlists = new TextView(mContext);	//테스트 항목과 테스트 결과를 붙일 TextView
		testlists.setId(textViewId);
		testlists.setTextSize(25);
		testlists.setText(R.string.ftdi_mode_change);
		
		board.addView(testlists);
		//textViewId++;
		
		
		
		testChargeButton = (Button) findViewById(R.id.chargingTestButton);
		testChargeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if( !SmartCareSystemApplication.getInstance().getMessageManager().isFT312DMode())
				{
					SmartCareSystemApplication.getInstance().getGlobalMsgFunc().sendGateWayMsg((byte)MessageID.PHONE_BATT_LOW);
				}
			}
		});
		
		
		
		testDischargeButton = (Button) findViewById(R.id.dischargingTestButton);
		testDischargeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if( SmartCareSystemApplication.getInstance().getMessageManager().isFT312DMode())
				{
					SmartCareSystemApplication.getInstance().getGlobalMsgFunc().sendGateWayMsg((byte)MessageID.PHONE_BATT_FULL);
				}
			}
		});
		
		if( SmartCareSystemApplication.getInstance().getMessageManager().isFT312DMode())
		{
			//현재가 충전 모드 상태이면
			testChargeButton.setClickable(false);
			testChargeButton.setBackgroundResource(R.drawable.bt_02_nor);
			
			testDischargeButton.setClickable(true);
			testDischargeButton.setBackgroundResource(R.drawable.button_confirm_selector);
		}else
		{
			//방전모드 중이면
			testChargeButton.setClickable(true);
			testChargeButton.setBackgroundResource(R.drawable.button_confirm_selector);
			
			testDischargeButton.setClickable(false);
			testDischargeButton.setBackgroundResource(R.drawable.bt_02_nor);
		}
		
		backKeyButton = (Button) findViewById(R.id.backButton);
		backKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});

		homeKeyButton = (Button) findViewById(R.id.homeButton);
		homeKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mContext, LauncherUI.class);
				// intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
				// Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});
	}
}
