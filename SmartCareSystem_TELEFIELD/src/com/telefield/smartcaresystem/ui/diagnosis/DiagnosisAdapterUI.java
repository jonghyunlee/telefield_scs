package com.telefield.smartcaresystem.ui.diagnosis;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.telefield.smartcaresystem.R;

public class DiagnosisAdapterUI extends BaseAdapter {
	Context mContext;
	String[] data;
	
	public DiagnosisAdapterUI(Context mContext, String[] data) {
		this.mContext = mContext;
		this.data = data;
	}
			
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data[position];
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;
		
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		if(view == null)
			view = inflater.inflate(R.layout.child_system_sub_menu, parent, false);
		
		TextView menuTitle = (TextView) view.findViewById(R.id.menuTitle);
		menuTitle.setText(data[position]);
		
		ImageView image = (ImageView) view.findViewById(R.id.menuItemImage);
		image.setVisibility(View.GONE);
		
		TextView info = (TextView) view.findViewById(R.id.info);
		info.setVisibility(View.GONE);
		
		return view;
	}

}
