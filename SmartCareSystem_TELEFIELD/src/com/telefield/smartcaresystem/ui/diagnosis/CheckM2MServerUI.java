package com.telefield.smartcaresystem.ui.diagnosis;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.m2m.CheckM2MConnect;
import com.telefield.smartcaresystem.m2m.ProtocolConfig;
import com.telefield.smartcaresystem.ui.LauncherUI;
import com.telefield.smartcaresystem.util.SmartCarePreference;

public class CheckM2MServerUI extends Activity {
	Button testButton, backKeyButton, homeKeyButton;
	Context mContext;
	CheckM2MConnect mCheckM2MConnect;
	AlertDialog.Builder builder;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.check_wireless_network_ui);
		
		mContext = this;
		
		TextView titleTextView = (TextView) findViewById(R.id.titleTextView);
		titleTextView.setText(R.string.m2m_server_test);

		testButton = (Button) findViewById(R.id.testStartButton);
		testButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// 테스트 중에는 화면이 계속 켜져 있어야 한다.
				getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
				
				LinearLayout board = (LinearLayout) findViewById(R.id.boardLayout);
				board.removeAllViews();	//TestStart를 2번 이상 누를 시 이전 결과 밑에 테스트 결과가 기록되는 현상이 없도록 한다.
				
				CheckServerProgressDialog dialog = new CheckServerProgressDialog();
				dialog.show(getFragmentManager(), "Dialog");
								
				mCheckM2MConnect = new CheckM2MConnect(dialog, board);
				
				SmartCarePreference pref = new SmartCarePreference(mContext, SmartCarePreference.ADDRESS_INFO_FILENAME);
				String IP = pref.getStringValue(SmartCarePreference.M2M_IP, SmartCarePreference.DEFAULT_COMMON_STRING);
				
				mCheckM2MConnect.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, IP.equals(SmartCarePreference.DEFAULT_COMMON_STRING) ? ProtocolConfig.M2M_ADDRESS : IP);
			}
		});

		backKeyButton = (Button) findViewById(R.id.backButton);
		backKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});

		homeKeyButton = (Button) findViewById(R.id.homeButton);
		homeKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mContext, LauncherUI.class);
				// intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
				// Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});
	}
	
	@Override
	protected void onStop() {
		if(mCheckM2MConnect != null)
			mCheckM2MConnect.cancel(true);
		super.onStop();
	}
	
	public class CheckServerProgressDialog extends DialogFragment implements OnClickListener {
		private Button btnCancel;
		
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		    if (getDialog() != null) {
		        getDialog().setCanceledOnTouchOutside(false);
		    }
		    return super.onCreateView(inflater, container, savedInstanceState);
		}
		
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
			LayoutInflater mLayoutInflater = getActivity().getLayoutInflater();
			View view = mLayoutInflater.inflate(R.layout.progress_dialog_bar_type, null);
			
			mBuilder.setView(view);
			
			TextView titleTextView = (TextView) view.findViewById(R.id.title_view);
			titleTextView.setText("테스트 진행중");
			
			btnCancel = (Button) view.findViewById(R.id.btn_cancel);
			btnCancel.setOnClickListener(this);
			
			return mBuilder.create();
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.btn_cancel:
				if(mCheckM2MConnect != null)
					mCheckM2MConnect.cancel(true);
				
				dismiss();
				break;
			}
		}
	}
}
