package com.telefield.smartcaresystem.ui.log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.m2m.ProtocolConfig;
import com.telefield.smartcaresystem.message.MessageID;
import com.telefield.smartcaresystem.util.LogMgr;

/**
 * Log 파일을 읽어서 List로 출력
 * @author Administrator
 *
 */
public class LogHistoryItemViewActivity extends ListActivity {
	
	String file_path;
	private List<String> itemList = new ArrayList<String>();
	ProgressDialog mProgress;

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		
		String title="";
		StringBuffer message = new StringBuffer();
		String line = itemList.get(position);		
		String msg_type;
		byte msg_type_value;
		

		if (line.indexOf(LogMgr.TYPE_STR[LogMgr.GW_SEND]) > 0 || line.indexOf(LogMgr.TYPE_STR[LogMgr.GW_RECEIVE]) > 0) {
			int msg_value = Integer.valueOf(line.substring(55, 57), 16);
			String subMessage = "";
			if(msg_value > ProtocolConfig.PACKET_TYPE.length){
				subMessage = " ACK";
			}
			title = ProtocolConfig.PACKET_TYPE[msg_value & 0x0F] + subMessage;
		} else {
			msg_type = line.substring(67, 69);
			msg_type_value = Byte.parseByte(msg_type, 16);
			if (msg_type_value >= MessageID.MAX_MESSAGE_ID ) 
			{
				title = "UNKNOWN MSG";
			}
			else	title = MessageID.MSG_TYPE_STR[msg_type_value];		
			
			message.append("LENGTH : " + line.substring(52,58));
			message.append("\r\n");
			message.append("SEQ_NUM : " + line.substring(58,60));
			message.append("\r\n");
			message.append("SENSOR_ID : " + line.substring(61,67));
			message.append("\r\n");
			message.append("MESSAGE ID : " + msg_type);
			message.append("\r\n");
			message.append("\r\n");
			
		}
		message.append(line.substring(46));
				
		
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(title);
		alertDialog.setMessage(message);				
		alertDialog.show();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);	
		
		setContentView(R.layout.logitem_detail);

        
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        
        String file_name = bundle.getString("FILE_NAME");
        
        if( file_name == null )
        {
        	Toast.makeText(this, "올바르지 않은 파일입니다.", Toast.LENGTH_SHORT).show();
        	finish();
        	return;
        }	
        
        file_path = Environment.getExternalStorageDirectory() + File.separator + "SmartCareSystem" +  File.separator +"logs" + File.separator + file_name;
        
        //load data
        AsyncLoadFileTask fileLoad = new AsyncLoadFileTask();
		fileLoad.execute(file_path);
		
		Button backButton = (Button) findViewById(R.id.backButton);
		backButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
	}
	
	private class AsyncLoadFileTask extends AsyncTask<String,Integer,Void>
	{		
		@Override
		protected void onPreExecute() {
						
			mProgress = new ProgressDialog(LogHistoryItemViewActivity.this);
			//mProgress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		    mProgress.setTitle("Loading ...");
		    mProgress.setMessage("Wait...");
		    mProgress.setCancelable(false);
		    mProgress.show();
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			//mProgress.setProgress(values[0]);
		}

		@Override
		protected Void doInBackground(String... params) {
			// TODO Auto-generated method stub
			
			File file;
			
			String file_name = params[0];
			
			file = new File(file_name);
			
			long total_size = file.getTotalSpace();
			
			try {
				BufferedReader br = new BufferedReader( new FileReader(file));
				String line ;
				while((line = br.readLine()) != null)
				{
					//정해진 규격만 add 시키자.
					for(int i=1; i <= LogMgr.GW_RECEIVE ; i++ )
					{
						String type = LogMgr.TYPE_STR[i];
						if(line.contains(type))
						{
							itemList.add(line);
							break;
						}
					}					
					
					publishProgress((int)(line.length()*8 / total_size * 100) );
				}
				
				
				br.close();
				
				
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				Toast.makeText(LogHistoryItemViewActivity.this, "파일을 읽는 도중 에러 발생!!", Toast.LENGTH_SHORT).show();
				e.printStackTrace();
				return null;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
			
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			
			 mProgress.dismiss();
			
			 //ArrayAdapter<String> fileList = new ArrayAdapter<String>(LogHistoryItemViewActivity.this, android.R.layout.simple_list_item_1, itemList);		        
		     //setListAdapter(fileList);
			 setListAdapter(new IconicArrayAdapter(LogHistoryItemViewActivity.this, itemList));
			 setSelection(itemList.size()-1);
			
		}

		@Override
		protected void onCancelled() {
			// TODO Auto-generated method stub
			super.onCancelled();
			
			 mProgress.dismiss();
		}
	}
	
	
	private class IconicArrayAdapter extends ArrayAdapter{
		
		Context context;
		LayoutInflater mInflater;
		private List<String> items;

		public IconicArrayAdapter(Context context, List<String> items) {
			super(context,R.layout.logitem_detail, items);
			
			this.context = context;
			this.mInflater = (LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
			this.items = items;
			
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view;
			
			TextView dateView;
			TextView msgTypeView;
			
			if( convertView == null)
			{
				view = mInflater.inflate(R.layout.logitem_row, parent, false);
			}else
			{
				view = convertView;
			}
			
			ImageView image = (ImageView)view.findViewById(R.id.imageView1);
			
			
			String line = items.get(position);
			String dateText = line.substring(0, 23);
			
//			LogMgr.d(LogMgr.TAG,"date " + dateText);			
//			LogMgr.d(LogMgr.TAG,"LINE " + line);
			dateView = (TextView)view.findViewById(R.id.date);
			dateView.setText(dateText);
			msgTypeView = (TextView)view.findViewById(R.id.msg_type);
			
			try{
			
				if (line.indexOf(LogMgr.TYPE_STR[LogMgr.GW_SEND]) > 0 || line.indexOf(LogMgr.TYPE_STR[LogMgr.GW_RECEIVE]) > 0) {
					
					if( line.indexOf(LogMgr.TYPE_STR[LogMgr.GW_SEND]) > 0 )
					{
						image.setImageResource(R.drawable.server_come);
					}else
					{
						image.setImageResource(R.drawable.server_out);
					}
					int msg_type_value = Integer.valueOf(line.substring(55, 57), 16);
					String subMessage = "";
					if(msg_type_value > ProtocolConfig.PACKET_TYPE.length){
						subMessage = " ACK";
					}
					msgTypeView.setText(ProtocolConfig.PACKET_TYPE[msg_type_value & 0x0F] + subMessage);
				} else {
					
					if( line.indexOf(LogMgr.TYPE_STR[LogMgr.CODI_SEND]) > 0 )
					{
						image.setImageResource(R.drawable.telephone_come);
					}else
					{
						image.setImageResource(R.drawable.telephone_out);
					}
					
					String msg_type = line.substring(67, 69);
					byte msg_type_value = Byte.parseByte(msg_type, 16);
					String title ="";
					if (msg_type_value >= MessageID.MAX_MESSAGE_ID ) 
					{
						title = "UNKNOWN MSG";
					}
					else	title = MessageID.MSG_TYPE_STR[msg_type_value];
					
					msgTypeView.setText(title);
				}
			}catch(NumberFormatException e)
			{
				msgTypeView.setText("UNKNOWN MSG");
			}
			return view;
			
		}		
	}

	@Override
	public ListView getListView() {
		// TODO Auto-generated method stub
		return super.getListView();
	}
	

}
