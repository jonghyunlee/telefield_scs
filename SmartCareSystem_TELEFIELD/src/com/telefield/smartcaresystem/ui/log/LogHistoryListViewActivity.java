package com.telefield.smartcaresystem.ui.log;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.telefield.smartcaresystem.R;

public class LogHistoryListViewActivity extends ListActivity {
	
	private List<String> mFileNames = new ArrayList<String>();
	 ListView mFlieListView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.loglist_menu);
		updateFileList();      
        
		Button backButton = (Button) findViewById(R.id.backButton);
		backButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
	}
	
	
	public void updateFileList()
	{
		String path = null;
        
        path = Environment.getExternalStorageDirectory() + File.separator + "SmartCareSystem" +  File.separator +"logs";        
       
        File files = new File(path);
        
        File[] list_items = files.listFiles();
        if( list_items != null)
        {        
	        for(int i=0; i < list_items.length; i++)
	        {
	        	File unit = list_items[i];
	        	mFileNames.add(unit.getName());	        	
	        }
        }

        ArrayAdapter<String> fileList = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mFileNames);
        setListAdapter(fileList);
	}


	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
				
		Intent intent = new Intent(this , LogHistoryItemViewActivity.class);
		intent.putExtra("FILE_NAME", (String)getListAdapter().getItem(position));
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
		
	}
}
