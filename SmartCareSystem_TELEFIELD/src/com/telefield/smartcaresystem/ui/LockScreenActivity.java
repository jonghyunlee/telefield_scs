package com.telefield.smartcaresystem.ui;

import java.io.File;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.telefield.smartcaresystem.GCMIntentHandler;
import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.bluetooth.DeviceScan;
import com.telefield.smartcaresystem.m2m.GatewayDevice;
import com.telefield.smartcaresystem.power.SmartCareSystemPowerManager;
import com.telefield.smartcaresystem.ui.testcase.TestCaseUI;
import com.telefield.smartcaresystem.util.LogMgr;
import com.telefield.smartcaresystem.util.SmartCarePreference;

public class LockScreenActivity extends Activity {
	ImageView tempRssi, isCharge, tempBattery, coordiBattery,cdma_communicate,bluetooth_icon;
	Button tmepReleaseLock;	//Lock를 푸는 임시 버튼
	private FrameLayout centerView;
	public static final int UPDATE_CENTERVIEW = 1;
	PhoneStateListener listener = new PhoneStateListener() {
		@Override
		public void onSignalStrengthsChanged(SignalStrength signalStrength) {
			setRssiValue(signalStrength.getGsmSignalStrength());
			
			super.onSignalStrengthsChanged(signalStrength);
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.lockscreen);
		
		centerView = (FrameLayout) findViewById(R.id.layout_center);
		
		tmepReleaseLock = (Button) findViewById(R.id.tempReleaseLock);
		tmepReleaseLock.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		TelephonyManager telManager  = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
		telManager.listen(listener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
		
		
		/*SmartCarePreference pref = new SmartCarePreference(this, SmartCarePreference.APPLICATION_SETTING_INFO_FILENAME);
		String result = pref.getStringValue(SmartCarePreference.OPENNING_RESULT, UIConstant.DEFAULT_OPENNING_STATE);
		if(result.equals(UIConstant.DEFAULT_OPENNING_STATE)){
			centerView.setBackgroundResource(R.drawable.telefield_lockscreen);
		}*/
		
		TestCaseUI.setHandler(handler);
		GCMIntentHandler.setHandler(handler);
		handler.sendMessage(Message.obtain(handler, UPDATE_CENTERVIEW));
	}
	
	
	
	@Override
	protected void onResume() {
		
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);

		tempBattery = (ImageView) findViewById(R.id.tempBattery);
		tempRssi = (ImageView) findViewById(R.id.tempRssi);
		isCharge = (ImageView) findViewById(R.id.isCharge);
		coordiBattery = (ImageView) findViewById(R.id.coordiBattery);
		cdma_communicate = (ImageView) findViewById(R.id.cdma_communicate);
		bluetooth_icon = (ImageView) findViewById(R.id.bluetooth_icon);
		
		SmartCareSystemPowerManager mgr = SmartCareSystemApplication.getInstance().getPwrMgr();
		int batLevel = mgr.getBatt_level();
		
		LogMgr.d(LogMgr.TAG, "LockScreenActivity.java:onResume:// batLevel : " + batLevel);

		if (batLevel < 20)
			tempBattery.setImageResource(R.drawable.icon_battery_01);
		else if (batLevel >= 20 && batLevel < 40)
			tempBattery.setImageResource(R.drawable.icon_battery_02);
		else if (batLevel >= 40 && batLevel < 60)
			tempBattery.setImageResource(R.drawable.icon_battery_03);
		else if (batLevel >= 60 && batLevel < 80)
			tempBattery.setImageResource(R.drawable.icon_battery_04);
		else if (batLevel >= 80)
			tempBattery.setImageResource(R.drawable.icon_battery_05);

		if (mgr.getBatt_status() == BatteryManager.BATTERY_STATUS_CHARGING) {
			isCharge.setVisibility(View.VISIBLE);
		} else {
			isCharge.setVisibility(View.GONE);
		}
		
		GatewayDevice gatewayDevice = GatewayDevice.getInstance();
		int coordi_batt = gatewayDevice.battery_state;
		
		if(coordi_batt != 0) {
			if (coordi_batt < 20)
				coordiBattery.setImageResource(R.drawable.icon_battery_01);
			else if (coordi_batt >= 20 && coordi_batt < 40)
				coordiBattery.setImageResource(R.drawable.icon_battery_02);
			else if (coordi_batt >= 40 && coordi_batt < 60)
				coordiBattery.setImageResource(R.drawable.icon_battery_03);
			else if (coordi_batt >= 60 && coordi_batt < 80)
				coordiBattery.setImageResource(R.drawable.icon_battery_04);
			else if (coordi_batt >= 80)
				coordiBattery.setImageResource(R.drawable.icon_battery_05);
		}
		else {
			coordiBattery.setImageResource(R.drawable.icon_battery_01);
		}
		
		ConnectivityManager connMgr = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		
		if(mobile.isAvailable() == true)
			cdma_communicate.setVisibility(View.VISIBLE);
		else
			cdma_communicate.setVisibility(View.GONE);
		
		DeviceScan dev = new DeviceScan();
		if(dev.isTurnOn())
			bluetooth_icon.setVisibility(View.VISIBLE);
		else
			bluetooth_icon.setVisibility(View.GONE);
		/*
		ConnectDB con = ConnectDB.getInstance(this);
		Sensor coordi = new Sensor();
		coordi.setSensor_id((long) 0xC001);
		List<Sensor> getCoordi = con.selectSensor(coordi);
		
		if(getCoordi != null && getCoordi.size() > 0) {
			Sensor getSensor = getCoordi.get(0);
			
			Integer coordi_batt = getSensor.getSensor_batt();
			if(coordi_batt == null){
				coordi_batt = 0;
			}
			LogMgr.d(LogMgr.TAG, "LockScreenActivity.java:onResume:// coordi_batt : " + coordi_batt);
			
			if(coordi_batt != null) {
				if (coordi_batt < 20)
					coordiBattery.setImageResource(R.drawable.icon_battery_01);
				else if (coordi_batt >= 20 && coordi_batt < 40)
					coordiBattery.setImageResource(R.drawable.icon_battery_02);
				else if (coordi_batt >= 40 && coordi_batt < 60)
					coordiBattery.setImageResource(R.drawable.icon_battery_03);
				else if (coordi_batt >= 60 && coordi_batt < 80)
					coordiBattery.setImageResource(R.drawable.icon_battery_04);
				else if (coordi_batt >= 80)
					coordiBattery.setImageResource(R.drawable.icon_battery_05);
			}
			else {
				coordiBattery.setImageResource(R.drawable.icon_battery_01);
			}
		}
		*/
		super.onResume();
	}


	@Override
	protected void onNewIntent(Intent intent) {
		LogMgr.d(LogMgr.TAG, "LockScreenActivity.java:onNewIntent:// intent : " + intent);
		
		if(intent == null ) return;

		LogMgr.d(LogMgr.TAG, "LockScreenActivity.java:onNewIntent:// intent.getAction() : " + intent.getAction());

		
		String action = intent.getAction();
		
		if( action !=null && action.equals("android.intent.ACTION.LOCKSCREEN_RELEASE"))
		{
			SmartCarePreference pref = new SmartCarePreference(this, SmartCarePreference.APPLICATION_SETTING_INFO_FILENAME);
			boolean handsetLockOnOff = pref.getBooleanValue(SmartCarePreference.HANDSEST_LOCK_ON_OFF, false);
			//
			if(handsetLockOnOff) {
				Intent i = new Intent(LockScreenActivity.this, /*SettingPasswordUI.class*/PasswordSelectUI.class);
				i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(i);
			}
			finish();
		}	
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)  {
		//Home button, Power button을 제외한 나머지 버튼은 작동이 안된다
		return true;
		
	}
	
	private void setRssiValue(int signalPower) {
		
		if(signalPower > 0 && signalPower <= 31) {
			int dBm = -113 + (2 * signalPower);
			if(dBm <= -113)
				tempRssi.setImageResource(R.drawable.icon_rssi_05);
			else if(dBm > -113 && dBm <= -98)
				tempRssi.setImageResource(R.drawable.icon_rssi_01);
			else if(dBm > -98 && dBm <= -83)
				tempRssi.setImageResource(R.drawable.icon_rssi_02);
			else if(dBm > -83 && dBm <= -68)
				tempRssi.setImageResource(R.drawable.icon_rssi_03);
			else if(dBm > -68 && dBm <= -53)
				tempRssi.setImageResource(R.drawable.icon_rssi_04);
			
		}else if(signalPower == 0 || signalPower == 99){
			tempRssi.setImageResource(R.drawable.icon_rssi_05);
		}
		
		else {
			tempRssi.setImageResource(R.drawable.icon_rssi_05);
		}
		
		ConnectivityManager connMgr = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		
		if(mobile.isAvailable() == true)
			tempRssi.setVisibility(View.VISIBLE);
		else
			tempRssi.setVisibility(View.GONE);
	}
	
	Handler handler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			int message = msg.what;
			switch (message) {
			case UPDATE_CENTERVIEW:
				
				String filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/SmartCareSystem/image/lockscreenBack.png";
				File file = new File(filePath);
				Bitmap bitmap;
				int isBitmap = 0;
				if(file.exists()){//
					BitmapFactory.Options option = new BitmapFactory.Options();
					option.outWidth = 300;
					option.outHeight = 460;
					bitmap = BitmapFactory.decodeFile(filePath, option);
					isBitmap = bitmap.getByteCount();
					
					//10보다 작으면 이미지가 없다고 판단한다...
					if(isBitmap <= 10){
						centerView.setBackgroundResource(R.drawable.telefield_lockscreen);
					}else{
						Drawable drawable = new BitmapDrawable(getResources(), bitmap);
						centerView.setBackground(drawable);
					}
				}else{
					centerView.setBackgroundResource(R.drawable.telefield_lockscreen);
				}
//				Toast.makeText(getApplicationContext(), filePath + "....isBitmap : " + isBitmap, Toast.LENGTH_LONG).show();
				break;

			default:
				break;
			}
			
		};
	};
}
