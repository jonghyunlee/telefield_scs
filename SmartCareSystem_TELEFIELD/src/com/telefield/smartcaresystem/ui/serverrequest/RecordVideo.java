package com.telefield.smartcaresystem.ui.serverrequest;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.widget.Toast;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.util.LogMgr;

public class RecordVideo extends Activity implements Callback {
	private SurfaceHolder surfaceHolder;
	private SurfaceView surfaceView;
	private MediaRecorder mrec = new MediaRecorder();
	private Timer recordingTimer;
	private Camera mCamera;
	
	private final int RECORDING_TIME = 10 * 1000;	//10초
	private final int WIDTH = 640;
	private final int HEIGHT = 480;
	private final String MP4 = ".mp4";
	private static String PATH = "";
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.camera_view);
		
		if(Camera.getNumberOfCameras() > 1) {	//카메라가 2개 이상 있는 경우(전면, 후면, 그 외)
			surfaceView = (SurfaceView) findViewById(R.id.surfaceView);
			surfaceHolder = surfaceView.getHolder();
			surfaceHolder.addCallback(this);
			//surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
			
			mCamera = Camera.open(1);
		}
		else {
			return;
		}
	}
	
	private void initRecording() throws IllegalStateException, IOException {
		if (mrec == null)
			mrec = new MediaRecorder(); // Works well
		
		mCamera.unlock();

		mrec.setCamera(mCamera);
		mrec.setPreviewDisplay(surfaceHolder.getSurface());
		mrec.setVideoSource(MediaRecorder.VideoSource.CAMERA);
		mrec.setAudioSource(MediaRecorder.AudioSource.MIC);
		mrec.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_LOW));
		
		String sdcard = Environment.getExternalStorageState();
		File file = null;
		
		if (!sdcard.equals(Environment.MEDIA_MOUNTED)) {
			// SD카드가 마운트되어있지 않음
			file = Environment.getRootDirectory();
		}
		else {
			// SD카드가 마운트되어있음
			file = Environment.getExternalStorageDirectory();
		}
		
		PATH = file.getAbsolutePath() + "/SmartCareSystem/Recorder";
		
		file = new File(PATH);
		if (!file.exists()) {
			// 디렉토리가 존재하지 않으면 디렉토리 생성
			file.mkdirs();
		}
		
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd_HHmmss");
		Date date = new Date();
		PATH = PATH + "/" + format.format(date) + MP4;
		
		LogMgr.d(LogMgr.TAG,"Recorded file path = "+PATH);
		
		mrec.setOutputFile(PATH);
		mrec.setMaxDuration(RECORDING_TIME); // 10 seconds
		//mrec.setMaxFileSize(5000000); // Approximately 5 megabytes
		mrec.setVideoSize(WIDTH, HEIGHT);
		// mrec.setVideoFrameRate(15);
		mrec.prepare();
	}

	protected void startRecording() throws IOException {
		mrec.start();
	}
	
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		try {
			initRecording();
			startRecording();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		recordingTimer = new Timer();		
		recordingTimer.schedule(new TimerTask() {			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				if (mrec != null) {
					mrec.stop();
				}
				finish();
			}
		}, RECORDING_TIME);
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		if (mCamera != null) {
			Camera.Parameters parameters = mCamera.getParameters();
			parameters.setPreviewSize(WIDTH, HEIGHT);
			mCamera.setParameters(parameters);
		}
		
		else {
			Toast.makeText(getApplicationContext(), "Camera not available!", Toast.LENGTH_SHORT).show();
			finish();
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		if (mrec != null) {
			mrec.stop();
			mrec.release();
			mrec = null;
		}
		
		mCamera.stopPreview();
		mCamera.release();
		mCamera = null;
	}
	
	public static String getPath() {
		return PATH;
	}
	
//	ex> 사용법
//	startActivity(new Intent(this, RecordVideo.class));	//서버 요청
//	Timer recordingTimer = new Timer();
//	recordingTimer.schedule(new TimerTask() {			
//		@Override
//		public void run() {
//			// TODO Auto-generated method stub
//			LogMgr.d(LogMgr.TAG,"Video file path = "+RecordVideo.getPath());	//녹화한 파일 경로
//			//TODO 
//		}
//	}, 11 * 1000);
}
