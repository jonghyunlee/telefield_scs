package com.telefield.smartcaresystem.ui.serverrequest;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Environment;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.telefield.server.util.ftp.FtpConnect;
import com.telefield.server.util.ftp.FtpUtils;
import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.ftp.AsyncFtpFileUploader;
import com.telefield.smartcaresystem.ftp.FtpUploadVO;
import com.telefield.smartcaresystem.util.LogMgr;

public class TakePicture extends Activity implements SurfaceHolder.Callback {
	Camera mCamera;
	SurfaceView sv;
	SurfaceHolder surfaceHolder;
	
	private final int WIDTH = 640;
	private final int HEIGHT = 480;
	private final String JPG = ".jpg";
	private static String PATH = "";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.camera_view);
		
		if(Camera.getNumberOfCameras() > 1) {	//카메라가 2개 이상 있는 경우(전면, 후면, 그 외)
			sv = (SurfaceView) findViewById(R.id.surfaceView);
			surfaceHolder = sv.getHolder();
			surfaceHolder.addCallback(this);
		}
		else {
			return;
		}
	}
	
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		// TODO Auto-generated method stub
		Camera.Parameters parameters = mCamera.getParameters();
		parameters.setPreviewSize(WIDTH, HEIGHT);
		
		mCamera.setParameters(parameters);
		mCamera.startPreview();
		
		Camera.PictureCallback mCall = new Camera.PictureCallback() {
			@Override
			public void onPictureTaken(byte[] data, Camera camera) {
				String sdcard = Environment.getExternalStorageState();
				File file = null;
				
				if (!sdcard.equals(Environment.MEDIA_MOUNTED)) {
					// SD카드가 마운트되어있지 않음
					file = Environment.getRootDirectory();
				}
				else {
					// SD카드가 마운트되어있음
					file = Environment.getExternalStorageDirectory();
				}
				
				PATH = file.getAbsolutePath() + "/SmartCareSystem/Picture";
				
				file = new File(PATH);
				if (!file.exists()) {
					// 디렉토리가 존재하지 않으면 디렉토리 생성
					file.mkdirs();
				}
				
				SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd_HHmmss");
				Date date = new Date();
				PATH = PATH + "/" + format.format(date) + JPG;
				
				LogMgr.d(LogMgr.TAG,"Picture file path = "+PATH);
				
				FileOutputStream out = null;
			    try {
					out = new FileOutputStream(PATH);
					out.write(data);
					out.close();
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			    
				try {
					String gateWayNumber = SmartCareSystemApplication.getInstance().getMyPhoneNumber();
					final String save_file_name = "["+format.format(date)+"]["+gateWayNumber +"]emergency_picture"+ JPG;	
					LogMgr.d("PICTURE","121212");
				    new AsyncFtpFileUploader().execute(new FtpUploadVO(data,"emergency",save_file_name),null,null);
				    LogMgr.d("PICTURE","1222222222");
				} catch (Exception e) {
					LogMgr.d("emergency_picture_upload",e.getMessage());
				}
				
//			    SmartCareSystemApplication.getInstance().getGlobalMsgFunc().insertCamData(PATH);
				finish();
			}
		};
		
		mCamera.takePicture(null, null, mCall);
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		mCamera = Camera.open(1);
		
		try {
			mCamera.setPreviewDisplay(holder);
		} catch (IOException exception) {
			mCamera.release();
			mCamera = null;
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		mCamera.stopPreview();
		mCamera.release();
		mCamera = null;
	}
	
	public static String getPath() {
		return PATH;
	}

//	ex> 사용법
//	startActivity(new Intent(this, TakePicture.class));	//서버 요청
//	Thread thread = new Thread() {
//		@Override
//		public void run() {
//			while(true) {
//				if(!(TakePicture.getPath() != null &&TakePicture.getPath().equals(""))) {
//					try {
//						Thread.sleep(300);	//시간차 문제로 진짜 Path를 가져오지 못한다.
//					} catch (InterruptedException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//					LogMgr.d(LogMgr.TAG,"Picture file path(real) = "+TakePicture.getPath());	//녹화한 파일 경로
//					//TODO 
//					break;
//				}
//			}
//		}
//	};
//	thread.start();
}
