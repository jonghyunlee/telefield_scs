package com.telefield.smartcaresystem.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.ui.care.CareComeOutUI;
import com.telefield.smartcaresystem.ui.common.ResultDialogUI;
import com.telefield.smartcaresystem.ui.diagnosis.CheckUI;
import com.telefield.smartcaresystem.ui.opening.OpenningUI;
import com.telefield.smartcaresystem.ui.sensor.SelectSensorUI;
import com.telefield.smartcaresystem.ui.statistics.SelectStatisticsUI;
import com.telefield.smartcaresystem.ui.system.SystemSettingUI;
import com.telefield.smartcaresystem.util.SmartCarePreference;

public class SettingMenuUI extends Activity implements OnClickListener {
	int[] buttonIDs = {R.id.systemButton, R.id.register_delete_Button, /*R.id.inquiryButton,*/ R.id.openingButton, R.id.checkButton, R.id.statisticsButton, R.id.forCareButton, /*R.id.backButton, */R.id.homeButton};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting_menu_ui);
		
		for(int i=0;i<buttonIDs.length;i++) {
			Button button = (Button) findViewById(buttonIDs[i]);
			button.setOnClickListener(this);
		}
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()) {
			case R.id.register_delete_Button:
				startActivity(new Intent(this, SelectSensorUI.class));
				break;
			case R.id.openingButton:
				SmartCarePreference pref = new SmartCarePreference(this, SmartCarePreference.ADDRESS_INFO_FILENAME);
				String address = pref.getStringValue(SmartCarePreference.M2M_IP, null);			
				
				//Preference에 값이 존재하지 않으면, Default 값을 넣어준 후, 넣은 값을 다시 가져온다.
				if(address == null || address.isEmpty()){
					pref.setStringValue(SmartCarePreference.M2M_IP, UIConstant.DEFAULT_SERVER_IP);
					address = pref.getStringValue(SmartCarePreference.M2M_IP, null);	
				}
				
				if(address == null || address.isEmpty()){
					ResultDialogUI dialog = ResultDialogUI.newInstance(ResultDialogUI.MODE_OPENNING_ERROR);
					dialog.setTitle(getResources().getString(R.string.CrashErrorMessage));
					dialog.show(getFragmentManager(), "Dialog");				
				}
				else
					startActivity(new Intent(this, OpenningUI.class));
				break;
			case R.id.systemButton:
				startActivity(new Intent(this, SystemSettingUI.class));
				break;
			case R.id.forCareButton:
				startActivity(new Intent(this, CareComeOutUI.class));
				break;
			case R.id.statisticsButton:
				startActivity(new Intent(this, SelectStatisticsUI.class));
				break;
			case R.id.checkButton:
				startActivity(new Intent(this, CheckUI.class));
				break;
			/*case R.id.backButton:
				onBackPressed();
				break;*/
			case R.id.homeButton:
				Intent intent = new Intent(this, LauncherUI.class);
				//intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
				break;
		}
	}
}
