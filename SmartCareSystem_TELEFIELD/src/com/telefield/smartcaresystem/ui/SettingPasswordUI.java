package com.telefield.smartcaresystem.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.ui.hiddenmenu.HiddenMenuSettingUI;
import com.telefield.smartcaresystem.ui.log.LogHistoryListViewActivity;
import com.telefield.smartcaresystem.ui.testcase.TestCaseUI;

public class SettingPasswordUI extends Activity {
	EditText pw;
	Context mContext;
	Button homeKeyButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting_ui);
        
		//0x11 플래그로 이 Activity에 이동될 때는 LCD가 ON 되어야 한다.
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
		
		mContext = this;
		
		pw = (EditText) findViewById(R.id.password);
		pw.addTextChangedListener(textWatcherInput);
		
		PasswordTransformationMethod passwdtm = new PasswordTransformationMethod();
		pw.setTransformationMethod(passwdtm);
		
		homeKeyButton = (Button) findViewById(R.id.homeButton);
		homeKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mContext, LauncherUI.class);
				//intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});
	}
	
	TextWatcher textWatcherInput = new TextWatcher() {
		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			// TODO Auto-generated method stub
			if(pw.getText().toString().equals(UIConstant.INIT_PASSWORD_NUM)) {
				Intent intent = new Intent(mContext, SettingMenuUI.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
				finish();
			}
			else if(pw.getText().toString().equals(UIConstant.DEBUG_PASSWORD_NUM)){
				Intent intent = new Intent(mContext, DebugUI.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
				finish();
			}
			else if(pw.getText().toString().equals(UIConstant.MESSAGE_VIEW_NUM)){
				Intent intent = new Intent(mContext, LogHistoryListViewActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
				finish();
			}
			else if(pw.getText().toString().equals(UIConstant.HIDDEN_PASSWORD_NUM)){
				Intent intent = new Intent(mContext, HiddenMenuSettingUI.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
				finish();
			}
			else if(pw.getText().toString().equals(UIConstant.TEST_CASE_NUM)){
				Intent intent = new Intent(mContext, TestCaseUI.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
				finish();
			}
			else if(pw.getText().toString().equals(UIConstant.MAIN_NUM)){
				Intent intent = new Intent(mContext, LauncherUI.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
				finish();
			}
		}
		
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			
		}
	};
}
