package com.telefield.smartcaresystem.ui.care;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.database.Care;
import com.telefield.smartcaresystem.database.ConnectDB;
import com.telefield.smartcaresystem.diagnostic.FireSensorDiagnostic;
import com.telefield.smartcaresystem.ui.LauncherUI;
import com.telefield.smartcaresystem.ui.common.CalendarViewDialogUI;
import com.telefield.smartcaresystem.util.LogMgr;

public class CarePresentConditionUI extends Activity implements OnClickListener {
	EditText startTime, endTime;
	TextView titleTextview, emptyTextView;
	ListView result;
	CarePresentConditionAdapterUI mAdapter;
	List<Care> resultCare;
	String startT;
	String endT;
	
	Date startDate;
	Date endDate;
	
	ConnectDB con;
	boolean flag = true;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.care_present_condition);
		
		titleTextview = (TextView) findViewById(R.id.titleTextView);
		titleTextview.setText(R.string.care_come_out_condition);
		
		emptyTextView = (TextView) findViewById(R.id.emptyTextView);
		
		startTime = (EditText) findViewById(R.id.startTime);
		startTime.setFocusable(false);
		startTime.setFocusableInTouchMode(false);
		startTime.setOnClickListener(this);
		endTime = (EditText) findViewById(R.id.endTime);
		endTime.setFocusable(false);
		endTime.setFocusableInTouchMode(false);
		endTime.setOnClickListener(this);
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, -10);
		
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		endTime.setHint("ex>" + format.format(new Date()));
		startTime.setHint("ex>" + format.format(cal.getTime()));
		
		int[] buttonIDs = {R.id.searchButton, R.id.backButton, R.id.homeButton};
		
		for(int i=0;i<buttonIDs.length;i++) {
			Button btn = (Button) findViewById(buttonIDs[i]);
			btn.setOnClickListener(this);
		}
		
		result = (ListView) findViewById(R.id.registerConditionListView);
		result.setVisibility(View.GONE);
		con = ConnectDB.getInstance(this);
		
		/**
		 * 최초에 돌보미 목록 안나오던 문제점 수정
		 */
		SimpleDateFormat firstFormat = new SimpleDateFormat("yyyyMMddHHmmss");		
		try {
			startDate = firstFormat.parse(firstFormat.format(cal.getTime()));
			endDate = firstFormat.parse(firstFormat.format(new Date()));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		resultCare = con.selectCare(startDate, endDate);
		if(resultCare.size() == 0) {
			result.setVisibility(View.GONE);
			emptyTextView.setVisibility(View.VISIBLE);
		}
		else {
			mAdapter = new CarePresentConditionAdapterUI(this, resultCare);
			
			result.setAdapter(mAdapter);
			result.setVisibility(View.VISIBLE);
			
			emptyTextView.setVisibility(View.GONE);
		}
		
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()) {
			case R.id.startTime:
				CalendarViewDialogUI dialog = new CalendarViewDialogUI(startTime, endTime, R.id.startTime);
				dialog.show(getFragmentManager(), "Dialog");
				break;
			case R.id.endTime:
				CalendarViewDialogUI dialog1 = new CalendarViewDialogUI(startTime, endTime, R.id.endTime);
				dialog1.show(getFragmentManager(), "Dialog");
				break;
			case R.id.searchButton:
				String startT = startTime.getText().toString();
				String endT = endTime.getText().toString();
				
				if((startT == null || startT.equals("")) || (endT == null || endT.equals(""))) {
					Toast.makeText(this, R.string.not_input_section, Toast.LENGTH_SHORT).show();
				}
				else {
					SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
					startDate = null;
					endDate = null;
					
					try {
						startDate = format.parse(startT);
						endDate = format.parse(endT);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						LogMgr.d(LogMgr.TAG, "Occur ParseException");
						Toast.makeText(this, R.string.wrong_date_format, Toast.LENGTH_SHORT).show();
						break;
					}
					
					Date now = new Date();
					String nowT = format.format(now);
					
					if(nowT.equals(endT)) {
						resultCare = con.selectCare(startDate, now);	//현재 날짜로 방문/퇴실 처리된 돌보미가 endDate로는 검색 안되는 문제 발생
					}
					else {
						resultCare = con.selectCare(startDate, endDate);
					}
					
					if(resultCare.size() == 0) {
						result.setVisibility(View.GONE);
						emptyTextView.setVisibility(View.VISIBLE);
					}
					else {
						mAdapter = new CarePresentConditionAdapterUI(this, resultCare);
						
						result.setAdapter(mAdapter);
						result.setVisibility(View.VISIBLE);
						
						emptyTextView.setVisibility(View.GONE);
					}
				}
				
				break;
			case R.id.backButton:
				onBackPressed();
				break;
			case R.id.homeButton:
				Intent intent = new Intent(this, LauncherUI.class);
				//intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
				break;
		}
	}
}
