package com.telefield.smartcaresystem.ui.care;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.database.Care;

public class CarePresentConditionAdapterUI extends BaseAdapter {
	Context mContext;
	List<Care> data;
	
	public CarePresentConditionAdapterUI(Context mContext, List<Care> data) {
		this.mContext = mContext;
		this.data = data;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if(data != null)
			return data.size();
		else
			return 0;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;
		
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		if(view == null)
			view = inflater.inflate(R.layout.child_list_item_care_present_condition, parent, false);
		
		TextView care = (TextView) view.findViewById(R.id.careTextView);
		TextView time = (TextView) view.findViewById(R.id.timeTextView);
		TextView action = (TextView) view.findViewById(R.id.actionTextView);
		
		if(data != null) {
			care.setText(Long.toString(data.get(position).getSensor_ID()));
			
			int doing = data.get(position).getAction();
			if(doing == 1) {
				action.setText(mContext.getResources().getString(R.string.visit));
			}
			else if(doing == 0) {
				action.setText(mContext.getResources().getString(R.string.out));
			}
			
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			String date = format.format(data.get(position).getTime());
			time.setText(date);
		}
		
		return view;
	}
}
