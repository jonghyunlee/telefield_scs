package com.telefield.smartcaresystem.ui.care;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.ui.LauncherUI;
import com.telefield.smartcaresystem.ui.common.ConfirmDialogUI;
import com.telefield.smartcaresystem.ui.diagnosis.DiagnosisAdapterUI;
import com.telefield.smartcaresystem.util.LogMgr;

public class CareComeOutUI extends Activity {
	Context mContext;
	ListView careListView;
	DiagnosisAdapterUI adapter;
	Button backKeyButton, homeKeyButton;
	TextView mTitleView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.care_come_out_ui);
		
		mContext = this;
		
		String[] careArray = getResources().getStringArray(R.array.care);
		
		adapter = new DiagnosisAdapterUI(mContext, careArray);
		
		mTitleView = (TextView) findViewById(R.id.titleTextView);
		mTitleView.setText(R.string.care_title);
		careListView = (ListView) findViewById(R.id.careListView);
		careListView.setAdapter(adapter);
		careListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				if(position == 0) {
					ConfirmDialogUI dialog = ConfirmDialogUI.newInstance(ConfirmDialogUI.MODE_COME_CARE);
					dialog.setTitle(mContext.getResources().getString(R.string.come_care));
					dialog.show(getFragmentManager(), "Dialog");
				}
				else if(position == 1) {
					ConfirmDialogUI dialog = ConfirmDialogUI.newInstance(ConfirmDialogUI.MODE_OUT_CARE);
					dialog.setTitle(mContext.getResources().getString(R.string.out_care));
					dialog.show(getFragmentManager(), "Dialog");
				}
				else if(position == 2) {
					startActivity(new Intent(mContext, CarePresentConditionUI.class));
				}
			}
		});
		
		
		backKeyButton = (Button) findViewById(R.id.backButton);
		backKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		homeKeyButton = (Button) findViewById(R.id.homeButton);
		homeKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mContext, LauncherUI.class);
				//intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});
	}
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub	
		super.onStop();
		
		//SmartCareSystemApplication.getInstance().getSensorRegisterFunc().removeEventListener(mHandler);
		
		LogMgr.d(LogMgr.TAG , " CareComeOutUI onStop()");
	}
}
