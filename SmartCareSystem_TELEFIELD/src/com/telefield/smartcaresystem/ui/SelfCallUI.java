package com.telefield.smartcaresystem.ui;

import android.app.Activity;
import android.os.Bundle;
import android.view.WindowManager;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.util.SelfCall;

public class SelfCallUI extends Activity {
	
	SelfCall selfCall;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.selfcall_ui);
		
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
		
		selfCall = new SelfCall(this);
		
		SmartCareSystemApplication.getInstance().setSelfCall(selfCall);
		
		selfCall.startCall();
	}
	
	public void selfCallExit()
	{
		SmartCareSystemApplication.getInstance().setSelfCall(null);
		finish();
	}
	
	@Override
	public void onBackPressed() {	//실제는 Back Key가 없어서 사용하지 않지만, Debug 과정에서 필요하여 추가
		// TODO Auto-generated method stub
		super.onBackPressed();
		
		selfCall.stopSelfCall();
	}
}
