package com.telefield.smartcaresystem.ui.statistics;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.database.BloodPressure;
import com.telefield.smartcaresystem.database.ConnectDB;
import com.telefield.smartcaresystem.database.SensorHistory;
import com.telefield.smartcaresystem.message.SensorID;
import com.telefield.smartcaresystem.ui.LauncherUI;
import com.telefield.smartcaresystem.ui.UIConstant;
import com.telefield.smartcaresystem.util.LogMgr;

public class SensorStatisticsUI extends ListActivity {
	TextView emptyView;
	private Button backKeyButton, homeKeyButton;
	private Context mContext;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.select_statistic_ui);
		mContext = this;

		final TextView tv = (TextView) findViewById(R.id.titleTextView);
		emptyView = (TextView) findViewById(R.id.emptyView);
		
		int extra_type = getIntent().getIntExtra(SelectStatisticsUI.EXTRA_TYPE, 0);

		if (extra_type == SensorID.FIRE_SENSOR_TYPE.getSensorType()){
			tv.setText(getResources().getString(R.string.fire_statistics_title));
			emptyView.setText(getResources().getString(R.string.fire_data_empty));
		}
		else if (extra_type == SensorID.GAS_SENSOR_TYPE.getSensorType()){
			tv.setText(getResources().getString(R.string.gas_statistics_title));
			emptyView.setText(getResources().getString(R.string.gas_data_empty));
		}
		else if(extra_type == UIConstant.TYPE_BLOOD_PRESSURE){
			tv.setText(getResources().getString(R.string.blood_pressure_title));
			emptyView.setText(getResources().getString(R.string.bloodpressure_data_empty));
		}

		ConnectDB db = ConnectDB.getInstance(this);
		Calendar date = new GregorianCalendar();
		Date current, before;

		current = date.getTime();
		date.add(Calendar.MONTH, -3); //Jong 1개월 전을 3개월 전으로 수정( -1 -> -3 )
		before = date.getTime();
		ArrayList<?> dataList = new ArrayList<>();

		LogMgr.d(LogMgr.TAG, "SensorStatisticsUI.java:onCreate:// extra_type : " + extra_type);

		if (extra_type == SensorID.FIRE_SENSOR_TYPE.getSensorType() || (extra_type == SensorID.GAS_SENSOR_TYPE.getSensorType())) {
			dataList = (ArrayList<SensorHistory>) db.selectSensorHistory(extra_type, before, current);
		}
		else if(extra_type == UIConstant.TYPE_BLOOD_PRESSURE){
			dataList = (ArrayList<BloodPressure>) db.selectBloodPressure(before, current);
			LogMgr.d(LogMgr.TAG, "SensorStatisticsUI.java:onCreate:// dataList : " + dataList);

		}
		
		if (dataList.size() == 0) {
			emptyView.setVisibility(TextView.VISIBLE);

		} else {
			StatisticsAdapterUI adapter = new StatisticsAdapterUI(this, R.layout.adapter_statistics, dataList, extra_type);
			setListAdapter(adapter);
		}
		
		backKeyButton = (Button) findViewById(R.id.backButton);
		backKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});

		homeKeyButton = (Button) findViewById(R.id.homeButton);
		homeKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mContext, LauncherUI.class);
				// intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
				// Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});
	}
}
