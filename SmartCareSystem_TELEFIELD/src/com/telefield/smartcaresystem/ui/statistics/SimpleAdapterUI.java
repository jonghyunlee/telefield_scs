package com.telefield.smartcaresystem.ui.statistics;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.telefield.smartcaresystem.R;

public class SimpleAdapterUI extends ArrayAdapter<String> {

	public SimpleAdapterUI(Context context, int resource) {
		super(context, resource);
		this.mContext = context;
	}

	public SimpleAdapterUI(Context context, int resource, String[] strings) {
		super(context, resource, strings);
		this.mContext = context;
		this.items = strings;
	}

	private String[] items;
	private Context mContext;

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View v = convertView;
		if (v == null) {
			LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.simple_child_list, null);
		}
		TextView tt = (TextView) v.findViewById(R.id.toptext);

		if (items != null) {
			String p = items[position];

			tt.setText(p);
			return v;
		} else {
			return super.getView(position, convertView, parent);
		}
	}
}
