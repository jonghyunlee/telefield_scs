package com.telefield.smartcaresystem.ui.statistics;

import java.io.File;
import java.lang.reflect.InvocationTargetException;

import android.app.Application;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.telefield.smartcaresystem.Constant;
import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.database.ConnectDB;
import com.telefield.smartcaresystem.firmware.ApplicationManager;
import com.telefield.smartcaresystem.message.SensorID;
import com.telefield.smartcaresystem.ui.LauncherUI;
import com.telefield.smartcaresystem.ui.UIConstant;

public class SelectStatisticsUI extends ListActivity implements OnItemClickListener {
	SimpleAdapterUI mArrayAdapter;
	ListView mListView;
	public static final String EXTRA_TYPE = "type";
	private Button backKeyButton, homeKeyButton;
	private Context mContext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.select_statistic_ui);

		mContext = this;

		final TextView tv = (TextView) findViewById(R.id.titleTextView);
		tv.setText(R.string.statistics);
		mArrayAdapter = new SimpleAdapterUI(this, R.layout.simple_child_list, getResources().getStringArray(R.array.statistic_item));
		// mArrayAdapter = new ArrayAdapter<String>(this,
		// R.layout.simple_child_list,
		// getResources().getStringArray(R.array.statistic_item));

		setListAdapter(mArrayAdapter);
		getListView().setOnItemClickListener(this);

		backKeyButton = (Button) findViewById(R.id.backButton);
		backKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});

		homeKeyButton = (Button) findViewById(R.id.homeButton);
		homeKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mContext, LauncherUI.class);
				// intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
				// Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		Intent intent;
		switch (position) {
		case 0:
			intent = new Intent(this, SensorStatisticsUI.class);
			intent.putExtra(EXTRA_TYPE, SensorID.FIRE_SENSOR_TYPE.getSensorType());
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
			break;
		case 1:
			intent = new Intent(this, SensorStatisticsUI.class);
			intent.putExtra(EXTRA_TYPE, SensorID.GAS_SENSOR_TYPE.getSensorType());
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
			break;
		case 2:
			startActivity(new Intent(this, ActivityStatisticsUI.class));
			break;
		case 3:
			intent = new Intent(this, SensorStatisticsUI.class);
			intent.putExtra(EXTRA_TYPE, UIConstant.TYPE_BLOOD_PRESSURE);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
			break;
		case 4:
			ConnectDB db = ConnectDB.getInstance(this);
			db.deleteAllActivityData();
			db.deleteAllSensorHistory();
			db.deleteAllSensor();
			db.deleteAllCare();
			db.deleteAllBloodPressure();
			db.insertTData();
			break;
		}
	}
}
