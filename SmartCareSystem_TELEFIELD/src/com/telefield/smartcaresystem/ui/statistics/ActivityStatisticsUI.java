package com.telefield.smartcaresystem.ui.statistics;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.TimeSeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;
import org.achartengine.tools.ZoomEvent;
import org.achartengine.tools.ZoomListener;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint.Align;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.database.ActivityData;
import com.telefield.smartcaresystem.database.ConnectDB;
import com.telefield.smartcaresystem.ui.LauncherUI;
import com.telefield.smartcaresystem.util.LogMgr;

public class ActivityStatisticsUI extends Activity {
	SimpleDateFormat format;

	private static final String TIME_HOUR = "dd일 HH시";
	private static final String TIME_MIN = "HH시";
	private static final String TIME_DAY = "MM월 dd일";
	private static final int COLOR = Color.argb(100, 0, 128, 0);
	private static final PointStyle POINTSTYLE = PointStyle.CIRCLE;

	private TimeSeries mDaySeries;
	private XYSeries mHourSeries;
	private XYMultipleSeriesRenderer hourRenderer;
	private XYMultipleSeriesRenderer dailyRenderer;

	Button backKeyButton, homeKeyButton;
	Context mContext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.statistics_ui);

		mContext = this;

		final TextView tv = (TextView) findViewById(R.id.titleTextView);
		tv.setText(R.string.activity_statistics);
		
		TabHost tabHost = (TabHost) findViewById(android.R.id.tabhost);
		tabHost.setup();

		TabSpec spec1 = tabHost.newTabSpec("Tab1").setContent(R.id.tab1).setIndicator(getString(R.string.tab1));
		tabHost.addTab(spec1);
		TabSpec spec2 = tabHost.newTabSpec("Tab2").setContent(R.id.tab2).setIndicator(getString(R.string.tab2));
		tabHost.addTab(spec2);

		tabHost.getTabWidget().getChildAt(0).getLayoutParams().height = 50;
		tabHost.getTabWidget().getChildAt(1).getLayoutParams().height = 50;

		mDaySeries = new TimeSeries("활동");
		mHourSeries = new XYSeries("활동");

		// Tab1
		LinearLayout tab1_layout = (LinearLayout) findViewById(R.id.tab1);

		// Tab2
		LinearLayout tab2_layout = (LinearLayout) findViewById(R.id.tab2);

		hourRenderer = buildRenderer(COLOR, POINTSTYLE);
		hourRenderer.setXLabels(0);
		dailyRenderer = buildRenderer(COLOR, POINTSTYLE);

		if(makeXvalue(true) > 0)
			addView(tab1_layout);
		else
			showEmptyView(tab1_layout);
		
		
		if(makeXvalue(false) > 0)
			addView(tab2_layout);
		else
			showEmptyView(tab2_layout);
		
		
		backKeyButton = (Button) findViewById(R.id.backButton);
		backKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});

		homeKeyButton = (Button) findViewById(R.id.homeButton);
		homeKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mContext, LauncherUI.class);
				// intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
				// Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});
	}

	private void showEmptyView(LinearLayout layout) {
		TextView tv = new TextView(this);
		tv.setTextSize(21);
		tv.setGravity(Gravity.CENTER);
		tv.setText(getResources().getString(R.string.data_empty));
		layout.addView(tv, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
	}

	private void addView(LinearLayout layout) {
		XYMultipleSeriesDataset dataSet = new XYMultipleSeriesDataset();

		if (layout.getId() == R.id.tab1) {
			dataSet.addSeries(mHourSeries);
			hourRenderer.setXAxisMax(mHourSeries.getItemCount());
			hourRenderer.setYAxisMax(mHourSeries.getMaxY() + 100);

			hourRenderer.setShowCustomTextGrid(true);
			hourRenderer.setXAxisMin(0);

			double[] limits = new double[] { mHourSeries.getMaxX() - 11, mHourSeries.getMaxX(), hourRenderer.getYAxisMin(), hourRenderer.getYAxisMax() };
			hourRenderer.setRange(limits);

			GraphicalView hourView = ChartFactory.getLineChartView(getBaseContext(), dataSet, hourRenderer);

			hourView.addZoomListener(new ZoomListener() {

				@Override
				public void zoomReset() {
					double[] limits = new double[] { mHourSeries.getMaxX() - 11, mHourSeries.getMaxX(), hourRenderer.getYAxisMin(), hourRenderer.getYAxisMax() };
					hourRenderer.setRange(limits);
				}

				@Override
				public void zoomApplied(ZoomEvent arg0) {

				}
			}, true, true);

			layout.addView(hourView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		} else if (layout.getId() == R.id.tab2) {
			dataSet.addSeries(mDaySeries);
			dailyRenderer.setYAxisMax(mDaySeries.getMaxY() + 1000);
			dailyRenderer.setYAxisMin(mDaySeries.getMinY() - 1000);
			GraphicalView hourView = ChartFactory.getTimeChartView(getBaseContext(), dataSet, dailyRenderer, TIME_DAY);

			layout.addView(hourView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		}
	}
	
	private int makeXvalue(boolean isHour) {
		List<ActivityData> list = ConnectDB.getInstance(this).selectAllActivityData();

		if (list.size() > 0) {
			Date date = list.get(0).getTime();
			Calendar checkTime = Calendar.getInstance();

			checkTime.setTime(date);

			checkTime.set(Calendar.MILLISECOND, 0);
			checkTime.set(Calendar.SECOND, 0);
			checkTime.set(Calendar.MINUTE, 0);

			int value = 0;
			int xLabel = 0;
			long currentTime = list.get(list.size() - 1).getTime().getTime();
			Iterator<ActivityData> it = list.iterator();
			ActivityData data = it.next();

			while (checkTime.getTimeInMillis() <= currentTime) {
				Calendar afterTime = (Calendar) checkTime.clone();
				if (isHour)
					afterTime.add(Calendar.HOUR_OF_DAY, 1);
				else
					afterTime.add(Calendar.DATE, 1);

				long dataTime;
				if (data != null) {
					dataTime = data.getTime().getTime();
					
					while (afterTime.getTimeInMillis() > dataTime && it.hasNext()) {
						value += data.getCount();
						data = it.next();
						dataTime = data.getTime().getTime();
					}

					if (dataTime >= checkTime.getTimeInMillis()) {
						if (isHour) {
							mHourSeries.add(xLabel, value);
							if (checkTime.get(Calendar.HOUR_OF_DAY) == 0)
								hourRenderer.addXTextLabel(xLabel, new SimpleDateFormat(TIME_HOUR).format(checkTime.getTime()));
							else
								hourRenderer.addXTextLabel(xLabel, new SimpleDateFormat(TIME_MIN).format(checkTime.getTime()));
						} else {
							mDaySeries.add(checkTime.getTime(), value);
						}
						value = 0;
					}
					if (isHour)
						checkTime.add(Calendar.HOUR_OF_DAY, 1);
					else
						checkTime.add(Calendar.DATE, 1);
					xLabel++;
				}
			}
		}

		if(isHour)
			return mHourSeries.getItemCount();
		else
			return mDaySeries.getItemCount();

	}

	private XYMultipleSeriesRenderer buildRenderer(int color, PointStyle style) {
		XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer();
		setRenderer(renderer, color, style);

		renderer.setFitLegend(true);
		renderer.setShowGrid(true);
		renderer.setExternalZoomEnabled(true);
		renderer.setAntialiasing(true);
		renderer.setInScroll(true);

		renderer.setXLabelsAlign(Align.CENTER);
		renderer.setYLabelsAlign(Align.CENTER);
		renderer.setZoomEnabled(true, false);
		renderer.setPanEnabled(true, false);
		renderer.setZoomButtonsVisible(true);
		renderer.setShowAxes(true);
		return renderer;
	}

	private XYSeriesRenderer getSeriesRenderer(final int color, final PointStyle style) {
		final XYSeriesRenderer r = new XYSeriesRenderer();
		r.setDisplayChartValues(true);
		r.setChartValuesTextSize(14);
		r.setPointStyle(style);
		r.setColor(color);
		r.setHighlighted(true);
		r.setPointStrokeWidth(4);
		r.setLineWidth(2);
		return r;
	}

	private void setRenderer(XYMultipleSeriesRenderer renderer, int color, PointStyle style) {
		renderer.setAxisTitleTextSize(16);
		renderer.setChartTitleTextSize(20);
		renderer.setLabelsTextSize(15);
		renderer.setLegendTextSize(15);
		renderer.setPointSize(2);
		renderer.setMargins(new int[] { 20, 30, 15, 20 });
		renderer.addSeriesRenderer(getSeriesRenderer(color, style));
	}
}
