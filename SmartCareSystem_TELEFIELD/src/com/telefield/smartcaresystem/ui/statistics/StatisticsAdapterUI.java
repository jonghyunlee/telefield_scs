package com.telefield.smartcaresystem.ui.statistics;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.database.BloodPressure;
import com.telefield.smartcaresystem.database.SensorHistory;
import com.telefield.smartcaresystem.message.SensorID;
import com.telefield.smartcaresystem.ui.UIConstant;

public class StatisticsAdapterUI extends ArrayAdapter<SensorHistory> {
	private List items;
	private Context mContext;
	private static final String FORMAT = "yyyy년 MM월 dd일 HH시 mm분";
	private final String SYSTOLIC = "최고 혈압 : ";
	private final String DIASTOLIC = "최저 혈압 : ";
	private final String PULSE = "맥박 혈압 : ";
	private int extra_type = -1;

	public StatisticsAdapterUI(Context context, int resource, List objects, int extra_type) {
		super(context, resource, objects);
		this.mContext = context;
		this.items = objects;
		this.extra_type = extra_type;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		if (v == null) {
			LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.adapter_statistics, null);
		}

		if (extra_type == SensorID.FIRE_SENSOR_TYPE.getSensorType() || (extra_type == SensorID.GAS_SENSOR_TYPE.getSensorType())) {
			SensorHistory p = (SensorHistory) items.get(position);
			if (p != null) {
				if (p.getTYPE().equals(extra_type)) {

					TextView tt = (TextView) v.findViewById(R.id.toptext);
					TextView bt = (TextView) v.findViewById(R.id.bottomtext);
					if (tt != null) {
						tt.setText(GetTypeToString(p.getData()));
					}
					if (bt != null) {
						bt.setText(new SimpleDateFormat(FORMAT).format(p.getTime()));
					}
				}
			}

		} else if (extra_type == UIConstant.TYPE_BLOOD_PRESSURE) {
			BloodPressure blood = (BloodPressure) items.get(position);
			if(blood != null){

				TextView tt = (TextView) v.findViewById(R.id.toptext);
				TextView bt = (TextView) v.findViewById(R.id.bottomtext);
				if (tt != null) {
					tt.setText(getDataToString(blood.getSystolic(), blood.getDiastolic(), blood.getPulse()));
				}
				if (bt != null) {
					bt.setText(new SimpleDateFormat(FORMAT).format(blood.getTIME()));
				}
			}
		}
		return v;
		// return super.getView(position, convertView, parent);
	}

	private String GetTypeToString(int data) {
		StringBuilder sb = new StringBuilder();
		if (extra_type == SensorID.FIRE_SENSOR_TYPE.getSensorType())
			sb.append(SensorID.FIRE_SENSOR_TYPE.getSensorName());
		else if (extra_type == SensorID.GAS_SENSOR_TYPE.getSensorType())
			sb.append(SensorID.GAS_SENSOR_TYPE.getSensorName());

		switch (data) {
		case 1:
			sb.append(" 발생");
			break;
		case 0:
			sb.append(" 해제");
			break;

		}
		return sb.toString();
	}
	
	private String getDataToString(int systolic, int diastolic, int pulse){
		StringBuilder sb = new StringBuilder();
		sb.append(this.SYSTOLIC).append(String.valueOf(systolic)).append("  ");
		sb.append(this.DIASTOLIC).append(String.valueOf(diastolic)).append("  ");;
		sb.append(this.PULSE).append(String.valueOf(pulse));
		return sb.toString();
	}

}
