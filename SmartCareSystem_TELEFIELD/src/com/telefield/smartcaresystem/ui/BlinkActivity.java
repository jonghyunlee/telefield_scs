package com.telefield.smartcaresystem.ui;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.telefield.smartcaresystem.Constant;
import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.database.ConnectDB;
import com.telefield.smartcaresystem.database.Users;

public class BlinkActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		setContentView(R.layout.empty_ui);

		String usertype = getIntent().getStringExtra(UIConstant.UTTERANCEID);

		ConnectDB con = ConnectDB.getInstance(this);
		
		InCallUI incall = new InCallUI();
		Users user = new Users();
		user.setType(usertype);
		List<Users> result = con.selectUser(user);
		if (result.size() > 0) {
			Users selectUser = result.get(0);

			Intent intent;
			boolean isEmergency = false;
			String checkNumber = selectUser.getNumber();
			for (int i = 0; i < Constant.EMERGENCY_NUMBER.length; i++) {
				if(checkNumber.matches(Constant.EMERGENCY_NUMBER[i])){
					isEmergency = true;
					break;
				}
			}
			
			if(isEmergency){
				intent = new Intent("android.intent.action.CALL_EMERGENCY");
				incall.setIsEmergency(isEmergency);
			}
			else{
				intent = new Intent(Intent.ACTION_CALL);
				incall.setIsEmergency(isEmergency);
			}
			incall.finish();
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.setData(Uri.parse(UIConstant.ACTION_TEL + selectUser.getNumber()));
			startActivity(intent);
		}
		super.onCreate(savedInstanceState);
	}

	@Override
	protected void onStop() {
		finish();
		super.onStop();
	}

}
