package com.telefield.smartcaresystem.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class AppStateReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context ctx, Intent intent) {
		String name = intent.getAction();
		Log.d("app_state_receive",name);
		Log.d("app_state_receive",intent.toString() + "\n");

	}
}
