package com.telefield.smartcaresystem.ui.opening;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.telefield.smartcaresystem.Constant;
import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.m2m.CallbackEvent;
import com.telefield.smartcaresystem.m2m.GatewayDevice;
import com.telefield.smartcaresystem.m2m.ProtocolConfig;
import com.telefield.smartcaresystem.m2m.ProtocolManger;
import com.telefield.smartcaresystem.ui.LauncherUI;
import com.telefield.smartcaresystem.ui.common.ConfirmDialogUI;
import com.telefield.smartcaresystem.util.LogMgr;
import com.telefield.smartcaresystem.util.SpeechUtil;

public class OpenningUI extends Activity {
	ListView openningListView;
	private Button backKeyButton, homeKeyButton;
	private Context mContext;
	private SpeechUtil mSpeechUtil;
	private boolean isRunning = false;

	CallbackEvent callbackEvent = new CallbackEvent() {

		@Override
		public void proccessmessage(String message) {
			Button msgbtn;
			if (message.equals(ProtocolConfig.OPENNING_START)) {
				mSpeechUtil.speech(SpeechUtil.START_OPENNING, TextToSpeech.QUEUE_FLUSH, null);
				isRunning = true;
			} else if (message.equals(ProtocolConfig.OPENNING_FINISH_SEND)) {
				msgbtn = (Button) findViewById(R.id.finish_send);
				msgbtn.setBackgroundResource(R.drawable.icon_open02_success);

			} else if (message.equals(ProtocolConfig.OPENNING_WAIT)) {
				msgbtn = (Button) findViewById(R.id.wait_message);
				msgbtn.setBackgroundResource(R.drawable.icon_open03_success);
			}
		}

		@Override
		public void finishmessage(boolean reseult, String message) {
			Button msgbtn;
			msgbtn = (Button) findViewById(R.id.openning_result);
			isRunning = false;
			//<--중앙서버에 번호가 등록되어 있지 않으면 개통시 Nullpointexception나던 부분 ver 1.1.7
			if(message == null){
				LogMgr.d(LogMgr.TAG, "OpenningUI is null......");
				message = ProtocolConfig.FAIL_NOT_RESPONSE;
			}//-->
			//
			TextView resultView = (TextView) findViewById(R.id.result_message);
			resultView.setText(message);

			if (reseult) {
				msgbtn.setBackgroundResource(R.drawable.icon_open04_success);
				mSpeechUtil.speech(SpeechUtil.SUCCESS_OPENING, TextToSpeech.QUEUE_FLUSH, null);
			} else {
				msgbtn.setBackgroundResource(R.drawable.icon_open04_fail);
				mSpeechUtil.speech(SpeechUtil.FAIL_OPENING, TextToSpeech.QUEUE_FLUSH, null);
			}

			if (message.equals(ProtocolConfig.FAIL_CANNOT_CONNECT)) {
				msgbtn = (Button) findViewById(R.id.finish_send);
				msgbtn.setBackgroundResource(R.drawable.icon_open02_fail);
			}
			if (message.equals(ProtocolConfig.FAIL_NOT_RESPONSE)) {
				msgbtn = (Button) findViewById(R.id.wait_message);
				msgbtn.setBackgroundResource(R.drawable.icon_open03_fail);

			}
			
		}

	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.openning_ui);

		mContext = this;
		mSpeechUtil = SmartCareSystemApplication.getInstance().getSpeechUtil();
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);

		openningListView = (ListView) findViewById(android.R.id.list);
		final TextView tv = (TextView) findViewById(R.id.titleTextView);
		tv.setText(R.string.openning);

		backKeyButton = (Button) findViewById(R.id.backButton);
		backKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});

		homeKeyButton = (Button) findViewById(R.id.homeButton);
		homeKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mContext, LauncherUI.class);
				// intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
				// Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});

		String number = SmartCareSystemApplication.getInstance().getMyPhoneNumber();
		if (number != null) {
			byte[] bytenum = number.getBytes();
			System.arraycopy(bytenum, 0, GatewayDevice.getInstance().number, 0, bytenum.length);
		}

		mSpeechUtil.speech(SpeechUtil.OPENNING_MENT, TextToSpeech.QUEUE_FLUSH, null);

		ConfirmDialogUI dialog = ConfirmDialogUI.newInstance(ConfirmDialogUI.MODE_OPENNING);
		dialog.setEventCallBack(callbackEvent);
		dialog.setTitle("개통 안내");
		dialog.show(getFragmentManager(), "Dialog");

		super.onResume();
	}

	@Override
	protected void onStop() {
		ProtocolManger mgr = SmartCareSystemApplication.getInstance().getProtocolManger();
		mgr.setEventRegistration(null);
		if (Constant.IS_RELEASE_VERSION)
			mgr.stop();
		else
			mgr.stop();
		super.onStop();
		if (isRunning)
			mSpeechUtil.speech(SpeechUtil.CANCEL_OPENNING, TextToSpeech.QUEUE_FLUSH, null);
	}

	@Override
	protected void onNewIntent(Intent intent) {
		if (intent == null || !isRunning)
			return;
		if (intent.getAction().equals("android.intent.ACTION.CANCEL_OPENNING")) {
			SmartCareSystemApplication.getInstance().getProtocolManger().updateUI(ProtocolConfig.OPENNING_CANCEL);
			finish();
		}
	}

}
