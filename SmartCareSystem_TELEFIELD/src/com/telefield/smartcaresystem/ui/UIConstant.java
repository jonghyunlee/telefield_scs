package com.telefield.smartcaresystem.ui;

public class UIConstant {
	//Enter Hidden Mode
	public static String INIT_PASSWORD_NUM = "1351";
	public static String MAIN_NUM = "1111";
	public static final String DEBUG_PASSWORD_NUM = "124361";
	public static final String MESSAGE_VIEW_NUM = "145145";
	public static final String HIDDEN_PASSWORD_NUM = "516456";
	public static final String TEST_CASE_NUM = "341623";
	

	
	public static String INIT_SETTING_PASSWORD = "1351";	//모드 초기화 할 때의 비밀번호
	
	public static final String SPACE_CHARACTER = " ";
	
	public static final int DEFAULT_TIMER_TIME = 30;
	
	//SensorAdapterUI에 사용하는 모드
	public static final int MODE_REGISTER_SENSOR = 1;	//센서 등록
	public static final int MODE_DELETE_SENSOR = 2;		//센서 삭제
	public static final int MODE_SEE_REGISTER_CONDITION = 3;	//등록상태 보기
	public static final int MODE_SEE_SENSOR_FOR_TEST = 4;	//테스트 할 센서 목록
	public static final int MODE_SEE_SENSOR_FOR_SUB_TEST = 5;	//테스트 할 센서 서브 목록
	public static final int MODE_TEST_SENSOR = 6;	//테스트 할 센서의 리스트
	public static final int MODE_REAL_TEST_SENSOR = 7;	//실제 테스트 할 센서의 리스트
	public static final int MODE_ALL_CHECK_TEST_LIST = 8;	//실제 테스트 할 센서의 리스트
	public static final int MODE_ONGOING_TEST = 9;	//테스트 중
	public static final int MODE_STOP_TEST = 10;	//테스트 중단
	
	public static final int MODE_SEE_SMARTPHONE_VERSION = 4;	//스마트폰 버전 표시
	public static final int MODE_SEE_SMARTPHONE_STATUS = 5;	//스마트폰 상태 표시
	public static final int MODE_SYSTEM_MAIN_MENU = 11;
	
	//센서 이름
	public static final String FIRE_SENSOR = "화재센서";
	public static final String GAS_SENSOR = "가스센서";
	public static final String ACTIVITY_SENSOR = "활동센서";
	public static final String DOOR_SENSOR = "출입감지센서";
	public static final String OUT_ACTIVITY_SENSOR = "외출활동센서";
	public static final String EMERGENCY_SENSOR = "응급호출장비";
	public static final String CARE_SENSOR = "돌보미";
	public static final String GAS_BREAKER_SENSOR = "가스차단기";
	
	//변경된 센서 이름
	public static final String DISPLAY_FIRE_SENSOR = "화재센서(화재 감지기)";
	public static final String DISPLAY_GAS_SENSOR = "가스센서(가스누설경보기)";
	
	public static final String ACTION_TEL = "tel:";
	
	//성공/실패 여부
	public static final int SUCCESS = 1;
	public static final int FAIL = 2;
	
	//intent key
	public static final String SENSOR_TYPE = "SensorType";	
	public static final String SENSOR_RESULT = "SensorResult";
	public static final String SEND_SENSOR_INFO = "SensorInfo";
	public static final String SEND_MAC_INFO = "MacInfo";
	public static final String SENSOR_ID_KEY = "SensorId";
	public static final String SENSOR_MAC = "SensorMac";
	public static final String SENSOR_BATTERY = "SensorBattery";
	public static final String SENSOR_ZIGBEE_BATTERY = "SensorZigbeeBattery";
	public static final String SENSOR_RSSI = "SensorRssi";
	public static final String SENSOR_COMMUNICATION_STATUS = "SensorCommunicationStatus";
	public static final String SENSOR_REGISTER_TIME = "SensorRegisterTime";
	public static final String SENSOR_LAST_COMMUNICATION_TIME = "SensorLastCommunicationTime";
	public static final String SENSOR_KEEP_ALIVE_TIME = "SensorKeepAliveTime";
	public static final String SENSOR_VERSION = "SensorVersion";
	public static final String UTTERANCEID = "utteranceId";
	public static final String BLOOD_PRESSURE_NAME = "BloodPressureName";
	public static final String BLOOD_PRESSURE_MAC = "BloodPressureMac";
	
	//등록/삭제 성공/실패 여부
	public static final int REGISTER_SUCCESS = 1;
	public static final int REGISTER_FAIL = 2;
	public static final int DELETE_SUCCESS = 3;
	public static final int DELETE_FAIL = 4;
	public static final int VISIT_CARE_REGISTER_SUCCESS = 5;
	public static final int VISIT_CARE_REGISTER_FAIL = 6;
	public static final int OUT_CARE_REGISTER_SUCCESS = 7;
	public static final int OUT_CARE_REGISTER_FAIL = 8;
	
	public static final String TITLE = "title";
	public static final String POSITION = "position";
	public static final String MODE = "mode";
	
	/** hadle id */
	public static final int DIALOG_CANCEL = 0;
	public static final int NOTIFY_MESSAGE = 1;
	public static final int NOTIFY_FTDI_DISCONNECTED = 2;
	
	
	public static final String NO_LONGER_SENSIR_REGISTER = "를 더 이상 등록할 수 없습니다.";
	public static final String ALREADY_REGISTERED ="이미 등록된 센서 입니다.";
	
	//Ringtone
	public static final String UNKNOWN_RINGTONE = "알 수 없는 벨소리";
	public static final String NO_SOUND = "무음";
	
	//Display Activity Sensor Setting's View
	public static final int MODE_ANNOUNCE_MENT_SETTING = 1;
	public static final int MODE_ACTIVITY_SENSOR_SETTING = 2;
	public static final int MODE_ACTIVITY_SENSOR_SETTING_HIDDEN = 3;
	public static final int MODE_LED_ON_OFF_SETTING = 4; 
	
	//Default value
	public static final int DEFAULT_NON_ACTIVITY_TIME_VALUE = 240;		//동작 미감지 시간
	public static final int DEFAULT_NON_ACTIVITY_NUM_VALUE = 1;		//미감지 threshold value
	public static final int DEFAULT_AUTO_RECEIVE_TIME_VALUE = 1;		//강제 착신을 위한 응답대기시간
	public static final int DEFAULT_PERIODIC_REPORT_TIME_VALUE = 10;	//통신 주기
	public static final int DEFAULT_PERIODIC_SEND_TIME_VALUE = 240;		//주기적 전송시간
	public static final String DEFAULT_START_SELF_CALL_TIME_VALUE = "19:00";	//셀프콜 금지 시작 시간
	public static final String DEFAULT_END_SELF_CALL_TIME_VALUE = "06:00";	//셀프콜 금지 종료 시간
	public static final int DEFAULT_CHARGE_DISCHARGE_PERIOD_DAY = 7;	//충전/방전 모드 시간 간격 기본값 7
	public static final int DEFAULT_CHARGE_DISCHARGE_PERFORM_TIME = 8;	//충전/방전 모드 수행 시간
	public static final int DEFAULT_DETECT_ACTIVITY_TIME = 5;	//활동량 감지 시간
	public static final String DEFAULT_SERVER_IP = "222.122.128.21";	//M2M SERVER IP (참고로 URL은 m2mcs.kt.com 임)
	public static final int DEFAULT_SERVER_PORT = 18003;	//M2M SERVER PORT
	public static final int DEFAULT_START_ADDRESS = 0;	//펌웨어 Verification 시작 주소
	public static final int DEFAULT_ADDRESS_SIZE = 128;	//펌웨어 Verification 크기
	public static final String ZIGBEE_DEFAULT_VER = "0";
	// 개통 현황
	public static final String DEFAULT_OPENNING_STATE = "미개통";
	public static final String OPENNING_FORMAT = "yyyy-MM-dd HH:mm";
	
	//기타
	public static final String ALL_APPLY = "모두 적용";
	
	public static final int CALL_ALARM_KEY = 364890;
	//Intent Extra Data
	public static final int TYPE_BLOOD_PRESSURE = 0x99;
}