package com.telefield.smartcaresystem.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.view.MenuItemCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.util.AppMonitorService;
import com.telefield.smartcaresystem.util.Format;
import com.telefield.smartcaresystem.util.Level;
import com.telefield.smartcaresystem.util.LogEntry;
import com.telefield.smartcaresystem.util.LogEntryAdapter;
import com.telefield.smartcaresystem.util.LogMgr;
import com.telefield.smartcaresystem.util.Logcat;
import com.telefield.smartcaresystem.util.insertDB;

public class DebugUI extends ListActivity {

	static final int WINDOW_SIZE = 1000;
	private static final Executor EX = Executors.newCachedThreadPool();

	public static final int CAT_WHAT = 0;
	public static final int CLEAR_WHAT = 2;

	static final int FORMAT_DIALOG = 1;

	private static final int MENU_CLEAR = 1;
	private static final int MENU_FORMAT = 2;
	private static final int MENU_BACKUP= 3;
	private static final int MENU_MONITOR_STOP = 4;
	private static final int MENU_MONITOR_START = 5;
	private static final int MENU_COPY_DB = 6;
	private static final int MENU_BACK = 7;
	
	private static final String LOGCAT_FORMAT = "format";

	private LogEntryAdapter mLogEntryAdapter;
	private Level mLastLevel = Level.V;
	private Logcat mLogcat;
	private boolean mPlay = true;
	private ListView mLogList;
	private Format mFormat;

	private Context mContext;
	
	SharedPreferences prefs;

	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case CAT_WHAT:
				@SuppressWarnings("unchecked")
				final List<String> lines = (List<String>) msg.obj;
				cat(lines);
				break;
			case CLEAR_WHAT:
				mLogEntryAdapter.clear();
				break;
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		setContentView(R.layout.debug_ui);

		mContext = this;

		mLogList = (ListView) findViewById(android.R.id.list);
		
		prefs = PreferenceManager.getDefaultSharedPreferences(this);
		
		mLogList.setOnScrollListener(new AbsListView.OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				pauseLog();
			}
			@Override
			public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
			}
		});
		
		mLogList.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
				openOptionsMenu();
				return false;
			}
		});
		
		String defaultFormat = prefs.getString(LOGCAT_FORMAT, "TAG");
		mFormat = Format.valueOf(defaultFormat);

		super.onCreate(savedInstanceState);
	}

	@Override
	protected void onResume() {
		onNewIntent(getIntent());
		init();
		super.onResume();
	}

	@Override
	protected void onStop() {
		if (mLogcat != null) {
			mLogcat.stop();
		}
		super.onStop();
	}

	private void init() {
		int color = Color.WHITE;
		mLogList.setBackgroundColor(color);
		mLogList.setCacheColorHint(color);

		mLogEntryAdapter = new LogEntryAdapter(this, R.layout.debug_entry, new ArrayList<LogEntry>(WINDOW_SIZE));
		setListAdapter(mLogEntryAdapter);
		reset();
	}

	private void cat(final String s) {
		if (mLogEntryAdapter.getCount() > WINDOW_SIZE) {
			mLogEntryAdapter.remove(0);
		}

		Format format = mLogcat.mFormat;
		Level level = format.getLevel(s);
		if (level == null) {
			level = mLastLevel;
		} else {
			mLastLevel = level;
		}

		final LogEntry entry = new LogEntry(s, level);
		mLogEntryAdapter.add(entry);
	}

	private void cat(List<String> lines) {
		for (String line : lines) {
			cat(line);
		}
		jumpBottom();
	}

	private void jumpBottom() {
		playLog();
		mLogList.setSelection(mLogEntryAdapter.getCount() - 1);
	}

	private void playLog() {
		if (mPlay) {
			return;
		}
		getWindow().setTitle(getResources().getString(R.string.app_name));
		if (mLogcat != null) {
			mLogcat.setPlay(true);
			mPlay = true;
		} else {
			reset();
		}
	}

	public void reset() {
		Toast.makeText(this, R.string.reading_logs, Toast.LENGTH_SHORT).show();
		mLastLevel = Level.V;
		mFormat = Format.valueOf(prefs.getString(LOGCAT_FORMAT, "TAG"));
		
		if (mLogcat != null) {
			mLogcat.stop();
		}

		mPlay = true;

		EX.execute(new Runnable() {
			public void run() {
				mLogcat = new Logcat(mContext, mHandler, mFormat);
				mLogcat.start();
			}
		});
	}

	private void pauseLog() {
		if (!mPlay) {
			return;
		}
		if (mLogcat != null) {
			mLogcat.setPlay(false);
			mPlay = false;
		}
	}

	private void clear() {
		try {
			Runtime.getRuntime().exec(new String[] { "logcat", "-c" });
		} catch (IOException e) {
			Log.e("alogcat", "error clearing log", e);
		} finally {
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);

		MenuItem clearItem = menu.add(0, MENU_CLEAR, 0, R.string.clear_menu);
		clearItem.setIcon(android.R.drawable.ic_menu_close_clear_cancel);
		MenuItemCompat.setShowAsAction(clearItem, MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);

		MenuItem formatItem = menu.add(0, MENU_FORMAT, 0, getResources().getString(R.string.format_menu));
		formatItem.setIcon(android.R.drawable.ic_menu_preferences);
		MenuItemCompat.setShowAsAction(formatItem, MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
		
		MenuItem backupItem = menu.add(0, MENU_BACKUP, 0, getResources().getString(R.string.db_backup));
		backupItem.setIcon(android.R.drawable.ic_menu_preferences);
		MenuItemCompat.setShowAsAction(backupItem, MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
		
		MenuItem dbMonitorStop = menu.add(0, MENU_MONITOR_STOP, 0, getResources().getString(R.string.monitor_stop));
		dbMonitorStop.setIcon(android.R.drawable.ic_menu_preferences);
		MenuItemCompat.setShowAsAction(dbMonitorStop, MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
		
		MenuItem dbMonitorStart = menu.add(0, MENU_MONITOR_START, 0, getResources().getString(R.string.monitor_start));
		dbMonitorStart.setIcon(android.R.drawable.ic_menu_preferences);
		MenuItemCompat.setShowAsAction(dbMonitorStart, MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
		
		MenuItem copyDB = menu.add(0, MENU_COPY_DB, 0, getResources().getString(R.string.copyDB));
		dbMonitorStart.setIcon(android.R.drawable.ic_menu_preferences);
		MenuItemCompat.setShowAsAction(copyDB, MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
		
		MenuItem back = menu.add(0, MENU_BACK, 0, getResources().getString(R.string.menu_back));
		dbMonitorStart.setIcon(android.R.drawable.ic_menu_preferences);
		MenuItemCompat.setShowAsAction(back, MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case MENU_CLEAR:
			clear();
			reset();
			return true;
		case MENU_FORMAT:
			showDialog(FORMAT_DIALOG);
			return true;
		case MENU_BACKUP:
			coptyDB();
			return true;
		case MENU_MONITOR_STOP:
			stopService(new Intent(this, AppMonitorService.class));
			return true;
		case MENU_MONITOR_START:
			startService(new Intent(this, AppMonitorService.class));
			return true;
		case MENU_COPY_DB:
			new insertDB(this);
			return true;
		case MENU_BACK:
			onBackPressed();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case FORMAT_DIALOG:

			final CharSequence[] items = { "BRIEF", "TAG", "TIME" };

			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Pick a color");
			builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int item) {
					SharedPreferences.Editor editor = prefs.edit();
					switch (item) {
					case 0:
						editor.putString(LOGCAT_FORMAT, "BRIEF");
						break;
					case 1:
						editor.putString(LOGCAT_FORMAT, "TAG");
						break;
					case 2:
						editor.putString(LOGCAT_FORMAT, "TIME");
						break;
					}
					editor.commit();
					dialog.dismiss();
				}
			});
			AlertDialog alert = builder.create();
			alert.show();

		}
		return null;
	}
	
	private void coptyDB(){
		SQLiteDatabase db = openOrCreateDatabase("telefield.db", Context.MODE_PRIVATE, null);
		File dbfile = new File(db.getPath());
		File makefile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/SmartCareSystem/");
		String filename = "/db.db";
		if(!makefile.exists())
			makefile.mkdirs();
		LogMgr.d(LogMgr.TAG, "DebugUI.java:coptyDB// makefile.getAbsolutePath() : " + makefile.getAbsolutePath());
		try {
			FileInputStream fis = new FileInputStream(dbfile);
			FileOutputStream fos = new FileOutputStream(makefile.getAbsolutePath() + filename);
			
			long filesize = fis.available();
			byte[] temp = new byte[(int) filesize];
			
			fis.read(temp);
			fis.close();
			
			fos.write(temp);
			fos.flush();
			
			fos.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
}
