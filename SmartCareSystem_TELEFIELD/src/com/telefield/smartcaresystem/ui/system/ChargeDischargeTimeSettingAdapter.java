package com.telefield.smartcaresystem.ui.system;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.ui.UIConstant;
import com.telefield.smartcaresystem.ui.common.IDialogChangeListener;
import com.telefield.smartcaresystem.util.SmartCarePreference;

public class ChargeDischargeTimeSettingAdapter extends BaseAdapter implements IDialogChangeListener {
	Context mContext;
	String[] data;
	
	public ChargeDischargeTimeSettingAdapter(Context mContext, String[] data) {
		this.mContext = mContext;
		this.data = data;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data[position];
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;
		if(view == null) {
			LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.child_system_sub_menu, parent, false);
		}
		
		TextView menuTitle = (TextView) view.findViewById(R.id.menuTitle);
		menuTitle.setTextSize(21);
		menuTitle.setText(data[position]);
		
		ImageView image = (ImageView) view.findViewById(R.id.menuItemImage);
		image.setVisibility(View.GONE);
		ImageView expandImage = (ImageView) view.findViewById(R.id.openSubMenuImageView);
		expandImage.setVisibility(View.GONE);
		
		TextView info = (TextView) view.findViewById(R.id.info);
		
		SmartCarePreference pref = new SmartCarePreference(mContext, SmartCarePreference.APPLICATION_SETTING_INFO_FILENAME);
		
		switch(position) {
		case 0:
			info.setVisibility(View.VISIBLE);
			info.setText(Integer.toString(pref.getIntValue(SmartCarePreference.CHARGE_DISCHARGE_PERIOD_DAY, UIConstant.DEFAULT_CHARGE_DISCHARGE_PERIOD_DAY)) + mContext.getResources().getString(R.string.day));
			break;
		case 1:
			info.setVisibility(View.VISIBLE);
			info.setText(Integer.toString(pref.getIntValue(SmartCarePreference.CHARGE_DISCHARGE_PERFORM_TIME, UIConstant.DEFAULT_CHARGE_DISCHARGE_PERFORM_TIME)) + mContext.getResources().getString(R.string.hour));
			break;
	}
		
		return view;
	}

	@Override
	public void notifyChangeListener() {
		// TODO Auto-generated method stub
		notifyDataSetChanged();
	}

}
