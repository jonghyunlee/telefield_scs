package com.telefield.smartcaresystem.ui.system;

import java.io.IOException;

import org.xmlpull.v1.XmlPullParserException;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.XmlResourceParser;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.ui.UIConstant;
import com.telefield.smartcaresystem.ui.common.IDialogChangeListener;
import com.telefield.smartcaresystem.util.SmartCarePreference;

public class SelfCallSettingAcapterUI extends BaseAdapter implements IDialogChangeListener {
	Context mContext;
	String[] data;
	
	TextView prohibitSelfcallTitle, prohibitSelfcallInfo;
	
	public SelfCallSettingAcapterUI(Context mContext, String[] data) {
		this.mContext = mContext;
		this.data = data;
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data[position];
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;
		
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		if(position == 0) {	//SelfCall On/Off
			view = inflater.inflate(R.layout.child_system_sub_menu_with_switch, parent, false);
			
			TextView menuTitle = (TextView) view.findViewById(R.id.menuTitle);
			menuTitle.setText(data[position]);
			
			TextView info = (TextView) view.findViewById(R.id.info);
			info.setVisibility(View.GONE);
			ImageView image = (ImageView) view.findViewById(R.id.menuItemImage);
			image.setVisibility(View.GONE);
			ImageView expandImage = (ImageView) view.findViewById(R.id.openSubMenuImageView);
			expandImage.setVisibility(View.GONE);
			
			final SmartCarePreference pref = new SmartCarePreference(mContext, SmartCarePreference.APPLICATION_SETTING_INFO_FILENAME);
			boolean selfCallOnOff = pref.getBooleanValue(SmartCarePreference.SELF_CALL_ON_OFF, false);
			
			Switch switchWidget = (Switch) view.findViewById(R.id.onOffSwitch);
			switchWidget.setChecked(selfCallOnOff);
			switchWidget.setOnCheckedChangeListener(new OnCheckedChangeListener(){
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					// TODO Auto-generated method stub
					pref.setBooleanValue(SmartCarePreference.SELF_CALL_ON_OFF, isChecked);
					
					XmlResourceParser disableParser = mContext.getResources().getXml(R.color.diagnosis_end_color);
					XmlResourceParser enableParser = mContext.getResources().getXml(R.color.diagnosis_wait_color);
				    ColorStateList disableColor = null;
				    ColorStateList enableColor = null;
				    try {
				    	disableColor = ColorStateList.createFromXml(mContext.getResources(), disableParser);
				    	enableColor = ColorStateList.createFromXml(mContext.getResources(), enableParser);
					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			    	
				    //SelfCall On/Off에 따른 글자색 변경
				    if(isChecked) {
				    	if(enableColor != null) {
				    		if(prohibitSelfcallTitle != null)
				    			prohibitSelfcallTitle.setTextColor(enableColor);
				    		else if(prohibitSelfcallInfo != null)
				    			prohibitSelfcallInfo.setTextColor(enableColor);
				    	}
				    }
				    else {
				    	if(disableColor != null) {
				    		if(prohibitSelfcallTitle != null)
				    			prohibitSelfcallTitle.setTextColor(disableColor);
				    		else if(prohibitSelfcallInfo != null)
				    			prohibitSelfcallInfo.setTextColor(disableColor);
				    	}
				    }
				    
				    notifyDataSetChanged();
				}
			});
		}
		else {	//SelfCall 금지 시간		
			view = inflater.inflate(R.layout.child_system_sub_menu, parent, false);
			
			prohibitSelfcallTitle = (TextView) view.findViewById(R.id.menuTitle);
			prohibitSelfcallTitle.setTextSize(21);
			prohibitSelfcallTitle.setText(data[position]);
			
			ImageView image = (ImageView) view.findViewById(R.id.menuItemImage);
			image.setVisibility(View.GONE);
			ImageView expandImage = (ImageView) view.findViewById(R.id.openSubMenuImageView);
			expandImage.setVisibility(View.GONE);
			
			prohibitSelfcallInfo = (TextView) view.findViewById(R.id.info);
			SmartCarePreference pref = new SmartCarePreference(mContext, SmartCarePreference.APPLICATION_SETTING_INFO_FILENAME);
			
			prohibitSelfcallInfo.setText(pref.getStringValue(SmartCarePreference.SELF_CALL_FROM_TIME, UIConstant.DEFAULT_START_SELF_CALL_TIME_VALUE) + " " + 
					mContext.getResources().getString(R.string.moire) + " " + pref.getStringValue(SmartCarePreference.SELF_CALL_TO_TIME, UIConstant.DEFAULT_END_SELF_CALL_TIME_VALUE));
			
			boolean selfCallOnOff = pref.getBooleanValue(SmartCarePreference.SELF_CALL_ON_OFF, false);
			if(!selfCallOnOff) {	//셀프콜이 Off 상태이면 글자색을 회색으로 처리한다.
				XmlResourceParser disableParser = mContext.getResources().getXml(R.color.diagnosis_end_color);
				ColorStateList disableColor = null;
				try {
					disableColor = ColorStateList.createFromXml(mContext.getResources(), disableParser);
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if(disableColor != null) {
					prohibitSelfcallTitle.setTextColor(disableColor);
					prohibitSelfcallInfo.setTextColor(disableColor);
				}
			}
		}
		return view;
	}

	@Override
	public void notifyChangeListener() {
		// TODO Auto-generated method stub
		notifyDataSetChanged();
	}

}
