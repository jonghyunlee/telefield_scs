package com.telefield.smartcaresystem.ui.system;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.ui.LauncherUI;
import com.telefield.smartcaresystem.ui.UIConstant;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class OldpersonOrDisabledUI extends Activity {
	Context mContext;
	Button backKeyButton, homeKeyButton;
	ListView handsetLockSettingListView;
	OldpersonOrDisabledAdepter adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.system_setting_ui);
		
		Bundle bundle = getIntent().getExtras();
		String title;
		
		if(bundle != null) {
			title = bundle.getString(UIConstant.TITLE);
		}
		else {
			title = "중증 장애인 설정";	//강제 세팅
		}
		
		mContext = this;
		
		TextView titleTextView = (TextView) findViewById(R.id.titleTextView);
		titleTextView.setText(title);
		
		final String[] mainMenuArray = getResources().getStringArray(R.array.disabled_setting);
		
		adapter = new OldpersonOrDisabledAdepter(this, mainMenuArray);
		handsetLockSettingListView = (ListView) findViewById(R.id.menuListView);
		handsetLockSettingListView.setAdapter(adapter);
		
		backKeyButton = (Button) findViewById(R.id.backButton);
		backKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		homeKeyButton = (Button) findViewById(R.id.homeButton);
		homeKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mContext, LauncherUI.class);
				//intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});
	}
}
