package com.telefield.smartcaresystem.ui.system;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.ui.LauncherUI;
import com.telefield.smartcaresystem.ui.UIConstant;
import com.telefield.smartcaresystem.ui.common.TimePickerDialogUI;
import com.telefield.smartcaresystem.util.SmartCarePreference;

public class SelfCallSettingUI extends Activity {
	Button backKeyButton, homeKeyButton;
	ListView menuListView;
	Context mContext;
	SelfCallSettingAcapterUI mAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.system_setting_ui);
		
		mContext = this;
		
		Bundle bundle = getIntent().getExtras();
		String title;
		
		if(bundle != null) {
			title = bundle.getString(UIConstant.TITLE);
		}
		else {
			title = "셀프콜 설정";	//강제 세팅
		}
		
		TextView titleTextView = (TextView) findViewById(R.id.titleTextView);
		titleTextView.setText(title);
		
		final String[] mainMenuArray = getResources().getStringArray(R.array.self_call_setting);
		
		mAdapter = new SelfCallSettingAcapterUI(this, mainMenuArray);
		menuListView = (ListView) findViewById(R.id.menuListView);
		menuListView.setAdapter(mAdapter);
		menuListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				if(position == 1) {
					SmartCarePreference pref = new SmartCarePreference(mContext, SmartCarePreference.APPLICATION_SETTING_INFO_FILENAME);
					boolean selfCallOnOff = pref.getBooleanValue(SmartCarePreference.SELF_CALL_ON_OFF, false);
					if(selfCallOnOff) {
						TimePickerDialogUI dialog = TimePickerDialogUI.newInstance(mAdapter);
						dialog.setTitle(mainMenuArray[position]);
						dialog.show(getFragmentManager(), "Dialog");
					}
				}
			}
		});
		
		backKeyButton = (Button) findViewById(R.id.backButton);
		backKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		homeKeyButton = (Button) findViewById(R.id.homeButton);
		homeKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mContext, LauncherUI.class);
				//intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});
	}

}
