package com.telefield.smartcaresystem.ui.system;

import java.util.List;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.database.ConnectDB;
import com.telefield.smartcaresystem.database.DBInfo;
import com.telefield.smartcaresystem.database.Users;
import com.telefield.smartcaresystem.ui.UIConstant;
import com.telefield.smartcaresystem.ui.common.IDialogChangeListener;
import com.telefield.smartcaresystem.util.SmartCarePreference;

public class SystemMenuAdapterUI extends BaseAdapter implements IDialogChangeListener {
	Context mContext;
	String[] data;
	
	//System SubMenuItem's number
	public static final int MENU_POS1 = 1;
	public static final int MENU_POS2 = 2;
	public static final int MENU_POS3 = 3;
	public static final int MENU_POS4 = 4;
	public static final int MENU_POS5 = 5;
	public static final int MENU_POS6 = 6;
	public static final int MENU_POS7 = 7;
	public static final int MENU_POS8 = 8;
	public static final int MENU_POS9 = 9;
	public static final int MENU_POS10 = 10;
	public static final int MENU_POS11 = 11;
	public static final int MENU_POS12 = 12;
	public static final int MENU_POS13 = 13;
	
	public final int START_POS = 0;							//사용자 설정 위치
	public final int HDR_POS1 = MENU_POS3 + 1;				//전화번호 설정 위치
	public final int HDR_POS2 = HDR_POS1 + MENU_POS5 + 1;	//센서 환경 설정 위치
	public final int HDR_POS3 = HDR_POS2 + MENU_POS2 + 1;	//통화 설정 위치
	public final int HDR_POS4 = HDR_POS3 + MENU_POS4 + 1;	//애플리케이션 설정 위치
	public final int HDR_POS5 = HDR_POS4 + MENU_POS13 + 1;	//서버 설정 위치
	public final int HDR_POS6 = HDR_POS5 + MENU_POS2 + 1;	//M2M 개통 위치
		
	public SystemMenuAdapterUI(Context mContext, String[] data) {
		this.mContext = mContext;
		this.data = data;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data[position];
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		String headerText = getHeader(position);
		
		View view = convertView;
		
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		if(headerText != null) {
			view = inflater.inflate(R.layout.child_system_menu, parent, false);	//서브 ListItem이 적용되어 있으므로 null check 없이 그냥 change함
			
			TextView item = (TextView) view.findViewById(R.id.menuText);
			item.setText(headerText);
			
			ImageView image = (ImageView) view.findViewById(R.id.menuImage);
			image.setVisibility(View.GONE);
		}
		else {
			view = inflater.inflate(R.layout.child_system_sub_menu, parent, false);	//헤더ListItem이 적용되어 있으므로 null check 없이 그냥 change함
			
			TextView menuTitle = (TextView) view.findViewById(R.id.menuTitle);
			menuTitle.setTextSize(21);
			menuTitle.setText(data[position]);
			
			ImageView image = (ImageView) view.findViewById(R.id.menuItemImage);
			image.setVisibility(View.GONE);
			ImageView expandImage = (ImageView) view.findViewById(R.id.openSubMenuImageView);
			expandImage.setVisibility(View.GONE);
			
			TextView info = (TextView) view.findViewById(R.id.info);
			
			if(position > START_POS && position < HDR_POS1) {	//사용자 설정
				SmartCarePreference pref = new SmartCarePreference(mContext, SmartCarePreference.USER_SETTING_INFO_FILENAME);
				
				if(position == START_POS+1) {
					info.setText(pref.getStringValue(SmartCarePreference.USER_NAME, SmartCarePreference.DEFAULT_COMMON_STRING));
				}
				else if(position == START_POS+2) {
					String number = SmartCareSystemApplication.getInstance().getMyPhoneNumber();	
					info.setText(number);
				}
				else if(position == START_POS+3) {
					info.setText(pref.getStringValue(SmartCarePreference.GATEWAY_MAC, SmartCarePreference.DEFAULT_COMMON_STRING));
				}
			}
			else if(position > HDR_POS1 && position < HDR_POS2) {	//전화번호 설정
				ConnectDB con = ConnectDB.getInstance(mContext);
				
				Users user = new Users();
				List<Users> getUser = null;
				
				if(position == HDR_POS1+1) {
					user.setType(DBInfo.HELPER_TYPE_119);
					getUser = con.selectUser(user);
					if(getUser.size() == 1)
						info.setText(getUser.get(0).getNumber());
					else {
						info.setText("119");
					}
				}
				else if(position == HDR_POS1+2) {
					user.setType(DBInfo.HELPER_TYPE_CENTER);
					
					getUser = con.selectUser(user);
					if(getUser.size() == 1)
						info.setText(getUser.get(0).getNumber());
					else
						info.setText("-");
				}
				else if(position == HDR_POS1+3) {
					user.setType(DBInfo.HELPER_TYPE_HELPER);

					getUser = con.selectUser(user);
					if(getUser.size() == 1)
						info.setText(getUser.get(0).getNumber());
					else 
						info.setText("-");
				}
				else if(position == HDR_POS1+4) {
					user.setType(DBInfo.HELPER_TYPE_CARE1);

					getUser = con.selectUser(user);
					if(getUser.size() == 1)
						info.setText(getUser.get(0).getNumber());
					else
						info.setText("-");
				}
				else if(position == HDR_POS1+5) {
					user.setType(DBInfo.HELPER_TYPE_CARE2);
					
					getUser = con.selectUser(user);
					if(getUser.size() == 1)
						info.setText(getUser.get(0).getNumber());
					else
						info.setText("-");
				}
			}
			else if(position > HDR_POS2 && position < HDR_POS3) {	//센서 환경 설정
				if(position == HDR_POS2+1 || position == HDR_POS2+2) {
					info.setVisibility(View.GONE);
					expandImage.setVisibility(View.VISIBLE);
				}
			}
			else if(position > HDR_POS3 && position < HDR_POS4) {	//통화 설정
				if(position == HDR_POS3+1) {
					Uri storedRingtoneUri = RingtoneManager.getActualDefaultRingtoneUri(mContext, RingtoneManager.TYPE_RINGTONE);
					
					Ringtone ringtone = RingtoneManager.getRingtone(mContext, storedRingtoneUri);
					
					String title = ringtone.getTitle(mContext);
					if(title.equals(UIConstant.UNKNOWN_RINGTONE)) {
						info.setText(UIConstant.NO_SOUND);
					}
					else {
						info.setText(title);
					}
				}
				else if(position == HDR_POS3+2 || position == HDR_POS3+3) {
					info.setVisibility(View.GONE);
				}
				else if(position == HDR_POS3+4) {
					SmartCarePreference pref = new SmartCarePreference(mContext, SmartCarePreference.CALL_SETTING_INFO_FILENAME);
					
					info.setText(Integer.toString(pref.getIntValue(SmartCarePreference.AUTO_RECEIVING_TIME, UIConstant.DEFAULT_AUTO_RECEIVE_TIME_VALUE)) + mContext.getResources().getString(R.string.minute));
				}
			}
			else if(position > HDR_POS4 && position < HDR_POS5) {	//애플리케이션 설정
				if(position == HDR_POS4+3) {
					expandImage.setVisibility(View.GONE);
					
					int getTime = Settings.System.getInt(mContext.getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT, 15 * 1000);
					int second = getTime / 1000;
					int minute = second / 60;
					
					if(second == 15 || second == 30) {
						info.setText(second + "초");
					}
					else if(minute == 1 || minute == 2 || minute == 5 || minute == 10 || minute == 30) {
						info.setText(minute + "분");
					}
				}
				else if(position == HDR_POS4+4) {
					expandImage.setVisibility(View.VISIBLE);
				}
				else if(position == HDR_POS4+5) {
					expandImage.setVisibility(View.VISIBLE);
				}
				else if(position == HDR_POS4+6) {
					expandImage.setVisibility(View.VISIBLE);
				}
				else if(position == HDR_POS4+8) {
					expandImage.setVisibility(View.GONE);
									
				}
				else if(position == HDR_POS4+9) {
					expandImage.setVisibility(View.GONE);
				}
				else if(position == HDR_POS4+10) {
					expandImage.setVisibility(View.GONE);
				}
				else if(position == HDR_POS4+11) {
					expandImage.setVisibility(View.GONE);
					PackageInfo i = null;
					try {
						i = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0);
						String version = i.versionName;
						info.setText(version);
					} catch (NameNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}		
				}
				else if(position == HDR_POS4+12) {
					expandImage.setVisibility(View.GONE);
					SmartCarePreference pref = new SmartCarePreference(mContext, SmartCarePreference.USER_SETTING_INFO_FILENAME);
					info.setText(pref.getStringValue(SmartCarePreference.ZIGBEE_VER, UIConstant.ZIGBEE_DEFAULT_VER));
				}
				else if(position == HDR_POS4+13) {
					expandImage.setVisibility(View.GONE);
					info.setText(R.string.build_date);
				}
				else {
					expandImage.setVisibility(View.GONE);
					info.setVisibility(View.GONE);
				}
				
			}
			else if(position > HDR_POS5 && position < HDR_POS6) {	//서버 설정
				SmartCarePreference pref = new SmartCarePreference(mContext, SmartCarePreference.ADDRESS_INFO_FILENAME);
				
				if(position == HDR_POS5+1) {
					info.setText(pref.getStringValue(SmartCarePreference.M2M_IP, UIConstant.DEFAULT_SERVER_IP));
				}
				else if(position == HDR_POS5+2) {
					info.setText(Integer.toString(pref.getIntValue(SmartCarePreference.M2M_PORT, UIConstant.DEFAULT_SERVER_PORT)));
				}
			}
			else {	//M2M 개통
				SmartCarePreference pref = new SmartCarePreference(mContext, SmartCarePreference.APPLICATION_SETTING_INFO_FILENAME);
				info.setText(pref.getStringValue(SmartCarePreference.OPENNING_RESULT, UIConstant.DEFAULT_OPENNING_STATE));
			}
				
		}
		
		return view;
	}
	
	private String getHeader(int position) {
		if(position == START_POS  || position == HDR_POS1 || position == HDR_POS2 || position == HDR_POS3 || position == HDR_POS4 || position == HDR_POS5 || position == HDR_POS6) {
			return data[position];
		}
        return null;
	}
	
	public TextView getInfoTextView() {
		return null;
	}
	
	@Override
	public void notifyChangeListener() {
		notifyDataSetChanged();
	}
}
