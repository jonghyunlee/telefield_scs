package com.telefield.smartcaresystem.ui.system;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.ui.LauncherUI;
import com.telefield.smartcaresystem.ui.UIConstant;
import com.telefield.smartcaresystem.ui.common.NumberPickerDialogUI;
import com.telefield.smartcaresystem.ui.common.SimpleSelectDialogUI;

public class ActivitySensorSettingUI extends Activity {
	Button backKeyButton, homeKeyButton;
	TextView titleTextView, emptyTextView;
	ListView activitySensorSettingListView;
	ActivitySensorSettingAdapterUI adapter;
	Context mContext;
	String[] listItem;
	
	int mode;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.see_register_condition);
		
		Bundle bundle = getIntent().getExtras();
		
		String title;
		
		if(bundle != null) {
			mode = bundle.getInt(UIConstant.MODE);
			title = bundle.getString(UIConstant.TITLE);
		}
		else {
			mode = 1;	//강제 세팅
			title = getString(R.string.activity_sensor_setting);
		}
		
		mContext = this;
		
		titleTextView = (TextView) findViewById(R.id.titleTextView);
		titleTextView.setText(title);
		emptyTextView = (TextView) findViewById(R.id.emptyTextView);
		emptyTextView.setVisibility(View.GONE);
		
		activitySensorSettingListView = (ListView) findViewById(R.id.registerConditionListView);
		
		if(mode == UIConstant.MODE_ANNOUNCE_MENT_SETTING)
			listItem = getResources().getStringArray(R.array.announce_ment_setting);
		else if(mode == UIConstant.MODE_ACTIVITY_SENSOR_SETTING)
			listItem = getResources().getStringArray(R.array.activity_sensor_setting);
		else if(mode == UIConstant.MODE_ACTIVITY_SENSOR_SETTING_HIDDEN)
			listItem = getResources().getStringArray(R.array.activity_sensor_setting_hidden);
		
		adapter = new ActivitySensorSettingAdapterUI(mContext, listItem, mode);
		
		activitySensorSettingListView.setAdapter(adapter);
		activitySensorSettingListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				if(mode == UIConstant.MODE_ACTIVITY_SENSOR_SETTING) {
					Intent intent = new Intent(mContext, LEDOnOffSettingUI.class);
					intent.putExtra(UIConstant.MODE, UIConstant.MODE_LED_ON_OFF_SETTING);
					intent.putExtra(UIConstant.TITLE, listItem[position]);
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(intent);
				}
				else if(mode == UIConstant.MODE_ACTIVITY_SENSOR_SETTING_HIDDEN) {
					switch(position) {
						case 0:
							SimpleSelectDialogUI dialog = SimpleSelectDialogUI.newInstance(adapter, SimpleSelectDialogUI.MODE_NON_ACTIVITY_TIME);
							dialog.setTitle(listItem[position]);
							dialog.show(getFragmentManager(), "Dialog");
							break;
						case 1:
							SimpleSelectDialogUI dialog1 = SimpleSelectDialogUI.newInstance(adapter, SimpleSelectDialogUI.MODE_NON_ACTIVITY_NUM);
							dialog1.setTitle(listItem[position]);
							dialog1.show(getFragmentManager(), "Dialog");
							break;
						case 2:
							NumberPickerDialogUI dialog2 = NumberPickerDialogUI.newInstance(adapter, NumberPickerDialogUI.MODE_DETECT_ACTIVITY_TIME);
							dialog2.setTitle(listItem[position]);
							dialog2.setMaxValue(10);
							dialog2.setMinValue(1);
							dialog2.show(getFragmentManager(), "Dialog");
							break;
					}
				}
			}
		});
		
		backKeyButton = (Button) findViewById(R.id.backButton);
		backKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		homeKeyButton = (Button) findViewById(R.id.homeButton);
		homeKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mContext, LauncherUI.class);
				//intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});
	}
}
