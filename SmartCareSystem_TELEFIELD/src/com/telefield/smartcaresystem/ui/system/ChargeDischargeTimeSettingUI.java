package com.telefield.smartcaresystem.ui.system;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.ui.LauncherUI;
import com.telefield.smartcaresystem.ui.UIConstant;
import com.telefield.smartcaresystem.ui.common.SimpleSelectDialogUI;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class ChargeDischargeTimeSettingUI extends Activity {
	Context mContext;
	Button backKeyButton, homeKeyButton;
	ListView chargeDischargeTimeSettingListView;
	ChargeDischargeTimeSettingAdapter adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.see_register_condition);
		
		Bundle bundle = getIntent().getExtras();
		String title;
		
		if(bundle != null) {
			title = bundle.getString(UIConstant.TITLE);
		}
		else {
			title = "충전/방전 모드 시간 설정";	//강제 세팅
		}
		
		mContext = this;
		
		TextView titleTextView, emptyTextView;
		titleTextView = (TextView) findViewById(R.id.titleTextView);
		titleTextView.setText(title);
		emptyTextView = (TextView) findViewById(R.id.emptyTextView);
		emptyTextView.setVisibility(View.GONE);
		
		final String[] item = getResources().getStringArray(R.array.charge_discharge_time);
		
		adapter = new ChargeDischargeTimeSettingAdapter(mContext, item);
		
		chargeDischargeTimeSettingListView = (ListView) findViewById(R.id.registerConditionListView);
		chargeDischargeTimeSettingListView.setAdapter(adapter);
		chargeDischargeTimeSettingListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				switch(position) {
					case 0:
						SimpleSelectDialogUI dialog = SimpleSelectDialogUI.newInstance(adapter, SimpleSelectDialogUI.MODE_PERIOD_DAY);
						dialog.setTitle(item[position]);
						dialog.show(getFragmentManager(), "Dialog");
						break;
					case 1:
						SimpleSelectDialogUI dialog1 = SimpleSelectDialogUI.newInstance(adapter, SimpleSelectDialogUI.MODE_PERFORM_TIME);
						dialog1.setTitle(item[position]);
						dialog1.show(getFragmentManager(), "Dialog");
						break;
				}
			}
		});
		
		backKeyButton = (Button) findViewById(R.id.backButton);
		backKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		homeKeyButton = (Button) findViewById(R.id.homeButton);
		homeKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mContext, LauncherUI.class);
				//intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});
	}
}
