package com.telefield.smartcaresystem.ui.system;

import java.util.ArrayList;

import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.telefield.smartcaresystem.GlobalMessageHandler;
import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.database.Sensor;
import com.telefield.smartcaresystem.message.MessageManager;
import com.telefield.smartcaresystem.ui.UIConstant;
import com.telefield.smartcaresystem.ui.common.IDialogChangeListener;
import com.telefield.smartcaresystem.util.SmartCarePreference;
import com.telefield.smartcaresystem.util.SpeechUtil;

public class ActivitySensorSettingAdapterUI extends BaseAdapter implements IDialogChangeListener{
	GlobalMessageHandler mGlobalMessageHandler;
	Context mContext;
	String[] data;
	ArrayList<Sensor> mSensor;
	int mode;
	
	public ActivitySensorSettingAdapterUI(Context mContext, String[] data, int mode) {
		this.mContext = mContext;
		this.data = data;
		this.mode = mode;
	}
	
	public ActivitySensorSettingAdapterUI(Context mContext, Object obj, int mode) {
		this.mContext = mContext;
		mSensor = (ArrayList<Sensor>) obj;
		this.mode = mode;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if(mode == UIConstant.MODE_LED_ON_OFF_SETTING)
			return mSensor.size();
		else
			return data.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		if(mode == UIConstant.MODE_LED_ON_OFF_SETTING)
			return mSensor.get(position);
		else
			return data[position];
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;
		
		if(mode == UIConstant.MODE_ANNOUNCE_MENT_SETTING) {
			if(view == null) {
				LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = inflater.inflate(R.layout.child_system_sub_menu_with_checkbox, parent, false);
				
				TextView menuTitle = (TextView) view.findViewById(R.id.menuTitle);
				menuTitle.setText(data[position]);
				
				ImageView image = (ImageView) view.findViewById(R.id.menuItemImage);
				image.setVisibility(View.GONE);
				TextView info = (TextView) view.findViewById(R.id.info);
				info.setVisibility(View.GONE);
				
				CheckBox check = (CheckBox) view.findViewById(R.id.onlyCheckBox);
				
				final SmartCarePreference pref = new SmartCarePreference(mContext, SmartCarePreference.SENSOR_SETTING_INFO_FILENAME);
				
				if(position == 0) {
					check.setChecked(pref.getBooleanValue(SmartCarePreference.DOOR_OPEN_CLOSE, true));
				}
				else if(position == 1) {
					check.setChecked(pref.getBooleanValue(SmartCarePreference.OUT_COME, true));
				}
				else if(position == 2) {
					check.setChecked(pref.getBooleanValue(SmartCarePreference.FIRE_ANNOUNCE, false));
				}
				
				check.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
						// TODO Auto-generated method stub
						if(position == 0) {
							pref.setBooleanValue(SmartCarePreference.DOOR_OPEN_CLOSE, isChecked);
						}
						else if(position == 1) {
							pref.setBooleanValue(SmartCarePreference.OUT_COME, isChecked);
						}
						else if(position == 2) {
							pref.setBooleanValue(SmartCarePreference.FIRE_ANNOUNCE, isChecked);
						}
					}
				});
			}
		}
		else if(mode == UIConstant.MODE_ACTIVITY_SENSOR_SETTING) {
			if(view == null) {
				LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = inflater.inflate(R.layout.child_system_sub_menu, parent, false);
			}
			
			TextView menuTitle = (TextView) view.findViewById(R.id.menuTitle);
			menuTitle.setTextSize(21);
			menuTitle.setText(data[position]);
			
			ImageView image = (ImageView) view.findViewById(R.id.menuItemImage);
			image.setVisibility(View.GONE);
			ImageView expandImage = (ImageView) view.findViewById(R.id.openSubMenuImageView);
			expandImage.setVisibility(View.GONE);
			
			TextView info = (TextView) view.findViewById(R.id.info);
			info.setVisibility(View.GONE);
		}
		else if(mode == UIConstant.MODE_ACTIVITY_SENSOR_SETTING_HIDDEN) {
			if(view == null) {
				LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = inflater.inflate(R.layout.child_system_sub_menu, parent, false);
			}
			
			TextView menuTitle = (TextView) view.findViewById(R.id.menuTitle);
			menuTitle.setTextSize(21);
			menuTitle.setText(data[position]);
			
			ImageView image = (ImageView) view.findViewById(R.id.menuItemImage);
			image.setVisibility(View.GONE);
			ImageView expandImage = (ImageView) view.findViewById(R.id.openSubMenuImageView);
			expandImage.setVisibility(View.GONE);
			
			TextView info = (TextView) view.findViewById(R.id.info);
			
			SmartCarePreference pref = new SmartCarePreference(mContext, SmartCarePreference.SENSOR_SETTING_INFO_FILENAME);
			
			switch(position) {
				case 0:
					info.setText(Integer.toString(pref.getIntValue(SmartCarePreference.NON_ACTIVITY_TIME, UIConstant.DEFAULT_NON_ACTIVITY_TIME_VALUE)) + mContext.getResources().getString(R.string.minute));
					break;
				case 1:
					info.setText(Integer.toString(pref.getIntValue(SmartCarePreference.NON_ACTIVITY_NUM, UIConstant.DEFAULT_NON_ACTIVITY_NUM_VALUE)) + mContext.getResources().getString(R.string.num));
					break;
				case 2:
					info.setText(Integer.toString(pref.getIntValue(SmartCarePreference.DETECT_ACTIVITY_TIME, UIConstant.DEFAULT_DETECT_ACTIVITY_TIME)) + mContext.getResources().getString(R.string.minute));
					break;
			}
		}
		else if(mode == UIConstant.MODE_LED_ON_OFF_SETTING) {
			mGlobalMessageHandler = SmartCareSystemApplication.getInstance().getGlobalMsgFunc();
			
			if(view == null) {
				LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = inflater.inflate(R.layout.child_system_sub_menu_with_two_button, parent, false);
			}
			
			TextView menuTitle = (TextView) view.findViewById(R.id.menuTitle);
			TextView info = (TextView) view.findViewById(R.id.info);
			info.setVisibility(View.GONE);
			
			ImageView image = (ImageView) view.findViewById(R.id.menuItemImage);
			image.setVisibility(View.GONE);
			
			Button ledOn = (Button) view.findViewById(R.id.ledOnButton);
			Button ledOff = (Button) view.findViewById(R.id.ledOffButton);
			
			if(position == mSensor.size()-1) {
				menuTitle.setText(UIConstant.ALL_APPLY);
			}
			else {
				menuTitle.setText(mSensor.get(position).getTypeStr() + mSensor.get(position).getSensorIndex());
			}
			
			ledOn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					MessageManager mgr = SmartCareSystemApplication.getInstance().getMessageManager();
					if(!mgr.ftdi_isConnect()) {
						Toast.makeText(mContext, R.string.not_connected_ftdi, Toast.LENGTH_SHORT).show();
						return;
					}
					
					if(position == mSensor.size()-1) {	//모두 적용
						mGlobalMessageHandler.sendActivityLEDCommandAll(true);
						
						SmartCareSystemApplication.getInstance().getSpeechUtil()
							.speech(SpeechUtil.SPEECH_ALL_ACTIVITY_SENSOR_LED_ON, TextToSpeech.QUEUE_FLUSH, null);
					}
					else {
						int ActivitySensorId = mSensor.get(position).getSensor_id().intValue();
						
						mGlobalMessageHandler.sendActivityLEDCommand(ActivitySensorId, true);
						
						SmartCareSystemApplication.getInstance().getSpeechUtil()
							.speech(SpeechUtil.ACTIVITY_SENSOR_LED_ON, ActivitySensorId, TextToSpeech.QUEUE_FLUSH, null);
					}
				}
			});
			
			ledOff.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					MessageManager mgr = SmartCareSystemApplication.getInstance().getMessageManager();
					if(!mgr.ftdi_isConnect()) {
						Toast.makeText(mContext, R.string.not_connected_ftdi, Toast.LENGTH_SHORT).show();
						return;
					}
					
					if(position == mSensor.size()-1) {	//모두 적용						
						mGlobalMessageHandler.sendActivityLEDCommandAll(false);
						
						SmartCareSystemApplication.getInstance().getSpeechUtil()
						.speech(SpeechUtil.SPEECH_ALL_ACTIVITY_SENSOR_LED_OFF, TextToSpeech.QUEUE_FLUSH, null);
					}
					else {
						int ActivitySensorId = mSensor.get(position).getSensor_id().intValue();
						
						mGlobalMessageHandler.sendActivityLEDCommand(ActivitySensorId, false);
						
						SmartCareSystemApplication.getInstance().getSpeechUtil()
							.speech(SpeechUtil.ACTIVITY_SENSOR_LED_OFF, ActivitySensorId, TextToSpeech.QUEUE_FLUSH, null);
					}
				}
			});
		}
		return view;
	}

	@Override
	public void notifyChangeListener() {
		notifyDataSetChanged();
		
	}

}
