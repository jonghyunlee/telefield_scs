package com.telefield.smartcaresystem.ui.system;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.util.SmartCarePreference;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

public class OldpersonOrDisabledAdepter extends BaseAdapter {
	Context mContext;
	String[] data;
	
	public OldpersonOrDisabledAdepter(Context mContext, String[] data) {
		this.mContext = mContext;
		this.data = data;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data[position];
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;
		
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		//첫번째 위치
		view = inflater.inflate(R.layout.child_system_sub_menu_with_switch, parent, false);
		
		TextView menuTitle = (TextView) view.findViewById(R.id.menuTitle);
		menuTitle.setText(data[position]);
		
		TextView info = (TextView) view.findViewById(R.id.info);
		info.setVisibility(View.GONE);
		ImageView image = (ImageView) view.findViewById(R.id.menuItemImage);
		image.setVisibility(View.GONE);
		ImageView expandImage = (ImageView) view.findViewById(R.id.openSubMenuImageView);
		expandImage.setVisibility(View.GONE);
		
		final SmartCarePreference pref = new SmartCarePreference(mContext, SmartCarePreference.APPLICATION_SETTING_INFO_FILENAME);
		boolean handsetLockOnOff = pref.getBooleanValue(SmartCarePreference.DISABLED_ON_OFF, false);
		
		Switch switchWidget = (Switch) view.findViewById(R.id.onOffSwitch);
		switchWidget.setChecked(handsetLockOnOff);
		switchWidget.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				pref.setBooleanValue(SmartCarePreference.DISABLED_ON_OFF, isChecked);
			}
		});
		
		return view;
	}

}
