package com.telefield.smartcaresystem.ui.system;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.database.ConnectDB;
import com.telefield.smartcaresystem.database.Sensor;
import com.telefield.smartcaresystem.message.SensorID;
import com.telefield.smartcaresystem.ui.LauncherUI;
import com.telefield.smartcaresystem.ui.UIConstant;
import com.telefield.smartcaresystem.util.LogMgr;

public class LEDOnOffSettingUI extends Activity {
	Button backKeyButton, homeKeyButton;
	TextView titleTextView, emptyTextView;
	ListView activitySensorListView;
	ActivitySensorSettingAdapterUI adapter;
	Context mContext;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.see_register_condition);
		
		mContext = this;
		
		Bundle bundle = getIntent().getExtras();
		int mode;
		String title;
		
		if(bundle != null) {
			mode = bundle.getInt(UIConstant.MODE);
			title = bundle.getString(UIConstant.TITLE);
		}
		else {	//강제 세팅
			mode = 3;
			title = "LED ON/OFF 설정";
		}
		
		titleTextView = (TextView) findViewById(R.id.titleTextView);
		titleTextView.setText(title);
		emptyTextView = (TextView) findViewById(R.id.emptyTextView);
		activitySensorListView = (ListView) findViewById(R.id.registerConditionListView);
		
		ConnectDB con = ConnectDB.getInstance(this);
		Sensor sensor = new Sensor();
		sensor.setType(SensorID.getSensorType(UIConstant.ACTIVITY_SENSOR));
		
		List<Sensor> result = con.selectSensor(sensor);
		
		sensor.setType(SensorID.getSensorType(UIConstant.DOOR_SENSOR));
		result.addAll(con.selectSensor(sensor));
		
		LogMgr.d(LogMgr.TAG, "LEDONOFF resultSize : " + result.size());
		for(Sensor sensor2 : result){
			LogMgr.d(LogMgr.TAG, "LEDONOFF sensor2 Name " + sensor2.getTypeStr());
		}
		if(result != null && result.size() > 0) {
			//마지막 리스트에 '모두 적용'을 추가하기 위한 뼈대
			sensor = new Sensor();
			sensor.setType(SensorID.getSensorType(UIConstant.ACTIVITY_SENSOR));
			
			result.add(sensor);
						
			adapter = new ActivitySensorSettingAdapterUI(mContext, result, mode);
			
			activitySensorListView.setVisibility(View.VISIBLE);
			activitySensorListView.setAdapter(adapter);
			
			emptyTextView.setVisibility(View.GONE);
			
			
		}
		else {
			activitySensorListView.setVisibility(View.GONE);
			emptyTextView.setVisibility(View.VISIBLE);
			emptyTextView.setText(R.string.no_activity_sensor);
		}
		
		backKeyButton = (Button) findViewById(R.id.backButton);
		backKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		homeKeyButton = (Button) findViewById(R.id.homeButton);
		homeKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mContext, LauncherUI.class);
				//intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});
	}
}
