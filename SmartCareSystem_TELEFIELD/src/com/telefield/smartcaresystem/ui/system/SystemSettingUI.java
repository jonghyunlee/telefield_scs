package com.telefield.smartcaresystem.ui.system;

import java.io.File;
import java.util.List;
import java.util.Set;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.telefield.smartcaresystem.Constant;
import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.bluetooth.DeviceScan;
import com.telefield.smartcaresystem.database.ConnectDB;
import com.telefield.smartcaresystem.database.DBInfo;
import com.telefield.smartcaresystem.database.Users;
import com.telefield.smartcaresystem.message.MessageID;
import com.telefield.smartcaresystem.message.SensorID;
import com.telefield.smartcaresystem.ui.LauncherUI;
import com.telefield.smartcaresystem.ui.UIConstant;
import com.telefield.smartcaresystem.ui.common.ConfirmDialogUI;
import com.telefield.smartcaresystem.ui.common.ControlDialogUI;
import com.telefield.smartcaresystem.ui.common.InsertNormalDialogUI;
import com.telefield.smartcaresystem.ui.common.PlusMinusDialogUI;
import com.telefield.smartcaresystem.ui.common.ProgressDialogUI;
import com.telefield.smartcaresystem.ui.common.ResultDialogUI;
import com.telefield.smartcaresystem.ui.common.SelectDialogUI;
import com.telefield.smartcaresystem.util.LogMgr;
import com.telefield.smartcaresystem.util.SmartCarePreference;
import com.telefield.smartcaresystem.util.SpeechUtil;

public class SystemSettingUI extends Activity {
	Button backKeyButton, homeKeyButton;
	ProgressDialogUI progressDialog;	//이건 dismiss를 위해 전역에 선언함
	Context mContext;
	ListView menuListView;
	TextView titleTextView;
	SystemMenuAdapterUI mAdapter;
	String[] mainMenuArray;
	int pos;	//ListItem의 Position 저장하는 변수
	public static boolean doNextDialog = false;	//true이면 다음 다이얼로그 보여줌
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.system_setting_ui);
		
		mContext = this;
		
		titleTextView = (TextView) findViewById(R.id.titleTextView);
		titleTextView.setText(R.string.system);
		
		backKeyButton = (Button) findViewById(R.id.backButton);
		backKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
				
				SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.SPEECH_EXIT_SYSTEM_SETTING, TextToSpeech.QUEUE_FLUSH, null);
			}
		});
		
		homeKeyButton = (Button) findViewById(R.id.homeButton);
		homeKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mContext, LauncherUI.class);
				//intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
				
				SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.SPEECH_EXIT_SYSTEM_SETTING, TextToSpeech.QUEUE_FLUSH, null);
			}
		});
		
		menuListView = (ListView) findViewById(R.id.menuListView);
		
		mainMenuArray = getResources().getStringArray(R.array.system);
		mAdapter = new SystemMenuAdapterUI(this, mainMenuArray);
		menuListView.setAdapter(mAdapter);
		
		menuListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				if(position > mAdapter.START_POS && position < mAdapter.HDR_POS1) {	//사용자 설정
					InsertNormalDialogUI dialog = InsertNormalDialogUI.newInstance(mAdapter, InsertNormalDialogUI.MODE_USER_NAME);
					
					if(position == mAdapter.START_POS+1) {
						dialog.setTitle(mainMenuArray[position]);
						dialog.show(getFragmentManager(), "Dialog");
					}
				}
				else if(position > mAdapter.HDR_POS1 && position < mAdapter.HDR_POS2) {	//전화번호 설정
					InsertNormalDialogUI dialog = InsertNormalDialogUI.newInstance(mAdapter);
					
					if(position == mAdapter.HDR_POS1+1) {
						dialog.setMode(InsertNormalDialogUI.MODE_119);
					}
					else if(position == mAdapter.HDR_POS1+2) {
						dialog.setMode(InsertNormalDialogUI.MODE_CENTER);
					}
					else if(position == mAdapter.HDR_POS1+3) {
						dialog.setMode(InsertNormalDialogUI.MODE_HELPER);
					}
					else if(position == mAdapter.HDR_POS1+4) {
						dialog.setMode(InsertNormalDialogUI.MODE_CARE1);
					}
					else if(position == mAdapter.HDR_POS1+5) {
						dialog.setMode(InsertNormalDialogUI.MODE_CARE2);
					}
					
					dialog.setTitle(mainMenuArray[position]);
					dialog.show(getFragmentManager(), "Dialog");
				}
				else if(position > mAdapter.HDR_POS2 && position < mAdapter.HDR_POS3) {	//센서 환경 설정
					if(position == mAdapter.HDR_POS2+1 || position == mAdapter.HDR_POS2+2) {
						Intent intent = new Intent(mContext, ActivitySensorSettingUI.class);
						
						if(position == mAdapter.HDR_POS2+1) {
							intent.putExtra(UIConstant.MODE, UIConstant.MODE_ANNOUNCE_MENT_SETTING);
							intent.putExtra(UIConstant.TITLE, mContext.getString(R.string.announce_ment_setting));
						}
						else {
							intent.putExtra(UIConstant.MODE, UIConstant.MODE_ACTIVITY_SENSOR_SETTING);
							intent.putExtra(UIConstant.TITLE, mContext.getString(R.string.activity_sensor_setting));
						}
						mContext.startActivity(intent);
					}
				}
				else if(position > mAdapter.HDR_POS3 && position < mAdapter.HDR_POS4) {	//통화 설정
					if(position == mAdapter.HDR_POS3+1) {
						Uri storedRingtoneUri = RingtoneManager.getActualDefaultRingtoneUri(mContext, RingtoneManager.TYPE_RINGTONE);
						
						Intent intent  = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
						intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, storedRingtoneUri);
						intent.putExtra(RingtoneManager.EXTRA_RINGTONE_DEFAULT_URI, storedRingtoneUri);
						startActivityForResult(intent, Constant.REQUEST_CODE_0);
					}
					else if(position == mAdapter.HDR_POS3+2 || position == mAdapter.HDR_POS3+3) {
						ControlDialogUI dialog = new ControlDialogUI();
						
						if(position == mAdapter.HDR_POS3+2) {
							dialog.setMode(ControlDialogUI.MODE_RING);
						}
						else if(position == mAdapter.HDR_POS3+3) {
							dialog.setMode(ControlDialogUI.MODE_VOICE_CALL);
						}
						
						dialog.setTitle(mainMenuArray[position]);
						dialog.show(getFragmentManager(), "Dialog");
					
					}
					else if(position == mAdapter.HDR_POS3+4) {
						InsertNormalDialogUI dialog = InsertNormalDialogUI.newInstance(mAdapter, InsertNormalDialogUI.MODE_AUTO_RECEIVE_TIME);
						dialog.setTitle(mainMenuArray[position]);
						dialog.show(getFragmentManager(), "Dialog");
					}
				}
				else if(position > mAdapter.HDR_POS4 && position < mAdapter.HDR_POS5) {	//애플리케이션 설정
					if(position == mAdapter.HDR_POS4+1) {
						PlusMinusDialogUI dialog = new PlusMinusDialogUI();
						dialog.setMode(PlusMinusDialogUI.MODE_MUSIC);
						dialog.setTitle(mainMenuArray[position]);
						dialog.show(getFragmentManager(), "Dialog");
					}
					else if(position == mAdapter.HDR_POS4+2) {
						ConfirmDialogUI dialog = ConfirmDialogUI.newInstance(ConfirmDialogUI.MODE_RELEASE_LOCKER);
						dialog.setTitle(mainMenuArray[position]);
						dialog.show(getFragmentManager(), "Dialog");
					}
					else if(position == mAdapter.HDR_POS4+3) {
						SelectDialogUI dialog = SelectDialogUI.newInstance(mAdapter, SelectDialogUI.MODE_CHANGE_LCD_OFF_TIME);
						dialog.setTitle(mainMenuArray[position]);
						dialog.show(getFragmentManager(), "Dialog");
					}
					else if(position == mAdapter.HDR_POS4+4) {
						Intent intent  = new Intent(mContext, SelfCallSettingUI.class);
						intent.putExtra(UIConstant.TITLE, mainMenuArray[position]);
						intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(intent);
					}
					else if(position == mAdapter.HDR_POS4+5) {
						Intent intent = new Intent(mContext, ChargeDischargeTimeSettingUI.class);
						intent.putExtra(UIConstant.TITLE, mainMenuArray[position]);
						intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(intent);
					}
					else if(position == mAdapter.HDR_POS4+6) {
						Intent intent = new Intent(mContext, HandsetLockSettingUI.class);
						intent.putExtra(UIConstant.TITLE, mainMenuArray[position]);
						intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(intent);
					}
					else if(position == mAdapter.HDR_POS4+7) {
						ConfirmDialogUI dialog = ConfirmDialogUI.newInstance(ConfirmDialogUI.MODE_APPLICATION_UPDATE);
						dialog.setTitle(mainMenuArray[position]);
						dialog.show(getFragmentManager(), "Dialog");
					}
					else if(position == mAdapter.HDR_POS4+8) {
						ConfirmDialogUI dialog = ConfirmDialogUI.newInstance(ConfirmDialogUI.MODE_ZIGBEE_UPDATE);
						dialog.setTitle(mainMenuArray[position]);
						dialog.show(getFragmentManager(), "Dialog");
					}
					else if(position == mAdapter.HDR_POS4+9) {
						pos = position;
						DialogStep dialogStep= new DialogStep();
						dialogStep.start();		
						
					}else if(position == mAdapter.HDR_POS4+10) {
						InsertNormalDialogUI dialog = InsertNormalDialogUI.newInstance(mAdapter,InsertNormalDialogUI.MODE_PASSWORD_CHANGE);
						dialog.setTitle(mainMenuArray[position]);
						dialog.show(getFragmentManager(), "Dialog");
					}

				}
				else if(position > mAdapter.HDR_POS5 && position < mAdapter.HDR_POS6) {	//서버 설정
					InsertNormalDialogUI dialog = null;
					
					if(position == mAdapter.HDR_POS5+1) {
						dialog = InsertNormalDialogUI.newInstance(mAdapter, InsertNormalDialogUI.MODE_IPADDRESS);
					}
					else if(position == mAdapter.HDR_POS5+2) {
						dialog = InsertNormalDialogUI.newInstance(mAdapter, InsertNormalDialogUI.MODE_PORT);
					}
					
					if(dialog != null) {
						dialog.setTitle(mainMenuArray[position]);
						dialog.show(getFragmentManager(), "Dialog");
					}
				}
				/**
				 * 개통 시간을 누르면 서버 주기보고 시간설정이 되던 문제 있음.
				 */
				else if(position > mAdapter.HDR_POS6 && position <= mAdapter.HDR_POS6+1) {	//개통 시간
				}
			}
		});
	}
	
//	private ServiceConnection mConnection = new ServiceConnection() {
//
//	    public void onServiceDisconnected(ComponentName className) {
//	    	appMonitorService = null;
//	    }
//
//		@Override
//		public void onServiceConnected(ComponentName name, IBinder service) {
//			AppMonitorService.MonitorServiceBinder b = (AppMonitorService.MonitorServiceBinder)service;
//			appMonitorService = b.getService();			
//		}
//	  };
	  
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch(requestCode) {
			case Constant.REQUEST_CODE_0:
				if(resultCode == -1) {
					Uri uri = data.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);
					RingtoneManager.setActualDefaultRingtoneUri(this, RingtoneManager.TYPE_RINGTONE, uri);
					updateView();
					SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.SPEECH_CHANGE_RINGTONE, TextToSpeech.QUEUE_FLUSH, null);
				}
				break;
		}
	}
	
	public void updateView(){
		mAdapter.notifyDataSetChanged();
	}
	
	public class DialogStep extends Thread {
		boolean wait = true;
		
		@Override
		public void run() {
			for(int i=0;i<3;i++) {
				if(i == 0)
					doNextDialog = true;	//첫번재 팝업은 무조건 나와야 함
				
				if(!doNextDialog)
					break;
				
				doNextDialog = false;
				wait = true;
				
				if(i == 0) {	//Password 일치 여부 확인
					InsertNormalDialogUI dialog = InsertNormalDialogUI.newInstance(null, InsertNormalDialogUI.MODE_RESET_PASSWORD);
					dialog.setTitle(mainMenuArray[pos]);
					dialog.setDialogStepThread(this);
					dialog.show(getFragmentManager(), "Dialog");
				}
				else if(i == 1) {	//삭제할 것인지 묻는 팝업
					ConfirmDialogUI dialog = ConfirmDialogUI.newInstance(ConfirmDialogUI.MODE_SYSTEM_RESET);
					dialog.setTitle(mainMenuArray[pos]);
					dialog.setDialogStepThread(this);
					dialog.show(getFragmentManager(), "Dialog");
				}
				else if(i == 2) {
					progressDialog = new ProgressDialogUI();
					progressDialog.setTitle(mainMenuArray[pos]);
					progressDialog.setMode(ProgressDialogUI.MODE_INITIALIZE_SENSOR_FINAL_STEP);
					progressDialog.show(getFragmentManager(), "Dialog");
					
					InitializeProgress doingInitialize = new InitializeProgress();
					doingInitialize.execute();
					
					wait = false;	//마지막 단계이므로 여기서 wait할 필요가 없다.
				}
				
				while(wait) {
					try {
						Thread.sleep(200);								
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						LogMgr.d(LogMgr.TAG,"interrupted!!!");
						wait = false;
					}
				}
			}
		}
	}
	
	public class InitializeProgress extends AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			LogMgr.d(LogMgr.TAG, "Initialize :: doInBackground");
			
			//초기화 작업
			//사용자 설정 초기화
			SmartCarePreference pref = new SmartCarePreference(mContext, SmartCarePreference.USER_SETTING_INFO_FILENAME);
			pref.setStringValue(SmartCarePreference.USER_NAME, SmartCarePreference.DEFAULT_COMMON_STRING);	//사용자명
			pref.setStringValue(SmartCarePreference.GATEWAY_MAC, SmartCarePreference.DEFAULT_COMMON_STRING);	//게이트웨이 MAC
			
			//전화번호 설정 초기화
			ConnectDB dbConnect = ConnectDB.getInstance(mContext);
			dbConnect.deleteAllUser();
			//119는 Default가 119이므로 다시 DB에 넣어야 한다.
			List<Users> tempData = dbConnect.selectUser(new Users(null, null, null, null, DBInfo.HELPER_TYPE_119));

			Users get_user = new Users();
			long id;
			if (tempData.size() == 1) {
				id = tempData.get(0).getId();
				get_user.setId(id);
			} else {
				// userType을 기준으로 DB에 1개 이상이 저장되어 있는 해당 userType을 가지고 있는 데이터를 모두 삭제
				dbConnect.deleteUser(new Users(null, DBInfo.HELPER_NAME_119, null, null, DBInfo.HELPER_TYPE_119));
			}
			get_user.setType(DBInfo.HELPER_TYPE_119);
			get_user.setNumber("119");
			get_user.setName(DBInfo.HELPER_NAME_119);
			dbConnect.insertUser(get_user);
			
			//센서 환경 설정 초기화
			pref = new SmartCarePreference(mContext, SmartCarePreference.SENSOR_SETTING_INFO_FILENAME);
			pref.setIntValue(SmartCarePreference.PERIODIC_REPORT_TIME, UIConstant.DEFAULT_PERIODIC_REPORT_TIME_VALUE);	//통신 주기 시간
			pref.setBooleanValue(SmartCarePreference.FIRE_ANNOUNCE, false);
			SmartCareSystemApplication.getInstance().getGlobalMsgFunc().sendKeepAliveTimeMsg(UIConstant.DEFAULT_PERIODIC_REPORT_TIME_VALUE );
			
			pref.setBooleanValue(SmartCarePreference.DOOR_OPEN_CLOSE, true);	//문열림/닫힘 안내 멘트
			pref.setBooleanValue(SmartCarePreference.OUT_COME, true);	//외출/재실 안내 멘트
			pref.setIntValue(SmartCarePreference.NON_ACTIVITY_TIME, UIConstant.DEFAULT_NON_ACTIVITY_TIME_VALUE);	//미감지 시간
			pref.setIntValue(SmartCarePreference.NON_ACTIVITY_NUM, UIConstant.DEFAULT_NON_ACTIVITY_NUM_VALUE);	//미감지 횟수
			pref.setIntValue(SmartCarePreference.DETECT_ACTIVITY_TIME, UIConstant.DEFAULT_DETECT_ACTIVITY_TIME);	//활동량 감지 시간
			
			//통화 설정 초기화
			pref = new SmartCarePreference(mContext, SmartCarePreference.CALL_SETTING_INFO_FILENAME);
			pref.setIntValue(SmartCarePreference.AUTO_RECEIVING_TIME, UIConstant.DEFAULT_AUTO_RECEIVE_TIME_VALUE);	//자동착신 응답대기 시간
			pref.setIntValue(SmartCarePreference.CALL_LIMIT_TIME, 10);//통화량 제한 10분으로 초기화
			
			//애플리케이션 설정 초기화
			pref = new SmartCarePreference(mContext, SmartCarePreference.APPLICATION_SETTING_INFO_FILENAME);
			pref.setStringValue(SmartCarePreference.SELF_CALL_FROM_TIME, UIConstant.DEFAULT_START_SELF_CALL_TIME_VALUE);	//셀프콜 금지 시작 시간
			pref.setStringValue(SmartCarePreference.SELF_CALL_TO_TIME, UIConstant.DEFAULT_END_SELF_CALL_TIME_VALUE);	//셀프콜 금지 종료 시간
			pref.setStringValue(SmartCarePreference.OPENNING_RESULT, null);	//개통 현황
			pref.setBooleanValue(SmartCarePreference.START_APP_MONITORING, true);	//앱 모니터링
			pref.setBooleanValue(SmartCarePreference.CHARGE_STATUSBAR_STATE, true);	//상태바 비활성화
			pref.setBooleanValue(SmartCarePreference.SELF_CALL_ON_OFF, false);	//셀프콜 On/Off
			pref.setBooleanValue(SmartCarePreference.HANDSEST_LOCK_ON_OFF, false);	//송수화기 잠금 On/Off
			
			//서버 설정 초기화
			pref = new SmartCarePreference(mContext, SmartCarePreference.ADDRESS_INFO_FILENAME);
			pref.setStringValue(SmartCarePreference.M2M_IP, UIConstant.DEFAULT_SERVER_IP);	//M2M Server IP
			pref.setIntValue(SmartCarePreference.M2M_PORT, UIConstant.DEFAULT_SERVER_PORT);	//M2M Server Port
			
			//서버 주기 보고 시간 설정 초기화
			pref = new SmartCarePreference(mContext, SmartCarePreference.SERVER_SETTING_INFO_FILENAME);
			pref.setIntValue(SmartCarePreference.SERVER_PERIODIC_REPORT_TIME, UIConstant.DEFAULT_PERIODIC_SEND_TIME_VALUE);	//주기 보고 시간
			
			//Hidden Menu 초기화
			pref = new SmartCarePreference(mContext, SmartCarePreference.HIDDEN_MENU_SETTING_INFO_FILENAME);
			pref.setBooleanValue(SmartCarePreference.FIRMWARE_VERIFICATION_SETTING, false);	//펌웨어 Verification 설정
			pref.setIntValue(SmartCarePreference.START_ADDRESS, UIConstant.DEFAULT_START_ADDRESS);	//시작 주소
			pref.setIntValue(SmartCarePreference.ADDRESS_SIZE, UIConstant.DEFAULT_ADDRESS_SIZE);	//크기
			
			
			
			//DB 삭제
			dbConnect.deleteAllActivityData();
			dbConnect.deleteAllCallLog();
			dbConnect.deleteAllCare();
			dbConnect.deleteAllMessageHistory();
			dbConnect.deleteAllSensor();
			dbConnect.deleteAllSensorHistory();
			dbConnect.deleteAllBloodPressure();
			
			// SensorDeviceManager 초기화
			SmartCareSystemApplication.getInstance().getSensorDeviceManager().clearSensorList();
			SmartCareSystemApplication.getInstance().getMessageManager().sendMessage((SensorID.CODI_SENSOR_TYPE.getSensorType() << 8) | 0x01, (byte)MessageID.REG_MAC_REQ , null);
			
			//이부분에서 exception이나서 앱초기화시에 앱이 다시 안켜짐...이유는모르겠음.
			/*if(!Constant.IS_RELEASE_VERSION)
				SmartCareSystemApplication.getInstance().updateProtocolManager();*/
			
			//페어링 된 혈압계가 있는 경우 언페어링
			DeviceScan dev = new DeviceScan();
			
			if(!dev.isTurnOn()) {
//				dev.turnOnBluetooth();
			}
			
//			while(!dev.isTurnOn()) { 
//				if(dev.isTurnOn()) {
//					break;
//				}
//			}
//			
//			Set<BluetoothDevice> pairedDevice = dev.getPairedDevice();
//			
//			if(pairedDevice.size() > 0) {
//				for(BluetoothDevice device : pairedDevice) {
//					dev.unPairDevice(device);
//				}
//			}
			
			UIConstant.INIT_PASSWORD_NUM = "1351";
			UIConstant.INIT_SETTING_PASSWORD = "1351";
			File file = new File(Constant.LOCKSCREEN_IMAGE_PATH);
			if(file.exists())
				file.delete();
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			LogMgr.d(LogMgr.TAG, "Initialize :: onPostExecute");
			progressDialog.dismiss();
			
			//timer 재시작 부분			
			SmartCareSystemApplication.getInstance().stopNonActivityReportTimer();
			SmartCareSystemApplication.getInstance().startNonActivityReportTimer();
			SmartCareSystemApplication.getInstance().stopPeriodReportTimer();
			SmartCareSystemApplication.getInstance().startPeriodReportTimer();
			
			ResultDialogUI dialog = ResultDialogUI.newInstance(mAdapter, ResultDialogUI.MODE_INITIALIZE_SYSTEM_COMPLETE);
			dialog.setTitle(mainMenuArray[pos]);
			dialog.show(getFragmentManager(), "Dialog");
			
			SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.SPEECH_INITIALIZE_COMPLETE, TextToSpeech.QUEUE_FLUSH, null);
			
			SmartCareSystemApplication.getInstance().getGlobalMsgFunc().sendGateWayMsg((byte)MessageID.CODI_SYSTEM_REBUILD);
		}
	}
}

