package com.telefield.smartcaresystem.ui.hiddenmenu;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.util.AppMonitorService;
import com.telefield.smartcaresystem.util.LogMgr;
import com.telefield.smartcaresystem.util.SmartCarePreference;

public class AppMonitoringSettingAdapterUI extends BaseAdapter {
	Context mContext;
	String[] data;
	boolean[] preferenceValues;
	
	public AppMonitoringSettingAdapterUI(Context mContext, String[] data) {
		this.mContext = mContext;
		this.data = data;
		
		preferenceValues = new boolean[data.length];
		for(int i=0;i<preferenceValues.length;i++) {
			preferenceValues[i] = false;
		}
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data[position];
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;
		if(view == null) {
			LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.child_system_sub_menu_with_switch, parent, false);
		}
		
		TextView menuTitle = (TextView) view.findViewById(R.id.menuTitle);
		menuTitle.setText(data[position]);
		
		TextView info = (TextView) view.findViewById(R.id.info);
		info.setVisibility(View.GONE);
		ImageView image = (ImageView) view.findViewById(R.id.menuItemImage);
		image.setVisibility(View.GONE);
		ImageView expandImage = (ImageView) view.findViewById(R.id.openSubMenuImageView);
		expandImage.setVisibility(View.GONE);
		
		final SmartCarePreference pref = new SmartCarePreference(mContext, SmartCarePreference.APPLICATION_SETTING_INFO_FILENAME);
		boolean isAppMonitoring = pref.getBooleanValue(SmartCarePreference.START_APP_MONITORING, true);
		boolean disableSatusbar = pref.getBooleanValue(SmartCarePreference.CHARGE_STATUSBAR_STATE, true);
		
		if(position < preferenceValues.length) {
			if(position == 0)
				preferenceValues[position] = isAppMonitoring;
		}
		
		Switch switchWidget = (Switch) view.findViewById(R.id.onOffSwitch);
		if(position == 0) {
			switchWidget.setChecked(preferenceValues[position]);
			
			startOrStopAppMonitoring(isAppMonitoring, pref);
			
			SmartCareSystemApplication.getInstance().getEnableDisableStatusBar().enableOrDisableStatusBar(disableSatusbar);
		}

		
		switchWidget.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(preferenceValues[position]) {
					preferenceValues[position] = false;
					
					if(position == 0) {
						startOrStopAppMonitoring(preferenceValues[position], pref);
						pref.setBooleanValue(SmartCarePreference.CHARGE_STATUSBAR_STATE, preferenceValues[position]);
						
						SmartCareSystemApplication.getInstance().getEnableDisableStatusBar().enableOrDisableStatusBar(preferenceValues[position]);
					
					}
				}
				else {
					preferenceValues[position] = true;
					
					if(position == 0) {
						startOrStopAppMonitoring(preferenceValues[position], pref);
						pref.setBooleanValue(SmartCarePreference.CHARGE_STATUSBAR_STATE, preferenceValues[position]);
						
						SmartCareSystemApplication.getInstance().getEnableDisableStatusBar().enableOrDisableStatusBar(preferenceValues[position]);
					}
							
				}
			}
		});
		
		return view;
	}
	
	public void startOrStopAppMonitoring(boolean value, SmartCarePreference preference) {
		if(value) {
			if(!AppMonitorService.isAppMonitorService()) {
				mContext.startService(new Intent(mContext, AppMonitorService.class));
			}
		}
		else {
			mContext.stopService(new Intent(mContext, AppMonitorService.class));
		}
		
		AppMonitorService.setAppMonitorService(value);
		
		preference.setBooleanValue(SmartCarePreference.START_APP_MONITORING, value);
	}
}
