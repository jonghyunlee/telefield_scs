package com.telefield.smartcaresystem.ui.hiddenmenu;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.ui.LauncherUI;
import com.telefield.smartcaresystem.ui.UIConstant;

public class AppMonitoringSettingUI extends Activity {
	Context mContext;
	ListView AppMonitoringListView;
	Button backKeyButton, homeKeyButton;
	AppMonitoringSettingAdapterUI adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.see_register_condition);
		
		Bundle bundle = getIntent().getExtras();
		String title;
		
		if(bundle != null) {
			title = bundle.getString(UIConstant.TITLE);
		}
		else {
			title = "앱 모니터링";	//강제 세팅
		}
		
		mContext = this;
		
		TextView titleTextView, emptyTextView;
		titleTextView = (TextView) findViewById(R.id.titleTextView);
		titleTextView.setText(title);
		emptyTextView = (TextView) findViewById(R.id.emptyTextView);
		emptyTextView.setVisibility(View.GONE);
		
		String[] itemArray = {title + ", 상태바"/*, "상태바"*/};
		adapter = new AppMonitoringSettingAdapterUI(mContext, itemArray);
		
		AppMonitoringListView = (ListView) findViewById(R.id.registerConditionListView);
		AppMonitoringListView.setAdapter(adapter);
		
		backKeyButton = (Button) findViewById(R.id.backButton);
		backKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		homeKeyButton = (Button) findViewById(R.id.homeButton);
		homeKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mContext, LauncherUI.class);
				//intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});
	}
}
