package com.telefield.smartcaresystem.ui.hiddenmenu;

import java.io.IOException;

import org.xmlpull.v1.XmlPullParserException;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.XmlResourceParser;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.ui.UIConstant;
import com.telefield.smartcaresystem.ui.common.IDialogChangeListener;
import com.telefield.smartcaresystem.util.SmartCarePreference;

public class SystemFirmwareSettingAdapterUI extends BaseAdapter implements IDialogChangeListener {
	Context mContext;
	String[] data;
	TextView prohibitTitle, prohibitInfo;
	
	public SystemFirmwareSettingAdapterUI(Context mContext, String[] data) {
		this.mContext = mContext;
		this.data = data;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data[position];
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;
		
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		if (position == 0) {
			view = inflater.inflate(R.layout.child_system_sub_menu_with_switch, parent, false);
			
			TextView menuTitle = (TextView) view.findViewById(R.id.menuTitle);
			menuTitle.setText(data[position]);
			
			TextView info = (TextView) view.findViewById(R.id.info);
			info.setVisibility(View.GONE);
			ImageView image = (ImageView) view.findViewById(R.id.menuItemImage);
			image.setVisibility(View.GONE);
			ImageView expandImage = (ImageView) view.findViewById(R.id.openSubMenuImageView);
			expandImage.setVisibility(View.GONE);
			
			final SmartCarePreference pref = new SmartCarePreference(mContext, SmartCarePreference.HIDDEN_MENU_SETTING_INFO_FILENAME);
			boolean isFirmwareVerification = pref.getBooleanValue(SmartCarePreference.FIRMWARE_VERIFICATION_SETTING, false);
			
			Switch switchWidget = (Switch) view.findViewById(R.id.onOffSwitch);
			switchWidget.setChecked(isFirmwareVerification);
			switchWidget.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					// TODO Auto-generated method stub
					pref.setBooleanValue(SmartCarePreference.FIRMWARE_VERIFICATION_SETTING, isChecked);
					
					XmlResourceParser disableParser = mContext.getResources().getXml(R.color.diagnosis_end_color);
					XmlResourceParser enableParser = mContext.getResources().getXml(R.color.diagnosis_wait_color);
				    ColorStateList disableColor = null;
				    ColorStateList enableColor = null;
				    try {
				    	disableColor = ColorStateList.createFromXml(mContext.getResources(), disableParser);
				    	enableColor = ColorStateList.createFromXml(mContext.getResources(), enableParser);
					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				    
				    //Verification On/Off에 따른 글자색 변경
				    if(isChecked) {
				    	if(enableColor != null) {
				    		if(prohibitTitle != null)
				    			prohibitTitle.setTextColor(enableColor);
				    		else if(prohibitInfo != null)
				    			prohibitInfo.setTextColor(enableColor);
				    	}
				    }
				    else {
				    	if(disableColor != null) {
				    		if(prohibitTitle != null)
				    			prohibitTitle.setTextColor(disableColor);
				    		else if(prohibitInfo != null)
				    			prohibitInfo.setTextColor(disableColor);
				    	}
				    }
				    
				    notifyDataSetChanged();
				}
				
			});
			
			
		}
		else {	//시작 주소, 크기
			view = inflater.inflate(R.layout.child_system_sub_menu, parent, false);
			
			prohibitTitle = (TextView) view.findViewById(R.id.menuTitle);
			prohibitTitle.setTextSize(21);
			prohibitTitle.setText(data[position]);
			
			ImageView image = (ImageView) view.findViewById(R.id.menuItemImage);
			image.setVisibility(View.GONE);
			ImageView expandImage = (ImageView) view.findViewById(R.id.openSubMenuImageView);
			expandImage.setVisibility(View.GONE);
			
			prohibitInfo = (TextView) view.findViewById(R.id.info);
			SmartCarePreference pref = new SmartCarePreference(mContext, SmartCarePreference.HIDDEN_MENU_SETTING_INFO_FILENAME);
			
			if(position == 1) {
				prohibitInfo.setText(Integer.toString(pref.getIntValue(SmartCarePreference.START_ADDRESS, UIConstant.DEFAULT_START_ADDRESS)));
			}
			else if(position == 2) {
				prohibitInfo.setText(Integer.toString(pref.getIntValue(SmartCarePreference.ADDRESS_SIZE, UIConstant.DEFAULT_ADDRESS_SIZE)));
			}
			
			boolean isFirmwareVerification = pref.getBooleanValue(SmartCarePreference.FIRMWARE_VERIFICATION_SETTING, false);
			
			if(!isFirmwareVerification) {	//셀프콜이 Off 상태이면 글자색을 회색으로 처리한다.
				XmlResourceParser disableParser = mContext.getResources().getXml(R.color.diagnosis_end_color);
				ColorStateList disableColor = null;
				try {
					disableColor = ColorStateList.createFromXml(mContext.getResources(), disableParser);
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if(disableColor != null && position != 3) {
					prohibitTitle.setTextColor(disableColor);
					prohibitInfo.setTextColor(disableColor);
				}
			}
		}
		
		return view;
	}
	
	@Override
	public void notifyChangeListener() {
		// TODO Auto-generated method stub
		notifyDataSetChanged();
	}
}
