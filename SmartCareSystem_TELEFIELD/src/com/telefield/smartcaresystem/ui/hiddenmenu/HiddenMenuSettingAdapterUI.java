package com.telefield.smartcaresystem.ui.hiddenmenu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.ui.UIConstant;
import com.telefield.smartcaresystem.ui.common.IDialogChangeListener;
import com.telefield.smartcaresystem.util.SmartCarePreference;

public class HiddenMenuSettingAdapterUI extends BaseAdapter implements IDialogChangeListener {
	Context mContext;
	String[] data;
	
	//System SubMenuItem's number
	public static final int MENU_POS1 = 1;
	public static final int MENU_POS2 = 2;
	public static final int MENU_POS3 = 3;
		
	public final int START_POS = 0;							//센서 환경 설정 위치
	public final int HDR_POS1 = MENU_POS2 + 1;				//스마트케어시스템 설정 위치
	public final int HDR_POS2 = HDR_POS1 + MENU_POS3 + 1;	//서버 주기 보고 시간 설정 위치
	public final int HDR_POS3 = HDR_POS2 + MENU_POS1 + 1;	//시스템 펌웨어 설정 위치
	public final int HDR_POS4 = HDR_POS3 + MENU_POS1 + 1;	//초기설정
	HiddenMenuSettingAdapterUI(Context mContext, String[] data) {
		this.mContext = mContext;
		this.data = data;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data[position];
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		String headerText = getHeader(position);
		
		View view = convertView;
		
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		if(headerText != null) {
			view = inflater.inflate(R.layout.child_system_menu, parent, false);	//서브 ListItem이 적용되어 있으므로 null check 없이 그냥 change함
			
			TextView item = (TextView) view.findViewById(R.id.menuText);
			item.setText(headerText);
			
			ImageView image = (ImageView) view.findViewById(R.id.menuImage);
			image.setVisibility(View.GONE);
		}
		else {
			view = inflater.inflate(R.layout.child_system_sub_menu, parent, false);	//헤더ListItem이 적용되어 있으므로 null check 없이 그냥 change함
			
			TextView menuTitle = (TextView) view.findViewById(R.id.menuTitle);
			menuTitle.setTextSize(21);
			menuTitle.setText(data[position]);
			
			ImageView image = (ImageView) view.findViewById(R.id.menuItemImage);
			image.setVisibility(View.GONE);
			ImageView expandImage = (ImageView) view.findViewById(R.id.openSubMenuImageView);
			expandImage.setVisibility(View.GONE);
			
			TextView info = (TextView) view.findViewById(R.id.info);
			
			if(position > START_POS && position < HDR_POS1) {	//센서 환경 설정
				if(position == START_POS+1) {
					SmartCarePreference pref = new SmartCarePreference(mContext, SmartCarePreference.SENSOR_SETTING_INFO_FILENAME);
					info.setText(Integer.toString(pref.getIntValue(SmartCarePreference.PERIODIC_REPORT_TIME, UIConstant.DEFAULT_PERIODIC_REPORT_TIME_VALUE)) + mContext.getResources().getString(R.string.minute));
				}
				else if(position == START_POS+2) {
					info.setVisibility(View.GONE);
					expandImage.setVisibility(View.VISIBLE);
				}
			}
			else if(position > HDR_POS1 && position < HDR_POS2) {	//스마트케어시스템 설정
				info.setVisibility(View.GONE);
				
				if(position == HDR_POS1+1) {
					expandImage.setVisibility(View.VISIBLE);
				}
				else if(position == HDR_POS1+2) {
					expandImage.setVisibility(View.GONE);
				}
				else if(position == HDR_POS1+3){
					info.setVisibility(View.VISIBLE);
					expandImage.setVisibility(View.GONE);
					SmartCarePreference pref = new SmartCarePreference(mContext, SmartCarePreference.CALL_SETTING_INFO_FILENAME);
					info.setText(Integer.toString(pref.getIntValue(SmartCarePreference.CALL_LIMIT_TIME, 10)) + mContext.getResources().getString(R.string.minute));
				}
			}
			else if(position > HDR_POS2 && position < HDR_POS3) {	//서버 주기 보고 시간 설정
				if(position == HDR_POS2+1) {
					SmartCarePreference pref = new SmartCarePreference(mContext, SmartCarePreference.SERVER_SETTING_INFO_FILENAME);
					info.setText(Integer.toString(pref.getIntValue(SmartCarePreference.SERVER_PERIODIC_REPORT_TIME, UIConstant.DEFAULT_PERIODIC_SEND_TIME_VALUE)) + mContext.getResources().getString(R.string.minute));
				}
			}
			else if(position > HDR_POS3 && position < HDR_POS3+2) {	//시스템 펌웨어 설정
				if(position == HDR_POS3+1) {
					info.setVisibility(View.GONE);
					expandImage.setVisibility(View.VISIBLE);
				}
			}
			else if(position > HDR_POS4 && position <= HDR_POS4 + 6){
				info.setVisibility(View.GONE);
				
				if(position == HDR_POS4 + 1){
					expandImage.setVisibility(View.VISIBLE);
				}else if(position == HDR_POS4 + 2){
					expandImage.setVisibility(View.VISIBLE);
				}else if(position == HDR_POS4 + 3){
					expandImage.setVisibility(View.VISIBLE);
				}else if(position == HDR_POS4 + 4){
					expandImage.setVisibility(View.VISIBLE);
				}else if(position == HDR_POS4 + 5){
					expandImage.setVisibility(View.GONE);
				}else if(position == HDR_POS4 + 6){
					expandImage.setVisibility(View.GONE);
				}
				
				
			}
		}
		
		return view;
	}

	@Override
	public void notifyChangeListener() {
		// TODO Auto-generated method stub
		notifyDataSetChanged();
	}
	
	private String getHeader(int position) {
		if(position == START_POS  || position == HDR_POS1 || position == HDR_POS2 || position == HDR_POS3 || position == HDR_POS4) {
			return data[position];
		}
        return null;
	}
}
