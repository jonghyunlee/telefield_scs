package com.telefield.smartcaresystem.ui.hiddenmenu;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.firmware.codi.CodiFirmwareUpgrade;
import com.telefield.smartcaresystem.ui.LauncherUI;
import com.telefield.smartcaresystem.ui.UIConstant;
import com.telefield.smartcaresystem.ui.common.InsertNormalDialogUI;
import com.telefield.smartcaresystem.util.SmartCarePreference;

public class SystemFirmwareSettingUI extends Activity {
	Context mContext;
	ListView systemFirmwareSettingListView;
	Button backKeyButton, homeKeyButton;
	SystemFirmwareSettingAdapterUI adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.see_register_condition);
		
		Bundle bundle = getIntent().getExtras();
		String title;
		
		if(bundle != null) {
			title = bundle.getString(UIConstant.TITLE);
		}
		else {
			title = "앱 모니터링";	//강제 세팅
		}
		
		mContext = this;
		
		TextView titleTextView, emptyTextView;
		titleTextView = (TextView) findViewById(R.id.titleTextView);
		titleTextView.setText(title);
		emptyTextView = (TextView) findViewById(R.id.emptyTextView);
		emptyTextView.setVisibility(View.GONE);
		
		final String[] itemArray = {title, "시작 주소", "크기", "verfication 시작"};
		adapter = new SystemFirmwareSettingAdapterUI(mContext, itemArray);
		
		systemFirmwareSettingListView = (ListView) findViewById(R.id.registerConditionListView);
		systemFirmwareSettingListView.setAdapter(adapter);
		systemFirmwareSettingListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				SmartCarePreference pref = new SmartCarePreference(mContext, SmartCarePreference.HIDDEN_MENU_SETTING_INFO_FILENAME);
				boolean isFirmwareVerification = pref.getBooleanValue(SmartCarePreference.FIRMWARE_VERIFICATION_SETTING, false);
				
				if(isFirmwareVerification) {
					if(position == 1) {
						InsertNormalDialogUI dialog = InsertNormalDialogUI.newInstance(adapter, InsertNormalDialogUI.MODE_START_ADDRESS);
						dialog.setTitle(itemArray[position]);
						dialog.show(getFragmentManager(), "Dialog");
					}
					else if(position == 2) {
						InsertNormalDialogUI dialog = InsertNormalDialogUI.newInstance(adapter, InsertNormalDialogUI.MODE_ADDRESS_SIZE);
						dialog.setTitle(itemArray[position]);
						dialog.show(getFragmentManager(), "Dialog");
					}
					else if(position == 3) {
						CodiFirmwareUpgrade cfu = new CodiFirmwareUpgrade(mContext, true);
						SmartCareSystemApplication.getInstance().setCodiFirmwareUpgrade(cfu);
					}
				}
			}
		});
		
		backKeyButton = (Button) findViewById(R.id.backButton);
		backKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		homeKeyButton = (Button) findViewById(R.id.homeButton);
		homeKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mContext, LauncherUI.class);
				//intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				startActivity(intent);
			}
		});
	}
	
	@Override
	protected void onStop() {
		SmartCareSystemApplication.getInstance().setCodiFirmwareUpgrade(null);		
		super.onStop();
	}

}
