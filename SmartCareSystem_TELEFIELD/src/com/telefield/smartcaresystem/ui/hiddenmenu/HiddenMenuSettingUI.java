package com.telefield.smartcaresystem.ui.hiddenmenu;

import java.lang.reflect.Method;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.internal.telephony.ITelephony;
import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.ui.LauncherUI;
import com.telefield.smartcaresystem.ui.UIConstant;
import com.telefield.smartcaresystem.ui.common.InsertNormalDialogUI;
import com.telefield.smartcaresystem.ui.common.SimpleSelectDialogUI;
import com.telefield.smartcaresystem.ui.system.ActivitySensorSettingUI;
import com.telefield.smartcaresystem.util.KTAPN;
import com.telefield.smartcaresystem.util.LogMgr;

public class HiddenMenuSettingUI extends Activity {
	Button backKeyButton, homeKeyButton;
	ListView menuListView;
	Context mContext;
	HiddenMenuSettingAdapterUI mAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.system_setting_ui);
		
		mContext = this;
		
		TextView titleTextView = (TextView) findViewById(R.id.titleTextView);
		titleTextView.setText(R.string.hidden_menu);
		
		final String[] menuArray = getResources().getStringArray(R.array.hidden_menu);
		
		mAdapter = new HiddenMenuSettingAdapterUI(this, menuArray);
		menuListView = (ListView) findViewById(R.id.menuListView);
		menuListView.setAdapter(mAdapter);
		menuListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				if(position > mAdapter.START_POS && position < mAdapter.HDR_POS1) {	//센서 환경 설정
					if(position == mAdapter.START_POS+1) {
						SimpleSelectDialogUI dialog = SimpleSelectDialogUI.newInstance(mAdapter, SimpleSelectDialogUI.MODE_PERIODIC_REPORT_TIME);
						dialog.setTitle(menuArray[position] + " " + mContext.getResources().getString(R.string.setting));
						dialog.show(getFragmentManager(), "Dialog");
					}
					else if(position == mAdapter.START_POS+2) {
						Intent intent = new Intent(mContext, ActivitySensorSettingUI.class);
						intent.putExtra(UIConstant.MODE, UIConstant.MODE_ACTIVITY_SENSOR_SETTING_HIDDEN);
						intent.putExtra(UIConstant.TITLE, mContext.getString(R.string.activity_sensor_setting));
						intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						mContext.startActivity(intent);
					}
				}
				else if(position > mAdapter.HDR_POS1 && position < mAdapter.HDR_POS2) {	//앱 모니터링
					if(position == mAdapter.HDR_POS1+1) {
						Intent intent = new Intent(mContext, AppMonitoringSettingUI.class);
						intent.putExtra(UIConstant.TITLE, menuArray[position]);
						intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(intent);
					}
					else if(position == mAdapter.HDR_POS1+2) {
						if(KTAPN.isExistKtApn(mContext) <= 0) {
							LogMgr.d(LogMgr.TAG,"insert APN");
							int isertID = KTAPN.InsertKTAPN(mContext);
							if( isertID != -1 ) {
								KTAPN.SetDefaultAPN(mContext,isertID);
								Toast.makeText(SmartCareSystemApplication.getInstance().getApplicationContext(), R.string.inserted_kt_apn, Toast.LENGTH_SHORT).show();
							}
						}
						else {
							Toast.makeText(SmartCareSystemApplication.getInstance().getApplicationContext(), R.string.exist_kt_apn, Toast.LENGTH_SHORT).show();
						}
					}else if(position == mAdapter.HDR_POS1+3){
						InsertNormalDialogUI dialog = InsertNormalDialogUI.newInstance(mAdapter);
						dialog.setMode(InsertNormalDialogUI.MODE_CALL_TIME_LIMIT);
						dialog.setTitle(menuArray[position]);
						dialog.show(getFragmentManager(), "Dialog");
					}
				}
				else if(position > mAdapter.HDR_POS2 && position < mAdapter.HDR_POS3) {	//서버 주기 보고 시간 설정
					SimpleSelectDialogUI dialog = SimpleSelectDialogUI.newInstance(mAdapter, SimpleSelectDialogUI.MODE_REPORT_TIME_PERIOD);
					dialog.setTitle(menuArray[position]);
					dialog.show(getFragmentManager(), "Dialog");
				}
				else if(position > mAdapter.HDR_POS3 && position <= mAdapter.HDR_POS3+1) {	//시스템 펌웨어 설정
					Intent intent = new Intent(mContext, SystemFirmwareSettingUI.class);
					intent.putExtra(UIConstant.TITLE, menuArray[position]);
					startActivity(intent);
				}
				else if(position > mAdapter.HDR_POS4 && position <= mAdapter.HDR_POS4+4) {	//초기설정
					Intent intent = new Intent();
					if(position == mAdapter.HDR_POS4 + 1){//메시지설정
						intent.setClassName("com.android.mms", "com.android.mms.ui.MessagingPreferenceActivity");
					}
					else if(position == mAdapter.HDR_POS4 + 2){//사운드설정
						intent.setAction(Settings.ACTION_SOUND_SETTINGS);
					}
					else if(position == mAdapter.HDR_POS4 + 3){//TTS설정
						intent.setClassName("com.android.settings", "com.android.settings.Settings$TextToSpeechSettingsActivity");
					}
					else if(position == mAdapter.HDR_POS4 + 4){//화면잠금 해제 설정
						intent.setClassName("com.android.settings", "com.android.settings.ChooseLockGeneric");				
					}
					
						if(intent != null){
							intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							startActivity(intent);
						}
					}
					else if(position == mAdapter.HDR_POS4 + 5){//APN 설정
						if(KTAPN.isExistKtApn(mContext) <= 0) {
							LogMgr.d(LogMgr.TAG,"insert APN");
							int isertID = KTAPN.InsertKTAPN(mContext);
							if( isertID != -1 ) {
								KTAPN.SetDefaultAPN(mContext,isertID);
								Toast.makeText(SmartCareSystemApplication.getInstance().getApplicationContext(), R.string.inserted_kt_apn, Toast.LENGTH_SHORT).show();
							}
						}
						else {
							Toast.makeText(SmartCareSystemApplication.getInstance().getApplicationContext(), R.string.exist_kt_apn, Toast.LENGTH_SHORT).show();
						}
						
					}else if(position == mAdapter.HDR_POS4 + 6){//3G 설정
						try {
							TelephonyAidl(getApplicationContext(), true);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
			}
		});
		
		backKeyButton = (Button) findViewById(R.id.backButton);
		backKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		homeKeyButton = (Button) findViewById(R.id.homeButton);
		homeKeyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mContext, LauncherUI.class);
				//intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});
	}
	
	private void TelephonyAidl(Context context, boolean dataconnectivity)
			throws Exception {
		TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
		@SuppressWarnings("rawtypes")
		Class c = Class.forName(tm.getClass().getName());
		Method m = c.getDeclaredMethod("getITelephony");
		m.setAccessible(true);
		ITelephony telephonyService;
		telephonyService = (ITelephony) m.invoke(tm);

		if (dataconnectivity) {
			Toast.makeText(mContext, "3G 연결 완료(약 5초 소요)", Toast.LENGTH_SHORT).show();
			telephonyService.enableDataConnectivity();
		} else {
			telephonyService.disableDataConnectivity();
		}
	}
}
