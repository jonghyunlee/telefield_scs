package com.telefield.smartcaresystem;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.os.CountDownTimer;
import android.speech.tts.TextToSpeech;

import com.telefield.smartcaresystem.database.ActivityData;
import com.telefield.smartcaresystem.database.ConnectDB;
import com.telefield.smartcaresystem.m2m.M2MReportUtil;
import com.telefield.smartcaresystem.ui.UIConstant;
import com.telefield.smartcaresystem.util.LogMgr;
import com.telefield.smartcaresystem.util.SelfCall;
import com.telefield.smartcaresystem.util.SmartCarePreference;

/**
 * 외출,재실 처리 기능 활동량 미보고, 보고 state 상태
 * 
 * 외출 상태로 천이
 * 
 * @author Administrator
 * 
 */
public class MySmartCareState implements ICallBackActivity{

	public static final int DOOR_CLOSE_STATE = 0;
	public static final int DOOR_OPEN_STATE = 1;
	/**
	 * 60초동안 문닫힘이 있을때까지 기다린다.
	 */
	public static final int DOOR_EVENT_WATING_EXPIRE_TIME = 60 * 1000; // 1분
	public static final int WAIT_DECIDE_INOUT = 5 * 60 * 1000; // 5분
//	public static final int OUT_BTN_EXPIRE_TIME = 5 * 60 * 1000; // 5분
	/**
	 * 활동량 미보고 상태 보고상태 : 1 보고안한상태 : 0
	 */
	boolean isReportNonActivity; // 활동량 미보고 상태 보고상태 : 1 보고안한상태 : 0
	/**
	 * 외출 상태:true 재실 상태:false
	 */
	static boolean isOuting = false; // 외출 상태 : 1 재실 상태 : 0
//	boolean isClickOutingBtn; // 외출버튼을 누른경우 : 1 그렇지 않으면 0

	boolean door_open_close_complete;
	/**
	 * 문이 닫혀있다. false , 문이 열려 있다. true
	 */
	static int door_state; // 문이 닫혀있다. 0 , 문이 열려 있다. 1
	/**
	 * 마지막 활동 보고 이후 활동 유무
	 * 있다 : 1 없다 : 0
	 */
	int prev_act_cnt;
	/**
	 * 문 열림 닫힘 이벤트 이후 활동 유무
	 * 있다 : 1 없다 : 0
	 */
	int after_act_cnt;

	Timer door_wating_time;
//	Timer outbtn_click_expire;
	private ConnectDB con;
	private Calendar date;
	private Date nowDate, beforeDate;
	int activityCount;
	int decideMode;
	/**
	 * 외출 모드이고 문이 열렸을때
	 */
	private static final int DECIDE_OUT_OPEN = 0;
	
	/**
	 * 외출 모드이고 문이 닫히고 활동량이 없을때
	 */
	private static final int DECIDE_OUT_CLOSED = 1;
	
	/**
	 * 재실 모드이고 문이 열렸을때
	 */
	private static final int DECIDE_IN_OPEN = 2;
	
	/**
	 * 재실 모드이고 문이 닫히고 활동량이 있을때
	 */
	private static final int DECIDE_IN_CLOSED = 3;
	private boolean alert_fire;
	private boolean alert_gas;
	private boolean alert_119;

	private long report_time; // 이전 활동보고 db insert time( db insert는 1시간 단위 임)
	
	private boolean isHookOn; //현재 hookon 상태인지 체크 , hook on 이면 1 hook off 이면 0
	
	private final int  TRY_OUTING_MODE = 0;
	private final int  TRY_IN_MODE_FIRST = 1;
	private final int  TRY_IN_MODE_MID = 1;
	private final int  TRY_IN_MODE_LAST = 2;
	/**
	 * 0이면 외출모드로 시도  , 1 이면 재실모드로 시도 -1 이면 아무것도 아님.
	 */
	private int enter_mode = -1; // 0이면 외출모드로 시도  , 1 이면 재실모드로 시도 -1 이면 아무것도 아님.
	
	/**
	 * 강제외출해제 timer default 5분
	 */
	MyCountDowntimer myTimer; //강제외출해제 timer default 5분

	public MySmartCareState() {		

		report_time = System.currentTimeMillis() / Constant.TIME_HOUR_UNIT; // 현재시간 보다 1시간 전		
		alert_fire = alert_gas = alert_119 = false;
		
		isOuting = false;
		isReportNonActivity = false;
//		isClickOutingBtn = false;
		
		enter_mode = -1;	
		
		setOutModeExpireTimerSet();

	}

	
	private void setActivityEventListener(int mode)
	{
		
		this.enter_mode = mode;		
		SmartCareSystemApplication.getInstance().getSensorDeviceManager().setActivityEventListener(this);
	}
	private void removeActivityEventListener()
	{
		this.enter_mode = -1;
		SmartCareSystemApplication.getInstance().getSensorDeviceManager().removeActivityEventListener();
	}
	// 문열림/닫힘 이벤트가 발생한 경우
	public void handleDoorEvent(int prev_activit_cnt, boolean isOpen,
			int after_activity_cnt) {
		LogMgr.d(LogMgr.TAG, "handleDoorEvent.....prev_activit_cnt : " + prev_activit_cnt + "...isOpen : " + isOpen + "...after_activity_cnt : " + after_activity_cnt + "...isOutgoing : " + isOuting);
		prev_act_cnt = prev_activit_cnt;
		after_act_cnt = after_activity_cnt;
		
		con = ConnectDB.getInstance(SmartCareSystemApplication
				.getInstance().getApplicationContext());
		date = new GregorianCalendar();
		activityCount = 0;
		//외출 모드였다. --> 활동량이 있으면 제실로 가야함.
		if(isOuting)
		{
			//외출 모드였다. 문이 열렸다. 
			if(isOpen){
				door_state = DOOR_OPEN_STATE;
				decideMode(DECIDE_OUT_OPEN);
			//외출 모드였다. 문이 닫혔다.
			} else {
				door_state = DOOR_CLOSE_STATE;
				//문이 닫혔는데 활동량이 있다면. 바로 제실모드로 변경한다.
				if(after_activity_cnt == 1){
					if(door_wating_time != null){
						door_wating_time.cancel();
						door_wating_time.purge();
						door_wating_time = null;
					}
					setInModeChange();
				//문이 닫히고 활동량이 없으면.	
				}else{
					decideMode(DECIDE_OUT_CLOSED);
				}
			}
		}
		else//재실 모드였다. --> 활동량이 없으면 외출로 가야함.
		{
			//재실 모드였다. 문이 열렸다. //무조건 5분을 기다린다.
			if(isOpen){
				door_state = DOOR_OPEN_STATE;
				decideMode(DECIDE_IN_OPEN);
				
				
			//재실 모드였다. 문이 닫혔다.
			} else {
				door_state = DOOR_CLOSE_STATE;
							
				//문이 닫혔는데 활동량이 있다면.
				if(after_activity_cnt == 1){
					decideMode(DECIDE_IN_CLOSED);
					
				//문이 닫히고 활동량이 없으면. 바로 외출모드로 변경한다.	
				}else{
					if(door_wating_time != null){
						door_wating_time.cancel();
						door_wating_time.purge();
						door_wating_time = null;
					}
					setOutModeChange();
				}
			}
		}

	}
	private void decideMode(int mode){
		decideMode = mode;
		if(door_wating_time != null){
			door_wating_time.cancel();
			door_wating_time.purge();
			door_wating_time = null;
		}
		door_wating_time = new Timer();
		door_wating_time.schedule(new TimerTask() {
			@Override
			public void run() {
				//현재 시간 -1분 부터 -5분까지의 활동량을 검사한다.
				//문이 열려 있는상태에서 5분 뒤에 문이 닫혀있는지 검사한다.
				if(decideMode == DECIDE_IN_OPEN || decideMode == DECIDE_OUT_OPEN)
				{
					if(door_state == DOOR_CLOSE_STATE){
						LogMgr.d(LogMgr.TAG, "MySmartCareState decideMode return.....closing door");
						return;
					}
				}
				Date current = date.getTime();
				date.add(Calendar.MINUTE, -1);
				nowDate = date.getTime();
				date.add(Calendar.MINUTE, -4);
				beforeDate = date.getTime();
				LogMgr.d(LogMgr.TAG, "MySmartCareState current : " + current + "...nowDate : " + nowDate + "...beforeDate : " + beforeDate);
				List<ActivityData> list = con.selectActivityData(beforeDate, nowDate);
				activityCount = 0;
				for(ActivityData getList : list){
					activityCount += getList.getCount().intValue();
				}
				//문이열리고 닫히고 1분 ~ 5분 사이의 활동량을 검사한다.
				switch (decideMode) {
					//외출 상태에서 문이 열린 후 5분동안 활동량이 있으면 재실이다.
				case DECIDE_OUT_OPEN:
						if(activityCount > 0){
							setInModeChange();
						}
					break;
					//외출 상태에서 문이 닫힌 후 5분동안 활동량이 있으면 재실이다.
				case DECIDE_OUT_CLOSED:
						if(activityCount > 0){
							setInModeChange();
						}
					break;
					//재실 상태에서 문이 열린 후 5분동안 활동량이 없으면 외출이다.
				case DECIDE_IN_OPEN:
						if(activityCount == 0){
							setOutModeChange();
						}
					break;
					//재실 상태에서 문이 열리거나 닫힌 후 활동량이 있으면 계속 재실 상태이다.
				case DECIDE_IN_CLOSED:
						if(activityCount > 0){
							
						} else {
							
						}
					break;

				}
				LogMgr.d(LogMgr.TAG, "MySmartCareState decideMode Mode : " + decideMode + "....activityCount : " + activityCount);
				decideMode = -1;
				activityCount = -1;
			}
		}, WAIT_DECIDE_INOUT);
	}
	/*private void decideInOutMode() {
		LogMgr.d(LogMgr.TAG,"decideInOutMode" + "..isOutgoing : " + isOuting + "..door_open_close_complete : " + door_open_close_complete + "..after_act_cnt : " + after_act_cnt );
		
//		SmartCarePreference pref = new SmartCarePreference(SmartCareSystemApplication.getInstance().getApplicationContext(), 
//				SmartCarePreference.SENSOR_SETTING_INFO_FILENAME);
//		
//		int threshold_val = pref.getIntValue(SmartCarePreference.NON_ACTIVITY_NUM, 1);
		
		int threshold_val = 0; //이후 활동량 , 이전활동량은 0,1 로 표현됨.
		
		// 현재 외출 모드인경우
		if (isOuting) {
			if (door_open_close_complete) // 문열림 닫힘모두 왔다.
			{
				if (after_act_cnt > threshold_val) {
					setInModeChange(); // 재실모드로 변경
				}
			} else {
				// 문열림 or 닫힘 event 하나만 나왔다면.
				if (after_act_cnt > threshold_val) {
					setInModeChange(); // 재실모드로 변경

				}
			}
		}
		// 현재 재실 상태인경우
		else {
//			if (isClickOutingBtn) // 외출 버튼을 눌렀다. //외출버튼을 누르지 않더라도 외출 모드로 가야함.
		{
			if (door_open_close_complete) {
				if (after_act_cnt <= threshold_val) 
				{
					setOutModeChange();
				}else{
					//문열림 닫힘 event 가 왔다. 그런데 이후 활동량이 존재 한다.
					//timer 재시작
					countTimer.cancel();						
					countTimer.start();
				}

			} else {
				if (after_act_cnt <= threshold_val) 
				{
					setOutModeChange();
				}
				else{
					//문열림 닫힘 event 가 왔다. 그런데 이후 활동량이 존재 한다.
					//timer 재시작
					countTimer.cancel();
					countTimer.start();
				}
			}
		}
		}

	}*/

	public void setOutModeChange() {
		LogMgr.d(LogMgr.TAG, "setOutModeChange");
		SmartCareSystemApplication.getInstance().getSpeechUtil().speech("외출 모드로 설정 되었습니다.", TextToSpeech.QUEUE_FLUSH, null);
		// 외출 모드로 설정
		isOuting = true; // 외출모드로 변경
	
		countTimer.cancel();
		LogMgr.d(LogMgr.TAG,"countTimer.cancel()");
		enter_mode = -1;	

		// to do need to report at server
		M2MReportUtil.reportOutMode();	
		
		removeActivityEventListener();
		
		//60초 후부터 재실이 가능 하도록 수정
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				setActivityEventListener(TRY_IN_MODE_FIRST); //재실 시도
			}
		}, 60 * 1000);
		
	}

	public void setInModeChange() {
		LogMgr.d(LogMgr.TAG, "setInModeChange");
		SmartCareSystemApplication.getInstance().getSpeechUtil().speech("재실 모드로 설정 되었습니다.", TextToSpeech.QUEUE_FLUSH, null);
		// 재실 모드로 설정
		isOuting = false;
		
		countTimer.cancel();
		LogMgr.d(LogMgr.TAG,"countTimer.cancel()");
		enter_mode = -1;
		// to do need to report at server ( 재실 보고 )
		M2MReportUtil.reportInMode();
		removeActivityEventListener();
	}

	public void eventReceiveHookOff() {
		
		
		boolean isNonActivityReport = isNonActivityReport();	
		LogMgr.d(LogMgr.TAG,"eventReceiveHookOff:: " + isNonActivityReport);
		//셀프콜이 울리고 있으면 종료를 시키자
		SelfCall selfCall = SmartCareSystemApplication.getInstance().getSelfCall();
		
		if( selfCall != null )
		{
			LogMgr.d(LogMgr.TAG,"if selfcall is alerting stop self call " + selfCall.isStartSelfCall());
			if( selfCall.isStartSelfCall() ) selfCall.stopSelfCall();
		}
		
		//활동 미감지 보고를 했다면 , 활동 감지 보고를 한다.
		if (isNonActivityReport) {
			M2MReportUtil.reportActivity();
			setReportNonActivity(false);
		}

		// 사람이 수화기를 들었으니, outing 이면 재실로 변경
		if (isOuting)
			setInModeChange();
	}

	// 10분마다 활동량 데이타 update
//	public void updateAcitivtyPircnt() {
//
//		SensorDeviceManager sensorDeviceMgr = SmartCareSystemApplication
//				.getInstance().getSensorDeviceManager();
//		long curr_time = System.currentTimeMillis() / Constant.TIME_HOUR_UNIT;
//
//		LogMgr.d(LogMgr.TAG, "updateAcitivtyPircnt curr_time: " + curr_time
//				+ "report_time : " + report_time);
//
//		if (curr_time - report_time > 0) // 한시간 이상 차이가 있다면
//		{
//			LogMgr.d(LogMgr.TAG, "updateAcitivtyPircnt db insert");
//			// to do 활동량 db insert 작업
//			sensorDeviceMgr.insertActivityPirCnt();
//
//			report_time = curr_time;
//		}
//	}

	public void setReportNonActivity(boolean isSet) {
		isReportNonActivity = isSet;
	}

	public boolean isNonActivityReport() {
		return isReportNonActivity;
	}
	
	public boolean isOutMode()
	{
		return isOuting;
	}
	
	public void setAlertFire(boolean isAlert)
	{
		alert_fire = isAlert;
	}
	public boolean getAlertFire()
	{
		return alert_fire;
	}	
	public void setAlertGas(boolean isAlert)
	{
		alert_gas = isAlert;
	}
	public boolean getAlertGas()
	{
		return alert_gas;
	}
	
	public void setAlert119(boolean isAlert)
	{
		alert_119 = isAlert;
	}
	
	public boolean getAlert119()
	{
		return alert_119;
	}

	public boolean isHookOn() {
		return isHookOn;
	}

	public void setHookOn(boolean isHookOn) {
		this.isHookOn = isHookOn;
	}

	@Override
	public void notifyActivityCnt(int sensor_id, int cnt) {
//		SmartCarePreference pref = new SmartCarePreference(SmartCareSystemApplication.getInstance().getApplicationContext(), 
//				SmartCarePreference.SENSOR_SETTING_INFO_FILENAME);
//		
//		int threshold_val = pref.getIntValue(SmartCarePreference.NON_ACTIVITY_NUM, 1);
//		
//		LogMgr.d(LogMgr.TAG,"notifyActivityCnt : sensor id:" + sensor_id + "cnt ::" + cnt + "enter_mode:" + enter_mode);
//		
//		if( enter_mode == TRY_OUTING_MODE)
//		{			
//			if( cnt > threshold_val ) //활동량이 감지가 되면 timer 를 reset 한다.
//			{
//				countTimer.cancel();
//				LogMgr.d(LogMgr.TAG,"countTimer.cancel()");
//				countTimer.start();
//			}
//			
//		}else if( enter_mode == TRY_IN_MODE_FIRST )
//		{
//			if( cnt > threshold_val)
//			{
//				countTimer.cancel();
//				LogMgr.d(LogMgr.TAG,"TRY_IN_MODE_FIRST countTimer.cancel()");
//				//countTimer.start();				
//				myTimer.cancel();
//				myTimer.start();
//				enter_mode = TRY_IN_MODE_MID;
//			}
//		}else if( enter_mode == TRY_IN_MODE_LAST )
//		{
//			if( cnt > threshold_val)
//			{
//				setInModeChange();
//				enter_mode = -1;
//			}
//		}
	}	
	
	public void expire5Minute()
	{
		LogMgr.d(LogMgr.TAG,"expire5Minute");
		if(enter_mode==TRY_OUTING_MODE)
		{
			setOutModeChange();			
			
		}else if( enter_mode==TRY_IN_MODE_MID)
		{
			//5분 타이머 만료
			SmartCareSystemApplication.getInstance().getSpeechUtil().speech("타이머 만료", TextToSpeech.QUEUE_FLUSH, null);
			enter_mode = TRY_IN_MODE_LAST;
		}else{
			SmartCareSystemApplication.getInstance().getSpeechUtil().speech("어떤모드인지 모름", TextToSpeech.QUEUE_FLUSH, null);
			setInModeChange();
		}
			
	}
	
	
	
	CountDownTimer countTimer = new CountDownTimer(5*60*1000, 10*1000)	
	{
		@Override
		public void onTick(long millisUntilFinished) {
			// TODO Auto-generated method stub	
			LogMgr.d(LogMgr.TAG,"count timer tick");
		}

		@Override
		public void onFinish() {
			LogMgr.d(LogMgr.TAG,"timer expire");
			expire5Minute();
		}
	};	
	
	class MyCountDowntimer extends CountDownTimer
	{

		public MyCountDowntimer(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onTick(long millisUntilFinished) {
			LogMgr.d(LogMgr.TAG,"count timer tick");
		}

		@Override
		public void onFinish() {
			LogMgr.d(LogMgr.TAG,"timer expire");
			expire5Minute();
		}
		
	}
	
	public void setOutModeExpireTimerSet()
	{
		SmartCarePreference pref = new SmartCarePreference(
				SmartCareSystemApplication.getInstance().getApplicationContext()
					, SmartCarePreference.SENSOR_SETTING_INFO_FILENAME);
		
		int outModeExpireTime = pref.getIntValue(SmartCarePreference.DETECT_ACTIVITY_TIME, UIConstant.DEFAULT_DETECT_ACTIVITY_TIME );		
		
		LogMgr.d(LogMgr.TAG,"setOutModeExpireTimerSet : " + outModeExpireTime);
		if( outModeExpireTime <= 0) return;
			
		if( myTimer != null) myTimer.cancel();
		myTimer = null;
		
		//
		myTimer = new MyCountDowntimer(outModeExpireTime * 60 * 1000 , 10 * 1000);
	}
	
}


