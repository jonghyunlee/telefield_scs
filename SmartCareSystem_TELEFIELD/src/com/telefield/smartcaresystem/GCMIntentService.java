package com.telefield.smartcaresystem;


import java.io.IOException;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;

import com.google.android.gcm.GCMBaseIntentService;
import com.telefield.smartcaresystem.http.HttpUtils;
import com.telefield.smartcaresystem.util.LogMgr;

public class GCMIntentService extends GCMBaseIntentService {
	
	public GCMIntentService() {
		super(Constant.SENDER_ID);
	}
	
	final private String MESSAGE_RECEIVE = "com.google.android.c2dm.intent.RECEIVE";
	final private String PUSH_CMD_PARAM = "push_cmd";
	
	/**
	 * 지그비 업데이트
	 */
	final private String ZIGBEE_FIRMWARE_UPDATE = "ZIGBEE_FIRMWARE";
	/**
	 * 게이트웨이 업데이트
	 */
	final private String GATEWAY_FIRMWARE_UPDATE = "GATEWAY_FIRMWARE";
	/**
	 * 원격 리셋
	 */
	final private String REMOTE_RESET = "RESET";
	/**
	 * 원격 개통
	 */
	final private String REMOTE_OPEN = "OPEN";
	/**
	 * 원격 로그파일 전송
	 */
	final private String LOG_REQUEST = "LOG";
	/**
	 * 미보고 데이터 전송
	 */
	final private String NOT_REPORTED = "NOT_REPORTED";
	/**
	 * 설정정보 변경
	 */
	final private String SENSOR_CONFIG_CHANGE = "SENSOR_CONFIG_CHANGE";
	/**
	 * 비밀번호 변경
	 */
	final private String PASSWORD_CHANGE = "PASSWORD_CHANGE";
	/**
	 * 스마트폰 데이터베이스 백업
	 */
	final private String DB_BACKUP = "DB_BACKUP";
	/**
	 * 배경화면 변경
	 */
	final private String IMAGE_CHANGE = "IMAGE_CHANGE";	
	@Override
	protected void onError(Context arg0, String arg1) {
	}

	@Override
	protected void onMessage(final Context context, Intent intent) {
    	final String action = intent.getAction();
    	final boolean isMessageReceive = MESSAGE_RECEIVE.equals(action);

    	LogMgr.d(GCMIntentHandler.TAG,intent.toString());
    	
    	if (isMessageReceive) {
        	excuteGCM.execute(intent);
        }        
	}
	
	public AsyncTask<Intent, Void, Void> excuteGCM  = new AsyncTask<Intent, Void, Void>() {
		
		@Override
		protected Void doInBackground(Intent... params) {
			Intent intent = null;
			
			if(params != null){
				for(Intent i : params)
					intent = i;
			}
			if(intent == null){
				return null;
			}
			
	    	final String action = intent.getAction();
	    	final boolean isMessageReceive = MESSAGE_RECEIVE.equals(action);
	    	GCMIntentHandler gcmIntentHandler = GCMIntentHandler.getInstance();
	    	gcmIntentHandler.setContext(getApplicationContext());
	      	
	    	if (isMessageReceive) {
	        	
	        	switch(intent.getStringExtra(PUSH_CMD_PARAM)) {
	        		case ZIGBEE_FIRMWARE_UPDATE : 
	        			gcmIntentHandler.handleZigbeeFirmwareUpdate();
	        			break;
	        		case GATEWAY_FIRMWARE_UPDATE :
	        			gcmIntentHandler.handleGatewayFirmwareUpdate();		
	        			break;
	        		case REMOTE_RESET : 
	        			gcmIntentHandler.handleRemoteReset();
	        			break;
	        		case REMOTE_OPEN : 
	        			gcmIntentHandler.handleRemoteOpen();
	        			break;
	        		case LOG_REQUEST :
	        			gcmIntentHandler.handleLogRequest(intent);
	        			break;     		
	        		case NOT_REPORTED:
	        			gcmIntentHandler.handleNotReported();
	        			break;
	        		case SENSOR_CONFIG_CHANGE:
	        			gcmIntentHandler.handleRemoteSetting(intent);
	        			break;
	        		case PASSWORD_CHANGE:
	        			gcmIntentHandler.handlePasswordChange(intent);
	        			break;
	        		case DB_BACKUP:
	        			gcmIntentHandler.handleSendDB();
	        			break;
	        		case IMAGE_CHANGE:
	        			gcmIntentHandler.handleLockscreenImg(intent);
	        			break;
	        		default : 
	        	}
	        }
		
			return null;
		}
	};
	
	@Override
	protected void onRegistered(Context context, String android_device_id) {
		PackageInfo pi = null;
		try {
			pi = getPackageManager().getPackageInfo(getPackageName(), 0);
		} catch (NameNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String version = pi.versionName;
		try {
			final String android_device_id_regist_url = 
					Constant.DEVICE_ID_REGIST_PATH+"/"+SmartCareSystemApplication.getInstance().getMyPhoneNumber()+"/"+android_device_id + "/" + version;
			HttpUtils.INSTANCE.httpUrlGETRequest(android_device_id_regist_url);
			
		} catch (IOException e) {
			LogMgr.d("REGIST_ERR",e.getMessage());
		}
	}
	
	@Override
	protected void onUnregistered(Context context, String android_device_id) {
		
	}
	
}
