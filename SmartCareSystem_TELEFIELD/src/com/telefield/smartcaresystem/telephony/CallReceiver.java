package com.telefield.smartcaresystem.telephony;

import java.util.Timer;
import java.util.TimerTask;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.bluetooth.DeviceScan;
import com.telefield.smartcaresystem.message.MessageID;
import com.telefield.smartcaresystem.ui.InCallUI;
import com.telefield.smartcaresystem.ui.common.CustomCountDownTimer;
import com.telefield.smartcaresystem.ui.common.ProgressDialogUI;
import com.telefield.smartcaresystem.util.LogMgr;

public class CallReceiver extends BroadcastReceiver {
	private static CallStateListner mCallStateListner = null;
	String incomingNumber;
	@Override
	public void onReceive(final Context context, Intent intent) {

		final String phone_state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
		incomingNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
		String outgoingnum = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);

		TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		if(mCallStateListner == null)
		{
			mCallStateListner = CallStateListner.getInstance();
			telephonyManager.listen(mCallStateListner, PhoneStateListener.LISTEN_CALL_STATE);
		}
		
		String action = intent.getAction();

		if (action.equals("android.intent.action.PHONE_STATE")) {
			if (phone_state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
				String number = null;
				
				mCallStateListner.notiActionCallEnd();
				
				if(mCallStateListner.getOutgoingNumber() != null){
					number =  mCallStateListner.getOutgoingNumber();
					mCallStateListner.setOutgoingNumber(null);
				}
				
				SmartCareSystemApplication.getInstance().getGlobalMsgFunc().sendGateWayMsg((byte)MessageID.CALL_ENDED);
				LogMgr.d(LogMgr.TAG, "CallReceiver.java:onReceive:// number : " + number);				

			} else if (phone_state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
				//송수화기 방치 관련 사운드가 나오면(멘트 포함), 사운드를 종료시킨다.
				SmartCareSystemApplication.getInstance().getEmergencyCallUtil().stopNeglectSound();
				
				//센서 등록 중일 때, 센서 등록을 취소해야 한다.
				CustomCountDownTimer timer = CustomCountDownTimer.getInstance();
				LogMgr.d(LogMgr.TAG,"CallReceiver.java:get timer instance : "+timer);
				if(timer != null) {
					timer.stopCustomProgressDialog();
					timer.cancel();
					timer.setNullInstance();
				}
				
				//블루투스 검색 중일 때, 전화가 오면 검색을 중지시킨다.
				DeviceScan dev = new DeviceScan();
				if(dev.isDiscovering())
					dev.cancelScan();
				
				/*
				 * 혈압계 페어링 중일 때, 전화가 오면 Dialog를 없애야 한다.
				 * (단, 환경설정에서 블루투스 페어링 중에 전화가 와도 페어링은 하므로, 페어링은 계속 진행한다.)
				 */
				ProgressDialogUI progressDialog = ProgressDialogUI.getInstance();
				LogMgr.d(LogMgr.TAG,"CallReceiver.java:get ProgressDialog instance : "+progressDialog);
				if(progressDialog != null) {
					progressDialog.dismiss();
					progressDialog.setNullInstance();
				}
				
				if(incomingNumber == null){
					incomingNumber = " ";
				}
				mCallStateListner.setIncommingNumber(incomingNumber);
				new Timer().schedule(new TimerTask() {

					@Override
					public void run() {
						Intent intent = new Intent(context, InCallUI.class);
						intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
						intent.putExtra(TelephonyConfig.CALL_NUMBER, incomingNumber);
						intent.putExtra(TelephonyConfig.CALL_STATE, phone_state);
						context.startActivity(intent);
					}
				}, TelephonyConfig.DELAY_START_ACTIVITY);
				
				SmartCareSystemApplication.getInstance().getGlobalMsgFunc().sendGateWayMsg((byte)MessageID.RINGING_ALERT);
				
			}
			else if (phone_state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
				
				mCallStateListner.notiActionCallOut();
				
				if(mCallStateListner.getIncommingNumber() != null){
					mCallStateListner.setIncommingNumber(null);
				}
				else {
					new Timer().schedule(new TimerTask() {
						
						@Override
						public void run() {
							Intent intent = new Intent(context, InCallUI.class);
							if(mCallStateListner.getOutgoingNumber() != null){
								intent.putExtra(TelephonyConfig.CALL_NUMBER, mCallStateListner.getOutgoingNumber());
//								mCallStateListner.setOutgoingNumber(null);
							}
							intent.putExtra(TelephonyConfig.CALL_STATE, phone_state);
							intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
							context.startActivity(intent);
						}
					}, TelephonyConfig.DELAY_START_ACTIVITY);
				}
				
				SmartCareSystemApplication.getInstance().getGlobalMsgFunc().sendGateWayMsg((byte)MessageID.CALL_CONNECTED);
			}
		}
		else if (action.equals(Intent.ACTION_NEW_OUTGOING_CALL)) {
			mCallStateListner.setOutgoingNumber(outgoingnum);
		}
	}
}
