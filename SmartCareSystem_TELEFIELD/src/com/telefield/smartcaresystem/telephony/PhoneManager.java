package com.telefield.smartcaresystem.telephony;

import java.lang.reflect.Method;

import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;

import com.android.internal.telephony.ITelephony;
import com.telefield.smartcaresystem.util.LogMgr;

public class PhoneManager {

	private Context mContext;

	public PhoneManager(Context context) {
		this.mContext = context;
	}

	/**
	 * 수화기를 들경우 에 전화를 받는다.
	 */
	public void answerCall() {
		TelephonyManager telephonyManager = (TelephonyManager) mContext
				.getSystemService(Context.TELEPHONY_SERVICE);

		int phone_state = CallStateListner.getInstance().getPhoneState();

		if (phone_state == TelephonyManager.CALL_STATE_RINGING) {
			ITelephony telephonyService = null;
			try {
				Class c = Class.forName(telephonyManager.getClass().getName());
				Method m = c.getDeclaredMethod("getITelephony");
				m.setAccessible(true);
				telephonyService = (ITelephony) m.invoke(telephonyManager);
				
				try {
					telephonyService.answerRingingCall();
				} catch (Exception e) {
					answerPhoneHeadsethook();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		} else {
			LogMgr.d(LogMgr.TAG, "Phone State: " + phone_state
					+ "   hook-on evnet ignore.");
		}

	}

	/**
	 * 수화기를 내릴경우 전화를 끊는다.
	 */
	public void endCall() {
		TelephonyManager telephonyManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);

		//int phone_state = CallStateListner.getInstance(mContext).getPhoneState();
		int call_state = telephonyManager.getCallState();
		
		if (call_state == TelephonyManager.CALL_STATE_OFFHOOK) {
			ITelephony telephonyService = null;
			try {
				Class c = Class.forName(telephonyManager.getClass().getName());
				Method m = c.getDeclaredMethod("getITelephony");
				m.setAccessible(true);
				telephonyService = (ITelephony) m.invoke(telephonyManager);

				try {
					telephonyService.endCall();
					
					Thread.sleep(500);
					
				} catch (Exception e) {
					e.printStackTrace();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		} else {
			LogMgr.d(LogMgr.TAG, "Phone State: " + call_state + "   hook-off evnet ignore.");
		}
	}
	
	public int getPhoneState()
	{
		TelephonyManager telephonyManager = (TelephonyManager) mContext
				.getSystemService(Context.TELEPHONY_SERVICE);

		int phone_state = CallStateListner.getInstance().getPhoneState();

		return phone_state;
	}

	private void answerPhoneHeadsethook() {
		// Simulate a press of the headset button to pick up the call
		Intent buttonDown = new Intent(Intent.ACTION_MEDIA_BUTTON);
		buttonDown.putExtra(Intent.EXTRA_KEY_EVENT, new KeyEvent(
				KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_HEADSETHOOK));
		mContext.sendOrderedBroadcast(buttonDown,
				"android.permission.CALL_PRIVILEGED");

		// froyo and beyond trigger on buttonUp instead of buttonDown
		Intent buttonUp = new Intent(Intent.ACTION_MEDIA_BUTTON);
		buttonUp.putExtra(Intent.EXTRA_KEY_EVENT, new KeyEvent(
				KeyEvent.ACTION_UP, KeyEvent.KEYCODE_HEADSETHOOK));
		mContext.sendOrderedBroadcast(buttonUp,
				"android.permission.CALL_PRIVILEGED");
	}

}
