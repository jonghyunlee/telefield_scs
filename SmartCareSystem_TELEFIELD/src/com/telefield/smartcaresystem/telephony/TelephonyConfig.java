package com.telefield.smartcaresystem.telephony;

public interface TelephonyConfig {
	static final String CALL_NUMBER = "NUMBER";
	static final String CALL_STATE = "STATE";
	static final int DELAY_START_ACTIVITY = 2000;
	
}
