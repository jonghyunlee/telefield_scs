package com.telefield.smartcaresystem.telephony;

import java.util.LinkedList;

import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

import com.telefield.smartcaresystem.util.IOnActionNotify;

public class CallStateListner extends PhoneStateListener {
	public static CallStateListner mCallStateListner = null;
	private String outgoingNumber;
	private String incommingNumber;
	private int phone_state;
	
	LinkedList<IOnActionNotify> notifier;
	

	private CallStateListner() {
		super();
		phone_state = TelephonyManager.CALL_STATE_IDLE;
		
		notifier = new LinkedList<IOnActionNotify>();
	}
	
	public void addActionNotifier(IOnActionNotify noti)
	{
		notifier.add(noti);
	}
	
	public void removeActionNotifier(IOnActionNotify noti)
	{
		notifier.remove(noti);
	}
	
	public void notiActionCallOut()
	{
		for(IOnActionNotify noti : notifier )
		{
			noti.makeCallNotify();
		}
	}
	
	public void notiActionCallEnd()
	{
		for(IOnActionNotify noti : notifier )
		{
			noti.endCall();
		}
	}

	public static CallStateListner getInstance() {
		if (mCallStateListner == null)
			mCallStateListner = new CallStateListner();
		return mCallStateListner;
	}

	@Override
	public void onCallStateChanged(int state, String incomingNumber) {

		phone_state = state;

		super.onCallStateChanged(state, incomingNumber);
	}

	public int getPhoneState() {
		return phone_state;
	}

	public String getOutgoingNumber() {
		return outgoingNumber;
	}

	public void setOutgoingNumber(String outgoingNumber) {
		this.outgoingNumber = outgoingNumber;
	}

	public String getIncommingNumber() {
		return incommingNumber;
	}

	public void setIncommingNumber(String incommingNumber) {
		this.incommingNumber = incommingNumber;
	}
}
