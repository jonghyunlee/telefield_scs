package com.telefield.smartcaresystem.util;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.http.util.ByteArrayBuffer;

public class ConvertTime {
	public static byte[] getCurrentTime(){
		long longtime = Calendar.getInstance().getTimeInMillis() / 1000;
		
		return getByteArrayTime(longtime);
	}
	
	public static byte[] getByteArrayTime(long longtime){
		ByteArrayBuffer bf = new ByteArrayBuffer(4);
		
		bf.append((int)((longtime>>24) & 0xff));
		bf.append((int)((longtime>>16) & 0xff));
		bf.append((int)((longtime>>8) & 0xff));
		bf.append((int)((longtime>>0) & 0xff));
		
		return bf.toByteArray();
	}
	
	public static long getLongTime(byte[] bytes) {
		ByteBuffer byte_buf = ByteBuffer.allocate(8);
		final byte[] change = new byte[8];

		for (int i = 0; i < bytes.length; i++) {
			change[7 - i] = bytes[bytes.length - 1 - i];
		}

		byte_buf = ByteBuffer.wrap(change);
		byte_buf.order(ByteOrder.BIG_ENDIAN);

		return byte_buf.getLong() * 1000;
	}
	
	public static Date getDateTime(String time) {
		Date date = null;
		try {
			date = new SimpleDateFormat("yyyyMMddHHmmss").parse(time);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}
}
