package com.telefield.smartcaresystem.util;

import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeSet;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import com.telefield.smartcaresystem.ui.LauncherUI;

public class AppMonitorService extends Service {
	private static boolean monitorServiceFlag = false;
	
	private final IBinder mBinder  = new MonitorServiceBinder();
	
	String myPkg;
	String className;
	TreeSet<String> exceptClasses = new TreeSet<String>();
	TreeSet<String> exceptPkges = new TreeSet<String>();
	
	boolean isStop;
	MonitorWork monitor;

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return mBinder;
	}

	@Override
	public void onStart(Intent intent, int startId) {
		LogMgr.d(LogMgr.TAG,"AppMonitorService onStart");
		
		isStop = false;
		composeExceptClass();
		
		monitor = new MonitorWork();
		monitor.start();
	}	

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
		LogMgr.d(LogMgr.TAG,"AppMonitorService onDestroy");
		monitor.monitorStop();
	}

	private void composeExceptClass()
	{
		myPkg = "com.telefield.smartcaresystem";
		className = "com.telefield.smartcaresystem.ui.LauncherUI";
		
		exceptPkges.add("com.android.phone");
		exceptPkges.add("android");
		
		exceptClasses.add("com.android.internal.app.ResolverActivity");
		exceptClasses.add("com.android.providers.media.RingtonePickerActivity");
		exceptClasses.add("com.android.settings.ChooseLockGeneric");
		exceptClasses.add("com.android.systemui.usb.UsbConfirmActivity");
		exceptClasses.add("com.android.incallui.InCallActivity");
		exceptClasses.add("com.android.settings.bluetooth.BluetoothPairingDialog"); //bluetooth paring 모드
		
		
	}
	
	
	class MonitorWork extends Thread
	{
		boolean isStop = false;
		public void monitorStop()
		{
			isStop = true;
		}
		@Override
		public void run() {
			while(isStop == false)
			{
				ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
				List<RunningTaskInfo> info = am.getRunningTasks(1);
				ComponentName topActivity = info.get(0).topActivity;
				String currPkgName = topActivity.getPackageName();			
				String currClassName = topActivity.getClassName();
			
				
				if(myPkg.equals(currPkgName) == false)
				{
					LogMgr.d(LogMgr.TAG,"AppMonitorService top Activity is :" + currClassName);
					boolean isMatch = false;
					
					Iterator it = exceptPkges.iterator();
					while(it.hasNext())
					{
						String exceptPkg = (String)it.next();
						
						if( exceptPkg.equals(currPkgName))
						{
							isMatch = true;
							break;
						}						
						
					}
					if( isMatch == false )
					{
						it = exceptClasses.iterator();
						while(it.hasNext())
						{
							String exceptClass = (String)it.next();
							
							if( exceptClass.equals(currClassName))
							{
								isMatch = true;
								break;
							}						
							
						}
					}
					
					if( !isMatch)
					{						
						//Intent intent = new Intent(SmartCareSystemApplication.getInstance().getApplicationContext(), LauncherUI.class);
						//intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
						//SmartCareSystemApplication.getInstance().getApplicationContext().startActivity(intent);
						LogMgr.d(LogMgr.TAG,"APPMONITOR DETECT !!!!! start activity");
						
						Intent intent = new Intent(getApplicationContext(),LauncherUI.class);
						intent.setAction("APPMONITOR DETECT");
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);						
						startActivity(intent);
						
						
						
						try {
							Thread.sleep(5000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}			
						
					}
					
					LogMgr.d(LogMgr.TAG,"exec comple");
				}
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
			}
		}
	}
	
	public void removeSettingPackage(){
		exceptClasses.remove("com.android.settings");
		
		Timer allowTime = new Timer();
		allowTime.schedule(new TimerTask() {				
			@Override
			public void run() {
				addSettingPackage();					
			}
		}, 5 * 1000);
	}		
	public void addSettingPackage(){
		exceptClasses.add("com.android.settings");
		
	}
	
	public static boolean isAppMonitorService() {
		return monitorServiceFlag;
	}
	
	public static void setAppMonitorService(boolean value) {
		monitorServiceFlag = value;
	}
	
	public class MonitorServiceBinder extends Binder{
		
		public AppMonitorService getService(){
			return AppMonitorService.this;
		}
		
	}
}
