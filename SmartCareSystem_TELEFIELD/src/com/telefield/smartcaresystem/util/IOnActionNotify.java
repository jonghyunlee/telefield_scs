package com.telefield.smartcaresystem.util;

public interface IOnActionNotify {
	
	public void ttsStarted(String utteranceId);
	public void ttsDone(String utteranceId);
	public void ttsStop();
	public void makeCallNotify();
	public void endCall();
	public void workCompleted();
	

}
