package com.telefield.smartcaresystem.util;

import org.apache.log4j.Logger;

import android.util.Log;

public class LogMgr{
	
	public static final String TAG = "SmartCareSystem";
	
    public static final boolean DEBUG = true;
    public static boolean SERVERDEBUG = false;
    public static final boolean SYSTEM_PRINT = false; 
    public static final boolean ISLOGGER = true;
    /** type */    
    public static final int CODI_RECEIVE = 1;
    public static final int CODI_SEND = 2;
    public static final int GW_SEND = 3;
    public static final int GW_RECEIVE = 4;
    public static final int EXCEPTION = 5;
    
    public static final String TYPE_STR[] ={   "[                   ]"    										  
    										  ,"[FROM_CODI_RECEIVE  ]"
    										  ,"[TO_CODI_SEND       ]"
    										  ,"[GATEWAY_TO_M2M     ]"
    										  ,"[M2M_TO_GATEWAY     ]"
    										  ,"[EXCEPTION          ]"
    										  };

    

    public static void d(String tag, String msg) {
    	if (DEBUG)
        {
        	if( SYSTEM_PRINT )
        	{
        		System.out.println("[" + tag + "]" + msg);
        	}else
        	{
        		Log.d(tag, msg );
        	}
        }
        
        if( ISLOGGER ) writeHistory(0,"[" + tag + "]" + msg);
    }

    public static void e(String tag, String msg) {
        if (DEBUG)
        {
        	if( SYSTEM_PRINT )
        	{
        		System.out.println("[" + tag + "]" + msg);
        	}else
        	{
        		Log.e(tag, msg);
        	}
        }
        
        if( ISLOGGER ) writeHistory(0,"[" + tag + "]" + msg);
    }

    public static void i(String tag, String msg) {
        if (DEBUG)
        {
        	if( SYSTEM_PRINT )
        	{
        		System.out.println("[" + tag + "]" + msg);
        	}else
        	{
        		Log.i(tag, msg );
        	}
        }
        
        if( ISLOGGER ) writeHistory(0,"[" + tag + "]" + msg);
    }

    public static void v(String tag, String msg) {
        if (DEBUG)
        {
        	if( SYSTEM_PRINT )
        	{
        		System.out.println("[" + tag + "]" + msg);
        	}else
        	{
        		Log.v(tag, msg );
        	}
        }
        
        if( ISLOGGER ) writeHistory(0,"[" + tag + "]" + msg);
    }

    public static void w(String tag, String msg) {
        if (DEBUG)
        {
        	if( SYSTEM_PRINT )
        	{
        		System.out.println("[" + tag + "]" + msg);
        	}else
        	{
        		Log.w(tag, msg);
        	}
        }
        
        if( ISLOGGER ) writeHistory(0,"[" + tag + "]" + msg);
    }
    
   
    public static void writeHistory(int type, String data)
    {
    	Logger logger = Logger.getLogger(TAG);
    	logger.info(TYPE_STR[type]+":"+data);
    }
    
    /*	
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
  	static Socket  socket = null;
    private static String date;
    private static String serverData;
    private static int logcnt = 1;
    private static boolean isexcute = false;
    	if(SERVERDEBUG)
    	{
	    	StringBuffer  sb = new StringBuffer();
	    	sb.append(date);
	    	sb.append(TYPE_STR[type]+":"+data);
	    	serverData = new String(sb);
	    	byte[] debugData = serverData.getBytes();
	    	SmartCareSystemApplication.getInstance().getProtocolManger().sendDebugLog(debugData, debugData.length, true);
    	}
    	if(SERVERDEBUG)
    	{
    		StringBuffer  sb = new StringBuffer();
	    	if(!isexcute){
	    		date = sdf.format(new Date());
		    	sb.append(date);
		    	sb.append(TYPE_STR[type]+":"+data);
		    	serverData = new String(sb);
		    	NetworkTask networkTask = new NetworkTask();
		    	networkTask.execute();
		    	try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}//
	    	}
    	}
    	try {
    		StringBuffer  sb = new StringBuffer();

    		date = sdf.format(new Date());
	    	sb.append(date);
	    	sb.append(TYPE_STR[type]+":"+data);
	    	serverData = new String(sb);
	    	NetworkTask networkTask = new NetworkTask();
	    	networkTask.execute();
        	final HttpEntity entity = MultipartEntityBuilder.create()
        			.addTextBody("call_num","01027150390")
        			.addTextBody("log_msg",serverData)
        			.build();
        	
			HttpUtils.INSTANCE
				.httpUrlPOSTRequest("http://222.122.128.45/admin/menu/log/ajax/receive/adnroid/deb/msg",entity);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	
    
    private static class NetworkTask extends AsyncTask<Void, Void, Void>{
//    	private static NetworkTask networkTask = NetworkTask.getInstance();
    	
		@Override
		protected  Void doInBackground(Void... params) {
			isexcute = true;
			// TODO Auto-generated method stub
			if(SERVERDEBUG *//**&& socket.isConnected()*//*)
        	{
				byte[] serverByteData = serverData.getBytes();
				try {
					final HttpEntity entity = MultipartEntityBuilder.create()
		        			.addTextBody("call_num","01027150390")
		        			.addTextBody("log_msg",serverData)
		        			.build();
		        	
					HttpUtils.INSTANCE
						.httpUrlPOSTRequest("http://222.122.128.45:8080/admin/menu/log/ajax/receive/adnroid/deb/msg",entity);
//					sendMessage(serverByteData);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();//
				}
			}
			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			isexcute = false;
			super.onPostExecute(result);
		}
		
		public boolean send(final byte[] packet_list) throws Exception {	
			SocketChannel client = SocketChannel.open();
			client.connect(new InetSocketAddress("222.122.128.45",18001));
			client.finishConnect();		
			int c = client.write(ByteBuffer.wrap(packet_list));
			Log.e(LogMgr.TAG, c + "logcnt : "+ logcnt++ + "....logData length : " + packet_list.length + "...raw Data : \n" + serverData);	
			
//			Thread.sleep(2000);
//	    		client.shutdownOutput().close();
			client.close();
			
	        return true;
		}
	    	
		public boolean send(List<byte[]> packet_list) throws Exception {	
			SocketChannel client = SocketChannel.open();
			client.connect(new InetSocketAddress("222.122.128.45",18001));
			client.finishConnect();
			
			for(byte[] packet : packet_list) {
				client.write(ByteBuffer.wrap(packet));
				Thread.sleep(2000);
			}
			
//	    		client.shutdownOutput().close();
			client.close();

	        return true;
		}
	    
    }
    public static void sendMessage(byte[] data){
    	SmartCarePreference pref = new SmartCarePreference(SmartCareSystemApplication.getInstance().getApplicationContext(), SmartCarePreference.ADDRESS_INFO_FILENAME);
    	String IP = pref.getStringValue(SmartCarePreference.M2M_IP, SmartCarePreference.DEFAULT_COMMON_STRING);
    	int port = pref.getIntValue(SmartCarePreference.M2M_PORT, SmartCarePreference.DEFAULT_COMMON_INT);
    	InetSocketAddress address = new InetSocketAddress(IP.equals(SmartCarePreference.DEFAULT_COMMON_STRING) ? ProtocolConfig.M2M_ADDRESS : IP,
    			port == SmartCarePreference.DEFAULT_COMMON_INT ? ProtocolConfig.M2M_PORT : port);

		Socket socket = new Socket();
		OutputStream output = null; 
		try {
			socket.setTcpNoDelay(true);
			socket.connect(address, ProtocolConfig.CONNECT_TIMEOUT);
			
			output = socket.getOutputStream();
			if(socket != null && output != null){
				output.write(data, 0, data.length);
				output.flush();
				Log.e(LogMgr.TAG, "logcnt : "+ logcnt++ + "....logData length : " + data.length + "...raw Data : \n" + serverData);
			}
		} catch (SocketException e) {
			// TODO Auto-generated catch block
    		StringWriter sw = new StringWriter();
    		e.printStackTrace(new PrintWriter(sw));
    		String stackTrace = sw.toString();
    		Log.e(LogMgr.TAG, stackTrace);
			e.printStackTrace();
		}catch (IOException e1) {
			// TODO Auto-generated catch block
			StringWriter sw = new StringWriter();
    		e1.printStackTrace(new PrintWriter(sw));
    		String stackTrace = sw.toString();
    		Log.e(LogMgr.TAG, stackTrace);

			e1.printStackTrace();
		}finally{
			try {
				output.close();
				socket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
    }

    public void openSocket(){
			Thread openSocket = new Thread(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					SmartCarePreference pref = new SmartCarePreference(SmartCareSystemApplication.getInstance().getApplicationContext(), SmartCarePreference.ADDRESS_INFO_FILENAME);
			    	String IP = pref.getStringValue(SmartCarePreference.M2M_IP, SmartCarePreference.DEFAULT_COMMON_STRING);
			    	int port = pref.getIntValue(SmartCarePreference.M2M_PORT, SmartCarePreference.DEFAULT_COMMON_INT);
			    	address = new InetSocketAddress(IP.equals(SmartCarePreference.DEFAULT_COMMON_STRING) ? ProtocolConfig.M2M_ADDRESS : IP,
			    			port == SmartCarePreference.DEFAULT_COMMON_INT ? ProtocolConfig.M2M_PORT : port);
			    	Log.d(LogMgr.TAG, "openSocket log....." + address);
		
					socket = new Socket();
					try {
					socket.setTcpNoDelay(true);
					socket.connect(address, ProtocolConfig.CONNECT_TIMEOUT);
		    	}catch(Exception e){
		    		openSocket();
		    		StringWriter sw = new StringWriter();
		    		e.printStackTrace(new PrintWriter(sw));
		    		String stackTrace = sw.toString();
		    		Log.e(LogMgr.TAG, stackTrace);
		    	}
				}
			});
			openSocket.start();
    }
   */

}


