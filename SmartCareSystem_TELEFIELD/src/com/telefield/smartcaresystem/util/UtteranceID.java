package com.telefield.smartcaresystem.util;

import java.util.HashMap;

import android.speech.tts.TextToSpeech;

public class UtteranceID {
	public static HashMap<String, String> TTSUtteranceId(String utteranceType) {
		HashMap<String, String> utteranceId = new HashMap<String, String>();
		utteranceId.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, utteranceType);
		return utteranceId;
	}
}
		