package com.telefield.smartcaresystem.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.telefield.smartcaresystem.ui.DebugUI;

public class Logcat {
	private static final long CAT_DELAY = 1;

	private boolean mRunning = false;
	private BufferedReader mReader = null;
	private Handler mHandler;
	private Process logcatProc;
	private ArrayList<String> mLogCache = new ArrayList<String>();
	private boolean mPlay = true;
	private long lastCat = -1;
	private Runnable catRunner = new Runnable() {

		@Override
		public void run() {
			if (!mPlay) {
				return;
			}
			long now = System.currentTimeMillis();
			if (now < lastCat + CAT_DELAY) {
				return;
			}
			lastCat = now;
			cat();
		}
	};
	private ScheduledExecutorService EX;
	public Format mFormat;

	public Logcat(Context context, Handler handler, Format format) {
		mHandler = handler;
		mFormat = format;
	}

	public void start() {
		stop();

		mRunning = true;

		EX = Executors.newScheduledThreadPool(1);
		EX.scheduleAtFixedRate(catRunner, CAT_DELAY, CAT_DELAY, TimeUnit.SECONDS);

		try {
			Message m = Message.obtain(mHandler, DebugUI.CLEAR_WHAT);
			mHandler.sendMessage(m);

			List<String> progs = new ArrayList<String>();

			progs.add("logcat");
			progs.add("-v");
			progs.add(mFormat.getValue());
			progs.add("-b");
			progs.add("main");
			progs.add("-s");
			progs.add("SmartCareSystem:* System.err:* AndroidRuntime:* j2xx:* ftdi:*");

			logcatProc = Runtime.getRuntime().exec(progs.toArray(new String[0]));

			mReader = new BufferedReader(new InputStreamReader(logcatProc.getInputStream()), 1024);

			String line;
			while (mRunning && (line = mReader.readLine()) != null) {
				if (!mRunning) {
					break;
				}
				if (line.length() == 0) {
					continue;
				}
				synchronized (mLogCache) {
					mLogCache.add(line);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			return;
		} finally {

			if (logcatProc != null) {
				logcatProc.destroy();
				logcatProc = null;
			}
			if (mReader != null) {
				try {
					mReader.close();
					mReader = null;
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void cat() {
		Message m;

		if (mLogCache.size() > 0) {
			synchronized (mLogCache) {
				if (mLogCache.size() > 0) {
					m = Message.obtain(mHandler, DebugUI.CAT_WHAT);
					m.obj = mLogCache.clone();
					mLogCache.clear();
					mHandler.sendMessage(m);
				}
			}
		}
	}

	public void stop() {
		mRunning = false;

		if (EX != null && !EX.isShutdown()) {
			EX.shutdown();
			EX = null;
		}
	}

	public boolean isRunning() {
		return mRunning;
	}

	public boolean isPlay() {
		return mPlay;
	}

	public void setPlay(boolean play) {
		mPlay = play;
	}

	public void setFormat(Format format) {
		mFormat = format;
	}
}
