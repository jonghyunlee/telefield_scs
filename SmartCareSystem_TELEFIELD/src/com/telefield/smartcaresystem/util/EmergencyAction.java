package com.telefield.smartcaresystem.util;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.speech.tts.TextToSpeech;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.telefield.smartcaresystem.Constant;
import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.database.ConnectDB;
import com.telefield.smartcaresystem.database.Sensor;
import com.telefield.smartcaresystem.database.Users;
import com.telefield.smartcaresystem.firmware.UpgradeController;
import com.telefield.smartcaresystem.m2m.GateWayMessage;
import com.telefield.smartcaresystem.m2m.GatewayDevice;
import com.telefield.smartcaresystem.m2m.GatewayIndex;
import com.telefield.smartcaresystem.m2m.ProtocolFrame;
import com.telefield.smartcaresystem.m2m.ProtocolManger;
import com.telefield.smartcaresystem.message.FormattedFrame;
import com.telefield.smartcaresystem.message.MessageID;
import com.telefield.smartcaresystem.message.SensorID;
import com.telefield.smartcaresystem.sensor.SensorDeviceManager;
import com.telefield.smartcaresystem.telephony.CallStateListner;
import com.telefield.smartcaresystem.telephony.PhoneManager;
import com.telefield.smartcaresystem.ui.BlinkActivity;
import com.telefield.smartcaresystem.ui.UIConstant;
import com.telefield.smartcaresystem.ui.serverrequest.TakePicture;

public class EmergencyAction implements IOnActionNotify{
	
	public static final int ACTION_READY = -1;
	public static final int ACTION_TTS_PLAY = 0;
	public static final int ACTION_TTS_DONE = 1;	
	public static final int ACTION_CALL_OUT = 2;
	public static final int ACTION_CALL_END = 3;
	public static final int ACTION_CAM = 4;
	public static final int ACTION_SERVER_REPORT = 5;
	
	private Context context;	

	private int stage;
	private FormattedFrame frame; 
	
	private String actionId;
	private String emergencyType;
	
	Timer mentTimer;
	Timer stopAlertSoundTimer;
	MediaPlayer mediaPlayer;
	
	private boolean previous_call ;
	
	private boolean reserved_power_off_ment; // 통화중 전원 차단되면 통화끝난후에 멘트 발송하도록
		
	public EmergencyAction(Context context)
	{
		this.context = context;		
		
		previous_call = false;
		
		stage = ACTION_READY;
		
		SmartCareSystemApplication.getInstance().getSpeechUtil().setActionNotifier(this);
		CallStateListner.getInstance().addActionNotifier(this);		
		
	}
	
	public void endCallEmergency(String emergencyType){
		actionId = Constant.UTTERANCE_ID_EMG_ENDCALL;
		this.emergencyType = emergencyType;
		
		HashMap<String, String> utteranceId = UtteranceID.TTSUtteranceId(Constant.UTTERANCE_ID_EMG_ENDCALL);
		if(emergencyType.equals(Constant.EMERGENCY)){
			SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.EMERGENCY_ENDCALL, TextToSpeech.QUEUE_FLUSH, utteranceId);
			
		}
		else if(emergencyType.equals(Constant.FIRE)){
			SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.EMERGENCY_FIRE_ENDCALL, TextToSpeech.QUEUE_FLUSH, utteranceId);
			
		}
		else if(emergencyType.equals(Constant.GAS)){
			SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.EMERGENCY_GAS_ENDCALL, TextToSpeech.QUEUE_FLUSH, utteranceId);
		}
	}
	public void Call119(FormattedFrame frame, String emergencyType) {
		
		if( actionId != null)
		{
			if( actionId.equals(Constant.UTTERANCE_ID_119))					
			{
				//지금 현재 응급 상황이 진행중이면
				if( stage != ACTION_READY )
				{
					//멘트는 말하지 않고, 서버에 보고만 하도록 한다.
					//globalmessagehandler 에서 보고하므로 아무것도 안한다.
					LogMgr.d(LogMgr.TAG,"ignore");
					
				}
			}
				
		}
		
		actionId = Constant.UTTERANCE_ID_119;
		this.emergencyType = emergencyType;
		
		if(existTelephoneNumber(Constant.UTTERANCE_ID_119)) {
			//utteranceId
			HashMap<String, String> utteranceId = UtteranceID.TTSUtteranceId(Constant.UTTERANCE_ID_119);			
			if(emergencyType.equals(Constant.EMERGENCY)){
				this.frame =  frame;
				SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.EMERGENCY_CALL_FROM_HOME, TextToSpeech.QUEUE_ADD, utteranceId);
				
			}
			else if(emergencyType.equals(Constant.FIRE)){
				SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.FIRE_CALL_FROM_HOME, TextToSpeech.QUEUE_ADD, utteranceId);
				
			}
			else if(emergencyType.equals(Constant.GAS)){
				SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.GAS_CALL_FROM_HOME, TextToSpeech.QUEUE_ADD, utteranceId);
			}
			else
				LogMgr.d(LogMgr.TAG, "Wrong Emergency Type!");
		}
		else {
			Toast.makeText(context, context.getString(R.string.one_one_nine) + UIConstant.SPACE_CHARACTER + context.getString(R.string.no_telephone) + 
					UIConstant.SPACE_CHARACTER + context.getString(R.string.go_system_setting_and_add_telephone),Toast.LENGTH_SHORT).show();
			SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.NO_119, TextToSpeech.QUEUE_FLUSH, null);
		}
	}
	
	public void CallCenter(){
		
		LogMgr.d(LogMgr.TAG,"CallCenter :" + stage);
		
		if( stage != ACTION_READY ) return;		
		
		actionId = Constant.UTTERANCE_ID_CENTER;
		
		if(existTelephoneNumber(Constant.UTTERANCE_ID_CENTER)) {
			HashMap<String, String> utteranceId = UtteranceID.TTSUtteranceId(Constant.UTTERANCE_ID_CENTER);
			SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.LOCAL_CENTER, TextToSpeech.QUEUE_FLUSH, utteranceId);
		}
		else {
			Toast.makeText(context, context.getString(R.string.local_center) + UIConstant.SPACE_CHARACTER + context.getString(R.string.no_telephone) + 
					UIConstant.SPACE_CHARACTER + context.getString(R.string.go_system_setting_and_add_telephone),Toast.LENGTH_SHORT).show();
			SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.NO_LOCAL_CENTER, TextToSpeech.QUEUE_FLUSH, null);
		}	
		
		
	}
	
	public void CallHelper() {
		
		LogMgr.d(LogMgr.TAG,"CallHelper :" + stage);
		if( stage != ACTION_READY ) return;
		
		actionId = Constant.UTTERANCE_ID_HELPER;
		
		if(existTelephoneNumber(Constant.UTTERANCE_ID_HELPER)) {
			HashMap<String, String> utteranceId = UtteranceID.TTSUtteranceId(Constant.UTTERANCE_ID_HELPER);
			SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.ELDERLY_CARE, TextToSpeech.QUEUE_FLUSH, utteranceId);
		}
		else {
			Toast.makeText(context, context.getString(R.string.helper) + UIConstant.SPACE_CHARACTER + context.getString(R.string.no_telephone) + 
					UIConstant.SPACE_CHARACTER + context.getString(R.string.go_system_setting_and_add_telephone),Toast.LENGTH_SHORT).show();
			SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.NO_ELDERLY_CARE, TextToSpeech.QUEUE_FLUSH, null);
		}
	}

	public void CallCare1() {
		
		LogMgr.d(LogMgr.TAG,"CallCare1 :" + stage);
		
		if( stage != ACTION_READY ) return;
		
		actionId = Constant.UTTERANCE_ID_CARE1;
		
		if(existTelephoneNumber(Constant.UTTERANCE_ID_CARE1)) {
			HashMap<String, String> utteranceId = UtteranceID.TTSUtteranceId(Constant.UTTERANCE_ID_CARE1);
			SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.PROTECTOR_1, TextToSpeech.QUEUE_FLUSH, utteranceId);
		}
		else {
			Toast.makeText(context, context.getString(R.string.care1) + UIConstant.SPACE_CHARACTER + context.getString(R.string.no_telephone) + 
					UIConstant.SPACE_CHARACTER + context.getString(R.string.go_system_setting_and_add_telephone),Toast.LENGTH_SHORT).show();
			SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.NO_PROTECTOR_1, TextToSpeech.QUEUE_FLUSH, null);
		}
	}

	public void CallCare2() {
		
		LogMgr.d(LogMgr.TAG,"CallCare2 :" + stage);
		
		if( stage != ACTION_READY ) return;
		
		actionId = Constant.UTTERANCE_ID_CARE2;
		
		if(existTelephoneNumber(Constant.UTTERANCE_ID_CARE2)) {
			HashMap<String, String> utteranceId = UtteranceID.TTSUtteranceId(Constant.UTTERANCE_ID_CARE2);
			SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.PROTECTOR_2, TextToSpeech.QUEUE_FLUSH, utteranceId);
		}
		else {
			Toast.makeText(context, context.getString(R.string.care2) + UIConstant.SPACE_CHARACTER + context.getString(R.string.no_telephone) + 
					UIConstant.SPACE_CHARACTER + context.getString(R.string.go_system_setting_and_add_telephone),Toast.LENGTH_SHORT).show();
			SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.NO_PROTECTOR_2, TextToSpeech.QUEUE_FLUSH, null);
		}
	}
	
	/**
	 * 화재센서, 가스 센서로보터 취소가 된경우
	 * 콜 종료 하지 않도록.
	 * 서버에 보고는 MessageManager 에서 진행
	 * 
	 */
	public void actionCancel()
	{
		stage = ACTION_READY;
		actionId = null;		
		// 응급 호출기로 응급 상황 해제 메시지 전달
		SmartCareSystemApplication.getInstance().getGlobalMsgFunc().sendEmgAlarmMsg(MessageID.CANCEL_EMG);
		SmartCareSystemApplication.getInstance().getSpeechUtil().stopSpeech();		
	}
	
	public void actionStop(FormattedFrame ff)
	{
		if( actionId != null && actionId.equals(Constant.UTTERANCE_ID_CANCEL) && stage != ACTION_READY) return;	
		stage = ACTION_READY;
		actionId = null;		
		
		LogMgr.d(LogMgr.TAG, "EmergencyAction actionStop FormattedFrame ff.getByte : " + Arrays.toString(ff.getBytes()));
		PhoneManager phoneMgr = SmartCareSystemApplication.getInstance().getPhoneManager();
		SmartCareSystemApplication.getInstance().getSpeechUtil().stopSpeech();
		if( phoneMgr.getPhoneState() != TelephonyManager.CALL_STATE_IDLE )
		{
						
			SmartCareSystemApplication.getInstance().getPhoneManager().endCall();
			
			actionId = Constant.UTTERANCE_ID_CANCEL; 		
			HashMap<String, String> utteranceId = UtteranceID.TTSUtteranceId(Constant.UTTERANCE_ID_CANCEL);			
			SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.SPEECH_CALL_END, TextToSpeech.QUEUE_ADD, utteranceId);
			
		}
		/**
		 * Jong 2014.10.31
		 * 취소버튼을 누르면 개통 메뉴로 이동하는 소스
		 */
		/*else if (SmartCareSystemApplication.getInstance().getProtocolManger().isRunningOpenning()) {

			Intent intent = new Intent("android.intent.ACTION.CANCEL_OPENNING");
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP); //AVAD_yhan 20141104 실행 오류 수정
			context.startActivity(intent);
		}*/
		else
		{

			StringBuffer preMent = new StringBuffer("");
			
			boolean isAlertingFire = SmartCareSystemApplication.getInstance().getMySmartCareState().getAlertFire();
			boolean isAlertingGas = SmartCareSystemApplication.getInstance().getMySmartCareState().getAlertGas();
			
			if( isAlertingFire || isAlertingGas)
			{			
				if( isAlertingFire ) {
					preMent.append("화재 ");
				}
				
				if( isAlertingGas ) {
					preMent.append("가스 ");
				}
				
				preMent.append("경보가 ");
			}
			
			actionId = Constant.UTTERANCE_ID_CANCEL; 		
			HashMap<String, String> utteranceId = UtteranceID.TTSUtteranceId(Constant.UTTERANCE_ID_CANCEL);
			SmartCareSystemApplication.getInstance().getSpeechUtil().speech(preMent.toString() + SpeechUtil.CALL_CANCEL, TextToSpeech.QUEUE_FLUSH, utteranceId);
			
			// 응급 호출기로 응급 상황 해제 메시지 전달
			SmartCareSystemApplication.getInstance().getGlobalMsgFunc().sendEmgAlarmMsg(ff);
		}
		
	}
	
	public void actionStop(){
		
		if( actionId != null && actionId.equals(Constant.UTTERANCE_ID_CANCEL) && stage != ACTION_READY) return;	
		stage = ACTION_READY;
		actionId = null;
		
		PhoneManager phoneMgr = SmartCareSystemApplication.getInstance().getPhoneManager();
		SmartCareSystemApplication.getInstance().getSpeechUtil().stopSpeech();		
		
		StringBuffer preMent = new StringBuffer("");
		
		boolean isAlertingFire = SmartCareSystemApplication.getInstance().getMySmartCareState().getAlertFire();
		boolean isAlertingGas = SmartCareSystemApplication.getInstance().getMySmartCareState().getAlertGas();
		boolean isAlerting119 = SmartCareSystemApplication.getInstance().getMySmartCareState().getAlert119();
		if( isAlertingFire || isAlertingGas)
		{			
			if( isAlertingFire ) {
				preMent.append("화재 ");
			}
			
			if( isAlertingGas ) {
				preMent.append("가스 ");
			}
			
			preMent.append("경보가 ");
		}
		
		actionId = Constant.UTTERANCE_ID_CANCEL; 		
		HashMap<String, String> utteranceId = UtteranceID.TTSUtteranceId(Constant.UTTERANCE_ID_CANCEL);
		SmartCareSystemApplication.getInstance().getSpeechUtil().speech(preMent.toString() + SpeechUtil.CALL_CANCEL, TextToSpeech.QUEUE_FLUSH, utteranceId);
		
		SmartCareSystemApplication.getInstance().getGlobalMsgFunc().sendEmgAlarmMsg((byte)0x01);

	}
	
	public void actionCompleted()
	{
		stage = ACTION_READY;
		actionId = null;
	}
	
	public int getActionStage()
	{
		return stage;
	}
	
	private boolean existTelephoneNumber(String userType) 
	{
		ConnectDB con = ConnectDB.getInstance(context);
		
		Users user = new Users();
		user.setType(userType);
		List<Users> result = con.selectUser(user);
		
		if(result.size() > 0) {
			for(Users users: result){
				// <-- 시스템에서 번호정보를 넣지않아도 전화가 가는 문제점 해결  2015.02.16
				if(users.getNumber() == null || users.getNumber().equals("-") || users.getNumber().equals(" "))
					return false;
			}// -->
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public void ttsStarted(String utteranceId) {
		
		LogMgr.d(LogMgr.TAG,"ttsStarted: Action ID " + actionId + " utteranceId : " + utteranceId) ;
		
		switch(utteranceId)
		{

			case Constant.UTTERANCE_ID_119:
			case Constant.UTTERANCE_ID_CENTER:
			case Constant.UTTERANCE_ID_CARE1:
			case Constant.UTTERANCE_ID_CARE2:
			case Constant.UTTERANCE_ID_HELPER:
			case Constant.UTTERANCE_ID_CANCEL:
			case Constant.UTTERANCE_ID_EMG_ENDCALL:
				stage = ACTION_TTS_PLAY;				
				break;			
			case Constant.UTTERANCE_ID_EMERGENCY_MENT:
				SmartCareSystemApplication.getInstance().getGlobalMsgFunc().sendGateWayMsg((byte)MessageID.SPEECH_MIC_PATH);
				break;
			case Constant.UTTERANCE_ID_NEGLECT_TELEPHONE:
				actionId = Constant.UTTERANCE_ID_NEGLECT_TELEPHONE;
				break;
		}
	}

	@Override
	public void ttsDone(String utteranceId) {
		LogMgr.d(LogMgr.TAG,"ttsDone: Action ID " + actionId + " utteranceId : " + utteranceId) ;
		
		//emergency ment 의 경우 action id 가  null이어서 조건을 올림.
		if(utteranceId.equals(Constant.UTTERANCE_ID_EMERGENCY_MENT) || utteranceId.equals(Constant.UTTERANCE_ID_EMG_ENDCALL))
		{
			SmartCareSystemApplication.getInstance().getGlobalMsgFunc().sendGateWayMsg((byte)MessageID.SPEECH_MIC_PATH_CANCEL);
		}
		
		if(!utteranceId.equals("119")){
			if( actionId == null || !actionId.equals(utteranceId)) return;
		}
		
		switch(utteranceId)
		{
			case Constant.UTTERANCE_ID_119:
			case Constant.UTTERANCE_ID_CENTER:
			case Constant.UTTERANCE_ID_CARE1:
			case Constant.UTTERANCE_ID_CARE2:
			case Constant.UTTERANCE_ID_HELPER:
				
				stage = ACTION_TTS_DONE;
				actionCall(actionId);
				break;		
			case Constant.UTTERANCE_ID_CANCEL:
				
				if( SmartCareSystemApplication.getInstance().getMySmartCareState().getAlertFire() ||
						SmartCareSystemApplication.getInstance().getMySmartCareState().getAlertGas() )					
				{
				
					ProtocolFrame ff = ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_SENSOR_EVENT, GatewayDevice.getBySensor(), GateWayMessage.DATA_CANCEL_ALERT);		
					SmartCareSystemApplication.getInstance().getProtocolManger().insertFrame(ff);
					
					
					SmartCareSystemApplication.getInstance().getGlobalMsgFunc().sendFireClrMsg();
					SmartCareSystemApplication.getInstance().getGlobalMsgFunc().sendGasClrMsg();
					
					SmartCareSystemApplication.getInstance().getProtocolManger().messageSend(null);		
					
				}
				/**
				 * 2015 03 10 가스, 화재, 응급 상황일떄만 취소메시지가 가도록 변경
				 */
				if( SmartCareSystemApplication.getInstance().getMySmartCareState().getAlertFire() ||
						SmartCareSystemApplication.getInstance().getMySmartCareState().getAlertGas() ||
						SmartCareSystemApplication.getInstance().getMySmartCareState().getAlert119()){		
					SmartCareSystemApplication.getInstance().getMySmartCareState().setAlert119(false);
					ProtocolFrame pframe = ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_GW_EVENT, FormattedFrame.getInstance((byte)MessageID.EMG_KEY_MSG, new byte[]{0x01}));
					SmartCareSystemApplication.getInstance().getProtocolManger().insertFrame(pframe);
					
					SmartCareSystemApplication.getInstance().getProtocolManger().messageSend(null);		
				}
				
				workCompleted();
				
				break;
			case Constant.UTTERANCE_ID_NEGLECT_TELEPHONE:
				if(mediaPlayer == null) {
					mediaPlayer = MediaPlayer.create(context, R.raw.alert_sound);
					mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
					mediaPlayer.setLooping(true);
				}
				
				if(!mediaPlayer.isPlaying()) {
					mediaPlayer.start();
					
					if(stopAlertSoundTimer != null) {
						stopAlertSoundTimer.cancel();
						stopAlertSoundTimer = null;
					}
					
					stopAlertSoundTimer = new Timer();
					stopAlertSoundTimer.schedule(new TimerTask() {
						@Override
						public void run() {
							// TODO Auto-generated method stub
							if(mediaPlayer.isPlaying()) {
								mediaPlayer.stop();
								mediaPlayer.release();
								mediaPlayer = null;
							}
						}
					}, 18 * 1000);	//멘트를 말하는 데 걸리는 시간이 약 12초 정도.
				}
					
				break;
			case Constant.UTTERANCE_ID_EMG_ENDCALL:
				//AVAD_yhan 20141022 통화중 경보 발생시 통화 끊고 멘트 발송하도록 수정 --[[
				PhoneManager phoneMgr = SmartCareSystemApplication.getInstance().getPhoneManager();
				
				if( phoneMgr.getPhoneState() == TelephonyManager.CALL_STATE_OFFHOOK)
				{
					LogMgr.d(LogMgr.TAG, "MessageManager phoneMgr.endCall();");
					phoneMgr.endCall();
				}
				//--]]	
				
				break;
		}
	}

	@Override
	public void makeCallNotify() {
		
		if( actionId == null) return;
		// TODO Auto-generated method stub
		stage = ACTION_CALL_OUT;		
		
		LogMgr.d(LogMgr.TAG,"makeCallNotify ::" + actionId  + "Emergency Type :" + emergencyType);
		
		ProtocolManger pmanager = SmartCareSystemApplication.getInstance().getProtocolManger();
		
		switch(actionId)
		{
			case Constant.UTTERANCE_ID_119:
				
				mentTimer = new Timer();				
				mentTimer.schedule(new TimerTask() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						HashMap<String, String> utteranceId = UtteranceID.TTSUtteranceId(Constant.UTTERANCE_ID_EMERGENCY_MENT);	
						SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.EMERGENCY_CALL_AT_FIRE_STATION, TextToSpeech.QUEUE_FLUSH, utteranceId);		
						
						
					}
				}, 5000);
				
				if(emergencyType.equals(Constant.EMERGENCY))
				{
					if( frame == null)
					{									
						ProtocolFrame pframe = ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_GW_EVENT, FormattedFrame.getInstance((byte)MessageID.EMG_KEY_MSG, new byte[]{0x10}));
						pmanager.insertFrame(pframe);
					}
					else{
						
						int sensorId = frame.getSensor_id();
						
						
						if( SensorID.getSensorType(sensorId) == SensorID.CALL_EMERGENCY_SENSOR_TYPE.getSensorType())
						{
							SensorDeviceManager mgr = SmartCareSystemApplication.getInstance()
									.getSensorDeviceManager();
							
							Sensor sensor = mgr.getSensor(sensorId);
							//응급호출장비로 emergency 가 호출이 된경우
							SmartCareSystemApplication.getInstance().getProtocolManger()
							.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_SENSOR_EVENT, sensor, (byte) GateWayMessage.DATA_EMERGENCY_ALERT));
							SmartCareSystemApplication.getInstance().getProtocolManger().messageSend(null);
							
						}else
						{						
							pmanager.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_GW_EVENT, frame));
						}
					}
					pmanager.messageSend(null);
				}
				else if(emergencyType.equals(Constant.FIRE)){
					//화재 발생시에도 가스차단메시지 전달
//					SmartCareSystemApplication.getInstance().getGlobalMsgFunc().sendGasBreakMsg();
//					pmanager.emergencyMessageSend();
				}
				else if(emergencyType.equals(Constant.GAS)){
//					SmartCareSystemApplication.getInstance().getGlobalMsgFunc().sendGasBreakMsg();
//					pmanager.emergencyMessageSend();
				}
				else
					LogMgr.d(LogMgr.TAG, "Wrong Emergency Type!");
				
				//5초후에 응급 멘트 전송....
				
				break;
			case Constant.UTTERANCE_ID_CENTER:
				pmanager.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_GW_EVENT, FormattedFrame.getInstance((byte)MessageID.EMG_KEY_MSG, new byte[]{0x20})));
				pmanager.messageSend(null);
				break;
				
		
		}
	}

	@Override
	public void workCompleted() {
		// TODO Auto-generated method stub
		stage = ACTION_READY;
		actionId = null;
		
		//update 진행할려고 하는 내역이 있으면 업데이트를 진행시켜주자..
		if ( UpgradeController.getInstance().getState() == UpgradeController.CONTROLLER_RESERVED_STATE )
		{
			UpgradeController.getInstance().updrage();
		}
		
	}

	@Override
	public void ttsStop() {
		// TODO Auto-generated method stub	
		
	}
	
	@Override
	public void endCall() {
		
		if( reserved_power_off_ment )
		{
			reserved_power_off_ment = false;
			
			// 전원 차단
			SmartCareSystemApplication
			.getInstance()
			.getSpeechUtil()
			.speech(SpeechUtil.SPEECH_CODI_POWER_OFF,
					TextToSpeech.QUEUE_FLUSH, null);
			
		}
		
		//update 진행할려고 하는 내역이 있으면 업데이트를 진행시켜주자..
		if ( UpgradeController.getInstance().getState() == UpgradeController.CONTROLLER_RESERVED_STATE )
		{
			UpgradeController.getInstance().updrage();
		}
		
		if( actionId == null) return;
		
		if( previous_call == true) {
			LogMgr.d(LogMgr.TAG,"previous call is true");
			return;
		}
		
		// TODO Auto-generated method stub
		stage = ACTION_CALL_END;	
		
		if( mentTimer !=null ) {
			mentTimer.cancel();
			mentTimer = null;
		}
		
		switch(actionId)
		{
			case Constant.UTTERANCE_ID_119:
				// 사진 촬영 시작
				SmartCarePreference pref = new SmartCarePreference(context, SmartCarePreference.APPLICATION_SETTING_INFO_FILENAME);

				if (pref.getStringValue(SmartCarePreference.OPENNING_RESULT, null) != null) {
					Intent picture = new Intent(SmartCareSystemApplication.getInstance().getApplicationContext(), TakePicture.class);
					picture.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					SmartCareSystemApplication.getInstance().getApplicationContext().startActivity(picture);
				}
				break;
				
			case Constant.UTTERANCE_ID_CENTER:
			case Constant.UTTERANCE_ID_CARE1:
			case Constant.UTTERANCE_ID_CARE2:
			case Constant.UTTERANCE_ID_HELPER:								
			break;				
		}		
		
		workCompleted();		
	}
	
	public void actionCall(String actionId)
	{
		PhoneManager phoneMgr = SmartCareSystemApplication.getInstance().getPhoneManager();
		
		if(actionId.equals(Constant.UTTERANCE_ID_119)) {	
			//통화중이면 전화를 강제로 끊는다. 현제 걸린전화가 119 인지 확인
//			previous_call = true;
//			phoneMgr.endCall();
//			
//			try {
//				Thread.sleep(500);
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			
//			previous_call = false;			
			MakeCall(actionId);
		} 

		else if (actionId.equals(Constant.UTTERANCE_ID_CENTER)) {
			if( phoneMgr.getPhoneState() == TelephonyManager.CALL_STATE_IDLE)							
				MakeCall(actionId);

		} else if (actionId.equals(Constant.UTTERANCE_ID_HELPER)) {
			if( phoneMgr.getPhoneState() == TelephonyManager.CALL_STATE_IDLE)
				MakeCall(actionId);

		} else if (actionId.equals(Constant.UTTERANCE_ID_CARE1)) {
			if( phoneMgr.getPhoneState() == TelephonyManager.CALL_STATE_IDLE)
				MakeCall(actionId);

		} else if (actionId.equals(Constant.UTTERANCE_ID_CARE2)) {
			if( phoneMgr.getPhoneState() == TelephonyManager.CALL_STATE_IDLE)
				MakeCall(actionId);
		}
	}
	
	private void MakeCall(String helperType) {
		Intent intent = new Intent(context, BlinkActivity.class);
		intent.putExtra(UIConstant.UTTERANCEID, helperType);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(intent);
	}
	
	//방치 사운드(멘트 포함)가 들릴 때, 수화기를 내려 놓으면, 사운드가 멈춰야 한다. (전화가 올 때도 포함)
	public void stopNeglectSound() {
		if(actionId == Constant.UTTERANCE_ID_NEGLECT_TELEPHONE && SmartCareSystemApplication.getInstance().getSpeechUtil().getTTS().isSpeaking()) {
			actionId = Constant.UTTERANCE_ID_CANCEL;
			HashMap<String, String> utteranceId = UtteranceID.TTSUtteranceId(Constant.UTTERANCE_ID_CANCEL);
			SmartCareSystemApplication.getInstance().getSpeechUtil().speech("", TextToSpeech.QUEUE_FLUSH, utteranceId);
			return;	//Cancel의 경우 ttsDone을 타지 않으므로, stopAlertSoundTimer와 mediaPlayer을 null 처리 할 필요가 없다.
		}
		
		if(stopAlertSoundTimer != null) {
			stopAlertSoundTimer.cancel();
			stopAlertSoundTimer = null;
		}
		
		if(mediaPlayer != null && mediaPlayer.isPlaying()) {
			mediaPlayer.stop();
			mediaPlayer.release();
			mediaPlayer = null;
		}
	}
	
	public void setReservePowerOffMent(boolean set)
	{
		reserved_power_off_ment = set;
	}
}
