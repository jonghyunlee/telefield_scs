package com.telefield.smartcaresystem.util;

import com.telefield.smartcaresystem.m2m.GatewayIndex;

public class CheckSum {
	
	public static byte getValue(byte data[], int length)
	{
		byte checkSum = 0;
		
//		LogMgr.d(LogMgr.TAG, "CheckSum Data : " + ByteArrayToHex.byteArrayToHex(data,length));
		
		for( int i=0 ; i < length; i++)
		{
			checkSum = (byte) (checkSum + data[i]);
		}
		
		return checkSum;
	}
	
	public static byte getProtocolCheckSum(byte data[], int length){
		byte checkSum = 0;
		for (int i = GatewayIndex.PACKET_LENGTH; i < data.length - 2; i++) {
			checkSum = (byte)(checkSum + data[i]);
		}
		return (byte) (checkSum & 0xff);
	}
	
	public static byte getCamDataCRC(byte data[]){
		byte checkCRC = 0;
		for (int i = 0; i < data.length; i++) {
			checkCRC = (byte)(checkCRC + data[i]);
		}
		return (byte) (checkCRC & 0xff);
	}

}
