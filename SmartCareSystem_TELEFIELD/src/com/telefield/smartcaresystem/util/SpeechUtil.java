package com.telefield.smartcaresystem.util;

import java.util.HashMap;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.speech.tts.UtteranceProgressListener;
import android.telephony.TelephonyManager;

import com.telefield.smartcaresystem.Constant;
import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.message.MessageID;
import com.telefield.smartcaresystem.message.SensorID;
import com.telefield.smartcaresystem.telephony.PhoneManager;

/**
 * TTS 엔진을 사용하여 data를 읽어주는 기능
 * @author Administrator
 *
 */
public class SpeechUtil {
	TextToSpeech tts;
	Context mContext;
	String actionId;
	
	IOnActionNotify actionNotifier;
	
	Timer speakTestTimer;
	private final long WAIT_TIME = 3 * 1000;	//TTS 수행 후 기다리는 시간
	
	public SpeechUtil(Context mContext) {
		this.mContext = mContext;
	}
	
	public void init() {
		tts = new TextToSpeech(mContext,new OnInitListener() {
			@Override
			public void onInit(int status) {
				// TODO Auto-generated method stub
				tts.setLanguage(Locale.KOREA);
				if(status == TextToSpeech.SUCCESS){
					LogMgr.d(LogMgr.TAG, "SpeechUtil TTS init success");
				}else{
					LogMgr.d(LogMgr.TAG, "SpeechUtil TTS init fail....");
				}
			}
		});

		tts.setOnUtteranceProgressListener(new UtteranceProgressListener() {			
			
			@Override
			public void onStart(String utteranceId) {
				LogMgr.d(LogMgr.TAG,"setOnUtteranceProgressListener onStart : " + utteranceId);
				
				//speak이 되면 정상동작하므로 Timer를 취소한 뒤, 종료시킨다.
				if(speakTestTimer != null) 
				{
					speakTestTimer.cancel();
					speakTestTimer = null;
				}
				
				actionId = utteranceId;
			
				if( actionNotifier != null)
				{
					actionNotifier.ttsStarted(utteranceId);
				}
				
				if( !utteranceId .equals(Constant.UTTERANCE_ID_EMERGENCY_MENT))
				{
					SmartCareSystemApplication.getInstance().getGlobalMsgFunc().sendGateWayMsg((byte)MessageID.TTS_START_INFO);
				}
			}

			@Override
			public void onDone(String utteranceId) {
				
				actionId = null;
				
				LogMgr.d(LogMgr.TAG,"SpeechUtil onDone  " + utteranceId);
				
				if( actionNotifier != null)
				{
					actionNotifier.ttsDone(utteranceId);
				}
				
				if( !utteranceId .equals(Constant.UTTERANCE_ID_EMERGENCY_MENT))
				{
					SmartCareSystemApplication.getInstance().getGlobalMsgFunc().sendGateWayMsg((byte)MessageID.TTS_STOP_INFO);
				}
	
			}

			@Override
			public void onError(String utteranceId) {
				// TODO Auto-generated method stub
				actionId = null;
				
				LogMgr.d(LogMgr.TAG,"SpeechUtil onError  ");
			}
			
		});
	}
	
	public TextToSpeech getTTS() {
		return tts;
	}
	
	public void setActionNotifier(IOnActionNotify notifier)
	{
		this.actionNotifier = notifier;
	}
	
	public void doTimer() {
		if(speakTestTimer == null) {
			speakTestTimer = new Timer();
			speakTestTimer.schedule(new TimerTask() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					LogMgr.d(LogMgr.TAG,"Remake TTS Instance");
					
					//TTS speak에 문제가 발생한 경우
					if(tts != null) {
						destroyTTS();	//shutdown 처리가 있는 함수를 먼저 수행
						tts =  null;
						init();	//TTS 인스턴스 재생성
					}
				}
			}, WAIT_TIME);
		}
	}
	
	public void speech(String tellMsg, int queueMode, HashMap<String, String> utteranceId) {
		//현재, 응급 멘트가 발송중이면 해당 멘트는 말하지 않는다.	
		String limit_call = SpeechUtil.END_CALL;
		String emg_ment =  ".*상황실로 연결됩니다..*";
		if( isPlayingEmergencyAction(actionId))
		{
			LogMgr.d(LogMgr.TAG,"ment igore. because of playing " + actionId + "ment : " + tellMsg);
		//통화중 응급상황 발생 멘트는 출력
		}
		else if(isOffHookStatus() 
				&& !tellMsg.equals(limit_call) 
				&& !tellMsg.equals(SpeechUtil.EMERGENCY_STATUS) 
				&& !tellMsg.matches(SpeechUtil.EMERGENCY_CALL_AT_FIRE_STATION) 
				&& !tellMsg.matches(emg_ment)
				&& !tellMsg.equals(SpeechUtil.EMERGENCY_ENDCALL)
				&& !tellMsg.equals(SpeechUtil.EMERGENCY_FIRE_ENDCALL)
				&& !tellMsg.equals(SpeechUtil.EMERGENCY_GAS_ENDCALL))
		{
			//통화중에는 멘트 발송하지 않는다.
			LogMgr.d(LogMgr.TAG,"ment igore. because of offhook status " + actionId + "ment : " + tellMsg);
		}
		else
		{			
			
			doTimer();
			
			if( utteranceId != null )
			{
				tts.speak(tellMsg, queueMode, utteranceId);
			}else
			{
				HashMap<String, String> temp_utteranceId = UtteranceID.TTSUtteranceId(Constant.UTTERANCE_ID_NULL);
				tts.speak(tellMsg, queueMode, temp_utteranceId);
			}
			LogMgr.d(LogMgr.TAG,"speech ment : " + tellMsg);
		}
	}
	
	public void speech(int mode, int sensor_id, int queueMode, HashMap<String, String> utteranceId) {
		//현재, 응급 멘트가 발송중이면 해당 멘트는 말하지 않는다.	
		if( isPlayingEmergencyAction(actionId))
		{
			LogMgr.d(LogMgr.TAG,"ment igore. because of playing " + actionId);
		}
		else if(isOffHookStatus())
		{
			//통화중에는 멘트 발송하지 않는다.
			LogMgr.d(LogMgr.TAG,"ment igore. because of offhook status " + actionId + "ment : ");
		}
		else{
			doTimer();
			
			String tellMsg = getSpeechStringData(sensor_id,mode);
			if( utteranceId != null)
			{
				tts.speak(tellMsg, queueMode, utteranceId);
			}else
			{
				HashMap<String, String> temp_utteranceId = UtteranceID.TTSUtteranceId(Constant.UTTERANCE_ID_NULL);
				tts.speak(tellMsg, queueMode, temp_utteranceId);
			}
			LogMgr.d(LogMgr.TAG,"speech ment : " + tellMsg);
		}
	}
	
	//센서 LINK_TEST_MSG 전용 Speech
	public void speech(int mode, int sensor_id, int rssi, int batt_val) {
		if( isPlayingEmergencyAction(actionId))
		{
			LogMgr.d(LogMgr.TAG,"ment igore. because of playing " + actionId);
		}
		else if(isOffHookStatus())
		{
			//통화중에는 멘트 발송하지 않는다.
			LogMgr.d(LogMgr.TAG,"ment igore. because of offhook status " + actionId + "ment : ");
		}
		else{
			doTimer();
			
			String tellMsg = getSpeechStringData(sensor_id,mode);
			
			HashMap<String, String> temp_utteranceId = UtteranceID.TTSUtteranceId(Constant.UTTERANCE_ID_NULL);
			tts.speak(tellMsg + "감도는  " + rssi + " 이며," + "배터리 잔여량은  " + batt_val + " 프로 입니다.", TextToSpeech.QUEUE_FLUSH, temp_utteranceId);
			LogMgr.d(LogMgr.TAG,"speech ment : " + tellMsg);
		}
	}
	
	public void destroyTTS() {
		tts.stop();
		tts.shutdown();
	}
	
	public void stopSpeech() {
		tts.stop();
		try {
			//멘트가 정상적으로 종료가 되도록 100ms 대기한다.
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
	
	public static String getSpeechStringData(int sensor_id, int mode)
	{
		StringBuffer speech = new StringBuffer("");
		
		//센서 type(name)
		speech.append(SensorID.getSensorName(sensor_id));
		
		//센서 index ( 일번, 이번 ....)		
		speech.append(getSpeechStringNumber(SensorID.getSensorIndex(sensor_id)));
		
		//등록되었습니다. 삭제되었습니다...
		if(mode == SENSOR_REGISTER_MODE)
		{
			speech.append(" " + SPEECH_REGISTERED );
		}else if( mode == SENSOR_DELETE_MODE)
		{
			speech.append(" " + SPEECH_DELETEED );
		}else if( mode == SENSOR_CHECK_MODE)
		{
			speech.append(" " + SPEECH_CHECK );			
		}else if( mode == SENSOR_POWER_ON_MDOE)
		{
			speech.append(" " + SPEECH_SENSOR_PWR_ON );
		}else if( mode == SENSOR_POWER_OFF_MDOE)
		{
			speech.append(" " + SPEECH_SENSOR_PWR_OFF );
		}else if( mode == SENSOR_REGISTER_FAIL_MODE )
		{
			speech.append(" " + SPEECH_REGISTER_FAILED );
		}else if( mode == SENSOR_DELETE_FAIL_MODE )
		{
			speech.append(" " + SPEECH_DELETE_FAILED );
		}
		else if( mode == CARE_COME_FAIL_MODE )
		{
			speech.append(" " + VISIT + " " + SPEECH_REGISTER_FAILED );
		}
		else if( mode == CARE_OUT_FAIL_MODE )
		{
			speech.append(" " + OUT + " " + SPEECH_REGISTER_FAILED );
		}
		else if( mode == CARE_OUT_SUCCESS_MODE )
		{
			speech.append(" " + OUT + " " + SPEECH_REGISTERED );
		}
		else if( mode == CARE_COME_SUCCESS_MODE )
		{
			speech.append(" " + VISIT + " " + SPEECH_REGISTERED );
		}
		else if( mode == ACTIVITY_SENSOR_LED_ON )
		{
			speech.replace(speech.length()-1, speech.length(), "");	//'이'라는 글자 삭제
			speech.append(" " + SPEECH_ACTIVITY_SENSOR_LED_ON );
		}
		else if( mode == ACTIVITY_SENSOR_LED_OFF )
		{
			speech.replace(speech.length()-1, speech.length(), "");	//'이'라는 글자 삭제
			speech.append(" " + SPEECH_ACTIVITY_SENSOR_LED_OFF );
		}
		
		return speech.toString();
	}
	
	public static String getSpeechStringNumber(int index)
	{
		StringBuffer number = new StringBuffer("");
		
		
		if( index > 0xff ) return "잘못된 번호";		
		
		int two_digit = index /100;
		int one_digit = (index - (two_digit * 100))/10;
		int zero_digit = index - (two_digit * 100 + one_digit * 10);
		
		switch(two_digit)
		{
			case 1:
				number.append("백");				
				break;
			case 2:
				number.append("이백");				
				break;
			case 3:
				number.append("삼백");				
				break;
			case 4:
				number.append("사백");
				
				break;
			case 5:
				number.append("오백");				
				break;
			case 6:
				number.append("육백");				
				break;
			case 7:
				number.append("칠백");				
				break;
			case 8:
				number.append("팔백");				
				break;
			case 9:
				number.append("구백");				
				break;
		}
		
		switch(one_digit)
		{
		case 1:
			number.append("십");
			break;
		case 2:
			number.append("이십");
			break;
		case 3:
			number.append("삼십");
			break;
		case 4:
			number.append("사십");
			break;
		case 5:
			number.append("오십");
			break;
		case 6:
			number.append("육십");
			break;
		case 7:
			number.append("칠십");
			break;
		case 8:
			number.append("팔십");
			break;
		case 9:
			number.append("구십");
			break;
		}
		
		switch(zero_digit)
		{
		case 0:
			number.append("번이");
			break;
		case 1:
			number.append("일번이");
			break;
		case 2:
			number.append("이번이");
			break;
		case 3:
			number.append("삼번이");
			break;
		case 4:
			number.append("사번이");
			break;
		case 5:
			number.append("오번이");
			break;
		case 6:
			number.append("육번이");
			break;
		case 7:
			number.append("칠번이");
			break;
		case 8:
			number.append("팔번이");
			break;
		case 9:
			number.append("구번이");
			break;
		}
		
		
		return number.toString();
	}
	
	public boolean isPlayingEmergencyAction(String id)
	{
		if(id == null ) return false;
		
		if( id.equals(Constant.UTTERANCE_ID_119) 
				|| id.equals(Constant.UTTERANCE_ID_EMG_ENDCALL) 
				|| id.equals(Constant.UTTERANCE_ID_EMERGENCY_MENT)) return true;
		
		return false;
	}
	
	public boolean isOffHookStatus()
	{
		//AVAD_yhan 20141016 통화중에는 멘트 발송하지 않도록.(응급멘트 제외)			
		PhoneManager phoneMgr = SmartCareSystemApplication.getInstance().getPhoneManager();
		if( phoneMgr.getPhoneState() == TelephonyManager.CALL_STATE_OFFHOOK)
		{
			if(actionId != null && !actionId.equals(Constant.UTTERANCE_ID_119)) {
				LogMgr.d(LogMgr.TAG,"ment igore. 통화중" );
				return true;
			}
			else if(actionId == null) {
				LogMgr.d(LogMgr.TAG,"ment igore. 통화중" );
				return true;
			}
		}
		
		return false;
	}
	
	public final static int SENSOR_REGISTER_MODE = 0;
	public final static int SENSOR_DELETE_MODE = 1;
	public final static int SENSOR_CHECK_MODE = 2;
	public final static int SENSOR_POWER_ON_MDOE = 3;
	public final static int SENSOR_POWER_OFF_MDOE = 4;
	public final static int SENSOR_REGISTER_FAIL_MODE = 5;
	public final static int SENSOR_DELETE_FAIL_MODE = 6;
	public final static int CARE_COME_SUCCESS_MODE = 7;
	public final static int CARE_COME_FAIL_MODE = 8;
	public final static int CARE_OUT_SUCCESS_MODE = 9;
	public final static int CARE_OUT_FAIL_MODE = 10;
	public final static int ACTIVITY_SENSOR_LED_ON = 11;
	public final static int ACTIVITY_SENSOR_LED_OFF = 12;
	
	//소방멘트
	public final static String FIRE_CALL_FROM_HOME = "화재경보가 발생하였습니다.소방방재청으로 연결됩니다. 경보상황이 아니라면 취소 버튼을 눌러주십시요. "
			+ "화재경보가 발생하였습니다.소방방재청으로 연결됩니다.경보상황이 아니라면 취소 버튼을 눌러주십시오. "
			+ "화재경보가 발생하였습니다.소방방재청으로 연결됩니다. 경보상황이 아니라면 취소 버튼을 눌러주십시오. "
			+ "소방 방재청으로 연결됩니다.";
	
	public final static String  FIRE_CALL_AT_FIRE_STATION = "응급안전돌보미 댁내 시스템에서 알려드립니다.화재경보가 발생하였습니다.즉시 출동하여 주시기 바랍니다."
			+ "응급안전돌보미 댁내 시스템에서 알려드립니다.화재경보가 발생하였습니다.즉시 출동하여 주시기 바랍니다."
			+ "응급안전돌보미 댁내 시스템에서 알려드립니다.화재경보가 발생하였습니다.즉시 출동하여 주시기 바랍니다.";
	
	public final static String GAS_CALL_FROM_HOME = "가스경보가 발생하였습니다.소방방재청으로 연결됩니다. 경보상황이 아니라면 취소 버튼을 눌러주십시요. "
			+ "가스경보가 발생하였습니다.소방방재청으로 연결됩니다. 경보상황이 아니라면 취소 버튼을 눌러주십시오."
			+ "가스경보가 발생하였습니다.소방방재청으로 연결됩니다. 경보상황이 아니라면 취소 버튼을 눌러주십시오."
			+ "소방방재청으로 연결됩니다.";
	public final static String GAS_CALL_AT_FIRE_STATION = "응급안전돌보미 댁내 시스템에서 알려드립니다.가스경보가 발생하였습니다.즉시 출동하여 주시기 바랍니다. ";
	public final static String EMERGENCY_CALL_FROM_HOME = "응급호출이 발생하였습니다. 일일구 상황실로 연결됩니다.          경보상황이 아니라면 취소 버튼을 눌러주십시오. "
			+ "응급호출이 발생하였습니다. 일일구 상황실로 연결됩니다.         경보상황이 아니라면 취소 버튼을 눌러주십시오. "
			+ "응급호출이 발생하였습니다 일일구 상황실로 연결됩니다.          경보상황이 아니라면 취소 버튼을 눌러주십시오. "
			+ "일일구 상황실로 연결됩니다. "
			;
	public final static String EMERGENCY_CALL_AT_FIRE_STATION = "응급안전돌보미 댁내 시스템에서 알려드립니다.응급호출이 발생하였습니다.즉시 출동하여 주시기 바랍니다." 
			+ "응급안전돌보미 댁내 시스템에서 알려드립니다.응급호출이 발생하였습니다.즉시 출동하여 주시기 바랍니다."
			+ "응급안전돌보미 댁내 시스템에서 알려드립니다.응급호출이 발생하였습니다.즉시 출동하여 주시기 바랍니다.";
	
	public final static String EMERGENCY_ENDCALL = "응급상황이 발생하였습니다. 통화를 종료하여 주십시오.     응급상황이 발생하였습니다. 통화를 종료하여 주십시오.";
	public final static String EMERGENCY_FIRE_ENDCALL = "화재경보가 발생하였습니다. 통화를 종료하여 주십시오.     화재경보가 발생하였습니다. 통화를 종료하여 주십시오.";
	public final static String EMERGENCY_GAS_ENDCALL = "가스경보가 발생하였습니다. 통화를 종료하여 주십시오.     가스경보가 발생하였습니다. 통화를 종료하여 주십시오.";
	
	public final static String END_CALL = "기본 통화 사용량을 모두 사용하여 통화가 종료 됩니다.";
	//사용자 멘트
	public final static String CALL_CANCEL = "취소되었습니다.";	
	public final static String IN_ROOM = "재실모드로 설정되었습니다.";
	public final static String GO_OUT = "외출모드로 설정되었습니다.";
	public final static String ELDERLY_CARE = "돌보미로 연결됩니다. ";
	public final static String LOCAL_CENTER = "지역센터로 연결됩니다. ";
	public final static String PROTECTOR_1 = "말벗으로 연결됩니다. ";
	public final static String PROTECTOR_2 = "보호자로 연결됩니다. ";
	public final static String NO_119 = "일일구 전화번호가 없습니다.";
	public final static String NO_LOCAL_CENTER = "지역센터 전화번호가 없습니다.";
	public final static String NO_ELDERLY_CARE = "돌보미 전화번호가 없습니다.";
	public final static String NO_PROTECTOR_1 = "말벗 전화번호가 없습니다.";
	public final static String NO_PROTECTOR_2 = "보호자 전화번호가 없습니다.";
	
	//운영자 멘트	
	public final static String SPEECH_REGISTERED = "등록 되었습니다.";	
	public final static String SPEECH_DELETEED = "삭제 되었습니다.";
	public final static String SPEECH_REGISTER_FAILED = "등록 실패 되었습니다.";
	public final static String SPEECH_DELETE_FAILED = "삭제 실패 되었습니다.";
	public final static String SPEECH_CHECK ="정상동작합니다.";
	public final static String SPEECH_SENSOR_PWR_OFF ="전원이 차단 되었습니다.";
	public final static String SPEECH_SENSOR_PWR_ON ="전원이 복구 되었습니다.";
	public final static String SPEECH_ACTIVITY_SENSOR_LED_ON ="LED를 ON 시킵니다.";
	public final static String SPEECH_ACTIVITY_SENSOR_LED_OFF ="LED를 OFF 시킵니다.";
	public final static String SPEECH_ALL_ACTIVITY_SENSOR_LED_ON ="모든 활동센서. 출입감지센서 LED를 ON 시킵니다.";
	public final static String SPEECH_ALL_ACTIVITY_SENSOR_LED_OFF ="모든 활동센서. 출입감지센서 LED를 OFF 시킵니다.";
	
	public final static String OPEN_DOOR_SENSOR = "문이 열렸습니다.";
	public final static String CLOSE_DOOR_SENSOR = "문이 닫혔습니다.";
	
	public final static String OPENNING_MENT = "확인 버튼을 누르면 개통이 시작 됩니다.";
	public final static String START_OPENNING = "개통을 시작합니다.";
	public final static String SUCCESS_OPENING = "개통이 완료 되었습니다.";
	public final static String FAIL_OPENING = "개통이 실패 되었습니다.";
	public final static String CANCEL_OPENNING = "개통이 취소 되었습니다.";
	
	public final static String SETTING = "설정 되었습니다.";
	public final static String DELETE = "삭제 되었습니다.";
	
	public final static String VISIT_ELDERLY_CARE = "태그버튼을 누르세요, 노인돌보미 방문이 처리되었습니다.";
	public final static String LEAVING_ELDERLY_CARE = "노인돌보미 방문이 퇴실로 처리되었습니다.";
	
	public final static String NOT_REGISTERED_SENSOR = "등록이 되지 않은 센서입니다. 등록을 먼저 진행해주세요";
	public final static String BOARD_LOW_BATT = "본체센서 베터리 양이 부족합니다." + "본체센서 베터리 양이 부족합니다." + "본체센서 베터리 양이 부족합니다.";
	public final static String SENSOR_LOW_BATT = " 베터리 양이 부족합니다. 건전지를 교체해 주세요.";
	
	public final static String CANCEL_FIRE_CALL = "화재경보가 해지 되었습니다.";
	public final static String CANCEL_GAS_CALL = "가스경보가 해지 되었습니다.";
	public final static String GAS_BREAK_COMPL = "가스밸브가 차단되었습니다.";
	
	public final static String VISIT = "방문";
	public final static String OUT = "퇴실";
	
	public final static String SPEECH_CALL_END = "통화가 종료됩니다.";
	
	public final static String SPEECH_CODI_POWER_OFF = "전원이 빠져있어 정상적인 서비스가 불가능 합니다." + "전원이 빠져있어 정상적인 서비스가 불가능 합니다." + "전원이 빠져있어 정상적인 서비스가 불가능 합니다.";
	public final static String SPEECH_CODI_POWER_ON = "전원이 복구되었습니다.";
	
	public final static String ALERT_NEGLECT_TELEPHONE = "송수화기가 들려져 있으니 제자리에 놓아주시기 바랍니다. 송수화기가 들려져 있으니 제자리에 놓아주시기 바랍니다. 송수화기가 들려져 있으니 제자리에 놓아주시기 바랍니다.";
	
	public final static String SPEECH_CHANGE_RINGTONE = "벨소리가 변경 되었습니다.";
	
	public final static String SPEECH_TEST_COMPLETE = "테스트가 완료 되었습니다.";
	
	public final static String SPEECH_INITIALIZE_COMPLETE = "초기화가 완료 되었습니다.";
	
	public final static String RECEIVE_SELF_CALL = "안녕하세요. 댁내 안전진단을 시행 하였습니다. 댁내 안전진단이 정상적으로 진행 되었으니 수화기를 내려 놓으세요.";
	
	public final static String SPEECH_EXIT_SYSTEM_SETTING = "시스템 설정을 종료합니다.";
	
	public final static String START_APPLICATION_UPDATE = "애플리케이션 업데이트를 시작합니다.";
	public final static String UPDATE_COMPLETE = "업데이트가 완료 되었습니다.";
	
	public final static String START_CODI_FIRMWARE_UPDATE = "본체 펌웨어 업데이트를 시작합니다.";
	
	public final static String EMERGENCY_STATUS = "경보상황이 발생하여 통화를 종료합니다.";
	
}
