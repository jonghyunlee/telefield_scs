package com.telefield.smartcaresystem.util;

import java.io.IOException;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.CountDownTimer;
import android.speech.tts.TextToSpeech;

import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.m2m.M2MReportUtil;
import com.telefield.smartcaresystem.ui.SelfCallUI;

public class SelfCall extends CountDownTimer {
	MediaPlayer mMediaPlayer;
	Context mContext;
	
	boolean isStartSelfCall = false;
	
	public SelfCall(long millisInFuture, long countDownInterval, Context mContext) {
		super(millisInFuture, countDownInterval);
		// TODO Auto-generated constructor stub
		this.mContext = mContext;
	}
	
	public SelfCall(Context mContext) {
		this(60 * 1000, 10000, mContext);	//Default
	}	
	
	public void startCall() {
		LogMgr.d(LogMgr.TAG,"SelfCall startCall");
		mMediaPlayer = new MediaPlayer();
		//디바이스에 설정된 벨소리
		Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
		
		try {
			mMediaPlayer.setDataSource(uri.toString());
			mMediaPlayer.setAudioStreamType(AudioManager.STREAM_RING);
			mMediaPlayer.setLooping(true);
			mMediaPlayer.prepare();
			mMediaPlayer.start();
			
			this.start();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		isStartSelfCall = true;
	}
	
	public void stopSelfCall() {	//재실
		LogMgr.d(LogMgr.TAG,"SelfCall stopSelfCall");
		stopRing();
		this.cancel();
		
		isStartSelfCall = false;
		
		//재실 start--[[
		if( SmartCareSystemApplication.getInstance().getMySmartCareState().isOutMode() )
			SmartCareSystemApplication.getInstance().getMySmartCareState().setInModeChange();		
		//재실 end --]]
		
		
		((SelfCallUI)mContext).selfCallExit(); //셀프콜 종료
		
		SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.RECEIVE_SELF_CALL, TextToSpeech.QUEUE_FLUSH, null);
		//수화기를 들었으니 초기화면으로 이동.
		//mContext.startActivity(new Intent(mContext, LauncherUI.class));
	}
	
	@Override
	public void onTick(long millisUntilFinished) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onFinish() {	//미감지(서버 보고)
		// TODO Auto-generated method stub
		stopRing();
		
		// 활동 미감지 보고 -- start --[[		
		M2MReportUtil.reportNonActivity();
		//활동 미감지 보고 -- stop --]]		
		
		((SelfCallUI)mContext).selfCallExit(); //셀프콜 종료
	}
	
	public void stopRing() {
		if(mMediaPlayer != null && mMediaPlayer.isPlaying()) {
			mMediaPlayer.stop();
			mMediaPlayer.release();
			mMediaPlayer = null;
		}
	}
	
	public boolean isStartSelfCall() {
		return isStartSelfCall;
	}
}
