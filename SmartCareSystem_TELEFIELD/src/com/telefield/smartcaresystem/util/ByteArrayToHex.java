package com.telefield.smartcaresystem.util;

import com.telefield.smartcaresystem.m2m.GatewayIndex;

public class ByteArrayToHex {
	
	public static String byteArrayToHex(byte[] ba , int length){
		  if(ba == null || ba.length == 0){
		   return null;
		  }
		  
		  StringBuffer sb = new StringBuffer(length *2);
		  String hexNumber;
		  
		  int idx = 1;
		  for(int x = 0; x < length; x++){		   
		   hexNumber = "0" + Integer.toHexString(0xFF & ba[x]);
		   sb.append(hexNumber.substring(hexNumber.length()-2));
		   sb.append(" ");
		   
//		   if(idx%20 == 0){
//		    sb.append("\n");
//		   }
		   
		   idx++;
		  }
		  return sb.toString();
		 }
	
	public static byte[] getMacAddress(String mac){
		String[] macArray = mac.split(" ");
		byte[] returnData = new byte[macArray.length];
		
		for (int i = 0; i < macArray.length; i++) {
			returnData[i] = Integer.valueOf(macArray[i], 16).byteValue();
		}
		return returnData;
	}
	
	public static byte[] getBloodPressureMacAddress(String mac) {
		String[] macArray = mac.split(":");
		byte[] returnData = new byte[macArray.length];

		for (int i = 0; i < macArray.length; i++) {
			returnData[i] = Integer.valueOf(macArray[i], 16).byteValue();
		}

		return returnData;

	}
	
	public static byte[] getPayload(byte[] data){
		int size;
		byte[] payload ;
		
 		size = data.length -5;
		payload = new byte[size];
		
		System.arraycopy(data, GatewayIndex.PACKET_PAYLOAD, payload, 0, size);
		return payload;
	}
	
	public static byte[] getPayload(byte[] data,int length){
		int size;
		byte[] payload ;
		
 		size = length -5;
		payload = new byte[size];
		
		System.arraycopy(data, GatewayIndex.PACKET_PAYLOAD, payload, 0, size);
		return payload;
	}
	
	public static int byteArrayToInt(byte[] data) {
		int ret = 0;
		for (int i = 0; i < 4 && i < data.length; i++) {
			ret <<= 8;
			ret |= (int) data[i] & 0xFF;
		}
		return ret;
	}
	
	public static String getTimeString(byte[] ba , int length){
		  if(ba == null || ba.length == 0){
		   return null;
		  }
		  
		  StringBuffer sb = new StringBuffer(length *2);
		  String hexNumber;
		  
		  int idx = 1;
		  for(int x = 0; x < length; x++){		   
		   hexNumber = "0" + Integer.toHexString(0xFF & ba[x]);
	   sb.append(hexNumber.substring(hexNumber.length()-2));	   
	   
	//		   if(idx%20 == 0){
	//		    sb.append("\n");
	//		   }
	   
	   idx++;
	  }
	  return sb.toString();
	 }
	
}
