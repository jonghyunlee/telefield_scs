
package com.telefield.smartcaresystem.util;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.DailyRollingFileAppender;
import org.apache.log4j.Layout;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.helpers.LogLog;

import android.content.Context;
import android.os.Environment;

public class ConfigureLog4J {
	
	static Level rootLevel = Level.DEBUG;
	static String filePattern = "%d %m%n";    
    static String fileName = "smartcaresystem.txt";
    static int maxBackupSize = 5;
    static long maxFileSize = 512 * 1024;
    static String datePattern = ".yyyy-MM-dd";
    
	public static void configure(Context context) {
		final Logger root = Logger.getRootLogger();
		
		
		LogManager.getLoggerRepository().resetConfiguration();
		LogLog.setInternalDebugging(false);
		
        final DailyRollingFileAppender  dailyRollingFileAppender;		
        final Layout fileLayout = new PatternLayout(filePattern);

        try {
        	dailyRollingFileAppender = new DailyRollingFileAppender(fileLayout, Environment.getExternalStorageDirectory() + File.separator + "SmartCareSystem" +  File.separator +"logs" + File.separator  + fileName , datePattern);        
        } catch (final IOException e) {
                throw new RuntimeException("Exception configuring log system", e);
        }
        
        dailyRollingFileAppender.setImmediateFlush(true);
        root.addAppender(dailyRollingFileAppender);        
        root.setLevel(rootLevel);
    }

}
