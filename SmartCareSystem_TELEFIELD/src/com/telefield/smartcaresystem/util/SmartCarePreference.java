package com.telefield.smartcaresystem.util;

import android.content.Context;
import android.content.SharedPreferences;

public class SmartCarePreference {
	//Default value
	public static final String DEFAULT_COMMON_STRING = "-";
	public static final int DEFAULT_COMMON_INT = 0;
	public static final boolean DEFAULT_COMMON_BOOLEAN = false;
	
	//Preference file name
	public static final String USER_SETTING_INFO_FILENAME = "UserSettingInfo";
	public static final String CALL_SETTING_INFO_FILENAME = "CallSettingInfo";
	public static final String SENSOR_SETTING_INFO_FILENAME = "SensorSettingInfo";
	public static final String ADDRESS_INFO_FILENAME = "AddressInfo";
	public static final String SERVER_SETTING_INFO_FILENAME = "ServerSesttingInfo";
	public static final String APPLICATION_SETTING_INFO_FILENAME = "ApplicationSesttingInfo";
	public static final String HIDDEN_MENU_SETTING_INFO_FILENAME = "HiddenMenuSettingInfo";
	
	//Key
	public static final String USER_NAME = "userName";
	public static final String GATEWAY_NUM = "gatewayNum";
	public static final String GATEWAY_MAC = "gatewayMac";
	public static final String ZIGBEE_VER = "zigbeeVer";
	
	public static final String AUTO_RECEIVING_TIME = "autoReceiveTime";
	
	public static final String DOOR_OPEN_CLOSE = "doorOpenClose";
	public static final String OUT_COME = "outCome";
	public static final String FIRE_ANNOUNCE = "fireAnnounce";
	
	public static final String PERIODIC_REPORT_TIME = "periodicReportTime";
	public static final String NON_ACTIVITY_TIME = "nonActivityTime";
	public static final String NON_ACTIVITY_NUM = "nonActivityNum";
	public static final String LED_ON_OFF = "ledOnOff";
	public static final String DETECT_ACTIVITY_TIME = "DetectActivityTime";
	
	public static final String M2M_IP = "m2mIp";
	public static final String M2M_PORT = "m2mPort";
	
	public static final String SERVER_PERIODIC_REPORT_TIME = "serverPeriodicReportTime";
	
	public static final String SELF_CALL_FROM_TIME = "selfCallFromTime";
	public static final String SELF_CALL_TO_TIME = "selfCallToTime";
	public static final String OPENNING_RESULT = "resultOpenning";
	public static final String CHARGE_DISCHARGE_PERIOD_DAY = "ChargeDischargePeriodDay";
	public static final String CHARGE_DISCHARGE_PERFORM_TIME = "ChargeDischargePerformTime";
	public static final String START_APP_MONITORING = "StartAppMonitoring";
	public static final String CHARGE_STATUSBAR_STATE = "ChangeStatusbarState";
	public static final String SELF_CALL_ON_OFF = "SelfCallOnOff";
	public static final String HANDSEST_LOCK_ON_OFF = "HandsetOnOff";
	
	public static final String FIRMWARE_VERIFICATION_SETTING = "FirmwareVerificationSetting";
	public static final String START_ADDRESS = "StartAddress";
	public static final String ADDRESS_SIZE = "AddressSize";
	public static final String DISABLED_ON_OFF = "DisabledOnOff";
	public static final String ALL_CALL_TIME = "AllCallTime";
	public static final String CALL_LIMIT_TIME = "CallLimitTime";
	public static final String VOLUME_SETTING = "Volume_setting";
	/**
	 * @see true = 전원복구상태.. 주기보고 시작
	 * @see	false = 전원차단상태.. 주기보고 중지
	 */
	public static final String IS_POWER_OFF_FOR_PERIOD = "PowerOffForPeriod";
	Context mContext;
	SharedPreferences pref;
		
	public SmartCarePreference(Context mContext){
		this.mContext = mContext;
	}
	
	public SmartCarePreference(Context mContext, String filename) {
		this.mContext = mContext;
		
		pref = mContext.getSharedPreferences(filename, Context.MODE_PRIVATE);
	}
	
	public void setFileName(String filename){
		pref = mContext.getSharedPreferences(filename, Context.MODE_PRIVATE);
	}
	
	public String getStringValue(String key, String defaultValue) {
		return pref.getString(key, defaultValue);
	}
	
	public int getIntValue(String key, int defaultValue) {
		return pref.getInt(key, defaultValue);
	}
	
	public boolean getBooleanValue(String key, boolean defaultValue) {
		return pref.getBoolean(key, defaultValue);
	}
	
	public void setStringValue(String key, String value) {
		SharedPreferences.Editor editor = pref.edit();
		editor.putString(key, value);
		editor.commit();
	}
	
	public void setIntValue(String key, int value) {
		SharedPreferences.Editor editor = pref.edit();
		editor.putInt(key, value);
		editor.commit();
	}
	
	public void setBooleanValue(String key, boolean value) {
		SharedPreferences.Editor editor = pref.edit();
		editor.putBoolean(key, value);
		editor.commit();
	}
}
