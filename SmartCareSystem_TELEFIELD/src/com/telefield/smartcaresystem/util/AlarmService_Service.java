package com.telefield.smartcaresystem.util;

import java.io.File;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.telefield.smartcaresystem.Constant;
import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.database.ConnectDB;
import com.telefield.smartcaresystem.m2m.PeriodicalReportService;

public class AlarmService_Service extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		LogMgr.d(LogMgr.TAG, "AlarmService_Service.java:onReceive:// intent.getAction() : " + intent.getAction());
		
		if( intent.getAction().equals(Constant.DELETE_HISTORY_ACTION))
		{
			ConnectDB db = ConnectDB.getInstance(context);
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.MONTH, -1);
			Date EndTime = cal.getTime();
			
			db.deleteActivityData(EndTime);
			db.deleteMessageHistory(EndTime);
			db.deleteSensorHistory(EndTime);
			LogMgr.d(LogMgr.TAG, "AlarmService_Service.java:onReceive: Done DELETE_HISTORY_ACTION ");
//			long beforeTime = cal.getTimeInMillis();
//			File[] logFile = new File(Constant.LOG_PATH).listFiles();
//			File[] camFile = new File(Constant.CAM_PATH).listFiles();	
//			
//			List<File> allList = new Vector<File>();
//			if(logFile != null)
//				allList.addAll(Arrays.asList(logFile));
//			if(camFile != null)
//				allList.addAll(Arrays.asList(camFile));
//			
//			LogMgr.d(LogMgr.TAG, "AlarmService_Service.java:onReceive:// allList.size() : " + allList.size());
//	
//			for (File tempfile : allList) {3/93
//				if(beforeTime > tempfile.lastModified()){
//					LogMgr.d(LogMgr.TAG, "AlarmService_Service.java:onReceive:// Delete File : " + tempfile.getAbsoluteFile());
//					tempfile.delete();
//				}
//			}
			
		}else if(intent.getAction().equals(Constant.BATT_FULL_ACTION))
		{
			LogMgr.d(LogMgr.TAG,"Constant.BATT_FULL_ACTION");
			
			SmartCareSystemApplication.getInstance().getPwrMgr().setCanModeChange(true);			
		}
		else if(intent.getAction().equals(Constant.PERIODIC_REPORT_ACTION))
		{
			LogMgr.d(LogMgr.TAG,"Constant.PERIODIC_REPORT_ACTION");
			Intent periodic_intent = new Intent(context, PeriodicalReportService.class);
			periodic_intent.putExtra(PeriodicalReportService.REPORT_PARAM, PeriodicalReportService.PERIODIC_REPORT);
			context.startService(periodic_intent);
		}else if(intent.getAction().equals(Constant.NON_ACTIVITY_REPORT_ACTION))
		{
			LogMgr.d(LogMgr.TAG,"Constant.NON_ACTIVITY_REPORT");
			Intent periodic_intent = new Intent(context, PeriodicalReportService.class);
			periodic_intent.putExtra(PeriodicalReportService.REPORT_PARAM, PeriodicalReportService.NON_ACTIVITY_REPORT);				
			context.startService(periodic_intent);
		}else if(intent.getAction().equals(Constant.LOCKSCREEN_DELETE_ACTION)){
			LogMgr.d(LogMgr.TAG,"Constant.LOCKSCREEN_DELETE_ACTION");
			File file = new File(Constant.LOCKSCREEN_IMAGE_PATH);
			if(file.exists())
				file.delete();	
		}

	}
}
