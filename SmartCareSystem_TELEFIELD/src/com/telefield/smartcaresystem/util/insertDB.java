package com.telefield.smartcaresystem.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import android.content.Context;
import android.content.res.AssetManager;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class insertDB extends SQLiteOpenHelper {

	static String NAME = "testdb.sqlite";
	static CursorFactory FACTORY = null;
	static String PACKEGE = "com.telefield.smartcaresystem";
	static String DB = "db.db";
	static final String DB_NAME = "telefield.db";
	static int VERSION = 1;

	public insertDB(Context context) {
		super(context, NAME, FACTORY, VERSION);
		boolean bResult = isCheckDB(context); // DB가 있는지?
		LogMgr.d(LogMgr.TAG, "MySQLiteOpenHelper.java:MySQLiteOpenHelper:// bResult : " + bResult);

		copyDB(context);
	}

	// DB가 있나 체크하기
	public boolean isCheckDB(Context mContext) {
		String filePath = "/data/data/" + PACKEGE + "/databases/" + DB_NAME;
		File file = new File(filePath);

		if (file.exists()) {
			return true;
		}

		return false;

	}

	// DB를 복사하기
	// assets의 /db/xxxx.db 파일을 설치된 프로그램의 내부 DB공간으로 복사하기
	public void copyDB(Context mContext) {
		LogMgr.d(LogMgr.TAG, "MySQLiteOpenHelper.java:copyDB");

		AssetManager manager = mContext.getAssets();
		String folderPath = "/data/data/" + PACKEGE + "/databases";
		String filePath = "/data/data/" + PACKEGE + "/databases/" + DB_NAME;
		File folder = new File(folderPath);
		File file = new File(filePath);

		FileOutputStream fos = null;
		BufferedOutputStream bos = null;
		try {
			InputStream is = manager.open(DB);
			BufferedInputStream bis = new BufferedInputStream(is);

			if (folder.exists()) {
			} else {
				folder.mkdirs();
			}

			if (file.exists()) {
				file.delete();
				file.createNewFile();
			}

			fos = new FileOutputStream(file);
			bos = new BufferedOutputStream(fos);
			int read = -1;
			byte[] buffer = new byte[1024];
			while ((read = bis.read(buffer, 0, 1024)) != -1) {
				bos.write(buffer, 0, read);
			}

			bos.flush();

			bos.close();
			fos.close();
			bis.close();
			is.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// String QUERY =
		// "CREATE TABLE word (_id INTEGER PRIMARY KEY autoincrement, word_e TEXT , word_k TEXT)";
		// db.execSQL(QUERY);

		// String QUERY1 =
		// "INSERT INTO word (word_e, word_k ) VALUES(apple , 사과)";
		// db.execSQL(QUERY1);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		String QUERY = "DROP TABLE IF EXISTS word";
		db.execSQL(QUERY);
		onCreate(db);

	}
}