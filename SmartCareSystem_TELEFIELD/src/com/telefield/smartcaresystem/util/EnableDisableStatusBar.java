package com.telefield.smartcaresystem.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import android.content.Context;

public class EnableDisableStatusBar {
	Context mContext;
	
	public EnableDisableStatusBar(Context mContext) {
		this.mContext = mContext;
	}
	
	public void enableOrDisableStatusBar(boolean isDisable) {
		Object object = mContext.getSystemService("statusbar");

	    Class<?> statusBarManager;
	    try {
	    	LogMgr.d(LogMgr.TAG,"Disable Status Bar => "+isDisable);
	        statusBarManager = Class.forName("android.app.StatusBarManager");
	        
	        Field field; 
	        if(isDisable) {
	        	field = statusBarManager.getDeclaredField("DISABLE_EXPAND");
	        }
	        else {
	        	field = statusBarManager.getDeclaredField("DISABLE_NONE");
	        }
	        field.setAccessible(true);
	        
	        Method disable = statusBarManager.getMethod("disable", new Class[] {int.class});
	        disable.setAccessible(true);
	        disable.invoke(object,new Integer((int)field.getInt(this)));
	    } catch (ClassNotFoundException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	    } catch (Exception e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	    }
	}
}
