package com.telefield.smartcaresystem.m2m;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.os.AsyncTask;

import com.telefield.smartcaresystem.Constant;
import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.database.ConnectDB;
import com.telefield.smartcaresystem.database.MessageHistory;
import com.telefield.smartcaresystem.util.ByteArrayToHex;
import com.telefield.smartcaresystem.util.LogMgr;

public class LogTransfer extends AsyncTask<Void, Void, Void> {

	public LogTransfer() {

	}

	@Override
	protected Void doInBackground(Void... params) {
		LogMgr.d(LogMgr.TAG, "LogTransfer START");

		ConnectDB con = ConnectDB.getInstance(SmartCareSystemApplication.getInstance().getApplicationContext());
		Date startTime, endTime;
		Calendar cal = Calendar.getInstance();
		endTime = cal.getTime();

		cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -15);
		startTime = cal.getTime();

		List<MessageHistory> selectList = con.selectMessageHistory(startTime, endTime, 0);
		if (selectList.size() > 0) {
			StringBuilder sb = new StringBuilder();
			for (MessageHistory data : selectList) {
				sb.append(ByteArrayToHex.byteArrayToHex(data.getDATA(), data.getDATA().length));
				sb.append("\n");
			}
			File temp = new File(Constant.LOG_TEMP_PATH);
			if (!temp.exists()) {
				try {
					temp.createNewFile();
				} catch (IOException e) {
				}
			}
			BufferedOutputStream bos = null;
			try {
				bos = new BufferedOutputStream(new FileOutputStream(temp));
				bos.write(sb.toString().getBytes());
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (bos != null) {
					try {
						bos.close();
					} catch (IOException e) {
					}
				}
			}

			FileInputStream fis = null;
			BufferedInputStream bis = null;
			ProtocolManger pmanager = SmartCareSystemApplication.getInstance().getProtocolManger();

			try {
				fis = new FileInputStream(temp);
				bis = new BufferedInputStream(fis);

				byte[] bytes = new byte[GatewayIndex.LOG_BUFFER_SIZE];

				int totalBlockNum = (int) (temp.length() / bytes.length) + 1;
				LogMgr.d(LogMgr.TAG, "ProtocolManger.java:insertCamData:// totalBlockNum : " + totalBlockNum);

				int currentBlockNum = 0;

				while (bis.read(bytes) != -1) {
					currentBlockNum++;
					pmanager.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_LOG_DATA, bytes, totalBlockNum, currentBlockNum));
				}

			} catch (Exception e1) {
				e1.printStackTrace();
			} finally {
				if (bis != null)
					try {
						bis.close();
					} catch (IOException e) {
					}
				if (fis != null)
					try {
						fis.close();
					} catch (IOException e) {
					}
				pmanager.messageSend(null);
				temp.delete();
			}

		}
		// InetSocketAddress address = new InetSocketAddress(IP, port);
		// FileInputStream fis = null;
		// BufferedInputStream bis = null;
		// BufferedOutputStream bos = null;
		// OutputStream output = null;
		// Socket socket = null;
		// File file = new File(filePath);
		// LogMgr.d(LogMgr.TAG,
		// "LogTransfer.java:doInBackground:// file.length() : " +
		// file.length());
		//
		// if (file.isFile()) {
		// try {
		// socket = new Socket();
		// socket.setTcpNoDelay(true);
		// socket.setKeepAlive(true);
		// socket.connect(address, ProtocolConfig.CONNECT_TIMEOUT);
		//
		// if (socket.isConnected()) {
		// output = socket.getOutputStream();
		// bos = new BufferedOutputStream(output);
		//
		// fis = new FileInputStream(file);
		// bis = new BufferedInputStream(fis);
		//
		// byte[] bytes = new byte[GatewayIndex.LOG_BUFFER_SIZE];
		//
		// int readsize;
		// while ((readsize = bis.read(bytes)) != -1) {
		// bos.write(bytes, 0, readsize);
		// bos.flush();
		// }
		// }
		// } catch (Exception e) {
		// e.printStackTrace();
		// } finally {
		// if (bis != null)
		// try {
		// bis.close();
		// } catch (IOException e) {
		// }
		// if (bos != null)
		// try {
		// bos.close();
		// } catch (IOException e) {
		// }
		// if (fis != null)
		// try {
		// fis.close();
		// } catch (IOException e) {
		// }
		// if (output != null)
		// try {
		// output.close();
		// } catch (IOException e) {
		// }
		// if (socket != null)
		// try {
		// socket.close();
		// } catch (IOException e) {
		// }
		// }
		// }
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		LogMgr.d(LogMgr.TAG, "LogTransfer END");

		super.onPostExecute(result);
	}

}
