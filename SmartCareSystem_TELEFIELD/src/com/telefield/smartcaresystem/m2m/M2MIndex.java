package com.telefield.smartcaresystem.m2m;

public interface M2MIndex {
	static final byte START_DATA = 0x02;
	static final byte END_DATA = 0x03;
	
	// 개통처리 결과 (등록요청결과)
	static final byte FAIL_OPENNING = 0x00;
	static final byte SUCCESS_OPENNING = 0x01;
	
	// Frame Size
	static final int SIZE_PACKET_NUMBER = 12;
	static final int SIZE_TIME = 4;
	
	// Frame Header Index
	static final int PACKET_START = 0;
	static final int PACKET_LENGTH = 1;
	
	// Common Data Index
	static final int PACKET_ID = 3;
	static final int PACKET_GW_NUM = 4;
	static final int PACKET_MSG_ID = PACKET_GW_NUM + SIZE_PACKET_NUMBER;
	
	
	// 설정변경(sensor - on/off type) Index
	static final int SENSOR_MAC = PACKET_MSG_ID + 1;
	
	// 설정변경(게이트웨이 설정 변경 - 전화번호등.) Index
	static final int CHANGE_DATA_LENGTH = PACKET_MSG_ID + 1;
	static final int CHANGE_DATA = CHANGE_DATA_LENGTH + 1;
	
	// data request Index
	static final int REQUEST_DATA_REQ_PACKET = PACKET_GW_NUM + SIZE_PACKET_NUMBER;
	static final int REQUEST_DATA_REQ_MSG = REQUEST_DATA_REQ_PACKET + 1;
	static final int REQUEST_DATA_REQ_SENSOR = REQUEST_DATA_REQ_MSG + 1;
	
	// data request (로그 송신 요청 및 활동량 미보고 데이터 요청) Index
	static final int REQUEST_DATA_LOG_AND_MISSED_MSG_ID = PACKET_GW_NUM + SIZE_PACKET_NUMBER;
	static final int REQUEST_DATA_LOG_AND_MISSED_START_TIME = REQUEST_DATA_REQ_PACKET + 1;
	static final int REQUEST_DATA_LOG_AND_MISSED_END_TIME = REQUEST_DATA_REQ_MSG + SIZE_TIME;
	
	// data request (활동량 1주일 데이터 및 역점검 요청) Index
	static final int REQUEST_DATA_WEEK_AND_CHECK_MSG_ID = PACKET_GW_NUM + SIZE_PACKET_NUMBER;
	
	// F/W update size
	static final int SIZE_FIRMWARE_INFO = 22;
	static final int SIZE_FIRMWARE_GATEWAY = 20;
	static final int SIZE_FIRMWARE_OTHER = 22;
	static final int FW_BLOCK_SIZE = 2;
	static final int FW_DATA_SIZE = 2;
	
	// F/W update GateWay index
	static final int FW_GW_CURRENT_BLOCK_NUM = PACKET_GW_NUM + SIZE_PACKET_NUMBER;
	static final int FW_GW_TOTAL_BLOCK_NUM = FW_GW_CURRENT_BLOCK_NUM + FW_BLOCK_SIZE;
	static final int FW_GW_BLOCK_DATA_SIZE = FW_GW_TOTAL_BLOCK_NUM + FW_BLOCK_SIZE;
	static final int FW_GW_BLOCK_DATA = FW_GW_BLOCK_DATA_SIZE + 1;
	
	// F/W update(센서,APP) index
	static final int FW_OTHER_CURRENT_BLOCK_NUM = PACKET_MSG_ID + 1;
	static final int FW_OTHER_TOTAL_BLOCK_NUM = FW_OTHER_CURRENT_BLOCK_NUM + FW_BLOCK_SIZE;
	static final int FW_OTHER_BLOCK_DATA_SIZE = FW_OTHER_TOTAL_BLOCK_NUM + FW_BLOCK_SIZE;
	static final int FW_OTHER_BLOCK_DATA = FW_OTHER_BLOCK_DATA_SIZE + FW_DATA_SIZE;
	
	
	/**
	 * 설정변경(게이트웨이 설정 변경 - 전화번호등.)
	 */
	static final byte CHANGE_SETTING = 0x09;
	
	/**
	 * data request
	 */
	static final byte DATA_REQUEST = 0x0A;
	
	/**
	 * data request (로그 송신 요청 및 활동량 미보고 데이터 요청)
	 */
	static final byte DATA_REQUEST_LOG_AND_MISSED = 0x0E;
	
	/**
	 * data request (활동량 1주일 데이터 및 역점검 요청)
	 */
	static final byte DATA_REQUEST_WEEK_AND_RECHECK = 0x0D;
	
	/**
	 * F/W update
	 */
	static final byte FIRMWARE_UPDATE_GW = 0x0C;
	
	/**
	 * F/W update(센서,APP)
	 */
	static final byte FIRMWARE_UPDATE_OTHER = 0x1C;
	
	/**
	 * 데이터 전송 종료
	 */
	static final byte FINISH_SEND = 0x00;
	
	/**
	 * 개통처리 결과 (등록요청결과)
	 */
	static final byte RESULT_OPENNING = 0x31;
	
	/**
	 * 개통처리요청 (등록요청)
	 */
	static final byte START_OPENNING = 0x32;
	
	
	/**
	 *  로그 전송 ( TEST )
	 */
	static final byte LOG_REQUEST = 0x22;
	
	
	/**
	 * 미보고 데이터 요청(test)
	 */
	static final byte NOT_REPORTED = 0x1A;
}
