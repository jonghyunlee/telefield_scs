package com.telefield.smartcaresystem.m2m;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.net.TrafficStats;
import android.os.IBinder;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.telefield.smartcaresystem.Constant;
import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.database.ActivityData;
import com.telefield.smartcaresystem.database.ConnectDB;
import com.telefield.smartcaresystem.database.Sensor;
import com.telefield.smartcaresystem.message.FormattedFrame;
import com.telefield.smartcaresystem.message.SensorID;
import com.telefield.smartcaresystem.sensor.SensorDeviceManager;
import com.telefield.smartcaresystem.ui.SelfCallUI;
import com.telefield.smartcaresystem.ui.UIConstant;
import com.telefield.smartcaresystem.util.LogMgr;
import com.telefield.smartcaresystem.util.SmartCarePreference;

/**
 * 10분마다 호출 되는 class 주기 보고를 위한 클래스
 * 
 * @author Administrator
 * 
 */
public class PeriodicalReportService extends Service {

	SensorDeviceManager sensorDeviceMgr;
	ProtocolManger mProtocolManger;
	int gatewayRssi = 0;
	public static final String REPORT_PARAM = "REPORT_PARAM";

	/**
	 * 주기보고 . 설정(10분)된 시간마다 보고
	 * 
	 */
	public static final int PERIODIC_REPORT = 0;
	/**
	 * 미활동 보고 설정된 시간동안 활동량이 존재하지 않으면 보고
	 */
	public static final int NON_ACTIVITY_REPORT = 1;

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		LogMgr.d(LogMgr.TAG, "PeriodicalReportService onCreate");
		sensorDeviceMgr = SmartCareSystemApplication.getInstance()
				.getSensorDeviceManager();
		mProtocolManger = SmartCareSystemApplication.getInstance()
				.getProtocolManger();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		LogMgr.d(LogMgr.TAG, "PeriodicalReportService onStartCommand");
		
		int mode = intent.getIntExtra(REPORT_PARAM, PERIODIC_REPORT);
		
		TelephonyManager telManager  = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
		telManager.listen(listener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
		
		SmartCarePreference pwrpreference = new SmartCarePreference(SmartCareSystemApplication.getInstance().getApplicationContext(), SmartCarePreference.SERVER_SETTING_INFO_FILENAME);
		boolean ispwroff = pwrpreference.getBooleanValue(SmartCarePreference.IS_POWER_OFF_FOR_PERIOD, true);
		LogMgr.d(LogMgr.TAG, "PeriodicalReportService gateway pwr state = " + ispwroff);
		if(ispwroff == false){
			return START_STICKY;
		}
		
		if (mode == PERIODIC_REPORT) {		
			
			
			LogMgr.d(LogMgr.TAG,
					"PeriodicalReportService onStartCommand -- PERIODIC_REPORT ");
			// sensor 들을 update 한다.
			sensorDeviceMgr.updateSensors();

			// to do 주기보고
			List<Sensor> sensor_list = sensorDeviceMgr.getSensorList();

			Iterator<Sensor> it = sensor_list.iterator();
			while (it.hasNext()) {
				Sensor sensor = it.next();
				if (SensorID.getSensorType(sensor.getSensor_id().intValue()) >= SensorID.CARE_SENSOR_TYPE.getSensorType())
					continue; // 돌보미 센서는 주기보고에서 제외한다. 돌보미 센서 타입은 0x80xx ~ 0xFFFF
				
				LogMgr.d(LogMgr.TAG,"PERIODIC_REPORT insert frame : " + sensor.getSensor_id());
				// 센서정보를 전달.
				mProtocolManger.insertFrame(ProtocolFrame.getInstance(
						GatewayIndex.INFO_REPORT_CYCLE, sensor, null));

			}
			Set<BluetoothDevice> pairedDevices = BluetoothAdapter.getDefaultAdapter().getBondedDevices();
    		BluetoothDevice bloodPressure = null;
    		for (BluetoothDevice device : pairedDevices) {
    			if(device.getBluetoothClass().getDeviceClass() == BluetoothClass.Device.HEALTH_BLOOD_PRESSURE)
    				bloodPressure = device;
    		}
			if (bloodPressure != null) {
				String mac = bloodPressure.getAddress().replaceAll(":", " ");
				Sensor sensor = new Sensor(SensorID.getSensorId(SensorID.BLOOD_PRESURE_TYPE.getSensorType(), 0x01), mac);
				sensor.setComm_status(1);
				sensor.setSensor_batt(100);

				mProtocolManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_CYCLE, sensor, null));
			}
			
			if(sensor_list.size() > 0 || bloodPressure != null){
				LogMgr.d(LogMgr.TAG, "PeriodicalReportService Gateway Rssi : " + gatewayRssi);
				mProtocolManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_GW_DATA, FormattedFrame.getInstance(GateWayMessage.ID_REPORT_GATEWAY_INFO, this.getSub_data()))); 
				mProtocolManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_GW_DATA, FormattedFrame.getInstance(GateWayMessage.ID_REPORT_FINISH_SEND)));
				mProtocolManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REQUEST_DATA));
				mProtocolManger.messageSend(null);
			}
		} else if (mode == NON_ACTIVITY_REPORT) // 미활동 보고
		{
			LogMgr.d(LogMgr.TAG,
					"PeriodicalReportService onStartCommand -- NON_ACTIVITY_REPORT ");
			boolean isNeedNonActivityReport = false;
			
			//설정한 시간의 정보를 바탕으로 활동량이 있는지 확인한다.활동량이 없으면 보고한다.
			ConnectDB db = ConnectDB.getInstance(SmartCareSystemApplication
					.getInstance().getApplicationContext());
			SmartCarePreference pref = new SmartCarePreference(SmartCareSystemApplication.getInstance().getApplicationContext(), 
					SmartCarePreference.SENSOR_SETTING_INFO_FILENAME);			
			
			//unit minute; default 60분			
			int diff_time = pref.getIntValue(SmartCarePreference.NON_ACTIVITY_TIME,UIConstant.DEFAULT_NON_ACTIVITY_TIME_VALUE);
			
			long curr = System.currentTimeMillis()
					/ Constant.TIME_MINUTE_UNIT;
			Date end_date = new Date(curr * Constant.TIME_MINUTE_UNIT);
			Date start_date = new Date((curr-( diff_time))  * Constant.TIME_MINUTE_UNIT);
			List<ActivityData> lists = db.selectActivityData(start_date,end_date);			
			
			int pir_cnt = 0;
			
			int threshold_val = 1;
			//preference 에서 값을 가져와서 확인한다.			
			threshold_val = pref.getIntValue(SmartCarePreference.NON_ACTIVITY_NUM, 1);			
			
			for( ActivityData actData : lists)
			{
				if( actData.getCount() > threshold_val)
				{
					pir_cnt += actData.getCount();
				}
			}			
			
			if( pir_cnt <= threshold_val) // threshold value 보다 작으면 미활동으로 감지 한다.
			{
				isNeedNonActivityReport = true;
			}

			if (isNeedNonActivityReport) {
				SmartCarePreference preference = new SmartCarePreference(SmartCareSystemApplication.getInstance().getApplicationContext(), SmartCarePreference.APPLICATION_SETTING_INFO_FILENAME);
				
				//셀프콜 On/Off 여부 확인
				boolean selfCallOnOff = preference.getBooleanValue(SmartCarePreference.SELF_CALL_ON_OFF, false);
				LogMgr.d(LogMgr.TAG, "Start SelfCall = "+selfCallOnOff);
				if(selfCallOnOff) {
					//셀프콜을 진행해서 수신이 없으면 활동 미감지 보고를 한다.
					SimpleDateFormat format = new SimpleDateFormat("HH:mm");
							
					String fromTimeData = preference.getStringValue(SmartCarePreference.SELF_CALL_FROM_TIME, UIConstant.DEFAULT_START_SELF_CALL_TIME_VALUE);
					String toTimeData = preference.getStringValue(SmartCarePreference.SELF_CALL_TO_TIME, UIConstant.DEFAULT_END_SELF_CALL_TIME_VALUE);
					LogMgr.d(LogMgr.TAG, "Start noSelfCallTime = "+fromTimeData);
					LogMgr.d(LogMgr.TAG, "END noSelfCallTime = "+toTimeData);
					
					long fromTime = 0;
					long toTime = 0;
					
					try {
						Date fromTimeDate = format.parse(fromTimeData);	//날짜는 1970/01/01로 setting 된다.
						Date toTimeDate = format.parse(toTimeData);
						LogMgr.d(LogMgr.TAG, "Start noSelfCallTime(Date) = "+fromTimeDate);
						
						fromTime = fromTimeDate.getTime();
						toTime = toTimeDate.getTime();
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
							
					//현재 시간(날짜는 해당 안됨)
					Calendar cal = Calendar.getInstance();
					cal.set(Calendar.YEAR, 1970);
					cal.set(Calendar.MONTH, 0);
					cal.set(Calendar.DAY_OF_MONTH, 1);
					
					long now = cal.getTimeInMillis();
					
					if(fromTime < toTime) {
						if(!(fromTime <= now && toTime > now)) {
							Intent selfcall_intent = new Intent(SmartCareSystemApplication.getInstance().getApplicationContext(), SelfCallUI.class);
							selfcall_intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_SINGLE_TOP);
							SmartCareSystemApplication.getInstance().getApplicationContext().startActivity(selfcall_intent);
						}else{
							//무조건 보고
							M2MReportUtil.reportNonActivity();
						}
					}
					else {
						if( fromTime > now && toTime <= now ) {
							Intent selfcall_intent = new Intent(SmartCareSystemApplication.getInstance().getApplicationContext(), SelfCallUI.class);
							selfcall_intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_SINGLE_TOP);
							SmartCareSystemApplication.getInstance().getApplicationContext().startActivity(selfcall_intent);
						}else
						{
							//무조건 보고
							M2MReportUtil.reportNonActivity();
						}
					}
				}
				else
				{
					//무조건 보고
					M2MReportUtil.reportNonActivity();					
				}
			}
		}

		return START_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onDestroy() {
		LogMgr.d(LogMgr.TAG, "PeriodicalReportService destroy");
		// TODO Auto-generated method stub
		super.onDestroy();
	}
	
	PhoneStateListener listener = new PhoneStateListener() {
		@Override
		public void onSignalStrengthsChanged(SignalStrength signalStrength) {
			gatewayRssi = -113 + (2 * signalStrength.getGsmSignalStrength());
//			LogMgr.d(LogMgr.TAG, "gatewayRssi : " + gatewayRssi);
			super.onSignalStrengthsChanged(signalStrength);
		}
	};
	
	public byte[] getSub_data(){
		byte [] sub_data = new byte[9]; 
		sub_data[0] = (byte) gatewayRssi;
		SmartCarePreference preference = new SmartCarePreference(this, SmartCarePreference.CALL_SETTING_INFO_FILENAME);
		int allCallTime = preference.getIntValue(SmartCarePreference.ALL_CALL_TIME, 0);
		byte[] getAllCallTime = FormattedFrame.intToFourByteArray(allCallTime);
		int i = 1;
		for(byte tobyte : getAllCallTime){
			sub_data[i] = tobyte;
			i++;
		}
		return sub_data;
	}
	
	
}
