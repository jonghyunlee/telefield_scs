package com.telefield.smartcaresystem.m2m;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.speech.tts.TextToSpeech;

import com.google.common.primitives.Bytes;
import com.telefield.smartcaresystem.Constant;
import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.firmware.ApplicationManager;
import com.telefield.smartcaresystem.message.MessageID;
import com.telefield.smartcaresystem.util.ByteArrayToHex;
import com.telefield.smartcaresystem.util.LogMgr;
import com.telefield.smartcaresystem.util.SpeechUtil;

public class BuildFirmware {
	private static BuildFirmware mBuildFirmware;

	private List<Byte> buildList;
	private int totalCount = -1;
	public static final byte TYPE_GATEWAY = 0x00;
	public static final int TYPE_SENSOR = 0x01;
	public static final int TYPE_APP = 0x02;	

	private BuildFirmware() {
		buildList = new ArrayList<Byte>();
	}

	public static BuildFirmware getInstance() {
		if (mBuildFirmware == null)
			mBuildFirmware = new BuildFirmware();
		return mBuildFirmware;
	}
	
	public byte checkCRC(byte[] data){
		byte check_crc = 0;
		
		for (int i = 0; i < data.length; i++) {
			check_crc += data[i];
		}
		return (byte) (check_crc & 0xFF);
	}

	public void insertBuffer(byte msg_id, byte[] total_cnt, byte[] cur_cnt, byte[] data) {
		int current = ByteArrayToHex.byteArrayToInt(cur_cnt);
		LogMgr.d(LogMgr.TAG, "BuildFirmware.java:insertBuffer:// current : " + current);

		if (current == 0){
			init();
			totalCount = ByteArrayToHex.byteArrayToInt(total_cnt);
		}
		else {
			buildList.addAll(Bytes.asList(data));
			
			if(current == totalCount){
				makeFile(msg_id);
			}
		}
	}

	public void makeFile(byte type) {
		File dir = new File(Constant.FIRMWARE_PATH);
		if (!dir.exists())
			dir.mkdirs();

		byte[] data = Bytes.toArray(buildList);

		ByteBuffer bf = ByteBuffer.allocate(data.length);
		bf.put(data);
		bf.clear();

		FileOutputStream fout = null;
		FileChannel fc = null;
		try {
			String path = getPath(type);
			fout = new FileOutputStream(path);
			fc = fout.getChannel();
			fc.write(bf);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				fc.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				fout.close();
			} catch (IOException ioe) {
			}
			if (type == TYPE_GATEWAY) {
				Context ctx = SmartCareSystemApplication.getInstance().getApplicationContext();

				SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.START_APPLICATION_UPDATE, TextToSpeech.QUEUE_FLUSH, null);

				// 시스템 초기화시 본체 RESET 을 해야 FTDI 가 재연결됨.
				if (SmartCareSystemApplication.getInstance().getMessageManager().ftdi_isConnect()) {
					SmartCareSystemApplication.getInstance().getGlobalMsgFunc().sendGateWayMsg((byte) MessageID.CODI_RESET);
				}

				ApplicationManager am;
				try {
					am = new ApplicationManager(ctx);
					am.installPackage(Constant.FW_APP_PATH);
				} catch (Exception e) {
					e.printStackTrace();
				}
				ActivityManager mgr = (ActivityManager) ctx.getSystemService(Activity.ACTIVITY_SERVICE);
				mgr.killBackgroundProcesses(ctx.getPackageName());
			}
			init();
		}
	}
	
	private String getPath(byte type){
		String path = "";
		switch(type){
		case TYPE_GATEWAY:
			path = Constant.FW_GATEWAY_PATH;
			break;
		case TYPE_SENSOR:
			path = Constant.FW_SENSOR_PATH;
			break;
		case TYPE_APP:
			path = Constant.FW_APP_PATH;
			break;
		}
		return path;
	}

	private void init() {
		totalCount = 0;
		buildList.clear();
	}
}
