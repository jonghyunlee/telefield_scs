package com.telefield.smartcaresystem.m2m;

import java.nio.ByteBuffer;

import com.telefield.smartcaresystem.database.Sensor;
import com.telefield.smartcaresystem.util.ByteArrayToHex;

public class GatewayDevice {
	public byte[] id = {(byte) 0xC0, 0x01};
	public byte[] number = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
	public byte[] MAC = { (byte) 0xA6, 0x1F, (byte) 0xE1, (byte) 0xD1, (byte) 0xC9, 0x64, 0x06, 0x65 };
	public byte power_state = 0x01;
	public byte battery_state = 0x01;
	public byte[] version = { 0x0, 0x0, 0x0 };
	public byte[] smartphone_version = { 0x0, 0x0, 0x0 };
	private static GatewayDevice gatewayDevice;

	private GatewayDevice() {
	}
	
	public static GatewayDevice getInstance() {
		if (gatewayDevice == null)
			gatewayDevice = new GatewayDevice();
		return gatewayDevice;
	}
	
	public static Sensor getBySensor(){
		if (gatewayDevice == null)
			gatewayDevice = new GatewayDevice();
		
		Sensor sensor = new Sensor();
		
		byte[] sensorID = new byte[8];
		System.arraycopy(gatewayDevice.id, 0, sensorID, sensorID.length - gatewayDevice.id.length, gatewayDevice.id.length);
		
		long sensor_id = ByteBuffer.wrap(sensorID).getLong();
		sensor.setSensor_id(sensor_id);
		
		sensor.setMAC(ByteArrayToHex.byteArrayToHex(gatewayDevice.MAC, gatewayDevice.MAC.length));
		sensor.setComm_status((int) gatewayDevice.power_state);
		sensor.setSensor_batt((int) gatewayDevice.battery_state);
		
		return sensor;
	}
}
