package com.telefield.smartcaresystem.m2m;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.math.BigInteger;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.http.util.ByteArrayBuffer;

import android.app.Activity;
import android.app.ActivityManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.os.AsyncTask;
import android.speech.tts.TextToSpeech;
import android.telephony.TelephonyManager;

import com.telefield.smartcaresystem.Constant;
import com.telefield.smartcaresystem.GlobalMessageHandler;
import com.telefield.smartcaresystem.MySmartCareState;
import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.database.ActivityData;
import com.telefield.smartcaresystem.database.ConnectDB;
import com.telefield.smartcaresystem.database.DBInfo;
import com.telefield.smartcaresystem.database.MessageHistory;
import com.telefield.smartcaresystem.database.NotReported;
import com.telefield.smartcaresystem.database.Sensor;
import com.telefield.smartcaresystem.database.Users;
import com.telefield.smartcaresystem.firmware.ApplicationManager;
import com.telefield.smartcaresystem.http.AsyncCallback;
import com.telefield.smartcaresystem.http.AsyncFileDownloader;
import com.telefield.smartcaresystem.message.FormattedFrame;
import com.telefield.smartcaresystem.message.MessageID;
import com.telefield.smartcaresystem.message.SensorID;
import com.telefield.smartcaresystem.sensor.SensorDeviceManager;
import com.telefield.smartcaresystem.ui.UIConstant;
import com.telefield.smartcaresystem.util.ByteArrayToHex;
import com.telefield.smartcaresystem.util.LogMgr;
import com.telefield.smartcaresystem.util.SmartCarePreference;
import com.telefield.smartcaresystem.util.ConvertTime;
import com.telefield.smartcaresystem.util.SpeechUtil;

public class CheckReceiveData {
	private ProtocolManger mProtocolManger;
	private SensorDeviceManager mSensorDeviceManager;
	private GlobalMessageHandler mGlobalMessageHandler;

	
	public CheckReceiveData(ProtocolManger pManager) {
		this.mProtocolManger = pManager;
		this.mGlobalMessageHandler = SmartCareSystemApplication.getInstance().getGlobalMsgFunc();
		this.mSensorDeviceManager = SmartCareSystemApplication.getInstance().getSensorDeviceManager();

	}

	public byte[] analysisData(byte[] data, byte rcv_packet_id) {
		byte[] ackData = null;
		ConnectDB db = ConnectDB.getInstance(SmartCareSystemApplication.getInstance().getApplicationContext());
		LogMgr.d(LogMgr.TAG,"CheckReceiveData : " + ByteArrayToHex.byteArrayToHex(data, data.length) + "...rcv_packet_id : " + rcv_packet_id);
		switch (rcv_packet_id) {
		case M2MIndex.CHANGE_SETTING:
			Sensor sensor = null;
			List<Users> tempData = null;
			byte[] message_data = new byte[data[M2MIndex.CHANGE_DATA_LENGTH]];
			byte msg_id = data[M2MIndex.PACKET_MSG_ID];

			if(msg_id == M2MMessage.ID_SENSOR_CANCEL_ALARM || msg_id == M2MMessage.ID_SENSOR_LED_ON || msg_id == M2MMessage.ID_SENSOR_LED_OFF){
				byte[] mac_address = new byte[ProtocolConfig.SIZE_MACADDRESS];
				System.arraycopy(data, M2MIndex.SENSOR_MAC, mac_address, 0, mac_address.length);

				sensor = mSensorDeviceManager.getSensor(ByteArrayToHex.byteArrayToHex(mac_address, mac_address.length));
			}
			else {
				System.arraycopy(data, M2MIndex.CHANGE_DATA, message_data, 0, message_data.length);
			}
			int messageDataToInt =  0;
			for (int i = 0; i < message_data.length; i++) {
				messageDataToInt += message_data[i] & 0xff;
			}
			
			switch (msg_id) {			
			case M2MMessage.ID_SENSOR_CANCEL_ALARM:
				if (sensor != null) {
					int sensor_type = sensor.getType();
					if (sensor_type == SensorID.FIRE_SENSOR_TYPE.getSensorType()) {
						mGlobalMessageHandler.sendFireClrMsg();
					} else if (sensor_type == SensorID.GAS_SENSOR_TYPE.getSensorType()) {
						mGlobalMessageHandler.sendGasClrMsg();
					}
				}
				break;
			case M2MMessage.ID_SENSOR_LED_ON:
				if (sensor != null)
					mGlobalMessageHandler.sendActivityLEDCommand(sensor.getSensor_id().intValue(), true);
				break;
			case M2MMessage.ID_SENSOR_LED_OFF:
				if (sensor != null)
					mGlobalMessageHandler.sendActivityLEDCommand(sensor.getSensor_id().intValue(), false);
				break;

			case M2MMessage.ID_SETTING_GW_NUMBER:
				setStringPreference(SmartCarePreference.USER_SETTING_INFO_FILENAME, SmartCarePreference.GATEWAY_NUM, new String(message_data));
				break;
			case M2MMessage.ID_SETTING_CENTER_NUMBER:
				tempData = db.selectUser(new Users(null, null, null, null, DBInfo.HELPER_TYPE_CENTER));
				insertData(db, tempData, DBInfo.HELPER_TYPE_CENTER, new String(message_data));

				break;
			case M2MMessage.ID_SETTING_HELPER_NUMBER:
				tempData = db.selectUser(new Users(null, null, null, null, DBInfo.HELPER_TYPE_HELPER));
				insertData(db, tempData, DBInfo.HELPER_TYPE_HELPER, new String(message_data));

				break;
			case M2MMessage.ID_SETTING_CARE1_NUMBER:
				tempData = db.selectUser(new Users(null, null, null, null, DBInfo.HELPER_TYPE_CARE1));
				insertData(db, tempData, DBInfo.HELPER_TYPE_CARE1, new String(message_data));

				break;
			case M2MMessage.ID_SETTING_CARE2_NUMBER:
				tempData = db.selectUser(new Users(null, null, null, null, DBInfo.HELPER_TYPE_CARE2));
				insertData(db, tempData, DBInfo.HELPER_TYPE_CARE2, new String(message_data));

				break;
			case M2MMessage.ID_SETTING_119_NUMBER:
				tempData = db.selectUser(new Users(null, null, null, null, DBInfo.HELPER_TYPE_119));
				insertData(db, tempData, DBInfo.HELPER_TYPE_119, new String(message_data));
				break;
				
			case M2MMessage.ID_SETTING_M2M_ADDRESS:
				String address_data[] = new String(message_data).split(":");
				if (address_data.length == 2) {
					String ip_address = address_data[0];
					String port = address_data[1];
					setStringPreference(SmartCarePreference.ADDRESS_INFO_FILENAME, SmartCarePreference.M2M_IP, ip_address);
					setIntPreference(SmartCarePreference.ADDRESS_INFO_FILENAME, SmartCarePreference.M2M_PORT, Integer.parseInt(port));
				}
				break;
			case M2MMessage.ID_SETTING_M2M_NUMBER:
				setStringPreference(SmartCarePreference.USER_SETTING_INFO_FILENAME, SmartCarePreference.GATEWAY_NUM, new String(message_data));
				break;
			case M2MMessage.ID_SETTING_M2M_GW_NUMBER:
				setStringPreference(SmartCarePreference.USER_SETTING_INFO_FILENAME, SmartCarePreference.GATEWAY_NUM, new String(message_data));
				break;
			case M2MMessage.ID_SETTING_U119_ADDRESS:
				break;
			case M2MMessage.ID_SETTING_REPORT_CYCLE:
				setIntPreference(SmartCarePreference.SENSOR_SETTING_INFO_FILENAME, SmartCarePreference.PERIODIC_REPORT_TIME, messageDataToInt);
				mGlobalMessageHandler.sendKeepAliveTimeMsg(messageDataToInt);
				
				break;
			case M2MMessage.ID_SETTING_REPORT_MISSED_TIME:
				setIntPreference(SmartCarePreference.SENSOR_SETTING_INFO_FILENAME, SmartCarePreference.NON_ACTIVITY_TIME, messageDataToInt);
				SmartCareSystemApplication.getInstance().stopNonActivityReportTimer();
				SmartCareSystemApplication.getInstance().startNonActivityReportTimer();
				break;
			case M2MMessage.ID_SETTING_REPORT_CYCLE_TIME:
				setIntPreference(SmartCarePreference.SERVER_SETTING_INFO_FILENAME, SmartCarePreference.SERVER_PERIODIC_REPORT_TIME, messageDataToInt);
				SmartCareSystemApplication.getInstance().stopPeriodReportTimer();
				SmartCareSystemApplication.getInstance().startPeriodReportTimer();
				break;
			case M2MMessage.ID_SETTING_MISSED_THRESHOLD:
				setIntPreference(SmartCarePreference.SENSOR_SETTING_INFO_FILENAME, SmartCarePreference.NON_ACTIVITY_NUM, messageDataToInt);
				break;
			case M2MMessage.ID_SETTING_ACTIVE_SENSOR_VALUE:
				setIntPreference(SmartCarePreference.SENSOR_SETTING_INFO_FILENAME, SmartCarePreference.DETECT_ACTIVITY_TIME, messageDataToInt);
				break;
			case M2MMessage.ID_SETTING_FORCE_RECEIVE:
				setIntPreference(SmartCarePreference.CALL_SETTING_INFO_FILENAME, SmartCarePreference.AUTO_RECEIVING_TIME, messageDataToInt);
				break;
			case M2MMessage.ID_SETTING_FINISH_SEND:
				break;
			case M2MMessage.ID_SETTING_OPEN_VALVE:
				
				break;
			case M2MMessage.ID_SETTING_CLOSE_VALVE:
				SmartCareSystemApplication.getInstance().getGlobalMsgFunc().sendGasBreakMsg();
				break;
			}


			ackData = new byte[M2MIndex.PACKET_MSG_ID - M2MIndex.PACKET_ID + 1];
			System.arraycopy(data, M2MIndex.PACKET_ID, ackData, 0, M2MIndex.PACKET_MSG_ID - M2MIndex.PACKET_ID + 1);

			break;
		case M2MIndex.DATA_REQUEST:
			checkDataRequest(data[M2MIndex.REQUEST_DATA_REQ_PACKET], data[M2MIndex.REQUEST_DATA_REQ_MSG]);
			break;
		case M2MIndex.DATA_REQUEST_LOG_AND_MISSED:
			byte[] start_time = new byte[M2MIndex.SIZE_TIME], end_time = new byte[M2MIndex.SIZE_TIME];
			
			System.arraycopy(start_time, 0, data, M2MIndex.REQUEST_DATA_LOG_AND_MISSED_START_TIME, M2MIndex.SIZE_TIME);
			System.arraycopy(end_time, 0, data, M2MIndex.REQUEST_DATA_LOG_AND_MISSED_END_TIME, M2MIndex.SIZE_TIME);
			
			checkLogAndMissed(data[M2MIndex.REQUEST_DATA_LOG_AND_MISSED_MSG_ID], start_time, end_time);
			break;
		case M2MIndex.DATA_REQUEST_WEEK_AND_RECHECK:
			byte msg_ID = data[M2MIndex.REQUEST_DATA_WEEK_AND_CHECK_MSG_ID];
			checkWeekAndReCheck(msg_ID);
			break;
		case M2MIndex.FIRMWARE_UPDATE_GW: {
			int data_size = data[M2MIndex.FW_GW_BLOCK_DATA_SIZE];
			byte crc, check_crc;

			byte[] fw_data = new byte[data_size];
			byte[] total_cnt = new byte[M2MIndex.FW_BLOCK_SIZE], cur_cnt = new byte[M2MIndex.FW_BLOCK_SIZE];

			System.arraycopy(data, M2MIndex.FW_GW_CURRENT_BLOCK_NUM, cur_cnt, 0, M2MIndex.FW_BLOCK_SIZE);
			System.arraycopy(data, M2MIndex.FW_GW_TOTAL_BLOCK_NUM, total_cnt, 0, M2MIndex.FW_BLOCK_SIZE);
			System.arraycopy(data, M2MIndex.FW_GW_BLOCK_DATA, fw_data, 0, data_size);

			crc = data[data.length - 3];
			check_crc = BuildFirmware.getInstance().checkCRC(fw_data);

			int currentCount = ByteArrayToHex.byteArrayToInt(cur_cnt);
			int totalCount = ByteArrayToHex.byteArrayToInt(total_cnt);
			
			if (currentCount == 0) {
				SmartCarePreference pref = new SmartCarePreference(SmartCareSystemApplication.getInstance().getApplicationContext(), SmartCarePreference.USER_SETTING_INFO_FILENAME);
				String version = pref.getStringValue(SmartCarePreference.ZIGBEE_VER, UIConstant.ZIGBEE_DEFAULT_VER);
				int cur_version = Integer.parseInt(version);
				int update_version = ByteArrayToHex.byteArrayToInt(fw_data);
				if (update_version > cur_version) {
					if(Constant.RUN_FOR_BMT){
						new AsyncFileDownloader(SmartCareSystemApplication.getInstance().getApplicationContext()).
						download(Constant.GATEWAY_FW_DOWNLOAD_PATH, Constant.FW_APP_PATH, fileDownCallback);
					}
					else{
						BuildFirmware.getInstance().insertBuffer(BuildFirmware.TYPE_GATEWAY, total_cnt, cur_cnt, fw_data);
						
						ackData = new byte[M2MIndex.SIZE_FIRMWARE_GATEWAY];
						System.arraycopy(data, M2MIndex.PACKET_ID, ackData, 0, M2MIndex.FW_GW_TOTAL_BLOCK_NUM - 1);
						
						// current 증가
						int curBlockNum = currentCount + 1;
						cur_cnt = ByteBuffer.allocate(2).putShort((short) curBlockNum).array();
						System.arraycopy(cur_cnt, 0, ackData, M2MIndex.FW_GW_CURRENT_BLOCK_NUM - M2MIndex.PACKET_ID, M2MIndex.FW_BLOCK_SIZE);
						
						// block data size : 1
						ackData[M2MIndex.FW_GW_BLOCK_DATA_SIZE - M2MIndex.PACKET_ID] = 0x01;						
					}
				} else {
					ackData = new byte[M2MIndex.SIZE_FIRMWARE_INFO];
					System.arraycopy(data, M2MIndex.PACKET_ID, ackData, 0, M2MIndex.SIZE_FIRMWARE_INFO);
				}
			} else if(currentCount < totalCount){
				ackData = new byte[M2MIndex.SIZE_FIRMWARE_GATEWAY];
				System.arraycopy(data, M2MIndex.PACKET_ID, ackData, 0, M2MIndex.FW_GW_TOTAL_BLOCK_NUM - 1);

				// block data size : 1
				ackData[M2MIndex.FW_GW_BLOCK_DATA_SIZE - M2MIndex.PACKET_ID] = 0x01;

				if (check_crc == crc) {
					// current 증가
					int curBlockNum = currentCount + 1;
					cur_cnt = ByteBuffer.allocate(2).putShort((short) curBlockNum).array();
					System.arraycopy(cur_cnt, 0, ackData, M2MIndex.FW_GW_CURRENT_BLOCK_NUM - M2MIndex.PACKET_ID, M2MIndex.FW_BLOCK_SIZE);

					BuildFirmware.getInstance().insertBuffer(BuildFirmware.TYPE_GATEWAY, total_cnt, cur_cnt, fw_data);
				} else {
					ackData[M2MIndex.FW_GW_BLOCK_DATA - M2MIndex.PACKET_ID] = 0x01;
				}
			}
			else if(currentCount == totalCount){
				if (check_crc == crc) {
					BuildFirmware.getInstance().insertBuffer(BuildFirmware.TYPE_GATEWAY, total_cnt, cur_cnt, fw_data);
				} else {
					ackData = new byte[M2MIndex.SIZE_FIRMWARE_GATEWAY];
					System.arraycopy(data, M2MIndex.PACKET_ID, ackData, 0, M2MIndex.FW_GW_TOTAL_BLOCK_NUM - 1);

					// block data size : 1
					ackData[M2MIndex.FW_GW_BLOCK_DATA_SIZE - M2MIndex.PACKET_ID + 1] = 0x01;
					ackData[M2MIndex.FW_GW_BLOCK_DATA - M2MIndex.PACKET_ID] = 0x01;
				}
			}

			LogMgr.d(LogMgr.TAG, "CheckReceiveData.java:analysisData:// ackData : " + ByteArrayToHex.byteArrayToHex(ackData, ackData.length));
		}
			break;
		case M2MIndex.RESULT_OPENNING:
			if (data[data.length - 3] == M2MIndex.FAIL_OPENNING) {
				mProtocolManger.updateUI(ProtocolConfig.OPENNING_FAIL);
			} else if (data[data.length - 3] == M2MIndex.SUCCESS_OPENNING) {
				mProtocolManger.updateUI(ProtocolConfig.OPENNING_SUCCESS);
				setStringPreference(SmartCarePreference.APPLICATION_SETTING_INFO_FILENAME, SmartCarePreference.OPENNING_RESULT, new SimpleDateFormat(UIConstant.OPENNING_FORMAT).format(new Date()));
			}
			ackData = new byte[M2MIndex.PACKET_MSG_ID - M2MIndex.PACKET_ID];
			System.arraycopy(data, M2MIndex.PACKET_ID, ackData, 0, M2MIndex.PACKET_MSG_ID - M2MIndex.PACKET_ID);

			break;
		case M2MIndex.FINISH_SEND:
			break;
		case M2MIndex.START_OPENNING:
			SmartCareSystemApplication.getInstance().getGlobalMsgFunc().startOpenning(null);
			break;
		case M2MIndex.FIRMWARE_UPDATE_OTHER: {
			byte crc, check_crc, msgID;

			msgID = data[M2MIndex.PACKET_MSG_ID];

			byte[] total_cnt = new byte[M2MIndex.FW_BLOCK_SIZE], cur_cnt = new byte[M2MIndex.FW_BLOCK_SIZE], size = new byte[M2MIndex.FW_DATA_SIZE];

			System.arraycopy(data, M2MIndex.FW_OTHER_BLOCK_DATA_SIZE, size, 0, M2MIndex.FW_DATA_SIZE);
			
			int data_size = ByteArrayToHex.byteArrayToInt(size);
			
			byte[] fw_data = new byte[data_size]; 
			
			System.arraycopy(data, M2MIndex.FW_OTHER_CURRENT_BLOCK_NUM, cur_cnt, 0, M2MIndex.FW_BLOCK_SIZE);
			System.arraycopy(data, M2MIndex.FW_OTHER_TOTAL_BLOCK_NUM, total_cnt, 0, M2MIndex.FW_BLOCK_SIZE);
			System.arraycopy(data, M2MIndex.FW_OTHER_BLOCK_DATA, fw_data, 0, data_size);

			crc = data[data.length - 3];
			check_crc = BuildFirmware.getInstance().checkCRC(fw_data);

			int currentCount = ByteArrayToHex.byteArrayToInt(cur_cnt);
			int totalCount = ByteArrayToHex.byteArrayToInt(total_cnt);
			
			if (currentCount == 0) {
				SmartCarePreference pref = new SmartCarePreference(SmartCareSystemApplication.getInstance().getApplicationContext(), SmartCarePreference.USER_SETTING_INFO_FILENAME);
				String version = pref.getStringValue(SmartCarePreference.ZIGBEE_VER, UIConstant.ZIGBEE_DEFAULT_VER);
				int cur_version = Integer.parseInt(version);
				int update_version = ByteArrayToHex.byteArrayToInt(fw_data);
				if (update_version > cur_version) {
					BuildFirmware.getInstance().insertBuffer(msgID, total_cnt, cur_cnt, fw_data);

					ackData = new byte[M2MIndex.SIZE_FIRMWARE_GATEWAY];
					System.arraycopy(data, M2MIndex.PACKET_ID, ackData, 0, M2MIndex.FW_OTHER_TOTAL_BLOCK_NUM - 1);

					// current 증가
					int curBlockNum = currentCount + 1;
					cur_cnt = ByteBuffer.allocate(2).putShort((short) curBlockNum).array();
					System.arraycopy(cur_cnt, 0, ackData, M2MIndex.FW_GW_CURRENT_BLOCK_NUM - M2MIndex.PACKET_ID, M2MIndex.FW_BLOCK_SIZE);

					// block data size : 1
					ackData[M2MIndex.FW_OTHER_BLOCK_DATA_SIZE - M2MIndex.PACKET_ID + 1] = 0x01;
				} else {
					ackData = new byte[M2MIndex.SIZE_FIRMWARE_INFO];
					System.arraycopy(data, M2MIndex.PACKET_ID, ackData, 0, M2MIndex.SIZE_FIRMWARE_INFO);
				}
			} else if(currentCount < totalCount){
				ackData = new byte[M2MIndex.SIZE_FIRMWARE_GATEWAY];
				System.arraycopy(data, M2MIndex.PACKET_ID, ackData, 0, M2MIndex.FW_OTHER_TOTAL_BLOCK_NUM - 1);

				// block data size : 1
				ackData[M2MIndex.FW_OTHER_BLOCK_DATA_SIZE - M2MIndex.PACKET_ID + 1] = 0x01;

				if (check_crc == crc) {
					// current 증가
					int curBlockNum = currentCount + 1;
					cur_cnt = ByteBuffer.allocate(2).putShort((short) curBlockNum).array();
					System.arraycopy(cur_cnt, 0, ackData, M2MIndex.FW_OTHER_CURRENT_BLOCK_NUM - M2MIndex.PACKET_ID, M2MIndex.FW_BLOCK_SIZE);

					BuildFirmware.getInstance().insertBuffer(msgID, total_cnt, cur_cnt, fw_data);
				} else {
					ackData[M2MIndex.FW_OTHER_BLOCK_DATA - M2MIndex.PACKET_ID] = 0x01;
				}
			}
			else if(currentCount == totalCount){
				if (check_crc == crc) {
					BuildFirmware.getInstance().insertBuffer(msgID, total_cnt, cur_cnt, fw_data);
				} else {
					ackData = new byte[M2MIndex.SIZE_FIRMWARE_GATEWAY];
					System.arraycopy(data, M2MIndex.PACKET_ID, ackData, 0, M2MIndex.FW_OTHER_TOTAL_BLOCK_NUM - 1);

					// block data size : 1
					ackData[M2MIndex.FW_OTHER_BLOCK_DATA_SIZE - M2MIndex.PACKET_ID + 1] = 0x01;
					ackData[M2MIndex.FW_OTHER_BLOCK_DATA - M2MIndex.PACKET_ID] = 0x01;
				}
			}
			
			LogMgr.d(LogMgr.TAG, "CheckReceiveData.java:analysisData:// ackData : " + ByteArrayToHex.byteArrayToHex(ackData, ackData.length));
		}
			break;
		case M2MIndex.LOG_REQUEST:
			sendLog();
			break;
		case M2MIndex.NOT_REPORTED:
			List<NotReported> list = db.selectAllNotReported();
			db.deleteAllNotReported();
			for(NotReported nrData : list){
				//payload 값만 저장하기때문에 payload값만 가져옴
				byte[] notReportedData = nrData.getContent();
				LogMgr.d(LogMgr.TAG, "CheckReceiveData notReportedData:" + notReportedData + "......list size:"+list.size());
				mProtocolManger.insertFrame(ProtocolFrame.getInstance(notReportedData));	
			}
			this.sendMessage();
			break;
		}
		
	

		return ackData;
	}

	private void sendLog() {
		LogTransfer trans = new LogTransfer();
		trans.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}

	private void checkDataRequest(byte req_packet_id, byte req_msg_id) {
		ByteBuffer bb = null;
		ConnectDB db = ConnectDB.getInstance(SmartCareSystemApplication.getInstance().getApplicationContext());
				
		String gateWayNumber = SmartCareSystemApplication.getInstance().getMyPhoneNumber();		
		Users user;
		
		if (req_packet_id == GatewayIndex.INFO_REPORT_GW_DATA) {
			switch (req_msg_id) {
			case GateWayMessage.ID_REPORT_GW_NUM:
				insertMessage(GatewayIndex.INFO_REPORT_GW_DATA, FormattedFrame.getInstance(req_msg_id, gateWayNumber.getBytes()));
				break;

			case GateWayMessage.ID_REPORT_119_NUM:
				user = selectData(db, DBInfo.HELPER_TYPE_119);
				if (user != null && user.getNumber() != null)
					insertMessage(GatewayIndex.INFO_REPORT_GW_DATA, FormattedFrame.getInstance(req_msg_id, user.getNumber().getBytes()));
				break;

			case GateWayMessage.ID_REPORT_CENTER_NUM:
				user = selectData(db, DBInfo.HELPER_TYPE_CENTER);
				if (user != null && user.getNumber() != null)
					insertMessage(GatewayIndex.INFO_REPORT_GW_DATA, FormattedFrame.getInstance(req_msg_id, user.getNumber().getBytes()));
				break;

			case GateWayMessage.ID_REPORT_HELPER_NUM:
				user = selectData(db, DBInfo.HELPER_TYPE_HELPER);
				if (user != null && user.getNumber() != null)
					insertMessage(GatewayIndex.INFO_REPORT_GW_DATA, FormattedFrame.getInstance(req_msg_id, user.getNumber().getBytes()));
				break;

			case GateWayMessage.ID_REPORT_CARE1_NUM:
				user = selectData(db, DBInfo.HELPER_TYPE_CARE1);
				if (user != null && user.getNumber() != null)
					insertMessage(GatewayIndex.INFO_REPORT_GW_DATA, FormattedFrame.getInstance(req_msg_id, user.getNumber().getBytes()));
				break;

			case GateWayMessage.ID_REPORT_CARE2_NUM:
				user = selectData(db, DBInfo.HELPER_TYPE_CARE2);
				insertMessage(GatewayIndex.INFO_REPORT_GW_DATA, FormattedFrame.getInstance(req_msg_id, user.getNumber().getBytes()));
				break;

			case GateWayMessage.ID_REPORT_M2M_ADDRESS:

				String address = getStringPreference(SmartCarePreference.ADDRESS_INFO_FILENAME, SmartCarePreference.M2M_IP) + File.pathSeparator
						+ String.valueOf(getIntPreference(SmartCarePreference.ADDRESS_INFO_FILENAME, SmartCarePreference.M2M_PORT));
				insertMessage(GatewayIndex.INFO_REPORT_GW_DATA, FormattedFrame.getInstance(GateWayMessage.ID_REPORT_M2M_ADDRESS, address.getBytes()));
				break;

			case GateWayMessage.ID_REPORT_M2M_NUMBER:
				insertMessage(GatewayIndex.INFO_REPORT_GW_DATA, FormattedFrame.getInstance(req_msg_id, gateWayNumber.getBytes()));
				break;

			case GateWayMessage.ID_REPORT_M2M_GW_NUM:
				insertMessage(GatewayIndex.INFO_REPORT_GW_DATA, FormattedFrame.getInstance(req_msg_id, gateWayNumber.getBytes()));
				break;

			case GateWayMessage.ID_REPORT_REPORT_CYCLE:
				bb = ByteBuffer.allocate(2).putShort((short) getIntPreference(SmartCarePreference.SENSOR_SETTING_INFO_FILENAME, SmartCarePreference.PERIODIC_REPORT_TIME));
				insertMessage(GatewayIndex.INFO_REPORT_GW_DATA, FormattedFrame.getInstance(req_msg_id, bb.array()));

				break;
			case GateWayMessage.ID_REPORT_REPORT_MISSED_TIME:
				bb = ByteBuffer.allocate(2).putShort((short) getIntPreference(SmartCarePreference.SENSOR_SETTING_INFO_FILENAME, SmartCarePreference.NON_ACTIVITY_TIME));
				insertMessage(GatewayIndex.INFO_REPORT_GW_DATA, FormattedFrame.getInstance(req_msg_id, bb.array()));
				break;

			case GateWayMessage.ID_REPORT_REPORT_CYCLE_TIME:
				bb = ByteBuffer.allocate(2).putShort((short) getIntPreference(SmartCarePreference.SERVER_SETTING_INFO_FILENAME, SmartCarePreference.SERVER_PERIODIC_REPORT_TIME));
				insertMessage(GatewayIndex.INFO_REPORT_GW_DATA, FormattedFrame.getInstance(req_msg_id, bb.array()));
				break;

			case GateWayMessage.ID_REPORT_MISSED_THRESHOLD:
				bb = ByteBuffer.allocate(2).putShort((short) getIntPreference(SmartCarePreference.SENSOR_SETTING_INFO_FILENAME, SmartCarePreference.NON_ACTIVITY_NUM));
				insertMessage(GatewayIndex.INFO_REPORT_GW_DATA, FormattedFrame.getInstance(req_msg_id, bb.array()));
				break;

			case GateWayMessage.ID_REPORT_ACTIVE_SENSOR_VALUE:
				bb = ByteBuffer.allocate(2).putShort((short) getIntPreference(SmartCarePreference.SENSOR_SETTING_INFO_FILENAME, SmartCarePreference.DETECT_ACTIVITY_TIME));
				insertMessage(GatewayIndex.INFO_REPORT_GW_DATA, FormattedFrame.getInstance(GateWayMessage.ID_REPORT_ACTIVE_SENSOR_VALUE, bb.array()));
				break;

			case GateWayMessage.ID_REPORT_FORCE_RECEIVE:
				bb = ByteBuffer.allocate(2).putShort((short) getIntPreference(SmartCarePreference.CALL_SETTING_INFO_FILENAME, SmartCarePreference.AUTO_RECEIVING_TIME));
				insertMessage(GatewayIndex.INFO_REPORT_GW_DATA, FormattedFrame.getInstance(req_msg_id, bb.array()));
				break;
			case GateWayMessage.ID_REPORT_FINISH_SEND:
				break;
			}
		}
		else if (req_packet_id == GatewayIndex.INFO_REPORT_SENSOR_DATA){
			SensorDeviceManager mgr = SmartCareSystemApplication.getInstance().getSensorDeviceManager();
			MySmartCareState state = SmartCareSystemApplication.getInstance().getMySmartCareState();
			
			for(Sensor sensor : mgr.getSensorList()){				
				int sensortype = sensor.getType();
				if(sensortype == SensorID.ACTIVITY_SENSOR_TYPE.getSensorType()){					
					mProtocolManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_SENSOR_DATA, sensor, null));
				}
				else if(sensortype == SensorID.DOOR_SENSOR_TYPE.getSensorType()){
					mProtocolManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_SENSOR_DATA, sensor, null));
				}
				else if(sensortype == SensorID.FIRE_SENSOR_TYPE.getSensorType()){
					int[] per_cnt = new int[sensor.getSensor_data().length];
					per_cnt[0] = state.getAlertFire() == true ? 1: 0;
					sensor.setSensor_data(per_cnt);
					mProtocolManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_SENSOR_DATA, sensor, null));
				}
				else if(sensortype == SensorID.GAS_SENSOR_TYPE.getSensorType()){
					int[] per_cnt = new int[sensor.getSensor_data().length];
					per_cnt[0] = state.getAlertGas() == true ? 1: 0;
					sensor.setSensor_data(per_cnt);
					mProtocolManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_SENSOR_DATA, sensor, null));
				}
				else if(sensortype == SensorID.CALL_EMERGENCY_SENSOR_TYPE.getSensorType()){
					mProtocolManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_SENSOR_DATA, sensor, null));
					int[] per_cnt = new int[sensor.getSensor_data().length];
					if(state.getAlertFire() || state.getAlertGas())
						per_cnt[0] = 0x01;
					sensor.setSensor_data(per_cnt);
					mProtocolManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_SENSOR_DATA, sensor, null));
				}
				else if(sensortype == SensorID.GAS_BREAKER_TYPE.getSensorType()){
					mProtocolManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_SENSOR_DATA, sensor, null));
				}
			}
			Set<BluetoothDevice> pairedDevices = BluetoothAdapter.getDefaultAdapter().getBondedDevices();
    		BluetoothDevice bloodPressure = null;
    		for (BluetoothDevice device : pairedDevices) {
    			if(device.getBluetoothClass().getDeviceClass() == BluetoothClass.Device.HEALTH_BLOOD_PRESSURE)
    				bloodPressure = device;
    		}
			if (bloodPressure != null) {
				String mac = bloodPressure.getAddress().replaceAll(":", " ");
				Sensor sensor = new Sensor(SensorID.getSensorId(SensorID.BLOOD_PRESURE_TYPE.getSensorType(), 0x01), mac);
				sensor.setComm_status(1);
				sensor.setSensor_batt(100);

				mProtocolManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_SENSOR_DATA, sensor, null));
			}
		}
		sendMessage();
	}
	
	private void checkLogAndMissed(byte req_packet_id, byte[] start_time, byte[] end_time) {
		ConnectDB db = ConnectDB.getInstance(SmartCareSystemApplication.getInstance().getApplicationContext());
		switch(req_packet_id){
		case M2MMessage.ID_REQUEST_CYCLE_LOG:
		case M2MMessage.ID_REQUEST_EVENT_LOG: 
		{
			List<MessageHistory> messageList = db.selectMessageHistory(new Date(ConvertTime.getLongTime(start_time)), new Date(ConvertTime.getLongTime(end_time)), 0);

			int cycleCount = 0, cycleSize = 0;
			int eventCount = 0, eventSize = 0;

			for (MessageHistory history : messageList) {
				int messageType = history.getTYPE();
				if (messageType == GatewayIndex.INFO_REPORT_CYCLE) {
					cycleCount++;
					cycleSize += history.getDATA().length;
				} else if (messageType == GatewayIndex.INFO_REPORT_GW_EVENT || messageType == GatewayIndex.INFO_REPORT_SENSOR_EVENT) {
					eventCount++;
					eventSize += history.getDATA().length;
				}
			}

			byte[] sub_data = new byte[12];

			byte[] cycle_count = getByteArrayTime(cycleCount, GatewayIndex.LENGTH_LOG_COUNT);
			byte[] cycle_size = getByteArrayTime(cycleSize, GatewayIndex.LENGTH_LOG_SIZE);
			byte[] event_count = getByteArrayTime(eventCount, GatewayIndex.LENGTH_LOG_COUNT);
			byte[] event_size = getByteArrayTime(eventSize, GatewayIndex.LENGTH_LOG_SIZE);

			int index = 0;
			System.arraycopy(cycle_count, 0, sub_data, index, cycle_count.length);
			index =+ cycle_count.length;
			System.arraycopy(cycle_size, 0, sub_data, index, cycle_size.length);
			index =+ cycle_size.length;
			System.arraycopy(event_count, 0, sub_data, index, event_count.length);
			index =+ event_count.length;
			System.arraycopy(event_size, 0, sub_data, index, event_size.length);

			LogMgr.d(LogMgr.TAG, "CheckReceiveData.java:checkLogAndMissed:// index : " + index);

			FormattedFrame ff = FormattedFrame.getInstance(GatewayIndex.INFO_RESULT_CHECK_GW, sub_data);
			insertMessage(GatewayIndex.INFO_RESULT_CHECK_GW, ff);
			sendMessage();
		}
			break;
		case M2MMessage.ID_REQUEST_MISSED_DATA: 
		{
			Date start = new Date(ConvertTime.getLongTime(start_time));
			Date end = new Date(ConvertTime.getLongTime(end_time));
			
			insertActivityData(start, end, GateWayMessage.ID_WEEK_MISSED_ACTIVITY_DATA);
			
			sendMessage();
		}
			break;
		}
	}
	
	private void insertActivityData(Date start_time, Date end_time, byte msg_id){
		ConnectDB db = ConnectDB.getInstance(SmartCareSystemApplication.getInstance().getApplicationContext());
		LogMgr.d(LogMgr.TAG, "CheckReceiveData.java:insertActivityData:// start_time : " + start_time);
		LogMgr.d(LogMgr.TAG, "CheckReceiveData.java:insertActivityData:// end_time : " + end_time);

		List<ActivityData> activityList = db.selectActivityData(start_time, end_time);
		SensorDeviceManager mgr = SmartCareSystemApplication.getInstance().getSensorDeviceManager();
		SmartCareSystemApplication.getInstance().getSensorDeviceManager();
		List<Sensor> sensorList = mgr.getSensorList(SensorID.ACTIVITY_SENSOR_TYPE.getSensorType());
		sensorList.addAll(mgr.getSensorList(SensorID.DOOR_SENSOR_TYPE.getSensorType()));
		
		LogMgr.d(LogMgr.TAG, "CheckReceiveData.java:insertActivityData:// sensorList.size() : " + sensorList.size());
		LogMgr.d(LogMgr.TAG, "CheckReceiveData.java:insertActivityData:// activityList.size() : " + activityList.size());

		for(Sensor sensor : sensorList){
			int[] per_cnt = new int[sensor.getSensor_data().length];

			int count = 0;
			for(ActivityData activity: activityList){
				if(sensor.getSensor_id().intValue() == activity.getSensor_ID().intValue()){
					per_cnt[count] = activity.getCount();
					count++;
					if(count >= sensor.getSensor_data().length){
						sensor.setSensor_data(per_cnt);
						FormattedFrame ff = FormattedFrame.getInstance(msg_id);
						mProtocolManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_WEEK_DATA, sensor, ff));
						count = 0;
					}
				}
			}
		}
		
		mProtocolManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_GW_DATA, FormattedFrame.getInstance(GateWayMessage.ID_REPORT_FINISH_SEND)));
	}
	
	private void checkWeekAndReCheck(byte msg_id){
		ConnectDB db = ConnectDB.getInstance(SmartCareSystemApplication.getInstance().getApplicationContext());
		switch(msg_id){
		case M2MMessage.ID_REQUEST_WEEK_DATA:
		{
			Calendar cal = Calendar.getInstance();
			Date end = cal.getTime();
			cal.add(Calendar.WEEK_OF_MONTH, -1);
			Date start = cal.getTime();
			
			insertActivityData(start, end, GateWayMessage.ID_WEEK_ACTIVITY_DATA);
			sendMessage();
		}	
			break;
		case M2MMessage.ID_REQUEST_CHECK_DATA:
			Calendar cal = Calendar.getInstance();
			Date endTime = cal.getTime();
			cal.add(Calendar.MONTH, -1);
			Date StartTime = cal.getTime();
			
			
			List<MessageHistory> messageList = db.selectMessageHistory(StartTime, endTime, 0);

			int cycleCount = 0, cycleSize = 0;
			int eventCount = 0, eventSize = 0;


			for (MessageHistory history : messageList) {
				int messageType = history.getTYPE();
				LogMgr.d(LogMgr.TAG, "CheckReceiveData.java:checkWeekAndReCheck:// messageType : " + messageType);
				if (messageType == GatewayIndex.INFO_REPORT_CYCLE) {
					cycleCount++;
					cycleSize += history.getDATA().length;
				} else if (messageType == GatewayIndex.INFO_REPORT_GW_EVENT || messageType == GatewayIndex.INFO_REPORT_SENSOR_EVENT) {
					eventCount++;
					eventSize += history.getDATA().length;
				}
			}

			byte[] sub_data = new byte[12];

			byte[] cycle_count = getByteArrayTime(cycleCount, GatewayIndex.LENGTH_LOG_COUNT);
			byte[] cycle_size = getByteArrayTime(cycleSize, GatewayIndex.LENGTH_LOG_SIZE);
			byte[] event_count = getByteArrayTime(eventCount, GatewayIndex.LENGTH_LOG_COUNT);
			byte[] event_size = getByteArrayTime(eventSize, GatewayIndex.LENGTH_LOG_SIZE);

			int index = 0;
			System.arraycopy(cycle_count, 0, sub_data, index, cycle_count.length);
			System.arraycopy(cycle_size, 0, sub_data, index + cycle_count.length, cycle_size.length);
			System.arraycopy(event_count, 0, sub_data, index + cycle_size.length, event_count.length);
			System.arraycopy(event_size, 0, sub_data, index + event_count.length, event_size.length);
			
			LogMgr.d(LogMgr.TAG, "CheckReceiveData.java:checkWeekAndReCheck:// sub_data : " + ByteArrayToHex.byteArrayToHex(sub_data, sub_data.length));


			FormattedFrame ff = FormattedFrame.getInstance(GatewayIndex.INFO_RESULT_CHECK_GW, sub_data);
			insertMessage(GatewayIndex.INFO_RESULT_CHECK_GW, ff);
			
			SensorDeviceManager mgr = SmartCareSystemApplication.getInstance().getSensorDeviceManager();
			for(Sensor sensor : mgr.getSensorList()){
				mProtocolManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_RESULT_CHECK_SENSOR, sensor, null));
			}
			
			mProtocolManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_GW_DATA, FormattedFrame.getInstance(GateWayMessage.ID_REPORT_FINISH_SEND)));
			sendMessage();
			break;
		}
	}

	private void insertMessage(byte messageType, FormattedFrame ff) {
		mProtocolManger.insertFrame(ProtocolFrame.getInstance(messageType, ff));
	}

	private void sendMessage() {
		mProtocolManger.messageSend(null);
	}

	private void insertData(ConnectDB con, List<Users> tempData, String userType, String number) {
		Users user = new Users();
		long id;
		if (tempData.size() == 1) {
			id = tempData.get(0).getId();
			user.setId(id);
		} else {
			con.deleteUser(new Users(null, null, null, null, userType));
		}
		user.setType(userType);
		user.setNumber(number);
		con.insertUser(user);
	}

	private Users selectData(ConnectDB con, String userType) {
		List<Users> userList = con.selectUser(new Users(null, null, null, null, userType));
		if (userList.size() == 1)
			return userList.get(0);
		else
			return null;
	}

	private void setIntPreference(String fileName, String KEY, int value) {
		SmartCarePreference pref = new SmartCarePreference(SmartCareSystemApplication.getInstance().getApplicationContext(), fileName);
		pref.setIntValue(KEY, value);
	}

	private void setStringPreference(String fileName, String KEY, String value) {
		SmartCarePreference pref = new SmartCarePreference(SmartCareSystemApplication.getInstance().getApplicationContext(), fileName);
		pref.setStringValue(KEY, value);
	}

	private int getIntPreference(String fileName, String KEY) {
		SmartCarePreference pref = new SmartCarePreference(SmartCareSystemApplication.getInstance().getApplicationContext(), fileName);
		return pref.getIntValue(KEY, 0);
	}

	private String getStringPreference(String fileName, String KEY) {
		SmartCarePreference pref = new SmartCarePreference(SmartCareSystemApplication.getInstance().getApplicationContext(), fileName);
		return pref.getStringValue(KEY, "");
	}
	
	private byte[] getByteArrayTime(int data, int size){
		byte[] returnData = new byte[size];
		
		byte[] bytes = null;
		if(size == GatewayIndex.LENGTH_LOG_COUNT)
			bytes = ByteBuffer.allocate(size).putShort((short) data).array();
		else if(size == GatewayIndex.LENGTH_LOG_SIZE)
			bytes = ByteBuffer.allocate(size).putInt(data).array();
		
		if(bytes != null) {
			for (int i = 0; i < bytes.length; i++) {
				returnData[i] = bytes[i];
			}
		}
		return returnData;
	}
	
	private AsyncCallback<File> fileDownCallback = new AsyncCallback.Base<File>() {
		@Override
		public void onResult(File result) {
			LogMgr.d(LogMgr.TAG, "ConfirmDialogUI.java:enclosing_method:// result.getAbsolutePath() : " + result.getAbsolutePath());
			LogMgr.d(LogMgr.TAG, "ConfirmDialogUI.java:enclosing_method:// result.length() : " + result.length());

			// 애플리케이션 업그레이드 '확인' 버튼을 누른 경우
			SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.START_APPLICATION_UPDATE, TextToSpeech.QUEUE_FLUSH, null);

			// 시스템 초기화시 본체 RESET 을 해야 FTDI 가 재연결됨.
			if (SmartCareSystemApplication.getInstance().getMessageManager().ftdi_isConnect()) {
				SmartCareSystemApplication.getInstance().getGlobalMsgFunc().sendGateWayMsg((byte) MessageID.CODI_RESET);
			}

			Context ctx = SmartCareSystemApplication.getInstance().getApplicationContext();

			ApplicationManager am;
			try {
				am = new ApplicationManager(ctx);
				am.installPackage(Constant.FW_APP_PATH);
			} catch (Exception e) {
				e.printStackTrace();
			}
			ActivityManager mgr = (ActivityManager) ctx.getSystemService(Activity.ACTIVITY_SERVICE);
			mgr.killBackgroundProcesses(ctx.getPackageName());
		}

		@Override
		public void exceptionOccured(Exception e) {
			// 익셉션 처리
			e.printStackTrace();
		}
	};
}
