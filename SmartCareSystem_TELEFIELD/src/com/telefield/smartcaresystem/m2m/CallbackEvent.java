package com.telefield.smartcaresystem.m2m;

public interface CallbackEvent {
	public void proccessmessage(String message);
	public void finishmessage(boolean reseult, String message);
}
