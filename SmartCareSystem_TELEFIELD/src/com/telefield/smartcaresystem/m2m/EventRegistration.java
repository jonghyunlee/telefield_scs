package com.telefield.smartcaresystem.m2m;

public class EventRegistration {
	private CallbackEvent callbackEvent;

	public EventRegistration(CallbackEvent event) {
		callbackEvent = event;
	}

	public void proccessmessage(String message) {
		callbackEvent.proccessmessage(message);
	}

	public void finishMessage(boolean result, String message) {
		callbackEvent.finishmessage(result, message);
	}
}
