package com.telefield.smartcaresystem.m2m;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;

import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.database.ConnectDB;
import com.telefield.smartcaresystem.util.ByteArrayToHex;
import com.telefield.smartcaresystem.util.LogMgr;
import com.telefield.smartcaresystem.util.SmartCarePreference;

public class SubProtocolManger extends AsyncTask<Integer, String, Integer>{

	private int sendCount = 0;
	private byte sendData[] = new byte[1024];
	Socket mSocket;

	private InputStream input;
	private OutputStream output;

	private boolean readyToSendMessage = true;
	
	private byte send_packet_id = 0;
	private byte rcv_packet_id = 0;
	
	private Timer time;	
	
	private LinkedList<ProtocolFrame> frameList;
	
	private Thread messageSend;
	
	private SubCheckReceiveData mSubCheckReceiveData;
	
	public SubProtocolManger() {
		super();
	}
	
	@Override
	protected Integer doInBackground(Integer... params) {
		messageSend = new Thread();
		frameList = new LinkedList<ProtocolFrame>();
		mSubCheckReceiveData = new SubCheckReceiveData(this);
		openServerConnect();			
		return null;
	}
	
	private synchronized void openServerConnect() {
		try {
			SmartCarePreference pref = new SmartCarePreference(SmartCareSystemApplication.getInstance().getApplicationContext(), SmartCarePreference.ADDRESS_INFO_FILENAME);
			String IP = pref.getStringValue(SmartCarePreference.M2M_IP, SmartCarePreference.DEFAULT_COMMON_STRING);
			int port = pref.getIntValue(SmartCarePreference.M2M_PORT, SmartCarePreference.DEFAULT_COMMON_INT);

			InetSocketAddress address = new InetSocketAddress(IP.equals(SmartCarePreference.DEFAULT_COMMON_STRING) ? ProtocolConfig.M2M_ADDRESS : IP,
					port == SmartCarePreference.DEFAULT_COMMON_INT ? ProtocolConfig.M2M_PORT : port);

			mSocket = new Socket();
			mSocket.setTcpNoDelay(true);
			mSocket.setKeepAlive(true);
			mSocket.connect(address, ProtocolConfig.CONNECT_TIMEOUT);

			output = mSocket.getOutputStream();
		} catch (Exception e) {
			e.printStackTrace();
			mSocket = new Socket();
			return;
		}

		if (mSocket != null && mSocket.isConnected() && !checkUpdate.isAlive()) {
			checkUpdate.start();
		}
		time = new Timer();
	}
	
	public Thread checkUpdate = new Thread() {
		@Override
		public void run() {
			try {
				input = mSocket.getInputStream();
				int size;

				while (mSocket != null && mSocket.isConnected()) {
					size = input.available();
					if (size > 0) {
						byte[] data = new byte[size];
						input.read(data);
						LogMgr.d(LogMgr.TAG, "rec data size // data : " + data.length);
						LogMgr.d(LogMgr.TAG, "rec adata : " + ByteArrayToHex.byteArrayToHex(data, data.length));
						LogMgr.writeHistory(LogMgr.GW_RECEIVE, ByteArrayToHex.byteArrayToHex(data, data.length));
						rcv_packet_id = data[GatewayIndex.PACKET_PAYLOAD];
						checkReceiveData(data);
						sleep(ProtocolConfig.READ_WAITING_TIME / 2);
					} else {
						sleep(ProtocolConfig.READ_WAITING_TIME);
					}
				}
				input.close();
			} catch (Exception ie) {
				ie.printStackTrace();
			}
			super.run();
		}
	};
	
	
	/**
	 * 서버에 전송할 메세지를 insert 한다
	 * @param ff
	 */
	public void insertFrame(ProtocolFrame ff){
		frameList.offer(ff);
	}
	
	/**
	 * LinkedList에 insert 된 메세지들을 모두 m2m서버로 전송 한다
	 * @param message UI로 전달할 메세지
	 */
	public void messageSend(final String message){
		final ConnectivityManager connMgr = (ConnectivityManager) SmartCareSystemApplication.getInstance().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
		final NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		final NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		final ConnectDB db = ConnectDB.getInstance(SmartCareSystemApplication.getInstance().getApplicationContext());

		if(!messageSend.isAlive()){
			messageSend = new Thread(new Runnable() {
				@Override
				public synchronized void run() {
					
					Iterator<ProtocolFrame> it = frameList.iterator();
					ProtocolFrame frame;
					byte[] data;
					
					try {
						if(mSocket != null && mSocket.isConnected())
							output = mSocket.getOutputStream();
					} catch (IOException e) {
						e.printStackTrace();
					}
					
					if (wifi.isAvailable() || mobile.isAvailable()){
						while(it.hasNext())
						{
							frame = frameList.poll();
							
							byte[] payload = frame.getPayload();
							LogMgr.d(LogMgr.TAG, "frameList.poll() ID : " + frame.getPacketID() + " payload : " + ByteArrayToHex.byteArrayToHex(payload, payload.length));
							
							if(frame != null){
								
								data = frame.getBytes();
								
								if(mSocket != null){
									LogMgr.d(LogMgr.TAG, "ProtocolManger.java:run// transmit : " + mSocket.isConnected());
								}
								if (mSocket == null || mSocket.isInputShutdown()|| mSocket.isOutputShutdown() || !mSocket.isConnected()) openServerConnect();
								
								boolean transmit =  mSocket.isConnected();
								
//								int packet_type = getPacketType(data[GatewayIndex.PACKET_PAYLOAD]);
//								if(packet_type > 0)
//									db.insertMessageHistory(new MessageHistory(packet_type, data.length, data, transmit, Calendar.getInstance().getTime()));
								
								if(!transmit) break;
								
								sendMessage(data, data.length);
							}
						}
					}
//					if (!mSocket.isConnected() || frameList.size() != 0) {
//						LogMgr.d(LogMgr.TAG, "ProtocolManger.java:run Connect Fail");
//						for (ProtocolFrame ff : frameList) {
//							byte[] input = ff.getBytes();
//							int packet_type = getPacketType(input[GatewayIndex.PACKET_PAYLOAD]);
//							if(packet_type > 0)
//								db.insertMessageHistory(new MessageHistory(packet_type, input.length, input, false, Calendar.getInstance().getTime()));
//						}
//					}
					init();
				}
			});
			messageSend.start();
		}
	}
	
//	private int getPacketType(byte packet_id){
//		int packet_type = -1;
//		switch(packet_id){
//		case GatewayIndex.INFO_REPORT_GW_EVENT:
//		case GatewayIndex.INFO_REPORT_SENSOR_EVENT:
//		case GatewayIndex.INFO_REPORT_CYCLE:
//			packet_type = packet_id;
//			break;
//		}
//		return packet_type;
//	}
	private void sendMessage(byte data[], int length) {
		System.arraycopy(data, 0, sendData, 0, length);
		
		LogMgr.d(LogMgr.TAG, "sendData size : " + length);
		LogMgr.d(LogMgr.TAG, "sendData : " + ByteArrayToHex.byteArrayToHex(sendData, length));
		byte packet_id = data[GatewayIndex.PACKET_PAYLOAD];
		if(packet_id != GatewayIndex.INFO_REPORT_CAM)
			LogMgr.writeHistory(LogMgr.GW_SEND, ByteArrayToHex.byteArrayToHex(data, length));
		else{
			byte[] index = new byte[GatewayIndex.CAM_LENGTH_BLOCK];
			System.arraycopy(data, GatewayIndex.CAM_CURRENT_BLOCK_NUM, index, 0, index.length);
			int cur_num = ByteArrayToHex.byteArrayToInt(index);
			if(cur_num == 0){
				LogMgr.writeHistory(LogMgr.GW_SEND, ByteArrayToHex.byteArrayToHex(data, length));
			}
		}
			
		try {
			send_packet_id = sendData[GatewayIndex.PACKET_PAYLOAD];
			output.write(sendData, 0, length);
			output.flush();
			readyToSendMessage = false;
			if (send_packet_id == GatewayIndex.INFO_REQUEST_DATA && sendCount == 0) {
				publishProgress(ProtocolConfig.OPENNING_WAIT);
				time.schedule(new TimerTask() {
					@Override
					public void run() {
						init();
					}
				}, ProtocolConfig.READ_WAITING_TIME * 10);
			}
		} catch (IOException e) {
			e.printStackTrace();
			readyToSendMessage = false;
			if (send_packet_id == GatewayIndex.INFO_REQUEST_DATA && sendCount == 0) {
				publishProgress(ProtocolConfig.OPENNING_WAIT);
				time.schedule(new TimerTask() {
					@Override
					public void run() {
						init();
					}
				}, ProtocolConfig.READ_WAITING_TIME * 10);
			}
		}
		while (sendCount < ProtocolConfig.RETRY_CNT) {
			try {
				Thread.sleep(ProtocolConfig.ACK_WAITING_TIME);
			} catch (InterruptedException e) {
			}
			if (!readyToSendMessage) {
				sendCount++;
				sendMessage(data, length);
			}
			else
				return;
		}
		LogMgr.d(LogMgr.TAG, "ProtocolManger.java:sendMessage// sendCount : " + sendCount);
		if(sendCount >= ProtocolConfig.RETRY_CNT){
			init();
		}
	}

	private void checkReceiveData(byte[] data){
		
		rcv_packet_id = data[M2MIndex.PACKET_ID];
		
		LogMgr.d(LogMgr.TAG, "ProtocolManger.java:checkReceiveData// rcv_packet_id : " + Integer.toHexString(0xFF & rcv_packet_id));
		LogMgr.d(LogMgr.TAG, "ProtocolManger.java:checkReceiveData// data[data.length -2] : " + Integer.toHexString(0xFF & data[data.length - 3]));
		
		if(rcv_packet_id == (byte)(send_packet_id | ProtocolConfig.ACK_OPERATOR)){
			readyToSendMessage = true;
			sendCount = 0;
			return;
		}
		
		mSubCheckReceiveData.analysisData(data, rcv_packet_id);
	}
	
	public void init(){
		if(messageSend != null){
			frameList.clear();
			messageSend.interrupt();
			readyToSendMessage = true;
			send_packet_id = 0;
			rcv_packet_id = 0;			
			sendCount = 0;
			if(time != null){
				time.cancel();
				time.purge();
				time = new Timer();
			}
		}
	}
	
	public void stop(){

		if(mSocket != null){
			if(output != null)
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			if(input != null)
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			try {
				mSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if(messageSend != null)
			messageSend.interrupt();
		if(checkUpdate != null)
			checkUpdate.interrupt();
	}
	
}
