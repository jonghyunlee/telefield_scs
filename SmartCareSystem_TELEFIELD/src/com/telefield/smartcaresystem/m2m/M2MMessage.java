package com.telefield.smartcaresystem.m2m;

public interface M2MMessage {
	
	/**
	 * 설정변경(sensor - on/off type)
	 */
	static final byte ID_SENSOR_CANCEL_ALARM = 0x01;
	static final byte ID_SENSOR_LED_ON = 0x02;
	static final byte ID_SENSOR_LED_OFF = 0x03;
	
	/**
	 * 설정변경(게이트웨이 설정 변경 - 전화번호등.)
	 */
	static final byte ID_SETTING_GW_NUMBER = 0x10;
	static final byte ID_SETTING_CENTER_NUMBER = 0x11;
	static final byte ID_SETTING_HELPER_NUMBER = 0x12;
	static final byte ID_SETTING_CARE1_NUMBER = 0x13;
	static final byte ID_SETTING_CARE2_NUMBER = 0x14;
	static final byte ID_SETTING_119_NUMBER = 0x15;
	static final byte ID_SETTING_M2M_ADDRESS = 0x20;
	static final byte ID_SETTING_M2M_NUMBER = 0x21;
	static final byte ID_SETTING_M2M_GW_NUMBER = 0x22;
	static final byte ID_SETTING_U119_ADDRESS = 0x23;
	static final byte ID_SETTING_REPORT_CYCLE = 0x30; // 통신 주기 변경 설정 (1~60분, Default 10분)
	static final byte ID_SETTING_REPORT_MISSED_TIME = 0x31; // 동작 미감지 시간 설정 (1~120분, Default 60분)
	static final byte ID_SETTING_REPORT_CYCLE_TIME = 0x32; // 주기적 전송시간 설정 (1~240분, Default 240분)
	static final byte ID_SETTING_MISSED_THRESHOLD = 0x33; // 미감지 threshold value (1~10회, Default 1회)
	static final byte ID_SETTING_ACTIVE_SENSOR_VALUE = 0x34; // 외출상태에서 활동량 감지되었을때 강제외출해제를 위한 활동량감지시간 설정(기본 5분)
	static final byte ID_SETTING_FORCE_RECEIVE = 0x35; // 119나 지역 캐어 센터에서 전화 연결 시 응답이 없을 때 강제 착신을 위한 응답대기시간 설정(기본 2분)
	
	static final byte ID_SETTING_OPEN_VALVE = 0x36;
	static final byte ID_SETTING_CLOSE_VALVE = 0x37;
	
	static final byte ID_SETTING_FINISH_SEND = 0x40; // 다중패킷이거나, M2M요청에 대한 응답인 경우 전송이 끝난 후 전송종료 신호로 사용
	
	
	/**
	 * data request (로그 송신 요청 및 활동량 미보고 데이터 요청)
	 */
	static final byte ID_REQUEST_CYCLE_LOG = 0x01;
	static final byte ID_REQUEST_EVENT_LOG = 0x02;
	static final byte ID_REQUEST_MISSED_DATA = 0x03;
	
	
	/**
	 * data request (활동량 1주일 데이터 및 역점검 요청) 
	 */
	static final byte ID_REQUEST_WEEK_DATA = 0x01;
	static final byte ID_REQUEST_CHECK_DATA = 0x02;
	
	
	/**
	 * F/W update(센서,APP)
	 */
	static final byte ID_FIRMWARE_SENSOR = 0x01;
	static final byte ID_FIRMWARE_APP = 0x02;
}
