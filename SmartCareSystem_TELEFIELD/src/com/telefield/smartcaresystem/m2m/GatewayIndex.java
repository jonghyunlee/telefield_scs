package com.telefield.smartcaresystem.m2m;

public interface GatewayIndex {

	static byte START_DATA = 0x02;
	static byte END_DATA = 0x03;

	// Log Buffer size
	static final int LOG_BUFFER_SIZE = 512;
	
	// Frame Header Index
	int PACKET_START = 0;
	int PACKET_LENGTH = 1;
	int PACKET_PAYLOAD = 3;

	// Sensor Data Default Index
	int PACKET_ID = 0;
	int GW_NUM = 1;
	int GW_MAC = 13;
	int GW_POWER = 21;
	int GW_BATTERY = 22;
	int TIME_TIC = 23;
	int SENSOR_ADDR = 27;
	int SENSOR_MAC = 29;
	
	// 게이트웨이 등록 Index
	int GW_FW_VER = 27;
	
	// 센서등록(화재,가스,모션,도어,돌보미) Index
	int SENSOR_REG_UNREG = 37;
	
	// 주기보고(가스,화재,도어,모션)	
	int CYCLE_SENSOR_BATTERY = 37;
	int CYCLE_SENSOR_STATUS = 38;
	int CYCLE_SENSOR_DATA = 39;
	int CYCLE_SENSOR_RSSI = 43;

	// 이벤트보고 (센서 이벤트 : 화재,가스,돌보미,재실(도어)) Index
	int EVENT_SENSOR_BATTERY = 37;
	int EVENT_SENSOR_STATUS = 38;
	int EVENT_SENSOR_DATA = 39;
	
	//이벤트보고 (센서 이벤트 : 혈압 ,맥박 보고)
	int EVENT_BLOODPRESSURE_BATTERY = 37;
	int EVENT_BLOODPRESSURE_STATUS = 38;
	int EVENT_BLOODPRESSURE_SYSTOLIC = 39;
	int EVENT_BLOODPRESSURE_DIASTOLIC = 40;
	int EVENT_BLOODPRESSURE_PULSE = 41;
	
	
	
	// 활동량 1주일 데이터 요청 결과 전송 Index
	int WEEK_MSG_ID = 27;
	int WEEK_SENSOR_ADDR = 28;
	int WEEK_SENSOR_MAC = 30;
	int WEEK_SENSOR_BATTERY = 38;
	int WEEK_SENSOR_STATUS = 39;
	int WEEK_SENSOR_DATA = 40;
	

	// 활동량 미보고 데이터 요청 결과 전송 Index
	int MISSED_SENSOR_STATUS = 37;
	int MISSED_SENSOR_BATTERY = 38;
	int MISSED_SENSOR_DATA_LENGTH = 39;
	int MISSED_SENSOR_DATA_TIME = 41;
	int MISSED_SENSOR_DATA = 45;
	
	// 센서정보(가스,화재,도어,모션. 요청시 전송) Index
	int REPORT_SENSOR_BATTERY = 37;
	int REPORT_SENSOR_STATUS = 38;
	int REPORT_SENSOR_DATA = 39;
	
	// 이벤트보고 (게이트웨이 이벤트) Index
	int EVENT_GW_MSG_ID = 27;
	int EVENT_GW_MSG_DATA = 28;
	
	// 게이트웨이 정보(요청시 전송) Index
	int REPORT_GW_MSG_ID = 27;
	int REPORT_GW_MSG_LENGTH = 28;
	
	// 역점검 요청 정보 결과 송신 (센서) Index
	int REPORT_CHECK_SENSOR_STATUS = 37;
	
	// 역점검 요청 정보 결과 송신 (게이트웨이) Index
	static final int CYCLE_LOG_COUNT = 30;
	static final int CYCLE_LOG_SIZE = 33;
	static final int EVENT_LOG_COUNT = 36;
	static final int EVENT_LOG_SIZE = 38;
	
	// CAM data(화재/가스 이벤트 발생시 전송) Index
	static final int CAM_CURRENT_BLOCK_NUM = 21;
	static final int CAM_TOTAL_BLOCK_NUM = 23;	
	static final int CAM_BLOCK_DATA_SIZE = 25;
	static final int CAM_BLOCK_DATA = 26;
	
	
	// CAM data(화재/가스 이벤트 발생시 전송) Packet Length
	static final int CAM_LENGTH_BLOCK = 2;
	static final int CAM_LENGTH_DATA = 64;
	
	// Log Data Index
	static final int LOG_BLOCK_DATA_SIZE = 25;
	static final int LOG_BLOCK_DATA = 27;
	
	// 역점검 요청 정보 결과 송신 (게이트웨이) Packet Length
	static final int LENGTH_LOG_COUNT = 2;
	static final int LENGTH_LOG_SIZE = 4;
	
	// GW -> M2M payload size
	static final int SIZE_REGISTER_GW = 30;
	static final int SIZE_REPORT_GW_DATA = 29;
	static final int SIZE_REGISTER_SENSOR = 38;
	static final int SIZE_REPORT_CYCLE = 43 + 1; // rssi감도를 넣기로 함.
	static final int SIZE_REPORT_SENSOR_EVENT = 40;
	static final int SIZE_REPORT_GW_EVENT = 29;
	static final int SIZE_RESULT_CHECK_GW = 42;
	static final int SIZE_RESULT_CHECK_SENSOR = 38;
	static final int SIZE_REPORT_WEEK_DATA = 44;
	static final int SIZE_REPORT_SENSOR_DATA = 43;
	static final int SIZE_REQUEST_TIME = 13;
	static final int SIZE_REPORT_CAM = 27;
	static final int SIZE_REQUEST_DATA = 21;
	static final int SIZE_REPORT_BLOODPRESSURE_DATA = 42;
	static final int SIZE_REPORT_LOG = 28;
	static final int SIZE_REPORT_REALTIME_LOG = 25;
	
	/**
	 * 게이트웨이 등록
	 */
	final static byte INFO_REGISTER_GW = 0x01;
	/**
	 * 게이트웨이 정보(요청시 전송)
	 */
	final static byte INFO_REPORT_GW_DATA = 0x02;
	/**
	 * 센서등록(화재,가스,모션,도어,돌보미)
	 */
	final static byte INFO_REGISTER_SENSOR = 0x03;
	/**
	 * 주기보고(가스,화재,도어,모션)
	 */
	final static byte INFO_REPORT_CYCLE = 0x04;
	/**
	 * 이벤트보고 (센서 이벤트 : 화재,가스,돌보미,재실(도어))
	 */
	final static byte INFO_REPORT_SENSOR_EVENT = 0x05;
	/**
	 * 이벤트보고 (게이트웨이 이벤트)
	 */
	final static byte INFO_REPORT_GW_EVENT = 0x0B;
	/**
	 * 역점검 요청 정보 결과 송신 (게이트웨이)
	 */
	final static byte INFO_RESULT_CHECK_GW = 0x10;
	/**
	 * 역점검 요청 정보 결과 송신 (센서) 
	 */
	final static byte INFO_RESULT_CHECK_SENSOR = 0x11;
	/**
	 * 활동량 1주일 데이터 요청 결과 전송 및 활동량 미보고 데이터 전송
	 */
	final static byte INFO_REPORT_WEEK_DATA = 0x13;
	/**
	 * 센서정보(가스,화재,도어,모션. 요청시 전송)
	 */
	final static byte INFO_REPORT_SENSOR_DATA = 0x06;
	/**
	 * CAM data(화재/가스 이벤트 발생시 전송)
	 */
	final static byte INFO_REPORT_CAM = 0x07;
	/**
	 * 시간 정보 요청
	 */
	final static byte INFO_REQUEST_TIME = 0x08;
	/**
	 * 데이터 요청
	 */
	final static byte INFO_REQUEST_DATA = 0x0F;
	/**
	 * 실시간으로 로그전송
	 */
	final static byte INFO_REQUEST_REALTIME_LOG = (byte) 0xFF;
	/**
	 * 이벤트보고 (센서 이벤트 : 혈압 ,맥박 보고)
	 */
	final static byte INFO_REPORT_BLOODPRESSURE_DATA = (byte) 0xA5;
	
	
	final static byte INFO_LOG_DATA = (byte) 0xA7;
	
}
