package com.telefield.smartcaresystem.m2m;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.math.BigInteger;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.Calendar;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import com.telefield.smartcaresystem.Constant;
import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.database.ConnectDB;
import com.telefield.smartcaresystem.database.MessageHistory;
import com.telefield.smartcaresystem.database.NotReported;
import com.telefield.smartcaresystem.util.ByteArrayToHex;
import com.telefield.smartcaresystem.util.LogMgr;
import com.telefield.smartcaresystem.util.SmartCarePreference;

public class ProtocolManger extends AsyncTask<Integer, String, Integer> {

	private int sendCount = 0;
	private int connectCount = 0;
	private byte sendData[] = new byte[1024];
	Socket mSocket;

	private InputStream input;
	private OutputStream output;

	private boolean readyToSendMessage = true;
	private boolean finishSend = false;

	private  byte send_packet_id = 0;
	private byte rcv_packet_id = 0;

	private Timer time;

	private LinkedList<ProtocolFrame> frameList;

	private LinkedList<ProtocolFrame> emergencyList;

	private EventRegistration mEventRegistration;

	private Thread messageSend, checkUpdate;

	private CheckReceiveData mCheckReceiveData;

	private String failReason;
	
	private boolean isConnect = false;

	private boolean isRunningOpenning = true;
	
	public ProtocolManger() {
		super();
	}

	public void setEventRegistration(EventRegistration mEventRegistration) {
		this.mEventRegistration = mEventRegistration;
	}

	@Override
	protected void onProgressUpdate(String... progress) {
		String message = progress[0];
		LogMgr.d(LogMgr.TAG, "onProgressUpdate ... " + message);
		if (message.equals(ProtocolConfig.OPENNING_FAIL)) {
			if (mEventRegistration != null) {
				mEventRegistration.finishMessage(false, failReason);
				isRunningOpenning = false;
			}
			mEventRegistration = null;
		} else if (message.equals(ProtocolConfig.OPENNING_SUCCESS)) {
			if (mEventRegistration != null) {
				mEventRegistration.finishMessage(true, message);
				isRunningOpenning = false;
			}
			mEventRegistration = null;
		} else if (message.equals(ProtocolConfig.OPENNING_CANCEL)) {
			if (mEventRegistration != null) {
				isRunningOpenning = false;
			}
			mEventRegistration = null;
		} else {
			if (mEventRegistration != null) {
				mEventRegistration.proccessmessage(message);
			}
		}
		super.onProgressUpdate(progress);
	}

	@Override
	protected Integer doInBackground(Integer... params) {
		messageSend = new Thread();
		checkUpdate = new Thread();
		frameList = new LinkedList<ProtocolFrame>();
		emergencyList = new LinkedList<ProtocolFrame>();
		mCheckReceiveData = new CheckReceiveData(this);

		if (!Constant.IS_RELEASE_VERSION) {
			SmartCarePreference appPref = new SmartCarePreference(SmartCareSystemApplication.getInstance().getApplicationContext(), SmartCarePreference.APPLICATION_SETTING_INFO_FILENAME);

			if (appPref.getStringValue(SmartCarePreference.OPENNING_RESULT, null) != null) {
				openServerConnect();
			}
		}

		return null;
	}

	private synchronized void openServerConnect() {

		if (mSocket != null) {
			try {
				mSocket.close();
				mSocket = null;
			} catch (Exception e) {
			}
		}

		try {
			SmartCarePreference pref = new SmartCarePreference(SmartCareSystemApplication.getInstance().getApplicationContext(), SmartCarePreference.ADDRESS_INFO_FILENAME);
			String IP = pref.getStringValue(SmartCarePreference.M2M_IP, SmartCarePreference.DEFAULT_COMMON_STRING);
			int port = pref.getIntValue(SmartCarePreference.M2M_PORT, SmartCarePreference.DEFAULT_COMMON_INT);

			InetSocketAddress address = new InetSocketAddress(IP.equals(SmartCarePreference.DEFAULT_COMMON_STRING) ? ProtocolConfig.M2M_ADDRESS : IP,
					port == SmartCarePreference.DEFAULT_COMMON_INT ? ProtocolConfig.M2M_PORT : port);

			LogMgr.d(LogMgr.TAG, "ProtocolManger.java:openServerConnect:// address : " + address);
			mSocket = new Socket();
			mSocket.setTcpNoDelay(true);
			mSocket.connect(address, ProtocolConfig.CONNECT_TIMEOUT);

			output = mSocket.getOutputStream();

		} catch (Exception e) {
			if(e.fillInStackTrace() instanceof SocketTimeoutException || e.fillInStackTrace() instanceof ConnectException) {
				while (connectCount < ProtocolConfig.RETRY_CNT) {
					LogMgr.d(LogMgr.TAG, "SocketTimeoutException retryCount....." + connectCount);
					connectCount++;
					openServerConnect();
					return;
				}
			}
			
			e.printStackTrace();
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			e.printStackTrace(new PrintStream(out));
			LogMgr.writeHistory(LogMgr.EXCEPTION, out.toString());
			publishProgress(ProtocolConfig.OPENNING_FAIL);
			stop();
		}

		if (mSocket != null && mSocket.isConnected()) {
			updateReader();
		}
		time = new Timer();
	}

	private void updateReader() {
		if (!Constant.IS_RELEASE_VERSION && checkUpdate != null) {
			if (checkUpdate.interrupted()) {
				return;
			}
		}
		if (!checkUpdate.isAlive()) {
			checkUpdate = new Thread(new Runnable() {

				@Override
				public void run() {
					try {
						input = mSocket.getInputStream();
						int size;

						while (mSocket != null && mSocket.isConnected() && !checkUpdate.interrupted()) {
							size = input.available();
							if (size > 0) {
								byte[] data = new byte[size];
								input.read(data);
								LogMgr.d(LogMgr.TAG, "rec data size // data : " + data.length);
								LogMgr.d(LogMgr.TAG, "rec adata : " + ByteArrayToHex.byteArrayToHex(data, data.length));
								LogMgr.writeHistory(LogMgr.GW_RECEIVE, ByteArrayToHex.byteArrayToHex(data, data.length));
								rcv_packet_id = data[GatewayIndex.PACKET_PAYLOAD];
								checkReceiveData(data);
								Thread.sleep(ProtocolConfig.READ_WAITING_TIME / 2);
							} else {
								Thread.sleep(ProtocolConfig.READ_WAITING_TIME);
							}
						}
					} catch (Exception ie) {
					}
				}
			});
			checkUpdate.start();
		}
	}

	/**
	 * 서버에 전송할 메세지를 insert 한다
	 * 
	 * @param ff
	 */
	public void insertFrame(ProtocolFrame ff) {
		if (Constant.IS_RELEASE_VERSION && finishSend) {
			finishSend = false;
			time.cancel();
			time = new Timer();
		}
		frameList.offer(ff);
	}

	public void insertEmergencyFrame(ProtocolFrame ff) {
		if (Constant.IS_RELEASE_VERSION && finishSend) {
			finishSend = false;
			time.cancel();
			time = new Timer();
		}
		emergencyList.offer(ff);
	}

	public void clearEmergenchMessage() {
		emergencyList.clear();
	}

	public void emergencyMessageSend() {
		if (emergencyList.size() > 0) {
			frameList.addAll(emergencyList);
			emergencyList.clear();
			messageSend(null);
		}
	}

	/**
	 * LinkedList에 insert 된 메세지들을 모두 m2m서버로 전송 한다
	 * 
	 * @param message
	 *            UI로 전달할 메세지
	 */
	public void messageSend(final String message) {
		SmartCarePreference pref = new SmartCarePreference(SmartCareSystemApplication.getInstance().getApplicationContext(), SmartCarePreference.APPLICATION_SETTING_INFO_FILENAME);

		
		 if (pref.getStringValue(SmartCarePreference.OPENNING_RESULT, null) == null && frameList.size() > 0) {
			ProtocolFrame frame = frameList.getFirst();
			LogMgr.d(LogMgr.TAG, "ProtocolManger.java:messageSend:// frame.getPacketID() : " + frame.getPacketID());
			if (frame.getPacketID() != GatewayIndex.INFO_REQUEST_TIME && !isRunningOpenning) {
				init();
				return;
			}
		}
		 
		if (message != null && mEventRegistration != null){
			publishProgress(ProtocolConfig.OPENNING_START);
			isRunningOpenning = true;
		}

		final ConnectivityManager connMgr = (ConnectivityManager) SmartCareSystemApplication.getInstance().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
		final NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		final NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		final ConnectDB db = ConnectDB.getInstance(SmartCareSystemApplication.getInstance().getApplicationContext());

		if (!messageSend.isAlive()) {
			messageSend = new Thread(new Runnable() {
				@Override
				public synchronized void run() {

					Iterator<ProtocolFrame> it = frameList.iterator();
					ProtocolFrame frame;
					byte[] data;

					try {
						if (mSocket == null || !mSocket.isConnected())
							openServerConnect();
						else
							output = mSocket.getOutputStream();
					} catch (IOException e) {
						ByteArrayOutputStream out = new ByteArrayOutputStream();
						e.printStackTrace(new PrintStream(out));
						LogMgr.writeHistory(LogMgr.EXCEPTION, out.toString());
					}

					if (wifi.isAvailable() || mobile.isAvailable()) {
						while (it.hasNext()) {
							frame = frameList.poll();

							if (frame != null) {
								finishSend = false;

								data = frame.getBytes();

								boolean transmit = mSocket.isConnected();

//								int packet_type = getPacketType(data[GatewayIndex.PACKET_PAYLOAD]);
//								if (packet_type > 0)
								db.insertMessageHistory(new MessageHistory((int)(data[GatewayIndex.PACKET_PAYLOAD]), data.length, data, transmit, Calendar.getInstance().getTime()));

								if (!transmit) {
									if (mEventRegistration != null) {
										failReason = ProtocolConfig.FAIL_CANNOT_CONNECT;
										stop();
									}
									break;
								}

								if (frame.isACK()) {
									sendMessage(data, data.length, frame.isACK());
								} else {
									sendMessage(data, data.length, frame.isACK());
								}
							}
						}
					}

					LogMgr.d(LogMgr.TAG, "ProtocolManger.java:run:// frameList.size() : " + frameList.size());

					if (Constant.IS_RELEASE_VERSION) {
						if (frameList.size() != 0) {
							LogMgr.d(LogMgr.TAG, "ProtocolManger.java:run Connect Fail");
							synchronized (frameList) {
								for (ProtocolFrame ff : frameList) {
									byte[] input = ff.getBytes();
//									int packet_type = getPacketType(input[GatewayIndex.PACKET_PAYLOAD]);
//									if (packet_type > 0)
									byte[] payload = ByteArrayToHex.getPayload(input);
									db.insertnotReportedDao(new NotReported(null, payload));
									db.insertMessageHistory(new MessageHistory((int)(input[GatewayIndex.PACKET_PAYLOAD]), input.length, input, false, Calendar.getInstance().getTime()));
									stop();
								}
							}
						} else {
							finishSend = true;
							time.schedule(new TimerTask() {
								@Override
								public void run() {
									LogMgr.d(LogMgr.TAG, "ProtocolManger.java:run:// frameList.size() : " + frameList.size());
									LogMgr.d(LogMgr.TAG, "ProtocolManger.java:messageSend:// finishSend : " + finishSend);
									if (finishSend) {
										stop();
									}
								}
							}, ProtocolConfig.DATA_WAITING_TIME);
						}
					} else {
						if (!mSocket.isConnected() || frameList.size() != 0) {
							LogMgr.d(LogMgr.TAG, "ProtocolManger.java:run Connect Fail");
							for (ProtocolFrame ff : frameList) {
								byte[] input = ff.getBytes();
//								int packet_type = getPacketType(input[GatewayIndex.PACKET_PAYLOAD]);
//								if (packet_type > 0)
								byte[] payload = ByteArrayToHex.getPayload(input);
								db.insertnotReportedDao(new NotReported(null, payload));
								db.insertMessageHistory(new MessageHistory((int)(input[GatewayIndex.PACKET_PAYLOAD]), input.length, input, false, Calendar.getInstance().getTime()));
							}
						}
						init();
					}

				}
			});
			messageSend.start();
		}
	}

//	private int getPacketType(byte packet_id) {
//		int packet_type = 255;
//		switch (packet_id) {
//		case GatewayIndex.INFO_REPORT_GW_EVENT:
//		case GatewayIndex.INFO_REPORT_SENSOR_EVENT:
//		case GatewayIndex.INFO_REPORT_CYCLE:
//			packet_type = packet_id;
//			break;
//		}
//		return packet_type;
//	}
	
	private void sendMessage(byte data[], int length, boolean isACK) {
		System.arraycopy(data, 0, sendData, 0, length);

		LogMgr.d(LogMgr.TAG, "sendData size : " + length);
		LogMgr.d(LogMgr.TAG, "sendData : " + ByteArrayToHex.byteArrayToHex(sendData, length));
		byte packet_id = data[GatewayIndex.PACKET_PAYLOAD];
		if (packet_id != GatewayIndex.INFO_REPORT_CAM)
			LogMgr.writeHistory(LogMgr.GW_SEND, ByteArrayToHex.byteArrayToHex(data, length));
		else {
			byte[] index = new byte[GatewayIndex.CAM_LENGTH_BLOCK];
			System.arraycopy(data, GatewayIndex.CAM_CURRENT_BLOCK_NUM, index, 0, index.length);
			int cur_num = ByteArrayToHex.byteArrayToInt(index);
			if (cur_num == 0) {
				LogMgr.writeHistory(LogMgr.GW_SEND, ByteArrayToHex.byteArrayToHex(data, length));
			}
		}

		try {
			send_packet_id = sendData[GatewayIndex.PACKET_PAYLOAD];
			if(output != null && sendData != null){
				output.write(sendData, 0, length);
				output.flush();
				readyToSendMessage = false;
				if (send_packet_id == GatewayIndex.INFO_REQUEST_DATA && sendCount == 0 && mEventRegistration != null) {
					failReason = ProtocolConfig.FAIL_NOT_RESPONSE;
					publishProgress(ProtocolConfig.OPENNING_WAIT);
					if (!Constant.IS_RELEASE_VERSION) {
						time.schedule(new TimerTask() {
							@Override
							public void run() {
								if (!readyToSendMessage) {
									failReason = ProtocolConfig.FAIL_NOT_RESPONSE;
									stop();
								}
							}
						}, ProtocolConfig.READ_WAITING_TIME * 10);
					}
				}
			}
		} catch (IOException e) {
			LogMgr.d(LogMgr.TAG, "ProtocolManger.java:sendMessage:// e.getMessage() : " + e.getMessage());
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			e.printStackTrace(new PrintStream(out));
			LogMgr.writeHistory(LogMgr.EXCEPTION, out.toString());
//			if(e.fillInStackTrace() instanceof SocketException) {
//				readyToSendMessage = true;
//				openServerConnect();
//			}else{
//				
			failReason = ProtocolConfig.FAIL_CANNOT_CONNECT;
			readyToSendMessage = true;
			sendCount = 0;
			openServerConnect();
			stop();
//			}
		}
		if (!isACK) {
			while (sendCount < ProtocolConfig.RETRY_CNT) {
				try {
					if (send_packet_id == GatewayIndex.INFO_REQUEST_DATA && sendCount == 0 && mEventRegistration != null)
						Thread.sleep(10000);
					else
						Thread.sleep(ProtocolConfig.ACK_WAITING_TIME);
				} catch (InterruptedException e) {
				}
				if (!readyToSendMessage) {
					sendCount++;
					sendMessage(data, length, isACK);
				} else
					return;
			}
			LogMgr.d(LogMgr.TAG, "ProtocolManger.java:sendMessage// sendCount : " + sendCount);
			if (sendCount >= ProtocolConfig.RETRY_CNT) {
				failReason = ProtocolConfig.FAIL_NOT_RESPONSE;
				stopSending(length);
			}
		}
	}
	/**
	 * 서버에서 전송받은 데이터를 검사한다.
	 * @param data
	 */
	private void checkReceiveData(byte[] data) {
		rcv_packet_id = data[M2MIndex.PACKET_ID];

		LogMgr.d(LogMgr.TAG, "ProtocolManger.java:checkReceiveData// rcv_packet_id : " + Integer.toHexString(0xFF & rcv_packet_id));
		LogMgr.d(LogMgr.TAG, "ProtocolManger.java:checkReceiveData// data[data.length -2] : " + Integer.toHexString(0xFF & data[data.length - 3]));
 
		if(send_packet_id == GatewayIndex.INFO_REQUEST_TIME && rcv_packet_id == (byte) (send_packet_id | ProtocolConfig.ACK_OPERATOR)){
			isConnect = true;
		}
		if (rcv_packet_id == (byte) (send_packet_id | ProtocolConfig.ACK_OPERATOR)) {
			readyToSendMessage = true;
			sendCount = 0;
			if (send_packet_id == GatewayIndex.INFO_REPORT_GW_DATA && mEventRegistration != null) {
				publishProgress(ProtocolConfig.OPENNING_FINISH_SEND);
			}
			return;
		} else if (send_packet_id == GatewayIndex.INFO_REQUEST_DATA) {
			readyToSendMessage = true;
			sendCount = 0;
		}

		byte[] ackdata = mCheckReceiveData.analysisData(data, rcv_packet_id);
		if (ackdata != null)
			sendACK(ackdata);
	}

	public void sendACK(byte[] data) {
		data[GatewayIndex.PACKET_ID] = (byte) (data[GatewayIndex.PACKET_ID] | ProtocolConfig.ACK_OPERATOR);
		LogMgr.d(LogMgr.TAG, "ProtocolManger.java:sendACK// data : " + ByteArrayToHex.byteArrayToHex(data, data.length));
		insertFrame(ProtocolFrame.getInstance(data, true));
		messageSend(null);
	}
	
	public void stopSending(int length) {
//		Log.w(LogMgr.TAG, "ProtocolManger.java:stopSending trace : ", new Exception("STACK TRACE"));
		//보내지 못한데이터의 페이로드만 저장한다.
		ConnectDB con = ConnectDB.getInstance(SmartCareSystemApplication
				.getInstance().getApplicationContext());
		byte[] payload = ByteArrayToHex.getPayload(sendData, length);
		//최초에 LIST에서 pop해가기 때문에 빼간 senddata를 db에 추가한다.
		con.insertnotReportedDao(new NotReported(null, payload));
		LogMgr.d(LogMgr.TAG, "stopSending sendData .....");
		LogMgr.d(LogMgr.TAG, "stopSending frameList size : " + frameList.size());
		for(ProtocolFrame list:frameList){
			payload = ByteArrayToHex.getPayload(list.getBytes());
			con.insertnotReportedDao(new NotReported(null, payload));
		}
		
		init();
		if (mEventRegistration != null)
			publishProgress(ProtocolConfig.OPENNING_FAIL);
	}

	public void updateUI(String message) {
		publishProgress(message);
	}
	
	
	public void init() {
//		Log.w(LogMgr.TAG, "ProtocolManger.java:init trace : ", new Exception("STACK TRACE"));
		 LogMgr.d(LogMgr.TAG, "ProtocolManger.java: init Call");
		 
		if (messageSend != null) {
			frameList.clear();
			messageSend.interrupt();
			readyToSendMessage = true;
			send_packet_id = 0;
			rcv_packet_id = 0;
			sendCount = 0;
			finishSend = false;
			connectCount = 0;
		}
	}

//	public void initstopsend() {
////		Log.w(LogMgr.TAG, "ProtocolManger.java:init trace : ", new Exception("STACK TRACE"));
////		 LogMgr.d(LogMgr.TAG, "ProtocolManger.java: initstopsend Call");
//
//		if (messageSend != null) {
//			frameList.clear();
//			messageSend.interrupt();
//			readyToSendMessage = true;
//			send_packet_id = 0;
//			rcv_packet_id = 0;
//			sendCount = 0;
//			finishSend = false;
//			connectCount = 0;
//		}
//	}

	public void stop() {
		LogMgr.d(LogMgr.TAG, "ProtocolManger.java:stop:// ");
		init();
		if (mSocket != null) {
			if (output != null)
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			if (input != null)
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			try {
				mSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			output = null;
			input = null;
			mSocket = null;
		}
		if (messageSend != null)
			messageSend.interrupt();
		if (checkUpdate != null)
			checkUpdate.interrupt();
	}

	@Override
	protected void onCancelled() {
		LogMgr.d(LogMgr.TAG, "ProtocolManger.java:onCancelled:// onCancelled");
		super.onCancelled();
	}

	@Override
	protected void onCancelled(Integer result) {
		LogMgr.d(LogMgr.TAG, "ProtocolManger.java:onCancelled:// result : " + result.intValue());

		super.onCancelled(result);
	}

	@Override
	protected void onPostExecute(Integer result) {
		LogMgr.d(LogMgr.TAG, "ProtocolManger.java:onPostExecute:// result : " + result);
		super.onPostExecute(result);
	}

	public boolean isConnect() {
		boolean result = isConnect;
		isConnect = false;
		return result;
	}

	public boolean isRunningOpenning() {
		return isRunningOpenning;
	}
	
}
