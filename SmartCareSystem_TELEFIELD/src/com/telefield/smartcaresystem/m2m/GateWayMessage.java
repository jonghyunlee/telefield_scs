package com.telefield.smartcaresystem.m2m;

public interface GateWayMessage {

	
	/**
	 * 이벤트보고 (센서 이벤트 : 화재,가스,돌보미,재실(도어)) Sensor Data 
	 */
	final static byte DATA_WARN_ALERT = 0x01;
	final static byte DATA_CANCEL_ALERT = 0x02;
	final static byte DATA_COME_CARE = 0x04;
	final static byte DATA_OUT_CARE = 0x08;
	final static byte DATA_IN_ROOT = 0x10;
	final static byte DATA_OUT_ROOT = 0x20;
	final static byte DATA_DETECT_ACTIVITY = 0x40;
	final static byte DATA_DETECT_MISSED_ACTIVITY = (byte) 0x80;
	final static byte DATA_EMERGENCY_ALERT = (byte) 0x90;
	final static byte DATA_COMPLETE_GAS_BLOCK = (byte) 0x91;
	final static byte LINK_TEST_MSG = (byte) 0xFE;
	final static byte SENSOR_LOW_BATTERY = (byte) 0x92;
	final static byte GAS_SENSOR_ATTACH = (byte) 0x93;
	final static byte GAS_SENSOR_DETACH = (byte) 0x94;
	/**
	 * 이벤트보고 (게이트웨이 이벤트) MSG ID
	 */
	final static byte ID_GW_EVENT_EMG = 0x01;
	final static byte ID_GW_EVENT_CENTOR = 0x02;
	final static byte ID_GW_EVENT_CANCEL = 0x03;
	final static byte ID_GW_EVENT_POWER = 0x04;
	final static byte ID_GW_EVENT_LOWBATT = 0x05;

	/**
	 * 게이트웨이 정보(요청시 전송) MSG ID
	 */
	final static byte ID_REPORT_GW_NUM = 0x10; // G/W 전화번호
	final static byte ID_REPORT_CENTER_NUM = 0x11; // Center 전화번호
	final static byte ID_REPORT_HELPER_NUM = 0x12; // 돌보미 전화번호
	final static byte ID_REPORT_CARE1_NUM = 0x13; // 보호자1 전화번호 => * 말벗1 전화번호로 사용
	final static byte ID_REPORT_CARE2_NUM = 0x14; // 보호자2 전화번호 => * 말벗2 전화번호로 사용
	final static byte ID_REPORT_119_NUM = 0x15; // 119 전화번호
	
	final static byte ID_REPORT_M2M_ADDRESS = 0x20; // M2M IP, Port 정보 ('.'와 ':' 포함하여 표기) 예=111.222.333.444:8088
	final static byte ID_REPORT_M2M_NUMBER = 0x21; // M2M(T) 전화번호 - Gateway가 거는 전화번호
	final static byte ID_REPORT_M2M_GW_NUM = 0x22; // M2M(R) 전화번호 - M2M이 Gateway로 전화할때 번호
	
	final static byte ID_REPORT_REPORT_CYCLE = 0x30; // 통신 주기 변경 설정
	final static byte ID_REPORT_REPORT_MISSED_TIME = 0x31; // 동작 미감지 시간 설정
	final static byte ID_REPORT_REPORT_CYCLE_TIME = 0x32; // 주기적 전송시간 설정
	final static byte ID_REPORT_MISSED_THRESHOLD = 0x33; // 미감지 threshold value
	final static byte ID_REPORT_ACTIVE_SENSOR_VALUE = 0x34; // 외출상태에서 활동량 감지되었을때 강제외출해제를 위한 활동량감지시간 설정(기본 5분)
	final static byte ID_REPORT_FORCE_RECEIVE = 0x35; // 119나 지역 캐어 센터에서 전화 연결 시 응답이 없을 때 강제 착신을 위한 응답대기시간 설정(기본 2분)
	
	final static byte ID_REPORT_GATEWAY_INFO = 0x50; //게이트웨이 RSSI, 총 통화량, 총 데이터 사용량
	final static byte ID_REPORT_FINISH_SEND = 0x40; //응답이 다중패킷으로 이루어진 경우 전송이 끝난 후 전송종료 신호로 사용
	final static byte ID_REPORT_LOG_START = 0x00; // Telefield 서버로 로그 파일 전송 시작 메시지
	
	/**
	 * 활동량 1주일 데이터 요청 결과 전송 및 활동량 미보고 데이터 전송
	 */
	final static byte ID_WEEK_ACTIVITY_DATA = 0x01;
	final static byte ID_WEEK_MISSED_ACTIVITY_DATA = 0x02;
	
	/**
	 * 센서 lowbatt 데이터 전송
	 */
}
