package com.telefield.smartcaresystem.m2m;

public interface ProtocolConfig {

	final static int MAX_PROTOCOL_QUEUE = 1024 * 10;
	
	final static int SIZE_MACADDRESS = 8;
	
	// M2M Default Address
	final static String M2M_ADDRESS = "222.122.128.21";
	final static int M2M_PORT = 18003;
	
	final static int CONNECT_TIMEOUT = 5000;
	final static int ACK_WAITING_TIME = 5000; // 1sec --> 3sec 변경 기본 3000으로 되어 있엇음
	final static int READ_WAITING_TIME = 1000; // 1sec --> 1sec 변경 기분 1000으로 되어 있었음
	final static int MAX_WAIT_TIME = 3000;
	final static int RETRY_CNT = 2;
	final static int DATA_WAITING_TIME = 1000 * 5; //10초 -> 5초로 변경
	
	/**
	 * ACK OPERATOR
	 */
	final static byte ACK_OPERATOR = (byte) 0x80;
	
	
	final static byte MESSAGE_REQUEST_TIME = (byte) 0x88;
	final static byte MESSAGE_CHANGE_GW = 0x09;
	
	// M2M -> GW 개통 성공
	final static byte MESSAGE_OPENNING_SUCCESS = 0x50;
	
	// Default GateWay MSG DATA
	final static byte DEFAULT_GW_EVENT_MSG_DATA = 0x01;

	
	final static String OPENNING_START = "개통 시작";
	final static String OPENNING_FINISH_SEND = "GW 전송 완료";
	final static String OPENNING_WAIT = "중앙서버 응답 대기중";
	final static String OPENNING_FAIL = "개통 실패";
	final static String OPENNING_SUCCESS = "개통 완료";
	final static String OPENNING_CANCEL = "개통 취소";

	final static String FAIL_CANNOT_CONNECT = "개통 실패! : M2M 서버로 연결할 수 없습니다.";
	final static String FAIL_NOT_RESPONSE = "개통 실패! : M2M 서버로 부터 응답이 없습니다.";	
	
	public static String PACKET_TYPE[] ={
		"데이터 전송 종료", //0x00
		"게이트웨이 등록", //0x01
		"게이트웨이 정보(요청시 전송)", //0x02
		"센서등록(화재,가스,모션,도어,돌보미)", //0x03
		"주기보고(가스,화재,도어,모션)", //0x04
		"이벤트보고 (센서 이벤트 : 화재,가스,돌보미,재실(도어))", // 0x05
		"센서정보(가스,화재,도어,모션. 요청시 전송)", //0x06
		"CAM data(화재/가스 이벤트 발생시 전송)", //0x07
		"시간정보요청", //0x08
		"설정변경(게이트웨이 설정 변경 - 전화번호등.)", //0x09
		"data request", //0x0a
		"이벤트보고 (게이트웨이 이벤트)", //0x0b
		"F/W update", //0x0c
		"data request (활동량 1주일 데이터 및 역점검 요청)", //0x0d
		"data request (로그 송신 요청 및 활동량 미보고 데이터 요청)", //0x0e
		"데이터 요청", //0x0f
		"역점검 요청 정보 결과 송신 (게이트웨이)", //0x10
		"",
		"",
		"활동량 1주일 데이터 요청 결과 전송", //0x13
		"센서 오동작 상황 취소 알림", //0x14
		"활동량 미보고 데이터 요청 결과 전송", //0x15
	};
}
