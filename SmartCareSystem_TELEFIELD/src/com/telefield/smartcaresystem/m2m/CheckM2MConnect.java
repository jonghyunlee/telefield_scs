package com.telefield.smartcaresystem.m2m;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

import android.graphics.Color;
import android.os.AsyncTask;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.telefield.smartcaresystem.Constant;
import com.telefield.smartcaresystem.R;
import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.ui.diagnosis.CheckM2MServerUI.CheckServerProgressDialog;
import com.telefield.smartcaresystem.util.LogMgr;

public class CheckM2MConnect extends AsyncTask<String, Integer, Void> {
	PipedOutputStream mPOut;
	PipedInputStream mPIn;
	LineNumberReader mReader;
	Process mProcess;
	int mValue;
	Boolean isSuccess = false;
	LinearLayout board;
	TextView mTextView;
	CheckServerProgressDialog dialog;
	private ProtocolManger mProtocolManger;
	
	public CheckM2MConnect(CheckServerProgressDialog dialog, LinearLayout layout) {
		this.dialog = dialog;
		this.board = layout;
		this.mProtocolManger = SmartCareSystemApplication.getInstance().getProtocolManger();
	}
	
	@Override
	protected void onPreExecute() {
		mValue = 0;
		mPOut = new PipedOutputStream();
		try {
			mPIn = new PipedInputStream(mPOut);
			mReader = new LineNumberReader(new InputStreamReader(mPIn));
		} catch (IOException e) {
			cancel(true);
		}
	}

	public void stop() {
		Process p = mProcess;
		if (p != null) {
			p.destroy();
		}
		cancel(true);
	}

	@Override
	protected Void doInBackground(String... params) {
//		try {
//			mProcess = new ProcessBuilder().command("/system/bin/ping", "-c " + Constant.COUNT_PING_TEST, "-W 1", params[0]).redirectErrorStream(true).start();
//
//			try {
//				InputStream in = mProcess.getInputStream();
//				OutputStream out = mProcess.getOutputStream();
//				byte[] buffer = new byte[1024];
//				int count;
//
//				// in -> buffer -> mPOut -> mReader -> 1 line of ping
//				// information to parse
//				while ((count = in.read(buffer)) != -1) {
//
//					mPOut.write(buffer, 0, count);
//					mValue += 10;
//					publishProgress(mValue);
//				}
//				out.close();
//				in.close();
//				mPOut.close();
//				mPIn.close();
//			} finally {
//				mProcess.destroy();
//				mProcess = null;
//			}
//		} catch (IOException e) {
//		}
		mProtocolManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REQUEST_TIME));
		mProtocolManger.messageSend(null);
		
		try {
			Thread.sleep(ProtocolConfig.ACK_WAITING_TIME * 3);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		publishProgress(null);
		return null;
		
	}

	@Override
	protected void onProgressUpdate(Integer... values) {
		isSuccess = mProtocolManger.isConnect();
//		try {
//			// Is a line ready to read from the "ping" command?
//
//			LogMgr.d(LogMgr.TAG, "CheckM2MConnect.java:onProgressUpdate:// mReader.ready() : " + mReader.ready());
//
//			while (mReader.ready()) {
//				// This just displays the output, you should typically
//				// parse it I guess.
//				String message = mReader.readLine();
//				LogMgr.d(LogMgr.TAG, "CheckM2MConnect.java:onProgressUpdate:// message : " + message);
//
//				if (message.contains("Unreachable") || message.contains("Timeout"))
//					isSuccess = false;
//				else if(message.contains("icmp_seq"))
//					isSuccess = true;
//			}
//		} catch (IOException t) {
//		}
		super.onProgressUpdate(values);
	}

	@Override
	protected void onCancelled() {
		super.onCancelled();
	}

	@Override
	protected void onPostExecute(Void result) {
		String checkLists = SmartCareSystemApplication.getInstance().getApplicationContext().getResources().getString(R.string.check_m2m_network);
		mTextView = new TextView(SmartCareSystemApplication.getInstance().getApplicationContext());
		mTextView.setTextSize(25);
		mTextView.setTextColor(Color.BLACK);
		mTextView.setText(checkLists + " : " + (isSuccess.equals(true) ? "OK" : "Fail"));
		
		board.addView(mTextView);
		dialog.dismiss();
		super.onPostExecute(result);
	}
}
