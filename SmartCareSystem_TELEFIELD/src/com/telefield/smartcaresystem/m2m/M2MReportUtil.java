package com.telefield.smartcaresystem.m2m;

import java.util.Date;
import java.util.List;

import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.database.ConnectDB;
import com.telefield.smartcaresystem.database.DBInfo;
import com.telefield.smartcaresystem.database.Sensor;
import com.telefield.smartcaresystem.database.SensorHistory;
import com.telefield.smartcaresystem.message.SensorID;
import com.telefield.smartcaresystem.sensor.SensorDeviceManager;
import com.telefield.smartcaresystem.util.LogMgr;

/**
 * M2M서버에 보고하는 UTIL 
 * 활동감지 보고 : reportActivity
 * 미활동감지 보고 : reportNonActivity
 * @author Administrator
 *
 */
public class M2MReportUtil {
	
	//미활동 감지 보고
	public static void reportNonActivity()
	{
		LogMgr.d(LogMgr.TAG,"M2MReportUtil start reportNonActivity");
		
		ProtocolManger mProtocolManger = SmartCareSystemApplication.getInstance()
				.getProtocolManger();
						
		SensorDeviceManager mgr = SmartCareSystemApplication.getInstance().getSensorDeviceManager();
		List<Sensor> sensor_list = mgr.getSensorList(SensorID.ACTIVITY_SENSOR_TYPE.getSensorType());
		
		if( sensor_list.size() == 0 ) return;
		
		Sensor sensor = sensor_list.get(0);
		
		if( sensor != null )
		{		
			//send to server for non-activity report
			mProtocolManger.insertFrame(ProtocolFrame.getInstance(
					GatewayIndex.INFO_REPORT_SENSOR_EVENT, sensor, GateWayMessage.DATA_DETECT_MISSED_ACTIVITY));		
			
	
			SmartCareSystemApplication.getInstance().getMySmartCareState()
					.setReportNonActivity(true);
			
			mProtocolManger.messageSend(null);
			
			LogMgr.d(LogMgr.TAG,"M2MReportUtil Done reportNonActivity");
		}
	}
	
	//활동감지 보고
	public static void reportActivity()
	{
		LogMgr.d(LogMgr.TAG,"M2MReportUtil Start reportActivity");
		SensorDeviceManager mgr = SmartCareSystemApplication.getInstance()
				.getSensorDeviceManager();
		List<Sensor> sensor_list = mgr
				.getSensorList(SensorID.ACTIVITY_SENSOR_TYPE
						.getSensorType());
		
		if( sensor_list.size() == 0 ) return;

		Sensor sensor = sensor_list.get(0);
		LogMgr.d(LogMgr.TAG, sensor.toString());
		if (sensor != null) {

			SmartCareSystemApplication
					.getInstance()
					.getProtocolManger()
					.insertFrame(
							ProtocolFrame
									.getInstance(
											GatewayIndex.INFO_REPORT_SENSOR_EVENT,
											sensor, (byte) GateWayMessage.DATA_DETECT_ACTIVITY));
			
			

			SmartCareSystemApplication.getInstance().getMySmartCareState()
			.setReportNonActivity(false);
			
			SmartCareSystemApplication
			.getInstance()
			.getProtocolManger().messageSend(null);
			LogMgr.d(LogMgr.TAG,"M2MReportUtil Done reportActivity");
		}
	}
	
	public static void reportOutMode()
	{
		LogMgr.d(LogMgr.TAG,"M2MReportUtil start reportOutMode");
		SensorDeviceManager mgr = SmartCareSystemApplication.getInstance()
				.getSensorDeviceManager();
		List<Sensor> sensor_list = mgr
				.getSensorList(SensorID.DOOR_SENSOR_TYPE
						.getSensorType());
		
		if( sensor_list.size() == 0 ) return;

		Sensor sensor = sensor_list.get(0);

		if (sensor != null) {

			SmartCareSystemApplication
					.getInstance()
					.getProtocolManger()
					.insertFrame(
							ProtocolFrame
									.getInstance(
											GatewayIndex.INFO_REPORT_SENSOR_EVENT,
											sensor, GateWayMessage.DATA_OUT_ROOT));
			
			

			SmartCareSystemApplication.getInstance().getMySmartCareState()
			.setReportNonActivity(false);
			
			ConnectDB con = ConnectDB.getInstance(SmartCareSystemApplication.getInstance().getApplicationContext());
			SensorHistory history = new SensorHistory(sensor.getSensor_id(), sensor.getType(), new Date(System.currentTimeMillis()), DBInfo.DOOR_LEAVE);
			con.insertSensorHistory(history);
			
			SmartCareSystemApplication
			.getInstance()
			.getProtocolManger().messageSend(null);
		}
		LogMgr.d(LogMgr.TAG,"M2MReportUtil Done reportOutMode");
	}
	
	public static void reportInMode()
	{
		LogMgr.d(LogMgr.TAG,"M2MReportUtil start reportInMode");
		SensorDeviceManager mgr = SmartCareSystemApplication.getInstance()
				.getSensorDeviceManager();
		List<Sensor> sensor_list = mgr
				.getSensorList(SensorID.DOOR_SENSOR_TYPE
						.getSensorType());
		
		if( sensor_list.size() == 0 ) return;

		Sensor sensor = sensor_list.get(0);

		if (sensor != null) {

			SmartCareSystemApplication
					.getInstance()
					.getProtocolManger()
					.insertFrame(
							ProtocolFrame
									.getInstance(
											GatewayIndex.INFO_REPORT_SENSOR_EVENT,
											sensor, GateWayMessage.DATA_IN_ROOT));
			
			

			SmartCareSystemApplication.getInstance().getMySmartCareState()
			.setReportNonActivity(false);
			
			ConnectDB con = ConnectDB.getInstance(SmartCareSystemApplication.getInstance().getApplicationContext());
			SensorHistory history = new SensorHistory(sensor.getSensor_id(), sensor.getType(), new Date(System.currentTimeMillis()), DBInfo.DOOR_ARRIVAL);
			con.insertSensorHistory(history);
			
			SmartCareSystemApplication
			.getInstance()
			.getProtocolManger().messageSend(null);
		}
		
		LogMgr.d(LogMgr.TAG,"M2MReportUtil Done reportInMode");
	}
	
	

}
