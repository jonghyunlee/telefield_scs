package com.telefield.smartcaresystem.m2m;

import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;

import com.telefield.smartcaresystem.util.LogMgr;

public class SocketMessageQueue {
public static final String TAG = "SmartCareSystem";
	
	private int front;
	private int rear;
	private int maxsize;
	private int size;
	private byte queue[]; // circular queue	
	
	
	final int MAX_WATING_TIME = 3000; //3sec	
	Timer timer;
	boolean waitingData = false;
	
	public SocketMessageQueue(int maxsize)
	{
		queue = new byte[maxsize];
		this.front = 0;
		this.rear = 0;
		this.maxsize = maxsize;
		timer = new Timer();
	}
	
	public void queueinit()
	{
		front = rear = 0;
		size = 0;
	}
	
	public int getSize()
	{
		return this.size;
	}
	
	public boolean isEmpty()
	{
		return (this.size == 0) ? true : false;
	}
	
	public boolean isFull()
	{
		return (this.size == maxsize) ? true : false;
	}
	
	public boolean isCanInsert(int length)
	{
		return ((this.size + length) > maxsize) ? false : true;
	}
	
	public synchronized void insert(byte data[],int length)
	{
		if( isCanInsert(length))
		{
			for( int i=0; i < length; i++)
			{
				queue[front] = data[i];
				this.size++;
				front++;
				
				if( front >= maxsize)
				{
					front=0;
				}
			}
		}else
		{
			LogMgr.d("MessageQueue", "Message 유실");
		}
	}
	
	public int getHeadPosition()
	{
		return front;
	}	
	
	public int getRearPosition()
	{
		return rear;
	}
	
	public synchronized byte get()
	{
		byte data = 0;	
		
		data = queue[rear];		
		rear++;
		this.size--;
		
		if(rear >= maxsize)
		{
			rear = 0;
		}
		
		return data;
	}
	
	public void waitingData()
	{
		if( waitingData == false)
		{
			waitingData = true;
			timer.schedule(
					new TimerTask() {					
						@Override
						public void run() {
							// TODO Auto-generated method stub
							queueinit();							
						}
					}
					, MAX_WATING_TIME);
		}
	}
	
	
	public synchronized LinkedList<ProtocolFrame> messageCompose()
	{
		int startbit_idx;
		int length=0;
		int endbit_idx;
		
		int current_idx = 0;		
		LinkedList<ProtocolFrame> messageList = new LinkedList<ProtocolFrame>();
		
		if( isEmpty()) return null;		
		
		while(true)
		{
			//먼저 start_bit를 찾는다.
			startbit_idx = rear;	
			
			if( startbit_idx >= maxsize) startbit_idx=0;
			
			if( startbit_idx == front ) 
			{				
				break; //다 찾았다.				
			}
			
			if( queue[startbit_idx] == GatewayIndex.START_DATA )
			{
				if( this.size < 5 ) 
				{
					waitingData();
					break;
				}
				
				int temp_idx = startbit_idx + GatewayIndex.PACKET_LENGTH;
				
				if(temp_idx >= maxsize)
				{
					length = (int)( 0xff & queue[ temp_idx - maxsize +1]);
					length += (int)(0xff & (queue[ temp_idx-maxsize] << 8 ));
				}else if(temp_idx >= (maxsize -1) )
				{
					length = (int)( 0xff & queue[ temp_idx+1-maxsize]);
					length += (int)(0xff & (queue[ temp_idx] << 8 ));
				}else
				{
					length = (int)(0xff & queue[ startbit_idx + GatewayIndex.PACKET_LENGTH + 1]);
					length += (int)( 0xff & (queue[ startbit_idx + GatewayIndex.PACKET_LENGTH] << 8 ));
				}		
				
							
				
				//end_bit idx
				endbit_idx = startbit_idx + 1 + length;
				
				
				if( endbit_idx - startbit_idx + 1 > this.size)
				{
					waitingData();
					//만약 endbi_idx 가 size 보다 크면 아직 수신을 안한것이다.
//					LogMgr.d(TAG,"waiting end bit data");
					break;
				}
				
				if( endbit_idx >= maxsize) // > -> >=  으로 변경  startbit_idx 10240 일때 0이 되지 않아 exception 발생
				{
					endbit_idx = endbit_idx - maxsize;
					
					if(  endbit_idx >= maxsize )
					{
						LogMgr.e(TAG,"error !!!!!");
						queueinit();
						break;
					}
				}
				
				if( queue[endbit_idx]== GatewayIndex.END_DATA)
				{
					//2번째 data까지 맞았다. 올바른 data
//					LogMgr.d(TAG,"Message Compose success. startbit_idx : " + startbit_idx + " endbit_idx : " + endbit_idx);
					
					int size = 0;
					
				    if( endbit_idx > startbit_idx)
				    {
				    	size = endbit_idx - startbit_idx + 1;
				    }
				    else
				    {
				    	size = maxsize - startbit_idx + endbit_idx + 1;
				    }
				    
				    if( size > 0)
				    {					
				    	byte data[] = new byte[size];
				    	
				    	for(int i=0; i < size; i++)
				    	{
				    		if( startbit_idx + i >= maxsize) // > -> >=  으로 변경  startbit_idx 10230 인경우 10240 일때 0이 되지 않아 exception 발생
				    		{
				    			data[i] = queue[startbit_idx + i - maxsize];
				    		}else	
				    		{
				    			data[i] = queue[startbit_idx + i];
				    		}
				    	}
				    	
				    	ProtocolFrame ff = ProtocolFrame.getInstance(data);
				    	if( ff != null)				    	
				    		messageList.add(ff);
				    	
				    	rear = startbit_idx = endbit_idx+1;
				    	this.size = this.size - size;
				    	
				    }					
				}else
				{
					LogMgr.d(TAG,"Invalid data -- endbit index : " + endbit_idx);
					startbit_idx++;
					rear++;
					this.size--;
					
				}					
				
			}else
			{
				LogMgr.d(TAG,"start bit value not equals");
				startbit_idx++;
				rear++;
				this.size--;
			}			
		}
		
		return messageList;
		
		
	}

}
