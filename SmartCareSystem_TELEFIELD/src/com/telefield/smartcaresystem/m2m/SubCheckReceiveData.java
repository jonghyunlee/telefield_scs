package com.telefield.smartcaresystem.m2m;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.StringTokenizer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import com.telefield.smartcaresystem.message.FormattedFrame;
import com.telefield.smartcaresystem.util.LogMgr;

public class SubCheckReceiveData {
	private SubProtocolManger mSubProtocolManger;

	public SubCheckReceiveData(SubProtocolManger pManager) {
		this.mSubProtocolManger = pManager;
	}

	public void analysisData(byte[] data, byte rcv_packet_id) {
		if (rcv_packet_id == M2MIndex.LOG_REQUEST) {
			mSubProtocolManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_GW_DATA, FormattedFrame.getInstance(GateWayMessage.ID_REPORT_LOG_START)));
			
			mSubProtocolManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_GW_DATA, FormattedFrame.getInstance(GateWayMessage.ID_REPORT_FINISH_SEND)));
			mSubProtocolManger.messageSend(null);
		}
	}

	public void zip(String sourcePath, String output) throws Exception {

		// 압축 대상(sourcePath)이 디렉토리나 파일이 아니면 리턴한다.
		File sourceFile = new File(sourcePath);
		if (!sourceFile.isFile() && !sourceFile.isDirectory()) {
			throw new Exception("압축 대상의 파일을 찾을 수가 없습니다.");
		}

		FileOutputStream fos = null;
		BufferedOutputStream bos = null;
		ZipOutputStream zos = null;

		try {
			fos = new FileOutputStream(output); // FileOutputStream
			bos = new BufferedOutputStream(fos); // BufferedStream
			zos = new ZipOutputStream(bos); // ZipOutputStream
			zos.setLevel(8); // 압축 레벨 - 최대 압축률은 9, 디폴트 8

			zipEntry(sourceFile, sourcePath, zos); // Zip 파일 생성
			zos.finish(); // ZipOutputStream finish
		} finally {
			if (zos != null) {
				zos.close();
			}
			if (bos != null) {
				bos.close();
			}
			if (fos != null) {
				fos.close();
			}
		}
	}

	/**
	 * 압축
	 * 
	 * @param sourceFile
	 * @param sourcePath
	 * @param zos
	 * @throws Exception
	 */
	private void zipEntry(File sourceFile, String sourcePath, ZipOutputStream zos) throws Exception {
		// sourceFile 이 디렉토리인 경우 하위 파일 리스트 가져와 재귀호출
		if (sourceFile.isDirectory()) {
			if (sourceFile.getName().equalsIgnoreCase(".metadata")) {
				return;
			}
			File[] fileArray = sourceFile.listFiles(); // sourceFile 의 하위 파일 리스트
			for (int i = 0; i < fileArray.length; i++) {
				zipEntry(fileArray[i], sourcePath, zos); // 재귀 호출
			}
		} else { // sourcehFile 이 디렉토리가 아닌 경우
			BufferedInputStream bis = null;

			try {
				String sFilePath = sourceFile.getPath();
				LogMgr.d(LogMgr.TAG, "SubCheckReceiveData.java:zipEntry:// sFilePath : " + sFilePath);

				StringTokenizer tok = new StringTokenizer(sFilePath, "/");

				int tok_len = tok.countTokens();
				String zipEntryName = tok.toString();
				while (tok_len != 0) {
					tok_len--;
					zipEntryName = tok.nextToken();
				}
				bis = new BufferedInputStream(new FileInputStream(sourceFile));

				ZipEntry zentry = new ZipEntry(zipEntryName);
				zentry.setTime(sourceFile.lastModified());
				zos.putNextEntry(zentry);

				byte[] buffer = new byte[2048];
				int cnt = 0;

				while ((cnt = bis.read(buffer, 0, 2048)) != -1) {
					zos.write(buffer, 0, cnt);
				}
				zos.closeEntry();
			} finally {
				if (bis != null) {
					bis.close();
				}
			}
		}
	}
}
