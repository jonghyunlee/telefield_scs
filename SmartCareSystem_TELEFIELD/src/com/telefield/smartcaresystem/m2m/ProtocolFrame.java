package com.telefield.smartcaresystem.m2m;

import java.nio.ByteBuffer;

import android.bluetooth.BluetoothDevice;

import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.database.BloodPressure;
import com.telefield.smartcaresystem.database.Sensor;
import com.telefield.smartcaresystem.message.FormattedFrame;
import com.telefield.smartcaresystem.message.MessageID;
import com.telefield.smartcaresystem.message.SensorID;
import com.telefield.smartcaresystem.sensor.SensorDeviceManager;
import com.telefield.smartcaresystem.util.ByteArrayToHex;
import com.telefield.smartcaresystem.util.CheckSum;
import com.telefield.smartcaresystem.util.LogMgr;
import com.telefield.smartcaresystem.util.ConvertTime;

public class ProtocolFrame {
	byte packet_start;
	byte packet_payload[];
	byte packet_end;
	boolean isACK = false;

	public void setPacket_start(byte packet_start) {
		this.packet_start = packet_start;
	}
	public void setPacket_payload(byte[] packet_payload) {
		this.packet_payload = packet_payload;
	}
	public void setPacket_end(byte packet_end) {
		this.packet_end = packet_end;
	}
	private ProtocolFrame() {

	}
	public static ProtocolFrame getInstance() {
		ProtocolFrame frame = new ProtocolFrame();
		return frame;
	}

	public static ProtocolFrame getInstance(byte type) {
		ProtocolFrame frame = new ProtocolFrame();
		frame.packet_start = GatewayIndex.START_DATA;
		frame.packet_payload = frame.payloadOther(type, null);
		frame.packet_end = GatewayIndex.END_DATA;
	    
		return frame;
	}
	
	/**
	 * 
	 * @param Event type type
	 * @param sensor
	 * @param ff
	 * @return
	 */
	public static ProtocolFrame getInstance(byte type, Sensor sensor, FormattedFrame ff){
		ProtocolFrame frame = new ProtocolFrame();
		frame.packet_start = GatewayIndex.START_DATA;
		frame.packet_payload = frame.payloadSensor(type, sensor, ff);
		frame.packet_end = GatewayIndex.END_DATA;
		
		return frame;
	}
	
	public static ProtocolFrame getInstance(byte type, Sensor sensor, byte data){
		ProtocolFrame frame = new ProtocolFrame();
		frame.packet_start = GatewayIndex.START_DATA;		
		frame.packet_payload = frame.payloadSensor(type, sensor, data);
		frame.packet_end = GatewayIndex.END_DATA;
		
		return frame;
	}
	
	public static ProtocolFrame getInstance(byte type, BluetoothDevice device, BloodPressure data){
		ProtocolFrame frame = new ProtocolFrame();
		frame.packet_start = GatewayIndex.START_DATA;		
		frame.packet_payload = frame.payloadBloodPressure(type, device, data);
		frame.packet_end = GatewayIndex.END_DATA;
		
		return frame;
	}
	
	public static ProtocolFrame getInstance(byte type, FormattedFrame ff){
		ProtocolFrame frame = new ProtocolFrame();
		frame.packet_start = GatewayIndex.START_DATA;
		frame.packet_payload = frame.payloadGateWay(type, ff);
		frame.packet_end = GatewayIndex.END_DATA;
		
		return frame;
	}
	
	public static ProtocolFrame getInstance(byte type, byte[] data) {
		ProtocolFrame frame = new ProtocolFrame();
		frame.packet_start = GatewayIndex.START_DATA;
		frame.packet_payload = frame.payloadOther(type, data);
		frame.packet_end = GatewayIndex.END_DATA;
	    
		return frame;
	}
	
	public static ProtocolFrame getInstance(byte type, byte[] bytes, int totalBlockNum, int currentBlockNum) {
		ProtocolFrame frame = new ProtocolFrame();
		frame.packet_start = GatewayIndex.START_DATA;
		frame.packet_payload = frame.payloadCamData(type, bytes, totalBlockNum, currentBlockNum);
		frame.packet_end = GatewayIndex.END_DATA;
		
		return frame;
	}
	
	public static ProtocolFrame getInstance(byte[] data) {
		ProtocolFrame frame = new ProtocolFrame();
		frame.packet_start = GatewayIndex.START_DATA;
		frame.packet_payload = data;
		frame.packet_end = GatewayIndex.END_DATA;
	    
		return frame;
	}
	
	public static ProtocolFrame getInstance(byte[] data, boolean isACK) {
		ProtocolFrame frame = new ProtocolFrame();
		frame.packet_start = GatewayIndex.START_DATA;
		frame.packet_payload = data;
		frame.packet_end = GatewayIndex.END_DATA;
	    frame.isACK = isACK;
		return frame;
	}
	
	
	private byte[] payloadCamData(byte type, byte[] data, int totalCount, int currentCount){
		byte[] payload = null;
		switch(type){
		case GatewayIndex.INFO_REPORT_CAM:
			payload = new byte[GatewayIndex.SIZE_REPORT_CAM + data.length];			
			payload[GatewayIndex.CAM_BLOCK_DATA_SIZE] = (byte) data.length;
			break;
		case GatewayIndex.INFO_LOG_DATA:
			payload = new byte[GatewayIndex.SIZE_REPORT_LOG + data.length];
			int data_size = data.length;
			
			byte[] size = new byte[M2MIndex.FW_DATA_SIZE];
			size = ByteBuffer.allocate(2).putShort((short) data_size).array();
			System.arraycopy(size, 0, payload, GatewayIndex.LOG_BLOCK_DATA_SIZE, size.length);
			break;
		}
		GatewayDevice gw = GatewayDevice.getInstance();
		byte[] total = getIntToByteArray(totalCount, GatewayIndex.CAM_LENGTH_BLOCK);
		byte[] current = getIntToByteArray(currentCount, GatewayIndex.CAM_LENGTH_BLOCK);
		
		payload[GatewayIndex.PACKET_ID] = type;
		System.arraycopy(gw.number, 0, payload, GatewayIndex.GW_NUM, gw.number.length);
		System.arraycopy(gw.MAC, 0, payload, GatewayIndex.GW_MAC, gw.MAC.length);
		System.arraycopy(total, 0, payload, GatewayIndex.CAM_TOTAL_BLOCK_NUM, total.length);
		System.arraycopy(current, 0, payload, GatewayIndex.CAM_CURRENT_BLOCK_NUM, current.length);
		
		System.arraycopy(data, 0, payload, GatewayIndex.CAM_BLOCK_DATA, data.length);
		payload[GatewayIndex.LOG_BLOCK_DATA + data.length] = CheckSum.getCamDataCRC(data);
		return payload;
	}
	
	private byte[] getIntToByteArray(int data, int size){
		byte[] bf = new byte[size];
		
		bf[0] = (byte) (data >>8);
		bf[1] = (byte) (data);
		
		return bf;
	}
	
	private byte[] payloadOther(byte type, byte[] data){
		byte[] payload = null;
		GatewayDevice gw = GatewayDevice.getInstance();
		byte[] byte_time = ConvertTime.getCurrentTime();
		
		switch (type) {
		case GatewayIndex.INFO_REGISTER_GW:
			//payload에 스마트폰 버전정보를 추가한다.
			payload = new byte[GatewayIndex.SIZE_REGISTER_GW + 3];
			System.arraycopy(gw.MAC, 0, payload, GatewayIndex.GW_MAC, gw.MAC.length);
			payload[GatewayIndex.GW_POWER] = gw.power_state;
			payload[GatewayIndex.GW_BATTERY] = gw.battery_state;
			System.arraycopy(byte_time, 0, payload, GatewayIndex.TIME_TIC, byte_time.length);
			// gateway fw 버전 입력 (3byte)
			//1~3byte 지그비 버전, 4~6byte 스마트폰 버전 
			payload[GatewayIndex.GW_FW_VER] =GatewayDevice.getInstance().version[0];
			payload[GatewayIndex.GW_FW_VER + 1] =GatewayDevice.getInstance().version[1];
			payload[GatewayIndex.GW_FW_VER + 2] =GatewayDevice.getInstance().version[2];
			
			payload[GatewayIndex.GW_FW_VER + 3] =GatewayDevice.getInstance().smartphone_version[0];
			payload[GatewayIndex.GW_FW_VER + 4] =GatewayDevice.getInstance().smartphone_version[1];
			payload[GatewayIndex.GW_FW_VER + 5] =GatewayDevice.getInstance().smartphone_version[2];
			
			break;
		case GatewayIndex.INFO_REQUEST_TIME:
			payload = new byte[GatewayIndex.SIZE_REQUEST_TIME];
			break;
		case GatewayIndex.INFO_REQUEST_DATA:
			payload = new byte[GatewayIndex.SIZE_REQUEST_DATA];
			System.arraycopy(gw.MAC, 0, payload, GatewayIndex.GW_MAC, gw.MAC.length);
			break;
		case GatewayIndex.INFO_REQUEST_REALTIME_LOG:
			payload = new byte[GatewayIndex.SIZE_REPORT_REALTIME_LOG + data.length];
			System.arraycopy(gw.MAC, 0, payload, GatewayIndex.GW_MAC, gw.MAC.length);
			System.arraycopy(data, 0, payload, GatewayIndex.SIZE_REPORT_REALTIME_LOG, data.length);
			payload[GatewayIndex.GW_POWER] = gw.power_state;
			payload[GatewayIndex.GW_BATTERY] = gw.battery_state;
			break;
		default:
			break;

		}
		payload[GatewayIndex.PACKET_ID] = type;
		System.arraycopy(gw.number, 0, payload, GatewayIndex.GW_NUM, gw.number.length);
		
		return payload;
	}

	private byte[] payloadSensor(byte type, Sensor sensor, byte data){
		byte[] payload = null;
		byte[] byte_time = ConvertTime.getCurrentTime();
		GatewayDevice gw = GatewayDevice.getInstance();
		byte sensor_batt = sensor.getSensor_batt() == null ? 0x00 : sensor.getSensor_batt().byteValue();
		byte sensor_status = sensor.getComm_status() == null ? 0x00 : sensor.getComm_status().byteValue();
		byte[] frameSubData = new byte[1];
		byte msg_id = 0;
		
		LogMgr.d(LogMgr.TAG, "ProtocolFrame.java:payloadSensor// msg_id : " + msg_id);
		LogMgr.d(LogMgr.TAG, "ProtocolFrame.java:payloadSensor// frameSubData : " + ByteArrayToHex.byteArrayToHex(frameSubData, frameSubData.length));
		
		switch (type) {
		case GatewayIndex.INFO_REPORT_SENSOR_EVENT:
			payload = new byte[GatewayIndex.SIZE_REPORT_SENSOR_EVENT];
			// 이벤트보고 (센서 이벤트 : 화재,가스,돌보미,재실(도어))
			payload[GatewayIndex.EVENT_SENSOR_BATTERY] = sensor_batt;
			payload[GatewayIndex.EVENT_SENSOR_STATUS] = sensor_status;
			payload[GatewayIndex.EVENT_SENSOR_DATA] = data;
			break;
			
		default:
			break;

		}
		
		payload[GatewayIndex.PACKET_ID] = type;
		System.arraycopy(gw.number, 0, payload, GatewayIndex.GW_NUM, gw.number.length);
		System.arraycopy(gw.MAC, 0, payload, GatewayIndex.GW_MAC, gw.MAC.length);
		payload[GatewayIndex.GW_POWER] = gw.power_state;
		payload[GatewayIndex.GW_BATTERY] = gw.battery_state;
		System.arraycopy(byte_time, 0, payload, GatewayIndex.TIME_TIC, byte_time.length);
		
		byte[] sensorID = ByteBuffer.allocate(Short.SIZE / 8).putShort(sensor.getSensor_id().shortValue()).array();
		System.arraycopy(sensorID, 0, payload, GatewayIndex.SENSOR_ADDR, sensorID.length);
		
		byte[] sensorMAC = ByteArrayToHex.getMacAddress(sensor.getMAC());
		System.arraycopy(sensorMAC, 0, payload, GatewayIndex.SENSOR_MAC, sensorMAC.length);
		
		return payload;
	}
	
	private byte[] payloadSensor(byte type, Sensor sensor, FormattedFrame ff){
		byte[] payload = null;
		byte[] byte_time = ConvertTime.getCurrentTime();
		GatewayDevice gw = GatewayDevice.getInstance();
		byte sensor_batt = sensor.getSensor_batt() == null ? 0x00 : sensor.getSensor_batt().byteValue();
		byte sensor_status = sensor.getComm_status() == null ? 0x00 : sensor.getComm_status().byteValue();
		byte[] frameSubData = new byte[1];
		byte msg_id = 0;
		if(ff != null){
			frameSubData = ff.getSub_data();
			msg_id = ff.getMsg_id();
			LogMgr.d(LogMgr.TAG, "ProtocolFrame.java:payloadSensor// msg_id : " + msg_id);
			if(frameSubData != null)
				LogMgr.d(LogMgr.TAG, "ProtocolFrame.java:payloadSensor// frameSubData : " + ByteArrayToHex.byteArrayToHex(frameSubData, frameSubData.length));
		}
		
		switch (type) {
		case GatewayIndex.INFO_REGISTER_SENSOR:
			// 센서등록(화재,가스,모션,도어,돌보미)
			payload = new byte[GatewayIndex.SIZE_REGISTER_SENSOR];
			if(sensor.getReg_status() != null)
				payload[GatewayIndex.SENSOR_REG_UNREG] = sensor.getReg_status().byteValue();
			break;
			
		case GatewayIndex.INFO_REPORT_CYCLE:
			payload = new byte[GatewayIndex.SIZE_REPORT_CYCLE];
			// 주기보고(가스,화재,도어,모션)	
			payload[GatewayIndex.CYCLE_SENSOR_BATTERY] = sensor_batt;
			payload[GatewayIndex.CYCLE_SENSOR_STATUS] = sensor_status;
			int[] cnt = sensor.getPirCnt();
			if( cnt != null )
			{
				for (int i = 0; i < cnt.length; i++) {
					payload[GatewayIndex.CYCLE_SENSOR_DATA + i] = (byte)cnt[i];
				}
			}
			if(sensor.getRssi() != null){
				payload[GatewayIndex.CYCLE_SENSOR_RSSI] = sensor.getRssi().byteValue();
			}else{
				payload[GatewayIndex.CYCLE_SENSOR_RSSI] = 0;
			}
			break;
			
		case GatewayIndex.INFO_REPORT_SENSOR_EVENT:
			payload = new byte[GatewayIndex.SIZE_REPORT_SENSOR_EVENT];
			// 이벤트보고 (센서 이벤트 : 화재,가스,돌보미,재실(도어))
			payload[GatewayIndex.EVENT_SENSOR_BATTERY] = sensor_batt;
			payload[GatewayIndex.EVENT_SENSOR_STATUS] = sensor_status;
			payload[GatewayIndex.EVENT_SENSOR_DATA] = getSensorData(msg_id, frameSubData);
			break;
			
		case GatewayIndex.INFO_REPORT_SENSOR_DATA:
		{
			payload = new byte[GatewayIndex.SIZE_REPORT_SENSOR_DATA];
			// 센서정보(가스,화재,도어,모션. 요청시 전송)
			payload[GatewayIndex.REPORT_SENSOR_BATTERY] = sensor_batt;
			payload[GatewayIndex.REPORT_SENSOR_STATUS] = sensor_status;
			int[] per_cnt = sensor.getSensor_data();
			if( per_cnt != null )
			{
				for (int i = 0; i < per_cnt.length; i++) {
					payload[GatewayIndex.REPORT_SENSOR_DATA + i] = (byte)per_cnt[i];
				}
			}
		}
			break;
			
		case GatewayIndex.INFO_RESULT_CHECK_SENSOR:
			payload = new byte[GatewayIndex.SIZE_RESULT_CHECK_SENSOR];
			payload[GatewayIndex.REPORT_CHECK_SENSOR_STATUS] = sensor_status;
			
			break;
			
		case GatewayIndex.INFO_REPORT_WEEK_DATA:
		{
			payload = new byte[GatewayIndex.SIZE_REPORT_WEEK_DATA];
			// 활동량 1주일 데이터 요청 결과 전송
			
			payload[GatewayIndex.WEEK_MSG_ID] = msg_id;
			
			byte[] sensorID = ByteBuffer.allocate(Short.SIZE / 8).putShort(sensor.getSensor_id().shortValue()).array();
			if(sensorID.length != 0)
				System.arraycopy(sensorID, 0, payload, GatewayIndex.WEEK_SENSOR_ADDR, sensorID.length);
			
			byte[] sensorMAC = ByteArrayToHex.getMacAddress(sensor.getMAC());
			System.arraycopy(sensorMAC, 0, payload, GatewayIndex.WEEK_SENSOR_MAC, sensorMAC.length);
			
			payload[GatewayIndex.WEEK_SENSOR_BATTERY] = sensor_batt;
			payload[GatewayIndex.WEEK_SENSOR_STATUS] = sensor_status;
			
			int[] per_cnt = sensor.getSensor_data();
			if( per_cnt != null )
			{
				for (int i = 0; i < per_cnt.length; i++) {
					payload[GatewayIndex.WEEK_SENSOR_DATA + i] = (byte)per_cnt[i];
				}
			}
			LogMgr.d(LogMgr.TAG, "ProtocolFrame.java:payloadSensor:// payload : " + ByteArrayToHex.byteArrayToHex(payload, payload.length));

		}
			break;
			
		default:
			break;

		}
		payload[GatewayIndex.PACKET_ID] = type;
		System.arraycopy(gw.number, 0, payload, GatewayIndex.GW_NUM, gw.number.length);
		System.arraycopy(gw.MAC, 0, payload, GatewayIndex.GW_MAC, gw.MAC.length);
		payload[GatewayIndex.GW_POWER] = gw.power_state;
		payload[GatewayIndex.GW_BATTERY] = gw.battery_state;
		System.arraycopy(byte_time, 0, payload, GatewayIndex.TIME_TIC, byte_time.length);
		
		if(type != GatewayIndex.INFO_REPORT_WEEK_DATA){
			byte[] sensorID = ByteBuffer.allocate(Short.SIZE / 8).putShort(sensor.getSensor_id().shortValue()).array();
			if(sensorID.length != 0)
				System.arraycopy(sensorID, 0, payload, GatewayIndex.SENSOR_ADDR, sensorID.length);
			
			byte[] sensorMAC = ByteArrayToHex.getMacAddress(sensor.getMAC());
			System.arraycopy(sensorMAC, 0, payload, GatewayIndex.SENSOR_MAC, sensorMAC.length);
		}
		
		return payload;
	}
	
	private byte[] payloadBloodPressure(byte type, BluetoothDevice device, BloodPressure data){
		byte[] payload = null;
		byte[] byte_time = ConvertTime.getCurrentTime();
		GatewayDevice gw = GatewayDevice.getInstance();
		
		switch (type) {
		// 이벤트보고 (센서 이벤트 : 화재,가스,돌보미,재실(도어))
		case GatewayIndex.INFO_REPORT_BLOODPRESSURE_DATA:
			payload = new byte[GatewayIndex.SIZE_REPORT_BLOODPRESSURE_DATA];
			
			payload[GatewayIndex.PACKET_ID] = type;
			System.arraycopy(gw.number, 0, payload, GatewayIndex.GW_NUM, gw.number.length);
			System.arraycopy(gw.MAC, 0, payload, GatewayIndex.GW_MAC, gw.MAC.length);
			payload[GatewayIndex.GW_POWER] = gw.power_state;
			payload[GatewayIndex.GW_BATTERY] = gw.battery_state;
			System.arraycopy(byte_time, 0, payload, GatewayIndex.TIME_TIC, byte_time.length);
			
			Integer sensorid = SensorID.getSensorId(SensorID.BLOOD_PRESURE_TYPE.getSensorType(), 0x01);
			byte[] sensorID = ByteBuffer.allocate(Short.SIZE / 8).putShort(sensorid.shortValue()).array();
			System.arraycopy(sensorID, 0, payload, GatewayIndex.SENSOR_ADDR, sensorID.length);

			LogMgr.d(LogMgr.TAG, "ProtocolFrame.java:payloadBloodPressure:// device.getAddress() : " + device.getAddress());


			byte[] sensorMAC = ByteArrayToHex.getBloodPressureMacAddress(device.getAddress());

			System.arraycopy(sensorMAC, 0, payload, GatewayIndex.SENSOR_MAC, sensorMAC.length);
			
			payload[GatewayIndex.EVENT_BLOODPRESSURE_BATTERY] = 0x64;
			payload[GatewayIndex.EVENT_BLOODPRESSURE_STATUS] = 0x01;
			payload[GatewayIndex.EVENT_BLOODPRESSURE_SYSTOLIC] = data.getSystolic().byteValue();
			payload[GatewayIndex.EVENT_BLOODPRESSURE_DIASTOLIC] = data.getDiastolic().byteValue();
			payload[GatewayIndex.EVENT_BLOODPRESSURE_PULSE] = data.getPulse().byteValue();
			
			break;
			
		default:
			break;

		}
		return payload;
	}
	
	private byte[] payloadGateWay(byte type, FormattedFrame ff){
		byte[] payload = null;
		byte[] byte_time = ConvertTime.getCurrentTime();
		GatewayDevice gw = GatewayDevice.getInstance();
		byte msg_id = ff.getMsg_id();
		byte[] sub_data = ff.getSub_data();
		LogMgr.d(LogMgr.TAG, "ProtocolFrame type data : " + type + ".. msg_id : " + msg_id);
		switch (type) {
		case GatewayIndex.INFO_REPORT_GW_EVENT:
			payload = new byte[GatewayIndex.SIZE_REPORT_GW_EVENT];
			byte gatewayMSGID = getEventMSGID(msg_id, sub_data);
			payload[GatewayIndex.EVENT_GW_MSG_ID] = gatewayMSGID;
			payload[GatewayIndex.EVENT_GW_MSG_DATA] = getEventMSGData(gatewayMSGID, sub_data);			
			break;
		//msg_id가 이상함	
		case GatewayIndex.INFO_REPORT_GW_DATA:
			if(sub_data != null){
				payload = new byte[GatewayIndex.SIZE_REPORT_GW_DATA + sub_data.length];
				payload[GatewayIndex.REPORT_GW_MSG_ID] = msg_id;
				payload[GatewayIndex.REPORT_GW_MSG_LENGTH] = (byte)sub_data.length;
				if(sub_data.length > 0)
					System.arraycopy(sub_data, 0, payload, GatewayIndex.SIZE_REPORT_GW_DATA, sub_data.length);
			}
			else {
				payload = new byte[GatewayIndex.SIZE_REPORT_GW_DATA];
				payload[GatewayIndex.REPORT_GW_MSG_ID] = msg_id;
				payload[GatewayIndex.REPORT_GW_MSG_LENGTH] = (byte)0x00;
			}
			break;
		case GatewayIndex.INFO_RESULT_CHECK_GW:
			payload = new byte[GatewayIndex.SIZE_RESULT_CHECK_GW + 3];
			// gateway fw 버전 입력 (3byte)
			payload[GatewayIndex.GW_FW_VER] = GatewayDevice.getInstance().version[0];
			payload[GatewayIndex.GW_FW_VER + 1] = GatewayDevice.getInstance().version[1];
			payload[GatewayIndex.GW_FW_VER + 2] = GatewayDevice.getInstance().version[2];
			
			payload[GatewayIndex.GW_FW_VER + 3] = GatewayDevice.getInstance().smartphone_version[0];
			payload[GatewayIndex.GW_FW_VER + 4] = GatewayDevice.getInstance().smartphone_version[1];
			payload[GatewayIndex.GW_FW_VER + 5] = GatewayDevice.getInstance().smartphone_version[2];
			
			int index = 0;
			LogMgr.d(LogMgr.TAG, "ProtocolFrame.java:payloadGateWay:// sub_data : " + ByteArrayToHex.byteArrayToHex(sub_data, sub_data.length));

			System.arraycopy(sub_data, index, payload, GatewayIndex.CYCLE_LOG_COUNT + 3, GatewayIndex.LENGTH_LOG_COUNT);
			System.arraycopy(sub_data, index + GatewayIndex.LENGTH_LOG_COUNT, payload, GatewayIndex.CYCLE_LOG_SIZE + 3, GatewayIndex.LENGTH_LOG_SIZE);
			System.arraycopy(sub_data, index + GatewayIndex.LENGTH_LOG_SIZE, payload, GatewayIndex.EVENT_LOG_COUNT + 3, GatewayIndex.LENGTH_LOG_COUNT);
			System.arraycopy(sub_data, index + GatewayIndex.LENGTH_LOG_COUNT, payload, GatewayIndex.EVENT_LOG_SIZE + 3, GatewayIndex.LENGTH_LOG_SIZE);
			break;
		default:
			break;

		}
		payload[GatewayIndex.PACKET_ID] = type;
		System.arraycopy(gw.number, 0, payload, GatewayIndex.GW_NUM, gw.number.length);
		System.arraycopy(gw.MAC, 0, payload, GatewayIndex.GW_MAC, gw.MAC.length);
		payload[GatewayIndex.GW_POWER] = gw.power_state;
		payload[GatewayIndex.GW_BATTERY] = gw.battery_state;
		System.arraycopy(byte_time, 0, payload, GatewayIndex.TIME_TIC, byte_time.length);
		
		return payload;
	}
	
	private byte getEventMSGID(byte msg_id, byte[] subData) {
		byte return_id = 0;
		byte sub_data = subData.length == 1 ? subData[0] : 0x01;
		if (msg_id == MessageID.EMG_KEY_MSG) {
			switch (sub_data) {
			case 0x01:
				return_id = GateWayMessage.ID_GW_EVENT_CANCEL;
				break;
			case 0x10:
				return_id = GateWayMessage.ID_GW_EVENT_EMG;
				break;
			case 0x20:
				return_id = GateWayMessage.ID_GW_EVENT_CENTOR;
				break;
			}
		}
		else if (msg_id ==  MessageID.PWR_STAT_MSG)
		{
			return_id = GateWayMessage.ID_GW_EVENT_POWER;
		}
		//NodeStatMSG 에서 Low Batt 을 검사하여 전송하기 때문에 MessageID는 NODE_STAT_MSG이다.
		else if (msg_id == MessageID.NODE_STAT_MSG) {
			return_id = GateWayMessage.ID_GW_EVENT_LOWBATT;
		}
				
		return return_id;
	}
	
	private byte getEventMSGData(byte msg_id, byte[] subData) {
		byte msg_data = 0;
		
		switch (msg_id) {
		case GateWayMessage.ID_GW_EVENT_EMG:
		case GateWayMessage.ID_GW_EVENT_CENTOR:
		case GateWayMessage.ID_GW_EVENT_CANCEL:
			msg_data = ProtocolConfig.DEFAULT_GW_EVENT_MSG_DATA;
			break;
		case GateWayMessage.ID_GW_EVENT_POWER:			
			msg_data = (byte) ((subData[0]==0) ? 1 : 0); 
			break;
		default:
			msg_data = ProtocolConfig.DEFAULT_GW_EVENT_MSG_DATA;
			break;
		}
		return msg_data;
	}
	
	private byte getSensorData(byte msg_id, byte[] frameSubData){
		byte sensorData = 0;
		switch(msg_id){
		case MessageID.FIRE_DET_MSG:
			sensorData = getSensorAlertData(frameSubData[frameSubData.length -1]);
			break;
		case MessageID.GAS_DET_MSG:
			sensorData = getSensorAlertData(frameSubData[frameSubData.length -1]);
			break;
		case MessageID.EMG_KEY_MSG:
			sensorData = GateWayMessage.DATA_CANCEL_ALERT;
			break;
		case MessageID.DOOR_STAT_MSG:
			
			break;
		case MessageID.PIR_CNT_MSG: 
			sensorData = GateWayMessage.DATA_DETECT_ACTIVITY;
			break;
		case MessageID.CUR_PIR_CNT_MSG:
			sensorData= GateWayMessage.DATA_DETECT_MISSED_ACTIVITY;
			break;
		}
		return sensorData;
	}
	
	private byte getSensorAlertData(byte sub_data){
		byte sensorData = 0;
		switch(sub_data){
		case 0x00:
			sensorData = GateWayMessage.DATA_CANCEL_ALERT;
			break;
		case 0x01:
			sensorData = GateWayMessage.DATA_WARN_ALERT;
			break;
		}
		return sensorData;
	}
	
	public byte[] getPayload() {
		return packet_payload;
	}
	
	public byte getPacketID(){
		return packet_payload[0];
	}
	
	public boolean isACK() {
		return isACK;
	}

	public byte[] getBytes() {
		int size;
		byte data[];

		size = 5 + packet_payload.length;

		data = new byte[size];

		data[GatewayIndex.PACKET_START] = packet_start;
		
		byte[] pack_length = ByteBuffer.allocate(2).putShort((short)(packet_payload.length + 3)).array();
		
		System.arraycopy(pack_length, 0, data, GatewayIndex.PACKET_LENGTH, pack_length.length);
		
		System.arraycopy(packet_payload, 0, data, GatewayIndex.PACKET_PAYLOAD, packet_payload.length);

		data[data.length - 2] = CheckSum.getProtocolCheckSum(data, packet_payload.length);
		data[data.length - 1] = packet_end;

		return data;
	}
}
