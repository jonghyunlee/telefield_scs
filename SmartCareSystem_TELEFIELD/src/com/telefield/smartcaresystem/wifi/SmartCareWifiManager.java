package com.telefield.smartcaresystem.wifi;

import android.content.Context;
import android.net.wifi.WifiManager;

public class SmartCareWifiManager {
	Context mContext;                       
	WifiManager wifiManager;
	
	public SmartCareWifiManager(Context mContext) {
		this.mContext = mContext;
		
		wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
	}
	
	public void turnOnWifi() {
		wifiManager.setWifiEnabled(true);
	}
	
	public void turnOffWifi() {
		wifiManager.setWifiEnabled(false);
	}
	
	public boolean isWifiEnable() {
		return wifiManager.isWifiEnabled();
	}
}
