package com.telefield.smartcaresystem;

import java.io.File;
import java.nio.channels.AlreadyConnectedException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.joda.time.DateTime;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.speech.tts.TextToSpeech;
import android.telephony.TelephonyManager;

import com.google.android.gcm.GCMRegistrar;
import com.telefield.smartcaresystem.bluetooth.SmartCareHealthApp;
import com.telefield.smartcaresystem.diagnostic.DiagnosticManager;
import com.telefield.smartcaresystem.firmware.UpgradeController;
import com.telefield.smartcaresystem.firmware.codi.CodiFirmwareUpgrade;
import com.telefield.smartcaresystem.function.SensorRegisterFunction;
import com.telefield.smartcaresystem.http.AsyncCallback;
import com.telefield.smartcaresystem.http.AsyncFileDownloader;
import com.telefield.smartcaresystem.http.HttpRequestHelper;
import com.telefield.smartcaresystem.lcd.SmartCareSystemLCDManager;
import com.telefield.smartcaresystem.m2m.ProtocolManger;
import com.telefield.smartcaresystem.m2m.SubProtocolManger;
import com.telefield.smartcaresystem.message.MessageID;
import com.telefield.smartcaresystem.message.MessageManager;
import com.telefield.smartcaresystem.power.SmartCareSystemPowerManager;
import com.telefield.smartcaresystem.sensor.SensorDeviceManager;
import com.telefield.smartcaresystem.telephony.PhoneManager;
import com.telefield.smartcaresystem.ui.UIConstant;
import com.telefield.smartcaresystem.ui.common.ConfirmDialogUI;
import com.telefield.smartcaresystem.ui.common.ResultDialogUI;
import com.telefield.smartcaresystem.ui.firmware.AppdownloadUI;
import com.telefield.smartcaresystem.ui.system.SystemMenuAdapterUI;
import com.telefield.smartcaresystem.util.ConfigureLog4J;
import com.telefield.smartcaresystem.util.EmergencyAction;
import com.telefield.smartcaresystem.util.EnableDisableStatusBar;
import com.telefield.smartcaresystem.util.LogMgr;
import com.telefield.smartcaresystem.util.SelfCall;
import com.telefield.smartcaresystem.util.SmartCarePreference;
import com.telefield.smartcaresystem.util.SpeechUtil;
import com.telefield.smartcaresystem.wifi.SmartCareWifiManager;

public class SmartCareSystemApplication{
	
	private static String TAG = "SmartCareSystem";
	
	private EmergencyAction emergencyCallUtil;
	private MessageManager messageManager;
	private GlobalMessageHandler globalMsgHandle;
	private SensorDeviceManager sensorDeviceManager;	
	private SensorRegisterFunction sensorRegisterFunc;	
	private SpeechUtil speechUtil;
	private PhoneManager phoneMgr;
	private ProtocolManger mProtocolManger;
	private SubProtocolManger mSubProtocolManger;
	private MySmartCareState mMyCare;
	private Timer perodic_report_timer; // 서버 주기 보고
	//private Timer pir_cnt_update_timer; // pir cnt update 
	private Timer nonAcitivty_report_timer;
	private SelfCall selfCall;
	private DiagnosticManager diagMgr;
	private SmartCareSystemPowerManager pwrMgr;
	private SmartCareHealthApp healthApp;
	private EnableDisableStatusBar setStatusBar;
	private CodiFirmwareUpgrade cfu;
	private SmartCareSystemLCDManager lcdMgr;
	Timer resetAllCallTime, deletePicture, deleteLog, loopUpdate, loopCodiUpdate;
	private Context mContext;
	WakeLock wakeLock; 
	
	private static SmartCareSystemApplication instance;
	
	private boolean isInit = false;
		
	private SmartCareSystemApplication(Context context)
	{
		this.mContext = context;		
	}
	
	public static SmartCareSystemApplication createInstance(Context context)
	{
		if( instance != null )
		{
			//종료를 시키자.
			instance.applicationExit();
			instance = null;
		}
		LogMgr.d(LogMgr.TAG, "mainApp is new create");
		instance = new SmartCareSystemApplication(context);	
		
		return instance;
	}
	
	public static SmartCareSystemApplication getInstance()
	{
		return instance;
	}
	
	public void setChangedContext(Context context)
	{
		this.mContext = context;
	}
	
	public void init() 
	{
		
		LogMgr.d(LogMgr.TAG,"init");
		
		if( isInit ) return;
		
		// initialize
		ConfigureLog4J.configure(mContext);	
		
		PowerManager powerManager = (PowerManager)mContext.getSystemService(Context.POWER_SERVICE);
		wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "SmartCareWakeLock");
		wakeLock.setReferenceCounted(false);
		
		messageManager = new MessageManager(mContext);	
		
		sensorDeviceManager = new SensorDeviceManager(mContext);		
		
		sensorRegisterFunc = new SensorRegisterFunction();	
		
		globalMsgHandle = new GlobalMessageHandler();
		
		mProtocolManger = new ProtocolManger();
		mProtocolManger.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		
//		mSubProtocolManger = new SubProtocolManger();
//		mSubProtocolManger.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		
		speechUtil = new SpeechUtil(mContext);
		speechUtil.init();		
		
		emergencyCallUtil = new EmergencyAction(mContext);
		phoneMgr = new PhoneManager(mContext);		
		mMyCare = new MySmartCareState();
		diagMgr = new DiagnosticManager();
		pwrMgr = new SmartCareSystemPowerManager(mContext);
		healthApp = new SmartCareHealthApp(mContext);
		setStatusBar = new EnableDisableStatusBar(mContext);
		lcdMgr = new SmartCareSystemLCDManager(mContext);
		
//		if(!Constant.IS_CAN_INSERT_EVERY_MINUTE_PIRCNT)
//		{			
//			//현재 시간을 가져와서 delaytime 을 정한다.
//			long delay_time=0;
//			
//			Calendar curr = Calendar.getInstance();
//			Calendar nextTime = Calendar.getInstance();
//			
//			nextTime.set(Calendar.HOUR_OF_DAY, curr.get(Calendar.HOUR_OF_DAY) + 1);
//			nextTime.set(Calendar.MINUTE, 1); //정각 1분후
//			nextTime.set(Calendar.SECOND, 0);
//			nextTime.set(Calendar.MILLISECOND,0);		
//			
//			delay_time = nextTime.getTimeInMillis() - curr.getTimeInMillis();
//			
//			if( delay_time < 0)
//			{
//				delay_time = Constant.DEFAULT_PERIODC_REPORT_TIME;
//			}
//			
//			pir_cnt_update_timer = new Timer();
//			pir_cnt_update_timer.scheduleAtFixedRate(new TimerTask() {			
//				@Override
//				public void run() {
//					// TODO Auto-generated method stub
//					Intent intent = new Intent(mContext, UpdateActivityPirCountService.class);			
//					mContext.startService(intent);
//					
//				}
//			}, delay_time, Constant.DEFAULT_PERIODC_REPORT_TIME);
//		}
		
		startPeriodReportTimer();
		startNonActivityReportTimer();
		resetLimitCallTimer();
		deletePictureTimer();
		deleteLogTimer();
		isInit = true;
		
//		loopSmartphoneUpdate();
//		loopCodiUpdate();
		
//		LogMgr.SERVERDEBUG = true;
//		new LogMgr().openSocket();
	}
	
	public void applicationExit()
	{		
		LogMgr.d(LogMgr.TAG,"applicationExit");
		
		speechUtil.destroyTTS();		
		stopPeriodReportTimer();
		stopNonActivityReportTimer();
		
//		if(!Constant.IS_CAN_INSERT_EVERY_MINUTE_PIRCNT)
//		{
//			if( pir_cnt_update_timer != null )
//			{		
//				pir_cnt_update_timer.cancel();
//				pir_cnt_update_timer.purge();
//				pir_cnt_update_timer = null;
//			}
//		}
		
		mContext.stopService(new Intent(Constant.PERIDIC_SERVICE));
	}
	
	public EmergencyAction getEmergencyCallUtil()
	{
		return emergencyCallUtil;
	}
	
	public MessageManager getMessageManager()
	{
		return messageManager;
	}
	
	public SensorRegisterFunction getSensorRegisterFunc()
	{
		return sensorRegisterFunc;
	}

	public GlobalMessageHandler getGlobalMsgFunc()
	{
		return globalMsgHandle;
	}
	
	public SensorDeviceManager getSensorDeviceManager()
	{
		return sensorDeviceManager;
	}
	
	public ProtocolManger getProtocolManger()
	{
		return mProtocolManger;
	}
	
	public SubProtocolManger getSubProtocolManger(){
		return mSubProtocolManger;
	}
	
	public SmartCareHealthApp getSmartCareHealthApp(){
		return healthApp;
	}
	
	public void updateProtocolManager(){
		mProtocolManger.stop();
		mProtocolManger.cancel(true);

		while(!mProtocolManger.isCancelled()){
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		mProtocolManger = new ProtocolManger();
		mProtocolManger.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		LogMgr.d(LogMgr.TAG, "SmartCareSystemApplication.java:updateProtocolManager:// mProtocolManger : " + mProtocolManger);

	}
	
	public void updateSubProtocolManger(){
		mSubProtocolManger.stop();
		mSubProtocolManger.cancel(true);

		while(!mSubProtocolManger.isCancelled()){
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		mSubProtocolManger = new SubProtocolManger();
		mSubProtocolManger.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}
	
	public SpeechUtil getSpeechUtil() {
		return speechUtil;
	}
	
	public Context getApplicationContext(){
		return mContext;
	}
	
	public PhoneManager getPhoneManager()
	{
		return phoneMgr;
	}
	
	public MySmartCareState getMySmartCareState()
	{
		return mMyCare;
	}
	
	public DiagnosticManager getDiagnosticManager()
	{
		return diagMgr;
	}
	
	public void setSelfCall(SelfCall selfCall)
	{
		this.selfCall = selfCall;
	}
	
	public SelfCall getSelfCall() {
		return selfCall;
	}
	
	/**
	 * 서버 주기 보고 설정 시간에 따라 동작 하도록 수정
	 */
	public void startPeriodReportTimer()
	{
		
		SmartCarePreference pref = new SmartCarePreference(mContext, SmartCarePreference.SERVER_SETTING_INFO_FILENAME);
		int value = pref.getIntValue(SmartCarePreference.SERVER_PERIODIC_REPORT_TIME,/*UIConstant.DEFAULT_PERIODIC_REPORT_TIME_VALUE*/ UIConstant.DEFAULT_PERIODIC_SEND_TIME_VALUE);
		
		//주기보고 최소 시간을 60분으로 한다
		if(value < 60)
			value = 60;
		
		long period_time = value * 60 * 1000;
		
		LogMgr.d(LogMgr.TAG,"startPeriodReportTimer :: " + period_time);
		
		/*		
						
		perodic_report_timer = new Timer();
		perodic_report_timer.scheduleAtFixedRate(new TimerTask() {			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mContext, PeriodicalReportService.class);
				intent.putExtra(PeriodicalReportService.REPORT_PARAM, PeriodicalReportService.PERIODIC_REPORT);
				mContext.startService(intent);
				
			}
		}, period_time, period_time);		
		*/
		
		PendingIntent check_sender = PendingIntent.getBroadcast(mContext, 0, new Intent(Constant.PERIODIC_REPORT_ACTION), PendingIntent.FLAG_NO_CREATE);		
		AlarmManager alarm = (AlarmManager)mContext.getSystemService(Context.ALARM_SERVICE);
		
		if( check_sender != null)
		{			
		    PendingIntent deleteSender = PendingIntent.getBroadcast(mContext, 0, new Intent(Constant.PERIODIC_REPORT_ACTION), PendingIntent.FLAG_UPDATE_CURRENT);
		    		    
		    if(deleteSender != null){
		    	alarm.cancel(deleteSender);
		    	deleteSender.cancel();
		    }
		}	
		
		PendingIntent sender = PendingIntent.getBroadcast(mContext, 0, new Intent(Constant.PERIODIC_REPORT_ACTION), PendingIntent.FLAG_UPDATE_CURRENT);
		
		Calendar calendar = Calendar.getInstance();	
		calendar.add(Calendar.MINUTE, value);				
		
		alarm.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), period_time, sender);
	}
	/**
	 * 매월 1일 0시 0분에 통화량 제한 시간을 초기화<br>
	 * 1분에 한번씩만 검사한다. 
	 */
	public void resetLimitCallTimer(){
		if(resetAllCallTime == null){
			resetAllCallTime = new Timer();
			resetAllCallTime.schedule(new TimerTask() {		
				@Override
				public void run() {
					if (1 == DateTime.now().getDayOfMonth()
							&& 0 == DateTime.now().getHourOfDay()
							&& 0 == DateTime.now().getMinuteOfHour()){
						SmartCarePreference pref = new SmartCarePreference(SmartCareSystemApplication.getInstance().getApplicationContext(),SmartCarePreference.CALL_SETTING_INFO_FILENAME);
						pref.setIntValue(SmartCarePreference.ALL_CALL_TIME, 0);		
						LogMgr.d(TAG, "SmartCarePreference.ALL_CALL_TIME reset : " + pref.getIntValue(SmartCarePreference.ALL_CALL_TIME, 60*60*1000));//초기화가 안되면 1시간으로 ..
					}	
				}
			}, 1000, 60 * 1000);
		}
	}
	public void loopCodiUpdate(){
		if(loopCodiUpdate == null){
			loopCodiUpdate = new Timer();
			loopCodiUpdate.schedule(new TimerTask() {		
				@Override
				public void run() {
					String tel_number = "01051498848";
					File file = new File(Constant.FIRMWARE_PATH);
					if(!file.exists())
						file.mkdirs();
					new AsyncFileDownloader(mContext).download(Constant.ZIGBEE_FW_DOWNLOAD_PATH + "/" + tel_number, Constant.FW_GATEWAY_PATH, fileDownCallback);

				}
			}, 5 * 60 * 1000, 10 * 60 * 1000);
		}
	}

	private AsyncCallback<File> fileDownCallback = new AsyncCallback.Base<File>() {
		@Override
		public void onResult(File result) {
			LogMgr.d("APP_UPDATE","APP_UPDATE_DOWNLOAD_END");

			LogMgr.d(LogMgr.TAG, "ConfirmDialogUI.java:enclosing_method:// result.getAbsolutePath() : " + result.getAbsolutePath());
			LogMgr.d(LogMgr.TAG, "ConfirmDialogUI.java:enclosing_method:// result.length() : " + result.length());
			
			if( result!=null && Constant.FW_GATEWAY_PATH.equals(result.getAbsolutePath()))
			{				
				SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.START_CODI_FIRMWARE_UPDATE, TextToSpeech.QUEUE_FLUSH, null);
				
				LogMgr.d(LogMgr.TAG,"UpgradeController upgrade start");
				//펌웨어 업데이트 진행.
				UpgradeController.getInstance().upgrade(UpgradeController.CODI_FIRMWARE_UPDATE_TYPE);
				
			}
			else //application update
			{		
			}

			
		}
	};
	public void loopSmartphoneUpdate(){
		if(loopUpdate == null){
			loopUpdate = new Timer();
			loopUpdate.schedule(new TimerTask() {		
				@Override
				public void run() {
					
					LogMgr.d(LogMgr.TAG,"Firmware UI start activity");
					Context context = SmartCareSystemApplication.getInstance().getApplicationContext();
					Intent intent = new Intent(context, AppdownloadUI.class);
					intent.setAction("localUpdate");
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					context.startActivity(intent);
				}
			}, 15 * 60 * 1000, 10 * 60 * 1000);
		}
	}
	
	/**
	 * 응급사진 데이터는 1주일까지만 보관한다. <br>
	 * 1일에 한번씩만 검사한다.
	 */
	public void deletePictureTimer(){
		
		if(deletePicture == null){
			deletePicture = new Timer();
			deletePicture.schedule(new TimerTask() {		
				@Override
				public void run() {
					SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
					int beforeDate = Integer.parseInt(sdf.format(DateTime.now().minusDays(7).toDate()));
					File fList = new File(Constant.CAM_PATH);
					if(fList.exists() && fList.listFiles().length != 0){
						File[] files = fList.listFiles();
						for(File picture : files){
							String pictureName = picture.getName();
							//0~8번째가 날짜정보 yyyyMMdd
							int pictureDate = Integer.parseInt((String) pictureName.subSequence(0, 8));
	
							if(pictureDate < beforeDate){
								picture.delete();
								LogMgr.d(LogMgr.TAG ,"deletePictureTimer delete picture....pictureName : " + pictureName + "...pictureDate : " + pictureDate + "...beforeDate : " + beforeDate);
							}
						}
					}
				}
			}, 2 * 1000, 24 * 60 * 60 * 1000);
		}
	}
	/**
	 * 로그파일을 2주일까지만 보관한다. <br>
	 * 1일에 한번씩만 검사한다.
	 */
	public void deleteLogTimer(){
		
		if(deleteLog == null){
			deleteLog = new Timer();
			deleteLog.schedule(new TimerTask() {		
				@Override
				public void run() {
					SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
					int beforeDate = Integer.parseInt(sdf.format(DateTime.now().minusDays(14).toDate()));
					File fList = new File(Constant.LOG_PATH);
					if(fList.exists() && fList.listFiles().length != 0){
						File[] files = fList.listFiles();
						for(File logs : files){
							String logName = logs.getName();
							String rawDate = (String)(logName.subSequence(logName.length() - 10, logName.length()));
							if(rawDate.matches(".*txt.*"))
								continue;
							rawDate = rawDate.replace("-", "");
							int logDate = Integer.parseInt(rawDate);
							if(logDate < beforeDate){
								logs.delete();
								LogMgr.d(LogMgr.TAG, "deleteLogTimer delete log....logName : " + logName + "...logDate : " + logDate + "...beforeDate : " + beforeDate);
							}
						}
					}
				}
			}, 3 * 1000, 24 * 60 * 60 * 1000);
		}
	}
	
	/**
	 * 미활동량 감지 체크 report
	 */
	public void startNonActivityReportTimer()
	{
		SmartCarePreference pref = new SmartCarePreference(mContext, SmartCarePreference.SENSOR_SETTING_INFO_FILENAME);
		int value = pref.getIntValue(SmartCarePreference.NON_ACTIVITY_TIME,UIConstant.DEFAULT_NON_ACTIVITY_TIME_VALUE);		
		long period_time = value * 60 * 1000;		
		
		LogMgr.d(LogMgr.TAG,"startNonActivityReportTimer :: " + period_time);
		
//		nonAcitivty_report_timer = new Timer();
//		nonAcitivty_report_timer.scheduleAtFixedRate(new TimerTask() {			
//			@Override
//			public void run() {
//				// TODO Auto-generated method stub
//				Intent intent = new Intent(mContext, PeriodicalReportService.class);
//				intent.putExtra(PeriodicalReportService.REPORT_PARAM, PeriodicalReportService.NON_ACTIVITY_REPORT);				
//				mContext.startService(intent);
//				
//			}
//		}, period_time, period_time);		
		
		PendingIntent check_sender = PendingIntent.getBroadcast(mContext, 0, new Intent(Constant.NON_ACTIVITY_REPORT_ACTION), PendingIntent.FLAG_NO_CREATE);		
		AlarmManager alarm = (AlarmManager)mContext.getSystemService(Context.ALARM_SERVICE);
		
		if( check_sender != null)
		{			
		    PendingIntent deleteSender = PendingIntent.getBroadcast(mContext, 0, new Intent(Constant.NON_ACTIVITY_REPORT_ACTION), PendingIntent.FLAG_UPDATE_CURRENT);
		    		    
		    if(deleteSender != null){
		    	alarm.cancel(deleteSender);
		    	deleteSender.cancel();
		    }
		}	
		
		PendingIntent sender = PendingIntent.getBroadcast(mContext, 0, new Intent(Constant.NON_ACTIVITY_REPORT_ACTION), PendingIntent.FLAG_CANCEL_CURRENT);
		
		Calendar calendar = Calendar.getInstance();
		//calendar.setTimeInMillis(System.currentTimeMillis());
		calendar.add(Calendar.MINUTE, value + 1 ); //1분을 더해준다.		
		
		//alarm.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), period_time, sender);		
		alarm.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), period_time, sender);
		
	}
	
	/**
	 * 서버 주기 보고 시간 설정 변경시 stop 후 재실행 하도록
	 * 주기 보고 멈춤
	 */
	public void stopPeriodReportTimer()
	{
		LogMgr.d(LogMgr.TAG,"stopPeriodReportTimer");
		
//		if( perodic_report_timer != null)
//		{
//			perodic_report_timer.cancel();
//			perodic_report_timer.purge();
//			perodic_report_timer = null;
//		}
		
		
		AlarmManager alarm = (AlarmManager)mContext.getSystemService(Context.ALARM_SERVICE);
		PendingIntent checkSender = PendingIntent.getBroadcast(mContext, 0, new Intent(Constant.PERIODIC_REPORT_ACTION), PendingIntent.FLAG_NO_CREATE);
		
		if( checkSender != null)
		{			
		    PendingIntent deleteSender = PendingIntent.getBroadcast(mContext, 0, new Intent(Constant.PERIODIC_REPORT_ACTION), PendingIntent.FLAG_UPDATE_CURRENT);
		    		    
		    if(deleteSender != null)
		    {
		    	alarm.cancel(deleteSender);
		    	deleteSender.cancel();
		    }
		}	    
		
	}
	
	/**
	 * 서버 주기 보고 시간 설정 변경시 stop 후 재실행 하도록
	 * 주기 보고 멈춤
	 */
	public void stopNonActivityReportTimer()
	{
		LogMgr.d(LogMgr.TAG,"stopNonActivityReportTimer");
//		if( nonAcitivty_report_timer != null)
//		{
//			nonAcitivty_report_timer.cancel();
//			nonAcitivty_report_timer.purge();
//			nonAcitivty_report_timer = null;
//		}
		
		AlarmManager alarm = (AlarmManager)mContext.getSystemService(Context.ALARM_SERVICE);
		PendingIntent checkSender = PendingIntent.getBroadcast(mContext, 0, new Intent(Constant.NON_ACTIVITY_REPORT_ACTION), PendingIntent.FLAG_NO_CREATE);
		
		if( checkSender != null)
		{			
		    PendingIntent deleteSender = PendingIntent.getBroadcast(mContext, 0, new Intent(Constant.NON_ACTIVITY_REPORT_ACTION), PendingIntent.FLAG_UPDATE_CURRENT);
		    		    
		    if(deleteSender != null){
		    	alarm.cancel(deleteSender);
		    	deleteSender.cancel();
		    }
		}
	}
	
	public String getMyPhoneNumber() {
		TelephonyManager telephonyManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
		
		String number = telephonyManager.getLine1Number();
		
		if(number != null)
		{			
			if(number.startsWith("+82"))
			{
				number = number.replace("+82", "0");
			}			
		}else{
		
		}
		return number;
	}

	public SmartCareSystemPowerManager getPwrMgr() {
		return pwrMgr;
	}
	
	
	public void acquireWakeLock()
	{
		wakeLock.acquire();
	}
	
	public void releaseWakeLock()
	{
		wakeLock.release();
	}
	
	public EnableDisableStatusBar getEnableDisableStatusBar() {
		return setStatusBar;
	}	
	
	public void setCodiFirmwareUpgrade(CodiFirmwareUpgrade cfu)
	{
		this.cfu = cfu;
	}
	
	public CodiFirmwareUpgrade getCodiFirmwareUpgrade()
	{
		return cfu;
	}
	
	public SmartCareSystemLCDManager getLCDMgr() {
		return lcdMgr;
	}
}
