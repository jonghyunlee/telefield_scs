package com.telefield.smartcaresystem.bluetooth;

import java.util.Set;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.widget.Toast;

import com.telefield.smartcaresystem.util.LogMgr;

public class SmartCareHealthApp {
	
	private Context mContext;
	
	
	// Use the appropriate IEEE 11073 data types based on the devices used.
    // Below are some examples.  Refer to relevant Bluetooth HDP specifications for detail.
    //     0x1007 - blood pressure meter
    //     0x1008 - body thermometer
    //     0x100F - body weight scale
    private static final int HEALTH_PROFILE_SOURCE_DATA_TYPE = 0x1007;  
    private Messenger mHealthService;
    private boolean mHealthServiceBound;
    
    private int sys,dia,pulse;
    
    public SmartCareHealthApp(Context context)
    {
    	this.mContext = context;
    	
    	mHealthServiceBound=false;
    }
    
    // Handles events sent by {@link HealthHDPService}.
    private Handler mIncomingHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                // Application registration complete.
                case BluetoothHDPService.STATUS_HEALTH_APP_REG:
                   
                    break;
                // Application unregistration complete.
                case BluetoothHDPService.STATUS_HEALTH_APP_UNREG:
                   
                    break;
                // Reading data from HDP device.
                case BluetoothHDPService.STATUS_READ_DATA:
                  
                    break;
                // Finish reading data from HDP device.
                case BluetoothHDPService.STATUS_READ_DATA_DONE:
                  
                    break;
                // Channel creation complete.  Some devices will automatically establish
                // connection.
                case BluetoothHDPService.STATUS_CREATE_CHANNEL:
                    
                    break;
                // Channel destroy complete.  This happens when either the device disconnects or
                // there is extended inactivity.
                case BluetoothHDPService.STATUS_DESTROY_CHANNEL:                  
                    break;
                case BluetoothHDPService.RECEIVED_SYS:
                	sys = msg.arg1;
                	break;
                case BluetoothHDPService.RECEIVED_DIA:
                	dia =msg.arg1;
                	break;
                case BluetoothHDPService.RECEIVED_PUL:
                	pulse = msg.arg1;
                	Toast.makeText( mContext , "최고혈압:" + sys + " 최저혈압:" + dia + "맥박수:"+pulse, Toast.LENGTH_SHORT).show();
                	break;
                default:
                    super.handleMessage(msg);
            }
        }
    };

    private final Messenger mMessenger = new Messenger(mIncomingHandler);
    
    // Sets up communication with {@link BluetoothHDPService}.
    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName name, IBinder service) {
            mHealthServiceBound = true;
            Message msg = Message.obtain(null, BluetoothHDPService.MSG_REG_CLIENT);
            msg.replyTo = mMessenger;
            mHealthService = new Messenger(service);
            try {
                mHealthService.send(msg);
            } catch (RemoteException e) {
                LogMgr.d(LogMgr.TAG, "Unable to register client to service.");
                e.printStackTrace();
            }
            
            sendMessage(BluetoothHDPService.MSG_REG_HEALTH_APP,HEALTH_PROFILE_SOURCE_DATA_TYPE);
            
        }

        public void onServiceDisconnected(ComponentName name) {
            mHealthService = null;
            mHealthServiceBound = false;
        }
    };
    
 // Sends a message to {@link BluetoothHDPService}.
    private void sendMessage(int what, int value) {
        if (mHealthService == null) {
        	LogMgr.d(LogMgr.TAG, "Health Service not connected.");
            return;
        }

        try {
            mHealthService.send(Message.obtain(null, what, value, 0));
        } catch (RemoteException e) {
        	LogMgr.d(LogMgr.TAG, "Unable to reach service.");
            e.printStackTrace();
        }
    }
    
    public void bloodPressureStartService() {
    	boolean isFindBloodPressure = false;
    	
    	BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter(); 
    	
    	 if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
    		 LogMgr.d(LogMgr.TAG,"BT is not available");
    		 return;
    	 }
    	
    	Set<BluetoothDevice> pairedDevices  = mBluetoothAdapter.getBondedDevices();
    	
    	if( pairedDevices != null && pairedDevices.size() > 0 )
    	{
    		for( BluetoothDevice device : pairedDevices)
    		{
    			int bluetoothClass = device.getBluetoothClass().getDeviceClass();
    			
    			if( bluetoothClass == BluetoothClass.Device.HEALTH_BLOOD_PRESSURE)
    			{
    				isFindBloodPressure = true;
    				break;
    			}
    		}
    	}
    	
    	if( isFindBloodPressure == false) return;
    	
        // Starts health service.
        Intent intent = new Intent(mContext, BluetoothHDPService.class);
        mContext.startService(intent);
        mContext.bindService(intent, mConnection, Context.BIND_AUTO_CREATE);       
        
    } 
    
    public void bloodPressureStopService()
    {	
    	Intent intent = new Intent(mContext, BluetoothHDPService.class);
    	if (mHealthServiceBound)  mContext.unbindService(mConnection);    	
    }

}
