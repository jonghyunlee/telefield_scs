package com.telefield.smartcaresystem.bluetooth;

import java.lang.reflect.Method;
import java.util.Set;

import com.telefield.smartcaresystem.util.LogMgr;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;

public class DeviceScan {
	private BluetoothAdapter mBluetoothAdapter;

	public DeviceScan() {
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
	}

	public void scan() {
		mBluetoothAdapter.startDiscovery();
	}

	public void pairDevice(BluetoothDevice device) {
		LogMgr.d(LogMgr.TAG, "DeviceScan.java:pairDevice:// device.getName() : " + device.getName());
		LogMgr.d(LogMgr.TAG, "DeviceScan.java:pairDevice:// device.getBondState() : " + device.getBondState());

		try {
			Method method = device.getClass().getMethod("createBond", (Class[]) null);
			method.invoke(device, (Object[]) null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void unPairDevice(BluetoothDevice device) {
		LogMgr.d(LogMgr.TAG, "DeviceScan.java:unPairDevice:// device.getName() : " + device.getName());
		LogMgr.d(LogMgr.TAG, "DeviceScan.java:unPairDevice:// device.getBondState() : " + device.getBondState());

		try {
			Method method = device.getClass().getMethod("removeBond", (Class[]) null);
			method.invoke(device, (Object[]) null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void turnOnBluetooth() {
		if(mBluetoothAdapter.getState() == BluetoothAdapter.STATE_TURNING_OFF || mBluetoothAdapter.getState() == BluetoothAdapter.STATE_OFF) {
			mBluetoothAdapter.enable();
		}
	}
	
	public void turnOffBluetooth() {
		mBluetoothAdapter.disable();
	}
	
	public void cancelScan() {
		mBluetoothAdapter.cancelDiscovery();
	}
	
	public boolean isTurnOn() {
		return mBluetoothAdapter.isEnabled();
	}
	
	public boolean isDiscovering() {
		return mBluetoothAdapter.isDiscovering();
	}
	
	public Set<BluetoothDevice> getPairedDevice() {
		Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
		
		return pairedDevices;
	}
}
