package com.telefield.smartcaresystem.firmware.codi;

import java.io.File;
import java.io.RandomAccessFile;

import android.content.Context;

import com.telefield.smartcaresystem.Constant;
import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.message.FormattedFrame;
import com.telefield.smartcaresystem.message.MessageID;
import com.telefield.smartcaresystem.message.MessageManager;
import com.telefield.smartcaresystem.message.SensorID;
import com.telefield.smartcaresystem.ui.UIConstant;
import com.telefield.smartcaresystem.ui.firmware.IFirmwareUpdateCallback;
import com.telefield.smartcaresystem.util.ByteArrayToHex;
import com.telefield.smartcaresystem.util.LogMgr;
import com.telefield.smartcaresystem.util.SmartCarePreference;

class FirmwarePacketData {
	int length;
	int messageType;
	byte messageData[];
	byte checkSum; //exclusive-or

	public int getMessageType() {
		return messageType;
	}

	public byte[] getMessageData() {
		return messageData;
	}
	
	public byte[] getPacketData()
	{		
		byte packet[] = new byte[length+1];
		packet[0] = (byte)length;
		packet[1] = (byte)messageType;
		
		
		if(messageData!= null )
		{
			System.arraycopy(messageData, 0, packet, 2, messageData.length);
			
			LogMgr.d(LogMgr.TAG,"getPacketData:" + length + ":" + messageType + ":" + messageData.length);
		}
		
		packet[length] = checkSum;
		
		return packet;
		
	}

	public static FirmwarePacketData makePacket(int messageType, byte messageData[], int messageDataLen) 
	{
		FirmwarePacketData instance = new FirmwarePacketData();

		instance.messageType = messageType;
		instance.length = messageDataLen + 1 + 1;
		instance.checkSum = (byte)instance.length;		
		instance.checkSum ^= (byte)messageType;
		
		if( messageDataLen > 0 )
		{
			instance.messageData = new byte[messageDataLen];	
			System.arraycopy(messageData, 0, instance.messageData, 0, messageDataLen);			
			for(int i=0; i < messageDataLen; i++ )
			{
				instance.checkSum ^=  messageData[i];
			}
		}
		
		return instance;
	}

	public static FirmwarePacketData makePacket(byte subdata[]) {
		FirmwarePacketData instance = new FirmwarePacketData();

		instance.length = (int) subdata[0] & 0xff;
		instance.messageType = subdata[1];
		instance.messageData = new byte[instance.length - 2];
		instance.checkSum = subdata[instance.length];

		System.arraycopy(subdata,2,instance.messageData, 0, instance.length - 2);

		return instance;
	}

}

/**
 * codi 에 upgrade data 를 전달하는 기능
 * 
 * @author Administrator
 * 
 */
public class CodiFirmwareUpgrade {

	/* flash program stage */
	public static final int IDLE_STAGE = 0;
	public static final int ID_CHECK_RSP_WAITING_STAGE = 1;
	public static final int FL_RAM_READ_RSP_WAITING_STAGE = 2;
	public static final int FL_TYPE_SELECT_RSP_WAITNG_STAGE = 3;
	public static final int FL_ERASE_COMPL_WAITING_STAGE = 4;
	public static final int FL_ERASE_VERIFY_WAITING_STAGE = 5;
	public static final int FLASH_PROGRAM_COMPL_WATING_STAGE = 6;
	public static final int FLASH_PROGRAM_VERIFY_WATING_STAGE = 7;
	public static final int FLASH_PROGRAM_COMPL_STAGE = 8;

	/* flash program event */
	public static final int EVT_NONE = 0;
	public static final int EVT_ID_CHECK_REQ = 1;
	public static final int EVT_RAM_READ_REQ = 2;
	public static final int EVT_FLASH_TYPE_SEL_REQ = 3;
	public static final int EVT_FLASH_ERASE_REQ = 4;
	public static final int EVT_FLASH_ERASE_VERIFY = 5;
	public static final int EVT_FLASH_PROG_START = 6;
	public static final int EVT_FLASH_PROG_ING = 7;
	public static final int EVT_FLASH_PROG_VERIFY = 8;
	public static final int EVT_FLASH_PROG_COMPL = 9;	
	public static final int EVT_FIRMWARE_UPDATE_FAIL = 10;

	/* response status code */
	public static final int OK = 0x0;
	public static final int NOT_SUPPORT = 0xff;
	public static final int WRITE_FAIL = 0xfe;
	public static final int INVALID_RESPONSE = 0xfd;
	public static final int CRC_ERROR = 0xfc;
	public static final int ASSERT_FAIL = 0xfb;
	public static final int USER_INTTERUPT = 0xfa;
	public static final int READ_FAIL = 0xf9;
	public static final int TST_ERROR = 0xf8;
	public static final int AUTH_ERROR = 0xf7;
	public static final int NO_RESPONSE = 0xf6;

	/* time out */
	public static final int TIME_OUT = 5 * 60; // sec
	
	public static final int FLASH_READ_REQUEST = 0x0b;
	public static final int FLASH_READ_RESONSE = 0x0c;
	public static final int FLASH_RAM_READ_REQ = 0x1f;
	public static final int FLASH_RAM_READ_RSP = 0x20;
	public static final int FLASH_WRITE_START = 0x21;
	/* message type */
	public static final int FLASH_READ_ID_REQ = 0x25;
	/**
	 * #1: Status (1 byte): 0x00 (success) 0xF7 (Auth error) #2: Flash
	 * manufacturer ID (1 byte) #3: Flash device ID (1 byte)
	 */
	public static final int FLASH_READ_ID_RSP = 0x26;
	/**
	 * #1 : Flash type (1 byte) (see table below for mapping) #2: Custom
	 * programming jump address (4 bytes), LSB first (set to 0000),
	 */
	public static final int FLASH_TYPE_SELECT_REQ = 0x2c;
	public static final int FLASH_TYPE_SELECT_RSP = 0x2d;

	public static final int FLASH_ERASE_REQ = 0x07;
	/**
	 * #1: Status (1 byte): 0x00: Erase successful 0xFF: Parameter error 0xF7:
	 * Auth error
	 */
	public static final int FLASH_ERASE_RSP = 0x08;

	/**
	 * #1: Flash address (4 bytes), LSB first #2: Up to 128 bytes of data Note:
	 * Data should not span page write boundary as defined by the Flash
	 * specification.
	 */
	public static final int FLASH_PROGRAM_REQ = 0x09;
	/**
	 * #1: Status (1 byte): 0x00: Program successful 0xFF: Readback verify
	 * failed 0xF7: Auth error
	 */
	public static final int FLASH_PROGRAM_RSP = 0x0a;

	/****************************************************************************
	 * 펌웨어 업그레이드 sequence flash id req response 를 받으면 check ( id ) check ok 이면
	 * flash erase reqeust 7초까지 기다릴수 있다 response 를 받는다. flash program
	 ******************************************************************************/	
	
	int stage;
	int percentage;
	long total_size;
	FlashProgram fp;
	IFirmwareUpdateCallback resultCallback;
	boolean is_flash_prgram_started;	
	
	long write_offset;
	
	int ram_read_req_cnt;
	int flash_type_select_cnt;
	
	Context mContext;
	boolean isEnalbeVerfication;
	long verify_start_address; // verify binary start address
	long verify_size;   // verify binary size
	
	public CodiFirmwareUpgrade(Context context, boolean isEnalbeVerfication){
		this.mContext = context;
		
		SmartCarePreference pref = new SmartCarePreference(context, SmartCarePreference.HIDDEN_MENU_SETTING_INFO_FILENAME);
		this.isEnalbeVerfication = isEnalbeVerfication;
		
		if( this.isEnalbeVerfication )
		{
			verify_start_address = pref.getIntValue(SmartCarePreference.START_ADDRESS, UIConstant.DEFAULT_START_ADDRESS);
			verify_size			 = pref.getIntValue(SmartCarePreference.ADDRESS_SIZE,  UIConstant.DEFAULT_ADDRESS_SIZE);
			
		}
	}
	
	public CodiFirmwareUpgrade(Context context)
	{
		this.mContext = context;
		
		SmartCarePreference pref = new SmartCarePreference(context, SmartCarePreference.HIDDEN_MENU_SETTING_INFO_FILENAME);
		isEnalbeVerfication = pref.getBooleanValue(SmartCarePreference.FIRMWARE_VERIFICATION_SETTING, false);
		
		if( isEnalbeVerfication )
		{
			verify_start_address = pref.getIntValue(SmartCarePreference.START_ADDRESS, UIConstant.DEFAULT_START_ADDRESS);
			verify_size			 = pref.getIntValue(SmartCarePreference.ADDRESS_SIZE,  UIConstant.DEFAULT_ADDRESS_SIZE);
			
		}
		 
	}
	

	/* message packet send function */
	private int messagePacketSend(byte packet[], int length) {
		int ret;
		ret = SmartCareSystemApplication.getInstance().getMessageManager().sendMessage(packet, length);
		return ret;
	}

	public void firmwareUpgrade() {
		
		LogMgr.d(LogMgr.TAG,"firmwareUpgrade start");
		
		//시작을 알리는 firmware DL_ENTER_MSG 전달.
		byte[] sub_data = new byte[1];		
		sub_data[0] = (byte)0xAA;		
		MessageManager mgr = SmartCareSystemApplication.getInstance().getMessageManager();
		mgr.sendMessage(SensorID.getSensorId(SensorID.CODI_SENSOR_TYPE.getSensorType(), 0x01), (byte) MessageID.DL_ENTER_MSG, sub_data);
		
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		is_flash_prgram_started = false;
		ram_read_req_cnt = 0;
		flash_type_select_cnt = 0;
		
		stage = IDLE_STAGE;
		eventHandler(EVT_ID_CHECK_REQ); // first sequence		
		
	}

	public void receiveMessage(FormattedFrame ff) {
		byte[] sub_data = ff.getSub_data();
		
		if( sub_data == null )
		{
			LogMgr.d(LogMgr.TAG, "firmware receiveMessage sub data is null");
			return;
		}
				
		LogMgr.d(LogMgr.TAG, "firmware receiveMessage : " + ByteArrayToHex.byteArrayToHex(sub_data, sub_data.length));
		FirmwarePacketData packet = FirmwarePacketData.makePacket(sub_data);
		
		LogMgr.d(LogMgr.TAG,"packet reciveMessage Type: " + packet.getMessageType());

		int next_event = EVT_NONE;
		
		if( packet != null)
		{		
			byte message_data[] = packet.getMessageData();
			byte status_code =  message_data[0];
			
			if( status_code != OK)
			{
				LogMgr.d(LogMgr.TAG,"Firmware update fail stage: " + stage + " status_code : " + status_code);
				eventHandler(EVT_FIRMWARE_UPDATE_FAIL);
				return;
			}
			
			// message parsing
			switch (packet.getMessageType()) {
			case FLASH_READ_ID_RSP:
				if (stage == ID_CHECK_RSP_WAITING_STAGE)
					next_event = EVT_RAM_READ_REQ;
				break;
			case FLASH_RAM_READ_RSP:
				if( stage == FL_RAM_READ_RSP_WAITING_STAGE)
				{
					if( ram_read_req_cnt >= 4 )
						next_event = EVT_FLASH_TYPE_SEL_REQ;
					else 
						next_event = EVT_RAM_READ_REQ;
				}
				break;
			case FLASH_TYPE_SELECT_RSP:
				if (stage == FL_TYPE_SELECT_RSP_WAITNG_STAGE)
				{
					if( flash_type_select_cnt >=2)
						next_event = EVT_FLASH_ERASE_REQ;
					else
						next_event = EVT_FLASH_TYPE_SEL_REQ;
				}
				break;
			case FLASH_ERASE_RSP:
				if (stage == FL_ERASE_COMPL_WAITING_STAGE)
					next_event = EVT_FLASH_ERASE_VERIFY;
				break;
			case FLASH_READ_RESONSE:
				if( stage == FL_ERASE_VERIFY_WAITING_STAGE)				
					next_event = EVT_FLASH_PROG_START;	
				
				else if( stage == FLASH_PROGRAM_VERIFY_WATING_STAGE)
				{
					//next_event = EVT_FLASH_PROG_COMPL;
					if(message_data != null && message_data.length > 0) 
					{							
						try {
							if( fp == null )
							{
								fp = new FlashProgram();
								fp.verify_init();
							}
							
							write_offset += message_data.length-1;
							
							if( write_offset >= verify_size/*total_size*/)
							{
								fp.verify_write(message_data, 1, message_data.length-1 -(int)(write_offset-verify_size/*total_size*/));
							}else
								fp.verify_write(message_data, 1, message_data.length-1);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					
					if( write_offset < total_size )
					{
						next_event = EVT_FLASH_PROG_VERIFY;
					}else
					{
						next_event = EVT_FLASH_PROG_COMPL;
						try {
							fp.verify_end();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				break;			
				
			case FLASH_PROGRAM_RSP:
				if (stage == FLASH_PROGRAM_COMPL_WATING_STAGE)
					next_event = EVT_FLASH_PROG_ING;
				break;
			}
		}

		if (next_event == EVT_NONE)
			return;

		eventHandler(next_event);
	}

	private void eventHandler(int event) 
	{
		byte sub_data[] = null;
		int subData_length=0;
		
		LogMgr.d(LogMgr.TAG,"eventHandler :: " + event);
		
		switch (event) {
		
		case EVT_ID_CHECK_REQ:			
			
			stage = ID_CHECK_RSP_WAITING_STAGE;			
			
			sub_data = null;
			subData_length = 0;
			sendMessage(FirmwarePacketData.makePacket(FLASH_READ_ID_REQ, sub_data, subData_length).getPacketData());
			
			break;
		case EVT_RAM_READ_REQ:
		{
			byte [][]ram_read_req={
				{	   0x70, 0x15, 0x00, 0x01, 0x08, 0x00 },	
				{(byte)0x80, 0x15, 0x00, 0x01, 0x08, 0x00 },
				{	   0x00, 0x15, 0x00, 0x01, 0x16, 0x00 },
				{	   0x62, 0x00, 0x00, 0x00, 0x04, 0x00 }
					
			};
			stage = FL_RAM_READ_RSP_WAITING_STAGE;
			
			if( ram_read_req_cnt >4 )
			{
				LogMgr.d(LogMgr.TAG,"EVT_RAM_READ_REQ Error!!!!!");
				eventHandler(EVT_FIRMWARE_UPDATE_FAIL);
				return;
			}
			
			sendMessage(FirmwarePacketData.makePacket(FLASH_RAM_READ_REQ, ram_read_req[ram_read_req_cnt], ram_read_req[ram_read_req_cnt].length).getPacketData());
			ram_read_req_cnt++;
			
			break;
		}
		case EVT_FLASH_ERASE_REQ:
			stage = FL_ERASE_COMPL_WAITING_STAGE;
			
			sub_data = null;
			subData_length = 0;
			sendMessage(FirmwarePacketData.makePacket(FLASH_ERASE_REQ, sub_data, subData_length).getPacketData());
			break;
		case EVT_FLASH_TYPE_SEL_REQ:
		{
			byte [][]flash_type_select={
					{ 0x03, 0x00, 0x00, 0x00, 0x00 },	
					{ 0x00, 0x00, 0x00, 0x00, 0x00 }						
				};
			
			stage = FL_TYPE_SELECT_RSP_WAITNG_STAGE;
			
			//flash type
			//sub_data = new byte[]{0x03,0x00,0x00,0x00,0x00};
			//subData_length = sub_data.length;
			sendMessage(FirmwarePacketData.makePacket(FLASH_TYPE_SELECT_REQ, flash_type_select[flash_type_select_cnt]/*sub_data*/, flash_type_select[flash_type_select_cnt].length).getPacketData());
			flash_type_select_cnt++;
			break;
		}
		case EVT_FLASH_ERASE_VERIFY:
			stage = FL_ERASE_VERIFY_WAITING_STAGE;
			
			sub_data = new byte[]{0x00,0x00,0x00,0x00,(byte)0x80,0x00};
			subData_length = sub_data.length;
			sendMessage(FirmwarePacketData.makePacket(FLASH_READ_REQUEST, sub_data, subData_length).getPacketData());
			break;
		case EVT_FLASH_PROG_START:
			
			LogMgr.d(LogMgr.TAG,"EVT_FLASH_PROG_START");
			
			fp = new FlashProgram();			
			try {
				fp.init();
				
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			is_flash_prgram_started = true;
		case EVT_FLASH_PROG_ING:
			
			if( fp == null )
			{
				LogMgr.d(LogMgr.TAG,"firmware download fail: fp is null");
				return;
			}
			
			//first, firmware data file read.
			stage = FLASH_PROGRAM_COMPL_WATING_STAGE;
		
			try
			{
				int read_len = fp.flash();
				if( read_len > 0 )
				{					
					
					//eventHandler(EVT_FLASH_PROG_ING);
					//firmware download success					
					LogMgr.d(LogMgr.TAG,"flash program ing send size : " + read_len);
					
				}
				//else
				{					
					if( is_flash_prgram_started && fp.isTransferCompleted())
					{
						fp.end();
						fp = null;
						
						if( !isEnalbeVerfication )
						{							
							eventHandler(EVT_FLASH_PROG_COMPL);
							
						}else
						{						
							eventHandler(EVT_FLASH_PROG_VERIFY);
						}
						
						//firmware download complete
						LogMgr.d(LogMgr.TAG,"firmware download complete");
						
						return;
					}else if( read_len <=0 )
					{
						fp.end();
						fp = null;
						
						is_flash_prgram_started = false;
						eventHandler(EVT_FIRMWARE_UPDATE_FAIL);
						//firmware download fail
						LogMgr.d(LogMgr.TAG,"firmware download fail: read_len < 0");
						
						return;
					}
				}
			}catch(Exception e){
				LogMgr.d(LogMgr.TAG,"firmware download fail : exception : " + e);
				
				try {
					fp.end();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				fp = null;
				is_flash_prgram_started = false;
				eventHandler(EVT_FIRMWARE_UPDATE_FAIL);
				return;
			}
			
			if( resultCallback != null)
				resultCallback.notify(IFirmwareUpdateCallback.RESULT_OK, percentage, null);
			break;
		case EVT_FLASH_PROG_VERIFY:
			stage = FLASH_PROGRAM_VERIFY_WATING_STAGE;	
			
	
			sub_data = new byte[]{(byte)0x80,0x3e,0x02,0x00,(byte)0x80,0x00};
			sub_data[0] = (byte)(write_offset & 0xff);
			sub_data[1] = (byte)((write_offset & 0xff00) >> 8);
			sub_data[2] = (byte)((write_offset & 0xff0000) >> 16);
			sub_data[3] = (byte)((write_offset & 0xff0000) >> 24);	
			
			subData_length = sub_data.length;
			sendMessage(FirmwarePacketData.makePacket(FLASH_READ_REQUEST, sub_data, subData_length).getPacketData());
			
			break;
		case EVT_FLASH_PROG_COMPL:
			stage = FLASH_PROGRAM_COMPL_STAGE;						
			is_flash_prgram_started = false;
			
			//flash program 완료시에 진행해야 할 사항.
			//noti 를 해준다. to do
			if( resultCallback != null)
				resultCallback.notify(IFirmwareUpdateCallback.RESULT_COMPL, 100, null);
			
			//write 하도록 명령 전달.
			sub_data = null;
			subData_length = 0;
			sendMessage(FirmwarePacketData.makePacket(FLASH_WRITE_START, sub_data, subData_length).getPacketData());
			
			break;
		case EVT_FIRMWARE_UPDATE_FAIL:
			//firmware update fail
			//noti to do
			if( resultCallback != null)
				resultCallback.notify(IFirmwareUpdateCallback.RESULT_FAIL, percentage, null);
			break;
		
		default:
			LogMgr.d(LogMgr.TAG,"Firmware Download message Type is not support :: " + event);
			break;
		}
	}
	
	private void sendMessage(byte sub_data[]) 
	{		
		if( sub_data != null)
		{
			SmartCareSystemApplication.getInstance().getMessageManager().sendMessage(SensorID.getSensorId(SensorID.CODI_SENSOR_TYPE.getSensorType(), 0x01), (byte) MessageID.DL_PKT_MSG, sub_data);
		}else
			LogMgr.d(LogMgr.TAG,"firmware data is null ERROR!!");
	}
	
	public void setListener(IFirmwareUpdateCallback resultCallback)
	{
		this.resultCallback = resultCallback;
	}
	
	class FlashProgram 
	{
		//File file;
		RandomAccessFile file;
		//BufferedReader br;
		
		RandomAccessFile verify_file;
		long verify_offset;
		
		long offset;
		long seek_address;
		boolean isCompleted = false;
		final int MAX_BUF_SIZE = 128;
		
		private void init() throws Exception
		{
			//file = new File(Constant.FW_GATEWAY_PATH);			
			file = new RandomAccessFile(Constant.FW_GATEWAY_PATH,"r");
			{
				//br = new BufferedReader(new FileReader(file));
				total_size = (int) file.length() -4; //4 byte ignore
				LogMgr.d(LogMgr.TAG, "Firmware File Size is " + total_size);
			}			
			
			seek_address = 4;
		}
		
		private void end() throws Exception 
		{
			//if( br != null)
			//{
			//	br.close();
			//	br = null;
			//}
			
			file.close();
		}

		/********************************************
		 * else false
		 * FlashProgram 가 완료가 되면 true return
		 ********************************************/
		public int flash() throws Exception {
			byte buffer[] = new byte[MAX_BUF_SIZE];
			int read_len =0;
			try
			{				
				//if( br == null ) throw new Exception("BufferedReader Null Error");
				if( file == null ) throw new Exception("RamdomAccessFile Null Error");
				
				file.seek(seek_address);
											
				read_len = file.read(buffer, 0, MAX_BUF_SIZE);
				
				LogMgr.d(LogMgr.TAG,"flash read file size : " + read_len + "offset : " + offset);
					
				if( read_len  > 0 )
				{
					//up to 128byte  flash data
					byte sub_data[] =new byte[read_len+4];
					
					//4byte lsb flash address
					sub_data[0] = (byte)(offset & 0xff);
					sub_data[1] = (byte)((offset & 0xff00) >> 8);
					sub_data[2] = (byte)((offset & 0xff0000) >> 16);
					sub_data[3] = (byte)((offset & 0xff0000) >> 24);
					
					
					for( int i=0 ; i < read_len ; i++)
					{
						sub_data[i+4] = (byte)buffer[i];
					}					
					
					//System.arraycopy(buffer, 0, sub_data, 4, read_len);					
										
					//to do flash data send
					FirmwarePacketData packetData = FirmwarePacketData.makePacket(FLASH_PROGRAM_REQ, sub_data, read_len+4);
					LogMgr.d(LogMgr.TAG,"packetData make");
					LogMgr.d(LogMgr.TAG, "firmware send Message : " + ByteArrayToHex.byteArrayToHex(packetData.getPacketData(), packetData.getPacketData().length));
					
					byte packet[] = packetData.getPacketData();
					LogMgr.d(LogMgr.TAG,"get packetData:: len" + packet.length);
					sendMessage(packet);
					LogMgr.d(LogMgr.TAG,"sendMessage complete::offset : " + offset +" send size : " + read_len);					
										
					offset += read_len;
					seek_address += read_len;
					
					if(total_size > 0)
					{
						percentage = (int)((offset  * 100 / total_size )); 
						
						LogMgr.d(LogMgr.TAG, "firmware percentage : " + percentage + "total size : " +  total_size + "  offset :" + offset);
						//Toast.makeText(SmartCareSystemApplication.getInstance().getApplicationContext(), 
						//		"firmware percentage : " + percentage, Toast.LENGTH_SHORT).show();
					}
					
					if( total_size <= offset)
					{
						LogMgr.d(LogMgr.TAG,"data transfer completed");
						isCompleted = true;
					}
				}
				
			}catch (Exception e)
			{
				LogMgr.d(LogMgr.TAG,"flash Exception e:: " + e);
				read_len = -1;
			}finally{
				//end();
			}
			
			return read_len;
		}
		
		public boolean isTransferCompleted()
		{
			return isCompleted;
		}
		
		/**
		 * flash data verify 기능.
		 * @return
		 */
		public int verify_init() throws Exception
		{
			LogMgr.d(LogMgr.TAG,"verify_init");
			
			File file = new File(Constant.FW_GATEWAY_VERIFY_PATH);
			if(file.exists()){
				file.delete();
			}
			
			verify_file = new RandomAccessFile(Constant.FW_GATEWAY_VERIFY_PATH, "rw");
			
			/* 설정된 정보 가져오기 */
			//verify_start_address = 
			//verify_size =
			
			if( verify_size > total_size)
				verify_size = total_size;
			
			verify_offset = verify_start_address;
			return 1;
		}
		
		public void verify_end() throws Exception
		{
			LogMgr.d(LogMgr.TAG,"verify_end");
			if( verify_file != null)
			{
				verify_file.close();
				verify_file = null;
			}		
		}
		
		public synchronized int  verify_write(final byte[] buffer, final int buffer_offset, final int byteCount ) throws Exception 
		{
			LogMgr.d(LogMgr.TAG,"verify_write::" + verify_offset + " write count : " + byteCount );
			
//			new Thread()
//			{							
//
//				@Override
//				public void run() {
//					try {
//						verify_file.seek(verify_offset);								
//						verify_file.write(buffer, buffer_offset, byteCount); // 처음은 status code
//					} catch (IOException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}	
//					
//					verify_offset += byteCount;
//				}
//				
//				
//			}.start();
			verify_file.seek(verify_offset);				
			verify_file.write(buffer, buffer_offset, byteCount);
			
			verify_offset += byteCount;
			
			return 1;
		}
		
	}

}
