package com.telefield.smartcaresystem.firmware;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.telephony.TelephonyManager;

import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.m2m.GatewayDevice;
import com.telefield.smartcaresystem.telephony.PhoneManager;
import com.telefield.smartcaresystem.ui.firmware.FirmwareUI;
import com.telefield.smartcaresystem.util.EmergencyAction;
import com.telefield.smartcaresystem.util.LogMgr;

public class UpgradeController{
	
	public static int CODI_FIRMWARE_UPDATE_TYPE = 0;
	public static int APPLICATION_UPDATE_TYPE = 1;
	public static int SENSOR_BIN_UPDATE_TYPE = 2;
	
	static UpgradeController instance = new UpgradeController();

	int upg_type;
	
	public static int CONTROLLER_IDLE_STATE = 0;
	public static int CONTROLLER_RESERVED_STATE = 1;
	public static int CONTROLLER_RUNNING_STATE = 2;
	
	int state; //controller idle 0 , reserved 1, running 2
	
	
	private UpgradeController()
	{
	}
	
	public static UpgradeController getInstance(){
		return instance;
	}
	
	public int getState() { return state; }
	
	public void setState(int state)
	{
		this.state = state;
	}
	
	/**
	 * delay 된 upgrade 를 진행하기 위한 메소드
	 * @return
	 */
	public int updrage()
	{
		//바로 upgrade 를 진행하는게 하니고 10초 정도 후에 진행하도록 수정
		final Handler handler =new Handler();
		handler.postDelayed(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				upgrade(upg_type);
				
			}
		}, 10*1000);
		
		return 1;
	}
	
	/**
	 * @param type
	 * @return 0 : running , 1 : delay , 2: fail
	 */
	public int upgrade(int type)
	{
		LogMgr.d(LogMgr.TAG,"upgrade type : " + type + "state : " + state);
		
		if( state != CONTROLLER_IDLE_STATE ) return 2;
		
		boolean is_delay = checkUpgradeCondition(type);
		
		upg_type = type;
		
		if(!is_delay) 
		{
			state = CONTROLLER_RESERVED_STATE;
			return 1;
			
		}		
		
		switch(type)
		{
			case 0:
				LogMgr.d(LogMgr.TAG,"Firmware UI start activity");
				Context context = SmartCareSystemApplication.getInstance().getApplicationContext();
				Intent i = new Intent(context, FirmwareUI.class);
				i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(i);
				return 0;
			
		}
		
		return 2;
		
	}
	
	public boolean checkUpgradeCondition(int type)
	{
		LogMgr.d(LogMgr.TAG,"checkUpgradeCondition= type : " + type);		
		//통화중인가???		
		PhoneManager phoneMgr = SmartCareSystemApplication.getInstance().getPhoneManager();
		
		if( phoneMgr.getPhoneState() != TelephonyManager.CALL_STATE_IDLE)
		{
			LogMgr.d(LogMgr.TAG,"checkUpgradeCondition calling state return false ");		
					
			return false;
		}
		
		//응급상황진행중인가???		
		if( SmartCareSystemApplication.getInstance().getEmergencyCallUtil().getActionStage() != EmergencyAction.ACTION_READY )
		{
			LogMgr.d(LogMgr.TAG,"checkUpgradeCondition emergency action state return false ");			
			return false;
		}
		
		//전원 차단 되었는가?
		if(GatewayDevice.getInstance().power_state == 0x00 )
		{
			LogMgr.d(LogMgr.TAG,"checkUpgradeCondition power off state return false ");
			return false;
		}
		
		//비상전모드인가???
//		if(SmartCareSystemApplication.getInstance().getMessageManager().isFT312DMode())
//		{
//			LogMgr.d(LogMgr.TAG,"checkUpgradeCondition ft312mode return false ");
//			return false;
//		}
		
		
		LogMgr.d(LogMgr.TAG,"checkUpgradeCondition return true ");
		return true;
	}
}
