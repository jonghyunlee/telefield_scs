package com.telefield.smartcaresystem.firmware;

public interface OnInstalledPackaged {
	
	public void packageInstalled(String packageName, int returnCode);

}
