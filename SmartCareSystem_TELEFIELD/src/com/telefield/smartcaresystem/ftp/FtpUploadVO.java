package com.telefield.smartcaresystem.ftp;

import java.util.Arrays;

import com.telefield.server.util.ftp.FtpConnect;
import com.telefield.server.util.ftp.FtpUtils;

public class FtpUploadVO {

	private FtpConnect ftpConnect;
	private FtpUtils ftpUtils;
	
	private byte[] upload_content;
	private String upload_folder_path;
	private String save_file_name;
	private String download_file_path;
	
	
	
	public byte[] getUpload_content() {
		return upload_content;
	}

	public String getUpload_folder_path() {
		return upload_folder_path;
	}

	public String getSave_file_name() {
		return save_file_name;
	}

	public String getDownload_file_path() {
		return download_file_path;
	}
	public FtpConnect getFtpConnect() {
		return ftpConnect;
	}

	public FtpUtils getFtpUtils() {
		return ftpUtils;
	}

	public FtpUploadVO(byte[] upload_content, String upload_folder_path,
			String save_file_name) {
		super();
		this.upload_content = upload_content;
		this.upload_folder_path = upload_folder_path;
		this.save_file_name = save_file_name;
		this.ftpConnect = new FtpConnect.Builder().defaultValueBuild();
		this.ftpUtils = new FtpUtils(ftpConnect);
	}
	
	public FtpUploadVO(String download_file_path) {
		super();
		this.download_file_path = download_file_path;
		this.ftpConnect = new FtpConnect.Builder().defaultValueBuild();
		this.ftpUtils = new FtpUtils(ftpConnect);
	}
	
	@Override
	public String toString() {
		return "FtpUploadVO [ftpConnect=" + ftpConnect + ", ftpUtils="
				+ ftpUtils + ", upload_content="
				+ Arrays.toString(upload_content) + ", upload_folder_path="
				+ upload_folder_path + ", save_file_name=" + save_file_name
				+ ", download_file_path=" + download_file_path + "]";
	}
	
	
}
