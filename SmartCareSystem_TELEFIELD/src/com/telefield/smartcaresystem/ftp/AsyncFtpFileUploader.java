package com.telefield.smartcaresystem.ftp;

import java.io.IOException;
import java.net.SocketException;

import com.telefield.server.util.ftp.FtpConnect;
import com.telefield.server.util.ftp.FtpUtils;
import com.telefield.smartcaresystem.util.LogMgr;

import android.os.AsyncTask;
import android.util.Log;

public class AsyncFtpFileUploader extends AsyncTask<FtpUploadVO,Void,Boolean> {
	
	@Override
	protected void onPostExecute(Boolean result) {
		LogMgr.d("PICTURE","onpostExeccute");

		super.onPostExecute(result);
	}

	@Override
	protected Boolean doInBackground(FtpUploadVO... ftpUploadVO) {
		boolean isUpload = false;
		try {
			
			for(FtpUploadVO ftp : ftpUploadVO) {
			
				final FtpUtils ftpUtils = ftp.getFtpUtils();
				final FtpConnect ftpConnect = ftp.getFtpConnect();				
				ftpConnect.connect();
				isUpload = ftpUtils.upload(ftp.getUpload_content(),ftp.getUpload_folder_path(),ftp.getSave_file_name());				
				break;
			}

		} catch (IOException e) {
			LogMgr.d("emergency_picture_upload",e.getMessage());
		}
		
		return isUpload;
	}
}
