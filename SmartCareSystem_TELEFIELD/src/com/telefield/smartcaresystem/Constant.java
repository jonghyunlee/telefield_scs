package com.telefield.smartcaresystem;

import android.os.Environment;

public interface Constant {
	public static final int MSG_VERSION = 1 ;//0.1
	
	public static final boolean IS_RELEASE_VERSION = true;
	public static final boolean RUN_FOR_BMT = true;
	
	//TTS utteranceId
	public static final String UTTERANCE_ID_EMG_ENDCALL = "endcall";
	public static final String UTTERANCE_ID_119 = "119";
	public static final String UTTERANCE_ID_EMERGENCY_MENT = "emergency_ment";
	public static final String UTTERANCE_ID_CENTER = "center";
	public static final String UTTERANCE_ID_HELPER = "helper";
	public static final String UTTERANCE_ID_CARE1 = "care1";
	public static final String UTTERANCE_ID_CARE2 = "care2";
	public static final String UTTERANCE_ID_CANCEL = "cancel";
	public static final String UTTERANCE_ID_DIAGNOSTIC_TEST = "diagnostic_test";
	public static final String UTTERANCE_ID_NULL = "utterance_id_null";
	public static final String UTTERANCE_ID_NEGLECT_TELEPHONE = "neglect_telephone";
	
	//Emergency Type
	public static final String EMERGENCY = "emergency";
	public static final String FIRE = "fire";
	public static final String GAS = "Gas";
	
	//startActivityForResult's requestCodes
	public static final int REQUEST_CODE_0 = 0;
	
	//주기보고 시간
	public static final long DEFAULT_PERIODC_REPORT_TIME = 10 * 60 * 1000; //10분
	
	public static final int TIME_HOUR_UNIT = 1000 * 60 * 60;
	public static final int TIME_MINUTE_UNIT = 1000 * 60;
	
	public static final int ACTIVITY_THRESHOLD = 1;
	
	public static final String PERIDIC_SERVICE = "com.telefield.smartcaresystem.periodicreport";
	
	// UnCatch Exception 발생 시 Activity 재시작 Delay
	public static final long UNCATCH_DELAY_TIME = 20000;
	
	public static final String DELETE_HISTORY_ACTION = "com.telefield.deletehistory";
	
	public static final String BATT_FULL_ACTION = "com.telefield.battfull";
	
	public static final String PERIODIC_REPORT_ACTION = "com.telefield.periodicreport";
	
	public static final String NON_ACTIVITY_REPORT_ACTION = "com.telefield.nonactivityreport";
	
	public static final String ALARM_CALL_TIME = "com.telefield.alarmcalltime";
	
	public static final String LOCKSCREEN_DELETE_ACTION = "com.telefield.lockscreendelete";
	// DB 및 Log 삭제 기준
	public static final int DAY_OF_DELETE_HISTORY = 30;
	
	// M2M 진단 테스트 횟수
	public static final int COUNT_PING_TEST = 10;
	
	public static boolean IS_CAN_INSERT_EVERY_MINUTE_PIRCNT = true;
	

	static final String ROOT_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + "/SmartCareSystem";
	
	// History 파일 위치
	static final String LOG_PATH = ROOT_PATH + "/logs";
	static final String CAM_PATH = ROOT_PATH + "/Picture";
	static final String FIRMWARE_PATH = ROOT_PATH + "/firmware";
	static final String IMAGE_PATH = ROOT_PATH + "/image";
	
	// FW 저장 위치
	static final String FW_GATEWAY_PATH = FIRMWARE_PATH + "/gateway.bin";
	static final String FW_GATEWAY_VERIFY_PATH = FIRMWARE_PATH + "/gateway_verify.bin";
	static final String FW_SENSOR_PATH = FIRMWARE_PATH + "/sensor.bin";
	static final String FW_APP_PATH = FIRMWARE_PATH + "/SmartCareSystem.apk";
	static final String LOCKSCREEN_IMAGE_PATH = IMAGE_PATH + "/lockscreenBack.png";
//	static final String ZIGBEE_FW_APP_PATH = FIRMWARE_PATH + "/zigbee_fw.bin";

	 
	// Log 전송시 임시파일 경로
	static final String LOG_TEMP_PATH = LOG_PATH + "/temp.log";
	
	// 지그비,게이트웨이 펌웨어 다운로드 경로
	static final String LOCKSCREEN_IMAGE_DOWNLOAD_PATH = "http://222.122.128.45:8080/image/download/android/background";
	static final String ZIGBEE_FW_DOWNLOAD_PATH = "http://222.122.128.45:8080/firmware/download/zigbee/lastest";
	static final String GATEWAY_FW_DOWNLOAD_PATH = "http://222.122.128.45:8080/firmware/download/gateway/lastest";
	static final String DEVICE_ID_REGIST_PATH = "http://222.122.128.45:8080/android/device/regist";
	static final String LOG_FILE_UPLOAD_PATH = "http://222.122.128.45:8080/upload/log";
	
	
	static final boolean DEBUG_OPTION = false;
	
	public static final String[] EMERGENCY_NUMBER = {"119", "112", "911"};
	
	public static int FTDI_BAUDRATE = /*115200*/38400;
		final static public String SENDER_ID = "460926049147";
}
