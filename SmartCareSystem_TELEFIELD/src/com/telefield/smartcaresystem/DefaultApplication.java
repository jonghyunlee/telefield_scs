package com.telefield.smartcaresystem;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.Thread.UncaughtExceptionHandler;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.speech.tts.TextToSpeech;

import com.telefield.smartcaresystem.bluetooth.DeviceScan;
import com.telefield.smartcaresystem.ui.LauncherUI;
import com.telefield.smartcaresystem.util.LogMgr;
import com.telefield.smartcaresystem.util.SpeechUtil;
import com.telefield.smartcaresystem.wifi.SmartCareWifiManager;

public class DefaultApplication extends Application {
	private UncaughtExceptionHandler defaultUEH;
	SmartCareSystemApplication mainApp;

	@Override
	public void onCreate() {
		super.onCreate();
		LogMgr.d(LogMgr.TAG,"doing DefaultApplication's onCreate");
		
		if( mainApp == null)
		{		
			LogMgr.d(LogMgr.TAG, "mainApp is null");
			mainApp = SmartCareSystemApplication.createInstance(this/*getApplicationContext()*/);
			mainApp.init();
		}
		
		
		defaultUEH = Thread.getDefaultUncaughtExceptionHandler();
		Thread.setDefaultUncaughtExceptionHandler(_unCaughtExceptionHandler);
		
		//<-- 최초 앱이 동작할때 와이파이가 꺼져있으면 와이파이를 끈다. 블루투스도 끈다. 
		SmartCareWifiManager wifiManager = new SmartCareWifiManager(this);
		if(wifiManager.isWifiEnable()) {
			wifiManager.turnOffWifi();
		}
		BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if(mBluetoothAdapter.getState() == mBluetoothAdapter.STATE_ON){
			LogMgr.d(LogMgr.TAG, "mBluetoothAdapter.getState() : " + mBluetoothAdapter.getState() + "..mBluetoothAdapter.STATE_ON : " + mBluetoothAdapter.STATE_ON);
			mBluetoothAdapter.disable();
		}
		// (2014.12.26)-->
		
		//최초 앱이 실행될때 음성 안나오는 부분 수정(최초 나오는 음성은 나오지 않음.. 그래서 앱이 실행될때 음성 tts하나를 출력해 줌.)<--
		SmartCareSystemApplication.getInstance().getSpeechUtil().stopSpeech();
		SmartCareSystemApplication
		.getInstance()
		.getSpeechUtil()
		.speech("음성 데이터 초기화 완료.", TextToSpeech.QUEUE_FLUSH, null);
		// (2014.12.29)-->
	}

	private final Thread.UncaughtExceptionHandler _unCaughtExceptionHandler = new Thread.UncaughtExceptionHandler() {
		@Override
		public void uncaughtException(Thread thread, Throwable ex) {
			ex.printStackTrace();
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			ex.printStackTrace(new PrintStream(out));
			LogMgr.writeHistory(LogMgr.EXCEPTION, out.toString());
			
			PendingIntent myActivity = PendingIntent.getActivity(getApplicationContext(), 0, new Intent(getApplicationContext(), LauncherUI.class), PendingIntent.FLAG_ONE_SHOT);

			AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
			
			Intent checkIntent = new Intent(Constant.DELETE_HISTORY_ACTION);
		    PendingIntent checkSender = PendingIntent.getBroadcast(getApplicationContext(), 0, checkIntent, PendingIntent.FLAG_NO_CREATE);

		    LogMgr.d(LogMgr.TAG, "DefaultApplication.java:enclosing_method:// checkSender : " + checkSender);

		    if(checkSender != null){
		    	Intent deleteIntent = new Intent(Constant.DELETE_HISTORY_ACTION);
			    PendingIntent deleteSender = PendingIntent.getBroadcast(getApplicationContext(), 0, deleteIntent, PendingIntent.FLAG_UPDATE_CURRENT);

			    LogMgr.d(LogMgr.TAG, "DefaultApplication.java:enclosing_method:// deleteSender : " + deleteSender);

			    if(deleteSender != null){
			    	alarmManager.cancel(deleteSender);
			    	deleteSender.cancel();
			    }
		    }
			
			alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, Constant.UNCATCH_DELAY_TIME, myActivity);
			System.exit(2);
			
			defaultUEH.uncaughtException(thread, ex);
		}
	};
	
}
