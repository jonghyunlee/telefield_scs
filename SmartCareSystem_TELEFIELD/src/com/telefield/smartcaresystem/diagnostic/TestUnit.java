package com.telefield.smartcaresystem.diagnostic;

import java.util.Arrays;
import java.util.HashMap;

import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.speech.tts.UtteranceProgressListener;

import com.telefield.smartcaresystem.Constant;
import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.message.MessageID;
import com.telefield.smartcaresystem.message.MessageManager;
import com.telefield.smartcaresystem.util.LogMgr;
import com.telefield.smartcaresystem.util.SpeechUtil;
import com.telefield.smartcaresystem.util.UtteranceID;

public class TestUnit extends UtteranceProgressListener {
	
	private int testId;
	private String ment;
	private int sensor_id;
	private int msg_id;
	private byte[] sub_data;
	
	TextToSpeech tts;
	
	TestUnit(UnitStruct unit)
	{
		this.testId = unit.ordinal();
		this.ment = unit.getMent();
		this.msg_id = unit.getMsg_id();
		if( unit.getSub_data() == null)
		{
			this.sub_data = null;
		}else
		{
			this.sub_data = Arrays.copyOf(unit.getSub_data(), unit.getSub_data().length);
		}
		
		tts = new TextToSpeech(SmartCareSystemApplication.getInstance().getApplicationContext(),new OnInitListener() {
			@Override
			public void onInit(int status) {
				// TODO Auto-generated method stub
				
			}
		});
		
		tts.setOnUtteranceProgressListener(this);
	}
	
	
	public void setSensorId(int sensor_id) { this.sensor_id = sensor_id; }	
	
	public int getExpectedMsgId() { return msg_id; }
	public byte[] getExpectedSubDatea() { return sub_data; }	
	public int getTestId() { return testId; }
	public String getMent() {return ment; }
	public void setMent(String ment) { this.ment = ment;}
	
	public void test() 
	{
		//멘트를 말한다.
		SpeechUtil speech = SmartCareSystemApplication.getInstance().getSpeechUtil();
		
		speech.stopSpeech();
		if( ment != null )
		{
			LogMgr.d(LogMgr.TAG,"ment :: " + ment);
			HashMap<String, String> utteranceId = UtteranceID.TTSUtteranceId(Constant.UTTERANCE_ID_DIAGNOSTIC_TEST);
			speech.speech(ment,TextToSpeech.QUEUE_FLUSH, utteranceId);
		}
	}

	@Override
	public void onStart(String utteranceId) {
		// TODO Auto-generated method stub
		LogMgr.d(LogMgr.TAG,"onStart :: " + ment);
		SmartCareSystemApplication.getInstance().getGlobalMsgFunc().sendGateWayMsg((byte)MessageID.TTS_START_INFO);
		
	}

	@Override
	public void onDone(String utteranceId) {	
		LogMgr.d(LogMgr.TAG,"onDone :: " + ment);
		SmartCareSystemApplication.getInstance().getGlobalMsgFunc().sendGateWayMsg((byte)MessageID.TTS_STOP_INFO);
	}

	@Override
	public void onError(String utteranceId) {
		// TODO Auto-generated method stub
		
		SmartCareSystemApplication.getInstance().getGlobalMsgFunc().sendGateWayMsg((byte)MessageID.TTS_STOP_INFO);
		
	}
}
