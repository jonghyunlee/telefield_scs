package com.telefield.smartcaresystem.diagnostic;

import java.util.LinkedHashMap;
import java.util.Map;

public class EmergencyCallSensorDiagnostic extends DiagnosticUnit {
	
	TestUnit all[] = {
			new TestUnit(UnitStruct.EMER_CALL_SENSOR_EMR_BTN1_TEST),
			new TestUnit(UnitStruct.EMER_CALL_SENSOR_EMR_BTN2_TEST)			
	};
	
	public EmergencyCallSensorDiagnostic(int sensor_id) {
		super(sensor_id);
		// TODO Auto-generated constructor stub
	}



	@Override
	public Map getAllDiagnosticIndex() {
		Map<Integer,String> ret = new LinkedHashMap<Integer,String>();
		
		for( TestUnit unit : all)
		{		
			ret.put(unit.getTestId(), unit.getMent());
		}	
		
		return ret;
	}

	@Override
	public TestUnit[] getAllDiagnosticList() {
		// TODO Auto-generated method stub
		return all;
	}

}
