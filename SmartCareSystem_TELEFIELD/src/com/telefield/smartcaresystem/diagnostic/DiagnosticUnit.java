package com.telefield.smartcaresystem.diagnostic;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.message.FormattedFrame;
import com.telefield.smartcaresystem.ui.UIConstant;
import com.telefield.smartcaresystem.util.LogMgr;
import com.telefield.smartcaresystem.util.SmartCarePreference;

public abstract class DiagnosticUnit {
	
	public static int ACK_EXPIRE_TIMER = 2 * 60 * 1000; //1분
	
	List<TestUnit> selectedDiagList;
	Timer waitTimer;
	TestUnit unit;	
	IDiagUnitResultCallback listener;
	int sensor_id;
	int current_idx;
	
	boolean isStop = false;
	TestRunner runner;
	
	public DiagnosticUnit(int sensor_id)
	{
		this.sensor_id = sensor_id;
		
		selectedDiagList = new LinkedList();		
	}
	
	public void addCallbackListener(IDiagUnitResultCallback listener)
	{
		this.listener = listener;
	}
	
	public void setDiagnosticList(List selectedIndex)
	{
		TestUnit all[];
		all = getAllDiagnosticList();		
		
		Iterator it = selectedIndex.iterator();
		while(it.hasNext())
		{
			int index = (Integer)it.next();
			selectedDiagList.add(all[index]);
		}
	}
	
	public List<TestUnit> getDiagnosticList()
	{
		return selectedDiagList;
	}
	
	public abstract Map getAllDiagnosticIndex();
	public abstract TestUnit[] getAllDiagnosticList();
	
	
	public boolean runTest(DiagnosticManager mgr)
	{			
		current_idx = -1;
		runner = new TestRunner(mgr);
		runner.start();		
		
		return true;			
	}
	
	private void runTest(int index)
	{
		
		int wait_time = ACK_EXPIRE_TIMER;
		current_idx = index;
		unit = selectedDiagList.get(index);		
		unit.test();
		
		if( unit.getTestId() == UnitStruct.ACTIVITY_SENSOR_ACT_TEST.ordinal() ||
				unit.getTestId() == UnitStruct.DOOR_SENSOR_ACT_TEST.ordinal()
				)
		{
			SmartCarePreference pref = new SmartCarePreference(
					SmartCareSystemApplication.getInstance().getApplicationContext(),
					SmartCarePreference.SENSOR_SETTING_INFO_FILENAME);
			
			int comm_periodic = pref.getIntValue(SmartCarePreference.PERIODIC_REPORT_TIME, UIConstant.DEFAULT_PERIODIC_REPORT_TIME_VALUE); //통신 주기 시간
			comm_periodic = comm_periodic / 10;
			if( comm_periodic <= 0 )
			{
				comm_periodic = 0;
			}
			
			wait_time = (comm_periodic + 1 ) * 60 * 1000;
		}
		
		waitTimer = new Timer();		
		waitTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				//call back
				runner.interrupt();
				listener.notify(current_idx,unit.getTestId(), IDiagUnitResultCallback.FAIL);
			}
		}, wait_time);
	}
	
	public void currentTestStop()
	{
		LogMgr.d(LogMgr.TAG,"currentTestStop");
		listener.notify(current_idx,unit.getTestId(), IDiagUnitResultCallback.IGNORE);
		runner.interrupt();
		
		if(waitTimer != null)
		{
			waitTimer.cancel();
			//waitTimer.purge();
			//waitTimer = null;
		}
	}
	
	public void testStop()
	{
		LogMgr.d(LogMgr.TAG,"testStop");
		isStop = true;
		runner.interrupt();
		
		if(waitTimer != null)
		{
			waitTimer.cancel();
			//waitTimer.purge();
			//waitTimer = null;
		}
		
	}
	
	public void receiveTestResult(FormattedFrame ff)
	{
		LogMgr.d(LogMgr.TAG,"receiveTestResult");		
		
		int result = IDiagUnitResultCallback.SUCCESS;
		//기대되는 메시지인가?
		if( ff != null)
		{			
			if( sensor_id != ff.getSensor_id()) return; //동일한 sensorId 의 경우만..
			
			LogMgr.d(LogMgr.TAG,"msgid : " + ff.getMsg_id() + "expected msg id ::" + unit.getExpectedMsgId());		
			if( ff.getMsg_id() == unit.getExpectedMsgId() ) // msg id 로 기대되는 메시지인지 파악하자...
			{
				LogMgr.d(LogMgr.TAG,"expected msg receive:::");
				byte subData[] = ff.getSub_data();
				byte expected_sub_data[] = unit.getExpectedSubDatea();
				
				if( expected_sub_data == null && subData == null)
				{
					//is success
					result = IDiagUnitResultCallback.SUCCESS;
					
				}
				else if( expected_sub_data== null || subData == null)
				{
					//is fail
					result = IDiagUnitResultCallback.FAIL;
				}
				else
				{
					if(Arrays.equals(subData, expected_sub_data))
					{
						//is success
						result = IDiagUnitResultCallback.SUCCESS;
					}					
					else{
						
						for (int i=0; i < expected_sub_data.length; i++)
						{
							if( expected_sub_data[i] == (byte)0xff) continue; // 기대되는 data 가 0xff 이면 모든 값을 수락한다는 의미
							
							LogMgr.d(LogMgr.TAG,"index : " + i + " expected data : " + expected_sub_data[i] + " real value : " + subData[i]);
							
							if( expected_sub_data[i] == subData[i]){
								continue;
							}
							else{
								LogMgr.d(LogMgr.TAG,"test fail");
								result = IDiagUnitResultCallback.FAIL;
								break;
							}
						}
					}
				}
				
				if(waitTimer != null)
				{
					waitTimer.cancel();
					//waitTimer.purge();
					//waitTimer = null;
				}
				
				if( result == IDiagUnitResultCallback.SUCCESS)
				{
					//call back
					runner.interrupt();
					listener.notify(current_idx,unit.getTestId(), result);
				}				
			}		
		}
	}
	
	
	class TestRunner extends Thread
	{
		DiagnosticManager mgr;
		boolean wait = true;
		
		TestRunner(DiagnosticManager mgr)
		{
			this.mgr =mgr;
		}
		
		@Override
		public void run() {
			
			for(int i=0; i < selectedDiagList.size();i++)
			{		
				if(isStop) return;
				
				runTest(i);
				wait = true;
				
				//결과를 받을 때까지 wait;
				while( wait )
				{
					try {
						LogMgr.d(LogMgr.TAG,"Thread wait data sleep!!!");
						Thread.sleep(200);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						LogMgr.d(LogMgr.TAG,"interrupted!!!");
						wait = false; // 다음동작 수행
					}
				}
			}			
			
			//test 완료
			mgr.stopTest();
			listener.notify(current_idx,unit.getTestId(), IDiagUnitResultCallback.COMPLETED);
			
			LogMgr.d(LogMgr.TAG,"runTest completed!!!");
		}
		
	};
	

}
