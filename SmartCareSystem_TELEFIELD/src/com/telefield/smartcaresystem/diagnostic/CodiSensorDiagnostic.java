package com.telefield.smartcaresystem.diagnostic;

import java.util.LinkedHashMap;
import java.util.Map;

public class CodiSensorDiagnostic extends DiagnosticUnit {

	TestUnit all[] = {
			new TestUnit(UnitStruct.CODI_SENSOR_EMERGENCY_BTN_TEST),
			new TestUnit(UnitStruct.CODI_SENSOR_CENTER_BTN_TEST),
			new TestUnit(UnitStruct.CODI_SENSOR_CANCEL_BTN_TEST),
			new TestUnit(UnitStruct.CODI_SENSOR_OUT_BTN_TEST),
			new TestUnit(UnitStruct.CODI_SENSOR_HOOK_OFF_TEST),
			new TestUnit(UnitStruct.CODI_SENSOR_HOOK_ON_TEST)
	};

	public CodiSensorDiagnostic(int sensor_id) {
		super(sensor_id);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Map<Integer,String>  getAllDiagnosticIndex() {
		Map<Integer,String> ret = new LinkedHashMap<Integer,String>();
		
		for( TestUnit unit : all)
		{		
			ret.put(unit.getTestId(), unit.getMent());
		}	
		
		return ret;
	}	

	@Override
	public TestUnit[] getAllDiagnosticList() {
		// TODO Auto-generated method stub
		return all;
	}

}
