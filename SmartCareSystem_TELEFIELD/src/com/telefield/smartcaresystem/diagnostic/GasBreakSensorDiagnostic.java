package com.telefield.smartcaresystem.diagnostic;

import java.util.LinkedHashMap;
import java.util.Map;

import com.telefield.smartcaresystem.SmartCareSystemApplication;

public class GasBreakSensorDiagnostic extends DiagnosticUnit {
	
	TestUnit all[] = {
			new TestUnit(UnitStruct.GAS_BREAK_SENSOR_TEST)			
	};
	

	public GasBreakSensorDiagnostic(int sensor_id) {
		super(sensor_id);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Map getAllDiagnosticIndex() {
		Map<Integer,String> ret = new LinkedHashMap<Integer,String>();		
		for( TestUnit unit : all)
		{
			ret.put(unit.getTestId(), unit.getMent());
		}		
		return ret;
	}

	@Override
	public TestUnit[] getAllDiagnosticList() {
		// TODO Auto-generated method stub
		return all;
	}

	@Override
	public boolean runTest(DiagnosticManager mgr) {
		
		//가스 차단 명령을 codi에게 전송한다.
		SmartCareSystemApplication.getInstance().getGlobalMsgFunc().sendGasBreakMsg();
		
		//전송한 후에 ack 가 오는지 확인한다.
		return super.runTest(mgr);
	}
	
	
	

}
