package com.telefield.smartcaresystem.diagnostic;

public interface IDiagUnitResultCallback {
	
	final int COMPLETED = 2;
	final int SUCCESS = 1;
	final int IGNORE = 0;	
	final int FAIL = -1;
	final int ABORT = -2;
	
	public void notify(int index, int testId, int result);

}
