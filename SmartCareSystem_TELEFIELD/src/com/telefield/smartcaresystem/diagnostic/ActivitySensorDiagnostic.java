package com.telefield.smartcaresystem.diagnostic;

import java.util.LinkedHashMap;
import java.util.Map;

import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.ui.UIConstant;
import com.telefield.smartcaresystem.util.SmartCarePreference;

public class ActivitySensorDiagnostic extends DiagnosticUnit {
	
	TestUnit all[] = {
			new TestUnit(UnitStruct.ACTIVITY_SENSOR_LINK_TEST),
			new TestUnit(UnitStruct.ACTIVITY_SENSOR_ACT_TEST)						
	};

	public ActivitySensorDiagnostic(int sensor_id) {
		super(sensor_id);
		// TODO Auto-generated constructor stub
		
		SmartCarePreference pref = new SmartCarePreference(
				SmartCareSystemApplication.getInstance().getApplicationContext(),
				SmartCarePreference.SENSOR_SETTING_INFO_FILENAME);
		
		int comm_periodic = pref.getIntValue(SmartCarePreference.PERIODIC_REPORT_TIME, UIConstant.DEFAULT_PERIODIC_REPORT_TIME_VALUE); //통신 주기 시간
		comm_periodic = comm_periodic / 10;
		if( comm_periodic <= 0 )
		{
			comm_periodic = 1;
		}
		String ment = all[1].getMent();		
		all[1].setMent(ment.replace("1분간", comm_periodic + "분간"));
	}

	@Override
	public Map<Integer,String>  getAllDiagnosticIndex() {
		Map<Integer,String> ret = new LinkedHashMap<Integer,String>();		
		for( TestUnit unit : all)
		{		
			ret.put(unit.getTestId(), unit.getMent());
		}
		return ret;
	}

	@Override
	public TestUnit[] getAllDiagnosticList() {
		// TODO Auto-generated method stub
		return all;
	}	

}
