package com.telefield.smartcaresystem.diagnostic;

import java.util.LinkedHashMap;
import java.util.Map;

public class GasSensorDiagnostic extends DiagnosticUnit {

	TestUnit all[] = {
			new TestUnit(UnitStruct.GAS_SENSOR_LINK_TEST),
			new TestUnit(UnitStruct.GAS_SENSOR_ALERT_TEST),
			new TestUnit(UnitStruct.GAS_SENSOR_ALERT_CANCEL_TEST)			
	};

	public GasSensorDiagnostic(int sensor_id) {
		super(sensor_id);
		
	}	
	
	@Override
	public Map<Integer,String>  getAllDiagnosticIndex() {
		Map<Integer,String> ret = new LinkedHashMap<Integer,String>();
		
		for( TestUnit unit : all)
		{		
			ret.put(unit.getTestId(), unit.getMent());
		}
		
		return ret;
	}	

	@Override
	public TestUnit[] getAllDiagnosticList() {
		// TODO Auto-generated method stub
		return all;
	}	

}
