package com.telefield.smartcaresystem.diagnostic;

import java.util.List;
import java.util.Map;

import com.telefield.smartcaresystem.message.FormattedFrame;
import com.telefield.smartcaresystem.message.SensorID;
import com.telefield.smartcaresystem.util.LogMgr;

public class DiagnosticManager {
	
	private boolean isDiagTesting = false;
	DiagnosticUnit diagUnit;
	
	
	private void setDiagnosticSensorId(int sensor_id)
	{
		switch(SensorID.getSensorType(sensor_id))
		{
		case 0x1: //activity
			diagUnit = new ActivitySensorDiagnostic(sensor_id);
			break;
		case 0x2: //fire	
			diagUnit = new FireSensorDiagnostic(sensor_id);
			break;
		case 0x3: //gas			
			diagUnit = new GasSensorDiagnostic(sensor_id);
			break;
		case 0x4: //door	
			diagUnit = new DoorSensorDiagnostic(sensor_id);
			break;
		case 0x5: //out
			break;
		case 0x6: //call emer //응급호출기
			diagUnit = new EmergencyCallSensorDiagnostic(sensor_id);
			break;	
		case 0x7: //가스차단기
			diagUnit = new GasBreakSensorDiagnostic(sensor_id);
			break;
		case 0xc0: //본체
			diagUnit = new CodiSensorDiagnostic(sensor_id);
			break;
		}
	}
	
	public Map<Integer,String> getAllDiagList(int sensorId)
	{		
		setDiagnosticSensorId(sensorId);		
		return diagUnit.getAllDiagnosticIndex();
	}
	
	public void runTest(List selectedList , IDiagUnitResultCallback callback)
	{
		LogMgr.d(LogMgr.TAG,"runTest selectedList : " + selectedList);
		isDiagTesting = true;
		diagUnit.setDiagnosticList(selectedList);
		diagUnit.addCallbackListener(callback);
		diagUnit.runTest(this);
	}
	
	public void stopCurrentTest()
	{
		LogMgr.d(LogMgr.TAG,"stopCurrentTest");
		if( diagUnit != null)
		{
			diagUnit.currentTestStop();
		}
	}
	
	public void stopTest()
	{
		isDiagTesting =false;
		if( diagUnit != null)
		{
			diagUnit.testStop();
		}
	}
	
	public void receiveTestResult(FormattedFrame ff)
	{
		diagUnit.receiveTestResult(ff);
	}
	
	public boolean isDiagnosticTesting() { return isDiagTesting; }
	

}
