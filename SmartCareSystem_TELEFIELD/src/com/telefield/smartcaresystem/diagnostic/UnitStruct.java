package com.telefield.smartcaresystem.diagnostic;

import java.util.Arrays;

import com.telefield.smartcaresystem.message.MessageID;

public enum UnitStruct {
	/*FIRE_SENSOR */
	FIRE_SENSOR_LINK_TEST("화재 센서의 테스트 버튼을 누르세요", MessageID.LINK_TEST_MSG, new byte[]{(byte)0xff,(byte)0xff,(byte)0xff,(byte)0xff}),
	FIRE_SENSOR_ALERT_TEST("화재 센서의 경보를 발생시켜 주세요", MessageID.FIRE_DET_MSG, new byte[]{0x01} ),
	FIRE_SENSOR_ALERT_CANCEL_TEST("화재 센서의 경보를 해제시켜 주세요", MessageID.FIRE_DET_MSG, new byte[]{0x00} ),
	
	/*GAS_SENSOR*/
	GAS_SENSOR_LINK_TEST("가스 센서의 테스트 버튼을 누르세요", MessageID.LINK_TEST_MSG, new byte[]{(byte)0xff,(byte)0xff,(byte)0xff,(byte)0xff}),
	GAS_SENSOR_ALERT_TEST("가스 센서의 경보를 발생시켜 주세요", MessageID.GAS_DET_MSG, new byte[]{0x01} ),
	GAS_SENSOR_ALERT_CANCEL_TEST("가스 센서의 경보를 해제시켜 주세요", MessageID.GAS_DET_MSG, new byte[]{0x00} ),
	
	/*활동센서 */
	ACTIVITY_SENSOR_LINK_TEST("활동센서의 테스트 버튼을 누르세요", MessageID.LINK_TEST_MSG, new byte[]{(byte)0xff,(byte)0xff,(byte)0xff,(byte)0xff}),
	ACTIVITY_SENSOR_ACT_TEST("활동센서의 근처에서 1분간 움직여주세요" , MessageID.PIR_CNT_MSG, new byte[]{(byte)0xff}),
	
	/*도어센서*/
	DOOR_SENSOR_LINK_TEST("출입감지센서의 테스트 버튼을 누르세요", MessageID.LINK_TEST_MSG, new byte[]{(byte)0xff,(byte)0xff,(byte)0xff,(byte)0xff}),
	DOOR_SENSOR_ACT_TEST("출입감지센서의 근처에서 1분간 움직여주세요" , MessageID.PIR_CNT_MSG, new byte[]{(byte)0xff}),
	DOOR_SENSOR_OPEN_TEST("출입감지센서의 문을 열어주세요" , MessageID.DOOR_STAT_MSG, new byte[]{0x01, (byte)0xff , (byte)0xff}),
	DOOR_SENSOR_CLOSE_TEST("출입감지센서의 문을 닫아주세요" , MessageID.DOOR_STAT_MSG, new byte[]{0x00, (byte)0xff , (byte)0xff}),
	
	/*응급호출장비*/
	EMER_CALL_SENSOR_EMR_BTN1_TEST("응급호출장비에서 응급버튼을 누르세요", MessageID.EMG_KEY_MSG, new byte[]{(byte)0x10}),
	EMER_CALL_SENSOR_EMR_BTN2_TEST("응급호출장비에서 상황해제 버튼을 누르세요" , MessageID.EMG_KEY_MSG, new byte[]{(byte)0x01}),
	
	/*가스차단기*/	
	GAS_BREAK_SENSOR_TEST("가스 센서의 경보를 발생시켜서, 가스차단기 센서의 가스가 차단되는지 확인해주세요", MessageID.GAS_COMP_MSG,new byte[]{(byte)0xff}),
	
	/*본체*/
	CODI_SENSOR_EMERGENCY_BTN_TEST("응급 버튼을 누르세요", MessageID.EMG_KEY_MSG, new byte[]{0x10}),
	CODI_SENSOR_CENTER_BTN_TEST("센터 버튼을 누르세요", MessageID.EMG_KEY_MSG, new byte[]{0x20}),
	CODI_SENSOR_CANCEL_BTN_TEST("취소 버튼을 누르세요", MessageID.EMG_KEY_MSG, new byte[]{0x01}),
	CODI_SENSOR_OUT_BTN_TEST("말벗 버튼을 누르세요", MessageID.COORD_EVENT_MSG, new byte[]{0x10}),
	CODI_SENSOR_HOOK_OFF_TEST("수화기를 들어주세요", MessageID.COORD_EVENT_MSG, new byte[]{0x20}),
	CODI_SENSOR_HOOK_ON_TEST("수화기를 내려주세요", MessageID.COORD_EVENT_MSG, new byte[]{0x21});
	
	String ment;
	int msg_id;
	byte[] sub_data;
	
	
	UnitStruct(String ment, int msg_id, byte[] sub_data)
	{
		this.ment = ment;
		this.msg_id = msg_id;
		this.sub_data = Arrays.copyOf(sub_data, sub_data.length);
	}

	public String getMent() {
		return ment;
	}

	public int getMsg_id() {
		return msg_id;
	}

	public byte[] getSub_data() {
		return sub_data;
	}
	
	

}
