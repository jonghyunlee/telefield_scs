package com.telefield.smartcaresystem.database;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.telefield.smartcaresystem.database.DaoMaster.DevOpenHelper;
import com.telefield.smartcaresystem.message.SensorID;
import com.telefield.smartcaresystem.util.ByteArrayToHex;
import com.telefield.smartcaresystem.util.LogMgr;

import de.greenrobot.dao.query.QueryBuilder;

public class ConnectDB {
	private static ConnectDB mConnDB = null;
	private SQLiteDatabase db = null;
	private UsersDao mUserDao = null;
	private SensorDao mSensorDao = null;
	private ActivityDataDao mActivityDataDao = null;
	private CareDao mCareDao = null;
	private SensorHistoryDao mSensorHistoryDao = null;
	private CallLogDao mCallLogDao = null;
	private MessageHistoryDao mMessageHistoryDao = null;
	private BloodPressureDao mBloodPressureDao = null;
	private NotReportedDao mNotReportedDao = null;
	private DaoSession daoSession = null;
	
	private ConnectDB(Context ctx){
		
		DevOpenHelper helper = new DaoMaster.DevOpenHelper(ctx, "telefield.db", null);
		db = helper.getWritableDatabase();
		UsersDao.createTable(db, true);
		SensorDao.createTable(db, true);
		DaoMaster daoMaster = new DaoMaster(db);		
		daoSession = daoMaster.newSession();		
		mUserDao = daoSession.getUsersDao();		
		mSensorDao = daoSession.getSensorDao();
		mActivityDataDao = daoSession.getActivityDataDao();
		mCareDao = daoSession.getCareDao();
		mSensorHistoryDao = daoSession.getSensorHistoryDao();
		mCallLogDao = daoSession.getCallLogDao();
		mMessageHistoryDao = daoSession.getMessageHistoryDao();
		mBloodPressureDao = daoSession.getBloodPressureDao();
		mNotReportedDao = daoSession.getNotReportedDao();
		//119는 Default가 119이므로 전화를 걸 수 있도록 데이터를 저장한다.
		set119Tel();
	}
	
	private void set119Tel() {
		if(selectUser(new Users(null, null, null, null, DBInfo.HELPER_TYPE_119)).size() == 0){
			Users user = new Users();
			user.setId((long) 1);
			user.setAddress("서울");
			user.setName("119");
			user.setNumber("119");
			user.setType(DBInfo.HELPER_TYPE_119);
			insertUser(user);
		}
	}
	
	public static ConnectDB getInstance(Context ctx){
		if(mConnDB == null){
			mConnDB = new ConnectDB(ctx);			
		}
		return mConnDB;
	}
	
	public synchronized void insertUser(Users user){
		mUserDao.insertOrReplace(user);
	}
	
	public synchronized void insertSensor(Sensor sensor){
		mSensorDao.insertOrReplace(sensor);
	}
	
	public synchronized void insertActivityData(ActivityData data){
		mActivityDataDao.insertOrReplace(data);
	}
	
	public synchronized void insertCare(Care care){
		mCareDao.insertOrReplace(care);
	}
	
	public synchronized void insertSensorHistory(SensorHistory history){
		mSensorHistoryDao.insertOrReplace(history);
	}
	
	public synchronized void insertCallLog(CallLog calllog){
		mCallLogDao.insertOrReplace(calllog);
	}
	
	public synchronized void insertMessageHistory(MessageHistory message){
		mMessageHistoryDao.insertOrReplace(message);
	}
	
	public synchronized void insertBloodPressureDao(BloodPressure bloodpressure){
		mBloodPressureDao.insertOrReplace(bloodpressure);
	}
	public synchronized void insertnotReportedDao(NotReported notReported){
		LogMgr.d(LogMgr.TAG, "insertnotReportedDao insertData : " + ByteArrayToHex.byteArrayToHex(notReported.getContent(), notReported.getContent().length));
		mNotReportedDao.insertOrReplace(notReported);
	}
	
	
	
	public synchronized void deleteUser(Users user){
		List<Users> userlist = null;
		try {
			QueryBuilder<Users> qb = mUserDao.queryBuilder();
			
			userlist = qb.where(qb.and(
					UsersDao.Properties.Id.eq(user.getId() == null ? 0 : user.getId())
					, UsersDao.Properties.Name.eq(user.getName() == null ? "" : user.getName())
					, UsersDao.Properties.Number.eq(user.getNumber() == null ? "" : user.getNumber())
					, UsersDao.Properties.Address.eq(user.getAddress() == null ? "" : user.getAddress())
					, UsersDao.Properties.Type.eq(user.getType() == null ? "" : user.getType())
					)).build().list();
			
			for(Users deleteUser : userlist){
				mUserDao.delete(deleteUser);
			}
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public synchronized void deleteSensor(Sensor sensor){
		List<Sensor> sensorList = null;
		try {
			
			QueryBuilder<Sensor> qb = mSensorDao.queryBuilder();
			sensorList = qb.where(qb.and(
					SensorDao.Properties.Sensor_id.eq(sensor.getSensor_id() == null ? 0: sensor.getSensor_id())
					, SensorDao.Properties.MAC.eq(sensor.getMAC() == null ? "" : sensor.getMAC())
					, SensorDao.Properties.Type.eq(sensor.getType() == null ? "" : sensor.getType())
					)).build().list();
			
			for(Sensor deleteSensor : sensorList){
				mSensorDao.delete(deleteSensor);
			}
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public synchronized void deleteActivityData(Date endTime){
		List<ActivityData> activityDataList = null;
		try {
			
			QueryBuilder<ActivityData> qb = mActivityDataDao.queryBuilder();
			activityDataList = qb.where(ActivityDataDao.Properties.Time.le(endTime)).build().list();
			
			for(ActivityData delete : activityDataList){
				mActivityDataDao.delete(delete);
			}
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public synchronized void deleteCare(Date startTime, Date endTime){
		List<Care> CareList = null;
		try {
			
			QueryBuilder<Care> qb = mCareDao.queryBuilder();
			CareList = qb.where(
					CareDao.Properties.Time.between(startTime, endTime)
					).build().list();
			
			for(Care delete : CareList){
				mCareDao.delete(delete);
			}
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public synchronized void deleteSensorHistory(Date endTime){
		List<SensorHistory> SensorHistoryList = null;
		try {
			
			QueryBuilder<SensorHistory> qb = mSensorHistoryDao.queryBuilder();
			SensorHistoryList = qb.where(SensorHistoryDao.Properties.Time.le(endTime)).build().list();
			
			for(SensorHistory delete : SensorHistoryList){
				mSensorHistoryDao.delete(delete);
			}
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public synchronized void deleteCallLog(Date startTime, Date endTime){
		List<CallLog> CallLogList = null;
		try {
			
			QueryBuilder<CallLog> qb = mCallLogDao.queryBuilder();
			CallLogList = qb.where(
					qb.and(CallLogDao.Properties.START_TIME.ge(startTime), 
							CallLogDao.Properties.END_TIME.le(endTime)
							)
					).build().list();
			
			for(CallLog delete : CallLogList){
				mCallLogDao.delete(delete);
			}
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public synchronized void deleteMessageHistory(Date EndTime){
		List<MessageHistory> MessageHistoryList = null;
		try {
			
			QueryBuilder<MessageHistory> qb = mMessageHistoryDao.queryBuilder();
			MessageHistoryList = qb.where(MessageHistoryDao.Properties.TIME.le(EndTime)).build().list();
			
			for(MessageHistory delete : MessageHistoryList){
				mMessageHistoryDao.delete(delete);
			}
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public synchronized void deleteBloodPressure(Date startTime, Date endTime){
		List<BloodPressure> BloodPressureList = null;
		try {
			
			QueryBuilder<BloodPressure> qb = mBloodPressureDao.queryBuilder();
			BloodPressureList = qb.where(BloodPressureDao.Properties.TIME.between(startTime, endTime)).build().list();
			
			for(BloodPressure delete : BloodPressureList){
				mBloodPressureDao.delete(delete);
			}
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	// 임시 데이터 입력 삭제 예정
	public void insertTestData() {
		if(selectUser(new Users(null, null, null, null, DBInfo.HELPER_TYPE_119)).size() == 0){
			Users user0 = new Users();
			user0.setId((long) 1);
			user0.setAddress("서울");
			user0.setName("119");
			user0.setNumber("1111111");
			user0.setType(DBInfo.HELPER_TYPE_119);
			insertUser(user0);
		}
		
		if(selectUser(new Users(null, null, null, null, DBInfo.HELPER_TYPE_CARE1)).size() == 0){
			
			Users user1 = new Users();
			user1.setId((long) 2);
			user1.setAddress("미국");
			user1.setName("보호자");
			user1.setNumber("0312129873");
			user1.setType(DBInfo.HELPER_TYPE_CARE1);
			insertUser(user1);
		}
		
		if(selectUser(new Users(null, null, null, null, DBInfo.HELPER_TYPE_CARE2)).size() == 0){
			
			Users user2 = new Users();
			user2.setId((long) 3);
			user2.setAddress("미국");
			user2.setName("보호자");
			user2.setNumber("987654321");
			user2.setType(DBInfo.HELPER_TYPE_CARE2);
			insertUser(user2);
		}

		if(selectUser(new Users(null, null, null, null, DBInfo.HELPER_TYPE_HELPER)).size() == 0){
			Users user3 = new Users();
			user3.setId((long) 4);
			user3.setAddress("부산");
			user3.setName("돌보미");
			user3.setNumber("011122312321234321111111");
			user3.setType(DBInfo.HELPER_TYPE_HELPER);
			insertUser(user3);
			
		}
		
		if(selectUser(new Users(null, null, null, null, DBInfo.HELPER_TYPE_CENTER)).size() == 0){
			
			Users user4 = new Users();
			user4.setId((long) 5);
			user4.setAddress("수원");
			user4.setName("지역센터");
			user4.setNumber("0311234567");
			user4.setType(DBInfo.HELPER_TYPE_CENTER);
			insertUser(user4);
		}
		
		DateFormat format = new SimpleDateFormat("yyyyMMdd");
		
		Care care = new Care();
		care.setSensor_ID(new Long(1));
		care.setMAC("11-11-11-11-11");
		try {
			care.setTime(format.parse("20140513"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		care.setAction(1);
		insertCare(care);
		
		Care care1 = new Care();
		care1.setSensor_ID(new Long(2));
		care1.setMAC("11-11-11-11-12");
		try {
			care1.setTime(format.parse("20140514"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		care1.setAction(0);
		insertCare(care1);
		
		Care care11 = new Care();
		care11.setSensor_ID(new Long(3));
		care11.setMAC("11-11-11-11-13");
		try {
			care11.setTime(format.parse("20140515"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		care11.setAction(1);
		insertCare(care11);
		
/*
		Sensor sensor1 = new Sensor();
		sensor1.setSensor_id((long) 1);
		sensor1.setMAC("0A-C3-5E-CE-6A-E5");
		sensor1.setType(SensorID.getSensorType(UIConstant.ACTIVITY_SENSOR));

		insertSensor(sensor1);
		
		Sensor sensor2 = new Sensor();
		sensor2.setSensor_id((long) 2);
		sensor2.setMAC("0A-C3-5E-CE-6A-E6");
		sensor2.setType(SensorID.getSensorType(UIConstant.ACTIVITY_SENSOR));

		insertSensor(sensor2);
		
		Sensor sensor3 = new Sensor();
		sensor3.setId((long) 3);
		sensor3.setMAC("0A-C3-5E-CE-6A-65");
		sensor3.setType(DBInfo.HELPER_TYPE_GAS);

		insertSensor(sensor3);
		
		Sensor sensor4 = new Sensor();
		sensor4.setId((long) 4);
		sensor4.setMAC("0A-C3-5E-CE-6A-75");
		sensor4.setType(DBInfo.HELPER_TYPE_SORTIE);

		insertSensor(sensor4);
*/
	}
	
	public List<Users> selectAllUsers(){
		return mUserDao.loadAll();
	}
	
	public List<Sensor> selectAllSensors(){
		return mSensorDao.loadAll();
	}
	
	public List<ActivityData> selectAllActivityData(){
		return mActivityDataDao.loadAll();
	}
	
	public List<Care> selectAllCares(){
		return mCareDao.loadAll();
	}
	
	public List<SensorHistory> selectAllSensorHistory(){
		return mSensorHistoryDao.loadAll();
	}
	
	public List<CallLog> selectAllCallLog(){
		return mCallLogDao.loadAll();
	}
	
	public List<MessageHistory> selectAllMessageHistory(){
		return mMessageHistoryDao.loadAll();
	}
	
	public List<BloodPressure> selectAllBloodPressure(){
		return mBloodPressureDao.loadAll();
	}
	
	public List<NotReported> selectAllNotReported(){
		return mNotReportedDao.loadAll();
	}
	
	public List<Users> selectUser(Users user){
		List<Users> userlist = null;
		try {
			QueryBuilder<Users> qb = mUserDao.queryBuilder();
			
			userlist = qb.where(qb.or(
					UsersDao.Properties.Id.eq(user.getId() == null ? 0 : user.getId())
					, UsersDao.Properties.Name.eq(user.getName() == null ? "" : user.getName())
					, UsersDao.Properties.Number.eq(user.getNumber() == null ? "" : user.getNumber())
					, UsersDao.Properties.Address.eq(user.getAddress() == null ? "" : user.getAddress())
					, UsersDao.Properties.Type.eq(user.getType() == null ? "" : user.getType())
					)).build().list();
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userlist;
	}
	
	public List<Sensor> selectSensor(Sensor sensor){
		List<Sensor> sensorList = null;
		try {
			
			QueryBuilder<Sensor> qb = mSensorDao.queryBuilder();
			sensorList = qb.where(qb.or(
					SensorDao.Properties.Sensor_id.eq(sensor.getSensor_id() == null ? 0: sensor.getSensor_id())
					, SensorDao.Properties.MAC.eq(sensor.getMAC() == null ? "" : sensor.getMAC())
					, SensorDao.Properties.Type.eq(sensor.getType() == null ? "" : sensor.getType())
					)).build().list();
			
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sensorList;
	}
	
	public List<ActivityData> selectActivityData(Long SensorID, Date startTime, Date endTime){
		List<ActivityData> userlist = null;
		try {
			QueryBuilder<ActivityData> qb = mActivityDataDao.queryBuilder();
			
			userlist = qb.where(
					qb.and(
							ActivityDataDao.Properties.Sensor_ID.eq(SensorID)
							,ActivityDataDao.Properties.Time.between(startTime, endTime))
					).build().list();
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userlist;
	}
	
	public List<ActivityData> selectActivityData(Date startTime, Date endTime){
		List<ActivityData> userlist = null;
		try {
			QueryBuilder<ActivityData> qb = mActivityDataDao.queryBuilder();
			
			userlist = qb.where(ActivityDataDao.Properties.Time.between(startTime, endTime)).build().list();
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userlist;
	}
	
	public List<Care> selectCare(Date startTime, Date endTime){
		List<Care> userlist = null;
		try {
			QueryBuilder<Care> qb = mCareDao.queryBuilder();
			
			userlist = qb.where(CareDao.Properties.Time.between(startTime, endTime)
					).orderDesc(CareDao.Properties.Time).build().list();
			
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userlist;
	}
	
	public List<SensorHistory> selectSensorHistory(Integer type, Date startTime, Date endTime){
		List<SensorHistory> historyList = null;
		try {
			QueryBuilder<SensorHistory> qb = mSensorHistoryDao.queryBuilder();
			
			historyList = qb.where(
					qb.and(SensorHistoryDao.Properties.TYPE.eq(type),
							SensorHistoryDao.Properties.Time.between(startTime, endTime)
					)).orderDesc(SensorHistoryDao.Properties.Time).build().list();
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return historyList;
	}
	
	public List<CallLog> selectCallLog(Date startTime, Date endTime){
		List<CallLog> CallLogList = null;
		try {
			
			QueryBuilder<CallLog> qb = mCallLogDao.queryBuilder();
			CallLogList = qb.where(
					qb.and(CallLogDao.Properties.START_TIME.ge(startTime), 
							CallLogDao.Properties.END_TIME.le(endTime)
							)
					).build().list();
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return CallLogList;
	}
	
	public List<BloodPressure> selectBloodPressure(Date startTime, Date endTime) {
		LogMgr.d(LogMgr.TAG, "ConnectDB.java:selectBloodPressure:// startTime : " + startTime);
		LogMgr.d(LogMgr.TAG, "ConnectDB.java:selectBloodPressure:// endTime : " + endTime);

		List<BloodPressure> BloodPressureList = null;
		try {
			QueryBuilder<BloodPressure> qb = mBloodPressureDao.queryBuilder();

			BloodPressureList = qb.where(BloodPressureDao.Properties.TIME.between(startTime, endTime))/*.orderDesc(CareDao.Properties.Time)*/.orderDesc(BloodPressureDao.Properties.TIME).build().list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return BloodPressureList;
	}
	
	public List<MessageHistory> selectMessageHistory(Date startTime, Date endTime, int type){
		List<MessageHistory> MessageHistoryList = null;
		try {
			
			QueryBuilder<MessageHistory> qb = mMessageHistoryDao.queryBuilder();
			
			if (type != 0) {
				MessageHistoryList = qb.where(
						qb.and(MessageHistoryDao.Properties.TIME.between(startTime, endTime), 
								MessageHistoryDao.Properties.TYPE.eq(type)
								)
						).build().list();
			}
			else {
				MessageHistoryList = qb.where(MessageHistoryDao.Properties.TIME.between(startTime, endTime)).build().list();
			}
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return MessageHistoryList;
	}
	
	
	public void deleteAllSensor(){
		mSensorDao.deleteAll();
	}
	
	public void deleteAllUser(){
		mUserDao.deleteAll();
	}
	
	public void deleteAllActivityData(){
		mActivityDataDao.deleteAll();
	}
	
	public void deleteAllCare(){
		mCareDao.deleteAll();
	}
	
	public void deleteAllSensorHistory(){
		mSensorHistoryDao.deleteAll();
	}
	
	public void deleteAllCallLog(){
		mCallLogDao.deleteAll();
	}
	
	public void deleteAllMessageHistory(){
		mMessageHistoryDao.deleteAll();
	}
	
	public void deleteAllBloodPressure(){
		mBloodPressureDao.deleteAll();
	}
	
	public void deleteAllNotReported(){
		mNotReportedDao.deleteAll();
	}
	public void insertTData() {
		daoSession.runInTx(new Runnable() {
			@Override
			public void run() {
				long id1 = 257;
				long id2 = 258;
//				Date date = Calendar.getInstance().getTime();
				Calendar date =  new GregorianCalendar();
				date.add(Calendar.MONTH, -1);
				for (int i = 0; i < 720; i++) {
					int data = (int) (Math.random() * 1800) + 1;
					ActivityData ad = null;
					date.add(Calendar.HOUR_OF_DAY, 1);
					if(i / 2 != 0)
						ad = new ActivityData(null, id1, date.getTime(), data);
					else
						ad = new ActivityData(null, id2, date.getTime(), data);
					mActivityDataDao.insert(ad);
				}
				
				date =  new GregorianCalendar();
				date.add(Calendar.MONTH, -1);
				for (int i = 0; i < 32; i++) {
					date.add(Calendar.DATE, 1);
					SensorHistory fire, gas;
					if(i % 2 == 0){
						fire = new SensorHistory(new Long(513), SensorID.FIRE_SENSOR_TYPE.getSensorType(), date.getTime(), 1);
						gas = new SensorHistory(new Long(769), SensorID.GAS_SENSOR_TYPE.getSensorType(), date.getTime(), 1);
					}
					else{
						fire = new SensorHistory(new Long(513), SensorID.FIRE_SENSOR_TYPE.getSensorType(), date.getTime(), 0);
						gas = new SensorHistory(new Long(769), SensorID.GAS_SENSOR_TYPE.getSensorType(), date.getTime(), 0);
					}
					mSensorHistoryDao.insert(fire);
					mSensorHistoryDao.insert(gas);
				}
				Sensor sensor = new Sensor(257, "0A-C3-5E-CE-6A-E5");
				Sensor sensor2 = new Sensor(1025, "01-C1-5E-CE-6A-E5");
				mSensorDao.insert(sensor);
				mSensorDao.insert(sensor2);
				
				
				for (int i = 0; i < 100; i++) {
					int state = i % 2;
					Care care = new Care(null, 1234l, "MAC", new Date(), state);
					mCareDao.insert(care);
				}
				
				date =  new GregorianCalendar();
				date.add(Calendar.WEEK_OF_MONTH, -1);
				BloodPressure test = new BloodPressure(1L, 90, 90, 90, 90, date.getTime());
				mBloodPressureDao.insert(test);
				
				date.add(Calendar.WEEK_OF_MONTH, -1);
				BloodPressure test1 = new BloodPressure(2L, 90, 90, 90, 90, date.getTime());
				mBloodPressureDao.insert(test1);
				
				
			}
		});
	}
	public void insertNotReported(){
		LogMgr.d(LogMgr.TAG, "insertNotReportedData test");
		NotReported n = new NotReported(null, new byte[]{1,2,3,4});
		mNotReportedDao.insert(n);
		NotReported n1 = new NotReported(null, new byte[]{1,2,3,4});
		mNotReportedDao.insert(n1);
		NotReported n2 = new NotReported(null, new byte[]{1,2,3,4});
		mNotReportedDao.insert(n2);
		NotReported n3 = new NotReported(null, new byte[]{1,2,3,4});
		mNotReportedDao.insert(n3);
		NotReported n4 = new NotReported(null, new byte[]{1,2,3,4});
		mNotReportedDao.insert(n4);
	}
}
