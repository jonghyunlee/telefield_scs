package com.telefield.smartcaresystem.database;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit. 
/**
 * Entity mapped to table MESSAGE_HISTORY.
 */
public class MessageHistory {

    private Long id;
    private Integer TYPE;
    private Integer LENGTH;
    private byte[] DATA;
    private Boolean RESULT_TRANSMIT;
    private java.util.Date TIME;

    public MessageHistory() {
    }

    public MessageHistory(Long id) {
        this.id = id;
    }

    public MessageHistory(Integer TYPE, Integer LENGTH, byte[] DATA, Boolean RESULT_TRANSMIT, java.util.Date TIME) {
        this.TYPE = TYPE;
        this.LENGTH = LENGTH;
        this.DATA = DATA;
        this.RESULT_TRANSMIT = RESULT_TRANSMIT;
        this.TIME = TIME;
    }
    
    public MessageHistory(Long id, Integer TYPE, Integer LENGTH, byte[] DATA, Boolean RESULT_TRANSMIT, java.util.Date TIME) {
        this.id = id;
        this.TYPE = TYPE;
        this.LENGTH = LENGTH;
        this.DATA = DATA;
        this.RESULT_TRANSMIT = RESULT_TRANSMIT;
        this.TIME = TIME;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getTYPE() {
        return TYPE;
    }

    public void setTYPE(Integer TYPE) {
        this.TYPE = TYPE;
    }

    public Integer getLENGTH() {
        return LENGTH;
    }

    public void setLENGTH(Integer LENGTH) {
        this.LENGTH = LENGTH;
    }

    public byte[] getDATA() {
        return DATA;
    }

    public void setDATA(byte[] DATA) {
        this.DATA = DATA;
    }

    public Boolean getRESULT_TRANSMIT() {
        return RESULT_TRANSMIT;
    }

    public void setRESULT_TRANSMIT(Boolean RESULT_TRANSMIT) {
        this.RESULT_TRANSMIT = RESULT_TRANSMIT;
    }

    public java.util.Date getTIME() {
        return TIME;
    }

    public void setTIME(java.util.Date TIME) {
        this.TIME = TIME;
    }

}
