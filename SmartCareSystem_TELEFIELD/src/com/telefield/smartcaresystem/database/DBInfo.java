package com.telefield.smartcaresystem.database;

public class DBInfo {
	//Helper Type
	public static final String HELPER_TYPE_HELPER = "helper";
	public static final String HELPER_TYPE_CARE1 = "care1";
	public static final String HELPER_TYPE_CARE2 = "care2";
	public static final String HELPER_TYPE_CENTER = "center";
	public static final String HELPER_TYPE_119 = "119";
	
	//Helper Name
	public static final String HELPER_NAME_HELPER = "돌보미";
	public static final String HELPER_NAME_CARE1 = "말벗";
	public static final String HELPER_NAME_CARE2 = "보호자";
	public static final String HELPER_NAME_CENTER = "센터";
	public static final String HELPER_NAME_119 = "119";
	
	//Sensor Type
	public static final String HELPER_TYPE_FIRE = "화재센서";
	public static final String HELPER_TYPE_GAS = "가스센서";
	public static final String HELPER_TYPE_ACTIVITY = "활동센서";
	public static final String HELPER_TYPE_SORTIE = "활동외출센서";
	
	//Sensor Table Column Name
	public static final String SENSOR_ID = "id";
	public static final String SENSOR_MAC = "mac";
	public static final String SENSOR_TYPE = "type";
	
	// Sensor History Data
	public static final int FIRE_ALERT_ON = 0x01;
	public static final int FIRE_ALERT_CLEAR = 0x00;
	public static final int GAS_ALERT_ON = 0x01;
	public static final int GAS_ALERT_CLEAR = 0x00;
	public static final int DOOR_OPEN = 0x01;
	public static final int DOOR_CLOSE = 0x00;
	public static final int DOOR_LEAVE = 0x10;
	public static final int DOOR_ARRIVAL = 0x11;
	public static final int CODI_EMERGENCY = 0x10;
	public static final int CODI_CANCEL = 0x01;
}
