package com.telefield.smartcaresystem.http;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import com.telefield.smartcaresystem.Constant;
import com.telefield.smartcaresystem.util.LogMgr;

import android.util.Log;

public class DownloadThread extends Thread {
	String ServerUrl;
	String LocalPath;

	public DownloadThread(String serverPath, String localPath) {
		ServerUrl = serverPath;
		LocalPath = localPath;
	}

	@Override
	public void run() {
		URL imgurl;
		int Read;
		try {
			imgurl = new URL(ServerUrl);
			HttpURLConnection conn = (HttpURLConnection) imgurl
					.openConnection();
			int len = conn.getContentLength();
			byte[] tmpByte = new byte[len];
			InputStream is = conn.getInputStream();
			File file = new File(LocalPath);
			new File(Constant.FIRMWARE_PATH).mkdir();
			FileOutputStream fos = new FileOutputStream(file);
			for (;;) {
				Read = is.read(tmpByte);
				LogMgr.d("tmpByte","tmpByte : " + tmpByte);
				if (Read <= 0) {
					break;
				}
				fos.write(tmpByte, 0, Read);
			}
			is.close();
			fos.close();
			conn.disconnect();

		} catch (MalformedURLException e) {
			Log.e("ERROR1", e.getMessage());
		} catch (IOException e) {
			Log.e("ERROR2", e.getMessage());
			e.printStackTrace();
		}
	}
}