package com.telefield.smartcaresystem.sensor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.joda.time.DateTime;

import android.content.Context;

import com.telefield.smartcaresystem.ICallBackActivity;
import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.database.ActivityData;
import com.telefield.smartcaresystem.database.Care;
import com.telefield.smartcaresystem.database.ConnectDB;
import com.telefield.smartcaresystem.database.Sensor;
import com.telefield.smartcaresystem.m2m.GateWayMessage;
import com.telefield.smartcaresystem.m2m.GatewayIndex;
import com.telefield.smartcaresystem.m2m.ProtocolFrame;
import com.telefield.smartcaresystem.m2m.ProtocolManger;
import com.telefield.smartcaresystem.message.SensorID;
import com.telefield.smartcaresystem.util.LogMgr;

public class SensorDeviceManager {
	
	final static int MAX_SENSOR_CNT = 0xff;	
	
	private Context mContext;
	
	private ConnectDB con;
	
	LinkedList<Sensor> sensorList;
	LinkedList<Care> careList;
	
	ICallBackActivity callBackActivityCnt;
	
	public SensorDeviceManager(Context mContext)
	{
		this.mContext = mContext;
		sensorList = new LinkedList<Sensor>();
		careList = new LinkedList<Care>();
		
		con = ConnectDB.getInstance(mContext);		
		sensorList.addAll(con.selectAllSensors());
		careList.addAll(con.selectAllCares());
		
		LogMgr.d(LogMgr.TAG,"Init DB");
		//저장된 db 리스트들
		for(Sensor sensor : sensorList){
			
			LogMgr.d(LogMgr.TAG,"SENSOR ID: " + sensor.getSensor_id() + " :: " + sensor.getTypeStr());
		}
		
		int empty_idx = getEmptySensorIndex(SensorID.DOOR_SENSOR_TYPE.getSensorType());
		LogMgr.d(LogMgr.TAG,"DOOR EMPTY : " + empty_idx);
		
		
	}
	public void addCodi(int sensor_id, String mac)
	{
		if(isDuplicate(mac))
		{
			removeSensor(mac);
		}
		//else		
		{
			Sensor sensor = new Sensor(sensor_id,mac);
			sensor.setReg_status(1);
			sensor.setRssi(new Integer(0));
			sensor.setSensor_batt(new Integer(0));
			sensor.setZigbee_batt(new Integer(0));
			sensor.setReg_status(new Integer(1));
			sensor.setRegister_time(new Date());
			sensor.setLast_communication_time(new Date());
			sensor.setKeepAliveTime(new Integer(0));
			sensor.setVersion(new Integer(0));
			sensor.setBatt_percentage(new Integer(0));
			
			sensorList.add(sensor);		
			
			//db 등록
			ConnectDB con = ConnectDB.getInstance(mContext);
			con.insertSensor(sensor);
		}		
	}
	public void addSensor(int sensor_id, String mac)
	{
		if(isDuplicate(mac))
		{
			removeSensor(mac);
		}
		//else		
		{
			Sensor sensor = new Sensor(sensor_id,mac);
			sensor.setReg_status(1);
			sensorList.add(sensor);		
			
			//db 등록
			ConnectDB con = ConnectDB.getInstance(mContext);
			SmartCareSystemApplication.getInstance().getProtocolManger().insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REGISTER_SENSOR, sensor, null));
			SmartCareSystemApplication.getInstance().getProtocolManger().messageSend(null);
			con.insertSensor(sensor);
		}		
	}
	public void addSensor(int sensor_id, String mac, int version)
	{
		if(isDuplicate(mac))
		{
			removeSensor(mac);
		}
		//else		
		{
			Sensor sensor = new Sensor(sensor_id,mac,version);
			sensor.setReg_status(1);
			sensorList.add(sensor);		
			
			//db 등록
			ConnectDB con = ConnectDB.getInstance(mContext);
			SmartCareSystemApplication.getInstance().getProtocolManger().insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REGISTER_SENSOR, sensor, null));
			SmartCareSystemApplication.getInstance().getProtocolManger().messageSend(null);
			con.insertSensor(sensor);
		}		
	}
	
	public void removeSensor(int sensor_id,String mac)
	{
		int index = getSensorIndex(sensor_id);
		
		if( index >= 0)
		{
			Sensor deleteSensor = sensorList.get(index);
			deleteSensor.setReg_status(0);
			sensorList.remove(index);
			
//			SmartCareSystemApplication.getInstance().getProtocolManger().changeRegistration(deleteSensor);
			SmartCareSystemApplication.getInstance().getProtocolManger().insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REGISTER_SENSOR, deleteSensor, null));
			SmartCareSystemApplication.getInstance().getProtocolManger().messageSend(null);
			ConnectDB con = ConnectDB.getInstance(mContext);	
			con.deleteSensor(deleteSensor);
		}
	}
	
	public void removeSensor(String mac)
	{
		int index = getSensorIndex(mac);
		
		if( index >= 0)
		{
			Sensor deleteSensor = sensorList.get(index);
			deleteSensor.setReg_status(0);
			sensorList.remove(index);
			
//			SmartCareSystemApplication.getInstance().getProtocolManger().changeRegistration(deleteSensor);
			SmartCareSystemApplication.getInstance().getProtocolManger().insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REGISTER_SENSOR, deleteSensor, null));
			SmartCareSystemApplication.getInstance().getProtocolManger().messageSend(null);
			ConnectDB con = ConnectDB.getInstance(mContext);	
			con.deleteSensor(deleteSensor);
		}
	}
	
	//Care
	public void visitCareSensor(int sensor_id, String mac) {
		Care care = new Care();
		care.setSensor_ID(new Long(sensor_id));
		care.setMAC(mac);
		care.setTime(new Date());
		care.setAction(1);
		careList.add(care);
		
		//db 등록
		ConnectDB con = ConnectDB.getInstance(mContext);
		con.insertCare(care);
		
		ProtocolManger mgr = SmartCareSystemApplication.getInstance().getProtocolManger();
		Sensor careToSensor = new Sensor(sensor_id, mac);
		careToSensor.setSensor_batt(100);
		careToSensor.setComm_status(1);
		mgr.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_SENSOR_EVENT, careToSensor, GateWayMessage.DATA_COME_CARE));
		mgr.messageSend(null);
	}
	
	public void outCareSensor(int sensor_id, String mac) {
		Care care = new Care();
		care.setSensor_ID(new Long(sensor_id));
		care.setMAC(mac);
		care.setTime(new Date());
		care.setAction(0);
		careList.add(care);
		
		//db 등록
		ConnectDB con = ConnectDB.getInstance(mContext);
		con.insertCare(care);
		
		ProtocolManger mgr = SmartCareSystemApplication.getInstance().getProtocolManger();
		Sensor careToSensor = new Sensor(sensor_id, mac);
		careToSensor.setSensor_batt(100);
		careToSensor.setComm_status(1);
		mgr.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_SENSOR_EVENT, careToSensor, GateWayMessage.DATA_OUT_CARE));
		mgr.messageSend(null);
	}
	
	public boolean isDuplicate( int sensor_id)
	{		
		Iterator<Sensor> it = sensorList.iterator();
		
		while(it.hasNext())
		{
			Sensor sensor = it.next();
			
			if( sensor.getSensor_id() == sensor_id )
			{
				return true;
			}
		}
		
		return false;		
	}
	
	public boolean isDuplicate( String mac)
	{		
		Iterator<Sensor> it = sensorList.iterator();
		
		while(it.hasNext())
		{
			Sensor sensor = it.next();
			
			if( sensor.getMAC().equals(mac))
			{
				return true;
			}
		}
		
		return false;		
	}
	
	private int getSensorIndex(int sensor_id)
	{
		int index = 0;
		Iterator<Sensor> it = sensorList.iterator();		
		
		while(it.hasNext())
		{
			Sensor sensor = it.next();
			
			if( sensor.getSensor_id() == sensor_id)
			{
				return index;
			}
			
			index++;
		}
		return -1;
	}
	
	private int getSensorIndex(String mac)
	{
		int index = 0;
		Iterator<Sensor> it = sensorList.iterator();		
		
		while(it.hasNext())
		{
			Sensor sensor = it.next();
			
			if( sensor.getMAC().equals(mac))
			{
				return index;
			}
			
			index++;
		}
		return -1;
	}
	
	public void setSensorInfoData(int sensor_id, int rssi, byte sensor_batt, byte zigbee_batt, byte comm_status, byte reg_status, int batt_percentage,int keepAliveTime, int sensor_version )
	{
		int index = getSensorIndex(sensor_id);
		
		if( index >=0)
		{
			Sensor sensor = sensorList.get(index);
			
			sensor.setRssi(new Integer(rssi));			
			sensor.setSensor_batt(new Integer(sensor_batt));
			sensor.setZigbee_batt(new Integer(zigbee_batt));
			sensor.setComm_status(new Integer(comm_status));
			sensor.setReg_status(new Integer(reg_status));			
			sensor.setLast_communication_time(new Date(System.currentTimeMillis()));
			sensor.setSensor_batt(batt_percentage);
			sensor.setKeepAliveTime(keepAliveTime);
			sensor.setVersion(new Integer(sensor_version));
			//db 등록
			ConnectDB con = ConnectDB.getInstance(mContext);	
			con.insertSensor(sensor);
		}		
	}
	
	public void setSensorInfoData(int sensor_id, boolean flag)
	{
		//flag false 전원차단 // flag true 전원 복구
		int index = getSensorIndex(sensor_id);
		
		if( index >=0)
		{
			Sensor sensor = sensorList.get(index);
		
			sensor.setLast_communication_time(new Date(System.currentTimeMillis()));
			sensor.setComm_status(flag == true ? new Integer(1) : new Integer(0));
			sensor.setSensor_batt(flag == true ? new Integer(100) : new Integer(0));
			sensor.setZigbee_batt(flag == true ? new Integer(0) : new Integer(1));
			//db 등록
			ConnectDB con = ConnectDB.getInstance(mContext);	
			con.insertSensor(sensor);
		}		
	}
	
	public List<Sensor> getSensorList(){
		return sensorList;
	}
	
	public void clearSensorList(){
		sensorList.clear();
		careList.clear();
	}
	
	public void updateActivityPirCnt(int sensor_id, int pir_cnt)	
	{
		int index = getSensorIndex(sensor_id);
		if( index >=0)
		{
			Sensor sensor = sensorList.get(index);
			
			sensor.setCurrPirCnt(pir_cnt + sensor.getCurrPirCnt());
			
			if( callBackActivityCnt != null)
				callBackActivityCnt.notifyActivityCnt(sensor_id,pir_cnt);
		}	
	}
	
	public Sensor getSensor(int sensor_id){
		int index = getSensorIndex(sensor_id);
		Sensor sensor = null;
		if( index >=0)
			sensor = sensorList.get(index);
		return sensor;
	}
	
	public Sensor getSensor(String MAC) {
		Iterator<Sensor> it = sensorList.iterator();

		while (it.hasNext()) {
			Sensor sensor = it.next();
			if (sensor.getMAC().equals(MAC))
				return sensor;
		}
		return null;
	}
	
	public int getEmptySensorIndex(int sensor_type)
	{
		LogMgr.d(LogMgr.TAG, "SensorDeviceManager.java:getEmptySensorIndex// sensor_type : " + sensor_type);
		boolean is_fill[] = new boolean[255];
		
		Arrays.fill(is_fill, false);
				
		
		Iterator<Sensor> it = sensorList.iterator();		
		
		while(it.hasNext())
		{
			Sensor sensor = it.next();
			
			if( SensorID.getSensorType(sensor.getSensor_id().intValue()) == sensor_type)
			{
				int idx = SensorID.getSensorIndex(sensor.getSensor_id().intValue());
				if( idx < 255 ){
					is_fill[idx] = true;
				}else
				{
					LogMgr.e(LogMgr.TAG, "getEmptySensorIndex Error. " + idx);
				}
			}
		}
		
		//가장 가까운 empty id를 넘겨준다. //sensor id 는 1부터 시작한다.
		
		int empty_idx = 0;
		for( empty_idx = 1; empty_idx < 255; empty_idx++){
			if( is_fill[empty_idx] == false ) break;
		}
		LogMgr.d(LogMgr.TAG, "SensorDeviceManager.java:getEmptySensorIndex// empty_idx : " + empty_idx);
		if( empty_idx == 255 ) return -1;
		return empty_idx;
	}
	
	
	public boolean isRegisteredSensor(int sensor_id)
	{
		boolean isRegistered = false;
		
		Iterator<Sensor> it = sensorList.iterator();		
		
		while(it.hasNext())
		{
			Sensor sensor = it.next();
			
			if( sensor.getSensor_id() == sensor_id)
			{
				isRegistered = true;
				break;
			}
		}
		return isRegistered;
	}
	
//	public void insertActivityPirCnt()
//	{
//		//모든 활동센서를 찾아서 db insert를 진행한다.		
//		Iterator<Sensor> it = sensorList.iterator();
//		while(it.hasNext())
//		{
//			Sensor sensor = it.next();		
//			
//			if( sensor.getType().intValue() == SensorID.ACTIVITY_SENSOR_TYPE.getSensorType()
//					|| sensor.getType().intValue() == SensorID.DOOR_SENSOR_TYPE.getSensorType() //door sensor 도 activity sensor 이다.
//					) //활동 센서인지 check
//			{
//				Long sensor_id = sensor.getSensor_id();
//				int pirCnt = sensor.getCurrPirCnt();
//				
//				if( pirCnt <= 0 ) continue;
//				
//				ActivityData activityData = new ActivityData();
//				activityData.setCount(pirCnt);
//				activityData.setTime(new Date(System.currentTimeMillis()));
//				activityData.setSensor_ID(sensor_id);
//				
//				//todo db insert
//				ConnectDB con = ConnectDB.getInstance(mContext);	
//				con.insertActivityData(activityData);
//				
//				//pir count init
//				sensor.setCurrPirCnt(0);
//				
//				updateActivitySensor(sensor_id.intValue()); //4시간 pircnt 를 업데이트 하기위해서..
//			}
//		}
//	}
	
	public void insertActivityPirCnt(int sensorId)
	{
		Sensor sensor = getSensor(sensorId);
		
		Long sensor_id = sensor.getSensor_id();
		int pirCnt = sensor.getCurrPirCnt();
		
		if( pirCnt <= 0 ) return;
		
		ActivityData activityData = new ActivityData();
		activityData.setCount(pirCnt);
		activityData.setTime(new Date(System.currentTimeMillis()));
		activityData.setSensor_ID(sensor_id);
		
		//todo db insert
		ConnectDB con = ConnectDB.getInstance(mContext);	
		con.insertActivityData(activityData);
		
		//pir count init
		sensor.setCurrPirCnt(0);		
	}
	
	public void updateSensors()
	{
		final long THIRTY_MINUTES = 30 * 60 * 1000; // 30분
		//모든 활동센서를 찾아서 30분동안 응답이 있는지 없는지 체크 한다.
		//없으면 통실 불량
		Iterator<Sensor> it = sensorList.iterator();
		while(it.hasNext())
		{
			Sensor sensor = it.next();	
			
			long last_comm_time = sensor.getLast_communication_time().getTime();
			long curr_time = System.currentTimeMillis();
			
			if( curr_time > last_comm_time )
			{
				long diff = curr_time - last_comm_time;
				if( diff > THIRTY_MINUTES )
				{
					//통실 불량이다. //need to db update
					ConnectDB con = ConnectDB.getInstance(mContext);					
					sensor.setReg_status(new Integer(Sensor.SENSOR_NOT_READY));
					con.insertSensor(sensor);
				}
			}
		}
		
	}
	
	public List<Long> getSensorID(int type) {
		List<Long> returnID = new ArrayList<Long>();

		Iterator<Sensor> it = sensorList.iterator();

		while (it.hasNext()) {
			Sensor sensor = it.next();

			if (sensor.getType().equals(type)) {
				returnID.add(sensor.getSensor_id());
			}
		}
		return returnID;
	}
	
	public List<Sensor> getSensorList(int type) {
		List<Sensor> lists = new ArrayList<Sensor>();

		Iterator<Sensor> it = sensorList.iterator();

		while (it.hasNext()) {
			Sensor sensor = it.next();

			if (sensor.getType().equals(type)) {
				lists.add(sensor);
			}
		}
		return lists;
	}
	
//	public void updateActivitySensor(int sensorId)
//	{
//		int pir_cnt[]={0,0,0,0};  	//첫번째 바이트 4시간 전, 두번째 바이트 3시간전, 세번째 바이트 2시간전, 네번재 바이트 1시간전
//		
//		//활동센서가 아니면 return
//		if( SensorID.getSensorType(sensorId) != SensorID.ACTIVITY_SENSOR_TYPE.getSensorType()
//				&& SensorID.getSensorType(sensorId) != SensorID.DOOR_SENSOR_TYPE.getSensorType()
//				)
//		{
//			LogMgr.d(LogMgr.TAG,"updateActivitySensor wrong sensor");
//			return;
//		}
//		
//		Sensor sensor = getSensor(sensorId);
//
//		ConnectDB con = ConnectDB.getInstance(mContext);
//		int diff_time = 4;
//		
//		long curr = System.currentTimeMillis()
//				/ Constant.TIME_HOUR_UNIT;
//		
//		LogMgr.d(LogMgr.TAG, "SensorDeviceManager.java:updateActivitySensor:// sensor.getSensor_id() : " + sensor.getSensor_id());
//
//		for( int i=0; i < diff_time ; i++) // 0: 1시간전    3 : 4시간전
//		{
//			LogMgr.d(LogMgr.TAG, "SensorDeviceManager.java:updateActivitySensor:// i : " + i);
//
//			Date end_date = new Date((curr - i + 1) * Constant.TIME_HOUR_UNIT);
//			Date start_date = new Date((curr -i -1 + 1)  * Constant.TIME_HOUR_UNIT);
//			LogMgr.d(LogMgr.TAG, "SensorDeviceManager.java:updateActivitySensor:// start_date : " + start_date);
//			LogMgr.d(LogMgr.TAG, "SensorDeviceManager.java:updateActivitySensor:// end_date : " + end_date);
//
//			List<ActivityData> lists = con.selectActivityData((long) sensorId, start_date,end_date);
//			LogMgr.d(LogMgr.TAG, "SensorDeviceManager.java:updateActivitySensor:// lists.size() : " + lists.size());
//
//			int pir = 0;
//			if( lists != null && lists.size() != 0)
//			{
//				for( ActivityData data : lists)
//				{
//					pir += data.getCount();
//				}
//			}
//			
//			pir_cnt[diff_time-1-i] = pir;
//			LogMgr.d(LogMgr.TAG,"PIR COUNT index : " + (diff_time-1-i) + ":COUNT : " +pir_cnt[diff_time-1-i] );
//		}
//		
//		if ( sensor != null)
//			sensor.setPirCnt(pir_cnt);	
//		
//	}
	
	public void setActivityEventListener(ICallBackActivity callback)
	{
		callBackActivityCnt = callback;
	}
	
	public void removeActivityEventListener()
	{
		callBackActivityCnt= null;
	}
	
	
}
	