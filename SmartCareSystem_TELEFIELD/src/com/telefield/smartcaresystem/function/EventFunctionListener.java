package com.telefield.smartcaresystem.function;

import java.util.Iterator;
import java.util.LinkedList;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.telefield.smartcaresystem.util.LogMgr;

public class EventFunctionListener {
	
	LinkedList<Handler> notifyHandlerList;
	int NOTIFY_MESSAGE_ID = -1;	
	
	public EventFunctionListener()
	{
		notifyHandlerList = new LinkedList<Handler>();
	}

	public void addEventListener(Handler listener)
	{
		if( listener != null)
			notifyHandlerList.add(listener);
	}
	public void removeEventListener(Handler listener)
	{
		if( listener != null)
			notifyHandlerList.remove(listener);
	}
	
	public void setNotifyMessageId(int messageId)
	{
		NOTIFY_MESSAGE_ID = messageId;
	}
	
	public int getSize()
	{
		return notifyHandlerList.size();
	}
	
	
	public boolean notify(Object frame)
	{
		boolean isHandle = false;
		Iterator<Handler> it = notifyHandlerList.iterator();
		LogMgr.d(LogMgr.TAG,"notify");
		while(it.hasNext())
		{
			Handler handler = it.next();
			isHandle = true;
			LogMgr.d(LogMgr.TAG,"notify :: ");
			
			Message msg = Message.obtain(handler, NOTIFY_MESSAGE_ID , frame);
			Bundle bundle = new Bundle();
			msg.setData(bundle);
			handler.sendMessage(msg);		
		}
		
		return isHandle;
	}	

}
