package com.telefield.smartcaresystem.function;

import java.util.Iterator;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.message.ErrorCode;
import com.telefield.smartcaresystem.message.MessageID;
import com.telefield.smartcaresystem.message.MessageManager;
import com.telefield.smartcaresystem.ui.UIConstant;
import com.telefield.smartcaresystem.util.LogMgr;

/**
 * 단말에서 등록모드를 선택했을때 기능을 수행하는 class
 * 주요 기능은 REG_MOD_MSG 전송을 한후 REG_INFO_MSG를 수신대기한다.  
 * REG_INFO_MSG를 수신하면 센서 해지 등을 수행한다.  
 * ACK 메시지 를 전송한다.
 * 
 * @author Administrator
 *
 */
public class SensorRegisterFunction extends EventFunctionListener {	
	
	MessageManager sensorProcessHandler;
	
	public SensorRegisterFunction()
	{
		super();
		sensorProcessHandler = SmartCareSystemApplication.getInstance().getMessageManager();
		
		setNotifyMessageId(UIConstant.NOTIFY_MESSAGE);
		
	}		
		
	
	/**
	 * 등록 메시지 전달
	 * @param sensor_id
	 * @param msg_id
	 * @param sub_data
	 */
	public int registerSensor( int sensor_id , String address)
	{		
		if(!sensorProcessHandler.ftdi_isConnect())
		{
			notifyDisconnectError();
			return ErrorCode.NOT_CONNECT;
		}
		return sensorProcessHandler.sendMessage(sensor_id, (byte)MessageID.REG_MOD_MSG, address == null ? null : address.getBytes());
	}
	
	public int deregisterSensor( int sensor_id  )
	{
		if(!sensorProcessHandler.ftdi_isConnect())
		{
			notifyDisconnectError();
			return ErrorCode.NOT_CONNECT;
		}
		return sensorProcessHandler.sendMessage(sensor_id, (byte)MessageID.REG_CLR_MSG, null);
	}	
	
	public void notifyDisconnectError()
	{
		Iterator<Handler> it = notifyHandlerList.iterator();
		LogMgr.d(LogMgr.TAG,"notifyDisconnectError notify");
		while(it.hasNext())
		{
			Handler handler = it.next();
			
			LogMgr.d(LogMgr.TAG,"notify :: ");
			
			Message msg = Message.obtain(handler, UIConstant.NOTIFY_FTDI_DISCONNECTED);			
			Bundle bundle = new Bundle();
			msg.setData(bundle);
			handler.sendMessage(msg);		
		}		
	}
	
}
