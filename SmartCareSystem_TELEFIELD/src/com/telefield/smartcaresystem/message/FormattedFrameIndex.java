package com.telefield.smartcaresystem.message;

public interface FormattedFrameIndex {
	
	static int START		= 0;
	static int VERSION		= 1;
	static int LENGTH		= 2;
	static int SEQ_NUM		= 4;
	static int SENSOR_ID	= 5;
	static int MSG_ID		= 7;
	static int SUB_DATA		= 8;
	
	static byte START_DATA 	= 0x02;
	static byte END_DATA	= 0x03;


}
