package com.telefield.smartcaresystem.message;

import com.telefield.smartcaresystem.ui.UIConstant;

public enum SensorID {
	/** sensor type */
	
	ACTIVITY_SENSOR_TYPE(0x01,"ACTIVITY_SENSOR_TYPE", UIConstant.ACTIVITY_SENSOR),
	FIRE_SENSOR_TYPE(0x02, "FIRE_SENSOR_TYPE",UIConstant.FIRE_SENSOR),
	GAS_SENSOR_TYPE(0x03,"GAS_SENSOR_TYPE",UIConstant.GAS_SENSOR),
	DOOR_SENSOR_TYPE(0x04,"DOOR_SENSOR_TYPE",UIConstant.DOOR_SENSOR),
	OUT_ACTIVITY_SENSOR_TYPE(0x05, "OUT_ACTIVITY_SENSOR_TYPE","외출활동센서"),
	CALL_EMERGENCY_SENSOR_TYPE(0x06, "CALL_EMERGENCY_SENSOR_TYPE",UIConstant.EMERGENCY_SENSOR),
	GAS_BREAKER_TYPE(0x07, "GAS_BREAKER_TYPE", UIConstant.GAS_BREAKER_SENSOR),
	BLOOD_PRESURE_TYPE(0x08, "BLOOD_PRESURE_TYPE", "무선맥박계"),
	CARE_SENSOR_TYPE(0x80,"CARE_SENSOR_TYPE",UIConstant.CARE_SENSOR),
	CODI_SENSOR_TYPE(0xC0,"CODI_SENSOR_TYPE","본체"),
	MAX_SENSOR_TYPE(0xff,"MAX_SENSOR_TYPE","최대");
	
	
	private int sensor_type;
	private String msg;
	private String sensor_name;
	

	
	public static final byte SENSOR_BATT_OK = 0x0;
	public static final byte SENSOR_BATT_LOW = 0x1;
	
	SensorID(int sensor_type, String msg, String sensor_name)
	{
		this.sensor_type = sensor_type;
		this.msg = msg;
		this.sensor_name = sensor_name;
	}
	
	public String getString(){
		return msg;
	}
	public String getSensorName(){
		return sensor_name;
	}
	public int getSensorType()
	{
		return sensor_type;
	}
	
	public static String getSensorTypeString(int sensor_id)
	{
		
		int sensor_type = getSensorType(sensor_id);
		String msg_type ="UNKNOWN";
		
		switch(sensor_type)
		{
		case 0x1:
			msg_type =  ACTIVITY_SENSOR_TYPE.getString();
			break;
		case 0x2:
			msg_type =  FIRE_SENSOR_TYPE.getString();
			break;
		case 0x3:
			msg_type =  GAS_SENSOR_TYPE.getString();
			break;
		case 0x4:
			msg_type =  DOOR_SENSOR_TYPE.getString();
			break;
		case 0x5:
			msg_type =  OUT_ACTIVITY_SENSOR_TYPE.getString();
			break;
		case 0x6:
			msg_type =  CALL_EMERGENCY_SENSOR_TYPE.getString();
			break;
		case 0x7:
			msg_type =  GAS_BREAKER_TYPE.getString();
			break;
		default:
			if( sensor_type >= CARE_SENSOR_TYPE.getSensorType() && sensor_type < CODI_SENSOR_TYPE.getSensorType())
			{
				msg_type = CARE_SENSOR_TYPE.getString();
			}else if(sensor_type >= CODI_SENSOR_TYPE.getSensorType() && sensor_type <= MAX_SENSOR_TYPE.getSensorType())
			{
				msg_type = CODI_SENSOR_TYPE.getString();
			}
			break;
		
		}
		
		return msg_type;	
		
	}
	
	public static int getSensorType(int sensor_id)
	{
		return ( 0xff00 & sensor_id ) >> 8;		
	}
	
	public static int getSensorIndex(int sensor_id)
	{
		return ( 0x00ff & sensor_id );
	}
	
	public static int getSensorId(int sensor_type, int sensor_index)
	{
		return ( ((sensor_type & 0xff ) << 8) +  (sensor_index & 0xff));
	}
	
	public static int getSensorType(String sensorTypeStr)
	{
		int type = 0;
		
		if(sensorTypeStr==null ) return -1;
		
		if( sensorTypeStr.equalsIgnoreCase(UIConstant.ACTIVITY_SENSOR))
		{
			type = ACTIVITY_SENSOR_TYPE.getSensorType();
		}else if(sensorTypeStr.equalsIgnoreCase(UIConstant.FIRE_SENSOR))
		{
			type = FIRE_SENSOR_TYPE.getSensorType();
		}else if(sensorTypeStr.equalsIgnoreCase(UIConstant.GAS_SENSOR))
		{
			type = GAS_SENSOR_TYPE.getSensorType();
		}
		else if(sensorTypeStr.equalsIgnoreCase(UIConstant.DOOR_SENSOR))
		{
			type = DOOR_SENSOR_TYPE.getSensorType();
		}else if(sensorTypeStr.equalsIgnoreCase(UIConstant.OUT_ACTIVITY_SENSOR))
		{
			type = OUT_ACTIVITY_SENSOR_TYPE.getSensorType();
		}else if(sensorTypeStr.equalsIgnoreCase(UIConstant.CARE_SENSOR))
		{
			type = CARE_SENSOR_TYPE.getSensorType();
		}else if(sensorTypeStr.equalsIgnoreCase(UIConstant.GAS_BREAKER_SENSOR))
		{
			type = GAS_BREAKER_TYPE.getSensorType();
		}else if(sensorTypeStr.equalsIgnoreCase(UIConstant.EMERGENCY_SENSOR))
		{
			type = CALL_EMERGENCY_SENSOR_TYPE.getSensorType();
		}
		
		return type;		
	}	
	
	public static String getSensorName(int sensor_id)
	{
		int sensor_type = getSensorType(sensor_id);
		String sensor_name ="UNKNOWN";
		
		switch(sensor_type)
		{
		case 0x1:
			sensor_name =  ACTIVITY_SENSOR_TYPE.getSensorName();
			break;
		case 0x2:
			sensor_name =  FIRE_SENSOR_TYPE.getSensorName();
			break;
		case 0x3:
			sensor_name =  GAS_SENSOR_TYPE.getSensorName();
			break;
		case 0x4:
			sensor_name =  DOOR_SENSOR_TYPE.getSensorName();
			break;
		case 0x5:
			sensor_name =  OUT_ACTIVITY_SENSOR_TYPE.getSensorName();
			break;
		case 0x6:
			sensor_name =  CALL_EMERGENCY_SENSOR_TYPE.getSensorName();
			break;
		case 0x7:
			sensor_name =  GAS_BREAKER_TYPE.getSensorName();
			break;	
		default:
			if( sensor_type >= CARE_SENSOR_TYPE.getSensorType() && sensor_type < CODI_SENSOR_TYPE.getSensorType())
			{
				//sensor_name = CARE_SENSOR_TYPE.getString();
				sensor_name = CARE_SENSOR_TYPE.getSensorName();
			}else if(sensor_type >= CODI_SENSOR_TYPE.getSensorType() && sensor_type <= MAX_SENSOR_TYPE.getSensorType())
			{
				sensor_name = CODI_SENSOR_TYPE.getSensorName();
			}
			break;
		
		}
		
		return sensor_name;	
	}
}

