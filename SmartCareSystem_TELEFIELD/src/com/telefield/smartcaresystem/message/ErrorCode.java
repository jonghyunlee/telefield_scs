package com.telefield.smartcaresystem.message;

public interface ErrorCode {
	
	public static int NOT_CONNECT = -6;
	public static int DUPLICATE = -5;	
	public static int BUSY = -4;
	public static int EMPTY_DATA = -3;
	public static int NOT_SUPPORT = -2;
	public static int ERROR = -1;	
	public static int SUCCESS = 0;
	

}
