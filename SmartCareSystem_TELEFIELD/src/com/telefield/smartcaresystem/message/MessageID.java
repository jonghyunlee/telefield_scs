package com.telefield.smartcaresystem.message;

public interface MessageID {	 
	
	public static int ACK_MSG = 0x01;
	
	public static int PIR_CNT_MSG = 0x02;
	
	public static int CUR_PIR_CNT_REQ = 0x03;
	public static int CUR_PIR_CNT_MSG = 0x04;
	
	public static int FIRE_DET_MSG = 0x05;
	public static int FIRE_CLR_MSG = 0x06;
	
	public static int GAS_DET_MSG = 0x07;
	public static int GAS_CLR_MSG = 0x08;
	
	public static int OUT_DET_MSG = 0x09;
	public static int EMG_KEY_MSG = 0x0a;
	
	public static int REG_MOD_MSG = 0x0b;
	public static int REG_INFO_MSG = 0x0c;
	public static int REG_CLR_MSG = 0x0d;
	public static int RESTART_MSG = 0x0e;
	public static int NODE_INFO_REQ = 0x0f;
	public static int NODE_STAT_MSG = 0x10;
	public static int GW_SLP_REQ = 0x11;
	public static int LINK_TEST_MSG = 0x12;
	public static int PWR_CONT_REQ = 0x13;
	public static int GW_BUSY_MSG = 0x14;
	public static int PWR_STAT_MSG = 0x15;
	public static int LED_ONOFF_REQ = 0x16;
	public static int DOOR_STAT_MSG = 0x17;
	public static int REG_MAC_REQ = 0x18;
	public static int SENPWR_STAT_MSG = 0x19;
	public static int REG_INFO_CHG_MSG = 0x1a;
	public static int GW_TEL_NR_REQ = 0x1b;
	public static int GW_TEL_INFO_MSG = 0x1c;
	public static int REG_ESC_MSG = 0x1d;	
	public static int KEEP_ALIVE_TIME_REQ = 0x1e;
	
	public static int EMG_ALARM_MSG = 0x1f;
	public static int GAS_BREAK_MSG = 0x20;
	public static int GAS_COMP_MSG = 0x21;
	
	
	public static int COORD_EVENT_MSG = 0x30;
	public static int GATEWAY_EVENT_MSG  = 0x31;
	
//	public static int COORD_EVENT_MSG = 0x20;
//	public static int GATEWAY_EVENT_MSG  = 0x21;
	
	public static int DL_ENTER_MSG = 0x70;
	public static int DL_PKT_MSG = 0x71;
	public static int DL_STOP_MSG = 0x72;
	
	public static int MAX_MESSAGE_ID = DL_STOP_MSG + 1;
	
	public static String MSG_TYPE_STR[] ={
		"",
		"ACK_MSG",                   //0x1
		"PIR_CNT_MSG",				 //0x2
		"CUR_PIR_CNT_REQ",
		"CUR_PIR_CNT_MSG",
		"FIRE_DET_MSG",
		"FIRE_CLR_MSG",
		"GAS_DET_MSG",
		"GAS_CLR_MSG",
		"OUT_DET_MSG",
		"EMG_KEY_MSG",
		"REG_MOD_MSG",
		"REG_INFO_MSG",
		"REG_CLR_MSG",
		"RESTART_MSG",
		"NODE_INFO_REQ",
		"NODE_STAT_MSG",			//0x10
		"GW_SLP_REQ",				//0x11
		"LINK_TEST_MSG",
		"PWR_CONT_REQ",
		"GW_BUSY_MSG",
		"PWR_STAT_MSG",
		"LED_ONOFF_REQ",
		"DOOR_STAT_MSG",
		"REG_MAC_REQ",
		"SENPWR_STAT_MSG",
		"REG_INFO_CHG_MSG",
		"GW_TEL_NR_REQ",
		"GW_TEL_INFO_MSG",		
		"REG_ESC_MSG",				//0x1d
		"KEEP_ALIVE_TIME_REQ",		//0x1e
		"EMG_ALARM_MSG",			//0x1f
		"GAS_BREAK_MSG",			//0x20
		"GAS_COMP_MSG",				//0x21
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",							//0x2f			
		"COORD_EVENT_MSG",			//0x30
		"GATEWAY_EVENT_MSG",		//0x31		
		"","","","","","","","","","","","","","",  //3f
		"","","","","","","","","","","","","","","","",  //4f
		"","","","","","","","","","","","","","","","",  //5f
		"","","","","","","","","","","","","","","","",  //6f
		"DL_ENTER_MSG","DL_PKT_MSG","DL_STOP_MSG","","","","","","","","","","","","","",  //7f
	};
	
	
	/* GATEWAY_EVENT_MSG  SUB DATA*/	
	public static int PHONE_BATT_LOW =  0x10;
	public static int PHONE_BATT_FULL = 0x11;
	public static int SPEECH_MIC_PATH = 0x20;
	public static int SPEECH_MIC_PATH_CANCEL = 0x21;
	public static int TTS_START_INFO = 0x22;
	public static int TTS_STOP_INFO = 0x23;
	public static int RINGING_ALERT = 0x30; //수신링
	public static int CALL_CONNECTED = 0x31; // 통화 연결
	public static int CALL_ENDED = 0x32; // 통화 종료
	public static int CODI_RESET = 0xf8; //본체 reset
	public static int CODI_SYSTEM_REBUILD = 0xfe; //시스템 초기화
	
	/*EMERGENCY_EVENT SUB DATA*/
	public static byte OCCURED_EMG = 0x10;
	public static byte CANCEL_EMG = 0x01;
}
