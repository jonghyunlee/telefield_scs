package com.telefield.smartcaresystem.message;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;

import android.R.integer;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Handler;
import android.os.Message;
import android.speech.tts.TextToSpeech;
import android.telephony.TelephonyManager;
import android.text.format.Time;
import android.widget.Toast;

import com.ftdi.j2xx.D2xxManager;
import com.telefield.smartcaresystem.Constant;
import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.database.ConnectDB;
import com.telefield.smartcaresystem.database.DBInfo;
import com.telefield.smartcaresystem.database.Sensor;
import com.telefield.smartcaresystem.database.SensorHistory;
import com.telefield.smartcaresystem.ftdi.Ft312DCtlService;
import com.telefield.smartcaresystem.ftdi.FtdiController;
import com.telefield.smartcaresystem.ftdi.MessageQueue;
import com.telefield.smartcaresystem.m2m.GatewayDevice;
import com.telefield.smartcaresystem.m2m.GatewayIndex;
import com.telefield.smartcaresystem.m2m.ProtocolConfig;
import com.telefield.smartcaresystem.m2m.ProtocolFrame;
import com.telefield.smartcaresystem.sensor.SensorDeviceManager;
import com.telefield.smartcaresystem.telephony.PhoneManager;
import com.telefield.smartcaresystem.ui.LockScreenActivity;
import com.telefield.smartcaresystem.util.ByteArrayToHex;
import com.telefield.smartcaresystem.util.LogMgr;
import com.telefield.smartcaresystem.util.SelfCall;
import com.telefield.smartcaresystem.util.SmartCarePreference;
import com.telefield.smartcaresystem.util.SpeechUtil;
import com.telefield.smartcaresystem.util.UtteranceID;


/** 
 * ftdi controller 제어
 * @author Administrator
 *
 */
public class MessageManager {
	public static final String TAG = "SmartCareSystem";
	
	final static int QUEUE_SIZE = 1024 * 10;  //10K
	final static int ACK_WAITING_TIME = 200;
	final static int RETRY_CNT = 0;
	
	Context context;	
	FtdiController ftdiController;
	Ft312DCtlService ft312dController;
	MessageQueue receiveMessageQueue;
	Timer timer, endCallTimer;	
	
	private boolean sendStatus = false ; // 0 not sending , 1 sending, ( ACK 수신 확인여부 체크. )
	private byte sendData[] = new byte[255];	
	private int sendDataLength = 0;
	private int sendCount = 0;
	
	private byte send_current_seq_num = 0;
	private byte rcv_current_seq_num = 0;
	
	
	private boolean ftdi_connect_status = false;
	private boolean connecting_req = false;
	
	private boolean IS_FT312 = true;
	
	private SensorHistory history;
	public MessageManager(Context context)
	{		
		this.context = context;	
		receiveMessageQueue = new MessageQueue(QUEUE_SIZE);
		timer = new Timer();
		
	}	
	
	/**
	 * must call after constructor
	 */	
	public void createFtdiController(boolean is_ft312)
	{
		this.IS_FT312 = is_ft312;
		
		if(IS_FT312)
		{
			ft312dController = new Ft312DCtlService(context, this, receiveMessageQueue);
		}else
		{
			ftdiController = new FtdiController(context, this, receiveMessageQueue);
		}	
	}
	
	/** 
	 * 아래 함수를 호출하기 이전에 createFtdiController 호출하여야함.
	 */
	public void ftdi_connect()
	{
		if(IS_FT312)
		{
			connecting_req = true;
			ft312dController.ResumeAccessory();			
			if(ft312dController.accessory_attached){
				sendMessage((SensorID.CODI_SENSOR_TYPE.getSensorType() << 8) | 0x01, (byte)MessageID.REG_MAC_REQ , null);
			}
			
		}else
		{
			int devCount = ftdiController.getDeviceList();
			if( devCount > 0)
			{
				ftdiController.setDeviceInfo(0, (byte)D2xxManager.FT_BITMODE_RESET, Constant.FTDI_BAUDRATE/*115200*/, (byte)D2xxManager.FT_DATA_BITS_8, (short)D2xxManager.FT_FLOW_NONE);				
				ftdi_connect_status = true;
				
				try {
					Thread.sleep(200); //ftdi 가 연결하여 read 할 수 있을때까지 시간을 주자.
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				sendMessage((SensorID.CODI_SENSOR_TYPE.getSensorType() << 8) | 0x01, (byte)MessageID.REG_MAC_REQ , null);
			}else
			{
				if( Constant.DEBUG_OPTION)	
					Toast.makeText(context, "ftdi not connect",Toast.LENGTH_SHORT).show();
				LogMgr.d(LogMgr.TAG,"ftdi not connect");
			}
		
		}
	}
	
	public void ftdi_disconnect()
	{		
		ftdi_connect_status = false;
		connecting_req = false;
		//if(IS_FT312)  //두개의 모드 다 끊어 버리자.
		{
			if( ft312dController != null)
				ft312dController.DestroyAccessory(true);
		}
		//else 
		{
			if( ftdiController != null)
				ftdiController.closeDevice();
		}
	}
	
	public boolean ftdi_isConnect()
	{
		if(IS_FT312){
			if( ft312dController == null ) return false;
			
			return ft312dController.isConnect();
		}
			
		else	
			return ftdi_connect_status;
	}
	public boolean ftdiController_isConnect(){
		if(ftdiController == null) 
			return false;
			
		return ftdiController.isConnected();
	}
	
	//test case 를 위해서 --[[
	public void setFtdiController(FtdiController controller)
	{
		this.ftdiController = controller;
	}
	
	public void setFt312DController(Ft312DCtlService controller)
	{
		this.ft312dController = controller;
	}	
	
	public void setEmulate()
	{
		ftdi_connect_status = true;
	}
	//--]]
	
	private void sendMessageStatusInit()
	{
		sendStatus = false;
		sendDataLength = 0;
		sendCount = 0;		
		Arrays.fill(sendData, (byte)0);
	}
	
	/**
	 * ftdi controller 에 메시지 전송
	 * ACK Message 용
	 * @param sensor_id
	 * @param msg_id
	 * @param sub_data
	 * @return
	 */
	
	public synchronized int sendMessage(int sensor_id, byte msg_id, byte sub_data[],byte seq_num)
	{
		int error = ErrorCode.SUCCESS;
		int length;		
		FormattedFrame ff = null;
		
//		LogMgr.d(LogMgr.TAG,"sendMessage = " + sensor_id + " msg_id : " + msg_id + "sendStatus: " + sendStatus);
	
			
		if( sub_data == null)
		{
			ff = (FormattedFrame)FormattedFrame.getInstance( 
					(byte)Constant.MSG_VERSION, FormattedFrame.LENGTH_MINIMUN_SIZE , seq_num, sensor_id, msg_id,
						sub_data);
		}else
		{		
			ff = (FormattedFrame)FormattedFrame.getInstance( 
					(byte)Constant.MSG_VERSION, FormattedFrame.LENGTH_MINIMUN_SIZE+sub_data.length , seq_num, sensor_id, msg_id,
						sub_data);
		}
		
		if( ff == null) {
			LogMgr.d(LogMgr.TAG,"FormattedFrame constructor fail");
			return ErrorCode.ERROR;
		}
		
		byte sendData[] = ff.getBytes();		
		
		error = sendMessage(sendData, sendData.length);
		if( error >= ErrorCode.SUCCESS){
			if( msg_id != MessageID.ACK_MSG) //ack 메시지의 경우 재전송을 하지 않는다.
			{
				sendStatus = true;
				sendDataLength = sendData.length;
			}
		}
		
		return error;
	}
	
	
	/**
	 * ftdi controller 에 메시지 전송
	 * 
	 * @param sensor_id
	 * @param msg_id
	 * @param sub_data
	 * @return
	 */
	
	public synchronized int sendMessage(int sensor_id, byte msg_id, byte sub_data[])
	{
		int error = ErrorCode.SUCCESS;
		int length;		
		FormattedFrame ff = null;
		
//		LogMgr.d(LogMgr.TAG,"sendMessage = " + sensor_id + " msg_id : " + msg_id + "sendStatus: " + sendStatus);
		
		//ACK 를 아직 받지 못한 상황
		//if( sendStatus == true) return ErrorCode.BUSY;
		
		if( sub_data == null)
		{
			ff = (FormattedFrame)FormattedFrame.getInstance( 
					(byte)Constant.MSG_VERSION, FormattedFrame.LENGTH_MINIMUN_SIZE , send_current_seq_num++, sensor_id, msg_id,
						sub_data);
		}else
		{		
			ff = (FormattedFrame)FormattedFrame.getInstance( 
					(byte)Constant.MSG_VERSION, FormattedFrame.LENGTH_MINIMUN_SIZE+sub_data.length , send_current_seq_num++, sensor_id, msg_id,
						sub_data);
		}
		
		if( ff == null) {
			LogMgr.d(LogMgr.TAG,"FormattedFrame constructor fail");
			return ErrorCode.ERROR;
		}
		
		byte sendData[] = ff.getBytes();		
		
		error = sendMessage(sendData, sendData.length);
		if( error >= ErrorCode.SUCCESS){
			if( msg_id != MessageID.ACK_MSG) //ack 메시지의 경우 재전송을 하지 않는다.
			{
				sendStatus = true;
				sendDataLength = sendData.length;
			}
		}
		
		return error;
	}
	
	public int sendMessage(byte data[] , int length)
	{				
		int error = ErrorCode.SUCCESS;	
		
		
		if( data == null)
		{
			return ErrorCode.EMPTY_DATA;
		}
		
//		sendStatus = true;
//		sendDataLength = length;
		
		System.arraycopy(data, 0, sendData, 0, length);
		
//		LogMgr.d(LogMgr.TAG, "MessageManager.java:sendMessage // ByteArrayToHex.byteArrayToHex(data) : " + ByteArrayToHex.byteArrayToHex(data,length) + "length : " + length);
		
		LogMgr.writeHistory(LogMgr.CODI_SEND, ByteArrayToHex.byteArrayToHex(data,length));
		
		if(IS_FT312)
		{
			if(ft312dController == null)
				return ErrorCode.ERROR;
			
			error = ft312dController.SendData(length, data);
		}		
		else error = ftdiController.sendMessage(data, length);
		
		if( sendCount < RETRY_CNT )
		{
			//ACK 를 수신하지 못할경우 3회에 걸쳐서 재발신을 진행한다.
			timer.schedule(new TimerTask() {			
				@Override
				public void run() {
					// TODO Auto-generated method stub
					sendCount++;
					sendMessage(sendData,sendDataLength);				
				}
			}, ACK_WAITING_TIME);	
		}
		else
		{
			//3회 발신하여도 ACK 를 수신하지 못할경우 그에 대한 처리를 진행하여야 한다.
			
		}
//		if( error > 0 ){
//			if( context != null)
//				Toast.makeText(context, "sendMessage success length : " + error , Toast.LENGTH_LONG).show();
//		}	
		
		return error;
	}	
	
	public synchronized int receiveMessage()
	{
		int error = ErrorCode.SUCCESS;
		
		//message queue 에서 메시지 를 가져온다.
		LinkedList messageList = receiveMessageQueue.messageCompose();
		if( messageList != null && messageList.size() > 0 )
		{
			for( int i=0; i < messageList.size(); i++)
			{
				FormattedFrame ff = (FormattedFrame)messageList.get(i);
				if( ff == null ) return ErrorCode.EMPTY_DATA; 
				
				//최초에 attach 된 데이터와 모드가 전환 되엇을때의 데이터는 duplication에서 재외한다.
				if(ff.getSeq_num() == 0x01){
					rcv_current_seq_num = (byte)0xff;
				}
				if( ff.getSeq_num() !=0 && rcv_current_seq_num == ff.getSeq_num() && ff.getSeq_num() != 0x01)
				{
					LogMgr.d(LogMgr.TAG, "duplicate msg received. ignore : " + ff.getSeq_num());
					LogMgr.d(LogMgr.TAG, "duplicate receive msg : " + ByteArrayToHex.byteArrayToHex(ff.getBytes(), ff.getBytes().length));
					return ErrorCode.DUPLICATE; 
				}				
				
				error = receiveMessage(ff);	
				
				LogMgr.writeHistory(LogMgr.CODI_RECEIVE, ByteArrayToHex.byteArrayToHex(ff.getBytes(),ff.getBytes().length)); 
				if(ff.getMsg_id() == MessageID.ACK_MSG){
					
				}
				else{
					rcv_current_seq_num = ff.getSeq_num();
				}
				LogMgr.d(LogMgr.TAG, "receive msg : " + ByteArrayToHex.byteArrayToHex(ff.getBytes(), ff.getBytes().length));
				if( Constant.DEBUG_OPTION){
//					Toast.makeText(context, "receive msg : " + ByteArrayToHex.byteArrayToHex(ff.getBytes(), ff.getBytes().length), Toast.LENGTH_SHORT).show();
				}
			}
		}else
		{
			error = ErrorCode.EMPTY_DATA;
		}
		
		return error;
	}
	
	/**
	 * 메시지를 수신하여 parsing 후 메시지 handle 부분	 
	 * @param frame
	 * @return
	 */
	public int receiveMessage(FormattedFrame frame)
	{
		
		if( SmartCareSystemApplication.getInstance().getDiagnosticManager().isDiagnosticTesting() )
		{
			ackResponse(frame);
			SmartCareSystemApplication.getInstance().getDiagnosticManager().receiveTestResult(frame);
			return ErrorCode.SUCCESS;
		}else if(SmartCareSystemApplication.getInstance().getCodiFirmwareUpgrade() != null)
		{			
			SmartCareSystemApplication.getInstance().getCodiFirmwareUpgrade().receiveMessage(frame);
			return ErrorCode.SUCCESS;
		}
		int error = ErrorCode.SUCCESS;
//		LogMgr.d(LogMgr.TAG,"Messagemanager receiveMessage frame.getMsg_id() : " + frame.getMsg_id() + "....frame.getSensor_id() : " +frame.getSensor_id());
		
		switch(frame.getMsg_id())
		{
		case MessageID.EMG_KEY_MSG:
			handleEmergencyKey(frame);
			break;
		case MessageID.ACK_MSG:
			handleAckMessage(frame);
			break;
		case MessageID.REG_INFO_MSG:
			handleRegistInfoMessage(frame);
			break;
		case MessageID.NODE_STAT_MSG:
			handleSensorStateMessage(frame);
			break;
		case MessageID.FIRE_DET_MSG:
			handleFireDetectMsg(frame);
			break;
		case MessageID.FIRE_CLR_MSG:
			break;
		case MessageID.GAS_DET_MSG:
			handleGasDetectMsg(frame);
			break;
		case MessageID.GAS_CLR_MSG:
			break;
		case MessageID.PIR_CNT_MSG:
			handlePirCntMsg(frame);
			break;
		case MessageID.NODE_INFO_REQ:
			break;
		case MessageID.LINK_TEST_MSG:
			handleSensorLinkTestMsg(frame);
			break;
		case MessageID.DOOR_STAT_MSG:
			handleDoorStateMessage(frame);
			break;
		case MessageID.COORD_EVENT_MSG:
			handleCoordEventMsg(frame);
			break;
		case MessageID.SENPWR_STAT_MSG:
			handleSensorPwrMsg(frame);
			break;
		case MessageID.PWR_STAT_MSG:
			handlePwrStateMsg(frame);
			break;
		case MessageID.PWR_CONT_REQ:
			handlePwrContReq(frame);
			break;
		case MessageID.GAS_COMP_MSG:
			handlGasCompMsgs(frame);
			break;
		default:
			LogMgr.d(TAG, "not support message");
			sendMessage(frame.getSensor_id(), (byte)MessageID.ACK_MSG, null);
			return ErrorCode.NOT_SUPPORT;			
		}	
		
		return error;		
		
	}
	
	
	
	private void ackResponse(FormattedFrame frame)
	{
		int sendStatus = 0;		
		LogMgr.d(LogMgr.TAG,"ackResponse");		
		//response data ACK 전송
		sendStatus = sendMessage(frame.getSensor_id(), (byte)MessageID.ACK_MSG, null , frame.getSeq_num());
		if(sendStatus < 0){
			if( Constant.DEBUG_OPTION){
//				Toast.makeText(context, "ACK send fail", Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	private void handlePirCntMsg(FormattedFrame frame)
	{
		ackResponse(frame);
		SmartCareSystemApplication.getInstance().getGlobalMsgFunc().handlePirCntMsg(frame);
	}
	
	private void handleFireDetectMsg(FormattedFrame frame)
	{
		ackResponse(frame);
		SmartCareSystemApplication.getInstance().getGlobalMsgFunc().handleFireDetectMsg(frame);
	}	
	
	private void handleGasDetectMsg(FormattedFrame frame)
	{
		ackResponse(frame);
		SmartCareSystemApplication.getInstance().getGlobalMsgFunc().handleGasDetectMsg(frame);
	}	
	
	private void handleSensorLinkTestMsg(FormattedFrame frame)
	{
		ackResponse(frame);
		SmartCareSystemApplication.getInstance().getGlobalMsgFunc().handleSensorLinkTestMsg(frame);
	}
	
	
	private void handleSensorStateMessage(FormattedFrame frame)
	{
		ackResponse(frame);
		
		SmartCareSystemApplication.getInstance().getGlobalMsgFunc().handleNodeInfoMsg(frame);		
		
	}
	
	private void handleDoorStateMessage(FormattedFrame frame)
	{
		ackResponse(frame);
		
		SmartCareSystemApplication.getInstance().getGlobalMsgFunc().handleDoorStateMsg(frame);
	}
	
	private void handleRegistInfoMessage(FormattedFrame frame) {
		// TODO Auto-generated method stub				
		LogMgr.d(LogMgr.TAG,"handleRegistInfoMessage");
		ackResponse(frame);		
		
		if( !SmartCareSystemApplication.getInstance().getSensorRegisterFunc().notify(frame) )
		{
			if(frame.getSensor_id() == SensorID.getSensorId(SensorID.CODI_SENSOR_TYPE.getSensorType(), 0x01)){
				LogMgr.d(LogMgr.TAG,"handleRegistInfoMessage 111");
				
				//코디도 sensor에 추가한다. GatewayDevice를 이용한다.
//				byte macAddress[] = Arrays.copyOf(frame.getSub_data(), frame.getSub_data().length - 1);
//				String macAddr = ByteArrayToHex.byteArrayToHex(macAddress, macAddress.length);
//				SmartCareSystemApplication.getInstance().getSensorDeviceManager().addCodi(frame.getSensor_id(), macAddr);
				
				byte[] subdata = frame.getSub_data();
				byte[] mac_address = new byte[ProtocolConfig.SIZE_MACADDRESS];
				String version = Sensor.getStringVersion(Integer.toHexString(frame.getVersion()));
				
				System.arraycopy(subdata, 0, mac_address, 0, mac_address.length);
				SmartCareSystemApplication.getInstance().getGlobalMsgFunc().putPreference(SmartCarePreference.USER_SETTING_INFO_FILENAME, SmartCarePreference.GATEWAY_MAC, ByteArrayToHex.byteArrayToHex(mac_address, mac_address.length));
				SmartCareSystemApplication.getInstance().getGlobalMsgFunc().putPreference(SmartCarePreference.USER_SETTING_INFO_FILENAME, SmartCarePreference.ZIGBEE_VER, version);
				GatewayDevice.getInstance().MAC = mac_address;
				
				String zigbee_version = version;
				zigbee_version = zigbee_version.replace(".", "");
				
				//지그비 버전, 스마트폰 버전정보 초기화
				GatewayDevice.getInstance().version[0] = (byte) Character.getNumericValue(zigbee_version.charAt(0));
				GatewayDevice.getInstance().version[1] = (byte) Character.getNumericValue(zigbee_version.charAt(1));
				GatewayDevice.getInstance().version[2] = 0;
			
				
				Context mContext = SmartCareSystemApplication.getInstance().getApplicationContext();
				PackageInfo pi = null;
				try {
					pi = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0);
				} catch (NameNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				String smartPhone_version = pi.versionName;
				smartPhone_version = smartPhone_version.replace(".", "");
				GatewayDevice.getInstance().smartphone_version[0] = (byte) Character.getNumericValue(smartPhone_version.charAt(0));
				GatewayDevice.getInstance().smartphone_version[1] = (byte) Character.getNumericValue(smartPhone_version.charAt(1));
				GatewayDevice.getInstance().smartphone_version[2] = (byte) Character.getNumericValue(smartPhone_version.charAt(2));
				
			}
		}
		
	}
	
	private void handleAckMessage(FormattedFrame frame)
	{
		LogMgr.d(LogMgr.TAG, "handleAckMessage");
		sendMessageStatusInit();
		
		//ack 메시지를 수신한 경우
	}
	
	/**
	 * MessageID.EMG_KEY_MSG
	 * emgergency key 를 코디로부터 수신	
	 * @param frame
	 */
	private void handleEmergencyKey(final FormattedFrame frame)
	{	
		ackResponse(frame);
		
		byte sub_data[] = frame.getSub_data();
		
		int flag = 0xff & sub_data[sub_data.length-1];
		
		final ConnectDB con = ConnectDB.getInstance(SmartCareSystemApplication.getInstance().getApplicationContext());
				
		if( flag == 0x10 || flag == 0x01 || flag == 0x20 || flag == 0x90 || flag == 0xA0 || flag== 0x81)
		{
			PhoneManager phoneMgr = SmartCareSystemApplication.getInstance().getPhoneManager();
			SelfCall selfCall = SmartCareSystemApplication.getInstance().getSelfCall();
			
			if( selfCall !=null && selfCall.isStartSelfCall() )
			{
				//수화기를 HookOff 했을때와 같은 효과이므로
				SmartCareSystemApplication.getInstance().getMySmartCareState().eventReceiveHookOff();
				if( flag != 0x10) return;				
			}else
			if( phoneMgr.getPhoneState() == TelephonyManager.CALL_STATE_RINGING ) {
				SmartCareSystemApplication.getInstance().getPhoneManager().answerCall();
				return;
			}else if( phoneMgr.getPhoneState() == TelephonyManager.CALL_STATE_OFFHOOK )
			{
//				if ( flag == 0x01 || flag == 0x10 )
//				{					
//					SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.SPEECH_CALL_END, TextToSpeech.QUEUE_ADD, null);
//					SmartCareSystemApplication.getInstance().getPhoneManager().endCall();
//					
//					if(flag ==0x01)
//					{
//						return; //취소버튼은 콜만 종료시키고 다른 동작은 안한다.
//					}
//				}
				if ( flag != 0x01 && flag != 0x10 && flag != 0x81)			
				{
					return; //버튼 무시
				}
			}
		}
		
		if(flag == 0x10) {			
			//응급 호출 버튼 누를 때 LCD On
			SmartCareSystemApplication.getInstance().getLCDMgr().setLCDOn();
			//USIM이 없는경우 전화를 하지 않는다.
			TelephonyManager tm = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
			if(tm.getSimState() == TelephonyManager.SIM_STATE_ABSENT)
				return;
			
			//AVAD_yhan 20141022 통화중 경보 발생시 통화 끊고 멘트 발송하도록 수정 --[[
			PhoneManager phoneMgr = SmartCareSystemApplication.getInstance().getPhoneManager();
			
			if( phoneMgr.getPhoneState() == TelephonyManager.CALL_STATE_OFFHOOK)
			{
				SmartCareSystemApplication.getInstance().getEmergencyCallUtil().endCallEmergency(Constant.EMERGENCY);
				LogMgr.d(LogMgr.TAG, "MessageManager phoneMgr.endCall();");
				endCallTimer = new Timer();
				endCallTimer.schedule(new TimerTask() {
					@Override
					public void run() {
						SmartCareSystemApplication.getInstance().getMySmartCareState().setAlert119(true);
						callEmergency(frame);					
					}
				}, 10 * 1000);	//멘트를 말하는 데 걸리는 시간이 약 5초 정도.
			}
			//--]]
			else{
				SmartCareSystemApplication.getInstance().getMySmartCareState().setAlert119(true);
				callEmergency(frame);
			}
			
			
		}
		else if( flag == 0x01 || flag == 0x81){	//현재 게이트웨이의 취소 버튼을 누르면 SensorID와 MessageID가 응급 호출과 같다(sub data가 다름)
			SensorHistory history = null;
			byte sensor_index = FormattedFrame.intToByteArray(frame.getSensor_id())[1];
			if(frame.getSensor_id() == SensorID.getSensorId(SensorID.CALL_EMERGENCY_SENSOR_TYPE.getSensorType(), sensor_index)){
				history = new SensorHistory((long)frame.getSensor_id(), SensorID.CALL_EMERGENCY_SENSOR_TYPE.getSensorType(), new Date(System.currentTimeMillis()), DBInfo.CODI_CANCEL);
			}
			else if(frame.getSensor_id() == SensorID.getSensorId(SensorID.CODI_SENSOR_TYPE.getSensorType(), 0x01)){
				history = new SensorHistory((long)frame.getSensor_id(), SensorID.CODI_SENSOR_TYPE.getSensorType(), new Date(System.currentTimeMillis()), DBInfo.CODI_CANCEL);
			}
			
			if(history != null)				
				con.insertSensorHistory(history);
			
			SmartCareSystemApplication.getInstance().getLCDMgr().setLCDOn();
			SmartCareSystemApplication.getInstance().getEmergencyCallUtil().actionStop(frame);
			
		}else if( flag == 0x20 ) //지역센터 버튼
		{
			
			//지역센터 버튼 누를 때 LCD On
			SmartCareSystemApplication.getInstance().getLCDMgr().setLCDOn();
			
			//USIM이 없는경우 전화를 하지 않는다.
			TelephonyManager tm = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
			if(tm.getSimState() == TelephonyManager.SIM_STATE_ABSENT)
				return;
			//SmartCareSystemApplication.getInstance().getSpeechUtil().stopSpeech();
			SmartCareSystemApplication.getInstance().getEmergencyCallUtil().CallCenter();		
			
		}
	}
	private void callEmergency(FormattedFrame frame){
		history = null;
		ConnectDB con = ConnectDB.getInstance(SmartCareSystemApplication.getInstance().getApplicationContext());
		if(frame.getSensor_id() == SensorID.getSensorId(SensorID.CALL_EMERGENCY_SENSOR_TYPE.getSensorType(), 0x01)){
			history = new SensorHistory((long)frame.getSensor_id(), SensorID.CALL_EMERGENCY_SENSOR_TYPE.getSensorType(), new Date(System.currentTimeMillis()), DBInfo.CODI_EMERGENCY);
		}
		else if(frame.getSensor_id() == SensorID.getSensorId(SensorID.CODI_SENSOR_TYPE.getSensorType(), 0x01)){
			history = new SensorHistory((long)frame.getSensor_id(), SensorID.CODI_SENSOR_TYPE.getSensorType(), new Date(System.currentTimeMillis()), DBInfo.CODI_EMERGENCY);
		}
		
		if(history != null)
			con.insertSensorHistory(history);
		
		// 본체에서 응급호출이 발생하면 응급호출기로 전달을 안하고,응급호출기에서 응급상황 발생하면 응급 호출기로 응급 상황 발생 메시지 전달
		if(49153/*C001 본체*/ != frame.getSensor_id()){
			SmartCareSystemApplication.getInstance().getGlobalMsgFunc().sendEmgAlarmMsg(frame);
		}
		//등록이 되지 않은 응급호출기는 응급호출을 발생시키지 않는다.
		if(SensorID.CALL_EMERGENCY_SENSOR_TYPE.getSensorType() == SensorID.getSensorType(frame.getSensor_id())){
			SensorDeviceManager mgr = SmartCareSystemApplication.getInstance()
					.getSensorDeviceManager();
			if (!mgr.isRegisteredSensor(frame.getSensor_id())) {
				return;		
			}
		}
		SmartCareSystemApplication.getInstance().getEmergencyCallUtil().Call119(frame, Constant.EMERGENCY);
	}
	public boolean isFT312DMode()
	{
		return IS_FT312;
	}
	
	private void handleCoordEventMsg(FormattedFrame frame)
	{
		ackResponse(frame);
		SmartCareSystemApplication.getInstance().getGlobalMsgFunc().handleCoordEventMsg(frame);
	}
	
	private void handleSensorPwrMsg(FormattedFrame frame)
	{
		ackResponse(frame);
		SmartCareSystemApplication.getInstance().getGlobalMsgFunc().handleSensorPwrMsg(frame);
	}	
	
	private void handlePwrStateMsg(FormattedFrame frame)
	{
		ackResponse(frame);
		SmartCareSystemApplication.getInstance().getGlobalMsgFunc().handlePwrStateMsg(frame);
	}
	
	private void handlePwrContReq(FormattedFrame frame)
	{
		ackResponse(frame);
		SmartCareSystemApplication.getInstance().getGlobalMsgFunc().handlePwrContReq(frame);
	}
	
	private void handlGasCompMsgs(FormattedFrame frame){
		ackResponse(frame);
		SmartCareSystemApplication.getInstance().getGlobalMsgFunc().handlGasCompMsgs(frame);
	}
}
