package com.telefield.smartcaresystem.message;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

import com.telefield.smartcaresystem.util.ByteArrayToHex;
import com.telefield.smartcaresystem.util.CheckSum;
import com.telefield.smartcaresystem.util.LogMgr;


/***********************************************************************************************************************
 * Frame Name 				Index			Field Name				Size			type		code		contents
 * Header					0				Start					1Byte			Hex			0x02		Frame 시작 
 * 							1				Version					1Byte			Hex 
 *  						2				Length					2Byte			Hex
 *  						4				Sequence Num			1Byte			Hex
 * Body						5				Sensor_Id				2Byte			
 * 							6				MSG_ID					1Byte									Msg Index	
 * 							7				SUB_DATA				Nbyte 
 * 							7+n				CheckSum				1Byte
 * Tail						7+n+1			End						1Byte			Hex			0x03		Frame종료				
 * **********************************************************************************************************************
 * 
 * 하나의 Frame 을 표현한다.
 *  
 * @author Administrator
 *
 **********************************************************************************************************************/
public class FormattedFrame {
	public static final String TAG = "SmartCareSystem";
	
	public static int FRAME_MINIMUM_SIZE = 10;
	public static int LENGTH_MINIMUN_SIZE = 5;  //최소 length값. sub_data가 포함이 되지않음.
	
	byte start;
	byte version;
	int length;
	byte seq_num;
	
	int sensor_id;
	byte msg_id;
	byte sub_data[];
	byte check_sum;
	
	byte end;	
	
	private FormattedFrame()
	{
		
	}
	
	private FormattedFrame(byte data[], int size)
	{
		
		
		version = data[FormattedFrameIndex.VERSION];
		length = (int)( 0xff & data[FormattedFrameIndex.LENGTH+1] );
		length += ( ((int)0xff & data[FormattedFrameIndex.LENGTH]) << 8 );
		seq_num = data[FormattedFrameIndex.SEQ_NUM];		
		sensor_id = (int)( 0xff & data[FormattedFrameIndex.SENSOR_ID+1] );
		sensor_id += ( ((int)0xff & data[FormattedFrameIndex.SENSOR_ID]) << 8 );		
		msg_id = data[FormattedFrameIndex.MSG_ID];
		
		if( size == FRAME_MINIMUM_SIZE ) //sub data non-exist;
		{
			sub_data = null;
		}else
			sub_data = Arrays.copyOfRange(data, FormattedFrameIndex.SUB_DATA, size-2);
		
		check_sum = data[data.length -1 -1];		
	}
	
	/**
	 * message 를 발신할때
	 * @param version
	 * @param length
	 * @param seq_num
	 * @param sensor_id
	 * @param msg_id
	 * @param sub_data
	 * @return
	 */
	public static FormattedFrame getInstance( byte version, int length, byte seq_num, int sensor_id, byte msg_id,
			byte sub_data[])
	{
//		LogMgr.d(LogMgr.TAG, "FormattedFrame.java:getInstance // version : " + version + "  // length : " + length + "  // sensor_id : " + sensor_id + " // msg_id : " + msg_id );
		FormattedFrame frame = new FormattedFrame();		
		
		frame.start = FormattedFrameIndex.START_DATA;
		frame.end = FormattedFrameIndex.END_DATA;		
		frame.version = version;		
		frame.length = length;		
		frame.seq_num = seq_num;
		frame.sensor_id = sensor_id;
		frame.msg_id = msg_id;
		
		if( sub_data != null)
		{			
			frame.sub_data = Arrays.copyOf(sub_data, sub_data.length);
		}else
			frame.sub_data = null;
		
		frame.check_sum =0;		
		byte data[] = frame.getBytes();		
		
//		LogMgr.d(LogMgr.TAG,"FormattedFrame getInstance :" + ByteArrayToHex.byteArrayToHex(data,data.length));
		frame.check_sum = CheckSum.getValue(Arrays.copyOfRange(data,FormattedFrameIndex.SENSOR_ID, data.length-1-1), data.length-7); //to do
		
		return frame;		
	}
	
	/**
	 * 데이타를 수신하였을때 parsing 부분
	 * @param data
	 * @param size
	 * @return FormattedFrame
	 *  
	 */
	public static FormattedFrame getInstance(byte data[], int size)
	{	
		if( data.length != size)
		{
			LogMgr.d(TAG,"frame data size error.");
			return null;
		}
		
		if( !isValidFrame(data) || !isValidCheckSum(data)) 
		{
			LogMgr.d(TAG,"frame data is invalid.");
			return null;
		}
		
		return new FormattedFrame(data,size);
	}
	
	/**
	 * 전송 완료 message
	 * @param msg_id
	 * @return FormattedFrame
	 *  
	 */
	public static FormattedFrame getInstance(byte msg_id)
	{
		FormattedFrame frame = new FormattedFrame();
		frame.msg_id = msg_id;
		return frame;
	}
	
	/**
	 * 이벤트보고 (게이트웨이 이벤트) 119, Center 호출 전송 message
	 * @param msg_id
	 * @param sub_data
	 * @return
	 */
	public static FormattedFrame getInstance(byte msg_id, byte[] sub_data)
	{
		FormattedFrame frame = new FormattedFrame();
		frame.msg_id = msg_id;
		frame.sub_data = sub_data;
		return frame;
	}
	
	/**
	 * start/end bit check
	 * @param data
	 * @return
	 */
	public static boolean isValidFrame(byte data[])
	{
		if( data == null || data[0] != FormattedFrameIndex.START_DATA  
				||data[data.length-1]!= FormattedFrameIndex.END_DATA ) {
			LogMgr.d(TAG,"frame data is error. check start data and end data.");
			return false;		
		}
		
		
		return true;
	}
	
	/**
	 * check the checksum
	 * to do checksum error implementation
	 */
	
	public static boolean isValidCheckSum(byte data[])
	{
		// to do
		
//		LogMgr.d(LogMgr.TAG,ByteArrayToHex.byteArrayToHex(data,data.length));
		
		byte check_sum = CheckSum.getValue(Arrays.copyOfRange(data,FormattedFrameIndex.SENSOR_ID, data.length-1-1), data.length-7);
		byte comp_data = data[data.length-1-1];
		
		if( check_sum != comp_data)
		{
			LogMgr.d(TAG,"isValidCheckSum check_sum : "  + check_sum + "  real_data : " + comp_data);			
			return false;
		}
		
		return true;
	}
	
	public byte getVersion() {
		return version;
	}


	public int getLength() {
		return length;
	}


	public byte getSeq_num() {
		return seq_num;
	}


	public int getSensor_id() {
		return sensor_id;
	}


	public byte getMsg_id() {
		return msg_id;
	}
	public void setMsg_id(byte msg_id) {
		this.msg_id = msg_id;
	}

	public byte[] getSub_data() {
		return sub_data;
	}
	public void setSub_data(byte[] sub_data){
		this.sub_data = sub_data;
	}
	
	public byte[] getBytes()
	{
		int size;		
		byte data[];
		
		if( sub_data == null)
		{
			size = FRAME_MINIMUM_SIZE;
		}else
			size = FRAME_MINIMUM_SIZE + sub_data.length;
		
		data = new byte[size];		
		
		data[FormattedFrameIndex.START] = FormattedFrameIndex.START_DATA;		
		data[FormattedFrameIndex.VERSION] = version;
		data[FormattedFrameIndex.LENGTH+1] = (byte)(length & 0xff);
		data[FormattedFrameIndex.LENGTH] = (byte)((length & 0xff00)  >> 8);
		data[FormattedFrameIndex.SEQ_NUM] = seq_num;		
		data[FormattedFrameIndex.SENSOR_ID+1] = (byte)(sensor_id & 0xff);
		data[FormattedFrameIndex.SENSOR_ID] = (byte)((sensor_id & 0xff00)  >> 8);
		data[FormattedFrameIndex.MSG_ID] = msg_id;
		
		if( sub_data != null)
		{
			for( int i=0; i < sub_data.length; i++)
			{
				data[FormattedFrameIndex.SUB_DATA + i] = sub_data[i];
			}
		}
		data[data.length -1 -1] = check_sum;
		data[data.length -1] = FormattedFrameIndex.END_DATA;
		
		return data;
	}
	
	public static byte[] intToByteArray(int integer) {
		ByteBuffer buff = ByteBuffer.allocate(Integer.SIZE / 16);
		buff.putShort((short)integer);
		buff.order(ByteOrder.BIG_ENDIAN);
		//buff.order(ByteOrder.LITTLE_ENDIAN);
		return buff.array();
	}
	public static byte[] intToFourByteArray(int integer) {
		ByteBuffer buff = ByteBuffer.allocate(Integer.SIZE / 4);
		buff.putInt((short)integer);
		buff.order(ByteOrder.BIG_ENDIAN);
		//buff.order(ByteOrder.LITTLE_ENDIAN);
		return buff.array();
	} 
	
	public static int byteArrayToInt(byte[] bytes) {
		final int size = Integer.SIZE / 8;
		ByteBuffer buff = ByteBuffer.allocate(size);
		final byte[] newBytes = new byte[size];
		for (int i = 0; i < size; i++) {
			if (i + bytes.length < size) {
				newBytes[i] = (byte) 0x00;
			} else {
				newBytes[i] = bytes[i + bytes.length - size];
			}
		}
		buff = ByteBuffer.wrap(newBytes);
		buff.order(ByteOrder.BIG_ENDIAN); // Endian에 맞게 세팅
		return buff.getInt();
	}
}
