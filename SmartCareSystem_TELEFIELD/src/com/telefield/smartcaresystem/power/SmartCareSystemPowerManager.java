package com.telefield.smartcaresystem.power;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.util.Log;

import com.telefield.smartcaresystem.Constant;
import com.telefield.smartcaresystem.SmartCareSystemApplication;
import com.telefield.smartcaresystem.message.MessageID;
import com.telefield.smartcaresystem.ui.UIConstant;
import com.telefield.smartcaresystem.util.LogMgr;
import com.telefield.smartcaresystem.util.SmartCarePreference;

public class SmartCareSystemPowerManager {

	public static final int LOW_MINIMUM_LEVEL = 10;
	public static final int LOW_BATT_LEVEL = 30; // 기본값 30
	public static final int BATT_GAP = 5;

	public static final int START_WAIT_TIME = 5 * 60 * 1000;

	private Context mContext;

	private int batt_level;
	private int batt_status; // BatteryManager.BATTERY_STATUS_CHARGING ,
								// BatteryManager.BATTERY_STATUS_DISCHARGING ,
								// BatteryManager.BATTERY_STATUS_FULL ,
								// BatteryManager.BATTERY_STATUS_NOT_CHARGING

	private int curr_pwr_status; // 0 : low 1 : normal 2 : full
	// private int receive_full_event;

	private boolean isElapsed5Minute = false;

	/*
	 * Accessory Mode 에서 Host 모드로 이동 가능 여부 체크 변수 Accessory Mode에서 Batt Full
	 * 상태이더라도 설정된 시간 간격후에 (default 7일 ) Host모드로 이동 가능하도록 하는 변수 false 로 변경시점은
	 * Host 모드로 바뀌면 false 로 바뀐다. true 는 Batt 가 Full 일경우에 한다.
	 */
	private boolean isCanModeChange = false;

	public SmartCareSystemPowerManager(Context context) {
		this.mContext = context;

		// 앱이 시작하여 5분후에 Full , Low event 를 전달하도록 한다.
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				isElapsed5Minute = true;
			}
		}, START_WAIT_TIME);

		initBroadcastReceiver();
		// receive_full_event = 0;

	}

	BroadcastReceiver batt_receiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {

			String action = intent.getAction();

			// LogMgr.d(LogMgr.TAG,"[SmartCareSystemPowerManager] action : " +
			// intent);

			ActivityManager am = (ActivityManager) context
					.getSystemService(Context.ACTIVITY_SERVICE);
			List<ActivityManager.RunningTaskInfo> info = am.getRunningTasks(1);
			ComponentName topComponentName = null;

			if (info != null) {
				topComponentName = info.get(0).topActivity;
			}

			if (action.equals(Intent.ACTION_BATTERY_CHANGED)) {
				int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
				int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, 100);

				batt_level = (level * 100) / scale;
				if(batt_level <= 10)
					LogMgr.d(LogMgr.TAG, "Warnning LowBatt !!!!!!! batt_level : " + batt_level);
				if (!isElapsed5Minute)
					return;

				// LogMgr.d(LogMgr.TAG,"[SmartCareSystemPowerManager] batt_level : "
				// + batt_level);

				if (batt_level <= LOW_BATT_LEVEL) {
					if (curr_pwr_status != 0) {
						if (batt_level < LOW_MINIMUM_LEVEL) {
							// 충전 모드로 가자... FT312D ASA 모드
							if (!SmartCareSystemApplication.getInstance()
									.getMessageManager().isFT312DMode()) {
								SmartCareSystemApplication
										.getInstance()
										.getGlobalMsgFunc()
										.sendGateWayMsg(
												(byte) MessageID.PHONE_BATT_LOW);
							}
							LogMgr.d(LogMgr.TAG,
									"[SmartCareSystemPowerManager] batt_status : LOW1 ");
							curr_pwr_status = 0;
						}
						// 현재 상태가 LauncherUI 이거나 Lockscreen 이면 전달
						else if (topComponentName
								.getClassName()
								.equals("com.telefield.smartcaresystem.ui.LockScreenActivity")
								|| topComponentName
										.getClassName()
										.equals("com.telefield.smartcaresystem.ui.LauncherUI")) {
							// 충전 모드로 가자... FT312D ASA 모드
							if (!SmartCareSystemApplication.getInstance()
									.getMessageManager().isFT312DMode()) {
								SmartCareSystemApplication
										.getInstance()
										.getGlobalMsgFunc()
										.sendGateWayMsg(
												(byte) MessageID.PHONE_BATT_LOW);
							}
							LogMgr.d(LogMgr.TAG,
									"[SmartCareSystemPowerManager] batt_status : LOW2 ");
							curr_pwr_status = 0;
							// 10%이하일때 베터리를 충전상태로 돌리라고 지속적으로 요청한다.
						}
					//Low batt 상태이고 batt_level이 10 이하이고 방전모드이면 충전모드로 전환한다.(1분마다 보냄)
					//Jong 14.11.07 	
					}else if (curr_pwr_status == 0
							&& batt_level < LOW_MINIMUM_LEVEL) {
						if (!SmartCareSystemApplication.getInstance()
								.getMessageManager().isFT312DMode()) {
							SmartCareSystemApplication.getInstance().getGlobalMsgFunc()
									.sendGateWayMsg((byte) MessageID.PHONE_BATT_LOW);
							LogMgr.d(LogMgr.TAG,"[SmartCareSystemPowerManager] batt_status : LOW3 ");
						}
						
					} 
				} else {
					if (curr_pwr_status == 0) // 이전 low batt 였다면
					{
						if (batt_level >= (BATT_GAP + LOW_BATT_LEVEL)) {
							curr_pwr_status = 1;
						}
					} else if (curr_pwr_status == 2) // 이전 Full 이었다면
					{
						if (batt_level <= (100 - BATT_GAP)) {
							curr_pwr_status = 1;
						}
					}
				}

				batt_status = intent
						.getIntExtra(BatteryManager.EXTRA_STATUS, 0);

				if (batt_status == BatteryManager.BATTERY_STATUS_CHARGING) {
					// LogMgr.d(LogMgr.TAG,"[SmartCareSystemPowerManager] batt_status : 충전중 "
					// );
				} else if (batt_status == BatteryManager.BATTERY_STATUS_DISCHARGING) {
					// LogMgr.d(LogMgr.TAG,"[SmartCareSystemPowerManager] batt_status : 방전중 "
					// );
				} else if (batt_status == BatteryManager.BATTERY_STATUS_NOT_CHARGING) {
					// LogMgr.d(LogMgr.TAG,"[SmartCareSystemPowerManager] batt_status : BATTERY_STATUS_NOTCHARGING "
					// );
				} else if (batt_status == BatteryManager.BATTERY_STATUS_FULL) {
					if (!isCanModeChange) {
//						LogMgr.d(LogMgr.TAG,
//								"[SmartCareSystemPowerManager] batt_status : FULL");

						PendingIntent checkSender = PendingIntent.getBroadcast(
								context, 0, new Intent(
										Constant.BATT_FULL_ACTION),
								PendingIntent.FLAG_NO_CREATE);
						if (checkSender != null)
							return;
						if (!SmartCareSystemApplication.getInstance()
								.getMessageManager().isFT312DMode())
							return; // host 모드에서 Full 은 무시

						LogMgr.d(LogMgr.TAG,
								"BATT FULL. isCanModeChange setting true");

						AlarmManager alarm = (AlarmManager) context
								.getSystemService(Context.ALARM_SERVICE);
						PendingIntent sender = PendingIntent.getBroadcast(
								context, 0, new Intent(
										Constant.BATT_FULL_ACTION),
								PendingIntent.FLAG_UPDATE_CURRENT);

						SmartCarePreference pref = new SmartCarePreference(
								context,
								SmartCarePreference.APPLICATION_SETTING_INFO_FILENAME);
						int date = pref
								.getIntValue(
										SmartCarePreference.CHARGE_DISCHARGE_PERIOD_DAY,
										UIConstant.DEFAULT_CHARGE_DISCHARGE_PERIOD_DAY);
						int time = pref
								.getIntValue(
										SmartCarePreference.CHARGE_DISCHARGE_PERFORM_TIME,
										UIConstant.DEFAULT_CHARGE_DISCHARGE_PERFORM_TIME);

						if (date <= 0)
							date = 1;

						Calendar calendar = Calendar.getInstance();
						calendar.setTimeInMillis(System.currentTimeMillis());
						calendar.add(Calendar.DATE, date);
						calendar.set(Calendar.HOUR_OF_DAY, time);
						calendar.set(Calendar.MINUTE, 0);
						calendar.set(Calendar.SECOND, 0);
						calendar.set(Calendar.MILLISECOND, 0);

						alarm.set(AlarmManager.RTC_WAKEUP,
								calendar.getTimeInMillis(), sender);
						LogMgr.d(LogMgr.TAG,
								"[SmartCareSystemPowerManager] set Alarm...");
						return;
					}

					LogMgr.d(LogMgr.TAG,
							"[SmartCareSystemPowerManager] batt_status : FULL1  current pwr staus : "
									+ curr_pwr_status);
					// receive_full_event++;

					if (curr_pwr_status != 2 /* || receive_full_event > 5 */) {
						// 현재 상태가 LauncherUI 이거나 Lockscreen 이면 전달
						if (topComponentName
								.getClassName()
								.equals("com.telefield.smartcaresystem.ui.LockScreenActivity")
								|| topComponentName
										.getClassName()
										.equals("com.telefield.smartcaresystem.ui.LauncherUI")) {
							// 방전 모드로 가자... FT234X 모드
							if (SmartCareSystemApplication.getInstance().getMessageManager().isFT312DMode())
							{
								SmartCareSystemApplication.getInstance().getGlobalMsgFunc().sendGateWayMsg((byte)MessageID.PHONE_BATT_FULL);
							}
							LogMgr.d(LogMgr.TAG,"[SmartCareSystemPowerManager] batt_status : FULL2 " );
							curr_pwr_status = 2;
							//receive_full_event = 0;
						}
					}
				}
			}
		}
	};
	
	private void initBroadcastReceiver()
	{
		IntentFilter filter = new IntentFilter();
		filter.addAction(Intent.ACTION_BATTERY_CHANGED);
		filter.addAction(Intent.ACTION_BATTERY_LOW);
		filter.addAction(Intent.ACTION_POWER_CONNECTED);
		filter.addAction(Intent.ACTION_POWER_DISCONNECTED);
		mContext.registerReceiver(batt_receiver, filter);
	}
	
	
	public int getBatt_level() 
	{
		return batt_level;
	}	
	
	public int getBatt_status() 
	{
		return batt_status;
	}
	
	public void setCanModeChange(boolean isCan)
	{
		this.isCanModeChange = isCan;
	}
	
}
