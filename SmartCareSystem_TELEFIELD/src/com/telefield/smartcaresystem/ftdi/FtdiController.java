package com.telefield.smartcaresystem.ftdi;

import java.util.Arrays;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

import com.ftdi.j2xx.D2xxManager;
import com.ftdi.j2xx.D2xxManager.D2xxException;
import com.ftdi.j2xx.FT_Device;
import com.telefield.smartcaresystem.Constant;
import com.telefield.smartcaresystem.message.MessageManager;
import com.telefield.smartcaresystem.util.ByteArrayToHex;
import com.telefield.smartcaresystem.util.LogMgr;

public class FtdiController {
	
	//const data
	

	Context context;
	
	public static D2xxManager ftd2xx = null;
	public FT_Device ftDev = null;
	int devCount;
	
	public static final int MAX_READ_LENGTH = 1024;
	int iavailable = 0; 	//버퍼 Queue
	char[] readDataToText = new char[1024];	//read 버퍼
	byte[] readData = new byte[1024];
	StringBuffer readText = new StringBuffer();
	
	Thread readThread;	// 리더 쓰레드
	
	boolean bReadThreadGoing = false;
	
	MessageQueue receiveMessageQueue;
	MessageManager messageProcessHandler;		
	
	//event handler	
	private static final int FTDI_CONNECTED = 0;
	private static final int FTDI_DISCONNECTED = 1;
	private static final int FTDI_RECEIVE_MESSAGE = 2;
	

	public FtdiController( Context context , MessageManager handler, MessageQueue queue) {
		this.context = context;			
		
		messageProcessHandler = handler;
		receiveMessageQueue = queue;		
	}
	
	/**
	 * 현재 연결되어있는 장비의 갯수를 가져온다.
	 */
	public int getDeviceList() {
		
		try {
			ftd2xx = D2xxManager.getInstance(context);
			devCount = ftd2xx.createDeviceInfoList(context);
			
		} catch (D2xxException e) {
			e.printStackTrace();
		}
		
		return devCount;
	}
	
	/**
	 * getDeviceList를 통해 얻은 장비 갯수에서 얻은 장비 index를 기준으로 장비 정보를 설정한다.
	 * index 는 devCount 보다 작아야만 한다. 
	 * @param index
	 */
	public void setDeviceInfo( int index, byte bitMode, int baudRate, byte dataCharacteristics, short flowControl ) {
		
		if(true == bReadThreadGoing)
		{
			bReadThreadGoing = false;
			try 
			{
				Thread.sleep(50);
			}
			catch (InterruptedException e) {e.printStackTrace();}
		}
		
		if( index < devCount ) {
			
			try {
				ftDev = ftd2xx.openByIndex( context, index);
				
				if( ftDev.isOpen() ) {
					ftDev.setBitMode( (byte)0, bitMode );
					ftDev.setBaudRate( baudRate );
					ftDev.setDataCharacteristics( dataCharacteristics, D2xxManager.FT_STOP_BITS_1, D2xxManager.FT_PARITY_NONE );
					ftDev.setFlowControl( flowControl, (byte)0x0b, (byte)0x0d );
					
					/*리더 및 커넥션 체크 쓰레드 생성*/
					if(false == bReadThreadGoing) {
						
						bReadThreadGoing = true;
						
						readThread = new ReadThread(handler);
						readThread.start();
						
						Thread conCheckThread = new Thread( new CheckConnection(handler) );
						conCheckThread.start();
						
					}
				}
				
				Toast.makeText(context, "장비 통신 값 설정완료. ", Toast.LENGTH_SHORT ).show();
				
			} catch ( Exception e ) {
				if( Constant.DEBUG_OPTION)
					Toast.makeText(context, "장비 통신 값 설정 실패!!! ", Toast.LENGTH_SHORT ).show();
				e.printStackTrace();
				if(ftDev!=null){
					ftDev.close();
					ftDev = null;
				}
					
			}
			
			
		} else {
			if( Constant.DEBUG_OPTION)
				Toast.makeText(context, "연결된 장비 갯수보다 index 값이 큽니다. ", Toast.LENGTH_SHORT ).show();
		}
		
	}
	
	/**
	 * 강제 연결 종료
	 */
	public void closeDevice() {
		bReadThreadGoing = false;
		if(ftDev!= null)
		{
			if( true == ftDev.isOpen())
			{
				ftDev.close();
			}
		}
		ftDev = null;
	}
	
	/**
	 * 연결 상태값 반환
	 * @return
	 */
	public boolean isConnected() {
		return bReadThreadGoing;
	}

	
	/**
	 * 연결된 장비로 메세지 보내기
	 * @param data
	 * @param length
	 * @return
	 */
	public int sendMessage( byte data[] , int length)
	{		
		int write_len = 0;
		
		if( ftDev == null) return -1;
		
		
		if (ftDev.isOpen() == false) {
			LogMgr.d("j2xx", "SendMessage: device not open");
		}

		//ftDev.setLatencyTimer((byte) 16);		
		
		if( length > 0 )
			write_len = ftDev.write(data, length );
		
		return write_len;
	}
	
	public int sendMessage (byte data)
	{
		int write_len = 1;
		
		if( ftDev == null) return -1;
		
		if (ftDev.isOpen() == false) {
			LogMgr.d("j2xx", "SendMessage: device not open");
		}
		
		byte tmpBuf[] = new byte[1];
		tmpBuf[0] = data;	

		//ftDev.setLatencyTimer((byte) 16);		
		write_len = ftDev.write(tmpBuf, 1 );
		
		return write_len;
	}
	
	/**
	 * 현재 버퍼 비움. 버퍼 리턴된 값을 다 읽고 나면 버퍼를 비워야 함.
	 */
	public void readBufferClear() {
		readText.delete(0, readText.length() );
	}
	
	/**
	 * 현재 버퍼에 리턴된 값 반환
	 * @return
	 */
	public StringBuffer readBufferGet() {
		
		return readText;
	}
	
	/**
	 * 쓰레드 메세지 처리 핸들러
	 */
	final Handler handler =  new Handler() {
    	
		@Override
    	public void handleMessage(Message msg) {
    		
			if( msg.what == FTDI_RECEIVE_MESSAGE ) {				
				//message queue에서 받는다.
				messageProcessHandler.receiveMessage();				

			}
			
			if( msg.what == FTDI_DISCONNECTED ) {
				Toast.makeText( context, "연결된 장비가 끊어졌습니다. ", Toast.LENGTH_SHORT ).show();
				bReadThreadGoing = false;
				closeDevice();
			}
			
    	}
    };
    
    /**
     * TX 버퍼 리더 쓰레드
     * @author Topper
     *
     */
	private class ReadThread  extends Thread {
		Handler mHandler;		

		public ReadThread(Handler h){
			mHandler = h;
			this.setPriority(Thread.MIN_PRIORITY);
		}

		@Override
		public void run() {
			int i;

			while(true == bReadThreadGoing) {
				
				//synchronized(ftDev) 
				{
					int readLen;
					
					//AVAD_yhan 20140624 nullpointer exception 방어코드
					if( ftDev == null) {
						bReadThreadGoing = false;
						break;
					}					
					
					iavailable = ftDev.getQueueStatus();				
					
					if (iavailable > 0) {
						
						if(iavailable > MAX_READ_LENGTH){
							iavailable = MAX_READ_LENGTH;
						}
						
						Arrays.fill(readData, (byte)0x0);
						
						readLen = ftDev.read(readData, iavailable);
						
//						LogMgr.d(LogMgr.TAG, "ReadData : " + ByteArrayToHex.byteArrayToHex(readData, readLen) + "Length: " + readLen);
						
						if( readLen > 0 )
						{
							receiveMessageQueue.insert(readData,readLen);					
						
							Message msg = mHandler.obtainMessage();
							msg.what = FTDI_RECEIVE_MESSAGE;
							mHandler.sendMessage(msg);
						}
						
					}else{
						try {
							Thread.sleep(200);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
				}
				
			}
		}

	}
	
	/**
	 * 장비 커넥션 상태 체크 쓰레드
	 * @author Topper
	 *
	 */
	class CheckConnection implements Runnable {
		
		Handler mHandler;
		
		int connect_status = FTDI_DISCONNECTED;
		
		public CheckConnection(Handler handler) {
			this.mHandler = handler;
		}

		@Override
		public void run() {
			
			while( bReadThreadGoing ) {
				
				int deviceListCnt = getDeviceList();			
				
				if( deviceListCnt == 0 ) {
					
					connect_status = FTDI_DISCONNECTED;
					Message msg = mHandler.obtainMessage();
					msg.what = FTDI_DISCONNECTED;
					mHandler.sendMessage(msg);
					
					//Toast.makeText(context, "FTDI DISCONNECT", Toast.LENGTH_SHORT).show();					
					LogMgr.d("ftdi","FTDI DISCONNECT");
					
					//AVAD_yhan 201406 handler 로 빠르게 전달이 안되는 경향이 있어서.. --[[					
					bReadThreadGoing = false;
					closeDevice();
					//--]]
					
				}
				else if( deviceListCnt > 0)
				{
					if( connect_status == FTDI_DISCONNECTED )
					{
						connect_status = FTDI_CONNECTED;
						Message msg = mHandler.obtainMessage();
						msg.what = FTDI_CONNECTED;
						mHandler.sendMessage(msg);
						
						//Toast.makeText(context, "FTDI CONNECTED", Toast.LENGTH_SHORT).show();
					}
				}				
				
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}				
			}			
		}		
	}	
}
