package com.telefield.smartcaresystem.ftdi;

import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbAccessory;
import android.hardware.usb.UsbManager;
import android.os.Handler;
import android.os.Message;
import android.os.ParcelFileDescriptor;
import android.widget.Toast;

import com.telefield.smartcaresystem.Constant;
import com.telefield.smartcaresystem.message.MessageManager;
import com.telefield.smartcaresystem.util.LogMgr;

public class Ft312DCtlService{
	
	private static final String ACTION_USB_PERMISSION =    "com.telefield.ftdi.USB_PERMISSION";
	public static final int MAX_READ_LENGTH = 1024;
	//
	private Context mContext;		
	private UsbManager usbmanager;
	private UsbAccessory usbaccessory;
	private PendingIntent mPermissionIntent;
	private ParcelFileDescriptor filedescriptor = null;
	private FileInputStream inputstream = null;
	private FileOutputStream outputstream = null;
	private boolean mPermissionRequestPending = false;
	private read_thread readThread;
	
	private byte [] usbdata; 
	private byte []	writeusbdata;	
	
	MessageQueue receiveMessageQueue;
	MessageManager messageProcessHandler;		
	
	
	final int  maxnumbytes = 65536;
	
	public boolean datareceived = false;
	public boolean READ_ENABLE = false;
	public boolean accessory_attached = false;
	private boolean registered = false;
	
	public static String ManufacturerString = "mManufacturer=FTDI";
	public static String ModelString1 = "mModel=FTDIUARTDemo";
	public static String ModelString2 = "mModel=Android Accessory FT312D";
	public static String VersionString = "mVersion=1.0";
	
	//event handler	
	private static final int FTDI_CONNECTED = 0;
	private static final int FTDI_DISCONNECTED = 1;
	private static final int FTDI_RECEIVE_MESSAGE = 2;
	
	public Ft312DCtlService( Context context , MessageManager handler, MessageQueue queue) {
		this.mContext = context;			
		
		messageProcessHandler = handler;
		receiveMessageQueue = queue;	
		
		writeusbdata = new byte[256];
		usbdata = new byte[MAX_READ_LENGTH]; 
		
		usbmanager = (UsbManager)context.getSystemService(Context.USB_SERVICE);		
		
		mPermissionIntent = PendingIntent.getBroadcast(context, 0, new Intent(ACTION_USB_PERMISSION), 0);
		IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
		filter.addAction(UsbManager.ACTION_USB_ACCESSORY_DETACHED);
		context.registerReceiver(mUsbReceiver, filter);
		registered = true;
		
		LogMgr.d(LogMgr.TAG,"Ft312DCtlService register");
		
		inputstream = null;
		outputstream = null;
	}
	
	/**
	 * 쓰레드 메세지 처리 핸들러
	 */
	final Handler handler =  new Handler() {
    	
		@Override
    	public void handleMessage(Message msg) {
    		
			if( msg.what == FTDI_RECEIVE_MESSAGE ) {				
				//message queue에서 받는다.
				messageProcessHandler.receiveMessage();		

			}
    	}
    };
	
	public void SetConfig(int baud, byte dataBits, byte stopBits,
			byte parity, byte flowControl) throws IOException
	{

		/*prepare the baud rate buffer*/
		writeusbdata[0] = (byte)baud;
		writeusbdata[1] = (byte)(baud >> 8);
		writeusbdata[2] = (byte)(baud >> 16);
		writeusbdata[3] = (byte)(baud >> 24);

		/*data bits*/
		writeusbdata[4] = dataBits;
		/*stop bits*/
		writeusbdata[5] = stopBits;
		/*parity*/
		writeusbdata[6] = parity;
		/*flow control*/
		writeusbdata[7] = flowControl;

		/*send the UART configuration packet*/
		SendPacket((int)8);
	}
	
	
	/*write data*/ 
	public byte SendData(int numBytes, byte[] buffer) 					     
	{
		byte status;
		
		status = 0x00; /*success by default*/
		/*
		 * if num bytes are more than maximum limit
		 */
		if(numBytes < 1){
			/*return the status with the error in the command*/
			return status;
		}
	 		
		/*check for maximum limit*/
		if(numBytes > 256){
			numBytes = 256;
		}

		/*prepare the packet to be sent*/
		for(int count = 0;count<numBytes;count++)
		{	
			writeusbdata[count] = buffer[count];
		}
		
		try
		{

			if(numBytes != 64)
			{
				SendPacket(numBytes);
			}
			else
			{
				byte temp = writeusbdata[63];
				SendPacket(63);
				writeusbdata[0] = temp;
				SendPacket(1);
			}
		}catch (IOException e)
		{
			LogMgr.d(LogMgr.TAG,"Exception e : " + e);
		}

		return status;
	}
	
		
	/*method to send on USB*/
	private void SendPacket(int numBytes) throws IOException
	{	
		if(outputstream != null){
				outputstream.write(writeusbdata, 0,numBytes);
		}		
	}
	
	
	/***********USB broadcast receiver*******************************************/
	private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() 
	{
		@Override
		public void onReceive(Context context, Intent intent) 
		{
			String action = intent.getAction();
			
			LogMgr.d(LogMgr.TAG, "BroadcastReceiver receive" + action);
			if (ACTION_USB_PERMISSION.equals(action)) 
			{
				synchronized (this)
				{
					UsbAccessory accessory = (UsbAccessory) intent.getParcelableExtra(UsbManager.EXTRA_ACCESSORY);
					if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false))
					{
						if( Constant.DEBUG_OPTION)
							Toast.makeText(context, "Allow USB Permission", Toast.LENGTH_SHORT).show();
						OpenAccessory(accessory);
					} 
					else 
					{
						if( Constant.DEBUG_OPTION)
							Toast.makeText(context, "Deny USB Permission", Toast.LENGTH_SHORT).show();						

					}
					mPermissionRequestPending = false;
				}
			} 
			
			else if (UsbManager.ACTION_USB_ACCESSORY_DETACHED.equals(action)) 
			{				
				DestroyAccessory(true);
				//CloseAccessory();
			}
			
			else
			{
				
			}
		}	
	};
	
	
	/*resume accessory*/
	public int ResumeAccessory()
	{
		// Intent intent = getIntent();
		if (inputstream != null && outputstream != null) {
			return 1;
		}

		UsbAccessory[] accessories = usbmanager.getAccessoryList();
		if(accessories != null)
		{
			Toast.makeText(mContext, "Accessory Attached", Toast.LENGTH_SHORT).show();
		}		
		else
		{
			// return 2 for accessory detached case
			//Log.e(">>@@","ResumeAccessory RETURN 2 (accessories == null)");
			accessory_attached = false;
			return 2;
		}

		UsbAccessory accessory = (accessories == null ? null : accessories[0]);
		if (accessory != null) {
			if( -1 == accessory.toString().indexOf(ManufacturerString))
			{
				if( Constant.DEBUG_OPTION)
					Toast.makeText(mContext, "Manufacturer is not matched!", Toast.LENGTH_SHORT).show();
				return 1;
			}

			if( -1 == accessory.toString().indexOf(ModelString1) && -1 == accessory.toString().indexOf(ModelString2))
			{
				if( Constant.DEBUG_OPTION)
					Toast.makeText(mContext, "Model is not matched!", Toast.LENGTH_SHORT).show();
				return 1;
			}

			if( -1 == accessory.toString().indexOf(VersionString))
			{
				if( Constant.DEBUG_OPTION)	
					Toast.makeText(mContext, "Version is not matched!", Toast.LENGTH_SHORT).show();
				return 1;
			}
			
			if( Constant.DEBUG_OPTION)
				Toast.makeText(mContext, "Manufacturer, Model & Version are matched!", Toast.LENGTH_SHORT).show();
			LogMgr.d(LogMgr.TAG,"Manufacturer, Model & Version are matched!");
			accessory_attached = true;

			if (usbmanager.hasPermission(accessory)) {
				LogMgr.d(LogMgr.TAG,"openAccessory");
				OpenAccessory(accessory);
			} 
			else
			{
				synchronized (mUsbReceiver) {
					LogMgr.d(LogMgr.TAG,"Request USB Permission::" +mPermissionRequestPending);
					if (!mPermissionRequestPending) {
						if( Constant.DEBUG_OPTION)
							Toast.makeText(mContext, "Request USB Permission", Toast.LENGTH_SHORT).show();
						usbmanager.requestPermission(accessory,
								mPermissionIntent);
						mPermissionRequestPending = true;
					}
				}
			}
		} else {}

		return 0;
	}
	
	/*destroy accessory*/
	public void DestroyAccessory(boolean bConfiged){
		
		Toast.makeText(mContext, "DestroyAccessory", Toast.LENGTH_SHORT).show();

		if(true == bConfiged){
			READ_ENABLE = false;  // set false condition for handler_thread to exit waiting data loop
			writeusbdata[0] = 0;  // send dummy data for instream.read going
			try {
				SendPacket(1);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else
		{
			try {
				SetConfig(Constant.FTDI_BAUDRATE/*9600*/,(byte)1,(byte)8,(byte)0,(byte)0);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}  // send default setting data for config
			try{Thread.sleep(10);}
			catch(Exception e){}

			READ_ENABLE = false;  // set false condition for handler_thread to exit waiting data loop
			writeusbdata[0] = 0;  // send dummy data for instream.read going
			try {
				SendPacket(1);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(true == accessory_attached)
			{
				//saveDefaultPreference();
			}
		}

		try{Thread.sleep(10);}
		catch(Exception e){}			
		CloseAccessory();
	}

	/*********************helper routines*************************************************/		

	public void OpenAccessory(UsbAccessory accessory)
	{	
		filedescriptor = usbmanager.openAccessory(accessory);
		LogMgr.d(LogMgr.TAG,"OpenAccessory 1 :: " + accessory);
		if(filedescriptor != null){
			usbaccessory = accessory;

			FileDescriptor fd = filedescriptor.getFileDescriptor();
			LogMgr.d(LogMgr.TAG,"OpenAccessory 2");
			if( fd != null)
			{
				LogMgr.d(LogMgr.TAG,"OpenAccessory 3");
				inputstream = new FileInputStream(fd);
				outputstream = new FileOutputStream(fd);
				/*check if any of them are null*/
				if(inputstream == null || outputstream==null){
					return;
				}
				
				LogMgr.d(LogMgr.TAG,"OpenAccessory 4 ::" + READ_ENABLE);
	
				if(READ_ENABLE == false){
					
					try {
						//SetConfig(115200, (byte)8, (byte)1, (byte)0, (byte)0);
						SetConfig(Constant.FTDI_BAUDRATE, (byte)8, (byte)1, (byte)0, (byte)0);
					} catch (IOException e) {						
						e.printStackTrace();
						return;
					}
					
					if( Constant.DEBUG_OPTION)
						Toast.makeText(mContext, "connect read start", Toast.LENGTH_SHORT).show();
					LogMgr.d(LogMgr.TAG,"connect read start");
					
					READ_ENABLE = true;					
					readThread = new read_thread(inputstream,handler);
					readThread.start();
				}
			}else
			{
				LogMgr.d(LogMgr.TAG,"OpenAccessory failed. FileDescriptor is null");
			}
		}
	}
	
	private void CloseAccessory()
	{
		try{
			if(filedescriptor != null)
				filedescriptor.close();

		}catch (IOException e){}

		try {
			if(inputstream != null)
				inputstream.close();
		} catch(IOException e){}

		try {
			if(outputstream != null)
				outputstream.close();

		}catch(IOException e){}
		/*FIXME, add the notfication also to close the application*/

		filedescriptor = null;
		inputstream = null;
		outputstream = null;
		
		if( registered)
		{
			mContext.unregisterReceiver(mUsbReceiver);
			registered = false;
		}
		
		//System.exit(0);		
		LogMgr.d(LogMgr.TAG,"Ft312DCtlService deregister");		
	}
	
	
	/*usb input data handler*/
	private class read_thread  extends Thread 
	{
		FileInputStream instream;
		Handler mHandler;

		read_thread(FileInputStream stream ,Handler mHandler){
			this.instream = stream;
			this.mHandler = mHandler;
			this.setPriority(Thread.MAX_PRIORITY);
		}

		public void run()
		{
			while(READ_ENABLE == true)
			{				
//				LogMgr.d(LogMgr.TAG,"reading");
				try
				{
					if(instream != null)
					{
						//int available = instream.available();
						//if( available > 0) 
						{
							int readcount;
							
							//if( available > MAX_READ_LENGTH)
							//	available = MAX_READ_LENGTH;
							
							readcount = instream.read(usbdata,0,MAX_READ_LENGTH/*available*/);
														
							if(readcount > 0)
							{
//								LogMgr.d(LogMgr.TAG, "ReadData : " + ByteArrayToHex.byteArrayToHex(usbdata, readcount) + "Length: " + readcount);
								
								receiveMessageQueue.insert(usbdata,readcount);
								
								Message msg = mHandler.obtainMessage();
								msg.what = FTDI_RECEIVE_MESSAGE;
								mHandler.sendMessage(msg);
								
							}else{
								LogMgr.d(LogMgr.TAG,"wait data~~~~~");
							}
						}
//						else{
//							Thread.sleep(500);
//						}
					}
				}				
				catch (IOException e){
					READ_ENABLE = false;
					StringWriter sw = new StringWriter();
					e.printStackTrace(new PrintWriter(sw));
					String stackTrace = sw.toString();
					LogMgr.d(LogMgr.TAG,"exception ::" + stackTrace);
					e.printStackTrace();
					
					break;
				}
				
//				catch (InterruptedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
			}		
			
			LogMgr.d(LogMgr.TAG,"read data exit");
		}
		
		
	}
	
	public boolean isConnect()
	{
		return READ_ENABLE;
	}
	

}
