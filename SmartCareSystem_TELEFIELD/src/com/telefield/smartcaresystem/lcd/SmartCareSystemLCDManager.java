package com.telefield.smartcaresystem.lcd;

import android.content.Context;
import android.hardware.input.InputManager;
import android.os.PowerManager;
import android.view.Window;
import android.view.WindowManager;

public class SmartCareSystemLCDManager {
	private Context context;
	private Window window;
	private static PowerManager.WakeLock wakeLock;
	public SmartCareSystemLCDManager(Context context) {
		this.context = context;
	}
	
	public Window getWindow() {
		return window;
	}
	
	public void setWindow(Window window) {
		this.window = window;
	}
	
	public void setLCDOn() {
		if(window != null) {
			PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
			PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "SmartCareSystemLCDManager");
			wl.acquire();
			window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
			wl.release();
		}
	}
	public void setLCDAlwaysOn() {
		if(window != null) {
			if(wakeLock != null) return;
			PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
			wakeLock = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP |                
	                PowerManager.ON_AFTER_RELEASE, "SmartCareSystemLCDManager");
			wakeLock.acquire();
			window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
		}
		
	}
	public void setLCDOff() {
		if(wakeLock != null){
			wakeLock.release();
			wakeLock = null;
		}
	}
	
}
