package com.telefield.smartcaresystem;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.joda.time.DateTime;
import org.joda.time.Days;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;

import com.telefield.smartcaresystem.database.ConnectDB;
import com.telefield.smartcaresystem.database.NotReported;
import com.telefield.smartcaresystem.database.Sensor;
import com.telefield.smartcaresystem.firmware.UpgradeController;
import com.telefield.smartcaresystem.http.AsyncCallback;
import com.telefield.smartcaresystem.http.AsyncFileDownloader;
import com.telefield.smartcaresystem.http.HttpRequestHelper;
import com.telefield.smartcaresystem.http.HttpUtils;
import com.telefield.smartcaresystem.m2m.GateWayMessage;
import com.telefield.smartcaresystem.m2m.GatewayIndex;
import com.telefield.smartcaresystem.m2m.ProtocolFrame;
import com.telefield.smartcaresystem.m2m.ProtocolManger;
import com.telefield.smartcaresystem.message.FormattedFrame;
import com.telefield.smartcaresystem.message.MessageID;
import com.telefield.smartcaresystem.ui.LockScreenActivity;
import com.telefield.smartcaresystem.ui.UIConstant;
import com.telefield.smartcaresystem.ui.firmware.AppdownloadUI;
import com.telefield.smartcaresystem.util.LogMgr;
import com.telefield.smartcaresystem.util.SmartCarePreference;

public class GCMIntentHandler{
	Context context;
	private static Handler handler;
	final static String TAG = "GCM";
	public static GCMIntentHandler gcmIntentHandler = null;
	
	private static final String ALARM_OFF ="ALARM_OFF";
	private static final String VALVE_CLOSE = "VALVE_CLOSE";
	
	private static final String PASSWORD_TYPE_MAIN = "main";
	private static final String PASSWORD_TYPE_SYSTEM = "system";
	
	private static final String change_image_url = "change_image_url";
	private static final String days = "days";
	public static GCMIntentHandler getInstance(){
		if(gcmIntentHandler == null){
			gcmIntentHandler = new GCMIntentHandler();
		}
		return gcmIntentHandler;
	}
	
	public GCMIntentHandler() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * 원격 지그비 펌웨어 다운로드 및 업데이트
	 */
	public void handleZigbeeFirmwareUpdate(){
		
		Context mContext = SmartCareSystemApplication.getInstance().getApplicationContext();
		SmartCarePreference pref = new SmartCarePreference(mContext, SmartCarePreference.USER_SETTING_INFO_FILENAME);
		String version = pref.getStringValue(SmartCarePreference.ZIGBEE_VER, UIConstant.ZIGBEE_DEFAULT_VER);
		//http://222.122.128.45:8080/firmware/validate/zigbee/version/update/2.0.0/JN_5168
		String url = "http://222.122.128.45:8080/firmware/validate/zigbee/version/update";
		String codi_version = version;
		String chip_id ="JN_5168";
		String req_url = url + "/" + codi_version + "/" + chip_id;			
		
		LogMgr.d(TAG, "GCMIntentHandler handleZigbeeFirmwareUpdate start...req_url : " +req_url + "...version : " + codi_version);
		String tel_number = SmartCareSystemApplication.getInstance().getMyPhoneNumber();
		String content = HttpRequestHelper.getInstance().urlrequest(req_url);
		if( content == null || content.isEmpty() || !content.equals("true"))
		{
			LogMgr.d(LogMgr.TAG,"Codi firmware's current veresion is latest");
			return;
		}
		File file = new File(Constant.FIRMWARE_PATH);
		if(!file.exists())
			file.mkdirs();
		new AsyncFileDownloader(context.getApplicationContext()).download(Constant.ZIGBEE_FW_DOWNLOAD_PATH + "/" + tel_number,Constant.FW_GATEWAY_PATH, new AsyncCallback.Base<File>() {
			@Override
			public void onResult(File result) {
				LogMgr.d("APP_UPDATE","APP_UPDATE_DOWNLOAD_END");

				LogMgr.d(LogMgr.TAG, "GCMIntentService ZIGBEE_FIRMWARE_UPDATE result.getAbsolutePath() : " + result.getAbsolutePath());
				LogMgr.d(LogMgr.TAG, "GCMIntentService ZIGBEE_FIRMWARE_UPDATE result.length() : " + result.length());
				
				if( result!=null && Constant.FW_GATEWAY_PATH.equals(result.getAbsolutePath()))
				{			
					//원격으로 업데이트를 했을경우 안내 음성이 나오면 안된다.
//					SmartCareSystemApplication.getInstance().getSpeechUtil().speech(SpeechUtil.START_CODI_FIRMWARE_UPDATE, TextToSpeech.QUEUE_FLUSH, null);
					
					LogMgr.d(LogMgr.TAG,"UpgradeController upgrade start");
					//펌웨어 업데이트 진행.
					UpgradeController.getInstance().upgrade(UpgradeController.CODI_FIRMWARE_UPDATE_TYPE);
					
				}
			}
		});
		LogMgr.d(TAG, "GCMIntentHandler handleZigbeeFirmwareUpdate end...");

	}
	
	/**
	 * 원격 게이트웨이 펌웨어 다운로드 및 업데이트
	 */
	public void handleGatewayFirmwareUpdate(){
		Context mContext = SmartCareSystemApplication.getInstance().getApplicationContext();
		PackageInfo pi = null;
		try {
			pi = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0);
		} catch (NameNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String version = pi.versionName;
		
		//http://222.122.128.45:8080/firmware/validate/gateway/version/update/1.0.2			
		String url = "http://222.122.128.45:8080/firmware/validate/gateway/version/update";
		String apk_version = version;			
		String req_url = url + "/" + apk_version;			
		LogMgr.d(TAG, "GCMIntentHandler handleGatewayFirmwareUpdate start...req_url : " + req_url + "...apk_version : " + apk_version);

		String content = HttpRequestHelper.getInstance().urlrequest(req_url);
					
		if( content == null || content.isEmpty() || !content.equals("true"))
		{
			LogMgr.d(LogMgr.TAG,"Android firmware's current veresion is latest");
			return;
		}
		mContext = SmartCareSystemApplication.getInstance().getApplicationContext();
		Intent intent = new Intent(context, AppdownloadUI.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.setAction("remoteUpdate");
		context.startActivity(intent);
		LogMgr.d(TAG, "GCMIntentHandler handleGatewayFirmwareUpdate end..");
	}
	
	/**
	 * 원격 리셋
	 */
	public void handleRemoteReset(){
		boolean isFtdiConnect = SmartCareSystemApplication.getInstance().getMessageManager().ftdi_isConnect();
		LogMgr.d(TAG, "GCMIntentHandler handleRemoteReset start..isFtdiConnect : " + isFtdiConnect);
		if (isFtdiConnect) {
			SmartCareSystemApplication.getInstance().getGlobalMsgFunc().sendGateWayMsg((byte) MessageID.CODI_RESET);
		}
		LogMgr.d(TAG, "GCMIntentHandler handleRemoteReset end...");
	}
	
	/**
	 * 원격 개통
	 */
	public void handleRemoteOpen(){
		LogMgr.d(TAG, "GCMIntentHandler handleRemoteOpen start...");
		try {
			
			ProtocolManger pManger = SmartCareSystemApplication.getInstance().getProtocolManger();
			
			//pManger.setEventRegistration(new EventRegistration(event));
			pManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REQUEST_TIME));
			pManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REGISTER_GW));

			List<Sensor> sensorList = SmartCareSystemApplication.getInstance().getSensorDeviceManager().getSensorList();
			for (Sensor sensor : sensorList) {
				pManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REGISTER_SENSOR, sensor, null));
			}

			new GlobalMessageHandler().sendDeviceInfo();
			
			pManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_GW_DATA, FormattedFrame.getInstance(GateWayMessage.ID_REPORT_FINISH_SEND)));
			pManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REQUEST_DATA));
			pManger.messageSend(null);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		LogMgr.d(TAG, "GCMIntentHandler handleRemoteOpen end...");
	}
	
	/**
	 * 원격 로그파일 요청
	 * @param intent 로그파일 요청정보
	 */
	public void handleLogRequest(Intent intent){
		final DateTime start_date = intent.getStringExtra("start_date") == null ? DateTime.parse("2014-01-01") : DateTime.parse(intent.getStringExtra("start_date"));
		final DateTime end_date = intent.getStringExtra("end_date") == null ? DateTime.parse("2014-01-01") : DateTime.parse(intent.getStringExtra("end_date"));
		final int days_between = Days.daysBetween(start_date, end_date).getDays();
		
		LogMgr.d(TAG,"GCMIntentHandler handleLogRequest start...start_date : " + start_date + "...end_date : " + end_date + "...days_between : " + days_between);
		for(int i=0; i<=days_between; i++) {
			try {
				
				final DateTime find_log_date = start_date.plusDays(i);
				final DateTime today = DateTime.parse(DateTime.now().toString().split("T")[0]);
				final String log_file_path = find_log_date.isEqual(today.getMillis()) ? Constant.LOG_PATH+"/smartcaresystem.txt" : 
						Constant.LOG_PATH+"/smartcaresystem.txt."+find_log_date.toString().split("T")[0];
				final File log_file = new File(log_file_path);
				
				if(log_file.isFile()) {            				
					final HttpEntity multipart = MultipartEntityBuilder.create()
        					.addBinaryBody("file",log_file)
        					.addTextBody("call_num",SmartCareSystemApplication.getInstance().getMyPhoneNumber()).build();
        				
        			HttpUtils.INSTANCE.httpUrlPOSTRequest(Constant.LOG_FILE_UPLOAD_PATH, multipart);
				}            				
			} catch(Exception e) {
				LogMgr.d("LOG_REQUEST_ERR",e.getMessage());
			}
		}
		LogMgr.d(TAG,"GCMIntentHandler handleLogRequest end...");
	}
	
	/**
	 * 원격 미보고 데이터 전송
	 */
	public void handleNotReported(){
		ConnectDB db = ConnectDB.getInstance(SmartCareSystemApplication.getInstance().getApplicationContext());
		ProtocolManger pManger = SmartCareSystemApplication.getInstance().getProtocolManger();
		List<NotReported> list = db.selectAllNotReported();
		db.deleteAllNotReported();
		LogMgr.d(TAG,"GCMIntentHandler handleNotReported start...list.size() : " + list.size());
		for(NotReported nrData : list){
			//payload 값만 저장하기때문에 payload값만 가져옴
			byte[] notReportedData = nrData.getContent();
			pManger.insertFrame(ProtocolFrame.getInstance(notReportedData));	
		}
		pManger.messageSend(null);
		LogMgr.d(TAG, "GCMIntentHandler handleNotReported end...");
	}
	
	/**
	 * 원격 셋팅(가스벨브 차단, 알람 취소..)
	 * @param intent
	 */
	public void handleRemoteSetting(Intent intent){
		LogMgr.d(TAG,"GCMIntentHandler handleRemoteSetting start... key : " + intent.getStringExtra("change_type"));
		String change_type = intent.getStringExtra("change_type");//ALARM_OFF VALVE_CLOSE
		//가스벨브 차단 열림
		//알람 취소
		switch (change_type) {
		case ALARM_OFF:
			SmartCareSystemApplication.getInstance().getEmergencyCallUtil().actionStop();
			break;
		case VALVE_CLOSE:
			SmartCareSystemApplication.getInstance().getGlobalMsgFunc().sendGasBreakMsg();
			break;
		default:
			break;
		}
		LogMgr.d(TAG, "GCMIntentHandler handleRemoteSetting end...");
	}
	
	/**
	 * 원격 비밀번호 변경
	 * @param intent
	 */
	public void handlePasswordChange(Intent intent){
		String password_type = intent.getStringExtra("password_type");
		String change_password = intent.getStringExtra("change_password");		
		LogMgr.d(TAG, "GCMIntentHandler handlePasswordChange start...password_type : " + password_type + "...change_password : " + change_password);
		switch (password_type) {
		case PASSWORD_TYPE_MAIN:
			UIConstant.MAIN_NUM = change_password;
			break;

		case PASSWORD_TYPE_SYSTEM:
			UIConstant.INIT_PASSWORD_NUM = change_password;
			UIConstant.INIT_SETTING_PASSWORD = change_password;
			break;
		default:
			break;
		}
		LogMgr.d(TAG, "GCMIntentHandler handlePasswordChange end...");
	}
	
	/**
	 * lockscreenImage 다운로드 및 적용..
	 */
	public void handleLockscreenImg(Intent intent){
		String url = intent.getStringExtra(change_image_url);
		String days = intent.getStringExtra(GCMIntentHandler.days);
		final int durationDays;
		if(days == null || days.equals("0"))
			durationDays = 30;
		else
			durationDays = Integer.parseInt(days);
		
		String filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/SmartCareSystem/image";
		File file = new File(filePath);		
		if(!file.exists()){
			file.mkdirs();
		}
		
		LogMgr.d(TAG, "GCMIntentHandler handleLockscreenImg start...filePath : " + filePath + "...file.exists : " + file.exists() + "...url : " + url);
		new AsyncFileDownloader(context.getApplicationContext()).download(url,Constant.LOCKSCREEN_IMAGE_PATH, new AsyncCallback.Base<File>() {
			@Override
			public void onResult(File result) {
				LogMgr.d(TAG,"LOCKSCREEN IMAGE DOWNLOAD COMPLETE");
				String resultUrl = "http://222.122.128.45:8080/admin/menu/image/change/state/";
				String tel_number = SmartCareSystemApplication.getInstance().getMyPhoneNumber();
				
				StringBuffer sb = new StringBuffer(resultUrl);
				sb.append(tel_number);
				if(handler != null && result != null){
					handler.sendMessage(Message.obtain(handler, LockScreenActivity.UPDATE_CENTERVIEW));
					sb.append("/complete");
					LogMgr.d(TAG,"LOCKSCREEN IMAGE APPLY COMPLETE");
					
				}else{
					sb.append("/fail");
					LogMgr.d(TAG,"LOCKSCREEN IMAGE APPLY FAIL REASON : handler or imagefile is null....");
				}
				HttpRequestHelper.getInstance().urlrequest(new String(sb));
				resetLockscreenTimer(durationDays);
			}
			
			@Override
			public void exceptionOccured(Exception e) {
				LogMgr.d(TAG,"LOCKSCREEN IMAGE APPLY FAIL REASON : " + e);
				super.exceptionOccured(e);
			}
		});
		LogMgr.d(TAG, "GCMIntentHandler handleLockscreenImg end...");
	}

	private void resetLockscreenTimer(int durationDays){
		AlarmManager alaram = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
		PendingIntent pIntent = PendingIntent.getBroadcast(context, 0 , new Intent(Constant.LOCKSCREEN_DELETE_ACTION), PendingIntent.FLAG_CANCEL_CURRENT);
		alaram.set(AlarmManager.RTC_WAKEUP, DateTime.now().plusDays(durationDays).getMillis(), pIntent);
	}
	
	/**
	 * 원격 DB파일 전송
	 */
	public void handleSendDB(){
		handleCoptyDB();
		
		String number = SmartCareSystemApplication.getInstance().getMyPhoneNumber();
		String fileName = number + "_android_database_backup.db";
		final String UPLOAD_URL = "http://222.122.128.45:8080/admin/menu/dbbackup/ajax/save";
		String filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/SmartCareSystem/"+fileName;
		
		LogMgr.d(TAG, "GCMIntentHandler handleSendDB start... number : " + number + "...fileName : " + fileName);
		File database = new File(filePath);
		final HttpEntity entity = MultipartEntityBuilder.create()
			.setMode(HttpMultipartMode.BROWSER_COMPATIBLE)
			.addTextBody("call_num",number)
			.addTextBody("db_file_name", fileName)
			.addBinaryBody("db_file", database)
			.build();
		
		try {
			HttpUtils.INSTANCE.httpUrlPOSTRequest(UPLOAD_URL, entity);
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if(database.exists())
			database.delete();
		LogMgr.d(TAG, "GCMIntentHandler handleSendDB end...");
	}
	
	/**
	 * DB파일 생성
	 */
	private void handleCoptyDB(){
		SQLiteDatabase db = context.openOrCreateDatabase("telefield.db", Context.MODE_PRIVATE, null);
		File dbfile = new File(db.getPath());
		File makefile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/SmartCareSystem/");
		String number = SmartCareSystemApplication.getInstance().getMyPhoneNumber();
		String filename = "/" + number + "_android_database_backup.db";
		if(!makefile.exists())
			makefile.mkdirs();
		LogMgr.d(TAG, "GCMIntentHandler handleCoptyDB start... filePath : " + makefile.getAbsolutePath());
		try {
			FileInputStream fis = new FileInputStream(dbfile);
			FileOutputStream fos = new FileOutputStream(makefile.getAbsolutePath() + filename);
			
			long filesize = fis.available();
			byte[] temp = new byte[(int) filesize];
			
			fis.read(temp);
			fis.close();
			
			fos.write(temp);
			fos.flush();
			
			fos.close();
			db.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		LogMgr.d(TAG, "GCMIntentHandler handleCoptyDB end...");
	}
	
	public void setContext(Context context){
		this.context = context;
	}
	
	public static void setHandler(Handler handler){
		GCMIntentHandler.handler = handler;
	}

}
