package com.telefield.smartcaresystem;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Timer;
import java.util.TimerTask;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Handler;
import android.os.Message;
import android.speech.tts.TextToSpeech;
import android.telephony.TelephonyManager;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.telefield.smartcaresystem.bluetooth.DeviceScan;
import com.telefield.smartcaresystem.database.ConnectDB;
import com.telefield.smartcaresystem.database.DBInfo;
import com.telefield.smartcaresystem.database.Sensor;
import com.telefield.smartcaresystem.database.SensorHistory;
import com.telefield.smartcaresystem.database.Users;
import com.telefield.smartcaresystem.firmware.UpgradeController;
import com.telefield.smartcaresystem.m2m.EventRegistration;
import com.telefield.smartcaresystem.m2m.GateWayMessage;
import com.telefield.smartcaresystem.m2m.GatewayDevice;
import com.telefield.smartcaresystem.m2m.GatewayIndex;
import com.telefield.smartcaresystem.m2m.M2MReportUtil;
import com.telefield.smartcaresystem.m2m.ProtocolConfig;
import com.telefield.smartcaresystem.m2m.ProtocolFrame;
import com.telefield.smartcaresystem.m2m.ProtocolManger;
import com.telefield.smartcaresystem.message.FormattedFrame;
import com.telefield.smartcaresystem.message.MessageID;
import com.telefield.smartcaresystem.message.MessageManager;
import com.telefield.smartcaresystem.message.SensorID;
import com.telefield.smartcaresystem.power.SmartCareSystemPowerManager;
import com.telefield.smartcaresystem.sensor.SensorDeviceManager;
import com.telefield.smartcaresystem.telephony.PhoneManager;
import com.telefield.smartcaresystem.ui.LauncherUI;
import com.telefield.smartcaresystem.ui.LockScreenActivity;
import com.telefield.smartcaresystem.ui.common.IDialogChangeListener;
import com.telefield.smartcaresystem.ui.sensor.DetailSensorInfoUI;
import com.telefield.smartcaresystem.util.ByteArrayToHex;
import com.telefield.smartcaresystem.util.EmergencyAction;
import com.telefield.smartcaresystem.util.LogMgr;
import com.telefield.smartcaresystem.util.SelfCall;
import com.telefield.smartcaresystem.util.SmartCarePreference;
import com.telefield.smartcaresystem.util.SpeechUtil;
import com.telefield.smartcaresystem.util.UtteranceID;
import com.telefield.smartcaresystem.wifi.SmartCareWifiManager;

import freemarker.template.SimpleDate;

public class GlobalMessageHandler {
	private IDialogChangeListener listener;
	private Handler handler;
	private long getSensorID;
	private Timer neglectTimer;
	private final long NEGLECT_TIME = 5 * 60 * 1000;
	private Object speak;
	private Timer endcallTimer;
	public static boolean TURN_ON_WIFI_STATE = false;	//Wi-Fi On 여부를 저장하는 변수(일단 false로 초기화)
	public static boolean TURN_ON_BLUETOOTH_STATE = false;	//Bluetooth On 여부를 저장하는 변수(일단 false로 초기화)
	
	/*
	 * Receive Handle
	 * /**********************************************************
	 * *****************************
	 */
	/**
	 * handleFireDetectMsg 화재 발생/해지
	 * 
	 * @param ff
	 */
	public void handleFireDetectMsg(final FormattedFrame ff) {
		
		if (!SmartCareSystemApplication.getInstance().getSensorDeviceManager()
				.isRegisteredSensor(ff.getSensor_id())) {
			LogMgr.d(LogMgr.TAG, "미동록된 센서 : " + ff.getSensor_id());
			return;
		}

		byte sub_data[] = ff.getSub_data();
		byte flag = sub_data[sub_data.length - 1];
		ProtocolManger pmanager = SmartCareSystemApplication.getInstance()
				.getProtocolManger();
		Sensor sensor = SmartCareSystemApplication.getInstance()
				.getSensorDeviceManager().getSensor(ff.getSensor_id());

		ConnectDB con = ConnectDB.getInstance(SmartCareSystemApplication
				.getInstance().getApplicationContext());

		if (flag == 0x01) // 화재 경보/해제가 같이 오므로 경보일 때만 메세지를 날려줌
		{
			
			//AVAD_yhan 20141022 통화중 경보 발생시 통화 끊고 멘트 발송하도록 수정 --[[
			PhoneManager phoneMgr = SmartCareSystemApplication.getInstance().getPhoneManager();
			
			if( phoneMgr.getPhoneState() == TelephonyManager.CALL_STATE_OFFHOOK)
			{
				SmartCareSystemApplication.getInstance().getEmergencyCallUtil().endCallEmergency(Constant.FIRE);
				LogMgr.d(LogMgr.TAG, "GlobalMessageHandler FireEndCall....CALL_STATE_OFFHOOK");
				endcallTimer = new Timer();
				endcallTimer.schedule(new TimerTask() {
					@Override
					public void run() {
						callEmergencyFire(ff);					
					}
				}, 10 * 1000);	//멘트를 말하는 데 걸리는 시간이 약 5초 정도.
			}
			//--]]
			else{
				LogMgr.d(LogMgr.TAG, "GlobalMessageHandler FireEndCall....CALL_STATE_IDLE");
				callEmergencyFire(ff);	
			}
		} else {
			
			SmartCareSystemApplication.getInstance().getMySmartCareState().setAlertFire(false);

			SensorHistory history = new SensorHistory((long) ff.getSensor_id(),
					SensorID.FIRE_SENSOR_TYPE.getSensorType(), new Date(
							System.currentTimeMillis()), DBInfo.FIRE_ALERT_CLEAR);
			con.insertSensorHistory(history);

			// 상황 해제
			
			SmartCareSystemApplication.getInstance().getEmergencyCallUtil().actionCancel();
			
			SmartCareSystemApplication
					.getInstance()
					.getSpeechUtil()
					.speech(SpeechUtil.CANCEL_FIRE_CALL,
							TextToSpeech.QUEUE_FLUSH, null);

			if (sensor != null) {
				pmanager.clearEmergenchMessage();
				ff.setSub_data(new byte[0x02]);
				pmanager.insertFrame(ProtocolFrame.getInstance(
						GatewayIndex.INFO_REPORT_SENSOR_EVENT, sensor, ff));
				pmanager.messageSend(null);
			}
		}
	}
	
	
	public void handleGasDetectMsg(final FormattedFrame ff) {
		
		if (!SmartCareSystemApplication.getInstance().getSensorDeviceManager()
				.isRegisteredSensor(ff.getSensor_id())) {
			LogMgr.d(LogMgr.TAG, "미동록된 센서 : " + ff.getSensor_id());
			return;
		}

		byte sub_data[] = ff.getSub_data();
		byte flag = sub_data[sub_data.length - 1];
		ProtocolManger pmanager = SmartCareSystemApplication.getInstance()
				.getProtocolManger();
		Sensor sensor = SmartCareSystemApplication.getInstance()
				.getSensorDeviceManager().getSensor(ff.getSensor_id());

		ConnectDB con = ConnectDB.getInstance(SmartCareSystemApplication
				.getInstance().getApplicationContext());

		if (flag == 0x01) // 화재 경보/해제가 같이 오므로 경보일 때만 메세지를 날려줌
		{
			
			//AVAD_yhan 20141022 통화중 경보 발생시 통화 끊고 멘트 발송하도록 수정 --[[
			PhoneManager phoneMgr = SmartCareSystemApplication.getInstance().getPhoneManager();
			
			if( phoneMgr.getPhoneState() == TelephonyManager.CALL_STATE_OFFHOOK)
			{
				SmartCareSystemApplication.getInstance().getEmergencyCallUtil().endCallEmergency(Constant.GAS);
				LogMgr.d(LogMgr.TAG, "GlobalMessageHandler GasEndCall....CALL_STATE_OFFHOOK");
				endcallTimer = new Timer();
				endcallTimer.schedule(new TimerTask() {
					@Override
					public void run() {
						callEmergencyGas(ff);					
					}
				}, 10 * 1000);	//멘트를 말하는 데 걸리는 시간이 약 10초 정도.
			}
			//--]]
			else{
				LogMgr.d(LogMgr.TAG, "GlobalMessageHandler GasEndCall....CALL_STATE_IDLE");
				callEmergencyGas(ff);	
			}
		} else {
			
			SmartCareSystemApplication.getInstance().getMySmartCareState().setAlertGas(false);
			
			SensorHistory history = new SensorHistory((long) ff.getSensor_id(),
					SensorID.GAS_SENSOR_TYPE.getSensorType(), new Date(
							System.currentTimeMillis()), DBInfo.GAS_ALERT_CLEAR);
			con.insertSensorHistory(history);			

			// 상황 해제			
			
			SmartCareSystemApplication.getInstance().getEmergencyCallUtil().actionCancel();
			
			SmartCareSystemApplication
					.getInstance()
					.getSpeechUtil()
					.speech(SpeechUtil.CANCEL_GAS_CALL,
							TextToSpeech.QUEUE_FLUSH, null);
			
			if (sensor != null) {
				pmanager.clearEmergenchMessage();
				ff.setSub_data(new byte[0x02]);
				pmanager.insertFrame(ProtocolFrame.getInstance(
						GatewayIndex.INFO_REPORT_SENSOR_EVENT, sensor, ff));
				pmanager.messageSend(null);
			}	
		}
	}
	
	
	public void handlePirCntMsg(FormattedFrame ff) {
		int sensor_id = ff.getSensor_id();
		byte sub_data[] = ff.getSub_data();
		int pir_cnt = (int) ((int) 0xff & sub_data[0]);

		if (!SmartCareSystemApplication.getInstance().getSensorDeviceManager()
				.isRegisteredSensor(sensor_id)) {
			LogMgr.d(LogMgr.TAG, "미동록된 센서 : " + sensor_id);
			return;
		}

		SmartCareSystemApplication.getInstance().getSensorDeviceManager()
				.updateActivityPirCnt(sensor_id, pir_cnt);
		
		if( Constant.IS_CAN_INSERT_EVERY_MINUTE_PIRCNT)
			SmartCareSystemApplication.getInstance().getSensorDeviceManager().insertActivityPirCnt(sensor_id);
		
		SmartCarePreference pref = new SmartCarePreference(SmartCareSystemApplication.getInstance().getApplicationContext(), 
				SmartCarePreference.SENSOR_SETTING_INFO_FILENAME);
		
		int threshold_val = pref.getIntValue(SmartCarePreference.NON_ACTIVITY_NUM, 1);
		
		LogMgr.d(LogMgr.TAG,"handlePirCntMsg : cnt : " + pir_cnt + ": threshold_val :" +  threshold_val + ": isReport :" +
				SmartCareSystemApplication.getInstance().getMySmartCareState().isNonActivityReport()
		);
		
		//미활동보고를 하였다면, 활동량이 감지가 되면 활동감지 보고를 한다.
		if( pir_cnt > threshold_val)
		{
			SmartCareSystemApplication.getInstance().stopNonActivityReportTimer();
			SmartCareSystemApplication.getInstance().startNonActivityReportTimer();
			
			if( SmartCareSystemApplication.getInstance().getMySmartCareState().isNonActivityReport() )
			{
				SmartCareSystemApplication.getInstance().getMySmartCareState().setReportNonActivity(false);
				M2MReportUtil.reportActivity();
			}
		}
	}

	public void handleNodeInfoMsg(FormattedFrame ff) {

		byte sub_data[] = ff.getSub_data();

		final int SUB_DATA_SIZE = 7;

		if (sub_data != null) {
			int temp_sensor_id = 0;
			int cnt = sub_data.length / SUB_DATA_SIZE;

			for (int i = 0; i < cnt; i++) {
				int each_sensor_id = (sub_data[i * SUB_DATA_SIZE] & 0xff) << 8;
				each_sensor_id += (sub_data[i * SUB_DATA_SIZE + 1] & 0xff);
				int rssi = (int) ((int) 0xff & sub_data[i * SUB_DATA_SIZE + 2]);
				byte flag = sub_data[i * SUB_DATA_SIZE + 3];
				
				byte GasBreakDevice_state = (byte)((flag & 0xDF) >> 6);
				byte sensor_batt = (byte) ((flag & 0x08) >> 3);
				byte zigbee_batt = (byte) ((flag & 0x04) >> 2);
				byte comm_stauts = (byte) ((flag & 0x02) >> 1);
				byte reg_stauts = (byte) (flag & 0x01);
				
				int batt_val = (int)( 0xff & sub_data[i * SUB_DATA_SIZE + 4]);
				int keep_alive_time = (int)(0xff & sub_data[i * SUB_DATA_SIZE + 5]);
				int sensor_version = (int)(0xff & sub_data[i * SUB_DATA_SIZE + 6]);
				//Codi, 등록된 센서만 처리한다.
				if(SensorID.getSensorType(each_sensor_id) != SensorID.CODI_SENSOR_TYPE.getSensorType()){
					if (!SmartCareSystemApplication.getInstance().getSensorDeviceManager()
							.isRegisteredSensor(each_sensor_id)) {
						continue;
					}
					LogMgr.d(LogMgr.TAG, "handleNodeInfoMsg : "+"..each_sensor_id : "+each_sensor_id + "..rssi : " 
							+ rssi + "..sensor_batt : " + sensor_batt + "..zigbee_batt : " + zigbee_batt + "..comm_stauts : " + comm_stauts
							+ "..reg_stauts : " + reg_stauts + "..batt_val : " + batt_val + "..keep_alive_time : " + keep_alive_time + "..sensor_version : " + sensor_version);
				}
				//가스차단기 현재 상태가 열려 있는지 닫혀 있는지 확인한다. 1:닫힘 0:열림
				if(SensorID.GAS_BREAKER_TYPE.getSensorType() == SensorID.getSensorType(each_sensor_id)){
					GasBreakDevice_state = (byte)((flag & 0xDF) >> 6);
				}
				//통신 상태가 불량이면, 배터리량을 0%로 업데이트 한다. 주기보고가 오면 본체 통신은 양호한 상태이기때문에 검사하지 않는다.
				/*if(((int) (comm_stauts & 0xFF) == 0) && 
					(SensorID.getSensorType(each_sensor_id) != SensorID.CODI_SENSOR_TYPE.getSensorType())) {	
					batt_val = 0;
				}2015-02-09 취약노인 지원시스템 상태 불일치로 마지막 데이터로 보내기로 함.(박대원 이사님)*/
				
				if(SensorID.getSensorType(each_sensor_id) == SensorID.CODI_SENSOR_TYPE.getSensorType()){
					GatewayDevice gatewayDevice = GatewayDevice.getInstance();
					gatewayDevice.battery_state = (byte) batt_val;
				} else {
					SmartCareSystemApplication
					.getInstance()
					.getSensorDeviceManager()
					.setSensorInfoData(each_sensor_id, rssi, sensor_batt,
							zigbee_batt, comm_stauts, reg_stauts , batt_val , keep_alive_time, sensor_version);
				}
				
				
				if(getSensorID == (long) each_sensor_id) {
					temp_sensor_id = each_sensor_id;
				}
				SmartCarePreference pref = new SmartCarePreference(SmartCareSystemApplication.getInstance().getApplicationContext(), SmartCarePreference.SENSOR_SETTING_INFO_FILENAME);
				//화재센서 전원이 부족하면 지속적으로 전원부족메시지가  출력된다.
				//안내음성 설정에서 안내음성이 나오게 할 수 있다.(2014.12.22)
				if(pref.getBooleanValue(SmartCarePreference.FIRE_ANNOUNCE, false)){
					if(SensorID.getSensorType(each_sensor_id) == SensorID.FIRE_SENSOR_TYPE.getSensorType()){
						if(batt_val < 90 && comm_stauts == 1){
							String speak = SensorID.getSensorName(each_sensor_id) + SpeechUtil.SENSOR_LOW_BATT;
							SmartCareSystemApplication
							.getInstance()
							.getSpeechUtil()
							.speech(speak ,TextToSpeech.QUEUE_FLUSH, null);
						}else if(zigbee_batt == 1){
							String speak = SensorID.getSensorName(each_sensor_id) + "지그비 " + SpeechUtil.SENSOR_LOW_BATT;
							SmartCareSystemApplication
							.getInstance()
							.getSpeechUtil()
							.speech(speak ,TextToSpeech.QUEUE_FLUSH, null);
						}else if(comm_stauts == 0){
								String speak = "화재센서와 통신상태가 좋지않습니다. 화재센서를 점검해 주십시오.";
								SmartCareSystemApplication
								.getInstance()
								.getSpeechUtil()
								.speech(speak ,TextToSpeech.QUEUE_FLUSH, null);
						}
					}
				}
				
/*
				//Jong 주기 통신을 할때 센서 베터리가 90%이하이면 음성 안내를 출력한다 --> 음성 안내는 출력하지 않고 서버에 보고한다. <--
				 * 센서 로우베터리는 주기보고로 대체한다.
				SmartCarePreference pref = new SmartCarePreference(SmartCareSystemApplication.getInstance().getApplicationContext(),SmartCarePreference.SENSOR_SETTING_INFO_FILENAME);
				boolean sensor_lowbatt_flag = pref.getBooleanValue(String.valueOf(each_sensor_id), false);
				ProtocolManger protocolManger = SmartCareSystemApplication.getInstance().getProtocolManger();
				SensorDeviceManager mgr = SmartCareSystemApplication.getInstance().getSensorDeviceManager();
				if(batt_val < 90){
					//각센서 ID를 key값으로 베터리값 TTS 출력 여부를 검사하여 10% 이하일때 한번만 출력되는 코드
					//베터리 상태가 90%이상 이었다가 85%로 떨어지면 flag는 true다
					if(sensor_lowbatt_flag == true){
						LogMgr.d(LogMgr.TAG, "Low Bett handleNodeInfoMsg sensor_lowbatt_flag " + sensor_lowbatt_flag +"..Sensor name : " + SensorID.getSensorName(each_sensor_id));
						pref.setBooleanValue(String.valueOf(each_sensor_id), false);
						if(SensorID.getSensorType(each_sensor_id) == SensorID.CODI_SENSOR_TYPE.getSensorType()){
							//본체 lowbett일때 Msg_id는 0x05이다..
							ff.setMsg_id((byte) 0x05);
							ProtocolFrame protocolFrame = ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_GW_EVENT, ff);  
							protocolManger.insertFrame(protocolFrame);		
							protocolManger.messageSend(null);
//							SmartCareSystemApplication
//							.getInstance()
//							.getSpeechUtil()
//							.speech(SpeechUtil.BOARD_LOW_BATT,TextToSpeech.QUEUE_FLUSH, null);
						} else {
							protocolManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_SENSOR_EVENT, mgr.getSensor(each_sensor_id), GateWayMessage.SENSOR_LOW_BATTERY));
							protocolManger.messageSend(null);
//							String speak = SensorID.getSensorName(each_sensor_id) + SpeechUtil.SENSOR_LOW_BATT + "." 
//									+ SensorID.getSensorName(each_sensor_id) + SpeechUtil.SENSOR_LOW_BATT + "."
//									+ SensorID.getSensorName(each_sensor_id) + SpeechUtil.SENSOR_LOW_BATT + ".";
//							SmartCareSystemApplication
//							.getInstance()
//							.getSpeechUtil()
//							.speech(speak ,TextToSpeech.QUEUE_FLUSH, null);
						}
					}
				}else if (batt_val > 93){
					LogMgr.d(LogMgr.TAG, "High Bett handleNodeInfoMsg sensor_lowbatt_flag " + sensor_lowbatt_flag +"..Sensor name : " + SensorID.getSensorName(each_sensor_id));
					pref.setBooleanValue(String.valueOf(each_sensor_id), true);
				}
				// -->
*/
			}
			SmartCareSystemPowerManager mgr = SmartCareSystemApplication.getInstance().getPwrMgr();			
			LogMgr.d(LogMgr.TAG, "GlobalMessageHandler SmartPhone battLevel : " + mgr.getBatt_level());
			
			Context context = SmartCareSystemApplication.getInstance().getApplicationContext();
			
			ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
			List<ActivityManager.RunningTaskInfo> info = am.getRunningTasks(1);
			ComponentName topComponentName = null;
			
			SmartCarePreference preference = new SmartCarePreference(SmartCareSystemApplication.getInstance().getApplicationContext(), SmartCarePreference.SERVER_SETTING_INFO_FILENAME);
			//전원이복구메시지가 없을때 안전 장치.. 
			//nodeinfo 메시지가 오면 전원이 복구되었다고 판단하고 주기보고,미활동감지를 다시 시작한다...
			boolean ispwroff = preference.getBooleanValue(SmartCarePreference.IS_POWER_OFF_FOR_PERIOD, true);
			if(ispwroff == false){
				SmartCareSystemApplication.getInstance().stopPeriodReportTimer();
				SmartCareSystemApplication.getInstance().startPeriodReportTimer();
				
				SmartCareSystemApplication.getInstance().stopNonActivityReportTimer();
				SmartCareSystemApplication.getInstance().startNonActivityReportTimer();
				preference.setBooleanValue(SmartCarePreference.IS_POWER_OFF_FOR_PERIOD, true);
				
				LogMgr.d(LogMgr.TAG, "GlobalMessageHandler nodeInfo pwrState : " + preference.getBooleanValue(SmartCarePreference.IS_POWER_OFF_FOR_PERIOD, true));
			}
			
			if (info != null) {
				topComponentName = info.get(0).topActivity;
			}
			
			if (topComponentName.getClassName().equals("com.telefield.smartcaresystem.ui.sensor.DetailSensorInfoUI")) {	//Top Activity가 센서 상세보기인 경우
				Sensor sensor = SmartCareSystemApplication.getInstance().getSensorDeviceManager().getSensor(temp_sensor_id);
				
				if(handler != null) {
					handler.sendMessage(Message.obtain(handler, DetailSensorInfoUI.UPDATE_UI, sensor));
				}
			}
			else {
				if(listener != null)
					listener.notifyChangeListener();
			}
		}
	}

	public void handleSensorLinkTestMsg(FormattedFrame ff) {

		byte data[] = ff.getSub_data();
		int rssi = (int) ((int) 0xff & data[0]);

		int sensor_id = ff.getSensor_id();

		LogMgr.d(LogMgr.TAG, "sensorID : " + sensor_id + "  RSSI : " + rssi);
		
		byte flag = data[1];

		byte sensor_batt = (byte) ((flag & 0x08) >> 3);
		byte zigbee_batt = (byte) ((flag & 0x04) >> 2);
		byte comm_stauts = (byte) ((flag & 0x02) >> 1);
		byte reg_stauts = (byte) (flag & 0x01);		
		int batt_val = (int)( 0xff & data[2]);
		int keep_alive_time = (int)(0xff & data[3]);
		int sensor_version = (int)(0xff & data[4]);

		SensorDeviceManager mgr = SmartCareSystemApplication.getInstance()
				.getSensorDeviceManager();

		if (!mgr.isRegisteredSensor(sensor_id)) {
//			return;
			SmartCareSystemApplication
					.getInstance()
					.getSpeechUtil()
					.speech(SpeechUtil.NOT_REGISTERED_SENSOR,
							TextToSpeech.QUEUE_ADD, null);
			
		} else {
			SmartCareSystemApplication
					.getInstance()
					.getSpeechUtil()
					.speech(SpeechUtil.SENSOR_CHECK_MODE, sensor_id, rssi, batt_val);
			//db update
			SmartCareSystemApplication
			.getInstance()
			.getSensorDeviceManager()
			.setSensorInfoData(sensor_id, rssi, sensor_batt,
					zigbee_batt, comm_stauts, reg_stauts , batt_val , keep_alive_time, sensor_version);			
			
			LogMgr.e(LogMgr.TAG,"sensor_name : "+ SensorID.getSensorName(sensor_id) +"sensor_version : " + sensor_version);
			//서버 보고
			ProtocolManger pm = SmartCareSystemApplication.getInstance().getProtocolManger();			
			pm.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_SENSOR_EVENT, mgr.getSensor(sensor_id), GateWayMessage.LINK_TEST_MSG));
			pm.messageSend(null);
			
			Context context = SmartCareSystemApplication.getInstance().getApplicationContext();
			ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
			List<ActivityManager.RunningTaskInfo> info = am.getRunningTasks(1);
			ComponentName topComponentName = null;
			if (info != null) {
				topComponentName = info.get(0).topActivity;
			}
			
			if (topComponentName.getClassName().equals("com.telefield.smartcaresystem.ui.sensor.DetailSensorInfoUI")) {	//Top Activity가 센서 상세보기인 경우
				Sensor sensor = SmartCareSystemApplication.getInstance().getSensorDeviceManager().getSensor(sensor_id);
				
				if(handler != null) {
					handler.sendMessage(Message.obtain(handler, DetailSensorInfoUI.UPDATE_UI, sensor));
				}
			}
			else {
				if(listener != null)
					listener.notifyChangeListener();
			}
		}
	}

	public void handleDoorStateMsg(FormattedFrame frame) {
		byte sub_data[] = frame.getSub_data();

		byte isDoorOpen = sub_data[0];
		byte prev_activit_cnt = sub_data[1];
		byte after_activity_cnt = sub_data[2];

		if (!SmartCareSystemApplication.getInstance().getSensorDeviceManager()
				.isRegisteredSensor(frame.getSensor_id())) {
			LogMgr.d(LogMgr.TAG, "미등록된 센서 : " + frame.getSensor_id());
			return;
		}

		ConnectDB con = ConnectDB.getInstance(SmartCareSystemApplication
				.getInstance().getApplicationContext());
		if (isDoorOpen == 0) {			

			SensorHistory history = new SensorHistory(
					(long) frame.getSensor_id(),
					SensorID.DOOR_SENSOR_TYPE.getSensorType(), new Date(
							System.currentTimeMillis()), DBInfo.DOOR_CLOSE);
			con.insertSensorHistory(history);	
			
			SmartCareSystemApplication
					.getInstance()
					.getMySmartCareState()
					.handleDoorEvent(prev_activit_cnt, false, after_activity_cnt);
			
			//문열림/닫힘 안내 멘트 설정값을 읽은뒤, 설정값에 따라 speak/do nothing 처리 
			SmartCarePreference pref = new SmartCarePreference(SmartCareSystemApplication.getInstance().getApplicationContext(), SmartCarePreference.SENSOR_SETTING_INFO_FILENAME);
			boolean speakDoorStatus = pref.getBooleanValue(SmartCarePreference.DOOR_OPEN_CLOSE, true);
			
			if(speakDoorStatus) {
				SmartCareSystemApplication
						.getInstance()
						.getSpeechUtil()
						.speech(SpeechUtil.CLOSE_DOOR_SENSOR,
								TextToSpeech.QUEUE_ADD, null);
			}

		} else {

			SensorHistory history = new SensorHistory(
					(long) frame.getSensor_id(),
					SensorID.DOOR_SENSOR_TYPE.getSensorType(), new Date(
							System.currentTimeMillis()), DBInfo.DOOR_OPEN);
			con.insertSensorHistory(history);
			
			SmartCareSystemApplication
			.getInstance()
			.getMySmartCareState()
			.handleDoorEvent(prev_activit_cnt, true, after_activity_cnt);
			
			//문열림/닫힘 안내 멘트 설정값을 읽은뒤, 설정값에 따라 speak/do nothing 처리 
			SmartCarePreference pref = new SmartCarePreference(SmartCareSystemApplication.getInstance().getApplicationContext(), SmartCarePreference.SENSOR_SETTING_INFO_FILENAME);
			boolean speakDoorStatus = pref.getBooleanValue(SmartCarePreference.DOOR_OPEN_CLOSE, true);
			
			if(speakDoorStatus) {
				SmartCareSystemApplication
						.getInstance()
						.getSpeechUtil()
						.speech(SpeechUtil.OPEN_DOOR_SENSOR,
								TextToSpeech.QUEUE_ADD, null);
			}
		}
	}

	public void handleCoordEventMsg(FormattedFrame frame) {
		/**
		 * COORD_MSG_OUTING 0x10 COORD_MSG_SETTING_MENU 0x11 COORD_MSG_HOOK_ON
		 * 0x20 COORD_MSG_HOOK_OFF 0x21
		 * 0x30 핸드폰충전모드진입(액세서리 모드)
		 * 0x31 핸드폰방전모드진입(USB Host)
		 */

		byte sub_data[] = frame.getSub_data();
		int flag = 0xff & (sub_data[sub_data.length - 1]);
		LogMgr.d(LogMgr.TAG,
				"GlobalMessageHandler.java:handleCoordEventMsg// frame.getMsg_id() : "
						+ frame.getMsg_id());
		LogMgr.d(
				LogMgr.TAG,
				"GlobalMessageHandler.java:i // frame.getSensor_id() :"
						+ frame.getSensor_id());
		LogMgr.d(LogMgr.TAG, "GlobalMessageHandler.java:i // sub_data :"
				+ ByteArrayToHex.byteArrayToHex(sub_data, sub_data.length));

		Context context = SmartCareSystemApplication.getInstance()
				.getApplicationContext();
		
		if( flag == 0x10 )
		{	
			//Jong 14.10.31 외출 버튼을 누르면 화면을 켠다 <--
			SmartCareSystemApplication.getInstance().getLCDMgr().setLCDOn();
			// -->
			
			//USIM이 없는경우 전화를 하지 않는다.
			TelephonyManager tm = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
			if(tm.getSimState() == TelephonyManager.SIM_STATE_ABSENT)
				return;
			
			//Jong 14.10.30 외출 버튼을 누르면 외출모드로 설정 되지 않고 보호 1로 전화가 간다. <--
			EmergencyAction emergencyCallUtil = SmartCareSystemApplication.getInstance().getEmergencyCallUtil();
			emergencyCallUtil.CallCare1();
			// -->
			SmartCareSystemApplication.getInstance().getMySmartCareState().eventReceiveHookOff();
			
			/*
			PhoneManager phoneMgr = SmartCareSystemApplication.getInstance().getPhoneManager();
			SelfCall selfCall = SmartCareSystemApplication.getInstance().getSelfCall();
			
			if( selfCall !=null && SmartCareSystemApplication.getInstance().getSelfCall().isStartSelfCall() )
			{
				//수화기를 HookOff 했을때와 같은 효과이므로
				SmartCareSystemApplication.getInstance().getMySmartCareState().eventReceiveHookOff();
				return;
			}else
			
			if( phoneMgr.getPhoneState() == TelephonyManager.CALL_STATE_RINGING ) {
				SmartCareSystemApplication.getInstance().getPhoneManager().answerCall();
				return;
			}else if( phoneMgr.getPhoneState() == TelephonyManager.CALL_STATE_OFFHOOK )
			{			
				return; //버튼 무시				
			}
		}

		if (flag == 0x10) {			
			/*
			//외출/재실 안내 멘트 설정값을 읽은뒤, 설정값에 따라 speak/do nothing 처리 
			SmartCarePreference pref = new SmartCarePreference(SmartCareSystemApplication.getInstance().getApplicationContext(), SmartCarePreference.SENSOR_SETTING_INFO_FILENAME);
			boolean speakOutInStatus = pref.getBooleanValue(SmartCarePreference.OUT_COME, true);
			
			if(speakOutInStatus) {
				SmartCareSystemApplication.getInstance().getSpeechUtil()
						.speech(SpeechUtil.GO_OUT, TextToSpeech.QUEUE_FLUSH, null);
			}
			// SmartCareSystemApplication.getInstance().getProtocolManger().emergency(frame,
			// sensorlist);
			*/
			
			
			
			
		} else if (flag == 0x21) {
			// Hook on
			// 수화기를 내릴때 : 통화중이면 통화를 끊는다. -> 초기화면으로 복귀한다.
			// 통화중이 아니면 -> 바로 초기화면으로 복귀한다.
			// 통화중이 아니고 top activity 가 Launch UI 이면 바로 시계화면으로 간다.
			
			SmartCareSystemApplication.getInstance().getMySmartCareState().setHookOn(true);
			
			
			PhoneManager phone = SmartCareSystemApplication.getInstance()
					.getPhoneManager();

			ActivityManager am = (ActivityManager) context
					.getSystemService(Context.ACTIVITY_SERVICE);
			List<ActivityManager.RunningTaskInfo> info = am.getRunningTasks(1);
			ComponentName topComponentName = null;

			if (info != null) {
				topComponentName = info.get(0).topActivity;
			}

			LogMgr.d(LogMgr.TAG, "PHONE STATE : " + phone.getPhoneState());
			if (phone.getPhoneState() == TelephonyManager.CALL_STATE_IDLE) {

				LogMgr.d(LogMgr.TAG,
						"Top component : " + topComponentName.getClassName());
				// top activity 가 LauncherUI 이면
				if (topComponentName.getClassName().equals(
						"com.telefield.smartcaresystem.ui.LauncherUI")) {
					Intent intent = new Intent(SmartCareSystemApplication
							.getInstance().getApplicationContext(),
							LockScreenActivity.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
					context.startActivity(intent);
				}
				else {
					Intent intent = new Intent(SmartCareSystemApplication.getInstance().getApplicationContext(), LauncherUI.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					context.startActivity(intent);
					
					intent = new Intent(SmartCareSystemApplication.getInstance().getApplicationContext(), LockScreenActivity.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
					context.startActivity(intent);
				}
			}
			else if(phone.getPhoneState() ==  TelephonyManager.CALL_STATE_RINGING)
			{
				//전화가 오고 있으면 HOOK ON 이벤트는 무시한다.
				;
			}
			else {
				// 전화를 끊는다.
				phone.endCall();
				
				Intent intent = new Intent(SmartCareSystemApplication.getInstance().getApplicationContext(), LauncherUI.class);					
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(intent);
				
				intent = new Intent(SmartCareSystemApplication.getInstance().getApplicationContext(), LockScreenActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
				context.startActivity(intent);
			}
			
			//송수화기를 내려 놓으면, 방치는 끝난 것이므로 Timer를 캔슬해야 된다.
			if(neglectTimer != null) {
				neglectTimer.cancel();
				neglectTimer = null;
			}
			
			//송수화기 방치 관련 사운드가 나오면(멘트 포함), 사운드를 종료시킨다.
			SmartCareSystemApplication.getInstance().getEmergencyCallUtil().stopNeglectSound();
		} else if (flag == 0x20) {
			// hook off			
			SmartCareSystemApplication.getInstance().getMySmartCareState().setHookOn(false);
			
			SmartCareSystemApplication.getInstance().getMySmartCareState().eventReceiveHookOff();	
			
			//JONG 14.10.27 // HOOK OFF하면 화면을 무조건 킨다. <--
			SmartCareSystemApplication.getInstance().getLCDMgr().setLCDOn();
			// -->
			
			// 수화기를 들때 : alerting 중이면 전화를 받는다. 그렇지 않으면 초기화면이 나온다.

			PhoneManager phone = SmartCareSystemApplication.getInstance()
					.getPhoneManager();

			ActivityManager am = (ActivityManager) context
					.getSystemService(Context.ACTIVITY_SERVICE);
			List<ActivityManager.RunningTaskInfo> info = am.getRunningTasks(1);
			ComponentName topComponentName = null;

			if (info != null) {
				topComponentName = info.get(0).topActivity;
			}		
			
			//만일 Hook Off 상태에서 Hook Off 메시지가 온다면, 타이머를 종료해야 한다.
			if(neglectTimer != null) {
				neglectTimer.cancel();
				neglectTimer = null;
			}
			
			LogMgr.d(LogMgr.TAG, "PHONE STATE : " + phone.getPhoneState());
			if (phone.getPhoneState() == TelephonyManager.CALL_STATE_IDLE) {

				LogMgr.d(LogMgr.TAG,
						"Top component : " + topComponentName.getClassName());
				// top activity 가 LauncherUI 이면
				if (topComponentName.getClassName().equals(
						"com.telefield.smartcaresystem.ui.LockScreenActivity")) {
					Intent intent = new Intent(
							"android.intent.ACTION.LOCKSCREEN_RELEASE");
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					// intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
					context.startActivity(intent);
					
					//LCD가 off된 상태에서 수화기를 들면 Launcher 화면이 보여져야 한다.
					SmartCareSystemApplication.getInstance().getLCDMgr().setLCDOn();
					
					//IDLE 상태에서 송수화기를 들면 타이머를 시작한다.
					neglectTimer = new Timer();
					neglectTimer.schedule(new TimerTask() {
						@Override
						public void run() {
							// TODO Auto-generated method stub
							//송수화기가 들려져 있다는 멘트를 하는 시점은 PhoneState가 IDLE이고, ftdi가 연결이 되어있는 상태이다
							PhoneManager innerPhone = SmartCareSystemApplication.getInstance().getPhoneManager();
							MessageManager mgr = SmartCareSystemApplication.getInstance().getMessageManager();
							
							LogMgr.d(LogMgr.TAG,"PhoneState = "+innerPhone.getPhoneState() + ", ftdi_isConnect = "+mgr.ftdi_isConnect());
							
							if(innerPhone.getPhoneState() == TelephonyManager.CALL_STATE_IDLE && mgr.ftdi_isConnect()) {
								HashMap<String, String> utteranceId = UtteranceID.TTSUtteranceId(Constant.UTTERANCE_ID_NEGLECT_TELEPHONE);
								
								SmartCareSystemApplication.getInstance().getSpeechUtil().
									speech(SpeechUtil.ALERT_NEGLECT_TELEPHONE, TextToSpeech.QUEUE_FLUSH, utteranceId);
							}
						}
					}, NEGLECT_TIME);
				}

			} else {
				// 전화를 받는다.
				SmartCareSystemApplication.getInstance().getPhoneManager()
						.answerCall();
			}
		} else if( flag == 0x30 )
		{
			//충전,accessory mode			
		}else if( flag == 0x31 )
		{
			//방전, USB host mode			
		}
		else if( flag == 0x90)
		{
			PhoneManager phoneMgr = SmartCareSystemApplication.getInstance().getPhoneManager();
			if( phoneMgr.getPhoneState() == TelephonyManager.CALL_STATE_RINGING ) {
				SmartCareSystemApplication.getInstance().getPhoneManager().answerCall();
				return;
			}else if( phoneMgr.getPhoneState() == TelephonyManager.CALL_STATE_OFFHOOK )
			{			
				return; //버튼 무시				
			}else {
				ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
				List<ActivityManager.RunningTaskInfo> info = am.getRunningTasks(1);
				ComponentName topComponentName = null;

				if (info != null) {
					topComponentName = info.get(0).topActivity;
					LogMgr.d(LogMgr.TAG,"topComponentName(at 0x90 flag) = " + topComponentName);
				}
				
				if (topComponentName.getClassName().equals("com.telefield.smartcaresystem.ui.LockScreenActivity")) {
					Intent intent = new Intent(SmartCareSystemApplication.getInstance().getApplicationContext(), LauncherUI.class);					
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					context.startActivity(intent);
				}
				else if(topComponentName.getClassName().equals("com.telefield.smartcaresystem.ui.LauncherUI")) {
					/* 한 번 add된 flag를 또 add 해도 효과가 없고, LauncherUI는 최초 실행 시 onStop()을 타므로 onStop()에 clear flag를 쓸 수 없다. 
					 * 따라서 다른 Activity로 갔다가 다시 LauncherUI로 이동한다. */
					Intent intent = new Intent(SmartCareSystemApplication.getInstance().getApplicationContext(), LockScreenActivity.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					context.startActivity(intent);
					
					intent = new Intent(SmartCareSystemApplication.getInstance().getApplicationContext(), LauncherUI.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					context.startActivity(intent);
				}else{
					SmartCareSystemApplication.getInstance().getLCDMgr().setLCDOn();
				}
			}
		}

	}

	public void handleSensorPwrMsg(FormattedFrame frame) {
		int sensor_id = frame.getSensor_id();

		if (!SmartCareSystemApplication.getInstance().getSensorDeviceManager()
				.isRegisteredSensor(sensor_id)) {
			LogMgr.d(LogMgr.TAG, "미동록된 센서 : " + sensor_id);
			return;
		}

		byte sub_data[] = frame.getSub_data();
		int flag = sub_data[0] & 0x01;
		Sensor sensor = SmartCareSystemApplication.getInstance().getSensorDeviceManager().getSensor(sensor_id);
		ProtocolManger protocolManger = SmartCareSystemApplication.getInstance().getProtocolManger();
		//Jong 14.10.28 가스센서 전원 차단 복구시 상태 업데이트 <--
		if (flag == 1) {
			// 전원 복구
			SmartCareSystemApplication
					.getInstance()
					.getSpeechUtil()
					.speech(SpeechUtil.SENSOR_POWER_ON_MDOE, sensor_id,
							TextToSpeech.QUEUE_FLUSH, null);
			
			SmartCareSystemApplication
			.getInstance()
			.getSensorDeviceManager()
			.setSensorInfoData(sensor_id, true);	
			
			
			protocolManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_SENSOR_EVENT, sensor, GateWayMessage.GAS_SENSOR_ATTACH));

		} else {
			PhoneManager phoneMgr = SmartCareSystemApplication.getInstance().getPhoneManager();
			if( phoneMgr.getPhoneState() != TelephonyManager.CALL_STATE_IDLE)
			{
				SmartCareSystemApplication.getInstance().getEmergencyCallUtil().setReservePowerOffMent(true);			
			}else
			{			
				// 전원 차단
				SmartCareSystemApplication
						.getInstance()
						.getSpeechUtil()
						.speech(SpeechUtil.SENSOR_POWER_OFF_MDOE, sensor_id,
								TextToSpeech.QUEUE_FLUSH, null);
				
				SmartCareSystemApplication
				.getInstance()
				.getSensorDeviceManager()
				.setSensorInfoData(sensor_id, false);
			}
			protocolManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_SENSOR_EVENT, sensor, GateWayMessage.GAS_SENSOR_DETACH));
			
		}
		protocolManger.messageSend(null);
		
		Context context = SmartCareSystemApplication.getInstance().getApplicationContext();

		LogMgr.d(LogMgr.TAG, "GlobalMessageHandler " + sensor.toString());
		ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		List<ActivityManager.RunningTaskInfo> info = am.getRunningTasks(1);
		ComponentName topComponentName = null;
		if (info != null) {
			topComponentName = info.get(0).topActivity;
		}
		if (topComponentName.getClassName().equals("com.telefield.smartcaresystem.ui.sensor.DetailSensorInfoUI")) {	//Top Activity가 센서 상세보기인 경우
			if(handler != null) {
				handler.sendMessage(Message.obtain(handler, DetailSensorInfoUI.UPDATE_UI, sensor));
			}
		}
		else {
			if(listener != null)
				listener.notifyChangeListener();
		}
		//-->
	}
	public void handlePwrStateMsg(FormattedFrame frame) {
		LogMgr.d(LogMgr.TAG,"handlePwrStateMsg");
		
		byte sub_data [] =frame.getSub_data();
		int pwr_conn = sub_data[0];
		
		if( pwr_conn == 0) // 비상전
		{	
			GatewayDevice.getInstance().power_state = 0x00;
			//전원이 차단 되면 베터리값을 0으로 한다.
			GatewayDevice.getInstance().battery_state = 0x00;
			PhoneManager phoneMgr = SmartCareSystemApplication.getInstance().getPhoneManager();
			if( phoneMgr.getPhoneState() != TelephonyManager.CALL_STATE_IDLE)
			{
				SmartCareSystemApplication.getInstance().getEmergencyCallUtil().setReservePowerOffMent(true);
			}else
			{
				SmartCareSystemApplication
				.getInstance()
				.getSpeechUtil()
				.speech(SpeechUtil.SPEECH_CODI_POWER_OFF,
						TextToSpeech.QUEUE_ADD, null);
			}
			
			//비상전에서는 BlueTooth, WiFi가 Off 되도록
			DeviceScan dev = new DeviceScan();
			TURN_ON_BLUETOOTH_STATE = dev.isTurnOn();
			if(TURN_ON_BLUETOOTH_STATE) {
				dev.turnOffBluetooth();
			}
			
			SmartCareWifiManager wifiManager = new SmartCareWifiManager(SmartCareSystemApplication.getInstance().getApplicationContext());
			TURN_ON_WIFI_STATE = wifiManager.isWifiEnable();
			if(TURN_ON_WIFI_STATE) {
				wifiManager.turnOffWifi();
			}
			SmartCarePreference preference = new SmartCarePreference(SmartCareSystemApplication.getInstance().getApplicationContext(), SmartCarePreference.SERVER_SETTING_INFO_FILENAME);
			//전원이 차단 되면 주기보고,미활동감지를 중지한다...
			boolean ispwroff = preference.getBooleanValue(SmartCarePreference.IS_POWER_OFF_FOR_PERIOD, true);
			if(ispwroff == true){
				SmartCareSystemApplication.getInstance().stopPeriodReportTimer();
				
				SmartCareSystemApplication.getInstance().stopNonActivityReportTimer();
				preference.setBooleanValue(SmartCarePreference.IS_POWER_OFF_FOR_PERIOD, false);
				
				LogMgr.d(LogMgr.TAG, "GlobalMessageHandler pwrOff pwrState : " + preference.getBooleanValue(SmartCarePreference.IS_POWER_OFF_FOR_PERIOD, true));
			}
			//비상전에서는 Sleep 에 진입하도록			
			SmartCareSystemApplication.getInstance().releaseWakeLock();

		}else // 상전
		{
			GatewayDevice.getInstance().power_state = 0x01;
			
			SmartCareSystemApplication.getInstance().getSpeechUtil().stopSpeech();
			SmartCareSystemApplication
			.getInstance()
			.getSpeechUtil()
			.speech(SpeechUtil.SPEECH_CODI_POWER_ON,
					TextToSpeech.QUEUE_ADD, null);
			
			//비상전 진입 전 WiFi가 On이었다면, 상전으로 복귀 시 다시 WiFi를 켜준다.
//			SmartCareWifiManager wifiManager = new SmartCareWifiManager(SmartCareSystemApplication.getInstance().getApplicationContext());
//			if(TURN_ON_WIFI_STATE) {
//				wifiManager.turnOnWifi();
//			}
			
//			//Bluetooth도 마찬가지
//			DeviceScan dev = new DeviceScan();
//			if(TURN_ON_BLUETOOTH_STATE) {
//				dev.turnOnBluetooth();
//			}
			SmartCarePreference preference = new SmartCarePreference(SmartCareSystemApplication.getInstance().getApplicationContext(), SmartCarePreference.SERVER_SETTING_INFO_FILENAME);			
			//전원이 복구되면 주기보고,미활동감지 보고를 시작한다...
			boolean ispwroff = preference.getBooleanValue(SmartCarePreference.IS_POWER_OFF_FOR_PERIOD, true);
			if(ispwroff == false){
				SmartCareSystemApplication.getInstance().stopPeriodReportTimer();
				SmartCareSystemApplication.getInstance().startPeriodReportTimer();
				
				SmartCareSystemApplication.getInstance().stopNonActivityReportTimer();
				SmartCareSystemApplication.getInstance().startNonActivityReportTimer();
				preference.setBooleanValue(SmartCarePreference.IS_POWER_OFF_FOR_PERIOD, true);
				
				LogMgr.d(LogMgr.TAG, "GlobalMessageHandler pwrOn pwrState : " + preference.getBooleanValue(SmartCarePreference.IS_POWER_OFF_FOR_PERIOD, true));
			}
			//상전에서는 Sleep에 진입하지 않도록
			SmartCareSystemApplication.getInstance().acquireWakeLock();
			
			
			//update 진행할려고 하는 내역이 있으면 업데이트를 진행시켜주자..
			if ( UpgradeController.getInstance().getState() == UpgradeController.CONTROLLER_RESERVED_STATE )
			{
				UpgradeController.getInstance().updrage();
			}
			
		}
		
		//서버에 전원 복구 보고 //서버에 전원차단 보고		
		ProtocolFrame ff = ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_GW_EVENT, frame);  
		SmartCareSystemApplication.getInstance().getProtocolManger().insertFrame(ff);		
		SmartCareSystemApplication.getInstance().getProtocolManger().messageSend(null);
	}
	
	/**
	 * 게이트웨이 전원제어 요청
	 * @param frame
	 */
	public void handlePwrContReq(FormattedFrame frame)
	{
//		SmartCareSystemApplication
//		.getInstance()
//		.getSpeechUtil()
//		.speech(SpeechUtil.SPEECH_CODI_POWER_OFF,
//				TextToSpeech.QUEUE_ADD, null);
	}
	
	public void handlGasCompMsgs(FormattedFrame frame) {
		SensorDeviceManager mgr = SmartCareSystemApplication.getInstance().getSensorDeviceManager();
		List<Sensor> sensor_list = mgr.getSensorList(SensorID.GAS_BREAKER_TYPE.getSensorType());

		if (sensor_list.size() == 0)
			return;

		Sensor sensor = sensor_list.get(0);

		if (sensor != null) {
			//AVAD_yhan 20141016 가스차단 밸브
			SmartCareSystemApplication
			.getInstance()
			.getSpeechUtil()
			.speech(SpeechUtil.GAS_BREAK_COMPL,
					TextToSpeech.QUEUE_ADD, null);
			
			
			SmartCareSystemApplication.getInstance().getProtocolManger()
					.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_SENSOR_EVENT, sensor, (byte) GateWayMessage.DATA_COMPLETE_GAS_BLOCK));
			SmartCareSystemApplication.getInstance().getProtocolManger().messageSend(null);
		}
	}

	/****************************************************************************************/
	/* SEND 명령 */
	/****************************************************************************************/

	public void sendActivityLEDCommand(int sensor_id, boolean isOn) {
		MessageManager mgr = SmartCareSystemApplication.getInstance()
				.getMessageManager();

		byte[] sub_data = new byte[1];

		if (isOn == true) {
			sub_data[0] = 1;
			mgr.sendMessage(sensor_id, (byte) MessageID.LED_ONOFF_REQ, sub_data);
		} else {
			sub_data[0] = 0;
			mgr.sendMessage(sensor_id, (byte) MessageID.LED_ONOFF_REQ, sub_data);
		}

	}
	
	public void sendActivityLEDCommandAll( boolean isOn) {
		MessageManager mgr = SmartCareSystemApplication.getInstance()
				.getMessageManager();

		byte[] sub_data = new byte[1];

		if (isOn == true) {
			sub_data[0] = 1;
			mgr.sendMessage(SensorID.getSensorId(SensorID.ACTIVITY_SENSOR_TYPE.getSensorType(), 0x00), (byte) MessageID.LED_ONOFF_REQ, sub_data);
			mgr.sendMessage(SensorID.getSensorId(SensorID.DOOR_SENSOR_TYPE.getSensorType(), 0x00), (byte) MessageID.LED_ONOFF_REQ, sub_data);
		} else {
			sub_data[0] = 0;
			mgr.sendMessage(SensorID.getSensorId(SensorID.ACTIVITY_SENSOR_TYPE.getSensorType(), 0x00), (byte) MessageID.LED_ONOFF_REQ, sub_data);
			mgr.sendMessage(SensorID.getSensorId(SensorID.DOOR_SENSOR_TYPE.getSensorType(), 0x00), (byte) MessageID.LED_ONOFF_REQ, sub_data);
		}

	}

	public void putPreference(String fileName, String KEY, String value) {
		Context ctx = SmartCareSystemApplication.getInstance()
				.getApplicationContext();
		SmartCarePreference pre = new SmartCarePreference(ctx, fileName);
		pre.setStringValue(KEY, value);
	}

	// sensor 등록 취소 메시지 전송
	public void cancelAddSensor(int sensor_id) {
		MessageManager mgr = SmartCareSystemApplication.getInstance()
				.getMessageManager();

		LogMgr.d(LogMgr.TAG, "cancelAddSensor :: sensor id :" + sensor_id);

		mgr.sendMessage(sensor_id, (byte) MessageID.REG_ESC_MSG, null);
	}

	public void sendFireClrMsg() {
		
		boolean isAlertingFire = SmartCareSystemApplication.getInstance().getMySmartCareState().getAlertFire();		
		if( isAlertingFire == false ) return;
		
		MessageManager mgr = SmartCareSystemApplication.getInstance()
				.getMessageManager();

		SensorDeviceManager sensorMgr = SmartCareSystemApplication
				.getInstance().getSensorDeviceManager();

		for (Long sensor_id : sensorMgr.getSensorID(SensorID.FIRE_SENSOR_TYPE
				.getSensorType())) {
			mgr.sendMessage(sensor_id.intValue(),
					(byte) MessageID.FIRE_CLR_MSG, null);
		}
	
//		화재경보 취소를 하면 0x00으로 묶어서 보낸다.(인증용)
//		mgr.sendMessage(SensorID.getSensorId(SensorID.FIRE_SENSOR_TYPE.getSensorType(), 0x00),(byte) MessageID.FIRE_CLR_MSG, null);
		
	}

	public void sendGasClrMsg() {
		
		boolean isAlertingGas = SmartCareSystemApplication.getInstance().getMySmartCareState().getAlertGas();
		if( isAlertingGas == false ) return;
		
		MessageManager mgr = SmartCareSystemApplication.getInstance()
				.getMessageManager();

		SensorDeviceManager sensorMgr = SmartCareSystemApplication
				.getInstance().getSensorDeviceManager();
		
		for (Long sensor_id : sensorMgr.getSensorID(SensorID.GAS_SENSOR_TYPE
				.getSensorType())) {
			mgr.sendMessage(sensor_id.intValue(), (byte) MessageID.GAS_CLR_MSG,
					null);
		}
		
//		가스경보 취소를 하면 0x00으로 묶어서 보낸다.(인증용)
//		mgr.sendMessage(SensorID.getSensorId(SensorID.GAS_SENSOR_TYPE.getSensorType(), 0x00),(byte) MessageID.GAS_CLR_MSG, null);

	}
	
	public void sendGwSleepMsg()
	{
		MessageManager mgr = SmartCareSystemApplication.getInstance()
				.getMessageManager();
		
		mgr.sendMessage( SensorID.getSensorId(SensorID.CODI_SENSOR_TYPE.getSensorType(), 0x01),
				(byte)MessageID.GW_SLP_REQ,
				null
				);
	}
	
	public void sendGwBusyMsg()
	{
		MessageManager mgr = SmartCareSystemApplication.getInstance()
				.getMessageManager();
		
		mgr.sendMessage( SensorID.getSensorId(SensorID.CODI_SENSOR_TYPE.getSensorType(), 0x01),
				(byte)MessageID.GW_BUSY_MSG,
				null
				);
	}
	
	
	/**
	 * 서버에서 각 센서 별로 keep alive time 을 setting 할때.
	 * @param sensor_id
	 * @param keepAlivetime ( 1~60 miniute)
	 */
	public void sendKeepAliveTimeMsg(int keepAlivetime)
	{
		byte[] sub_data = new byte[1];
		
		sub_data[0] = (byte)keepAlivetime;
		
		int sensor_id;		
		
		MessageManager mgr = SmartCareSystemApplication.getInstance()
				.getMessageManager();		
		
		mgr.sendMessage( SensorID.getSensorId(SensorID.CODI_SENSOR_TYPE.getSensorType(), 0x01),
				(byte)MessageID.KEEP_ALIVE_TIME_REQ,
				sub_data
				);

	}
	
	public void sendGateWayMsg(byte subdata)
	{
		byte[] sub_data = new byte[1];		
		sub_data[0] = subdata;	
		
		MessageManager mgr = SmartCareSystemApplication.getInstance()
				.getMessageManager();		
		
		mgr.sendMessage( SensorID.getSensorId(SensorID.CODI_SENSOR_TYPE.getSensorType(), 0x01),
				(byte)MessageID.GATEWAY_EVENT_MSG ,
				sub_data
				);	
	}
	/**
	 * 화재센서 가스센서용
	 * @param subdata
	 */
	public void sendEmgAlarmMsg(byte subdata) {
		
		byte[] sub_data = new byte[1];		
		sub_data[0] = subdata;
		
//		Toast.makeText(SmartCareSystemApplication.getInstance().getApplicationContext(), "sendAlarmMsg : " + SensorID.getSensorId(SensorID.CALL_EMERGENCY_SENSOR_TYPE.getSensorType(), 0x00), Toast.LENGTH_LONG).show();
		MessageManager mgr = SmartCareSystemApplication.getInstance().getMessageManager();
		mgr.sendMessage(SensorID.getSensorId(SensorID.CALL_EMERGENCY_SENSOR_TYPE.getSensorType(), 0x00), (byte) MessageID.EMG_ALARM_MSG, sub_data);
	}
	
	/**
	 * 응급호출기용
	 * @param ff
	 */
	public void sendEmgAlarmMsg(FormattedFrame ff) {
		
		byte[] sub_data = new byte[1];		
		sub_data[0] = ff.getSub_data()[0];
		
		byte sensor_index = FormattedFrame.intToByteArray(ff.getSensor_id())[1];
		byte sensor_type = FormattedFrame.intToByteArray(ff.getSensor_id())[0];
		LogMgr.d(LogMgr.TAG, "sendEmgAlarmMsg for Emergency Sensor getbytes : " + ByteArrayToHex.byteArrayToInt(ff.getBytes()) + "...sensor_id : " +ByteArrayToHex.byteArrayToInt(FormattedFrame.intToByteArray(ff.getSensor_id())));
		MessageManager mgr = SmartCareSystemApplication.getInstance().getMessageManager();
		//게이트웨이의 취소버튼을 누르면 index 0x00, subData 0x01로 보낸다.
		if((sensor_type&0xff) == SensorID.CODI_SENSOR_TYPE.getSensorType()){
			sub_data[0] = 0x01;
//			Toast.makeText(SmartCareSystemApplication.getInstance().getApplicationContext(),"sendMessage = " + SensorID.getSensorId(SensorID.CALL_EMERGENCY_SENSOR_TYPE.getSensorType(), 0x00) + " msg_id : " + (byte) MessageID.EMG_ALARM_MSG + "sub_data[] : " + ByteArrayToHex.byteArrayToHex(sub_data, sub_data.length), Toast.LENGTH_LONG).show();
			mgr.sendMessage(SensorID.getSensorId(SensorID.CALL_EMERGENCY_SENSOR_TYPE.getSensorType(), 0x00), (byte) MessageID.EMG_ALARM_MSG, sub_data);	
			return;
		}
		
		//등록이 되지 않은 응급호출기에서 취소 메시지가 오더라도 무시한다.
		SensorDeviceManager smg = SmartCareSystemApplication.getInstance()
				.getSensorDeviceManager();
		if (!smg.isRegisteredSensor(ff.getSensor_id()) && (sensor_type&0xff) == SensorID.CALL_EMERGENCY_SENSOR_TYPE.getSensorType()) {
			return;		
		}
//		Toast.makeText(SmartCareSystemApplication.getInstance().getApplicationContext(), "sensor Type : " + (sensor_type&0xff) + "...sensor_index : " + sensor_index, Toast.LENGTH_LONG).show();
		mgr.sendMessage(SensorID.getSensorId(SensorID.CALL_EMERGENCY_SENSOR_TYPE.getSensorType(), sensor_index), (byte) MessageID.EMG_ALARM_MSG, sub_data);
	}
	
	public void sendGasBreakMsg(){
		
		SensorDeviceManager sensorDeviceMgr = SmartCareSystemApplication.getInstance().getSensorDeviceManager();
		List<Sensor> sensor_list = sensorDeviceMgr.getSensorList(SensorID.GAS_BREAKER_TYPE.getSensorType());
		
		MessageManager mgr = SmartCareSystemApplication.getInstance().getMessageManager();
		for( Sensor sensor : sensor_list)
		{
			if( sensor != null)
				mgr.sendMessage(sensor.getSensor_id().intValue(), (byte) MessageID.GAS_BREAK_MSG, null);
		}
	}
	
	public void insertCamData(String path)  {
		FileInputStream fis = null;
		BufferedInputStream bis = null;
		ProtocolManger pmanager = SmartCareSystemApplication.getInstance().getProtocolManger();
		try {
			File file = new File(path);
			
			fis= new FileInputStream(file);
			bis = new BufferedInputStream(fis);

			byte[] bytes = new byte[GatewayIndex.CAM_LENGTH_DATA];

			int totalBlockNum = (int) (file.length() / bytes.length) + 1;
			LogMgr.d(LogMgr.TAG, "ProtocolManger.java:insertCamData:// totalBlockNum : " + totalBlockNum);

			int currentBlockNum = 0;

			while (bis.read(bytes) != -1) {
				currentBlockNum++;
				pmanager.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_CAM, bytes, totalBlockNum, currentBlockNum));
			}

		} catch (Exception e1) {
			e1.printStackTrace();
		} finally {
			if (bis != null)
				try {
					bis.close();
				} catch (IOException e) {
				}
			if (fis != null)
				try {
					fis.close();
				} catch (IOException e) {
				}
			pmanager.messageSend(null);
		}
	}
	
	public void startOpenning(EventRegistration registor){
//		Context ctx = SmartCareSystemApplication.getInstance().getApplicationContext();
//		ctx.startActivity(new Intent(ctx, OpenningUI.class));
		
		ProtocolManger pManger = SmartCareSystemApplication.getInstance().getProtocolManger();
		
		if(registor != null)
			pManger.setEventRegistration(registor);
		
		pManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REQUEST_TIME));
		pManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REGISTER_GW));

		List<Sensor> sensorList = SmartCareSystemApplication.getInstance().getSensorDeviceManager().getSensorList();
		for (Sensor sensor : sensorList) {
			//돌보미는 센서로 취급하지 않으므로 개통할때 등록정보를 보내지 않는다.
			if(SensorID.getSensorType(sensor.getSensor_id().intValue())>= SensorID.CARE_SENSOR_TYPE.getSensorType()){
				continue;
				}
			else{
				pManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REGISTER_SENSOR, sensor, null));
			}
		}
		sendDeviceInfo();
		pManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_GW_DATA, FormattedFrame.getInstance(GateWayMessage.ID_REPORT_FINISH_SEND)));
		pManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REQUEST_DATA));

		pManger.messageSend("개통 시작");
	}
	
	private Users selectData(List<Users> userList, String userType) {
		
		if(userList != null && userList.size() > 0){
			for (Users user : userList){
				if(user != null && user.getType().equals(userType))
					return user;				
			}
			return null;
		}
		else
			return null;
	}
	
	private int getIntPreference(String fileName, String KEY) {
		SmartCarePreference pref = new SmartCarePreference(SmartCareSystemApplication.getInstance().getApplicationContext(), fileName);
		return pref.getIntValue(KEY, 0);
	}

	private String getStringPreference(String fileName, String KEY) {
		SmartCarePreference pref = new SmartCarePreference(SmartCareSystemApplication.getInstance().getApplicationContext(), fileName);
		return pref.getStringValue(KEY, "");
	}
	
	public void sendDeviceInfo(){
		ProtocolManger pManger = SmartCareSystemApplication.getInstance().getProtocolManger();
		ByteBuffer bb;
		Users user;
		String gateWayNumber = SmartCareSystemApplication.getInstance().getMyPhoneNumber();
		ConnectDB db = ConnectDB.getInstance(SmartCareSystemApplication.getInstance().getApplicationContext());
		
		List<Users> userList = db.selectAllUsers();
		
		pManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_GW_DATA, FormattedFrame.getInstance(GateWayMessage.ID_REPORT_GW_NUM, gateWayNumber.getBytes())));
		
		user = selectData(userList, DBInfo.HELPER_TYPE_CENTER);
		if(user != null && user.getNumber() != null)
			pManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_GW_DATA, FormattedFrame.getInstance(GateWayMessage.ID_REPORT_CENTER_NUM, user.getNumber().getBytes())));
		
		user = selectData(userList, DBInfo.HELPER_TYPE_HELPER);
		if(user != null && user.getNumber() != null)
			pManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_GW_DATA, FormattedFrame.getInstance(GateWayMessage.ID_REPORT_HELPER_NUM, user.getNumber().getBytes())));
		
		user = selectData(userList, DBInfo.HELPER_TYPE_CARE1);
		if(user != null && user.getNumber() != null)
			pManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_GW_DATA, FormattedFrame.getInstance(GateWayMessage.ID_REPORT_CARE1_NUM, user.getNumber().getBytes())));
		
		user = selectData(userList, DBInfo.HELPER_TYPE_CARE2);
		if(user != null && user.getNumber() != null)
			pManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_GW_DATA, FormattedFrame.getInstance(GateWayMessage.ID_REPORT_CARE2_NUM, user.getNumber().getBytes())));
		
		user = selectData(userList, DBInfo.HELPER_TYPE_119);
		if(user != null && user.getNumber() != null)
			pManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_GW_DATA, FormattedFrame.getInstance(GateWayMessage.ID_REPORT_119_NUM, user.getNumber().getBytes())));
		
//		KT M2M에 119 번호 보고 기능 없음
//		user = selectData(userList, DBInfo.HELPER_TYPE_119);
//		if(user != null && user.getNumber() != null)
//			pManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_GW_DATA, FormattedFrame.getInstance(GateWayMessage.ID_REPORT_119_NUM, user.getNumber().getBytes())));
		
			
		SmartCarePreference pref = new SmartCarePreference(SmartCareSystemApplication.getInstance().getApplicationContext(), SmartCarePreference.ADDRESS_INFO_FILENAME);
		
		String IP = pref.getStringValue(SmartCarePreference.M2M_IP, SmartCarePreference.DEFAULT_COMMON_STRING);
		int port = pref.getIntValue(SmartCarePreference.M2M_PORT, SmartCarePreference.DEFAULT_COMMON_INT);

		InetSocketAddress inetAddress = new InetSocketAddress(IP.equals(SmartCarePreference.DEFAULT_COMMON_STRING) ? ProtocolConfig.M2M_ADDRESS : IP,
				port == SmartCarePreference.DEFAULT_COMMON_INT ? ProtocolConfig.M2M_PORT : port);
		
		String address = inetAddress.toString();
		LogMgr.d(LogMgr.TAG, "GlobalMessageHandler.java:sendDeviceInfo:// address : " + address);
		/**
		 * 2015.03.24
		 * address 맨앞에 /가 들어감. 
		 * ex>/222.122.128.21:18003 
		 */
		address = address.replace("/", "");
		
		pManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_GW_DATA, FormattedFrame.getInstance(GateWayMessage.ID_REPORT_M2M_ADDRESS, address.getBytes())));
		
		pManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_GW_DATA, FormattedFrame.getInstance(GateWayMessage.ID_REPORT_M2M_NUMBER, gateWayNumber.getBytes())));

		pManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_GW_DATA, FormattedFrame.getInstance(GateWayMessage.ID_REPORT_M2M_GW_NUM, gateWayNumber.getBytes())));
		
		bb = ByteBuffer.allocate(2).putShort((short) getIntPreference(SmartCarePreference.SENSOR_SETTING_INFO_FILENAME, SmartCarePreference.PERIODIC_REPORT_TIME));
		pManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_GW_DATA, FormattedFrame.getInstance(GateWayMessage.ID_REPORT_REPORT_CYCLE, bb.array())));
		
		bb = ByteBuffer.allocate(2).putShort((short) getIntPreference(SmartCarePreference.SENSOR_SETTING_INFO_FILENAME, SmartCarePreference.NON_ACTIVITY_TIME));
		pManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_GW_DATA, FormattedFrame.getInstance(GateWayMessage.ID_REPORT_REPORT_MISSED_TIME, bb.array())));
		
		bb = ByteBuffer.allocate(2).putShort((short) getIntPreference(SmartCarePreference.SERVER_SETTING_INFO_FILENAME, SmartCarePreference.SERVER_PERIODIC_REPORT_TIME));
		pManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_GW_DATA, FormattedFrame.getInstance(GateWayMessage.ID_REPORT_REPORT_CYCLE_TIME, bb.array())));
		
		bb = ByteBuffer.allocate(2).putShort((short) getIntPreference(SmartCarePreference.SENSOR_SETTING_INFO_FILENAME, SmartCarePreference.NON_ACTIVITY_NUM));
		pManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_GW_DATA, FormattedFrame.getInstance(GateWayMessage.ID_REPORT_MISSED_THRESHOLD, bb.array())));
		
		bb = ByteBuffer.allocate(2).putShort((short) getIntPreference(SmartCarePreference.SENSOR_SETTING_INFO_FILENAME, SmartCarePreference.DETECT_ACTIVITY_TIME));
		pManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_GW_DATA, FormattedFrame.getInstance(GateWayMessage.ID_REPORT_ACTIVE_SENSOR_VALUE, bb.array())));
		
		bb = ByteBuffer.allocate(2).putShort((short) getIntPreference(SmartCarePreference.CALL_SETTING_INFO_FILENAME, SmartCarePreference.AUTO_RECEIVING_TIME));
		pManger.insertFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_GW_DATA, FormattedFrame.getInstance(GateWayMessage.ID_REPORT_FORCE_RECEIVE, bb.array())));
		
	}
	
	public void setMobileDataEnabled(boolean enabled) {
		try {
			final ConnectivityManager conman = (ConnectivityManager) SmartCareSystemApplication.getInstance().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
			final Class conmanClass = Class.forName(conman.getClass().getName());
			final Field iConnectivityManagerField = conmanClass.getDeclaredField("mService");
			iConnectivityManagerField.setAccessible(true);
			final Object iConnectivityManager = iConnectivityManagerField.get(conman);
			final Class iConnectivityManagerClass = Class.forName(iConnectivityManager.getClass().getName());
			final Method setMobileDataEnabledMethod = iConnectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
			setMobileDataEnabledMethod.setAccessible(true);
			setMobileDataEnabledMethod.invoke(iConnectivityManager, enabled);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String makeLogFile(int days) {

		String output = Constant.LOG_PATH + File.separator + new SimpleDate((java.sql.Date) new Date()).toString() + ".zip";
		// 압축 대상(sourcePath)이 디렉토리나 파일이 아니면 리턴한다.
		File sourceFile = new File(Constant.LOG_PATH);
		if (!sourceFile.isFile() && !sourceFile.isDirectory()) {
			return null;
		}

		FileOutputStream fos = null;
		BufferedOutputStream bos = null;
		ZipOutputStream zos = null;

		try {
			fos = new FileOutputStream(output); // FileOutputStream
			bos = new BufferedOutputStream(fos); // BufferedStream
			zos = new ZipOutputStream(bos); // ZipOutputStream
			zos.setLevel(8); // 압축 레벨 - 최대 압축률은 9, 디폴트 8

			zipEntry(sourceFile, Constant.LOG_PATH, zos); // Zip 파일 생성
			zos.finish(); // ZipOutputStream finish
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			if (zos != null) {
				try {
					zos.close();
				} catch (IOException e) {
				}
			}
			if (bos != null) {
				try {
					bos.close();
				} catch (IOException e) {
				}
			}
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
				}
			}
		}

		return output;
	}
	
	private void zipEntry(File sourceFile, String sourcePath, ZipOutputStream zos) throws Exception {
		// sourceFile 이 디렉토리인 경우 하위 파일 리스트 가져와 재귀호출
		if (sourceFile.isDirectory()) {
			if (sourceFile.getName().equalsIgnoreCase(".metadata")) {
				return;
			}
			File[] fileArray = sourceFile.listFiles(); // sourceFile 의 하위 파일 리스트
			for (int i = 0; i < fileArray.length; i++) {
				zipEntry(fileArray[i], sourcePath, zos); // 재귀 호출
			}
		} else { // sourcehFile 이 디렉토리가 아닌 경우
			BufferedInputStream bis = null;

			try {
				String sFilePath = sourceFile.getPath();
				LogMgr.d(LogMgr.TAG, "SubCheckReceiveData.java:zipEntry:// sFilePath : " + sFilePath);

				StringTokenizer tok = new StringTokenizer(sFilePath, "/");

				int tok_len = tok.countTokens();
				String zipEntryName = tok.toString();
				while (tok_len != 0) {
					tok_len--;
					zipEntryName = tok.nextToken();
				}
				bis = new BufferedInputStream(new FileInputStream(sourceFile));

				ZipEntry zentry = new ZipEntry(zipEntryName);
				zentry.setTime(sourceFile.lastModified());
				zos.putNextEntry(zentry);

				byte[] buffer = new byte[2048];
				int cnt = 0;

				while ((cnt = bis.read(buffer, 0, 2048)) != -1) {
					zos.write(buffer, 0, cnt);
				}
				zos.closeEntry();
			} finally {
				if (bis != null) {
					bis.close();
				}
			}
		}
	}
	private void callEmergencyFire(FormattedFrame ff){
		ProtocolManger pmanager = SmartCareSystemApplication.getInstance()
				.getProtocolManger();
		Sensor sensor = SmartCareSystemApplication.getInstance()
				.getSensorDeviceManager().getSensor(ff.getSensor_id());

		ConnectDB con = ConnectDB.getInstance(SmartCareSystemApplication
				.getInstance().getApplicationContext());
		
		SmartCareSystemApplication.getInstance().getMySmartCareState().setAlertFire(true);	
		
		SensorHistory history = new SensorHistory((long) ff.getSensor_id(),
				SensorID.FIRE_SENSOR_TYPE.getSensorType(), new Date(
						System.currentTimeMillis()), DBInfo.FIRE_ALERT_ON);
		con.insertSensorHistory(history);
					
		// 응급 호출기로 응급 상황 발생 메시지 전달 , 가스차단 메시지 전달
		SmartCareSystemApplication.getInstance().getGlobalMsgFunc().sendEmgAlarmMsg(MessageID.OCCURED_EMG);
		SmartCareSystemApplication.getInstance().getGlobalMsgFunc().sendGasBreakMsg();

		// 화재 발생 및 119 로 전화
		SmartCareSystemApplication.getInstance().getEmergencyCallUtil()
				.Call119(ff, Constant.FIRE);
		if (sensor != null) {
			pmanager.insertEmergencyFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_SENSOR_EVENT, sensor, ff));
			pmanager.emergencyMessageSend();
		}
	}
	
	private void callEmergencyGas(FormattedFrame ff){
		ProtocolManger pmanager = SmartCareSystemApplication.getInstance()
				.getProtocolManger();
		Sensor sensor = SmartCareSystemApplication.getInstance()
				.getSensorDeviceManager().getSensor(ff.getSensor_id());

		ConnectDB con = ConnectDB.getInstance(SmartCareSystemApplication
				.getInstance().getApplicationContext());
		SmartCareSystemApplication.getInstance().getMySmartCareState().setAlertGas(true);
		
		SensorHistory history = new SensorHistory((long) ff.getSensor_id(),
				SensorID.GAS_SENSOR_TYPE.getSensorType(), new Date(
						System.currentTimeMillis()), DBInfo.GAS_ALERT_ON);
		con.insertSensorHistory(history);		

		
		// 응급 호출기로 응급 상황 발생 메시지 전달 , 가스차단 메시지 전달
		SmartCareSystemApplication.getInstance().getGlobalMsgFunc().sendEmgAlarmMsg(MessageID.OCCURED_EMG);
		SmartCareSystemApplication.getInstance().getGlobalMsgFunc().sendGasBreakMsg();

		// 가스 경보 발생 119 전화
		SmartCareSystemApplication.getInstance().getEmergencyCallUtil()
				.Call119(ff, Constant.GAS);
		if (sensor != null) {
			LogMgr.d(LogMgr.TAG, "가스경보 서버전송");
			pmanager.insertEmergencyFrame(ProtocolFrame.getInstance(GatewayIndex.INFO_REPORT_SENSOR_EVENT, sensor, ff));
			pmanager.emergencyMessageSend();
		}
	}
	public void setDialogChangeListener(IDialogChangeListener listener) {
		this.listener = listener;
	}
	
	public void setHandler(Handler handler) {
		this.handler = handler;
	}
	
	public void setGetSensorID(long getSensorID) {
		this.getSensorID = getSensorID;
	}
}
