package com.telefield.server.util.ftp;

import java.io.IOException;
import java.net.SocketException;

import org.apache.commons.net.ftp.FTPClient;

public class FtpConnect {
	
	private String id;
	private String pw;
	private String host;
	private FTPClient client;
	private boolean isConnect;	
	
	public FTPClient getClient() {
		return client;
	}

	public boolean isConnect() {
		return isConnect;
	}

	private FtpConnect(Builder builder) {
		this.id = builder.id;
		this.pw = builder.pw;
		this.host = builder.host;
	}
	
	public boolean connect() throws SocketException, IOException {
		client = new FTPClient();
		client.connect(host);
		client.enterLocalPassiveMode();
		isConnect = client.login(id, pw);
		return isConnect;
	}
	
	public static class Builder {
		private String id;
		private String pw;
		private String host;
		
		public Builder id(final String id) {
			this.id = id;
			return this;
		}
		
		public Builder pw(final String pw) {
			this.pw = pw;
			return this;
		}
		
		public Builder host(final String host) {
			this.host = host;
			return this;
		}
		
		public FtpConnect defaultValueBuild() {
			this.id = "telefield";
			this.pw = "xpffpvlfem!1";
			this.host = "222.122.128.45";
			
			return new FtpConnect(this);
		}
		
		public FtpConnect build() {
			return new FtpConnect(this);
		}
	}
}
