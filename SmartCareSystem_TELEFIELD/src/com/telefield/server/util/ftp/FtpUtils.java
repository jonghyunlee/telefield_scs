package com.telefield.server.util.ftp;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketException;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import com.telefield.smartcaresystem.util.LogMgr;

public class FtpUtils {
	
	private FtpConnect ftpConnect;
	private FTPClient client;
	
	public FtpUtils(final FtpConnect ftpConnect) {
		this.ftpConnect = ftpConnect;
		this.client = ftpConnect.getClient();
	}
	
	public boolean upload(final String save_file_folder_path,final String save_file_name) throws IOException {
		
		if(!isConnected()) {
			client = reConnect();
		}
		
		boolean isSaved = false;
		final String path = save_file_folder_path + "/" + save_file_name;
        final BufferedInputStream bis = new BufferedInputStream(new FileInputStream(new File(path)));
        
        
        client.setFileType(FTP.BINARY_FILE_TYPE,FTP.BINARY_FILE_TYPE);
        client.setFileTransferMode(FTP.BLOCK_TRANSFER_MODE);
        isSaved = client.storeFile(save_file_name,bis);
        
        bis.close();
        client.logout();
        
        return isSaved;
	}
	
	public boolean upload(final byte[] upload_content,final String upload_folder_path,final String save_file_name) throws IOException {
		
		this.ftpConnect = new FtpConnect.Builder().defaultValueBuild();
		ftpConnect.connect();
		this.client = ftpConnect.getClient();
		
		LogMgr.d("PICTURE","client : " + client);
		LogMgr.d("PICTURE","upload_folder_path : " + upload_folder_path);
		LogMgr.d("PICTURE","save_file_name : " + save_file_name);
		
		if(!isConnected()) {
			client = reConnect();
		}

		client.changeWorkingDirectory(upload_folder_path);
		
		boolean isSaved = false;
        final BufferedInputStream bis = new BufferedInputStream(new ByteArrayInputStream(upload_content));
        
        
        client.setFileType(FTP.BINARY_FILE_TYPE,FTP.BINARY_FILE_TYPE);
        client.setFileTransferMode(FTP.BLOCK_TRANSFER_MODE);
        isSaved = client.storeFile(save_file_name,bis);
        
        bis.close();
        client.logout();
        
        return isSaved;
	}

	
	public boolean download(
			final String download_file_name,
			final String download_folder_path,
			final String save_folder_path,
			final String save_file_name) throws FileNotFoundException , IOException , SocketException {
		
		if(!isConnected()) {
			client = reConnect();
		}
		
		boolean isDownloaded = false;
		final boolean isChangeDirectory = client.changeWorkingDirectory(download_folder_path);
		
		if(!isChangeDirectory) {
			throw new FileNotFoundException();
		}
		
		final FileOutputStream fos = new FileOutputStream(new File(save_folder_path+"/"+save_file_name));
		isDownloaded = client.retrieveFile(download_file_name,fos);
		
		return isDownloaded;
	}
	
	private boolean isConnected() {
		return client != null || client.isConnected() == true;
	}
	
	private FTPClient reConnect() throws SocketException, IOException {
		ftpConnect.connect();
		return ftpConnect.getClient();
	}
}
